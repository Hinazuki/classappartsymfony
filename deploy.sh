app/console assets:install --symlink web/
app/console assetic:dump --env=prod --no-debug
rm -Rf app/cache/prod/
rm -Rf app/cache/dev/
chmod -R 777 app/cache
chmod -R 777 app/logs
