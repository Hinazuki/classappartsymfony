<?php

/* PatCompteBundle:AdminReservation:liste_content.html.twig */
class __TwigTemplate_0069593874733f12db37a463ecd2d6c096942be66e7d4f1a218e738def08ca26 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "
  <div id=\"contenuCentrale1\">
    <h1>Réservation</h1>

    <div class=\"clear\"></div>

    <div class=\"formRechercheAdmin\">
      <form action=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("pat_admin_reservation_index");
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), 'enctype');
        echo ">
        <div class=\"row-fluid\">
          <div class=\"span6\">
            ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "reference_resa", array()), 'label');
        echo "
            ";
        // line 13
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "reference_resa", array()), 'widget');
        echo "
          </div>
          <div class=\"span6\">
            ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "reference_appart", array()), 'label');
        echo "
            ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "reference_appart", array()), 'widget');
        echo "
          </div>
        </div>

        <div class=\"row-fluid\">
          <div class=\"span6\">
            ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "reference_locataire", array()), 'label');
        echo "
            ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "reference_locataire", array()), 'widget');
        echo "
          </div>
          <div class=\"span6\">
          </div>
        </div>

        <div class=\"row-fluid\">
          <div class=\"span6\">
            ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "nom_locataire", array()), 'label');
        echo "
            ";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "nom_locataire", array()), 'widget');
        echo "
          </div>
          <div class=\"span6\">
            ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "prenom_locataire", array()), 'label');
        echo "
            ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "prenom_locataire", array()), 'widget');
        echo "
          </div>
        </div>

        <div class=\"row-fluid\">
          <div class=\"span6\">
            ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "date_debut", array()), 'label');
        echo "
            ";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "date_debut", array()), 'widget', array("attr" => array("class" => "cal_date_in_admin")));
        echo "
          </div>
          <div class=\"span6\">
            ";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "date_fin", array()), 'label');
        echo "
            ";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "date_fin", array()), 'widget', array("attr" => array("class" => "cal_date_out_admin")));
        echo "
          </div>
        </div>

        <div class=\"row-fluid\">
          <div>
            ";
        // line 54
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), "only_between", array()), 'widget');
        echo "
          </div>
        </div>

        <br/><br/>
        <div class=\"row-fluid center\">
          <input type=\"submit\" value=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rechercher"), "html", null, true);
        echo "\" class=\"btn btn-primary\"/>
        </div>

        ";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formRechercheAvancee"]) ? $context["formRechercheAvancee"] : $this->getContext($context, "formRechercheAvancee")), 'rest');
        echo "
      </form>
    </div>

    <br/><br/>

    ";
        // line 69
        $context["__internal_02ab77b629fdfebf1cea0bd575379707fa463cd2937678a17c882e6283e25eb5"] = $this->env->loadTemplate("MopaBootstrapBundle::flash.html.twig");
        // line 70
        echo "    ";
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "peekAll", array())) > 0)) {
            // line 71
            echo "      <div class=\"row\">
        <div class=\"span12\">
          ";
            // line 73
            echo $context["__internal_02ab77b629fdfebf1cea0bd575379707fa463cd2937678a17c882e6283e25eb5"]->getsession_flash();
            echo "
        </div>
      </div>
    ";
        }
        // line 77
        echo "
    ";
        // line 78
        if ((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation"))) {
            // line 79
            echo "
      ";
            // line 80
            echo $this->env->getExtension('knp_pagination')->render((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")));
            echo "

      ";
            // line 82
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["resa"]) {
                // line 83
                echo "        <table class=\"ListeAppart\" style=\"width: 100%;\">
          <tr>
            <td class=\"ListeAppart1\" style=\"width: 40%;\">
              <div class=\"titreAppart\">
                ";
                // line 87
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "prenom", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "nom", array()), "html", null, true);
                echo " <small>(";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "username", array()), "html", null, true);
                echo ")</small><br/>
                Réservation : ";
                // line 88
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "reference", array()), "html", null, true);
                echo "
              </div>
              <div class=\"reference\">Bien : Réf. ";
                // line 90
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "reference", array()), "html", null, true);
                echo "</div>
            </td>
            <td class=\"ListeAppart1 center\" style=\"width: 20%;\">
              Réservé le ";
                // line 93
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "createdAt", array()), "d/m/Y"), "html", null, true);
                echo "
            </td>
            <td class=\"ListeAppart1 center\" style=\"width: 20%;\">
              Du ";
                // line 96
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "dateDebut", array()), "d/m/Y"), "html", null, true);
                echo "<br/>au ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "dateFin", array()), "d/m/Y"), "html", null, true);
                echo "
            </td>
            <td class=\"ListeAppart2\" style=\"width: 20%;\">
              ";
                // line 99
                $this->env->loadTemplate("PatCompteBundle:Reservation:_resa_status.html.twig")->display($context);
                // line 100
                echo "
              <br/><br/>
              <a href=\"";
                // line 102
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_show", array("id_reservation" => $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "id", array()))), "html", null, true);
                echo "\"><i class=\"icon-search\"></i></a>&nbsp;&nbsp;&nbsp;
              <a href=\"";
                // line 103
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_modifier", array("id_reservation" => $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "id", array()))), "html", null, true);
                echo "\"><i class=\"icon-edit\"></i></a>&nbsp;&nbsp;&nbsp;
                ";
                // line 107
                echo "            </td>
          </tr>
        </table>
      ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['resa'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 111
            echo "
      <br/>
      ";
            // line 113
            echo $this->env->getExtension('knp_pagination')->render((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")));
            echo "

    ";
        } else {
            // line 116
            echo "      La recherche n'a renvoyé aucune réservation.
    ";
        }
        // line 118
        echo "  </div><!-- /contenuCentrale1 -->

";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:AdminReservation:liste_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  281 => 118,  197 => 83,  419 => 185,  308 => 130,  260 => 109,  228 => 96,  355 => 153,  346 => 150,  288 => 122,  692 => 459,  603 => 377,  561 => 347,  515 => 319,  475 => 300,  379 => 162,  348 => 227,  298 => 129,  1174 => 722,  1167 => 718,  1161 => 715,  1154 => 710,  1149 => 707,  1141 => 704,  1134 => 702,  1131 => 701,  1129 => 700,  1114 => 695,  1111 => 694,  1109 => 693,  1105 => 692,  1096 => 689,  1092 => 688,  1089 => 687,  1072 => 675,  1064 => 670,  1042 => 651,  1034 => 645,  1026 => 642,  1022 => 640,  1019 => 639,  1017 => 638,  1012 => 637,  1008 => 635,  1004 => 633,  1002 => 632,  996 => 629,  988 => 626,  984 => 624,  979 => 623,  977 => 622,  968 => 616,  962 => 613,  951 => 605,  917 => 586,  915 => 585,  912 => 584,  905 => 580,  897 => 575,  893 => 574,  881 => 568,  873 => 562,  871 => 561,  863 => 555,  853 => 496,  842 => 488,  831 => 480,  812 => 464,  800 => 456,  798 => 455,  787 => 447,  754 => 423,  747 => 418,  739 => 414,  731 => 410,  725 => 407,  718 => 404,  696 => 388,  681 => 378,  675 => 375,  665 => 369,  663 => 368,  651 => 359,  628 => 344,  606 => 334,  602 => 332,  600 => 331,  579 => 315,  572 => 311,  555 => 303,  542 => 295,  532 => 323,  513 => 282,  501 => 315,  499 => 271,  473 => 267,  441 => 245,  438 => 198,  416 => 231,  330 => 143,  289 => 158,  633 => 347,  627 => 291,  621 => 289,  619 => 288,  594 => 272,  576 => 263,  570 => 353,  562 => 255,  558 => 254,  552 => 251,  540 => 245,  508 => 228,  498 => 314,  486 => 308,  480 => 215,  454 => 201,  450 => 200,  410 => 265,  325 => 215,  220 => 93,  407 => 237,  402 => 234,  377 => 245,  313 => 172,  232 => 96,  465 => 197,  457 => 192,  417 => 270,  411 => 176,  408 => 225,  396 => 256,  391 => 168,  388 => 229,  372 => 160,  344 => 226,  332 => 220,  293 => 130,  274 => 149,  231 => 98,  200 => 82,  792 => 418,  788 => 416,  782 => 414,  776 => 439,  774 => 411,  762 => 402,  748 => 394,  742 => 391,  734 => 386,  730 => 385,  724 => 382,  716 => 403,  706 => 373,  698 => 389,  694 => 367,  688 => 364,  680 => 359,  676 => 358,  662 => 350,  658 => 349,  652 => 346,  644 => 341,  640 => 340,  634 => 337,  622 => 331,  616 => 386,  608 => 323,  598 => 273,  580 => 264,  564 => 297,  545 => 287,  541 => 286,  526 => 237,  507 => 317,  488 => 257,  462 => 259,  433 => 226,  424 => 235,  395 => 169,  382 => 168,  376 => 162,  341 => 188,  327 => 140,  320 => 153,  310 => 206,  291 => 118,  278 => 121,  259 => 122,  244 => 102,  448 => 250,  443 => 230,  429 => 259,  406 => 174,  366 => 206,  318 => 211,  282 => 192,  258 => 178,  222 => 93,  120 => 47,  272 => 115,  266 => 147,  226 => 159,  178 => 85,  111 => 44,  393 => 255,  386 => 166,  380 => 163,  362 => 157,  358 => 207,  342 => 139,  340 => 287,  334 => 284,  326 => 278,  319 => 138,  314 => 136,  299 => 163,  265 => 182,  252 => 107,  237 => 128,  194 => 75,  132 => 53,  23 => 3,  97 => 45,  81 => 32,  53 => 16,  654 => 223,  637 => 295,  632 => 206,  625 => 343,  623 => 342,  617 => 195,  612 => 337,  604 => 322,  593 => 327,  591 => 186,  586 => 365,  583 => 183,  578 => 180,  571 => 178,  557 => 177,  534 => 242,  522 => 236,  520 => 320,  504 => 316,  494 => 158,  463 => 145,  446 => 186,  440 => 136,  434 => 134,  431 => 240,  427 => 182,  405 => 176,  401 => 221,  397 => 114,  389 => 215,  381 => 210,  357 => 148,  349 => 193,  339 => 146,  303 => 128,  295 => 119,  287 => 121,  268 => 114,  74 => 22,  470 => 398,  452 => 236,  444 => 197,  435 => 405,  430 => 397,  414 => 183,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 887,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 864,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 825,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 808,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 763,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 732,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 697,  1387 => 694,  1378 => 688,  1374 => 687,  1368 => 684,  1359 => 678,  1355 => 677,  1349 => 674,  1340 => 668,  1336 => 667,  1330 => 664,  1321 => 658,  1317 => 657,  1311 => 654,  1302 => 648,  1298 => 647,  1292 => 644,  1283 => 638,  1279 => 637,  1273 => 634,  1264 => 628,  1260 => 627,  1254 => 624,  1245 => 618,  1241 => 617,  1235 => 614,  1226 => 608,  1222 => 607,  1216 => 604,  1207 => 598,  1203 => 597,  1197 => 594,  1188 => 588,  1184 => 587,  1178 => 584,  1169 => 578,  1165 => 577,  1159 => 574,  1150 => 568,  1146 => 567,  1140 => 564,  1123 => 699,  1119 => 697,  1113 => 546,  1104 => 540,  1100 => 690,  1094 => 536,  1085 => 686,  1081 => 529,  1075 => 526,  1066 => 671,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 599,  933 => 450,  929 => 588,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 569,  874 => 420,  870 => 419,  866 => 418,  860 => 510,  851 => 409,  847 => 408,  841 => 405,  832 => 399,  828 => 398,  822 => 395,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 431,  756 => 359,  752 => 395,  746 => 355,  737 => 413,  733 => 348,  727 => 408,  712 => 376,  708 => 332,  702 => 329,  693 => 323,  689 => 322,  683 => 379,  674 => 241,  670 => 355,  664 => 309,  649 => 297,  645 => 296,  639 => 350,  630 => 287,  626 => 332,  620 => 341,  611 => 277,  607 => 279,  601 => 376,  592 => 267,  588 => 269,  582 => 263,  563 => 348,  554 => 344,  550 => 246,  544 => 246,  535 => 283,  531 => 236,  516 => 233,  512 => 318,  506 => 163,  493 => 312,  478 => 253,  468 => 209,  459 => 144,  455 => 209,  449 => 193,  436 => 192,  428 => 278,  422 => 234,  409 => 180,  403 => 261,  390 => 230,  384 => 165,  351 => 152,  337 => 187,  311 => 118,  296 => 124,  256 => 108,  241 => 168,  215 => 91,  207 => 86,  192 => 80,  186 => 76,  175 => 80,  153 => 70,  118 => 48,  61 => 19,  34 => 8,  65 => 28,  77 => 32,  37 => 7,  190 => 77,  161 => 60,  137 => 40,  126 => 35,  261 => 106,  255 => 107,  247 => 105,  242 => 102,  214 => 87,  211 => 88,  191 => 79,  157 => 37,  145 => 71,  127 => 54,  373 => 111,  367 => 159,  363 => 155,  359 => 154,  354 => 152,  343 => 147,  335 => 145,  328 => 152,  322 => 138,  315 => 170,  309 => 140,  305 => 115,  302 => 267,  290 => 157,  284 => 121,  279 => 119,  271 => 113,  264 => 110,  248 => 103,  236 => 99,  223 => 93,  170 => 78,  110 => 40,  96 => 38,  84 => 32,  472 => 210,  467 => 148,  375 => 161,  371 => 160,  360 => 183,  356 => 232,  353 => 194,  350 => 151,  338 => 145,  336 => 221,  331 => 141,  321 => 130,  316 => 173,  307 => 132,  304 => 268,  301 => 122,  297 => 131,  292 => 123,  286 => 116,  283 => 105,  277 => 116,  275 => 188,  270 => 148,  263 => 112,  257 => 121,  253 => 108,  249 => 135,  245 => 92,  233 => 92,  225 => 89,  216 => 90,  206 => 149,  202 => 78,  198 => 105,  185 => 95,  180 => 82,  177 => 57,  165 => 65,  150 => 53,  124 => 51,  113 => 51,  100 => 39,  58 => 15,  251 => 173,  234 => 163,  213 => 113,  195 => 91,  174 => 84,  167 => 77,  146 => 51,  140 => 58,  128 => 52,  104 => 43,  90 => 23,  83 => 31,  52 => 11,  596 => 225,  590 => 314,  585 => 221,  577 => 357,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 210,  551 => 176,  546 => 207,  543 => 206,  539 => 205,  529 => 322,  525 => 173,  523 => 321,  518 => 193,  514 => 192,  509 => 189,  503 => 266,  500 => 160,  497 => 263,  495 => 313,  492 => 157,  490 => 219,  487 => 213,  484 => 256,  482 => 177,  479 => 176,  477 => 401,  474 => 206,  471 => 173,  469 => 172,  466 => 146,  464 => 292,  461 => 169,  458 => 239,  456 => 167,  451 => 189,  445 => 160,  442 => 199,  439 => 229,  437 => 262,  432 => 191,  426 => 188,  423 => 186,  420 => 219,  418 => 251,  413 => 172,  399 => 237,  394 => 162,  378 => 162,  370 => 159,  368 => 203,  365 => 212,  361 => 199,  347 => 148,  345 => 94,  333 => 131,  329 => 179,  323 => 139,  317 => 272,  312 => 131,  306 => 205,  300 => 110,  294 => 199,  285 => 193,  280 => 117,  276 => 116,  267 => 111,  250 => 107,  239 => 100,  229 => 116,  218 => 157,  212 => 88,  210 => 97,  205 => 83,  188 => 79,  184 => 75,  181 => 74,  169 => 87,  160 => 73,  152 => 61,  148 => 60,  139 => 41,  134 => 45,  114 => 47,  107 => 76,  76 => 30,  70 => 24,  273 => 127,  269 => 94,  254 => 139,  246 => 103,  243 => 88,  240 => 101,  238 => 100,  235 => 95,  230 => 127,  227 => 99,  224 => 94,  221 => 96,  219 => 112,  217 => 84,  208 => 87,  204 => 86,  179 => 71,  159 => 65,  143 => 58,  135 => 56,  131 => 87,  108 => 44,  102 => 26,  71 => 28,  67 => 27,  63 => 23,  59 => 15,  47 => 13,  94 => 25,  89 => 34,  85 => 33,  79 => 30,  75 => 31,  68 => 25,  56 => 19,  50 => 13,  38 => 9,  29 => 4,  87 => 35,  72 => 26,  55 => 21,  21 => 2,  26 => 2,  35 => 9,  31 => 3,  41 => 15,  28 => 2,  201 => 145,  196 => 81,  183 => 77,  171 => 64,  166 => 59,  163 => 79,  156 => 71,  151 => 69,  142 => 63,  138 => 44,  136 => 60,  123 => 45,  121 => 54,  115 => 52,  105 => 32,  101 => 39,  91 => 36,  69 => 30,  66 => 23,  62 => 16,  49 => 23,  98 => 40,  93 => 41,  88 => 33,  78 => 20,  46 => 12,  44 => 18,  32 => 5,  27 => 4,  43 => 12,  40 => 14,  25 => 4,  24 => 2,  172 => 79,  158 => 58,  155 => 77,  129 => 61,  119 => 49,  117 => 36,  20 => 1,  22 => 2,  19 => 1,  209 => 87,  203 => 87,  199 => 84,  193 => 76,  189 => 87,  187 => 96,  182 => 137,  176 => 73,  173 => 89,  168 => 68,  164 => 67,  162 => 85,  154 => 54,  149 => 73,  147 => 48,  144 => 59,  141 => 35,  133 => 48,  130 => 42,  125 => 55,  122 => 82,  116 => 46,  112 => 45,  109 => 47,  106 => 31,  103 => 42,  99 => 44,  95 => 37,  92 => 37,  86 => 36,  82 => 27,  80 => 31,  73 => 19,  64 => 24,  60 => 23,  57 => 17,  54 => 15,  51 => 13,  48 => 17,  45 => 13,  42 => 15,  39 => 16,  36 => 9,  33 => 9,  30 => 3,);
    }
}
