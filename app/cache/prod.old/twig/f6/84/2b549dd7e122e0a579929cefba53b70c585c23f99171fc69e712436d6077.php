<?php

/* PatFrontBundle:Appartement:detail_content.html.twig */
class __TwigTemplate_f6842b549dd7e122e0a579929cefba53b70c585c23f99171fc69e712436d6077 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "  ";
        if ((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement"))) {
            // line 3
            echo "    <h1>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Résultat"), "html", null, true);
            echo "</h1>

    ";
            // line 5
            if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "started", array())) {
                // line 6
                echo "      ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "warning"), "method"));
                foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
                    // line 7
                    echo "        <div class=\"alert alert-block\">
          ";
                    // line 8
                    echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
                    echo "
        </div>
      ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 11
                echo "    ";
            }
            // line 12
            echo "
    <div class=\"ficheProduit row\">
      <div class=\"reference span9\">
        ";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nomResidence", array()), "html", null, true);
            echo " | Réf : ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "reference", array()), "html", null, true);
            echo "
      </div>
      <div class=\"titreAppartFiche span9\">
        <strong>
          ";
            // line 19
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "type", array())), "html", null, true);
            echo " ";
            if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbPieces", array())) {
                echo "T";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbPieces", array()), "html", null, true);
            }
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "surfaceSol", array()), "html", null, true);
            echo " m<sup>2</sup>
          ";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("situe a"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "ville", array()), "html", null, true);
            echo "
        </strong>
      </div>




      <div id=\"contenuCentrale\" class=\"span6\">
        ";
            // line 28
            if ((isset($context["media"]) ? $context["media"] : $this->getContext($context, "media"))) {
                // line 29
                echo "          ";
                // line 43
                echo "


          <div class=\"royalSlider rsDefault\">
            ";
                // line 47
                $context["k"] = "0";
                // line 48
                echo "            ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["media"]) ? $context["media"] : $this->getContext($context, "media")));
                foreach ($context['_seq'] as $context["_key"] => $context["medias"]) {
                    // line 49
                    echo "              ";
                    $context["myimg"] = ((("/images/photos/biens/" . $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "reference", array())) . "/") . $this->getAttribute($this->getAttribute((isset($context["media"]) ? $context["media"] : $this->getContext($context, "media")), (isset($context["k"]) ? $context["k"] : $this->getContext($context, "k")), array(), "array"), "fichier", array()));
                    // line 50
                    echo "              <img class=\"rsImg\" src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter((isset($context["myimg"]) ? $context["myimg"] : $this->getContext($context, "myimg")), "appartement_big"), "html", null, true);
                    echo "\" data-rsBigImg=\"";
                    echo twig_escape_filter($this->env, (isset($context["myimg"]) ? $context["myimg"] : $this->getContext($context, "myimg")), "html", null, true);
                    echo "\" data-rsTmb=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter((isset($context["myimg"]) ? $context["myimg"] : $this->getContext($context, "myimg")), "appartement_thumb"), "html", null, true);
                    echo "\" alt=\"\" />
              ";
                    // line 51
                    $context["k"] = ((isset($context["k"]) ? $context["k"] : $this->getContext($context, "k")) + 1);
                    // line 52
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['medias'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 53
                echo "          </div>
          <div class=\"clear\"></div>
          <br/><br/>
        ";
            }
            // line 57
            echo "


        ";
            // line 60
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "isResa", array()) != 1)) {
                // line 61
                echo "          <div class=\"ficheContent row\">
            <div class=\"MessageNoResa\">";
                // line 62
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Note"), "html", null, true);
                echo " : ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ce bien n'est pas disponible"), "html", null, true);
                echo "</div>
          </div>
        ";
            }
            // line 65
            echo "


        <div class=\"ficheContent row\">
          ";
            // line 69
            if ((!(null === $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "tarif", array())))) {
                // line 70
                echo "            <div class=\"tarifAppart\">
              ";
                // line 71
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("A partir de"), "html", null, true);
                echo "
              ";
                // line 77
                echo "              <span class=\"justTarif\">";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->env->getExtension('my_twig_extension')->getFromTarif((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")))), "html", null, true);
                echo "
                ";
                // line 78
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("€/nuit"), "html", null, true);
                echo " *</span>
            </div>
            <small>* ";
                // line 80
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Légende tarif"), "html", null, true);
                echo "</small><br/>
            ";
                // line 81
                if ((null != $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "getActivePromotion", array(), "method"))) {
                    // line 82
                    echo "              <br>
              <span class=\"text-danger\">";
                    // line 83
                    echo ((($this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "getActivePromotion", array(), "method"), "percentage", array()) > 0)) ? ("Majoration de +") : ("Promotion de "));
                    echo "<strong>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "getActivePromotion", array(), "method"), "percentage", array()), "html", null, true);
                    echo "%</strong> si votre réservation débute avant le <strong>";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_date_modify_filter($this->env, twig_date_converter($this->env, "now"), (("+ " . $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "getActivePromotion", array(), "method"), "nbDaysBefore", array())) . " days")), "d/m/Y"), "html", null, true);
                    echo "</strong> </span>
              <br>
            ";
                }
                // line 86
                echo "            ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["future_promotions"]) ? $context["future_promotions"] : $this->getContext($context, "future_promotions")));
                foreach ($context['_seq'] as $context["_key"] => $context["promotion"]) {
                    // line 87
                    echo "              <br>
              <span class=\"text-danger\">
                ";
                    // line 89
                    echo ((($this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "percentage", array()) > 0)) ? ("Majoration de +") : ("Promotion de "));
                    echo "<strong>";
                    echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "percentage", array())), "html", null, true);
                    echo " %</strong> si votre réservation se situe entre<br>
                le <strong>";
                    // line 90
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "begin", array()), "d/m/Y"), "html", null, true);
                    echo "</strong> et le <strong>";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "end", array()), "d/m/Y"), "html", null, true);
                    echo "</strong>
              </span>
              <br>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['promotion'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 94
                echo "          ";
            }
            // line 95
            echo "          <br/>
          <div>
            ";
            // line 97
            echo nl2br(twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "descriptionFr", array()), "html", null, true));
            echo "
          </div>
        </div>

        <div class=\"ficheContent row\">
          <ul class=\"noStyle noMargin\">
            ";
            // line 103
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "type", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("type de bien"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "type", array())), "html", null, true);
                echo "</b></li>";
            }
            // line 104
            echo "            ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "surfaceSol", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Surface"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "surfaceSol", array()), "html", null, true);
                echo " m<sup>2</sup></b></li>";
            }
            // line 105
            echo "            ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbPersonne", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de personne maximum"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbPersonne", array()), "html", null, true);
                echo "</b></li>";
            }
            // line 106
            echo "            ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "heureArrivee", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Heure d'arrivée"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "heureArrivee", array())), "html", null, true);
                echo "</b></li>";
            }
            // line 107
            echo "            ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "heureDepart", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Heure de sortie"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "heureDepart", array())), "html", null, true);
                echo "</b> ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("au plus tard"), "html", null, true);
                echo "</li>";
            }
            // line 108
            echo "          </ul>
        </div>


        ";
            // line 113
            echo "        ";
            if ((($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : null), "ville", array(), "any", true, true) && (!(null === $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "ville", array())))) && (!(null === $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "ville", array()), "description", array()))))) {
                // line 114
                echo "          <div class=\"ficheContent row\">
            ";
                // line 115
                if ($this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "ville", array()), "quartier", array())) {
                    echo "<h3>";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Description du quartier"), "html", null, true);
                    echo "</h3>";
                } else {
                    echo "<h3>";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Description de la ville"), "html", null, true);
                    echo "</h3>";
                }
                // line 116
                echo "            ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "ville", array()), "description", array()), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 119
            echo "
        <div class=\"ficheContent row\">

          <div class=\"span3 noMargin\">
            <h3>";
            // line 123
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Localisation"), "html", null, true);
            echo "</h3>
            <ul class=\"noStyle noMargin\">
              ";
            // line 125
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceAeroport", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Distance aéroport"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceAeroport", array())), "html", null, true);
                echo " km</b></li>";
            }
            // line 126
            echo "              ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceAutoroute", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Distance autoroute"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceAutoroute", array())), "html", null, true);
                echo " km</b></li>";
            }
            // line 127
            echo "              ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceCommerce", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Distance commerces"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceCommerce", array())), "html", null, true);
                echo " km</b></li>";
            }
            // line 128
            echo "              ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesMetro", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accès métro"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesMetro", array())), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("min"), "html", null, true);
                echo "</b></li>";
            }
            // line 129
            echo "              ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesTram", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accès tram"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesTram", array())), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("min"), "html", null, true);
                echo "</b></li>";
            }
            // line 130
            echo "              ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesBus", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accès bus"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesBus", array())), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("min"), "html", null, true);
                echo "</b></li>";
            }
            // line 131
            echo "            </ul>
          </div>

          <div class=\"span3\">
            <h3>";
            // line 135
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Note"), "html", null, true);
            echo "</h3>
            <ul class=\"noStyle margin0\">
              ";
            // line 137
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "clair", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Clair"), "html", null, true);
                echo " : <span class=\"blocRight\"><b>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "clair", array()), "html", null, true);
                echo "/5</b></span></li>";
            }
            // line 138
            echo "              ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "calme", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Calme"), "html", null, true);
                echo " : <span class=\"blocRight\"><b>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "calme", array()), "html", null, true);
                echo "/5</b></span></li>";
            }
            // line 139
            echo "              ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "standingImmeuble", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Standing"), "html", null, true);
                echo " : <span class=\"blocRight\"><b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "standingImmeuble", array())), "html", null, true);
                echo "</b></span></li>";
            }
            // line 140
            echo "              ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "etatInterieur", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("État intérieur"), "html", null, true);
                echo " : <span class=\"blocRight\"><b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "etatInterieur", array())), "html", null, true);
                echo "</b></span></li>";
            }
            // line 141
            echo "            </ul>
          </div>
        </div>

        <div class=\"ficheContent row\">
          <h3>";
            // line 146
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Prestations"), "html", null, true);
            echo "</h3>
          <ul class=\"noStyle noMargin\">
            <li>";
            // line 148
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Situé au"), "html", null, true);
            echo " : ";
            echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->affEtageBien($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "etage", array())), "html", null, true);
            echo " ";
            if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbEtage", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("sur"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbEtage", array()), "html", null, true);
                echo " ";
            }
            echo "</li>
            ";
            // line 149
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "stationnement", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Stationnement"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "stationnement", array()), "html", null, true);
                echo "</b></li>";
            }
            // line 150
            echo "          </ul>
          <ul class=\"noStyle span3 noMargin\">
            <li>&nbsp;</li>
            ";
            // line 153
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "jardin", array()) != "")) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Jardin"), "html", null, true);
                echo " :   <b> ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("oui"), "html", null, true);
                echo " </b></li>";
            }
            // line 154
            echo "            ";
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "videOrdure", array()) != "")) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Vide ordure"), "html", null, true);
                echo " : <b> ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("oui"), "html", null, true);
                echo " </b></li>";
            }
            // line 155
            echo "            ";
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "digicode", array()) != "")) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Digicode"), "html", null, true);
                echo " :   <b> ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("oui"), "html", null, true);
                echo " </b></li>";
            }
            // line 156
            echo "            ";
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "climatisation", array()) != "")) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Climatisation"), "html", null, true);
                echo " :   <b> ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("oui"), "html", null, true);
                echo " </b></li>";
            }
            // line 157
            echo "            ";
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "animal", array()) != "")) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Animaux admis"), "html", null, true);
                echo " :   <b> ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("oui"), "html", null, true);
                echo " </b></li>";
            }
            // line 158
            echo "            ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "typeChauffage", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Type chauffage"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "typeChauffage", array())), "html", null, true);
                echo "</b></li>";
            }
            // line 159
            echo "            ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "modeChauffage", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mode de chauffage"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "modeChauffage", array())), "html", null, true);
                echo "</b></li>";
            }
            // line 160
            echo "            ";
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "ascenseur", array()) != "")) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ascenseur"), "html", null, true);
                echo " :   <b> ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("oui"), "html", null, true);
                echo " </b></li>";
            }
            // line 161
            echo "            ";
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "grenier", array()) != "")) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Grenier"), "html", null, true);
                echo " :   <b> ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("oui"), "html", null, true);
                echo " </b></li>";
            }
            // line 162
            echo "            ";
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "interphone", array()) != "")) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Interphone"), "html", null, true);
                echo " :   <b> ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("oui"), "html", null, true);
                echo " </b></li>";
            }
            // line 163
            echo "            ";
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesHandicapes", array()) != "")) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accès handicapés"), "html", null, true);
                echo " :   <b> ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("oui"), "html", null, true);
                echo " </b></li>";
            }
            // line 164
            echo "            ";
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "gardien", array()) != "")) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Gardien"), "html", null, true);
                echo " :   <b> ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("oui"), "html", null, true);
                echo " </b></li>";
            }
            // line 165
            echo "            ";
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "fumeur", array()) != "")) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fumeur"), "html", null, true);
                echo " :   <b> ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("oui"), "html", null, true);
                echo " </b></li>";
            }
            // line 166
            echo "            ";
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "internet", array()) != "")) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Internet"), "html", null, true);
                echo " : <b> ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("oui"), "html", null, true);
                echo " </b></li>";
            }
            // line 167
            echo "          </ul>
          <ul class=\"noStyle span3\">
            <li>&nbsp;</li>
            ";
            // line 170
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "parking", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de parkings"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "parking", array())), "html", null, true);
                echo "</b></li>";
            }
            // line 171
            echo "            ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "garage", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de garages"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "garage", array())), "html", null, true);
                echo "</b></li>";
            }
            // line 172
            echo "            ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "cave", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de caves"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "cave", array())), "html", null, true);
                echo "</b></li>";
            }
            // line 173
            echo "            ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "balcon", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de balcons"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "balcon", array())), "html", null, true);
                echo "</b></li>";
            }
            // line 174
            echo "            ";
            if ((!twig_test_empty($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "terrasse", array())))) {
                echo "<li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de terrasses"), "html", null, true);
                echo " : <b>";
                echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "terrasse", array())), "html", null, true);
                echo "</b></li>";
            }
            // line 175
            echo "            <li>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de lits simples"), "html", null, true);
            echo " : <b>";
            echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbLitSimple", array())), "html", null, true);
            echo "</b></li>
            <li>";
            // line 176
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de lits doubles"), "html", null, true);
            echo " : <b>";
            echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbLitDouble", array())), "html", null, true);
            echo "</b></li>
            <li>";
            // line 177
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de canapés convertibles"), "html", null, true);
            echo " : <b>";
            echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbCanapeConvertible", array())), "html", null, true);
            echo "</b></li>
            <li>";
            // line 178
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de double lits simples / lit double"), "html", null, true);
            echo " : <b>";
            echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbLitSimpleDouble", array())), "html", null, true);
            echo "</b></li>
          </ul>
        </div>

        ";
            // line 182
            if ((!twig_test_empty((isset($context["pieces"]) ? $context["pieces"] : $this->getContext($context, "pieces"))))) {
                // line 183
                echo "          <div class=\"ficheContent row\">
            <h3>";
                // line 184
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Description des pièces"), "html", null, true);
                echo "</h3>
            <ul class=\"noStyle noMargin\">
              ";
                // line 186
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["pieces"]) ? $context["pieces"] : $this->getContext($context, "pieces")));
                foreach ($context['_seq'] as $context["_key"] => $context["piece"]) {
                    // line 187
                    echo "                <li>
                  ";
                    // line 188
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["piece"]) ? $context["piece"] : $this->getContext($context, "piece")), "typepiece", array()), "type", array()), "html", null, true);
                    echo " :
                  <b>
                    ";
                    // line 190
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["piece"]) ? $context["piece"] : $this->getContext($context, "piece")), "equipement", array()));
                    $context['loop'] = array(
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    );
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["equipement"]) {
                        // line 191
                        echo "                      ";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["equipement"]) ? $context["equipement"] : $this->getContext($context, "equipement")), "intitule", array()), "html", null, true);
                        if ((!$this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "last", array()))) {
                            echo ", ";
                        }
                        // line 192
                        echo "                    ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['equipement'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 193
                    echo "                  </b>
                </li>
                <br/>
              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['piece'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 197
                echo "            </ul>
          </div>
        ";
            }
            // line 200
            echo "

        ";
            // line 202
            if ((!twig_test_empty((isset($context["documents"]) ? $context["documents"] : $this->getContext($context, "documents"))))) {
                // line 203
                echo "          <div class=\"ficheContent row\">
            <h3>";
                // line 204
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Documents"), "html", null, true);
                echo "</h3>
            <div class=\"row-fluid\">
              ";
                // line 206
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["documents"]) ? $context["documents"] : $this->getContext($context, "documents")));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
                    // line 207
                    echo "                ";
                    if ($this->getAttribute((isset($context["document"]) ? $context["document"] : $this->getContext($context, "document")), "file", array())) {
                        // line 208
                        echo "                  <div class=\"span3\" align=\"center\" style=\"padding:10px; word-break:break-all;\">
                    <img src=\"";
                        // line 209
                        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/bundles/patfront/images/img_doc.png"), "html", null, true);
                        echo "\" border=\"0\" /> <br />
                    <a href=\"";
                        // line 210
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_front_documents_filedownload", array("fileid" => $this->getAttribute((isset($context["document"]) ? $context["document"] : $this->getContext($context, "document")), "id", array()))), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["document"]) ? $context["document"] : $this->getContext($context, "document")), "documentName", array()), "html", null, true);
                        echo "</a>
                  </div>

                  ";
                        // line 213
                        if ((0 == $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index", array()) % 4)) {
                            // line 214
                            echo "                  </div><div class=\"row-fluid\">
                  ";
                        }
                        // line 216
                        echo "                ";
                    }
                    // line 217
                    echo "              ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 218
                echo "            </div>
          </div>
        ";
            }
            // line 221
            echo "


        ";
            // line 224
            if ((isset($context["calendrier"]) ? $context["calendrier"] : $this->getContext($context, "calendrier"))) {
                // line 225
                echo "          <div class=\"ficheContent row blocDispo\">
            <h3>";
                // line 226
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Disponibilités"), "html", null, true);
                echo "</h3>
            <div id=\"slider\">
              <ul>
                <div id=\"DetailCalendrier\">
                  ";
                // line 231
                echo "                    ";
                echo (isset($context["calendrier"]) ? $context["calendrier"] : $this->getContext($context, "calendrier"));
                echo "
                  ";
                // line 233
                echo "                </div>
              </ul>
            </div>

            <table id=\"legendCalendrier\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">
              <tr>
                <td><div style=\"width:20px;height:20px;background-color:#DD2525;\"></div></td>
                <td>";
                // line 240
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Réservé"), "html", null, true);
                echo "</td>
                <td width=\"30\"></td>
                <td><div style=\"width:20px;height:20px;background-color:#5BD835;\"></div></td>
                <td>";
                // line 243
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Disponible"), "html", null, true);
                echo "</td>
              </tr>
            </table>

          </div>
        ";
            }
            // line 249
            echo "

      </div><!-- /contenuCentrale -->

      <div id=\"contenuDroite\" class=\"span2\">


        ";
            // line 257
            echo "        <a id=\"resaGo\"></a>
        ";
            // line 258
            echo $this->env->getExtension('actions')->renderUri($this->env->getExtension('http_kernel')->controller("PatFrontBundle:Reservation:selectionDateResa", array("message" => (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "appartRef" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "reference", array()))), array());
            // line 259
            echo "        ";
            // line 280
            echo "        <div id=\"map_canvas\"></div>
        <div>
          <input id=\"address\" type=\"hidden\" value=\"";
            // line 282
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "adresse", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "ville", array()), "nom", array()), "html", null, true);
            echo "\">

        </div>

        <div class=\"rightBottomFiche\">
          ";
            // line 296
            echo "          <br/>
          ";
            // line 298
            echo "          ";
            // line 299
            echo "          ";
            // line 300
            echo "        </div>

        ";
            // line 328
            echo "



        <script>
          jQuery(document).ready(function (\$) {
            \$(\".royalSlider\").royalSlider({
              controlNavigation: 'thumbnails',
              autoScaleSlider: true,
              autoScaleSliderWidth: 960,
              autoScaleSliderHeight: 850,
              loop: false,
              imageScaleMode: 'fit-if-smaller',
              navigateByClick: true,
              numImagesToPreload: 2,
              arrowsNav: true,
              arrowsNavAutoHide: true,
              arrowsNavHideOnTouch: true,
              keyboardNavEnabled: true,
              fadeinLoadedSlide: true,
              globalCaption: true,
              globalCaptionInside: false,
              thumbs: {
                appendSpan: true,
                firstMargin: true,
                paddingBottom: 4
              },
              autoScaleSlider: true,
              fullscreen: {
                // fullscreen options go gere
                enabled: true,
                native: true
              }
            });
          });
        </script>


        <!-- API Google Maps Javascript V3 -->
        <!-- Note : Voir pour la cle = -->
        <script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?key=AIzaSyAn8ZF23BJFXwMHGzL-BGiZ0zEDS9mGfdo&sensor=false\"></script>
        <script type=\"text/javascript\">
          var geocoder;
          var map;
          function initialize() {
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(-34.397, 150.644);
            var myOptions = {
              zoom: 12,
              center: latlng,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById(\"map_canvas\"), myOptions);
          }

          function codeAddress() {
            var address = document.getElementById(\"address\").value;
            geocoder.geocode({'address': address}, function (results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                  map: map,
                  position: results[0].geometry.location
                });
              } else {
                //alert(\"Geocode was not successful for the following reason: \" + status);
                \$(\"#map_canvas\").hide();
              }
            });
          }

          initialize();
          codeAddress();
        </script>
        <!-- /API Google Maps Javascript V3 -->

      ";
        }
        // line 405
        echo "
    </div><!-- /contenuCentrale -->
  </div>
";
    }

    public function getTemplateName()
    {
        return "PatFrontBundle:Appartement:detail_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  991 => 405,  908 => 300,  906 => 299,  901 => 296,  887 => 280,  880 => 257,  862 => 243,  856 => 240,  830 => 224,  820 => 218,  806 => 217,  799 => 214,  789 => 210,  785 => 209,  779 => 207,  743 => 197,  720 => 192,  714 => 191,  697 => 190,  666 => 178,  660 => 177,  647 => 175,  638 => 174,  517 => 158,  850 => 388,  843 => 383,  836 => 379,  825 => 221,  823 => 373,  817 => 370,  808 => 365,  802 => 363,  777 => 350,  768 => 346,  757 => 204,  729 => 329,  726 => 328,  719 => 325,  713 => 321,  707 => 318,  704 => 317,  691 => 312,  684 => 309,  678 => 305,  672 => 302,  669 => 301,  659 => 297,  656 => 296,  642 => 288,  610 => 270,  575 => 257,  527 => 231,  511 => 225,  491 => 215,  476 => 209,  421 => 183,  415 => 180,  383 => 161,  736 => 438,  732 => 437,  685 => 186,  648 => 380,  568 => 253,  528 => 314,  524 => 313,  460 => 149,  425 => 140,  374 => 214,  324 => 123,  661 => 417,  636 => 374,  629 => 173,  618 => 365,  589 => 166,  587 => 382,  559 => 364,  547 => 237,  537 => 232,  530 => 345,  510 => 302,  453 => 256,  392 => 226,  369 => 208,  1843 => 1037,  1834 => 1030,  1747 => 947,  1742 => 944,  1728 => 936,  1718 => 929,  1706 => 920,  1694 => 915,  1692 => 914,  1683 => 908,  1677 => 905,  1668 => 899,  1660 => 894,  1656 => 893,  1652 => 892,  1645 => 888,  1641 => 887,  1630 => 882,  1626 => 881,  1622 => 880,  1614 => 874,  1601 => 832,  1595 => 829,  1586 => 823,  1582 => 822,  1576 => 819,  1567 => 813,  1563 => 812,  1557 => 809,  1547 => 802,  1543 => 801,  1537 => 798,  1528 => 792,  1518 => 788,  1500 => 773,  1496 => 772,  1492 => 771,  1488 => 770,  1482 => 767,  1473 => 761,  1469 => 760,  1465 => 759,  1461 => 758,  1448 => 750,  1438 => 748,  1436 => 747,  1432 => 746,  1428 => 745,  1422 => 742,  1413 => 736,  1409 => 735,  1403 => 732,  1389 => 724,  1370 => 711,  1364 => 708,  1351 => 701,  1345 => 698,  1332 => 691,  1326 => 688,  1313 => 681,  1307 => 678,  1294 => 671,  1288 => 668,  1275 => 661,  1269 => 658,  1256 => 651,  1250 => 648,  1237 => 641,  1231 => 638,  1218 => 631,  1212 => 628,  1199 => 621,  1193 => 618,  1180 => 611,  1155 => 598,  1142 => 591,  1136 => 588,  1127 => 582,  1117 => 578,  1108 => 572,  1098 => 568,  1079 => 558,  1071 => 552,  1067 => 550,  1065 => 549,  1054 => 541,  1050 => 540,  1044 => 537,  1035 => 531,  1031 => 530,  1025 => 527,  1016 => 521,  1006 => 517,  997 => 511,  993 => 510,  987 => 507,  978 => 501,  974 => 500,  959 => 491,  955 => 490,  949 => 487,  940 => 481,  936 => 480,  930 => 477,  921 => 471,  911 => 467,  902 => 461,  898 => 460,  892 => 457,  883 => 258,  879 => 450,  864 => 441,  854 => 437,  845 => 431,  835 => 226,  826 => 421,  816 => 417,  805 => 411,  801 => 410,  797 => 213,  791 => 406,  778 => 399,  772 => 348,  763 => 343,  759 => 389,  753 => 386,  744 => 380,  740 => 379,  721 => 369,  715 => 366,  687 => 350,  677 => 183,  668 => 340,  643 => 324,  624 => 314,  614 => 393,  605 => 304,  595 => 266,  538 => 267,  519 => 257,  485 => 212,  481 => 154,  447 => 218,  262 => 106,  281 => 118,  197 => 100,  419 => 185,  308 => 130,  260 => 105,  228 => 50,  355 => 128,  346 => 127,  288 => 113,  692 => 188,  603 => 170,  561 => 278,  515 => 319,  475 => 234,  379 => 162,  348 => 227,  298 => 114,  1174 => 608,  1167 => 718,  1161 => 601,  1154 => 710,  1149 => 707,  1141 => 704,  1134 => 702,  1131 => 701,  1129 => 700,  1114 => 695,  1111 => 694,  1109 => 693,  1105 => 692,  1096 => 689,  1092 => 688,  1089 => 562,  1072 => 675,  1064 => 670,  1042 => 651,  1034 => 645,  1026 => 642,  1022 => 640,  1019 => 639,  1017 => 638,  1012 => 520,  1008 => 635,  1004 => 633,  1002 => 632,  996 => 629,  988 => 626,  984 => 624,  979 => 623,  977 => 622,  968 => 497,  962 => 613,  951 => 605,  917 => 470,  915 => 585,  912 => 328,  905 => 580,  897 => 575,  893 => 574,  881 => 568,  873 => 447,  871 => 249,  863 => 555,  853 => 496,  842 => 231,  831 => 376,  812 => 464,  800 => 362,  798 => 455,  787 => 447,  754 => 203,  747 => 446,  739 => 333,  731 => 410,  725 => 370,  718 => 404,  696 => 356,  681 => 401,  675 => 182,  665 => 369,  663 => 298,  651 => 359,  628 => 344,  606 => 269,  602 => 268,  600 => 356,  579 => 258,  572 => 311,  555 => 303,  542 => 268,  532 => 264,  513 => 254,  501 => 329,  499 => 156,  473 => 153,  441 => 146,  438 => 257,  416 => 139,  330 => 139,  289 => 108,  633 => 320,  627 => 398,  621 => 289,  619 => 288,  594 => 353,  576 => 344,  570 => 254,  562 => 163,  558 => 335,  552 => 251,  540 => 350,  508 => 157,  498 => 314,  486 => 308,  480 => 215,  454 => 201,  450 => 196,  410 => 265,  325 => 161,  220 => 104,  407 => 138,  402 => 228,  377 => 130,  313 => 172,  232 => 103,  465 => 203,  457 => 192,  417 => 239,  411 => 179,  408 => 178,  396 => 256,  391 => 168,  388 => 131,  372 => 210,  344 => 163,  332 => 220,  293 => 163,  274 => 111,  231 => 99,  200 => 86,  792 => 418,  788 => 356,  782 => 208,  776 => 439,  774 => 349,  762 => 206,  748 => 200,  742 => 334,  734 => 193,  730 => 385,  724 => 327,  716 => 403,  706 => 360,  698 => 314,  694 => 313,  688 => 364,  680 => 184,  676 => 358,  662 => 350,  658 => 384,  652 => 411,  644 => 405,  640 => 375,  634 => 337,  622 => 274,  616 => 273,  608 => 323,  598 => 167,  580 => 165,  564 => 338,  545 => 353,  541 => 321,  526 => 159,  507 => 317,  488 => 321,  462 => 227,  433 => 226,  424 => 184,  395 => 223,  382 => 216,  376 => 162,  341 => 223,  327 => 230,  320 => 181,  310 => 154,  291 => 156,  278 => 107,  259 => 105,  244 => 127,  448 => 252,  443 => 194,  429 => 251,  406 => 177,  366 => 129,  318 => 119,  282 => 138,  258 => 178,  222 => 103,  120 => 44,  272 => 100,  266 => 121,  226 => 118,  178 => 81,  111 => 34,  393 => 169,  386 => 187,  380 => 184,  362 => 204,  358 => 207,  342 => 139,  340 => 162,  334 => 140,  326 => 138,  319 => 134,  314 => 204,  299 => 193,  265 => 144,  252 => 137,  237 => 94,  194 => 83,  132 => 53,  23 => 3,  97 => 29,  81 => 29,  53 => 12,  654 => 176,  637 => 295,  632 => 280,  625 => 343,  623 => 342,  617 => 195,  612 => 362,  604 => 357,  593 => 327,  591 => 186,  586 => 348,  583 => 259,  578 => 379,  571 => 164,  557 => 277,  534 => 242,  522 => 236,  520 => 340,  504 => 248,  494 => 216,  463 => 262,  446 => 148,  440 => 136,  434 => 141,  431 => 244,  427 => 182,  405 => 233,  401 => 175,  397 => 114,  389 => 220,  381 => 210,  357 => 150,  349 => 148,  339 => 142,  303 => 197,  295 => 113,  287 => 185,  268 => 123,  74 => 21,  470 => 398,  452 => 236,  444 => 250,  435 => 246,  430 => 187,  414 => 183,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 940,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 916,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 886,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 833,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 791,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 755,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 725,  1387 => 694,  1378 => 688,  1374 => 712,  1368 => 684,  1359 => 678,  1355 => 702,  1349 => 674,  1340 => 668,  1336 => 692,  1330 => 664,  1321 => 658,  1317 => 682,  1311 => 654,  1302 => 648,  1298 => 672,  1292 => 644,  1283 => 638,  1279 => 662,  1273 => 634,  1264 => 628,  1260 => 652,  1254 => 624,  1245 => 618,  1241 => 642,  1235 => 614,  1226 => 608,  1222 => 632,  1216 => 604,  1207 => 598,  1203 => 622,  1197 => 594,  1188 => 588,  1184 => 612,  1178 => 584,  1169 => 578,  1165 => 602,  1159 => 574,  1150 => 568,  1146 => 592,  1140 => 564,  1123 => 581,  1119 => 697,  1113 => 546,  1104 => 571,  1100 => 690,  1094 => 536,  1085 => 561,  1081 => 529,  1075 => 526,  1066 => 671,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 599,  933 => 450,  929 => 588,  923 => 446,  914 => 440,  910 => 439,  904 => 298,  895 => 430,  891 => 282,  885 => 259,  874 => 420,  870 => 419,  866 => 418,  860 => 440,  851 => 409,  847 => 233,  841 => 382,  832 => 225,  828 => 398,  822 => 420,  813 => 368,  809 => 388,  803 => 216,  794 => 359,  790 => 357,  784 => 354,  775 => 369,  771 => 368,  765 => 431,  756 => 359,  752 => 202,  746 => 355,  737 => 413,  733 => 330,  727 => 408,  712 => 420,  708 => 419,  702 => 416,  693 => 323,  689 => 187,  683 => 349,  674 => 241,  670 => 355,  664 => 339,  649 => 293,  645 => 296,  639 => 323,  630 => 371,  626 => 276,  620 => 172,  611 => 171,  607 => 279,  601 => 303,  592 => 262,  588 => 261,  582 => 347,  563 => 348,  554 => 344,  550 => 246,  544 => 161,  535 => 160,  531 => 236,  516 => 228,  512 => 318,  506 => 223,  493 => 294,  478 => 210,  468 => 150,  459 => 200,  455 => 209,  449 => 193,  436 => 191,  428 => 208,  422 => 234,  409 => 232,  403 => 261,  390 => 188,  384 => 165,  351 => 152,  337 => 126,  311 => 116,  296 => 146,  256 => 104,  241 => 53,  215 => 90,  207 => 79,  192 => 82,  186 => 74,  175 => 72,  153 => 74,  118 => 37,  61 => 19,  34 => 5,  65 => 20,  77 => 22,  37 => 6,  190 => 83,  161 => 87,  137 => 58,  126 => 52,  261 => 127,  255 => 130,  247 => 98,  242 => 112,  214 => 93,  211 => 80,  191 => 124,  157 => 53,  145 => 61,  127 => 43,  373 => 155,  367 => 154,  363 => 173,  359 => 172,  354 => 152,  343 => 143,  335 => 145,  328 => 152,  322 => 160,  315 => 175,  309 => 137,  305 => 115,  302 => 120,  290 => 185,  284 => 152,  279 => 139,  271 => 132,  264 => 185,  248 => 129,  236 => 106,  223 => 99,  170 => 82,  110 => 37,  96 => 34,  84 => 30,  472 => 210,  467 => 148,  375 => 211,  371 => 160,  360 => 183,  356 => 245,  353 => 149,  350 => 151,  338 => 145,  336 => 221,  331 => 169,  321 => 135,  316 => 157,  307 => 142,  304 => 121,  301 => 115,  297 => 131,  292 => 133,  286 => 129,  283 => 181,  277 => 147,  275 => 62,  270 => 143,  263 => 138,  257 => 146,  253 => 123,  249 => 90,  245 => 110,  233 => 112,  225 => 88,  216 => 91,  206 => 87,  202 => 85,  198 => 72,  185 => 81,  180 => 63,  177 => 121,  165 => 55,  150 => 48,  124 => 51,  113 => 38,  100 => 31,  58 => 16,  251 => 104,  234 => 97,  213 => 140,  195 => 84,  174 => 73,  167 => 71,  146 => 48,  140 => 43,  128 => 42,  104 => 35,  90 => 28,  83 => 24,  52 => 13,  596 => 225,  590 => 314,  585 => 221,  577 => 357,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 162,  551 => 239,  546 => 207,  543 => 235,  539 => 205,  529 => 322,  525 => 173,  523 => 230,  518 => 229,  514 => 192,  509 => 224,  503 => 330,  500 => 219,  497 => 295,  495 => 313,  492 => 323,  490 => 155,  487 => 213,  484 => 275,  482 => 177,  479 => 176,  477 => 271,  474 => 206,  471 => 207,  469 => 276,  466 => 228,  464 => 292,  461 => 169,  458 => 239,  456 => 199,  451 => 189,  445 => 160,  442 => 258,  439 => 248,  437 => 214,  432 => 191,  426 => 242,  423 => 186,  420 => 240,  418 => 204,  413 => 172,  399 => 137,  394 => 135,  378 => 162,  370 => 213,  368 => 203,  365 => 212,  361 => 151,  347 => 195,  345 => 194,  333 => 188,  329 => 125,  323 => 179,  317 => 223,  312 => 133,  306 => 170,  300 => 110,  294 => 116,  285 => 193,  280 => 180,  276 => 176,  267 => 109,  250 => 156,  239 => 115,  229 => 89,  218 => 94,  212 => 88,  210 => 101,  205 => 87,  188 => 94,  184 => 93,  181 => 80,  169 => 116,  160 => 57,  152 => 51,  148 => 62,  139 => 46,  134 => 54,  114 => 32,  107 => 48,  76 => 24,  70 => 22,  273 => 138,  269 => 106,  254 => 160,  246 => 128,  243 => 103,  240 => 126,  238 => 126,  235 => 124,  230 => 95,  227 => 94,  224 => 97,  221 => 118,  219 => 85,  217 => 144,  208 => 91,  204 => 87,  179 => 82,  159 => 68,  143 => 60,  135 => 45,  131 => 38,  108 => 36,  102 => 30,  71 => 19,  67 => 20,  63 => 16,  59 => 16,  47 => 13,  94 => 25,  89 => 28,  85 => 27,  79 => 22,  75 => 20,  68 => 12,  56 => 19,  50 => 14,  38 => 9,  29 => 3,  87 => 25,  72 => 23,  55 => 12,  21 => 2,  26 => 2,  35 => 5,  31 => 3,  41 => 6,  28 => 4,  201 => 76,  196 => 83,  183 => 67,  171 => 77,  166 => 81,  163 => 73,  156 => 65,  151 => 66,  142 => 44,  138 => 57,  136 => 44,  123 => 41,  121 => 38,  115 => 50,  105 => 47,  101 => 34,  91 => 33,  69 => 19,  66 => 16,  62 => 15,  49 => 15,  98 => 29,  93 => 29,  88 => 21,  78 => 17,  46 => 11,  44 => 12,  32 => 6,  27 => 1,  43 => 7,  40 => 11,  25 => 4,  24 => 2,  172 => 61,  158 => 114,  155 => 52,  129 => 44,  119 => 39,  117 => 47,  20 => 1,  22 => 2,  19 => 1,  209 => 89,  203 => 95,  199 => 93,  193 => 71,  189 => 70,  187 => 82,  182 => 80,  176 => 78,  173 => 59,  168 => 75,  164 => 70,  162 => 69,  154 => 50,  149 => 61,  147 => 53,  144 => 45,  141 => 50,  133 => 52,  130 => 52,  125 => 37,  122 => 39,  116 => 37,  112 => 49,  109 => 42,  106 => 28,  103 => 38,  99 => 43,  95 => 28,  92 => 20,  86 => 24,  82 => 20,  80 => 13,  73 => 27,  64 => 25,  60 => 15,  57 => 12,  54 => 11,  51 => 17,  48 => 11,  45 => 8,  42 => 7,  39 => 8,  36 => 7,  33 => 7,  30 => 4,);
    }
}
