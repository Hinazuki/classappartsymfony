<?php

/* PatCompteBundle:Admin:_planning_month.html.twig */
class __TwigTemplate_30cfcf1fb8425991544e7e14f76fcba65bb97ac6cf5099d36a69f191676c8ae3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table id=\"planning\">
  ";
        // line 2
        if ((twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "m") == 1)) {
            // line 3
            echo "    ";
            $context["prev_date"] = ("12-" . (twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "Y") - 1));
            // line 4
            echo "  ";
        } else {
            // line 5
            echo "    ";
            if (((twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "m") - 1) < 10)) {
                // line 6
                echo "      ";
                $context["prev_date"] = ((("0" . (twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "m") - 1)) . "-") . twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "Y"));
                // line 7
                echo "    ";
            } else {
                // line 8
                echo "      ";
                $context["prev_date"] = (((twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "m") - 1) . "-") . twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "Y"));
                // line 9
                echo "    ";
            }
            // line 10
            echo "  ";
        }
        // line 11
        echo "  ";
        if ((twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "m") == 12)) {
            // line 12
            echo "    ";
            $context["next_date"] = ("01-" . (twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "Y") + 1));
            // line 13
            echo "  ";
        } else {
            // line 14
            echo "    ";
            if (((twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "m") + 1) < 10)) {
                // line 15
                echo "      ";
                $context["next_date"] = ((("0" . (twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "m") + 1)) . "-") . twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "Y"));
                // line 16
                echo "    ";
            } else {
                // line 17
                echo "      ";
                $context["next_date"] = (((twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "m") + 1) . "-") . twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "Y"));
                // line 18
                echo "    ";
            }
            // line 19
            echo "  ";
        }
        // line 20
        echo "
  <tr>
    <th></th>
    <td colspan=\"3\" id=\"link-prev\" date=\"";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["prev_date"]) ? $context["prev_date"] : $this->getContext($context, "prev_date")), "html", null, true);
        echo "\"><</td>
    <td colspan=\"";
        // line 24
        echo twig_escape_filter($this->env, ((isset($context["nb_jours"]) ? $context["nb_jours"] : $this->getContext($context, "nb_jours")) - 6), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans(twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "F")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), "Y"), "html", null, true);
        echo "</td>
    <td colspan=\"3\" id=\"link-next\" date=\"";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["next_date"]) ? $context["next_date"] : $this->getContext($context, "next_date")), "html", null, true);
        echo "\">></td>
  </tr>
  <tr>
    <th>Référence</th>
      ";
        // line 29
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, (isset($context["nb_jours"]) ? $context["nb_jours"] : $this->getContext($context, "nb_jours"))));
        foreach ($context['_seq'] as $context["_key"] => $context["day"]) {
            // line 30
            echo "      <td>";
            echo twig_escape_filter($this->env, (isset($context["day"]) ? $context["day"] : $this->getContext($context, "day")), "html", null, true);
            echo "</td>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['day'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "  </tr>
  ";
        // line 33
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["planning"]) ? $context["planning"] : $this->getContext($context, "planning")));
        foreach ($context['_seq'] as $context["key"] => $context["p"]) {
            // line 34
            echo "    <tr>
      <th>";
            // line 35
            echo twig_escape_filter($this->env, (isset($context["key"]) ? $context["key"] : $this->getContext($context, "key")), "html", null, true);
            echo "</th>
        ";
            // line 36
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")));
            foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
                // line 37
                echo "        <td class=\"";
                echo twig_escape_filter($this->env, (isset($context["j"]) ? $context["j"] : $this->getContext($context, "j")), "html", null, true);
                echo "\">
          ";
                // line 38
                if ((((twig_in_filter("planning-resa-debut", (isset($context["j"]) ? $context["j"] : $this->getContext($context, "j"))) || twig_in_filter("planning-resa-fin", (isset($context["j"]) ? $context["j"] : $this->getContext($context, "j")))) || twig_in_filter("planning-bloc-debut", (isset($context["j"]) ? $context["j"] : $this->getContext($context, "j")))) || twig_in_filter("planning-bloc-fin", (isset($context["j"]) ? $context["j"] : $this->getContext($context, "j"))))) {
                    // line 39
                    echo "            <span>
              ";
                    // line 40
                    if (twig_in_filter("planning-bloc-menage", (isset($context["j"]) ? $context["j"] : $this->getContext($context, "j")))) {
                        // line 41
                        echo "                <img src=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/images/losange_small.png"), "html", null, true);
                        echo "\" alt=\"*\"/>
              ";
                    }
                    // line 43
                    echo "              ";
                    if (twig_in_filter("planning-resa-fin", (isset($context["j"]) ? $context["j"] : $this->getContext($context, "j")))) {
                        // line 44
                        echo "                <img src=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/images/icon_end-resa.png"), "html", null, true);
                        echo "\" alt=\"/\"/>
              ";
                    }
                    // line 46
                    echo "            </span>
          ";
                } elseif (twig_in_filter("planning-bloc-menage", (isset($context["j"]) ? $context["j"] : $this->getContext($context, "j")))) {
                    // line 47
                    echo "<img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/images/losange_small.png"), "html", null, true);
                    echo "\" alt=\"*\" style=\"margin: 0;\"/>
          ";
                }
                // line 49
                echo "        </td>
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "    </tr>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "</table>

<script type=\"text/javascript\">
  \$(\"#link-prev, #link-next\").on('click', function () {
    var date = \$(this).attr(\"date\");

    var request = \$.ajax({
      url: \"";
        // line 60
        echo $this->env->getExtension('routing')->getPath("pat_admin_planning_ajax");
        echo "\",
      type: \"POST\",
      data: {date: date},
      dataType: \"html\"
    });
    request.done(function (html) {
      \$(\"#planning-div\").html(html);
    });
    request.fail(function (jqXHR, textStatus) {
      alert(\"Erreur : \" + textStatus);
    });
  });
</script>";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:Admin:_planning_month.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 60,  161 => 47,  137 => 39,  126 => 36,  261 => 138,  255 => 135,  247 => 130,  242 => 127,  214 => 110,  211 => 109,  191 => 98,  157 => 46,  145 => 60,  127 => 49,  373 => 176,  367 => 173,  363 => 172,  359 => 171,  354 => 169,  343 => 161,  335 => 156,  328 => 152,  322 => 149,  315 => 144,  309 => 140,  305 => 138,  302 => 137,  290 => 133,  284 => 132,  279 => 129,  271 => 126,  264 => 122,  248 => 118,  236 => 110,  223 => 113,  170 => 74,  110 => 38,  96 => 32,  84 => 24,  472 => 149,  467 => 148,  375 => 146,  371 => 130,  360 => 99,  356 => 170,  353 => 99,  350 => 98,  338 => 101,  336 => 98,  331 => 95,  321 => 89,  316 => 87,  307 => 85,  304 => 84,  301 => 83,  297 => 70,  292 => 67,  286 => 65,  283 => 64,  277 => 71,  275 => 64,  270 => 61,  263 => 25,  257 => 121,  253 => 119,  249 => 21,  245 => 20,  233 => 23,  225 => 21,  216 => 111,  206 => 22,  202 => 91,  198 => 20,  185 => 13,  180 => 12,  177 => 11,  165 => 154,  150 => 63,  124 => 49,  113 => 83,  100 => 60,  58 => 30,  251 => 123,  234 => 112,  213 => 104,  195 => 93,  174 => 51,  167 => 49,  146 => 58,  140 => 40,  128 => 113,  104 => 35,  90 => 28,  83 => 24,  52 => 14,  596 => 225,  590 => 224,  585 => 221,  577 => 218,  573 => 216,  569 => 214,  560 => 212,  556 => 211,  553 => 210,  551 => 209,  546 => 207,  543 => 206,  539 => 205,  529 => 197,  525 => 195,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 185,  500 => 184,  497 => 183,  495 => 182,  492 => 181,  490 => 180,  487 => 179,  484 => 178,  482 => 177,  479 => 176,  477 => 175,  474 => 174,  471 => 173,  469 => 172,  466 => 171,  464 => 170,  461 => 169,  458 => 168,  456 => 167,  451 => 164,  445 => 160,  442 => 159,  439 => 158,  437 => 157,  432 => 154,  426 => 150,  423 => 149,  420 => 148,  418 => 147,  413 => 144,  399 => 143,  394 => 140,  378 => 137,  370 => 135,  368 => 129,  365 => 128,  361 => 131,  347 => 125,  345 => 94,  333 => 121,  329 => 94,  323 => 117,  317 => 116,  312 => 114,  306 => 113,  300 => 110,  294 => 68,  285 => 105,  280 => 103,  276 => 102,  267 => 60,  250 => 100,  239 => 95,  229 => 116,  218 => 82,  212 => 95,  210 => 23,  205 => 100,  188 => 89,  184 => 73,  181 => 53,  169 => 66,  160 => 59,  152 => 54,  148 => 43,  139 => 48,  134 => 54,  114 => 42,  107 => 33,  76 => 23,  70 => 33,  273 => 127,  269 => 94,  254 => 92,  246 => 117,  243 => 88,  240 => 19,  238 => 113,  235 => 94,  230 => 111,  227 => 81,  224 => 109,  221 => 20,  219 => 112,  217 => 75,  208 => 108,  204 => 72,  179 => 69,  159 => 61,  143 => 59,  135 => 38,  131 => 44,  108 => 36,  102 => 74,  71 => 21,  67 => 20,  63 => 16,  59 => 14,  47 => 9,  94 => 30,  89 => 20,  85 => 19,  79 => 24,  75 => 20,  68 => 14,  56 => 15,  50 => 10,  38 => 6,  29 => 3,  87 => 29,  72 => 19,  55 => 12,  21 => 2,  26 => 2,  35 => 5,  31 => 3,  41 => 7,  28 => 2,  201 => 92,  196 => 101,  183 => 81,  171 => 6,  166 => 61,  163 => 128,  156 => 66,  151 => 44,  142 => 41,  138 => 56,  136 => 56,  123 => 46,  121 => 46,  115 => 33,  105 => 40,  101 => 30,  91 => 27,  69 => 18,  66 => 17,  62 => 31,  49 => 27,  98 => 32,  93 => 9,  88 => 27,  78 => 22,  46 => 26,  44 => 12,  32 => 4,  27 => 4,  43 => 6,  40 => 11,  25 => 3,  24 => 3,  172 => 106,  158 => 67,  155 => 65,  129 => 119,  119 => 34,  117 => 43,  20 => 1,  22 => 2,  19 => 1,  209 => 82,  203 => 106,  199 => 71,  193 => 85,  189 => 84,  187 => 84,  182 => 70,  176 => 64,  173 => 68,  168 => 72,  164 => 71,  162 => 99,  154 => 58,  149 => 51,  147 => 119,  144 => 118,  141 => 117,  133 => 55,  130 => 37,  125 => 38,  122 => 35,  116 => 41,  112 => 32,  109 => 34,  106 => 33,  103 => 30,  99 => 29,  95 => 28,  92 => 25,  86 => 28,  82 => 22,  80 => 23,  73 => 23,  64 => 17,  60 => 15,  57 => 14,  54 => 13,  51 => 12,  48 => 11,  45 => 10,  42 => 9,  39 => 8,  36 => 7,  33 => 6,  30 => 5,);
    }
}
