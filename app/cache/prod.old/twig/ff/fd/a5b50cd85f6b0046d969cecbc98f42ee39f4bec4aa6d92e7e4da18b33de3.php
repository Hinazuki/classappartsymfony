<?php

/* PatCompteBundle:Tarif:editTaxes_content.html.twig */
class __TwigTemplate_fffda5b50cd85f6b0046d969cecbc98f42ee39f4bec4aa6d92e7e4da18b33de3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "  <div id=\"contenuCentrale1\">
    <h1>Gestion des taxes</h1>
    <div class=\"clear\"></div>

    ";
        // line 6
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 7
            echo "      <div class=\"alert alert-success\">
        ";
            // line 8
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
      </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "

    <form action=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("pat_admin_taxes_editer");
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo ">
      ";
        // line 14
        if ($this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors')) {
            // line 15
            echo "        <div class=\"alert alert-error\">
          ";
            // line 16
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
            echo "
        </div>
      ";
        }
        // line 19
        echo "
      <div class=\"blocForm\" style=\"margin-top: 20px;\">
        <div class=\"title\">
          Commission
        </div>


        <div class=\"content\">
          <div class=\"row-fluid\">
            <div class=\"span4 right\">
              ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commission", array()), "nuit", array()), 'label');
        echo "
            </div>
            <div class=\"span8 input-append\">
              ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commission", array()), "nuit", array()), 'widget');
        echo "<span class=\"add-on\">%</span>
              ";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commission", array()), "nuit", array()), 'errors');
        echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4 right\">
              ";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commission", array()), "semaine", array()), 'label');
        echo "
            </div>
            <div class=\"span8 input-append\">
              ";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commission", array()), "semaine", array()), 'widget');
        echo "<span class=\"add-on\">%</span>
              ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commission", array()), "semaine", array()), 'errors');
        echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4 right\">
              ";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commission", array()), "mois", array()), 'label');
        echo "
            </div>
            <div class=\"span8 input-append\">
              ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commission", array()), "mois", array()), 'widget');
        echo "<span class=\"add-on\">%</span>
              ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commission", array()), "mois", array()), 'errors');
        echo "
            </div>
          </div>
        </div>
      </div>

      <div class=\"blocForm\" style=\"margin-top: 20px;\">
        <div class=\"title\">
          Ménage
        </div>

        <div class=\"content\">
          <div class=\"row-fluid\">
            <div class=\"span1\">
              <strong>Studio</strong>
            </div>
            <div class=\"span1\">
              <strong>2 p.</strong>
            </div>
            <div class=\"span2 right\">
              < 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "studio", array()), "deuxpersonnes", array()), "inf15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "studio", array()), "deuxpersonnes", array()), "inf15j", array()), 'errors');
        echo "
            </div>
            <div class=\"span2 right\">
              > 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 81
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "studio", array()), "deuxpersonnes", array()), "sup15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "studio", array()), "deuxpersonnes", array()), "sup15j", array()), 'errors');
        echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span1 offset1\">
              <strong>4 p.</strong>
            </div>
            <div class=\"span2 right\">
              < 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "studio", array()), "quatrepersonnes", array()), "inf15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "studio", array()), "quatrepersonnes", array()), "inf15j", array()), 'errors');
        echo "
            </div>
            <div class=\"span2 right\">
              > 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 100
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "studio", array()), "quatrepersonnes", array()), "sup15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 101
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "studio", array()), "quatrepersonnes", array()), "sup15j", array()), 'errors');
        echo "
            </div>
          </div>
        </div>

        <div class=\"content\">
          <div class=\"row-fluid\">
            <div class=\"span1\">
              <strong>T2</strong>
            </div>
            <div class=\"span1\">
              <strong>2 p.</strong>
            </div>
            <div class=\"span2 right\">
              < 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 118
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T2", array()), "deuxpersonnes", array()), "inf15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 119
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T2", array()), "deuxpersonnes", array()), "inf15j", array()), 'errors');
        echo "
            </div>
            <div class=\"span2 right\">
              > 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 125
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T2", array()), "deuxpersonnes", array()), "sup15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 126
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T2", array()), "deuxpersonnes", array()), "sup15j", array()), 'errors');
        echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span1 offset1\">
              <strong>4 p.</strong>
            </div>
            <div class=\"span2 right\">
              < 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 137
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T2", array()), "quatrepersonnes", array()), "inf15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 138
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T2", array()), "quatrepersonnes", array()), "inf15j", array()), 'errors');
        echo "
            </div>
            <div class=\"span2 right\">
              > 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 144
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T2", array()), "quatrepersonnes", array()), "sup15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 145
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T2", array()), "quatrepersonnes", array()), "sup15j", array()), 'errors');
        echo "
            </div>
          </div>
        </div>

        <div class=\"content\">
          <div class=\"row-fluid\">
            <div class=\"span1\">
              <strong>T3</strong>
            </div>
            <div class=\"span1\">
              <strong>4 p.</strong>
            </div>
            <div class=\"span2 right\">
              < 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 162
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T3", array()), "quatrepersonnes", array()), "inf15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 163
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T3", array()), "quatrepersonnes", array()), "inf15j", array()), 'errors');
        echo "
            </div>
            <div class=\"span2 right\">
              > 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 169
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T3", array()), "quatrepersonnes", array()), "sup15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 170
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T3", array()), "quatrepersonnes", array()), "sup15j", array()), 'errors');
        echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span1 offset1\">
              <strong>6 p.</strong>
            </div>
            <div class=\"span2 right\">
              < 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 181
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T3", array()), "sixpersonnes", array()), "inf15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 182
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T3", array()), "sixpersonnes", array()), "inf15j", array()), 'errors');
        echo "
            </div>
            <div class=\"span2 right\">
              > 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 188
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T3", array()), "sixpersonnes", array()), "sup15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 189
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T3", array()), "sixpersonnes", array()), "sup15j", array()), 'errors');
        echo "
            </div>
          </div>
        </div>

        <div class=\"content\">
          <div class=\"row-fluid\">
            <div class=\"span1\">
              <strong>T4</strong>
            </div>
            <div class=\"span1\">
              <strong>6 p.</strong>
            </div>
            <div class=\"span2 right\">
              < 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 206
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T4", array()), "sixpersonnes", array()), "inf15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 207
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T4", array()), "sixpersonnes", array()), "inf15j", array()), 'errors');
        echo "
            </div>
            <div class=\"span2 right\">
              > 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 213
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T4", array()), "sixpersonnes", array()), "sup15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 214
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T4", array()), "sixpersonnes", array()), "sup15j", array()), 'errors');
        echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span1 offset1\">
              <strong>8 p.</strong>
            </div>
            <div class=\"span2 right\">
              < 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 225
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T4", array()), "huitpersonnes", array()), "inf15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 226
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T4", array()), "huitpersonnes", array()), "inf15j", array()), 'errors');
        echo "
            </div>
            <div class=\"span2 right\">
              > 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 232
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T4", array()), "huitpersonnes", array()), "sup15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 233
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T4", array()), "huitpersonnes", array()), "sup15j", array()), 'errors');
        echo "
            </div>
          </div>
        </div>

        <div class=\"content\">
          <div class=\"row-fluid\">
            <div class=\"span1\">
              <strong>T5</strong>
            </div>
            <div class=\"span1\">
              <strong>8 p.</strong>
            </div>
            <div class=\"span2 right\">
              < 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 250
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T5", array()), "huitpersonnes", array()), "inf15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 251
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T5", array()), "huitpersonnes", array()), "inf15j", array()), 'errors');
        echo "
            </div>
            <div class=\"span2 right\">
              > 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 257
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T5", array()), "huitpersonnes", array()), "sup15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 258
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T5", array()), "huitpersonnes", array()), "sup15j", array()), 'errors');
        echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span1 offset1\">
              <strong>10 p.</strong>
            </div>
            <div class=\"span2 right\">
              < 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 269
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T5", array()), "dixpersonnes", array()), "inf15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 270
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T5", array()), "dixpersonnes", array()), "inf15j", array()), 'errors');
        echo "
            </div>
            <div class=\"span2 right\">
              > 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 276
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T5", array()), "dixpersonnes", array()), "sup15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 277
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T5", array()), "dixpersonnes", array()), "sup15j", array()), 'errors');
        echo "
            </div>
          </div>
        </div>

        <div class=\"content\">
          <div class=\"row-fluid\">
            <div class=\"span1\">
              <strong>T6</strong>
            </div>
            <div class=\"span1\">
              <strong>10 p.</strong>
            </div>
            <div class=\"span2 right\">
              < 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 294
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T6", array()), "dixpersonnes", array()), "inf15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 295
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T6", array()), "dixpersonnes", array()), "inf15j", array()), 'errors');
        echo "
            </div>
            <div class=\"span2 right\">
              > 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 301
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T6", array()), "dixpersonnes", array()), "sup15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 302
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T6", array()), "dixpersonnes", array()), "sup15j", array()), 'errors');
        echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span1 offset1\">
              <strong>12 p.</strong>
            </div>
            <div class=\"span2 right\">
              < 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 313
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T6", array()), "douzepersonnes", array()), "inf15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 314
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T6", array()), "douzepersonnes", array()), "inf15j", array()), 'errors');
        echo "
            </div>
            <div class=\"span2 right\">
              > 15 jours
            </div>
            <div class=\"span3 input-append\">
              ";
        // line 320
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T6", array()), "douzepersonnes", array()), "sup15j", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
              ";
        // line 321
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "menage", array()), "T6", array()), "douzepersonnes", array()), "sup15j", array()), 'errors');
        echo "
            </div>
          </div>
        </div>
      </div>

      <div class=\"blocForm\" style=\"margin-top: 20px;\">
        <div class=\"title\">
          EDF
        </div>

        <div class=\"content\">
          <div class=\"row-fluid\">
            <div class=\"span4 right\">
              ";
        // line 335
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "studio", array()), 'label');
        echo "
            </div>
            <div class=\"span8 input-append\">
              ";
        // line 338
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "studio", array()), 'widget');
        echo "<span class=\"add-on\">€/nuit</span>
              ";
        // line 339
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "studio", array()), 'errors');
        echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4 right\">
              ";
        // line 344
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T2", array()), 'label');
        echo "
            </div>
            <div class=\"span8 input-append\">
              ";
        // line 347
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T2", array()), 'widget');
        echo "<span class=\"add-on\">€/nuit</span>
              ";
        // line 348
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T2", array()), 'errors');
        echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4 right\">
              ";
        // line 353
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T3", array()), 'label');
        echo "
            </div>
            <div class=\"span8 input-append\">
              ";
        // line 356
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T3", array()), 'widget');
        echo "<span class=\"add-on\">€/nuit</span>
              ";
        // line 357
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T3", array()), 'errors');
        echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4 right\">
              ";
        // line 362
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T4", array()), 'label');
        echo "
            </div>
            <div class=\"span8 input-append\">
              ";
        // line 365
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T4", array()), 'widget');
        echo "<span class=\"add-on\">€/nuit</span>
              ";
        // line 366
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T4", array()), 'errors');
        echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4 right\">
              ";
        // line 371
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T5", array()), 'label');
        echo "
            </div>
            <div class=\"span8 input-append\">
              ";
        // line 374
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T5", array()), 'widget');
        echo "<span class=\"add-on\">€/nuit</span>
              ";
        // line 375
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T5", array()), 'errors');
        echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4 right\">
              ";
        // line 380
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T6", array()), 'label');
        echo "
            </div>
            <div class=\"span8 input-append\">
              ";
        // line 383
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T6", array()), 'widget');
        echo "<span class=\"add-on\">€/nuit</span>
              ";
        // line 384
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "EDF", array()), "T6", array()), 'errors');
        echo "
            </div>
          </div>
        </div>
      </div>

      <div class=\"blocForm\" style=\"margin-top: 20px;\">
        <div class=\"title\">
          Assurance
        </div>

        <div class=\"content\">
          <div class=\"row-fluid\">
            <div class=\"span4 right\">
              ";
        // line 398
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "assurance", array()), 'label');
        echo "
            </div>
            <div class=\"span8 input-append\">
              ";
        // line 401
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "assurance", array()), 'widget');
        echo "<span class=\"add-on\">%</span>
              ";
        // line 402
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "assurance", array()), 'errors');
        echo "
            </div>
          </div>
        </div>
      </div>

      <div class=\"blocForm\" style=\"margin-top: 20px;\">
        <div class=\"title\">
          TVA
        </div>

        <div class=\"content\">
          <div class=\"row-fluid\">
            <div class=\"span4 right\">
              ";
        // line 416
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "tva", array()), 'label');
        echo "
            </div>
            <div class=\"span8 input-append\">
              ";
        // line 419
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "tva", array()), 'widget');
        echo "<span class=\"add-on\">%</span>
              ";
        // line 420
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "tva", array()), 'errors');
        echo "
            </div>
          </div>
        </div>
      </div>

      <div class=\"blocForm\" style=\"margin-top: 20px;\">
        <div class=\"title\">
          Taxe de séjour
        </div>

        <div class=\"content\">
          <div class=\"row-fluid\">
            <div class=\"span4 right\">
              Taxe de séjour
            </div>
            <div class=\"span8 input-append\">
              ";
        // line 437
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "sejour", array()), 'widget');
        echo "<span class=\"add-on\">€/nuit</span>
              ";
        // line 438
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "sejour", array()), 'errors');
        echo "
            </div>
          </div>
        </div>
      </div>
      <br/>

      <div class=\"row-fluid center\">
        <input class=\"btn btn-primary\" type=\"submit\" value=\"";
        // line 446
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enregistrer"), "html", null, true);
        echo "\"/>
      </div>
      ";
        // line 448
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
    </form>


  </div><!-- /contenuCentrale -->

";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:Tarif:editTaxes_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  736 => 438,  732 => 437,  685 => 402,  648 => 380,  568 => 339,  528 => 314,  524 => 313,  460 => 270,  425 => 250,  374 => 214,  324 => 182,  661 => 417,  636 => 374,  629 => 399,  618 => 365,  589 => 383,  587 => 382,  559 => 364,  547 => 354,  537 => 320,  530 => 345,  510 => 302,  453 => 256,  392 => 226,  369 => 208,  1843 => 1037,  1834 => 1030,  1747 => 947,  1742 => 944,  1728 => 936,  1718 => 929,  1706 => 920,  1694 => 915,  1692 => 914,  1683 => 908,  1677 => 905,  1668 => 899,  1660 => 894,  1656 => 893,  1652 => 892,  1645 => 888,  1641 => 887,  1630 => 882,  1626 => 881,  1622 => 880,  1614 => 874,  1601 => 832,  1595 => 829,  1586 => 823,  1582 => 822,  1576 => 819,  1567 => 813,  1563 => 812,  1557 => 809,  1547 => 802,  1543 => 801,  1537 => 798,  1528 => 792,  1518 => 788,  1500 => 773,  1496 => 772,  1492 => 771,  1488 => 770,  1482 => 767,  1473 => 761,  1469 => 760,  1465 => 759,  1461 => 758,  1448 => 750,  1438 => 748,  1436 => 747,  1432 => 746,  1428 => 745,  1422 => 742,  1413 => 736,  1409 => 735,  1403 => 732,  1389 => 724,  1370 => 711,  1364 => 708,  1351 => 701,  1345 => 698,  1332 => 691,  1326 => 688,  1313 => 681,  1307 => 678,  1294 => 671,  1288 => 668,  1275 => 661,  1269 => 658,  1256 => 651,  1250 => 648,  1237 => 641,  1231 => 638,  1218 => 631,  1212 => 628,  1199 => 621,  1193 => 618,  1180 => 611,  1155 => 598,  1142 => 591,  1136 => 588,  1127 => 582,  1117 => 578,  1108 => 572,  1098 => 568,  1079 => 558,  1071 => 552,  1067 => 550,  1065 => 549,  1054 => 541,  1050 => 540,  1044 => 537,  1035 => 531,  1031 => 530,  1025 => 527,  1016 => 521,  1006 => 517,  997 => 511,  993 => 510,  987 => 507,  978 => 501,  974 => 500,  959 => 491,  955 => 490,  949 => 487,  940 => 481,  936 => 480,  930 => 477,  921 => 471,  911 => 467,  902 => 461,  898 => 460,  892 => 457,  883 => 451,  879 => 450,  864 => 441,  854 => 437,  845 => 431,  835 => 427,  826 => 421,  816 => 417,  805 => 411,  801 => 410,  797 => 409,  791 => 406,  778 => 399,  772 => 396,  763 => 390,  759 => 389,  753 => 386,  744 => 380,  740 => 379,  721 => 369,  715 => 366,  687 => 350,  677 => 346,  668 => 340,  643 => 324,  624 => 314,  614 => 393,  605 => 304,  595 => 385,  538 => 267,  519 => 257,  485 => 238,  481 => 237,  447 => 218,  262 => 114,  281 => 118,  197 => 100,  419 => 185,  308 => 130,  260 => 137,  228 => 50,  355 => 183,  346 => 177,  288 => 141,  692 => 459,  603 => 377,  561 => 278,  515 => 319,  475 => 234,  379 => 162,  348 => 227,  298 => 132,  1174 => 608,  1167 => 718,  1161 => 601,  1154 => 710,  1149 => 707,  1141 => 704,  1134 => 702,  1131 => 701,  1129 => 700,  1114 => 695,  1111 => 694,  1109 => 693,  1105 => 692,  1096 => 689,  1092 => 688,  1089 => 562,  1072 => 675,  1064 => 670,  1042 => 651,  1034 => 645,  1026 => 642,  1022 => 640,  1019 => 639,  1017 => 638,  1012 => 520,  1008 => 635,  1004 => 633,  1002 => 632,  996 => 629,  988 => 626,  984 => 624,  979 => 623,  977 => 622,  968 => 497,  962 => 613,  951 => 605,  917 => 470,  915 => 585,  912 => 584,  905 => 580,  897 => 575,  893 => 574,  881 => 568,  873 => 447,  871 => 561,  863 => 555,  853 => 496,  842 => 488,  831 => 480,  812 => 464,  800 => 456,  798 => 455,  787 => 447,  754 => 423,  747 => 446,  739 => 414,  731 => 410,  725 => 370,  718 => 404,  696 => 356,  681 => 401,  675 => 398,  665 => 369,  663 => 368,  651 => 359,  628 => 344,  606 => 389,  602 => 332,  600 => 356,  579 => 315,  572 => 311,  555 => 303,  542 => 268,  532 => 264,  513 => 254,  501 => 329,  499 => 271,  473 => 277,  441 => 245,  438 => 257,  416 => 231,  330 => 143,  289 => 162,  633 => 320,  627 => 398,  621 => 289,  619 => 288,  594 => 353,  576 => 344,  570 => 284,  562 => 255,  558 => 335,  552 => 251,  540 => 350,  508 => 228,  498 => 314,  486 => 308,  480 => 215,  454 => 201,  450 => 200,  410 => 265,  325 => 161,  220 => 104,  407 => 237,  402 => 228,  377 => 245,  313 => 172,  232 => 103,  465 => 263,  457 => 192,  417 => 239,  411 => 176,  408 => 225,  396 => 256,  391 => 168,  388 => 225,  372 => 210,  344 => 163,  332 => 220,  293 => 163,  274 => 175,  231 => 99,  200 => 103,  792 => 418,  788 => 416,  782 => 400,  776 => 439,  774 => 411,  762 => 402,  748 => 394,  742 => 391,  734 => 376,  730 => 385,  724 => 382,  716 => 403,  706 => 360,  698 => 389,  694 => 367,  688 => 364,  680 => 359,  676 => 358,  662 => 350,  658 => 384,  652 => 411,  644 => 405,  640 => 375,  634 => 337,  622 => 366,  616 => 386,  608 => 323,  598 => 386,  580 => 288,  564 => 338,  545 => 353,  541 => 321,  526 => 237,  507 => 317,  488 => 321,  462 => 227,  433 => 226,  424 => 207,  395 => 223,  382 => 216,  376 => 162,  341 => 223,  327 => 230,  320 => 181,  310 => 154,  291 => 156,  278 => 136,  259 => 126,  244 => 127,  448 => 252,  443 => 217,  429 => 251,  406 => 174,  366 => 206,  318 => 208,  282 => 138,  258 => 178,  222 => 103,  120 => 44,  272 => 100,  266 => 121,  226 => 118,  178 => 81,  111 => 34,  393 => 255,  386 => 187,  380 => 184,  362 => 204,  358 => 207,  342 => 139,  340 => 162,  334 => 159,  326 => 180,  319 => 138,  314 => 204,  299 => 193,  265 => 144,  252 => 137,  237 => 104,  194 => 83,  132 => 43,  23 => 3,  97 => 33,  81 => 29,  53 => 12,  654 => 383,  637 => 295,  632 => 400,  625 => 343,  623 => 342,  617 => 195,  612 => 362,  604 => 357,  593 => 327,  591 => 186,  586 => 348,  583 => 380,  578 => 379,  571 => 178,  557 => 277,  534 => 242,  522 => 236,  520 => 340,  504 => 248,  494 => 324,  463 => 262,  446 => 186,  440 => 136,  434 => 134,  431 => 244,  427 => 182,  405 => 233,  401 => 232,  397 => 114,  389 => 220,  381 => 210,  357 => 206,  349 => 228,  339 => 239,  303 => 197,  295 => 119,  287 => 185,  268 => 123,  74 => 27,  470 => 398,  452 => 236,  444 => 250,  435 => 246,  430 => 397,  414 => 183,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 940,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 916,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 886,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 833,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 791,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 755,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 725,  1387 => 694,  1378 => 688,  1374 => 712,  1368 => 684,  1359 => 678,  1355 => 702,  1349 => 674,  1340 => 668,  1336 => 692,  1330 => 664,  1321 => 658,  1317 => 682,  1311 => 654,  1302 => 648,  1298 => 672,  1292 => 644,  1283 => 638,  1279 => 662,  1273 => 634,  1264 => 628,  1260 => 652,  1254 => 624,  1245 => 618,  1241 => 642,  1235 => 614,  1226 => 608,  1222 => 632,  1216 => 604,  1207 => 598,  1203 => 622,  1197 => 594,  1188 => 588,  1184 => 612,  1178 => 584,  1169 => 578,  1165 => 602,  1159 => 574,  1150 => 568,  1146 => 592,  1140 => 564,  1123 => 581,  1119 => 697,  1113 => 546,  1104 => 571,  1100 => 690,  1094 => 536,  1085 => 561,  1081 => 529,  1075 => 526,  1066 => 671,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 599,  933 => 450,  929 => 588,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 569,  874 => 420,  870 => 419,  866 => 418,  860 => 440,  851 => 409,  847 => 408,  841 => 430,  832 => 399,  828 => 398,  822 => 420,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 431,  756 => 359,  752 => 448,  746 => 355,  737 => 413,  733 => 348,  727 => 408,  712 => 420,  708 => 419,  702 => 416,  693 => 323,  689 => 322,  683 => 349,  674 => 241,  670 => 355,  664 => 339,  649 => 297,  645 => 296,  639 => 323,  630 => 371,  626 => 332,  620 => 313,  611 => 277,  607 => 279,  601 => 303,  592 => 267,  588 => 269,  582 => 347,  563 => 348,  554 => 344,  550 => 246,  544 => 246,  535 => 283,  531 => 236,  516 => 233,  512 => 318,  506 => 301,  493 => 294,  478 => 253,  468 => 209,  459 => 144,  455 => 209,  449 => 193,  436 => 192,  428 => 208,  422 => 234,  409 => 232,  403 => 261,  390 => 188,  384 => 165,  351 => 152,  337 => 189,  311 => 118,  296 => 146,  256 => 138,  241 => 53,  215 => 111,  207 => 137,  192 => 82,  186 => 74,  175 => 72,  153 => 74,  118 => 37,  61 => 19,  34 => 8,  65 => 20,  77 => 20,  37 => 10,  190 => 77,  161 => 87,  137 => 58,  126 => 65,  261 => 127,  255 => 130,  247 => 120,  242 => 112,  214 => 93,  211 => 90,  191 => 124,  157 => 75,  145 => 76,  127 => 51,  373 => 111,  367 => 248,  363 => 173,  359 => 172,  354 => 152,  343 => 167,  335 => 145,  328 => 152,  322 => 160,  315 => 175,  309 => 137,  305 => 115,  302 => 169,  290 => 185,  284 => 152,  279 => 139,  271 => 132,  264 => 185,  248 => 129,  236 => 106,  223 => 99,  170 => 82,  110 => 37,  96 => 34,  84 => 30,  472 => 210,  467 => 148,  375 => 211,  371 => 160,  360 => 183,  356 => 245,  353 => 232,  350 => 151,  338 => 145,  336 => 221,  331 => 169,  321 => 178,  316 => 157,  307 => 142,  304 => 167,  301 => 130,  297 => 131,  292 => 133,  286 => 129,  283 => 181,  277 => 147,  275 => 62,  270 => 143,  263 => 138,  257 => 146,  253 => 123,  249 => 90,  245 => 110,  233 => 112,  225 => 119,  216 => 91,  206 => 87,  202 => 85,  198 => 72,  185 => 85,  180 => 63,  177 => 121,  165 => 74,  150 => 48,  124 => 40,  113 => 38,  100 => 27,  58 => 14,  251 => 159,  234 => 125,  213 => 140,  195 => 84,  174 => 73,  167 => 73,  146 => 48,  140 => 43,  128 => 42,  104 => 37,  90 => 28,  83 => 24,  52 => 13,  596 => 225,  590 => 314,  585 => 221,  577 => 357,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 210,  551 => 274,  546 => 207,  543 => 206,  539 => 205,  529 => 322,  525 => 173,  523 => 258,  518 => 193,  514 => 192,  509 => 189,  503 => 330,  500 => 247,  497 => 295,  495 => 313,  492 => 323,  490 => 219,  487 => 213,  484 => 275,  482 => 177,  479 => 176,  477 => 271,  474 => 206,  471 => 173,  469 => 276,  466 => 228,  464 => 292,  461 => 169,  458 => 239,  456 => 269,  451 => 189,  445 => 160,  442 => 258,  439 => 248,  437 => 214,  432 => 191,  426 => 242,  423 => 186,  420 => 240,  418 => 204,  413 => 172,  399 => 194,  394 => 162,  378 => 162,  370 => 213,  368 => 203,  365 => 212,  361 => 207,  347 => 195,  345 => 194,  333 => 188,  329 => 214,  323 => 179,  317 => 223,  312 => 133,  306 => 170,  300 => 110,  294 => 189,  285 => 193,  280 => 180,  276 => 176,  267 => 172,  250 => 156,  239 => 115,  229 => 146,  218 => 94,  212 => 88,  210 => 101,  205 => 87,  188 => 94,  184 => 93,  181 => 122,  169 => 116,  160 => 57,  152 => 80,  148 => 65,  139 => 104,  134 => 54,  114 => 32,  107 => 33,  76 => 28,  70 => 22,  273 => 138,  269 => 145,  254 => 160,  246 => 128,  243 => 106,  240 => 126,  238 => 126,  235 => 124,  230 => 100,  227 => 106,  224 => 97,  221 => 118,  219 => 112,  217 => 144,  208 => 91,  204 => 87,  179 => 82,  159 => 68,  143 => 61,  135 => 41,  131 => 38,  108 => 33,  102 => 30,  71 => 19,  67 => 18,  63 => 16,  59 => 16,  47 => 9,  94 => 25,  89 => 29,  85 => 26,  79 => 22,  75 => 20,  68 => 12,  56 => 19,  50 => 12,  38 => 9,  29 => 6,  87 => 32,  72 => 23,  55 => 12,  21 => 2,  26 => 2,  35 => 8,  31 => 3,  41 => 6,  28 => 2,  201 => 101,  196 => 83,  183 => 73,  171 => 77,  166 => 81,  163 => 73,  156 => 68,  151 => 66,  142 => 44,  138 => 41,  136 => 44,  123 => 50,  121 => 38,  115 => 45,  105 => 41,  101 => 37,  91 => 33,  69 => 19,  66 => 29,  62 => 21,  49 => 15,  98 => 29,  93 => 33,  88 => 21,  78 => 17,  46 => 11,  44 => 13,  32 => 6,  27 => 1,  43 => 7,  40 => 11,  25 => 4,  24 => 2,  172 => 61,  158 => 114,  155 => 60,  129 => 53,  119 => 35,  117 => 47,  20 => 1,  22 => 2,  19 => 1,  209 => 100,  203 => 95,  199 => 93,  193 => 90,  189 => 122,  187 => 92,  182 => 80,  176 => 82,  173 => 69,  168 => 75,  164 => 68,  162 => 114,  154 => 50,  149 => 61,  147 => 53,  144 => 45,  141 => 50,  133 => 52,  130 => 52,  125 => 37,  122 => 39,  116 => 36,  112 => 38,  109 => 42,  106 => 28,  103 => 38,  99 => 38,  95 => 32,  92 => 20,  86 => 24,  82 => 18,  80 => 13,  73 => 27,  64 => 25,  60 => 15,  57 => 18,  54 => 13,  51 => 17,  48 => 11,  45 => 13,  42 => 10,  39 => 8,  36 => 7,  33 => 7,  30 => 4,);
    }
}
