<?php

/* PatFrontBundle:Appartement:rechercher.html.twig */
class __TwigTemplate_ff25a789fd6980c98543f742136ab89ec79a6e8cfbacb5a2b3eb2d7b88d75abb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"recherche\">
  <!--<div class=\"titrecontentGauche\">Votre recherche :</div>-->
  <form action=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("pat_front_appartement_index");
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formRecherche"]) ? $context["formRecherche"] : $this->getContext($context, "formRecherche")), 'enctype');
        echo " id=\"formRecherche\">
    ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formRecherche"]) ? $context["formRecherche"] : $this->getContext($context, "formRecherche")), 'errors');
        echo "

    <div class=\"headerRecherche\">
      <span>";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Trouver un logement"), "html", null, true);
        echo "</span>
    </div>
    <div class=\"etapesRecherche\">

      <div class=\"Etapes dates\">

        </br><!--<br/>-->
        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
          <tr>
            <td colspan=\"2\">
              ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRecherche"]) ? $context["formRecherche"] : $this->getContext($context, "formRecherche")), "datearrivee", array()), 'widget', array("attr" => array("class" => "champ-date cal_date_in", "placeholder" => $this->env->getExtension('translator')->trans("Arrivée"))));
        echo "
            </td>
          </tr>
          <tr>
            <td  colspan=\"2\" class=\"row\">
              ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRecherche"]) ? $context["formRecherche"] : $this->getContext($context, "formRecherche")), "datedepart", array()), 'widget', array("attr" => array("class" => "champ-date cal_date_out", "placeholder" => $this->env->getExtension('translator')->trans("Départ"))));
        echo "
            </td>
          </tr>
        </table>
      </div><!-- /Etapes -->

      ";
        // line 29
        echo "      ";
        // line 30
        echo "      ";
        // line 31
        echo "      ";
        // line 32
        echo "      ";
        // line 33
        echo "      ";
        // line 34
        echo "      ";
        // line 35
        echo "      ";
        // line 36
        echo "      ";
        // line 37
        echo "      ";
        // line 38
        echo "      ";
        // line 39
        echo "      ";
        // line 40
        echo "      ";
        // line 41
        echo "      ";
        // line 42
        echo "      ";
        // line 43
        echo "      ";
        // line 44
        echo "      ";
        // line 45
        echo "      ";
        // line 46
        echo "
      ";
        // line 48
        echo "      ";
        // line 49
        echo "      ";
        // line 50
        echo "      ";
        // line 51
        echo "      ";
        // line 52
        echo "      ";
        // line 53
        echo "      ";
        // line 54
        echo "      ";
        // line 55
        echo "      ";
        // line 56
        echo "      ";
        // line 57
        echo "      ";
        // line 58
        echo "      ";
        // line 60
        echo "      ";
        // line 61
        echo "      ";
        // line 62
        echo "      ";
        // line 63
        echo "
      ";
        // line 65
        echo "      ";
        // line 66
        echo "      ";
        // line 67
        echo "      ";
        // line 68
        echo "      ";
        // line 69
        echo "      ";
        // line 70
        echo "      ";
        // line 71
        echo "      ";
        // line 72
        echo "      ";
        // line 73
        echo "      ";
        // line 74
        echo "      ";
        // line 75
        echo "      ";
        // line 76
        echo "      ";
        // line 77
        echo "
      <div class=\"Etapes type\">
        ";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRecherche"]) ? $context["formRecherche"] : $this->getContext($context, "formRecherche")), "nbPersonnes", array()), 'widget', array("attr" => array("class" => "champ-nb-personnes", "placeholder" => "Nombre de personnes")));
        echo "
      </div>

      <div class=\"Etapes ref\">
        ";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRecherche"]) ? $context["formRecherche"] : $this->getContext($context, "formRecherche")), "reference", array()), 'widget', array("attr" => array("placeholder" => $this->env->getExtension('translator')->trans("Référence ?"))));
        echo "
      </div><!-- /Etapes -->

      <div class=\"Etapes center\">
        <input id=\"btLancerRecherche\" type=\"submit\" value=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rechercher"), "html", null, true);
        echo "\" class=\"btn btn-primary btn-large\" name=\"SendRecherche\" />
      </div>
      ";
        // line 90
        echo "    </div>\t<!-- /etapesRecherche -->
    ";
        // line 91
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formRecherche"]) ? $context["formRecherche"] : $this->getContext($context, "formRecherche")), "_token", array()), 'widget');
        echo "
  </form>

  <div id=\"recherche\">
    <div class=\"headerRecherche\">
      <span>";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rechercher par quartier"), "html", null, true);
        echo "</span>
    </div>
    <div class=\"etapesRecherche\">
      <img id=\"areaMap\" border=\"0\" src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patfront/images/arrond_montpellier_1.png"), "html", null, true);
        echo "\" usemap=\"#arrond_montpellier\" width=\"200\" height=\"174\"/>



      <map name=\"arrond_montpellier\">
        <area shape=\"poly\" coords=\"150,145,182,129,183,111,196,110,196,147,187,169,177,164,166,169,149,146\" alt=\"Port Marianne\" title=\"Port Marianne\" href=\"";
        // line 104
        echo $this->env->getExtension('routing')->getPath("pat_front_appartement_index_ville", array("idVille" => "37015"));
        echo "\">
        <area shape=\"poly\" coords=\"99,168,106,145,88,136,91,131,120,130,131,123,144,138,151,139,127,158,99,168\" alt=\"Gares\" title=\"Gares\" href=\"";
        // line 105
        echo $this->env->getExtension('routing')->getPath("pat_front_appartement_index_ville", array("idVille" => "37020"));
        echo "\">
        <area shape=\"poly\" coords=\"90,129,90,119,102,95,117,99,116,107,131,109,129,120,118,128,89,128\" alt=\"Comédie\" title=\"Comédie\" href=\"";
        // line 106
        echo $this->env->getExtension('routing')->getPath("pat_front_appartement_index_ville", array("idVille" => "37021"));
        echo "\">
        <area shape=\"poly\" coords=\"120,85,120,85,141,85,161,78,178,95,179,128,154,138,132,119,133,108,120,106,119,84,139,85\" alt=\"Antigone\" title=\"Antigone\" href=\"";
        // line 107
        echo $this->env->getExtension('routing')->getPath("pat_front_appartement_index_ville", array("idVille" => "37022"));
        echo "\">
        <area shape=\"poly\" coords=\"51,118,54,160,81,173,96,169,102,145,85,138,88,129,75,118,51,117\" alt=\"Gambetta\" title=\"Gambetta\" href=\"";
        // line 108
        echo $this->env->getExtension('routing')->getPath("pat_front_appartement_index_ville", array("idVille" => "37023"));
        echo "\">
        <area shape=\"poly\" coords=\"56,91,56,91,67,75,99,80,98,97,86,125,77,115,53,115,56,92\" alt=\"Centre Historique\" title=\"Centre Historique\" href=\"";
        // line 109
        echo $this->env->getExtension('routing')->getPath("pat_front_appartement_index_ville", array("idVille" => "37013"));
        echo "\">
        <area shape=\"poly\" coords=\"96,15,127,12,157,28,117,84,117,97,103,93,103,78,86,76,97,55,95,15\" alt=\"Beaux-Arts\" title=\"Beaux-Arts\" href=\"";
        // line 110
        echo $this->env->getExtension('routing')->getPath("pat_front_appartement_index_ville", array("idVille" => "37024"));
        echo "\">
        <area shape=\"poly\" coords=\"8,47,48,11,78,10,91,14,92,45,90,50,93,57,81,76,66,73,51,92,8,48\" alt=\"Boutonnet\" title=\"Boutonnet\" href=\"";
        // line 111
        echo $this->env->getExtension('routing')->getPath("pat_front_appartement_index_ville", array("idVille" => "37025"));
        echo "\">
        <area shape=\"poly\" coords=\"21,69,55,97,49,116,48,119,1,117,11,74,21,70\" alt=\"Les arceaux\" title=\"Les arceaux\" href=\"";
        // line 112
        echo $this->env->getExtension('routing')->getPath("pat_front_appartement_index_ville", array("idVille" => "37026"));
        echo "\">
      </map>
    </div>
  </div>
  ";
        // line 116
        if (($this->getAttribute((isset($context["attributes"]) ? $context["attributes"] : $this->getContext($context, "attributes")), "get", array(0 => "_route"), "method") == "PatFrontBundle_homepage")) {
            // line 117
            echo "    <div class=\"span3\">
      <img border=\"0\" src=\"";
            // line 118
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patfront/images/Visuels-ColonneDroite.jpg"), "html", null, true);
            echo "\">
      <em class=\"bandeauPhoto\">* ";
            // line 119
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Photos non contractuelles"), "html", null, true);
            echo "</em>
    </div>
  ";
        }
        // line 122
        echo "
</div>

<script type=\"text/javascript\">
  (function () {
    \$(\"#frontappartementrecherche_ville\").keyup(function () {
      var ville = \$(\"#frontappartementrecherche_ville\").val();
      var DATA = 'ville=' + ville;

      if (ville.length >= 3) {
        \$(\"#imgLoader\").css(\"visibility\", \"visible\");
        \$.ajax({
          type: \"POST\",
          url: \"";
        // line 135
        echo $this->env->getExtension('routing')->getPath("pat_front_contenu_ville");
        echo "\",
          data: DATA,
          cache: false,
          success: function (data) {
            \$('#resultats_recherche1').html(data);
            \$(\"#resultats_recherche1\").show();
            \$(\"#imgLoader\").css(\"visibility\", \"hidden\");
          },
          error: function (xhr, ajaxOptions, thrownError) {
            //alert(\"error : \"+xhr.status);
            \$(\"#imgLoader\").css(\"visibility\", \"hidden\");
          }
        });
        return false;
      }
    });

    // On interdit de taper des caracteres
    \$(\"#frontappartementrecherche_budgetmini\").keyup(function () {
      var info = \$(\"#frontappartementrecherche_budgetmini\").val();
      if (isNaN(info))
      {
        \$(\"#frontappartementrecherche_budgetmini\").val(\$(\"#frontappartementrecherche_budgetmini\").val().substr(0, \$(\"#frontappartementrecherche_budgetmini\").val().length - 1));
      }
    });

    // On interdit de taper des caracteres
    \$(\"#frontappartementrecherche_budgetmaxi\").keyup(function () {
      var info = \$(\"#frontappartementrecherche_budgetmaxi\").val();
      if (isNaN(info))
      {
        \$(\"#frontappartementrecherche_budgetmaxi\").val(\$(\"#frontappartementrecherche_budgetmaxi\").val().substr(0, \$(\"#frontappartementrecherche_budgetmaxi\").val().length - 1));
      }
    });

  })();
</script>
";
    }

    public function getTemplateName()
    {
        return "PatFrontBundle:Appartement:rechercher.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  991 => 405,  908 => 300,  906 => 299,  901 => 296,  887 => 280,  880 => 257,  862 => 243,  856 => 240,  830 => 224,  820 => 218,  806 => 217,  799 => 214,  789 => 210,  785 => 209,  779 => 207,  743 => 197,  720 => 192,  714 => 191,  697 => 190,  666 => 178,  660 => 177,  647 => 175,  638 => 174,  517 => 158,  850 => 388,  843 => 383,  836 => 379,  825 => 221,  823 => 373,  817 => 370,  808 => 365,  802 => 363,  777 => 350,  768 => 346,  757 => 204,  729 => 329,  726 => 328,  719 => 325,  713 => 321,  707 => 318,  704 => 317,  691 => 312,  684 => 309,  678 => 305,  672 => 302,  669 => 301,  659 => 297,  656 => 296,  642 => 288,  610 => 270,  575 => 257,  527 => 231,  511 => 225,  491 => 215,  476 => 209,  421 => 183,  415 => 180,  383 => 161,  736 => 438,  732 => 437,  685 => 186,  648 => 380,  568 => 253,  528 => 314,  524 => 313,  460 => 149,  425 => 140,  374 => 214,  324 => 123,  661 => 417,  636 => 374,  629 => 173,  618 => 365,  589 => 166,  587 => 382,  559 => 364,  547 => 237,  537 => 232,  530 => 345,  510 => 302,  453 => 256,  392 => 226,  369 => 208,  1843 => 1037,  1834 => 1030,  1747 => 947,  1742 => 944,  1728 => 936,  1718 => 929,  1706 => 920,  1694 => 915,  1692 => 914,  1683 => 908,  1677 => 905,  1668 => 899,  1660 => 894,  1656 => 893,  1652 => 892,  1645 => 888,  1641 => 887,  1630 => 882,  1626 => 881,  1622 => 880,  1614 => 874,  1601 => 832,  1595 => 829,  1586 => 823,  1582 => 822,  1576 => 819,  1567 => 813,  1563 => 812,  1557 => 809,  1547 => 802,  1543 => 801,  1537 => 798,  1528 => 792,  1518 => 788,  1500 => 773,  1496 => 772,  1492 => 771,  1488 => 770,  1482 => 767,  1473 => 761,  1469 => 760,  1465 => 759,  1461 => 758,  1448 => 750,  1438 => 748,  1436 => 747,  1432 => 746,  1428 => 745,  1422 => 742,  1413 => 736,  1409 => 735,  1403 => 732,  1389 => 724,  1370 => 711,  1364 => 708,  1351 => 701,  1345 => 698,  1332 => 691,  1326 => 688,  1313 => 681,  1307 => 678,  1294 => 671,  1288 => 668,  1275 => 661,  1269 => 658,  1256 => 651,  1250 => 648,  1237 => 641,  1231 => 638,  1218 => 631,  1212 => 628,  1199 => 621,  1193 => 618,  1180 => 611,  1155 => 598,  1142 => 591,  1136 => 588,  1127 => 582,  1117 => 578,  1108 => 572,  1098 => 568,  1079 => 558,  1071 => 552,  1067 => 550,  1065 => 549,  1054 => 541,  1050 => 540,  1044 => 537,  1035 => 531,  1031 => 530,  1025 => 527,  1016 => 521,  1006 => 517,  997 => 511,  993 => 510,  987 => 507,  978 => 501,  974 => 500,  959 => 491,  955 => 490,  949 => 487,  940 => 481,  936 => 480,  930 => 477,  921 => 471,  911 => 467,  902 => 461,  898 => 460,  892 => 457,  883 => 258,  879 => 450,  864 => 441,  854 => 437,  845 => 431,  835 => 226,  826 => 421,  816 => 417,  805 => 411,  801 => 410,  797 => 213,  791 => 406,  778 => 399,  772 => 348,  763 => 343,  759 => 389,  753 => 386,  744 => 380,  740 => 379,  721 => 369,  715 => 366,  687 => 350,  677 => 183,  668 => 340,  643 => 324,  624 => 314,  614 => 393,  605 => 304,  595 => 266,  538 => 267,  519 => 257,  485 => 212,  481 => 154,  447 => 218,  262 => 106,  281 => 118,  197 => 99,  419 => 185,  308 => 130,  260 => 105,  228 => 50,  355 => 128,  346 => 127,  288 => 113,  692 => 188,  603 => 170,  561 => 278,  515 => 319,  475 => 234,  379 => 162,  348 => 227,  298 => 114,  1174 => 608,  1167 => 718,  1161 => 601,  1154 => 710,  1149 => 707,  1141 => 704,  1134 => 702,  1131 => 701,  1129 => 700,  1114 => 695,  1111 => 694,  1109 => 693,  1105 => 692,  1096 => 689,  1092 => 688,  1089 => 562,  1072 => 675,  1064 => 670,  1042 => 651,  1034 => 645,  1026 => 642,  1022 => 640,  1019 => 639,  1017 => 638,  1012 => 520,  1008 => 635,  1004 => 633,  1002 => 632,  996 => 629,  988 => 626,  984 => 624,  979 => 623,  977 => 622,  968 => 497,  962 => 613,  951 => 605,  917 => 470,  915 => 585,  912 => 328,  905 => 580,  897 => 575,  893 => 574,  881 => 568,  873 => 447,  871 => 249,  863 => 555,  853 => 496,  842 => 231,  831 => 376,  812 => 464,  800 => 362,  798 => 455,  787 => 447,  754 => 203,  747 => 446,  739 => 333,  731 => 410,  725 => 370,  718 => 404,  696 => 356,  681 => 401,  675 => 182,  665 => 369,  663 => 298,  651 => 359,  628 => 344,  606 => 269,  602 => 268,  600 => 356,  579 => 258,  572 => 311,  555 => 303,  542 => 268,  532 => 264,  513 => 254,  501 => 329,  499 => 156,  473 => 153,  441 => 146,  438 => 257,  416 => 139,  330 => 139,  289 => 108,  633 => 320,  627 => 398,  621 => 289,  619 => 288,  594 => 353,  576 => 344,  570 => 254,  562 => 163,  558 => 335,  552 => 251,  540 => 350,  508 => 157,  498 => 314,  486 => 308,  480 => 215,  454 => 201,  450 => 196,  410 => 265,  325 => 161,  220 => 104,  407 => 138,  402 => 228,  377 => 130,  313 => 172,  232 => 103,  465 => 203,  457 => 192,  417 => 239,  411 => 179,  408 => 178,  396 => 256,  391 => 168,  388 => 131,  372 => 210,  344 => 163,  332 => 220,  293 => 163,  274 => 135,  231 => 99,  200 => 86,  792 => 418,  788 => 356,  782 => 208,  776 => 439,  774 => 349,  762 => 206,  748 => 200,  742 => 334,  734 => 193,  730 => 385,  724 => 327,  716 => 403,  706 => 360,  698 => 314,  694 => 313,  688 => 364,  680 => 184,  676 => 358,  662 => 350,  658 => 384,  652 => 411,  644 => 405,  640 => 375,  634 => 337,  622 => 274,  616 => 273,  608 => 323,  598 => 167,  580 => 165,  564 => 338,  545 => 353,  541 => 321,  526 => 159,  507 => 317,  488 => 321,  462 => 227,  433 => 226,  424 => 184,  395 => 223,  382 => 216,  376 => 162,  341 => 223,  327 => 230,  320 => 181,  310 => 154,  291 => 156,  278 => 107,  259 => 122,  244 => 116,  448 => 252,  443 => 194,  429 => 251,  406 => 177,  366 => 129,  318 => 119,  282 => 138,  258 => 178,  222 => 103,  120 => 57,  272 => 100,  266 => 121,  226 => 118,  178 => 81,  111 => 34,  393 => 169,  386 => 187,  380 => 184,  362 => 204,  358 => 207,  342 => 139,  340 => 162,  334 => 140,  326 => 138,  319 => 134,  314 => 204,  299 => 193,  265 => 144,  252 => 196,  237 => 112,  194 => 83,  132 => 53,  23 => 3,  97 => 45,  81 => 37,  53 => 12,  654 => 176,  637 => 295,  632 => 280,  625 => 343,  623 => 342,  617 => 195,  612 => 362,  604 => 357,  593 => 327,  591 => 186,  586 => 348,  583 => 259,  578 => 379,  571 => 164,  557 => 277,  534 => 242,  522 => 236,  520 => 340,  504 => 248,  494 => 216,  463 => 262,  446 => 148,  440 => 136,  434 => 141,  431 => 244,  427 => 182,  405 => 233,  401 => 175,  397 => 114,  389 => 220,  381 => 210,  357 => 150,  349 => 148,  339 => 142,  303 => 197,  295 => 113,  287 => 185,  268 => 123,  74 => 21,  470 => 398,  452 => 236,  444 => 250,  435 => 246,  430 => 187,  414 => 183,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 940,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 916,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 886,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 833,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 791,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 755,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 725,  1387 => 694,  1378 => 688,  1374 => 712,  1368 => 684,  1359 => 678,  1355 => 702,  1349 => 674,  1340 => 668,  1336 => 692,  1330 => 664,  1321 => 658,  1317 => 682,  1311 => 654,  1302 => 648,  1298 => 672,  1292 => 644,  1283 => 638,  1279 => 662,  1273 => 634,  1264 => 628,  1260 => 652,  1254 => 624,  1245 => 618,  1241 => 642,  1235 => 614,  1226 => 608,  1222 => 632,  1216 => 604,  1207 => 598,  1203 => 622,  1197 => 594,  1188 => 588,  1184 => 612,  1178 => 584,  1169 => 578,  1165 => 602,  1159 => 574,  1150 => 568,  1146 => 592,  1140 => 564,  1123 => 581,  1119 => 697,  1113 => 546,  1104 => 571,  1100 => 690,  1094 => 536,  1085 => 561,  1081 => 529,  1075 => 526,  1066 => 671,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 599,  933 => 450,  929 => 588,  923 => 446,  914 => 440,  910 => 439,  904 => 298,  895 => 430,  891 => 282,  885 => 259,  874 => 420,  870 => 419,  866 => 418,  860 => 440,  851 => 409,  847 => 233,  841 => 382,  832 => 225,  828 => 398,  822 => 420,  813 => 368,  809 => 388,  803 => 216,  794 => 359,  790 => 357,  784 => 354,  775 => 369,  771 => 368,  765 => 431,  756 => 359,  752 => 202,  746 => 355,  737 => 413,  733 => 330,  727 => 408,  712 => 420,  708 => 419,  702 => 416,  693 => 323,  689 => 187,  683 => 349,  674 => 241,  670 => 355,  664 => 339,  649 => 293,  645 => 296,  639 => 323,  630 => 371,  626 => 276,  620 => 172,  611 => 171,  607 => 279,  601 => 303,  592 => 262,  588 => 261,  582 => 347,  563 => 348,  554 => 344,  550 => 246,  544 => 161,  535 => 160,  531 => 236,  516 => 228,  512 => 318,  506 => 223,  493 => 294,  478 => 210,  468 => 150,  459 => 200,  455 => 209,  449 => 193,  436 => 191,  428 => 208,  422 => 234,  409 => 232,  403 => 261,  390 => 188,  384 => 165,  351 => 152,  337 => 126,  311 => 116,  296 => 146,  256 => 104,  241 => 53,  215 => 90,  207 => 79,  192 => 82,  186 => 74,  175 => 87,  153 => 75,  118 => 56,  61 => 19,  34 => 5,  65 => 29,  77 => 35,  37 => 8,  190 => 64,  161 => 79,  137 => 67,  126 => 61,  261 => 127,  255 => 197,  247 => 98,  242 => 112,  214 => 93,  211 => 174,  191 => 96,  157 => 77,  145 => 71,  127 => 43,  373 => 155,  367 => 154,  363 => 173,  359 => 172,  354 => 152,  343 => 143,  335 => 145,  328 => 152,  322 => 160,  315 => 175,  309 => 137,  305 => 115,  302 => 120,  290 => 185,  284 => 152,  279 => 139,  271 => 132,  264 => 185,  248 => 129,  236 => 106,  223 => 99,  170 => 57,  110 => 52,  96 => 34,  84 => 30,  472 => 210,  467 => 148,  375 => 211,  371 => 160,  360 => 183,  356 => 245,  353 => 149,  350 => 151,  338 => 145,  336 => 221,  331 => 169,  321 => 135,  316 => 157,  307 => 142,  304 => 121,  301 => 115,  297 => 131,  292 => 133,  286 => 129,  283 => 181,  277 => 147,  275 => 62,  270 => 143,  263 => 200,  257 => 146,  253 => 119,  249 => 118,  245 => 110,  233 => 111,  225 => 109,  216 => 91,  206 => 87,  202 => 85,  198 => 72,  185 => 81,  180 => 90,  177 => 121,  165 => 55,  150 => 48,  124 => 60,  113 => 38,  100 => 31,  58 => 16,  251 => 104,  234 => 97,  213 => 106,  195 => 84,  174 => 73,  167 => 71,  146 => 48,  140 => 43,  128 => 62,  104 => 49,  90 => 28,  83 => 38,  52 => 13,  596 => 225,  590 => 314,  585 => 221,  577 => 357,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 162,  551 => 239,  546 => 207,  543 => 235,  539 => 205,  529 => 322,  525 => 173,  523 => 230,  518 => 229,  514 => 192,  509 => 224,  503 => 330,  500 => 219,  497 => 295,  495 => 313,  492 => 323,  490 => 155,  487 => 213,  484 => 275,  482 => 177,  479 => 176,  477 => 271,  474 => 206,  471 => 207,  469 => 276,  466 => 228,  464 => 292,  461 => 169,  458 => 239,  456 => 199,  451 => 189,  445 => 160,  442 => 258,  439 => 248,  437 => 214,  432 => 191,  426 => 242,  423 => 186,  420 => 240,  418 => 204,  413 => 172,  399 => 137,  394 => 135,  378 => 162,  370 => 213,  368 => 203,  365 => 212,  361 => 151,  347 => 195,  345 => 194,  333 => 188,  329 => 125,  323 => 179,  317 => 223,  312 => 133,  306 => 170,  300 => 110,  294 => 116,  285 => 193,  280 => 180,  276 => 176,  267 => 109,  250 => 156,  239 => 115,  229 => 110,  218 => 94,  212 => 88,  210 => 101,  205 => 104,  188 => 94,  184 => 62,  181 => 61,  169 => 116,  160 => 57,  152 => 51,  148 => 62,  139 => 68,  134 => 54,  114 => 54,  107 => 31,  76 => 24,  70 => 22,  273 => 138,  269 => 106,  254 => 160,  246 => 117,  243 => 103,  240 => 192,  238 => 126,  235 => 124,  230 => 95,  227 => 94,  224 => 97,  221 => 108,  219 => 85,  217 => 107,  208 => 173,  204 => 87,  179 => 82,  159 => 68,  143 => 70,  135 => 66,  131 => 38,  108 => 51,  102 => 48,  71 => 32,  67 => 30,  63 => 16,  59 => 16,  47 => 5,  94 => 25,  89 => 41,  85 => 39,  79 => 36,  75 => 34,  68 => 12,  56 => 22,  50 => 6,  38 => 5,  29 => 4,  87 => 40,  72 => 19,  55 => 12,  21 => 2,  26 => 2,  35 => 7,  31 => 5,  41 => 6,  28 => 4,  201 => 76,  196 => 68,  183 => 91,  171 => 77,  166 => 51,  163 => 50,  156 => 65,  151 => 74,  142 => 45,  138 => 57,  136 => 44,  123 => 36,  121 => 38,  115 => 34,  105 => 47,  101 => 27,  91 => 42,  69 => 31,  66 => 16,  62 => 15,  49 => 15,  98 => 26,  93 => 43,  88 => 21,  78 => 21,  46 => 11,  44 => 12,  32 => 6,  27 => 1,  43 => 7,  40 => 11,  25 => 4,  24 => 2,  172 => 61,  158 => 114,  155 => 76,  129 => 44,  119 => 39,  117 => 47,  20 => 1,  22 => 2,  19 => 1,  209 => 105,  203 => 95,  199 => 93,  193 => 71,  189 => 70,  187 => 82,  182 => 80,  176 => 78,  173 => 59,  168 => 83,  164 => 70,  162 => 69,  154 => 50,  149 => 73,  147 => 72,  144 => 45,  141 => 69,  133 => 65,  130 => 63,  125 => 37,  122 => 58,  116 => 55,  112 => 53,  109 => 32,  106 => 50,  103 => 38,  99 => 46,  95 => 44,  92 => 25,  86 => 23,  82 => 20,  80 => 13,  73 => 33,  64 => 25,  60 => 14,  57 => 13,  54 => 11,  51 => 10,  48 => 17,  45 => 8,  42 => 7,  39 => 8,  36 => 7,  33 => 6,  30 => 4,);
    }
}
