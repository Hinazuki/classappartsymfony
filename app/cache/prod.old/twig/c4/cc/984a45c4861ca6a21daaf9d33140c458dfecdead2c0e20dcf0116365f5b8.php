<?php

/* PatCompteBundle:AdminAppartement:ajouter_content.html.twig */
class __TwigTemplate_c4cc984a45c4861ca6a21daaf9d33140c458dfecdead2c0e20dcf0116365f5b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
        // line 1120
        echo "

<script type=\"text/javascript\">

  \$(\"#check_proprio\").click(function () {

    var proprionom = \$(\"#proprio_nom\").val();
    var proprioprenom = \$(\"#proprio_prenom\").val();
    var propriotype = \$(\"#proprio_type\").val();
    var proprioreference = \$(\"#proprio_reference\").val();
    var DATA = 'infosproprio=' + proprionom + '-' + proprioprenom + '-' + propriotype + '-' + proprioreference;

    \$(\"#imgLoader\").css(\"visibility\", \"visible\");
    \$.ajax({
      type: \"POST\",
      url: \"";
        // line 1135
        echo $this->env->getExtension('routing')->getPath("pat_admin_appartement_select_proprietaire");
        echo "\",
      data: DATA,
      cache: false,
      success: function (data) {
        \$('#resultats_proprietaire').html(data);
        \$(\"#resultats_proprietaire\").show();
        //alert(\"ok\");
        \$(\"#imgLoader\").css(\"visibility\", \"hidden\");
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert(\"error : \" + xhr.status);
        \$(\"#imgLoader\").css(\"visibility\", \"hidden\");
      }
    });
    return false;
  });
</script>
";
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "
  <div id=\"contenuCentrale1\">
    <h1>";
        // line 4
        if ((isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))) {
            echo "Modifier un bien <small>(réf : ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "reference", array()), "html", null, true);
            echo ")</small>";
        } else {
            echo "Créer un bien";
        }
        echo "</h1>
    <div class=\"clear\"></div>

    ";
        // line 7
        if ((isset($context["validation"]) ? $context["validation"] : $this->getContext($context, "validation"))) {
            // line 8
            echo "      ";
            // line 9
            echo "        <p style=\"font-size:14px;\">";
            echo (isset($context["validation"]) ? $context["validation"] : $this->getContext($context, "validation"));
            echo "</p>
      ";
            // line 11
            echo "      <br/><br/>
      <a href=\"";
            // line 12
            echo $this->env->getExtension('routing')->getPath("pat_admin_appartement_index");
            echo "\" class=\"boutonJ3\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Revenir à la liste des biens"), "html", null, true);
            echo "</a>
    ";
        } else {
            // line 14
            echo "
      <div class=\"etapesBien\">
        <div class=\"linksEtapesBien\">
          ";
            // line 17
            if ((isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))) {
                // line 18
                echo "            <ul>
              <li><a class=\"active\" href=\"#\">Description</a></li>
              <li><a href=\"";
                // line 20
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_appartement_modif_proprietaire", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
                echo "\">Propriétaire</a></li>
              <li><a href=\"";
                // line 21
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_tarif_ajouter", array("id_bien" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
                echo "\">Loyers</a></li>
              <li><a href=\"";
                // line 22
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_promotion_index", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
                echo "\">Promotion / Majoration</a></li>
              <li><a href=\"";
                // line 23
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_piece_index", array("id" => (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")))), "html", null, true);
                echo "\">Pièces</a></li>
              <li><a href=\"";
                // line 24
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_media_ajouter", array("id" => (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")))), "html", null, true);
                echo "\">Images</a></li>
              <li><a href=\"";
                // line 25
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_calendrier_index", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
                echo "\">Disponibilités</a></li>
            </ul>
          ";
            } else {
                // line 28
                echo "            <ul>
              <li><a class=\"active\" href=\"#\">Description</a></li>
              <li><a href=\"#\">Propriétaire</a></li>
              <li><a href=\"#\">Loyers</a></li>
              <li><a href=\"#\">Pièces</a></li>
              <li><a href=\"#\">Images</a></li>
              <li><a href=\"#\">Promos/majorations</a></li>
              <li><a href=\"#\">Disponibilités</a></li>
            </ul>
          ";
            }
            // line 38
            echo "        </div>
      </div>

      ";
            // line 41
            if ((isset($context["message"]) ? $context["message"] : $this->getContext($context, "message"))) {
                // line 42
                echo "        <div class=\"alert alert-success\">
          ";
                // line 43
                echo nl2br(twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true));
                echo "
        </div>
      ";
            }
            // line 46
            echo "
      ";
            // line 47
            if (((isset($context["erreur"]) ? $context["erreur"] : $this->getContext($context, "erreur")) == 1)) {
                // line 48
                echo "
        <div class=\"error\">Vous devez accepter les conditions du mandat de commercialisation de Class Appart ®</div>

      ";
            }
            // line 52
            echo "

      <div class=\"formBienAdministration\">
        <h3>Administration :</h3>
        ";
            // line 56
            if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "createdBy", array())) {
                // line 57
                echo "          Création :
          ";
                // line 58
                if (($this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "createdBy", array()), "typeUtilisateur", array()) == 9)) {
                    // line 59
                    echo "            Administrateur
          ";
                } elseif (($this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "createdBy", array()), "typeUtilisateur", array()) == 2)) {
                    // line 61
                    echo "            Propriétaire
          ";
                }
                // line 63
                echo "          ";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "createdBy", array()), "prenom", array())), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "createdBy", array()), "Nom", array())), "html", null, true);
                echo " le ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "createdAt", array()), "d/m/Y H:i:s"), "html", null, true);
                echo "
          <br/>
        ";
            }
            // line 66
            echo "
        ";
            // line 67
            if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "updatedBy", array())) {
                // line 68
                echo "          Dernière modification :
          ";
                // line 69
                if (($this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "updatedBy", array()), "typeUtilisateur", array()) == 9)) {
                    // line 70
                    echo "            Administrateur
          ";
                } elseif (($this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "updatedBy", array()), "typeUtilisateur", array()) == 2)) {
                    // line 72
                    echo "            Propriétaire
          ";
                }
                // line 74
                echo "          ";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "updatedBy", array()), "prenom", array())), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "updatedBy", array()), "Nom", array())), "html", null, true);
                echo " le ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "updatedAt", array()), "d/m/Y H:i:s"), "html", null, true);
                echo "
          <br/>
        ";
            }
            // line 77
            echo "
        ";
            // line 78
            if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "utilisateur", array())) {
                // line 79
                echo "          Propriétaire : <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_proprietaire_afficher", array("id_proprio" => $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "utilisateur", array()), "id", array()))), "html", null, true);
                echo "\" target=\"_blank\">";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "utilisateur", array()), "prenom", array())), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "utilisateur", array()), "Nom", array())), "html", null, true);
                echo "</a>
        ";
            }
            // line 81
            echo "      </div>

      <div class=\"formBien\">
        <form action=\"\" method=\"post\" ";
            // line 84
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
            echo ">
          <div class=\"error\">
            ";
            // line 86
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
            echo "
          </div>

          <div class=\"blocForm active\">
            <div class=\"title\">Type de bien</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                <div class=\"span4\">
                  Changer le type du Bien
                </div>
                <div class=\"span8\">
                  ";
            // line 98
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "type", array()), 'errors');
            echo "
                  ";
            // line 99
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "type", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  Nombre de pièces du Bien
                </div>
                <div class=\"span8\">
                  ";
            // line 108
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_pieces", array()), 'errors');
            echo "
                  ";
            // line 109
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_pieces", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  <strong>";
            // line 115
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_meuble", array()), 'label');
            echo "</strong>
                </div>
                <div class=\"span8\">
                  ";
            // line 118
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_meuble", array()), 'errors');
            echo "
                  ";
            // line 119
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_meuble", array()), 'widget');
            echo "
                </div>
              </div>
            </div>
          </div>

          <div class=\"blocForm active\">
            <div class=\"title\">Description du bien</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                ";
            // line 130
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description_fr", array()), 'label');
            echo "<br/>
                ";
            // line 131
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description_fr", array()), 'errors');
            echo "
                ";
            // line 132
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description_fr", array()), 'widget', array("attr" => array("class" => "span12", "rows" => "5")));
            echo "
              </div>
              <div class=\"row-fluid\">
                ";
            // line 135
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description_courte_fr", array()), 'label');
            echo "<br/>
                ";
            // line 136
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description_courte_fr", array()), 'errors');
            echo "
                ";
            // line 137
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description_courte_fr", array()), 'widget', array("attr" => array("class" => "span12")));
            echo "
              </div>
            </div>
          </div>

          <div class=\"blocForm\">
            <div class=\"title\">Localisation*</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 148
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom_residence", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 151
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom_residence", array()), 'errors');
            echo "
                  ";
            // line 152
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom_residence", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 158
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 161
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'errors');
            echo "
                  ";
            // line 162
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 168
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 171
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2", array()), 'errors');
            echo "
                  ";
            // line 172
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 178
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 181
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'errors');
            echo "
                  ";
            // line 182
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'widget');
            echo "
                  <input type=\"hidden\" id=\"url_ville_search\" value=\"";
            // line 183
            echo $this->env->getExtension('routing')->getPath("pat_front_contenu_all_ville");
            echo "\">
                  <!-- affichage des résultas des villes ajax -->
                  <div id=\"resultats_recherche_villes_ajax\">
                    <div id=\"resultats_recherche_villes\"></div>
                  </div>
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 193
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_aeroport", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 196
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_aeroport", array()), 'errors');
            echo "
                  ";
            // line 197
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_aeroport", array()), 'widget');
            echo " km
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 203
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_autoroute", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 206
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_autoroute", array()), 'errors');
            echo "
                  ";
            // line 207
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_autoroute", array()), 'widget');
            echo " km
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 213
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_voie_rapide", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 216
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_voie_rapide", array()), 'errors');
            echo "
                  ";
            // line 217
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_voie_rapide", array()), 'widget');
            echo " km
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 223
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_commerce", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 226
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_commerce", array()), 'errors');
            echo "
                  ";
            // line 227
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_commerce", array()), 'widget');
            echo " km
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 233
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_gare", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 236
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_gare", array()), 'errors');
            echo "
                  ";
            // line 237
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_gare", array()), 'widget');
            echo " min
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 243
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_marche", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 246
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_marche", array()), 'errors');
            echo "
                  ";
            // line 247
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_marche", array()), 'widget');
            echo " min
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 253
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_bus", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 256
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_bus", array()), 'errors');
            echo "
                  ";
            // line 257
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_bus", array()), 'widget');
            echo " min
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 263
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_tram", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 266
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_tram", array()), 'errors');
            echo "
                  ";
            // line 267
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_tram", array()), 'widget');
            echo " min
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 273
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 276
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram", array()), 'errors');
            echo "
                  ";
            // line 277
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 283
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram2", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 286
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram2", array()), 'errors');
            echo "
                  ";
            // line 287
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram2", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 293
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram3", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 296
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram3", array()), 'errors');
            echo "
                  ";
            // line 297
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram3", array()), 'widget');
            echo "
                </div>
              </div>
            </div>
          </div>

          <div class=\"blocForm\">
            <div class=\"title\">Surface*</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 309
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_sol", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 312
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_sol", array()), 'errors');
            echo "
                  ";
            // line 313
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_sol", array()), 'widget');
            echo " m<sup>2</sup>
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 319
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_carrez", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 322
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_carrez", array()), 'errors');
            echo "
                  ";
            // line 323
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_carrez", array()), 'widget');
            echo " m<sup>2</sup> <i>(mesurage loi Carrez-DPE)</i>
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 329
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_terrain", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 332
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_terrain", array()), 'errors');
            echo "
                  ";
            // line 333
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_terrain", array()), 'widget');
            echo " m<sup>2</sup> environ
                </div>
              </div>
            </div>
          </div>

          <div class=\"blocForm\">
            <div class=\"title\">Prestations*</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 345
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "entree_immeuble", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 348
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "entree_immeuble", array()), 'errors');
            echo "
                  ";
            // line 349
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "entree_immeuble", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 355
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "serrure", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 358
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "serrure", array()), 'errors');
            echo "
                  ";
            // line 359
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "serrure", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 365
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_personne", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 368
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_personne", array()), 'errors');
            echo "
                  ";
            // line 369
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_personne", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 375
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_simple", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 378
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_simple", array()), 'errors');
            echo "
                  ";
            // line 379
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_simple", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 385
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_double", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 388
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_double", array()), 'errors');
            echo "
                  ";
            // line 389
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_double", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 395
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_simple_double", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 398
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_simple_double", array()), 'errors');
            echo "
                  ";
            // line 399
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_simple_double", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 405
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_canape_convertible", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 408
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_canape_convertible", array()), 'errors');
            echo "
                  ";
            // line 409
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_canape_convertible", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 415
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "etage", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 418
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "etage", array()), 'errors');
            echo "
                  ";
            // line 419
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_etage", array()), 'errors');
            echo "
                  ";
            // line 420
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "etage", array()), 'widget');
            echo "<br/>sur ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_etage", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 426
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cave", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 429
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cave", array()), 'errors');
            echo "
                  ";
            // line 430
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cave", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 436
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parking", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 439
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parking", array()), 'errors');
            echo "
                  ";
            // line 440
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parking", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 446
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "stationnement", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 449
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "stationnement", array()), 'errors');
            echo "
                  ";
            // line 450
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "stationnement", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 456
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "terrasse", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 459
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "terrasse", array()), 'errors');
            echo "
                  ";
            // line 460
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "terrasse", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 466
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "balcon", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 469
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "balcon", array()), 'errors');
            echo "
                  ";
            // line 470
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "balcon", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 476
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "garage", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 479
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "garage", array()), 'errors');
            echo "
                  ";
            // line 480
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "garage", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 486
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "escalier", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 489
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "escalier", array()), 'errors');
            echo "
                  ";
            // line 490
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "escalier", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 496
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ascenseur", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 499
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ascenseur", array()), 'errors');
            echo "
                  ";
            // line 500
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ascenseur", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 506
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "grenier", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 509
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "grenier", array()), 'errors');
            echo "
                  ";
            // line 510
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "grenier", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 516
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "jardin", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 519
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "jardin", array()), 'errors');
            echo "
                  ";
            // line 520
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "jardin", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 526
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vide_ordure", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 529
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vide_ordure", array()), 'errors');
            echo "
                  ";
            // line 530
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vide_ordure", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 536
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "interphone", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 539
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "interphone", array()), 'errors');
            echo "
                  ";
            // line 540
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "interphone", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 546
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "has_digicode", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 549
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "has_digicode", array()), 'errors');
            echo "
                  ";
            // line 550
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "has_digicode", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid has_digicod\">
                <div class=\"span4\">
                </div>
                <div class=\"span8\">
                  <small>Ces informations ne seront visibles que par nos services et ne seront pas diffusées sur internet</small>
                </div>
              </div>

              <div class=\"row-fluid has_digicod\">
                <div class=\"span4\">
                  ";
            // line 564
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "digicode", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 567
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "digicode", array()), 'errors');
            echo "
                  ";
            // line 568
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "digicode", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 574
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_handicapes", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 577
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_handicapes", array()), 'errors');
            echo "
                  ";
            // line 578
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_handicapes", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 584
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "climatisation", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 587
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "climatisation", array()), 'errors');
            echo "
                  ";
            // line 588
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "climatisation", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 594
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gardien", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 597
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gardien", array()), 'errors');
            echo "
                  ";
            // line 598
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gardien", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 604
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "animal", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 607
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "animal", array()), 'errors');
            echo "
                  ";
            // line 608
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "animal", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 614
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fumeur", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 617
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fumeur", array()), 'errors');
            echo "
                  ";
            // line 618
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fumeur", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 624
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "internet", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 627
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "internet", array()), 'errors');
            echo "
                  ";
            // line 628
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "internet", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 634
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "type_chauffage", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 637
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "type_chauffage", array()), 'errors');
            echo "
                  ";
            // line 638
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "type_chauffage", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 644
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mode_chauffage", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 647
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mode_chauffage", array()), 'errors');
            echo "
                  ";
            // line 648
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mode_chauffage", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 654
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "compteur_edf", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 657
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "compteur_edf", array()), 'errors');
            echo "
                  ";
            // line 658
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "compteur_edf", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 664
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "compteur_gdf", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 667
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "compteur_gdf", array()), 'errors');
            echo "
                  ";
            // line 668
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "compteur_gdf", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 674
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_compteurs", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 677
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_compteurs", array()), 'errors');
            echo "
                  ";
            // line 678
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_compteurs", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 684
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_compteur_eau", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 687
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_compteur_eau", array()), 'errors');
            echo "
                  ";
            // line 688
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_compteur_eau", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 694
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_boite_lettres", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 697
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_boite_lettres", array()), 'errors');
            echo "
                  ";
            // line 698
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_boite_lettres", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 704
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom_boite_lettres", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 707
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom_boite_lettres", array()), 'errors');
            echo "
                  ";
            // line 708
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom_boite_lettres", array()), 'widget');
            echo "
                </div>
              </div>
            </div>
          </div>

          <div class=\"blocForm\">
            <div class=\"title\">Diagnostic Performance Energétique*</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                <div class=\"span12\">
                  ";
            // line 720
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "statut_diagnostic", array()), 'errors');
            echo "
                  ";
            // line 721
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "statut_diagnostic", array()), 'widget');
            echo "
                </div>
              </div>

              <div id=\"tableDiagnostic\">
                <div class=\"row-fluid\">
                  <div class=\"span4\">
                    ";
            // line 728
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_diagnostic", array()), 'label');
            echo "
                  </div>
                  <div class=\"span8\">
                    ";
            // line 731
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_diagnostic", array()), 'errors');
            echo "
                    ";
            // line 732
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_diagnostic", array()), 'widget');
            echo " <i><small>(jj/mm/yyyy)</small></i>
                  </div>
                </div>

                <div class=\"row-fluid\">
                  <div class=\"span4\">
                    ";
            // line 738
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fichier_diagnostic", array()), 'label');
            echo "
                  </div>
                  <div class=\"span8\">
                    ";
            // line 741
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fichier_diagnostic", array()), 'errors');
            echo "
                    ";
            // line 742
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fichier_diagnostic", array()), 'widget');
            echo "
                    ";
            // line 743
            if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "fichierDiagnostic", array())) {
                // line 744
                echo "                      <br/><a id=\"linkFormulaire\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/photos/biens/"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "reference", array()), "html", null, true);
                echo "/diagnostic/";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "fichierDiagnostic", array()), "html", null, true);
                echo "\" target=\"_blank\">Télécharger votre DPE</a>
                    ";
            }
            // line 746
            echo "                  </div>
                </div>

                <div class=\"row-fluid\">
                  <div class=\"span4\">
                    ";
            // line 751
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "consommation_lettre", array()), 'label');
            echo "
                  </div>
                  <div class=\"span8\">
                    ";
            // line 754
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "consommation_lettre", array()), 'errors');
            echo "
                    ";
            // line 755
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "consommation_lettre", array()), 'widget');
            echo "
                    ";
            // line 756
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "consommation_chiffre", array()), 'errors');
            echo "
                    ";
            // line 757
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "consommation_chiffre", array()), 'widget');
            echo " kWh/m2/an
                  </div>
                </div>

                <div class=\"row-fluid\">
                  <div class=\"span4\">
                    ";
            // line 763
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gaz_serre_lettre", array()), 'label');
            echo "
                  </div>
                  <div class=\"span8\">
                    ";
            // line 766
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gaz_serre_lettre", array()), 'errors');
            echo "
                    ";
            // line 767
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gaz_serre_lettre", array()), 'widget');
            echo "
                    ";
            // line 768
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gaz_serre_chiffre", array()), 'errors');
            echo "
                    ";
            // line 769
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gaz_serre_chiffre", array()), 'widget');
            echo " kg/m2/an
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class=\"blocForm\">
            <div class=\"title\">Visites</div>

            <div class=\"content\">
              <small>Ces informations ne seront visibles que par nos services et ne seront pas diffusées sur internet</small>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 784
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_porte", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 787
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_porte", array()), 'errors');
            echo "
                  ";
            // line 788
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_porte", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 794
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_description", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 797
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_description", array()), 'errors');
            echo "
                  ";
            // line 798
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_description", array()), 'widget');
            echo "
                  <br/><i><small>Indiquez ici comment accéder à l’appartement ou la maison (position sur le palier, dans le lotissement) Vous pouvez également ajouter les informations utiles comme le téléphone du concierge ou celui du syndic.</small></i>
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 805
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_cle", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 808
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_cle", array()), 'errors');
            echo "
                  ";
            // line 809
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_cle", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 815
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_cave", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 818
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_cave", array()), 'errors');
            echo "
                  ";
            // line 819
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_cave", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 825
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_parking", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 828
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_parking", array()), 'errors');
            echo "
                  ";
            // line 829
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_parking", array()), 'widget');
            echo "
                </div>
              </div>
            </div>
          </div>

          <div class=\"blocForm\">
            <div class=\"title\">Horaires</div>

            <div class=\"content\">
              <p class=\"text-info\">
                <small>
                  - pour une arrivée un lundi, mardi, mercredi, jeudi, vendredi ou samedi – hors jours fériés - le Locataire pourra entrer dans les lieux entre 16 heures et 18 heures 30 ;<br/>
                  - pour une arrivée un dimanche ou un jour férié, le Locataire pourra entrer dans les lieux entre 18 heures et 19 heures 30.<br/>
                  Toute arrivée tardive - en dehors de ces horaires - entraînera la facturation par le Bailleur d’un honoraire de 50€ TTC payable sur place par le Locataire, correspondant aux frais découlant d’une telle arrivée tardive.<br/>
                  En tout état de cause et pour respecter la tranquillité du voisinage, aucune arrivée ne pourra être effectuée après 23 heures. Le cas échéant, l’arrivée sera reportée au lendemain, le Locataire restant redevable du loyer afférent à la première nuit.<br/>
                  Les départs devront être effectués le dernier jour de la location, avant 11 heures.
                </small>
              </p>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 851
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "heure_arrivee", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 854
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "heure_arrivee", array()), 'errors');
            echo "
                  ";
            // line 855
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "heure_arrivee", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 861
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "heure_depart", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 864
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "heure_depart", array()), 'errors');
            echo "
                  ";
            // line 865
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "heure_depart", array()), 'widget');
            echo "
                </div>
              </div>
            </div>
          </div>

          <div class=\"blocForm\">
            <div class=\"title\">Observations</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 877
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commentaire", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 880
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commentaire", array()), 'errors');
            echo "
                  ";
            // line 881
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commentaire", array()), 'widget', array("attr" => array("class" => "span12", "rows" => "5")));
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 887
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "confidentiel", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 890
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "confidentiel", array()), 'errors');
            echo "
                  ";
            // line 891
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "confidentiel", array()), 'widget', array("attr" => array("class" => "span12", "rows" => "5")));
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 897
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "coordonnees_syndic", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 900
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "coordonnees_syndic", array()), 'errors');
            echo "
                  ";
            // line 901
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "coordonnees_syndic", array()), 'widget', array("attr" => array("class" => "span12", "rows" => "5")));
            echo "
                </div>
              </div>
            </div>
          </div>

          <div class=\"blocForm\">
            <div class=\"title\">Autres*</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 913
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_labeliser", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 916
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_labeliser", array()), 'errors');
            echo "
                  ";
            // line 917
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_labeliser", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 923
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_reloger", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 926
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_reloger", array()), 'errors');
            echo "
                  ";
            // line 927
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_reloger", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 933
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "etat_interieur", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 936
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "etat_interieur", array()), 'errors');
            echo "
                  ";
            // line 937
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "etat_interieur", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 943
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "calme", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 946
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "calme", array()), 'errors');
            echo "
                  ";
            // line 947
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "calme", array()), 'widget');
            echo " / 5
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 953
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clair", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 956
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clair", array()), 'errors');
            echo "
                  ";
            // line 957
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clair", array()), 'widget');
            echo " / 5
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 963
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "standing_immeuble", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 966
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "standing_immeuble", array()), 'errors');
            echo "
                  ";
            // line 967
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "standing_immeuble", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 973
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "neuf_ancien", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 976
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "neuf_ancien", array()), 'errors');
            echo "
                  ";
            // line 977
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "neuf_ancien", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 983
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_resa", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 986
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_resa", array()), 'errors');
            echo "
                  ";
            // line 987
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_resa", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 993
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_selection", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 996
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_selection", array()), 'errors');
            echo "
                  ";
            // line 997
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_selection", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 1003
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "statut", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 1006
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "statut", array()), 'errors');
            echo "
                  ";
            // line 1007
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "statut", array()), 'widget');
            echo "
                </div>
              </div>
            </div>
          </div>

          ";
            // line 1013
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
            echo "

          <br/><br/>

          <div class=\"row-fluid\">
            <div class=\"span6\">
              <a href=\"";
            // line 1019
            echo $this->env->getExtension('routing')->getPath("pat_admin_dashboard");
            echo "\" class=\"btn btn-large\">Revenir</a>
            </div>
            <div class=\"span6 right\">
              <input class=\"btn btn-primary btn-large\" type=\"submit\" value=\"";
            // line 1022
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Continuer"), "html", null, true);
            echo "\"/>
            </div>
          </div>
        </form>
      </div>

      <!-- compte le nombre de caractères dans la version courte de la description-->
      <script type=\"text/javascript\" src=\"";
            // line 1029
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/js/charCount.js"), "html", null, true);
            echo "\"></script>
      <script type=\"text/javascript\">
        \$(document).ready(function () {
          \$(\"#appartement_description_courte_fr\").charCount({
            allowed: 200,
            exceeded: 0,
            counterText: 'Caractères restants : '
          });

          \$(\".has_digicod\").hide();
          if (\$(\"#appartement_has_digicode\").is(':checked'))
            \$(\".has_digicod\").show();

          \$(\"#appartement_has_digicode\").change(function () {
            if (\$(\"#appartement_has_digicode\").is(':checked'))
              \$(\".has_digicod\").show(500);
            else
              \$(\".has_digicod\").hide(500);
          });
        });

        /* Calcul du prix indicatif du séjour */
        function calculPrixSejourAppart(prix) {

          caracteresValides = \"0,123456789.\";
          for (i = 0; i < prix.length; i++) {
            if (caracteresValides.indexOf(prix.charAt(i)) == -1)
              return false;
          }

          prix = prix.replace(\",\", \".\");
          document.getElementById(\"prix_mois\").value = (prix * 1.18).toFixed(2).replace(\".\", \",\");
        }

        \$('#appartement_loyer').change(function () {
          calculPrixSejourAppart(document.getElementById(\"appartement_loyer\").value);
        });

        \$('#appartement_loyer').keyup(function () {
          calculPrixSejourAppart(document.getElementById(\"appartement_loyer\").value);
        });

        if (document.getElementById(\"appartement_loyer\") != null)
          calculPrixSejourAppart(document.getElementById(\"appartement_loyer\").value);


        /* Calcul du prix indicatif du séjour */
        function displayDiagnostic(valeur) {
          if (valeur == 1)
            document.getElementById(\"tableDiagnostic\").style.display = \"block\";
          else {
            document.getElementById(\"tableDiagnostic\").style.display = \"none\";
            document.getElementById(\"appartement_consommation_chiffre\").value = \"\";
            document.getElementById(\"appartement_gaz_serre_chiffre\").value = \"\";
            document.getElementById(\"appartement_date_diagnostic\").value = \"\";
          }
        }

        \$('#appartement_statut_diagnostic').change(function () {
          displayDiagnostic(document.getElementById(\"appartement_statut_diagnostic\").value);
        });

        displayDiagnostic(document.getElementById(\"appartement_statut_diagnostic\").value)


        /* Gestion de l'affichage des blocs pour la création d'un appart */
        ";
            // line 1113
            echo "
      </script>

    ";
        }
        // line 1116
        echo "<!-- end if validation -->
  </div><!-- /contenuCentrale1 -->

";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:AdminAppartement:ajouter_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 887,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 864,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 825,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 808,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 763,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 732,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 697,  1387 => 694,  1378 => 688,  1374 => 687,  1368 => 684,  1359 => 678,  1355 => 677,  1349 => 674,  1340 => 668,  1336 => 667,  1330 => 664,  1321 => 658,  1317 => 657,  1311 => 654,  1302 => 648,  1298 => 647,  1292 => 644,  1283 => 638,  1279 => 637,  1273 => 634,  1264 => 628,  1260 => 627,  1254 => 624,  1245 => 618,  1241 => 617,  1235 => 614,  1226 => 608,  1222 => 607,  1216 => 604,  1207 => 598,  1203 => 597,  1197 => 594,  1188 => 588,  1184 => 587,  1178 => 584,  1169 => 578,  1165 => 577,  1159 => 574,  1150 => 568,  1146 => 567,  1140 => 564,  1123 => 550,  1119 => 549,  1113 => 546,  1104 => 540,  1100 => 539,  1094 => 536,  1085 => 530,  1081 => 529,  1075 => 526,  1066 => 520,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 456,  933 => 450,  929 => 449,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 426,  874 => 420,  870 => 419,  866 => 418,  860 => 415,  851 => 409,  847 => 408,  841 => 405,  832 => 399,  828 => 398,  822 => 395,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 365,  756 => 359,  752 => 358,  746 => 355,  737 => 349,  733 => 348,  727 => 345,  712 => 333,  708 => 332,  702 => 329,  693 => 323,  689 => 322,  683 => 319,  674 => 313,  670 => 312,  664 => 309,  649 => 297,  645 => 296,  639 => 293,  630 => 287,  626 => 286,  620 => 283,  611 => 277,  607 => 276,  601 => 273,  592 => 267,  588 => 266,  582 => 263,  563 => 253,  554 => 247,  550 => 246,  544 => 243,  535 => 237,  531 => 236,  516 => 227,  512 => 226,  506 => 223,  493 => 216,  478 => 207,  468 => 203,  459 => 197,  455 => 196,  449 => 193,  436 => 183,  428 => 181,  422 => 178,  409 => 171,  403 => 168,  390 => 161,  384 => 158,  351 => 137,  337 => 132,  311 => 118,  296 => 109,  256 => 84,  241 => 79,  215 => 69,  207 => 66,  192 => 61,  186 => 58,  175 => 52,  153 => 41,  118 => 22,  61 => 1,  34 => 5,  65 => 17,  77 => 24,  37 => 8,  190 => 60,  161 => 47,  137 => 39,  126 => 24,  261 => 86,  255 => 135,  247 => 130,  242 => 127,  214 => 110,  211 => 109,  191 => 98,  157 => 46,  145 => 60,  127 => 49,  373 => 176,  367 => 173,  363 => 172,  359 => 171,  354 => 169,  343 => 135,  335 => 156,  328 => 152,  322 => 149,  315 => 119,  309 => 140,  305 => 115,  302 => 137,  290 => 133,  284 => 132,  279 => 129,  271 => 126,  264 => 122,  248 => 118,  236 => 77,  223 => 113,  170 => 74,  110 => 20,  96 => 32,  84 => 9,  472 => 149,  467 => 148,  375 => 152,  371 => 151,  360 => 99,  356 => 170,  353 => 99,  350 => 98,  338 => 101,  336 => 98,  331 => 95,  321 => 89,  316 => 87,  307 => 85,  304 => 84,  301 => 83,  297 => 70,  292 => 108,  286 => 65,  283 => 64,  277 => 71,  275 => 64,  270 => 61,  263 => 25,  257 => 121,  253 => 119,  249 => 21,  245 => 20,  233 => 23,  225 => 74,  216 => 111,  206 => 22,  202 => 91,  198 => 20,  185 => 13,  180 => 12,  177 => 11,  165 => 154,  150 => 63,  124 => 49,  113 => 83,  100 => 60,  58 => 30,  251 => 81,  234 => 112,  213 => 104,  195 => 93,  174 => 51,  167 => 47,  146 => 58,  140 => 40,  128 => 113,  104 => 17,  90 => 28,  83 => 24,  52 => 14,  596 => 225,  590 => 224,  585 => 221,  577 => 218,  573 => 257,  569 => 256,  560 => 212,  556 => 211,  553 => 210,  551 => 209,  546 => 207,  543 => 206,  539 => 205,  529 => 197,  525 => 233,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 185,  500 => 184,  497 => 217,  495 => 182,  492 => 181,  490 => 180,  487 => 213,  484 => 178,  482 => 177,  479 => 176,  477 => 175,  474 => 206,  471 => 173,  469 => 172,  466 => 171,  464 => 170,  461 => 169,  458 => 168,  456 => 167,  451 => 164,  445 => 160,  442 => 159,  439 => 158,  437 => 157,  432 => 182,  426 => 150,  423 => 149,  420 => 148,  418 => 147,  413 => 172,  399 => 143,  394 => 162,  378 => 137,  370 => 135,  368 => 129,  365 => 148,  361 => 131,  347 => 136,  345 => 94,  333 => 131,  329 => 130,  323 => 117,  317 => 116,  312 => 114,  306 => 113,  300 => 110,  294 => 68,  285 => 105,  280 => 99,  276 => 98,  267 => 60,  250 => 100,  239 => 78,  229 => 116,  218 => 82,  212 => 68,  210 => 67,  205 => 100,  188 => 59,  184 => 73,  181 => 56,  169 => 48,  160 => 59,  152 => 54,  148 => 38,  139 => 48,  134 => 54,  114 => 21,  107 => 33,  76 => 23,  70 => 20,  273 => 127,  269 => 94,  254 => 92,  246 => 117,  243 => 88,  240 => 19,  238 => 113,  235 => 94,  230 => 111,  227 => 81,  224 => 109,  221 => 72,  219 => 112,  217 => 70,  208 => 108,  204 => 72,  179 => 69,  159 => 61,  143 => 59,  135 => 38,  131 => 44,  108 => 36,  102 => 74,  71 => 21,  67 => 20,  63 => 16,  59 => 14,  47 => 12,  94 => 30,  89 => 11,  85 => 19,  79 => 24,  75 => 20,  68 => 4,  56 => 15,  50 => 10,  38 => 6,  29 => 3,  87 => 31,  72 => 22,  55 => 13,  21 => 2,  26 => 2,  35 => 5,  31 => 4,  41 => 7,  28 => 3,  201 => 92,  196 => 63,  183 => 57,  171 => 6,  166 => 61,  163 => 128,  156 => 66,  151 => 44,  142 => 41,  138 => 56,  136 => 28,  123 => 46,  121 => 46,  115 => 33,  105 => 40,  101 => 30,  91 => 27,  69 => 18,  66 => 17,  62 => 18,  49 => 17,  98 => 28,  93 => 9,  88 => 27,  78 => 22,  46 => 26,  44 => 12,  32 => 6,  27 => 4,  43 => 7,  40 => 8,  25 => 3,  24 => 3,  172 => 106,  158 => 43,  155 => 42,  129 => 119,  119 => 34,  117 => 43,  20 => 1,  22 => 1120,  19 => 1,  209 => 82,  203 => 106,  199 => 71,  193 => 85,  189 => 84,  187 => 84,  182 => 70,  176 => 64,  173 => 68,  168 => 72,  164 => 46,  162 => 99,  154 => 58,  149 => 51,  147 => 119,  144 => 118,  141 => 117,  133 => 55,  130 => 25,  125 => 38,  122 => 23,  116 => 41,  112 => 32,  109 => 34,  106 => 18,  103 => 30,  99 => 14,  95 => 33,  92 => 12,  86 => 28,  82 => 8,  80 => 7,  73 => 23,  64 => 2,  60 => 15,  57 => 14,  54 => 14,  51 => 13,  48 => 9,  45 => 10,  42 => 13,  39 => 1135,  36 => 7,  33 => 7,  30 => 6,);
    }
}
