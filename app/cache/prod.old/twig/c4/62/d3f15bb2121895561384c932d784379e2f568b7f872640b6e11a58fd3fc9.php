<?php

/* PatCompteBundle:AdminLocataire:listeReservation_content.html.twig */
class __TwigTemplate_c462d3f15bb2121895561384c932d784379e2f568b7f872640b6e11a58fd3fc9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "
  <div id=\"contenuCentrale1\">
    <h1>Réservations <small>(";
        // line 4
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["locataire"]) ? $context["locataire"] : $this->getContext($context, "locataire")), "prenom", array())), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["locataire"]) ? $context["locataire"] : $this->getContext($context, "locataire")), "nom", array())), "html", null, true);
        echo ")</small></h1>

    <div class=\"clear\"></div>

    <div class=\"etapesBien\">
      <div class=\"linksEtapesBien\">
        <ul>
          <li ><a href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_locataire_afficher", array("id_locataire" => $this->getAttribute((isset($context["locataire"]) ? $context["locataire"] : $this->getContext($context, "locataire")), "id", array()))), "html", null, true);
        echo "\">Profil</a></li>
          <li><a class=\"active\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_locataire_reservation_index", array("id_locataire" => $this->getAttribute((isset($context["locataire"]) ? $context["locataire"] : $this->getContext($context, "locataire")), "id", array()))), "html", null, true);
        echo "\">Réservations</a></li>
          <li><a href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_list_payment_by_member", array("member_id" => $this->getAttribute((isset($context["locataire"]) ? $context["locataire"] : $this->getContext($context, "locataire")), "id", array()))), "html", null, true);
        echo "\">Paiements</a></li>
        </ul>
      </div>
    </div>

    ";
        // line 18
        if ((isset($context["reservations"]) ? $context["reservations"] : $this->getContext($context, "reservations"))) {
            // line 19
            echo "      ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["reservations"]) ? $context["reservations"] : $this->getContext($context, "reservations")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["resa"]) {
                // line 20
                echo "        <div class=\"ListeAppart row-fluid\">
          <div class=\"span5 ListeAppart1\" style=\"min-height: 80px;\">
            ";
                // line 22
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "prenom", array())), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "nom", array())), "html", null, true);
                echo " (";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "username", array()), "html", null, true);
                echo ")<br/>
            ";
                // line 23
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "type", array()), "html", null, true);
                if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "nbPieces", array())) {
                    echo " T";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "nbPieces", array()), "html", null, true);
                }
                echo " (Réf : ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "reference", array()), "html", null, true);
                echo ")<br/>
            Réservation ";
                // line 24
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "reference", array()), "html", null, true);
                echo "
          </div>

          <div class=\"span2 ListeAppart1\" style=\"margin-left: 0; min-height: 80px;\">
            Réservé le ";
                // line 28
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "createdAt", array()), "d/m/Y"), "html", null, true);
                echo "
          </div>

          <div class=\"span3 ListeAppart1 center\" style=\"margin-left: 0; min-height: 80px;\">
            Du ";
                // line 32
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "dateDebut", array()), "d/m/Y"), "html", null, true);
                echo "<br/>
            au ";
                // line 33
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "dateFin", array()), "d/m/Y"), "html", null, true);
                echo "
          </div>

          <div class=\"span2 ListeAppart2\">
            ";
                // line 37
                $this->env->loadTemplate("PatCompteBundle:Reservation:_resa_status.html.twig")->display($context);
                // line 38
                echo "
            <br/><br/>
            <a href=\"";
                // line 40
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_show", array("id_reservation" => $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "id", array()))), "html", null, true);
                echo "\"><i class=\"icon-search\"></i></a>&nbsp;&nbsp;&nbsp;
            <a href=\"";
                // line 41
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_modifier", array("id_reservation" => $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "id", array()))), "html", null, true);
                echo "\"><i class=\"icon-edit\"></i></a>&nbsp;&nbsp;&nbsp;
              ";
                // line 45
                echo "          </div>
        </div>
      ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['resa'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "    ";
        } else {
            // line 49
            echo "      <br/><br/>
      <div class=\"alert\">Il n'y a aucune réservation actuellement.</div>
    ";
        }
        // line 52
        echo "  </div><!-- /contenuCentrale1 -->

";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:AdminLocataire:listeReservation_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  792 => 418,  788 => 416,  782 => 414,  776 => 412,  774 => 411,  762 => 402,  748 => 394,  742 => 391,  734 => 386,  730 => 385,  724 => 382,  716 => 377,  706 => 373,  698 => 368,  694 => 367,  688 => 364,  680 => 359,  676 => 358,  662 => 350,  658 => 349,  652 => 346,  644 => 341,  640 => 340,  634 => 337,  622 => 331,  616 => 328,  608 => 323,  598 => 319,  580 => 310,  564 => 297,  545 => 287,  541 => 286,  526 => 277,  507 => 267,  488 => 257,  462 => 240,  433 => 226,  424 => 220,  395 => 206,  382 => 199,  376 => 196,  341 => 173,  327 => 167,  320 => 153,  310 => 149,  291 => 139,  278 => 132,  259 => 122,  244 => 113,  448 => 267,  443 => 230,  429 => 259,  406 => 242,  366 => 206,  318 => 175,  282 => 133,  258 => 135,  222 => 108,  120 => 29,  272 => 129,  266 => 101,  226 => 87,  178 => 69,  111 => 27,  393 => 305,  386 => 200,  380 => 301,  362 => 294,  358 => 292,  342 => 188,  340 => 287,  334 => 284,  326 => 278,  319 => 273,  314 => 271,  299 => 266,  265 => 144,  252 => 137,  237 => 128,  194 => 103,  132 => 32,  23 => 3,  97 => 37,  81 => 28,  53 => 16,  654 => 223,  637 => 208,  632 => 206,  625 => 200,  623 => 199,  617 => 195,  612 => 192,  604 => 322,  593 => 187,  591 => 186,  586 => 313,  583 => 183,  578 => 180,  571 => 178,  557 => 177,  534 => 175,  522 => 276,  520 => 171,  504 => 162,  494 => 158,  463 => 145,  446 => 138,  440 => 136,  434 => 134,  431 => 260,  427 => 132,  405 => 210,  401 => 209,  397 => 114,  389 => 113,  381 => 112,  357 => 109,  349 => 108,  339 => 105,  303 => 99,  295 => 160,  287 => 97,  268 => 91,  74 => 18,  470 => 398,  452 => 236,  444 => 387,  435 => 405,  430 => 397,  414 => 216,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 887,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 864,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 825,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 808,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 763,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 732,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 697,  1387 => 694,  1378 => 688,  1374 => 687,  1368 => 684,  1359 => 678,  1355 => 677,  1349 => 674,  1340 => 668,  1336 => 667,  1330 => 664,  1321 => 658,  1317 => 657,  1311 => 654,  1302 => 648,  1298 => 647,  1292 => 644,  1283 => 638,  1279 => 637,  1273 => 634,  1264 => 628,  1260 => 627,  1254 => 624,  1245 => 618,  1241 => 617,  1235 => 614,  1226 => 608,  1222 => 607,  1216 => 604,  1207 => 598,  1203 => 597,  1197 => 594,  1188 => 588,  1184 => 587,  1178 => 584,  1169 => 578,  1165 => 577,  1159 => 574,  1150 => 568,  1146 => 567,  1140 => 564,  1123 => 550,  1119 => 549,  1113 => 546,  1104 => 540,  1100 => 539,  1094 => 536,  1085 => 530,  1081 => 529,  1075 => 526,  1066 => 520,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 456,  933 => 450,  929 => 449,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 426,  874 => 420,  870 => 419,  866 => 418,  860 => 415,  851 => 409,  847 => 408,  841 => 405,  832 => 399,  828 => 398,  822 => 395,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 365,  756 => 359,  752 => 395,  746 => 355,  737 => 349,  733 => 348,  727 => 345,  712 => 376,  708 => 332,  702 => 329,  693 => 323,  689 => 322,  683 => 319,  674 => 241,  670 => 355,  664 => 309,  649 => 297,  645 => 296,  639 => 293,  630 => 287,  626 => 332,  620 => 283,  611 => 277,  607 => 276,  601 => 189,  592 => 267,  588 => 185,  582 => 263,  563 => 253,  554 => 293,  550 => 246,  544 => 243,  535 => 283,  531 => 236,  516 => 273,  512 => 165,  506 => 163,  493 => 216,  478 => 253,  468 => 203,  459 => 144,  455 => 271,  449 => 193,  436 => 183,  428 => 181,  422 => 395,  409 => 117,  403 => 168,  390 => 224,  384 => 158,  351 => 137,  337 => 172,  311 => 118,  296 => 109,  256 => 138,  241 => 79,  215 => 99,  207 => 66,  192 => 88,  186 => 71,  175 => 93,  153 => 58,  118 => 33,  61 => 24,  34 => 8,  65 => 25,  77 => 28,  37 => 7,  190 => 102,  161 => 60,  137 => 52,  126 => 52,  261 => 86,  255 => 135,  247 => 130,  242 => 127,  214 => 110,  211 => 109,  191 => 98,  157 => 37,  145 => 53,  127 => 38,  373 => 111,  367 => 173,  363 => 172,  359 => 171,  354 => 197,  343 => 135,  335 => 104,  328 => 152,  322 => 176,  315 => 101,  309 => 270,  305 => 115,  302 => 267,  290 => 157,  284 => 154,  279 => 129,  271 => 147,  264 => 122,  248 => 118,  236 => 77,  223 => 82,  170 => 74,  110 => 37,  96 => 44,  84 => 32,  472 => 148,  467 => 148,  375 => 152,  371 => 151,  360 => 183,  356 => 182,  353 => 99,  350 => 179,  338 => 101,  336 => 98,  331 => 169,  321 => 89,  316 => 152,  307 => 269,  304 => 268,  301 => 143,  297 => 142,  292 => 110,  286 => 154,  283 => 105,  277 => 104,  275 => 103,  270 => 144,  263 => 123,  257 => 121,  253 => 119,  249 => 21,  245 => 92,  233 => 88,  225 => 103,  216 => 111,  206 => 93,  202 => 92,  198 => 20,  185 => 13,  180 => 79,  177 => 79,  165 => 42,  150 => 59,  124 => 46,  113 => 32,  100 => 24,  58 => 18,  251 => 93,  234 => 109,  213 => 113,  195 => 93,  174 => 72,  167 => 68,  146 => 56,  140 => 50,  128 => 48,  104 => 42,  90 => 23,  83 => 23,  52 => 11,  596 => 225,  590 => 314,  585 => 221,  577 => 218,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 210,  551 => 176,  546 => 207,  543 => 206,  539 => 205,  529 => 174,  525 => 173,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 266,  500 => 160,  497 => 263,  495 => 182,  492 => 157,  490 => 180,  487 => 213,  484 => 256,  482 => 177,  479 => 176,  477 => 401,  474 => 206,  471 => 173,  469 => 172,  466 => 146,  464 => 397,  461 => 169,  458 => 239,  456 => 167,  451 => 164,  445 => 160,  442 => 159,  439 => 229,  437 => 262,  432 => 398,  426 => 396,  423 => 149,  420 => 219,  418 => 251,  413 => 172,  399 => 237,  394 => 162,  378 => 215,  370 => 135,  368 => 295,  365 => 110,  361 => 131,  347 => 136,  345 => 94,  333 => 131,  329 => 130,  323 => 102,  317 => 272,  312 => 114,  306 => 166,  300 => 110,  294 => 68,  285 => 105,  280 => 99,  276 => 98,  267 => 60,  250 => 100,  239 => 91,  229 => 116,  218 => 82,  212 => 100,  210 => 97,  205 => 100,  188 => 59,  184 => 99,  181 => 65,  169 => 43,  160 => 66,  152 => 63,  148 => 38,  139 => 45,  134 => 49,  114 => 32,  107 => 28,  76 => 30,  70 => 17,  273 => 127,  269 => 94,  254 => 92,  246 => 126,  243 => 88,  240 => 112,  238 => 113,  235 => 94,  230 => 111,  227 => 124,  224 => 109,  221 => 102,  219 => 112,  217 => 84,  208 => 108,  204 => 97,  179 => 71,  159 => 61,  143 => 60,  135 => 41,  131 => 40,  108 => 30,  102 => 25,  71 => 20,  67 => 23,  63 => 16,  59 => 33,  47 => 13,  94 => 40,  89 => 36,  85 => 19,  79 => 22,  75 => 21,  68 => 18,  56 => 22,  50 => 13,  38 => 14,  29 => 4,  87 => 24,  72 => 25,  55 => 21,  21 => 2,  26 => 2,  35 => 4,  31 => 3,  41 => 11,  28 => 2,  201 => 80,  196 => 89,  183 => 72,  171 => 92,  166 => 63,  163 => 52,  156 => 66,  151 => 81,  142 => 41,  138 => 55,  136 => 33,  123 => 46,  121 => 44,  115 => 50,  105 => 40,  101 => 39,  91 => 25,  69 => 24,  66 => 16,  62 => 14,  49 => 13,  98 => 38,  93 => 36,  88 => 48,  78 => 20,  46 => 12,  44 => 7,  32 => 5,  27 => 4,  43 => 7,  40 => 6,  25 => 4,  24 => 2,  172 => 106,  158 => 49,  155 => 48,  129 => 119,  119 => 35,  117 => 44,  20 => 1,  22 => 103,  19 => 1,  209 => 82,  203 => 109,  199 => 79,  193 => 76,  189 => 76,  187 => 83,  182 => 70,  176 => 68,  173 => 67,  168 => 73,  164 => 72,  162 => 99,  154 => 59,  149 => 63,  147 => 119,  144 => 56,  141 => 35,  133 => 54,  130 => 53,  125 => 37,  122 => 41,  116 => 28,  112 => 45,  109 => 47,  106 => 26,  103 => 43,  99 => 43,  95 => 26,  92 => 37,  86 => 29,  82 => 22,  80 => 31,  73 => 19,  64 => 22,  60 => 19,  57 => 18,  54 => 15,  51 => 21,  48 => 14,  45 => 13,  42 => 11,  39 => 10,  36 => 9,  33 => 9,  30 => 4,);
    }
}
