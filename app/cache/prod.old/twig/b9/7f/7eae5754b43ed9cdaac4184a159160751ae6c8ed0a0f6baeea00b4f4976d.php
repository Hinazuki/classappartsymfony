<?php

/* PatCompteBundle::layout_admin.html.twig */
class __TwigTemplate_b97f7eae5754b43ed9cdaac4184a159160751ae6c8ed0a0f6baeea00b4f4976d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'header' => array($this, 'block_header'),
            'contentGauche' => array($this, 'block_contentGauche'),
            'contentDroit' => array($this, 'block_contentDroit'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "  ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

  <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/css/back.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" media=\"all\" />
  <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/css/slidingmenu.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" media=\"all\" />
";
    }

    // line 11
    public function block_header($context, array $blocks = array())
    {
        // line 12
        echo "  <div id=\"logo\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("PatFrontBundle_homepage", array("lg" => "fr"));
        echo "\"></a></div>

  <div class=\"posMenu\">

    <ul class=\"nav \">
      <li class=\"dropdown\">
        <a href=\"#\"  class=\"dropdown-toggle\" data-toggle=\"dropdown\">Créer</a>
        <ul class=\"dropdown-menu\">
          <li><a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("pat_admin_proprietaire_ajouter");
        echo "\" class=\" niveau_1_\">Propriétaire</a></li>
          <li><a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("pat_admin_locataire_ajouter");
        echo "\" class=\" niveau_1_\">Locataire</a></li>
          <li><a href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("pat_admin_appartement_ajouter");
        echo "\" class=\" niveau_1_\">Bien</a></li>
          <li><a href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("pat_admin_equipement_ajouter");
        echo "\" class=\" niveau_1_\">Équipement</a></li>
          <li><a href=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("pat_admin_ville_index");
        echo "\" class=\" niveau_1_\">Ville</a></li>
        </ul>
      </li>
      <li class=\"dropdown\">
        <a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("pat_admin_reservation_select_info_recherche");
        echo "\">Faire une réservation</a>
      </li>
      <li class=\"dropdown\">
        <a href=\"#\"  class=\"dropdown-toggle\" data-toggle=\"dropdown\">Gestion des contenus web</a>
        <ul class=\"dropdown-menu\">
          <li><a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("pat_admin_contenu_index_lg", array("langue" => "fr"));
        echo "\">Voir les contenus</a></li>
            ";
        // line 34
        echo $this->env->getExtension('actions')->renderUri($this->env->getExtension('http_kernel')->controller("PatCompteBundle:AdminContenu:menu"), array());
        // line 35
        echo "        </ul>
      </li>

    </ul>

    <ul class=\"nav pull-right\">
      ";
        // line 41
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 42
            echo "
        <a  class=\"btn btn-normal btn-inverse\" style=\"color: #fff\" href=\"";
            // line 43
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\">Quitter</a>

      ";
        }
        // line 46
        echo "    </ul>

  </div><!-- /menu -->
  ";
        // line 49
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 50
            echo "    <div id=\"brand\" class=\"adminArea\"><p>Espace administrateur</p></div>
  ";
        } else {
            // line 52
            echo "    <div id=\"brand\" class=\"ownerArea\"><p>Espace propriétaire</p></div>
  ";
        }
    }

    // line 56
    public function block_contentGauche($context, array $blocks = array())
    {
        // line 57
        echo "  ";
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 58
            echo "
    <div id=\"recherche\">
      <div class=\"headerRecherche\">
        <span>Recherche</span>
      </div>

      <div class=\"etapesRecherche adminRecherche\">
        <div class=\"adminTopSearch\">
          <label>Trouver par :</label>
          <select class=\"adminChooseSearch\">
            <option value=\"1\">Propriétaire</option>
            <option value=\"2\">Locataire</option>
            <option value=\"3\" selected=\"selected\">Bien</option>
            <option value=\"4\">Réservation</option>
            <option value=\"5\">Derniers biens ajoutés</option>
          </select>
        </div>

        <div class=\"adminSearchType1 adminSearchType\">
          <div class=\"contenuMenuAdmin\">";
            // line 77
            echo $this->env->getExtension('actions')->renderUri($this->env->getExtension('http_kernel')->controller("PatCompteBundle:AdminProprietaire:rechercher"), array());
            echo "</div>
        </div>

        <div class=\"adminSearchType2 adminSearchType\">
          <div class=\"contenuMenuAdmin\">";
            // line 81
            echo $this->env->getExtension('actions')->renderUri($this->env->getExtension('http_kernel')->controller("PatCompteBundle:AdminLocataire:rechercher"), array());
            echo "</div>
        </div>

        <div class=\"adminSearchType3 adminSearchType\">
          <div class=\"contenuMenuAdmin\">";
            // line 85
            echo $this->env->getExtension('actions')->renderUri($this->env->getExtension('http_kernel')->controller("PatCompteBundle:AdminAppartement:rechercher"), array());
            echo "</div>
        </div>

        <div class=\"adminSearchType4 adminSearchType\">
          <div class=\"contenuMenuAdmin\">";
            // line 89
            echo $this->env->getExtension('actions')->renderUri($this->env->getExtension('http_kernel')->controller("PatCompteBundle:AdminReservation:rechercher"), array());
            echo "</div>
        </div>

        <div class=\"adminSearchType5 adminSearchType\">
          <a href=\"";
            // line 93
            echo $this->env->getExtension('routing')->getPath("pat_admin_appartement_derniers_ajouts");
            echo "\" >Derniers biens ajoutés</a>
        </div>

      </div><!-- etapesRecherche -->
    </div><!-- /MenuBack -->

  ";
        }
        // line 100
        echo "
";
    }

    // line 103
    public function block_contentDroit($context, array $blocks = array())
    {
        // line 104
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
    }

    // line 108
    public function block_javascripts($context, array $blocks = array())
    {
        // line 109
        echo "  ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

  <script type=\"text/javascript\" src=\"";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/js/CompteFunctions.js"), "html", null, true);
        echo "\"></script>
  <script type=\"text/javascript\" src=\"";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/js/datepicker_admin_scripts.js"), "html", null, true);
        echo "\"></script>
  <script type=\"text/javascript\" src=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/__tinymce/tiny_mce.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\">
    \$(\"#ReservationRecherche_date_debut, #ReservationRecherche_date_fin\").attr('readonly', \"true\");

    \$('.adminChooseSearch').change(function () {
      if (\$(this).val() != 5) {
        \$('.adminSearchType').hide();
        \$('.adminSearchType' + \$(this).val()).show();
      } else {
        window.location.href = \"";
        // line 123
        echo $this->env->getExtension('routing')->getPath("pat_admin_appartement_derniers_ajouts");
        echo "\";
      }
    });
  </script>
";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle::layout_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  251 => 123,  234 => 112,  213 => 104,  195 => 93,  174 => 81,  167 => 77,  146 => 58,  140 => 56,  128 => 49,  104 => 35,  90 => 28,  83 => 24,  52 => 11,  596 => 225,  590 => 224,  585 => 221,  577 => 218,  573 => 216,  569 => 214,  560 => 212,  556 => 211,  553 => 210,  551 => 209,  546 => 207,  543 => 206,  539 => 205,  529 => 197,  525 => 195,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 185,  500 => 184,  497 => 183,  495 => 182,  492 => 181,  490 => 180,  487 => 179,  484 => 178,  482 => 177,  479 => 176,  477 => 175,  474 => 174,  471 => 173,  469 => 172,  466 => 171,  464 => 170,  461 => 169,  458 => 168,  456 => 167,  451 => 164,  445 => 160,  442 => 159,  439 => 158,  437 => 157,  432 => 154,  426 => 150,  423 => 149,  420 => 148,  418 => 147,  413 => 144,  399 => 143,  394 => 140,  378 => 137,  370 => 135,  368 => 134,  365 => 133,  361 => 131,  347 => 125,  345 => 124,  333 => 121,  329 => 120,  323 => 117,  317 => 116,  312 => 114,  306 => 113,  300 => 110,  294 => 109,  285 => 105,  280 => 103,  276 => 102,  267 => 101,  250 => 100,  239 => 95,  229 => 91,  218 => 82,  212 => 78,  210 => 103,  205 => 100,  188 => 89,  184 => 73,  181 => 85,  169 => 66,  160 => 59,  152 => 54,  148 => 53,  139 => 48,  134 => 52,  114 => 42,  107 => 33,  76 => 18,  70 => 16,  273 => 96,  269 => 94,  254 => 92,  246 => 99,  243 => 88,  240 => 86,  238 => 113,  235 => 94,  230 => 111,  227 => 81,  224 => 109,  221 => 108,  219 => 76,  217 => 75,  208 => 73,  204 => 72,  179 => 69,  159 => 61,  143 => 57,  135 => 53,  131 => 44,  108 => 36,  102 => 34,  71 => 21,  67 => 20,  63 => 13,  59 => 14,  47 => 9,  94 => 28,  89 => 20,  85 => 19,  79 => 23,  75 => 22,  68 => 14,  56 => 9,  50 => 10,  38 => 6,  29 => 3,  87 => 24,  72 => 16,  55 => 12,  21 => 2,  26 => 6,  35 => 5,  31 => 3,  41 => 7,  28 => 2,  201 => 92,  196 => 90,  183 => 82,  171 => 64,  166 => 61,  163 => 60,  156 => 66,  151 => 63,  142 => 49,  138 => 54,  136 => 56,  123 => 46,  121 => 46,  115 => 43,  105 => 40,  101 => 30,  91 => 27,  69 => 25,  66 => 14,  62 => 23,  49 => 19,  98 => 33,  93 => 9,  88 => 6,  78 => 22,  46 => 7,  44 => 12,  32 => 4,  27 => 4,  43 => 6,  40 => 8,  25 => 3,  24 => 3,  172 => 106,  158 => 67,  155 => 96,  129 => 119,  119 => 42,  117 => 43,  20 => 1,  22 => 2,  19 => 1,  209 => 82,  203 => 78,  199 => 71,  193 => 73,  189 => 71,  187 => 84,  182 => 70,  176 => 64,  173 => 68,  168 => 72,  164 => 60,  162 => 99,  154 => 58,  149 => 51,  147 => 50,  144 => 49,  141 => 48,  133 => 55,  130 => 50,  125 => 38,  122 => 37,  116 => 41,  112 => 41,  109 => 34,  106 => 33,  103 => 31,  99 => 31,  95 => 28,  92 => 21,  86 => 28,  82 => 22,  80 => 20,  73 => 17,  64 => 17,  60 => 13,  57 => 11,  54 => 10,  51 => 14,  48 => 13,  45 => 8,  42 => 6,  39 => 9,  36 => 4,  33 => 3,  30 => 7,);
    }
}
