<?php

/* ::email_layout.html.twig */
class __TwigTemplate_68723dc094571643140923bf8f0f86d6258b98cdc301b28c0535ec12eb642c93 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'styles' => array($this, 'block_styles'),
            'title' => array($this, 'block_title'),
            'subtitle' => array($this, 'block_subtitle'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
        <meta  />
        <title>Class Appart</title>
        <style type=\"text/css\">
                /* Client-specific Styles */
                #outlook a{padding:0;} /* Force Outlook to provide a \"view in browser\" button. */
                body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
                body{-webkit-text-size-adjust:none;font-size:14px;} /* Prevent Webkit platforms from changing default text sizes. */

                /* Reset Styles */
                body{margin:0; padding:0;}
                img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
                table td{border-collapse:collapse;}
                #backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}

                #templateContainer{
                    border: 1px solid #c2c2c2;
                    margin-bottom:40px;
                }
                ul{
                    margin-top: 5px;
                }
                ul li{
                    list-style: none;
                }
                h1, .h1{
                    color: #A42242;
                    font-family: Raleway-Regular;
                    font-size: 32px;
                    font-weight: normal;
                    text-align: left;
                    text-transform: none;
                    margin:0;
                    padding:0;
                    display:block;
                    font-weight:bold;
                    line-height:100%;
                    margin-top:0;
                    margin-right:0;
                    margin-bottom:0px;
                    margin-left:0;
                    text-align:left;
                    font-weight: normal;
                }
                
                h3{
                    color: #A42242;
                    font-family: Raleway-Regular;
                    font-size: 14px;
                    font-weight: normal;
                }
                h3.titleResa{
                    color: #A42242;
                    font-family: Raleway-Regular;
                    font-size: 18px;
                    font-weight: bold;
                }
                    
                hr{
                    border: 1px solid #E8E8E8
                }
                
                label, .label{
                    font-weight: bold;
                }

                sup {
                    vertical-align: super;
                    font-size: smaller;
                    line-height: .5em\t
                }


                h2, .h2{
                    color:#202020;
                    display:block;
                    font-family:Arial;
                    font-size:14px;
                    font-weight:bold;
                    line-height:100%;
                    margin-top:10px;
                    margin-right:0;
                    margin-bottom:7px;
                    margin-left:0;
                    text-transform: uppercase;
                    text-align:left;
                }

                h4, .h4{
                    color:#202020;
                    display:block;
                    font-family:Arial;
                    font-size:22px;
                    font-weight:bold;
                    line-height:100%;
                    margin-top:0;
                    margin-right:0;
                    margin-bottom:10px;
                    margin-left:0;
                    text-align:left;
                }

                #templatePreheader{
                    background-color:#FAFAFA;

                }

                #templateAdresse{
                    background-color: #000000;
                    border:1px solid #000000;
                    color:#FFFFFF;
                    font-family: Arial;
                    font-size:14px;
                    font-weight:normal !important;
                }

                .adresse{
                    color:#FFFFFF;
                    text-align: left;
                    margin:20px;
                    font-family: Arial;
                    font-size: 14px;
                }

                .preheaderContent div{
                    color:#505050;
                    font-family:Arial;
                    font-size:10px;
                    line-height:100%;


                }

                .preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{
                    font-weight:normal;
                }

                #templateHeader{
                    background-color:#FFFFFF;
                    border-bottom:0;
                }

                .headerContent{
                    color:#202020;
                    font-family:Arial;
                    font-size:34px;
                    font-weight:bold;
                    line-height:100%;
                    padding:0;
                    text-align:center;
                    vertical-align:middle;
                }

                .headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
                    font-weight:normal;
                }

                #headerImage{
                    height:auto;
                    max-width:700px;
                }

                #templateContainer, .bodyContent{
                    background-color:#FFFFFF;
                }

                .bodyContent div{
                    font-family:Arial;
                    line-height:140%;
                }

                .bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{
                    font-weight:normal;
                }

                .bodyContent img{
                    display:inline;
                    height:auto;
                }

                #templateSidebar{
                    background-color:#FFFFFF;
                    border-left:0;
                }

                .sidebarContent div{
                    font-family:Arial;
                    font-size:12px;
                    line-height:150%;
                }

                .sidebarContent div a:link, .sidebarContent div a:visited, /* Yahoo! Mail Override */ .sidebarContent div a .yshortcuts /* Yahoo! Mail Override */{
                    font-weight:normal;
                }

                .sidebarContent img{
                    display:inline;
                    height:auto;
                }

                #templateFooter{
                    background-color:#FFFFFF;
                    border-top:0;
                }

                .footerContent div{
                    color:#707070;
                    font-family:Arial;
                    font-size:12px;
                    line-height:125%;
                    text-align:left;
                }

                .footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{
                    font-weight:normal;
                }

                .footerContent img{
                    display:inline;
                }

                #social{
                    background-color:#FAFAFA;
                    border:0;
                }

                #social div{
                    text-align:center;
                }

                #utility{
                    background-color:#FFFFFF;
                    border:0;
                }


                #utility div{
                    text-align:center;
                }

                #monkeyRewards img{
                    max-width:190px;
                }

                .contentText{
                    font-family: Arial;
                    font-size: 14px;
                    color:#000000;
                    text-align: left;
                }

                .signature{

                }

                .contentMarques{
                    margin-top:35px;
                    font-size:12px;
                    color:#525252;
                    text-align: left;
                }

                .contentSidebar{
                    border-left:1px dotted #000000;
                    text-align: center;
                    margin-top:40px;

                }

                .contentSAText{
                    margin:15px;
                    color:#525252;
                    font-size:12px;
                }

                .contentSAText a, .premiumFooter2 a{
                    background:#fff3ae;
                    margin:5px;
                    text-decoration: none;
                    color:#000000;
                    margin-bottom:20px;
                    font-size:14px;
                } 

                .contentSAText2{
                    color:#525252;
                    font-size:12px;
                    margin:15px;
                }

                .contentSAText2 a{
                    color:#eb1200;
                    text-decoration: none;
                }

                .premiumContent{
                    text-align:center;
                    padding-top:20px;
                }

                .premiumFooter{
                    font-size:16px;
                }

                .premiumFooter2{
                    margin-top:10px; 
                    font-size:14px;
                }

                .phoneNumber{
                    color:#cc0c20;
                    font-size:19px;
                    font-weight:bold;
                }

                .contentSAMap{
                    margin:15px;
                }

                .label{
                    font-weight:bold;
                    color:#000000;
                }

                .value{
                    color:#525252;
                }
                
                .legend{
                    font-size: 10px;
                }
                
                a{
                    color:#00b3a9;
                }
                
                .withoutMargin{
                    margin: 5px 0;
                }
                
                .footer span{
                    color: #00b3a9;
                    font-family: Raleway-Regular, Arial, sans-serif;;
                    font-size: 14px;
                    font-weight: normal;
                    line-height: 14px;
                    margin: 0;
                    padding: 0;
                }
                h4{
                    color: #333333;
                    font-family: Raleway-Thin, Arial, sans-serif;
                    font-size: 36px;
                    font-weight: lighter;
                    letter-spacing: -2px;
                    line-height: 40px;
                    padding-top: 10px;
                    text-transform: uppercase;
                }
                
                .footer .left{
                    width: 65px;
                    float:left;
                }
                
                .footer{
                    width:100%;                    
                    margin-top: 40px;
                    margin-bottom: 50px;
                }
                .footer .right{
                    width: 450px;
                    float: left;
                }
                
                .colorLink{
                    color: #00b3a9;
                }
                 .strongFont{ font-size:16px!important;}
                
                .center{
                    text-align: center;
                }
                  ";
        // line 387
        $this->displayBlock('styles', $context, $blocks);
        // line 389
        echo " 
                
        </style>
    </head>
    <body leftmargin=\"0\" marginwidth=\"0\" topmargin=\"0\" marginheight=\"0\" offset=\"0\" id=\"background\">
        <div style=\"width:550px; margin:0 auto;\">
            <h1>";
        // line 395
        $this->displayBlock('title', $context, $blocks);
        echo "</h1>
            <h2>";
        // line 396
        $this->displayBlock('subtitle', $context, $blocks);
        echo "</h2>
            ";
        // line 397
        $this->displayBlock('content', $context, $blocks);
        // line 398
        echo "            ";
        $this->displayBlock('footer', $context, $blocks);
        // line 405
        echo "        </div>
        
    </body>
</html>

";
    }

    // line 387
    public function block_styles($context, array $blocks = array())
    {
        echo "        

                  ";
    }

    // line 395
    public function block_title($context, array $blocks = array())
    {
        echo "Titre";
    }

    // line 396
    public function block_subtitle($context, array $blocks = array())
    {
        echo "Sous-Titre";
    }

    // line 397
    public function block_content($context, array $blocks = array())
    {
        echo "Contenu";
    }

    // line 398
    public function block_footer($context, array $blocks = array())
    {
        echo " 
            <div class=\"footer\">
                <a href=\"http://www.class-appart.com\" target=\"_blank\" style=\"border: none; text-decoration: none;\">
                    <img width=\"610\" height=\"100\" style=\"border: none;\" src=\"";
        // line 401
        echo twig_escape_filter($this->env, ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "getSchemeAndHttpHost", array(), "method") . $this->env->getExtension('assets')->getAssetUrl("/bundles/patfront/images/classappart_signature.jpg")), "html", null, true);
        echo "\"/>
                </a>
            </div>
        ";
    }

    public function getTemplateName()
    {
        return "::email_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  470 => 398,  452 => 395,  444 => 387,  435 => 405,  430 => 397,  414 => 389,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 887,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 864,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 825,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 808,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 763,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 732,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 697,  1387 => 694,  1378 => 688,  1374 => 687,  1368 => 684,  1359 => 678,  1355 => 677,  1349 => 674,  1340 => 668,  1336 => 667,  1330 => 664,  1321 => 658,  1317 => 657,  1311 => 654,  1302 => 648,  1298 => 647,  1292 => 644,  1283 => 638,  1279 => 637,  1273 => 634,  1264 => 628,  1260 => 627,  1254 => 624,  1245 => 618,  1241 => 617,  1235 => 614,  1226 => 608,  1222 => 607,  1216 => 604,  1207 => 598,  1203 => 597,  1197 => 594,  1188 => 588,  1184 => 587,  1178 => 584,  1169 => 578,  1165 => 577,  1159 => 574,  1150 => 568,  1146 => 567,  1140 => 564,  1123 => 550,  1119 => 549,  1113 => 546,  1104 => 540,  1100 => 539,  1094 => 536,  1085 => 530,  1081 => 529,  1075 => 526,  1066 => 520,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 456,  933 => 450,  929 => 449,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 426,  874 => 420,  870 => 419,  866 => 418,  860 => 415,  851 => 409,  847 => 408,  841 => 405,  832 => 399,  828 => 398,  822 => 395,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 365,  756 => 359,  752 => 358,  746 => 355,  737 => 349,  733 => 348,  727 => 345,  712 => 333,  708 => 332,  702 => 329,  693 => 323,  689 => 322,  683 => 319,  674 => 313,  670 => 312,  664 => 309,  649 => 297,  645 => 296,  639 => 293,  630 => 287,  626 => 286,  620 => 283,  611 => 277,  607 => 276,  601 => 273,  592 => 267,  588 => 266,  582 => 263,  563 => 253,  554 => 247,  550 => 246,  544 => 243,  535 => 237,  531 => 236,  516 => 227,  512 => 226,  506 => 223,  493 => 216,  478 => 207,  468 => 203,  459 => 197,  455 => 196,  449 => 193,  436 => 183,  428 => 181,  422 => 395,  409 => 171,  403 => 168,  390 => 161,  384 => 158,  351 => 137,  337 => 132,  311 => 118,  296 => 109,  256 => 84,  241 => 79,  215 => 69,  207 => 66,  192 => 61,  186 => 58,  175 => 52,  153 => 41,  118 => 22,  61 => 1,  34 => 4,  65 => 15,  77 => 24,  37 => 8,  190 => 60,  161 => 47,  137 => 39,  126 => 24,  261 => 86,  255 => 135,  247 => 130,  242 => 127,  214 => 110,  211 => 109,  191 => 98,  157 => 46,  145 => 60,  127 => 49,  373 => 176,  367 => 173,  363 => 172,  359 => 171,  354 => 169,  343 => 135,  335 => 156,  328 => 152,  322 => 149,  315 => 119,  309 => 140,  305 => 115,  302 => 137,  290 => 133,  284 => 132,  279 => 129,  271 => 126,  264 => 122,  248 => 118,  236 => 77,  223 => 113,  170 => 74,  110 => 20,  96 => 29,  84 => 9,  472 => 149,  467 => 148,  375 => 152,  371 => 151,  360 => 99,  356 => 170,  353 => 99,  350 => 98,  338 => 101,  336 => 98,  331 => 95,  321 => 89,  316 => 87,  307 => 85,  304 => 84,  301 => 83,  297 => 70,  292 => 108,  286 => 65,  283 => 64,  277 => 71,  275 => 64,  270 => 61,  263 => 25,  257 => 121,  253 => 119,  249 => 21,  245 => 20,  233 => 23,  225 => 74,  216 => 111,  206 => 22,  202 => 91,  198 => 20,  185 => 13,  180 => 12,  177 => 11,  165 => 154,  150 => 63,  124 => 49,  113 => 83,  100 => 30,  58 => 17,  251 => 81,  234 => 112,  213 => 104,  195 => 93,  174 => 51,  167 => 47,  146 => 58,  140 => 40,  128 => 113,  104 => 31,  90 => 28,  83 => 24,  52 => 14,  596 => 225,  590 => 224,  585 => 221,  577 => 218,  573 => 257,  569 => 256,  560 => 212,  556 => 211,  553 => 210,  551 => 209,  546 => 207,  543 => 206,  539 => 205,  529 => 197,  525 => 233,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 185,  500 => 184,  497 => 217,  495 => 182,  492 => 181,  490 => 180,  487 => 213,  484 => 178,  482 => 177,  479 => 176,  477 => 401,  474 => 206,  471 => 173,  469 => 172,  466 => 171,  464 => 397,  461 => 169,  458 => 396,  456 => 167,  451 => 164,  445 => 160,  442 => 159,  439 => 158,  437 => 157,  432 => 398,  426 => 396,  423 => 149,  420 => 148,  418 => 147,  413 => 172,  399 => 143,  394 => 162,  378 => 137,  370 => 135,  368 => 129,  365 => 148,  361 => 131,  347 => 136,  345 => 94,  333 => 131,  329 => 130,  323 => 117,  317 => 116,  312 => 114,  306 => 113,  300 => 110,  294 => 68,  285 => 105,  280 => 99,  276 => 98,  267 => 60,  250 => 100,  239 => 78,  229 => 116,  218 => 82,  212 => 68,  210 => 67,  205 => 100,  188 => 59,  184 => 73,  181 => 56,  169 => 48,  160 => 59,  152 => 54,  148 => 38,  139 => 48,  134 => 54,  114 => 21,  107 => 33,  76 => 23,  70 => 23,  273 => 127,  269 => 94,  254 => 92,  246 => 117,  243 => 88,  240 => 19,  238 => 113,  235 => 94,  230 => 111,  227 => 81,  224 => 109,  221 => 72,  219 => 112,  217 => 70,  208 => 108,  204 => 72,  179 => 69,  159 => 61,  143 => 59,  135 => 38,  131 => 44,  108 => 32,  102 => 74,  71 => 21,  67 => 20,  63 => 16,  59 => 14,  47 => 12,  94 => 30,  89 => 11,  85 => 19,  79 => 24,  75 => 20,  68 => 21,  56 => 15,  50 => 10,  38 => 6,  29 => 3,  87 => 31,  72 => 22,  55 => 15,  21 => 2,  26 => 2,  35 => 4,  31 => 3,  41 => 7,  28 => 2,  201 => 92,  196 => 63,  183 => 57,  171 => 6,  166 => 61,  163 => 128,  156 => 66,  151 => 44,  142 => 41,  138 => 56,  136 => 28,  123 => 46,  121 => 46,  115 => 33,  105 => 40,  101 => 30,  91 => 27,  69 => 18,  66 => 17,  62 => 18,  49 => 11,  98 => 28,  93 => 9,  88 => 27,  78 => 22,  46 => 10,  44 => 12,  32 => 6,  27 => 4,  43 => 7,  40 => 6,  25 => 3,  24 => 1,  172 => 106,  158 => 43,  155 => 42,  129 => 119,  119 => 34,  117 => 43,  20 => 1,  22 => 1120,  19 => 1,  209 => 82,  203 => 106,  199 => 71,  193 => 85,  189 => 84,  187 => 84,  182 => 70,  176 => 64,  173 => 68,  168 => 72,  164 => 46,  162 => 99,  154 => 58,  149 => 51,  147 => 119,  144 => 118,  141 => 117,  133 => 55,  130 => 25,  125 => 38,  122 => 23,  116 => 34,  112 => 33,  109 => 34,  106 => 18,  103 => 30,  99 => 14,  95 => 33,  92 => 28,  86 => 27,  82 => 8,  80 => 24,  73 => 24,  64 => 2,  60 => 15,  57 => 16,  54 => 14,  51 => 13,  48 => 9,  45 => 10,  42 => 13,  39 => 1135,  36 => 9,  33 => 7,  30 => 3,);
    }
}
