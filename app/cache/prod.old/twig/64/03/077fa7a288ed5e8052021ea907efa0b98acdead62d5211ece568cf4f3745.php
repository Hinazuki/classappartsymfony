<?php

/* PatCompteBundle:AdminDocuments:edit_content.html.twig */
class __TwigTemplate_6403077fa7a288ed5e8052021ea907efa0b98acdead62d5211ece568cf4f3745 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"content\">

  <h1>Documents <small>(";
        // line 3
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["proprietaire"]) ? $context["proprietaire"] : $this->getContext($context, "proprietaire")), "prenom", array())), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["proprietaire"]) ? $context["proprietaire"] : $this->getContext($context, "proprietaire")), "nom", array())), "html", null, true);
        echo ")</small></h1>

  ";
        // line 5
        $this->env->loadTemplate("PatCompteBundle:AdminDocuments:_header_navigation.html.twig")->display($context);
        // line 6
        echo "
  <div class=\"formBien\">
    <form action=\"\" method=\"post\" ";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo ">

      <div class=\"error\">
        ";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
      </div>

      <div class=\"blocForm\">
        <div class=\"title\">Editer un document</div>

        <div class=\"content\">

          <div class=\"row-fluid\">
            <div class=\"span4\">
              ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "documentName", array()), 'label');
        echo "
            </div>
            <div class=\"span8\">
              ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "documentName", array()), 'errors');
        echo "
              ";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "documentName", array()), 'widget');
        echo "
            </div>
          </div>
          <br />
          <div class=\"row-fluid\">
            <div class=\"span4\">
              ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'label');
        echo "
            </div>
            <div class=\"span8\">
              ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'errors');
        echo "
              ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'widget', array("required" => false));
        echo "
              <script>
                \$('input[type=file]').bootstrapFileInput();
              </script>
            </div>
          </div>
          <br />
          <div class=\"row-fluid\">
            <div class=\"span4\">
              ";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "appartement", array()), 'label');
        echo "
            </div>
            <div class=\"span8\">
              ";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "appartement", array()), 'errors');
        echo "
              ";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "appartement", array()), 'widget');
        echo "
            </div>
          </div>

          <br />

          <div class=\"row-fluid\">
            <div class=\"span4\">
              Visible sur le bien* :
            </div>
            <div class=\"span8\">
              ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visible", array()), 'errors');
        echo "
              ";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visible", array()), 'widget');
        echo "
            </div>
          </div>

          ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "

          <br />
          <div class=\"row-fluid\">
            <div class=\"span4\"></div>
            <div class=\"span8\">
              <button type=\"submit\"  class=\"btn btn-primary\">Valider</button>
            </div>
          </div>
          </form>
        </div>
      </div>
      * Si la case est coch&eacute;e, le document sera t&eacute;l&eacute;chargeable par les internautes depuis la fiche du bien. Son nom correspondra au type de document choisi.
  </div>
</div><!-- /contenuCentrale -->
";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:AdminDocuments:edit_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 59,  272 => 102,  266 => 101,  226 => 87,  178 => 69,  111 => 47,  393 => 305,  386 => 302,  380 => 301,  362 => 294,  358 => 292,  342 => 288,  340 => 287,  334 => 284,  326 => 278,  319 => 273,  314 => 271,  299 => 266,  265 => 144,  252 => 137,  237 => 128,  194 => 103,  132 => 71,  23 => 3,  97 => 35,  81 => 28,  53 => 16,  654 => 223,  637 => 208,  632 => 206,  625 => 200,  623 => 199,  617 => 195,  612 => 192,  604 => 190,  593 => 187,  591 => 186,  586 => 184,  583 => 183,  578 => 180,  571 => 178,  557 => 177,  534 => 175,  522 => 172,  520 => 171,  504 => 162,  494 => 158,  463 => 145,  446 => 138,  440 => 136,  434 => 134,  431 => 133,  427 => 132,  405 => 116,  401 => 115,  397 => 114,  389 => 113,  381 => 112,  357 => 109,  349 => 108,  339 => 105,  303 => 99,  295 => 111,  287 => 97,  268 => 91,  74 => 31,  470 => 398,  452 => 139,  444 => 387,  435 => 405,  430 => 397,  414 => 389,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 887,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 864,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 825,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 808,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 763,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 732,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 697,  1387 => 694,  1378 => 688,  1374 => 687,  1368 => 684,  1359 => 678,  1355 => 677,  1349 => 674,  1340 => 668,  1336 => 667,  1330 => 664,  1321 => 658,  1317 => 657,  1311 => 654,  1302 => 648,  1298 => 647,  1292 => 644,  1283 => 638,  1279 => 637,  1273 => 634,  1264 => 628,  1260 => 627,  1254 => 624,  1245 => 618,  1241 => 617,  1235 => 614,  1226 => 608,  1222 => 607,  1216 => 604,  1207 => 598,  1203 => 597,  1197 => 594,  1188 => 588,  1184 => 587,  1178 => 584,  1169 => 578,  1165 => 577,  1159 => 574,  1150 => 568,  1146 => 567,  1140 => 564,  1123 => 550,  1119 => 549,  1113 => 546,  1104 => 540,  1100 => 539,  1094 => 536,  1085 => 530,  1081 => 529,  1075 => 526,  1066 => 520,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 456,  933 => 450,  929 => 449,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 426,  874 => 420,  870 => 419,  866 => 418,  860 => 415,  851 => 409,  847 => 408,  841 => 405,  832 => 399,  828 => 398,  822 => 395,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 365,  756 => 359,  752 => 358,  746 => 355,  737 => 349,  733 => 348,  727 => 345,  712 => 333,  708 => 332,  702 => 329,  693 => 323,  689 => 322,  683 => 319,  674 => 241,  670 => 312,  664 => 309,  649 => 297,  645 => 296,  639 => 293,  630 => 287,  626 => 286,  620 => 283,  611 => 277,  607 => 276,  601 => 189,  592 => 267,  588 => 185,  582 => 263,  563 => 253,  554 => 247,  550 => 246,  544 => 243,  535 => 237,  531 => 236,  516 => 227,  512 => 165,  506 => 163,  493 => 216,  478 => 150,  468 => 203,  459 => 144,  455 => 196,  449 => 193,  436 => 183,  428 => 181,  422 => 395,  409 => 117,  403 => 168,  390 => 161,  384 => 158,  351 => 137,  337 => 132,  311 => 118,  296 => 109,  256 => 138,  241 => 79,  215 => 69,  207 => 66,  192 => 61,  186 => 71,  175 => 93,  153 => 60,  118 => 22,  61 => 24,  34 => 13,  65 => 25,  77 => 21,  37 => 10,  190 => 102,  161 => 56,  137 => 52,  126 => 68,  261 => 86,  255 => 135,  247 => 130,  242 => 127,  214 => 110,  211 => 109,  191 => 98,  157 => 65,  145 => 78,  127 => 51,  373 => 111,  367 => 173,  363 => 172,  359 => 171,  354 => 169,  343 => 135,  335 => 104,  328 => 152,  322 => 149,  315 => 101,  309 => 270,  305 => 115,  302 => 267,  290 => 157,  284 => 154,  279 => 129,  271 => 147,  264 => 122,  248 => 118,  236 => 77,  223 => 82,  170 => 74,  110 => 37,  96 => 44,  84 => 35,  472 => 148,  467 => 148,  375 => 152,  371 => 151,  360 => 99,  356 => 170,  353 => 99,  350 => 290,  338 => 101,  336 => 98,  331 => 103,  321 => 89,  316 => 87,  307 => 269,  304 => 268,  301 => 83,  297 => 70,  292 => 110,  286 => 106,  283 => 105,  277 => 104,  275 => 103,  270 => 61,  263 => 100,  257 => 121,  253 => 86,  249 => 21,  245 => 92,  233 => 88,  225 => 74,  216 => 111,  206 => 22,  202 => 91,  198 => 20,  185 => 13,  180 => 75,  177 => 11,  165 => 62,  150 => 59,  124 => 60,  113 => 61,  100 => 30,  58 => 19,  251 => 93,  234 => 112,  213 => 113,  195 => 93,  174 => 72,  167 => 68,  146 => 52,  140 => 32,  128 => 29,  104 => 35,  90 => 38,  83 => 24,  52 => 17,  596 => 225,  590 => 224,  585 => 221,  577 => 218,  573 => 257,  569 => 256,  560 => 212,  556 => 211,  553 => 210,  551 => 176,  546 => 207,  543 => 206,  539 => 205,  529 => 174,  525 => 173,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 185,  500 => 160,  497 => 217,  495 => 182,  492 => 157,  490 => 180,  487 => 213,  484 => 151,  482 => 177,  479 => 176,  477 => 401,  474 => 206,  471 => 173,  469 => 172,  466 => 146,  464 => 397,  461 => 169,  458 => 396,  456 => 167,  451 => 164,  445 => 160,  442 => 159,  439 => 158,  437 => 157,  432 => 398,  426 => 396,  423 => 149,  420 => 148,  418 => 147,  413 => 172,  399 => 143,  394 => 162,  378 => 300,  370 => 135,  368 => 295,  365 => 110,  361 => 131,  347 => 136,  345 => 94,  333 => 131,  329 => 130,  323 => 102,  317 => 272,  312 => 114,  306 => 113,  300 => 110,  294 => 68,  285 => 105,  280 => 99,  276 => 98,  267 => 60,  250 => 100,  239 => 91,  229 => 116,  218 => 82,  212 => 68,  210 => 97,  205 => 100,  188 => 59,  184 => 99,  181 => 65,  169 => 70,  160 => 66,  152 => 63,  148 => 38,  139 => 48,  134 => 45,  114 => 21,  107 => 58,  76 => 29,  70 => 38,  273 => 127,  269 => 94,  254 => 92,  246 => 134,  243 => 88,  240 => 19,  238 => 113,  235 => 94,  230 => 111,  227 => 124,  224 => 109,  221 => 86,  219 => 112,  217 => 84,  208 => 108,  204 => 94,  179 => 71,  159 => 61,  143 => 60,  135 => 38,  131 => 64,  108 => 22,  102 => 47,  71 => 18,  67 => 22,  63 => 16,  59 => 33,  47 => 13,  94 => 40,  89 => 36,  85 => 19,  79 => 26,  75 => 31,  68 => 24,  56 => 22,  50 => 18,  38 => 14,  29 => 3,  87 => 32,  72 => 28,  55 => 21,  21 => 2,  26 => 6,  35 => 9,  31 => 3,  41 => 7,  28 => 2,  201 => 80,  196 => 63,  183 => 70,  171 => 92,  166 => 69,  163 => 57,  156 => 66,  151 => 81,  142 => 41,  138 => 55,  136 => 60,  123 => 46,  121 => 44,  115 => 50,  105 => 46,  101 => 36,  91 => 34,  69 => 28,  66 => 23,  62 => 2,  49 => 20,  98 => 52,  93 => 35,  88 => 48,  78 => 41,  46 => 11,  44 => 7,  32 => 6,  27 => 4,  43 => 12,  40 => 11,  25 => 4,  24 => 2,  172 => 106,  158 => 43,  155 => 82,  129 => 119,  119 => 51,  117 => 62,  20 => 1,  22 => 103,  19 => 1,  209 => 82,  203 => 109,  199 => 79,  193 => 76,  189 => 77,  187 => 76,  182 => 70,  176 => 68,  173 => 68,  168 => 72,  164 => 46,  162 => 99,  154 => 54,  149 => 59,  147 => 119,  144 => 56,  141 => 53,  133 => 54,  130 => 56,  125 => 38,  122 => 41,  116 => 39,  112 => 23,  109 => 47,  106 => 48,  103 => 43,  99 => 43,  95 => 33,  92 => 37,  86 => 36,  82 => 11,  80 => 34,  73 => 39,  64 => 25,  60 => 12,  57 => 20,  54 => 17,  51 => 21,  48 => 8,  45 => 10,  42 => 11,  39 => 9,  36 => 8,  33 => 7,  30 => 5,);
    }
}
