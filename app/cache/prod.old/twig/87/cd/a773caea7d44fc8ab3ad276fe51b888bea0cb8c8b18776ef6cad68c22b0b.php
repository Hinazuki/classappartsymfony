<?php

/* PatCompteBundle:AdminReservation:selectAppartement_content.html.twig */
class __TwigTemplate_87cda773caea7d44fc8ab3ad276fe51b888bea0cb8c8b18776ef6cad68c22b0b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "
  <div id=\"contenuCentrale1\">

    ";
        // line 5
        if ((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement"))) {
            // line 6
            echo "      ";
            $context["i"] = "0";
            // line 7
            echo "
      <div id=\"titreAccueilAdmin\">
        <h1><span class=\"spanH1\">Sélection d'un appartement</h1>

      </div>
      <div class=\"clear\"></div>

      <div id=\"titreAccueil\">
        <h1><span>ma recherche :</span> ";
            // line 15
            echo twig_escape_filter($this->env, (isset($context["nb_resultat"]) ? $context["nb_resultat"] : $this->getContext($context, "nb_resultat")), "html", null, true);
            echo " ";
            if (((isset($context["nb_resultat"]) ? $context["nb_resultat"] : $this->getContext($context, "nb_resultat")) < "2")) {
                echo " résultat ";
            } else {
                echo " résultats ";
            }
            echo "</h1>
      </div>

      <div class=\"clear\"></div>

      <a href=\"";
            // line 20
            echo $this->env->getExtension('routing')->getPath("pat_admin_reservation_select_info_recherche");
            echo "\" class=\"boutonJ2\">Précédent</a>

      <br/>
      ";
            // line 23
            echo $this->env->getExtension('knp_pagination')->render((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")));
            echo "

      <div style=\"float:left; width:100%; height:20px;\"></div>
      ";
            // line 26
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")));
            foreach ($context['_seq'] as $context["_key"] => $context["appart"]) {
                // line 27
                echo "
        <div class=\"ListeAppart row-fluid blocItem\">
          <div class=\"row ListeAppart1\">
            <div class=\"row-fluid\">
              <div class=\"span2\">
                ";
                // line 36
                echo "              </div>
              <div class=\"span8\">
                <a href=\"";
                // line 38
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_appartement_detail", array("id" => $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "id", array()))), "html", null, true);
                echo "\" target=\"_blank\" class=\"appart_list_link\">
                  <div class=\"reference\">
                    ";
                // line 40
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "nomResidence", array()), "html", null, true);
                echo " | Réf : ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "reference", array()), "html", null, true);
                echo "
                  </div>
                  <div class=\"titreAppart\">
                    <strong>
                      ";
                // line 44
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "type", array())), "html", null, true);
                echo " ";
                if ($this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "nbPieces", array())) {
                    echo "T";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "nbPieces", array()), "html", null, true);
                }
                echo " - ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "surfaceSol", array()), "html", null, true);
                echo " m<sup>2</sup>
                      situé à ";
                // line 45
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "ville", array()), "html", null, true);
                echo "
                    </strong>
                  </div>
                  ";
                // line 48
                if ((!(null === $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "tarif", array())))) {
                    // line 49
                    echo "                    <div class=\"tarifAppart\">
                      A partir de <strong>";
                    // line 50
                    echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format($this->env->getExtension('my_twig_extension')->getFromTarif((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")))), "html", null, true);
                    echo "</strong> €/nuit
                    </div>
                  ";
                }
                // line 53
                echo "                </a>
              </div>
              <div class=\"span2 itemLink\">
                <a href=\"";
                // line 56
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_appartement_detail", array("id" => $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-primary\">Découvrir</a>
              </div>
            </div>
          </div>
        </div>

        <div id=\"ZoneRecherche\">
          <div id=\"RechercheHaut\"></div><!-- /RechercheHaut -->
          <div id=\"RecherchePixel\">
            <div id=\"RecherchePixel1\">
              <div class=\"HeaderBien\">
                <div class=\"TitreBien\">
                  <span class=\"BlocDetail\">RÉF : ";
                // line 68
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "reference", array()), "html", null, true);
                echo "</span><br/>
                  ";
                // line 69
                echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "type", array())), "html", null, true);
                echo "&nbsp;";
                if ($this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "nbPieces", array())) {
                    echo "T";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "nbPieces", array()), "html", null, true);
                }
                echo "&nbsp;";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "surfaceSol", array()), "html", null, true);
                echo " m<sup>2</sup>&nbsp;";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "ville", array()), "html", null, true);
                echo "
                </div><!-- /TitreBien -->
                <div class=\"LoyerBien\">
                  A partir de : <span class=\"LoyerBien1\"> ";
                // line 72
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->env->getExtension('my_twig_extension')->getFromTarif((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")))), "html", null, true);
                echo " €/mois</span>
                </div>
              </div><!-- /HeaderBien -->


              ";
                // line 77
                if ((isset($context["media"]) ? $context["media"] : $this->getContext($context, "media"))) {
                    // line 78
                    echo "                ";
                    if ($this->getAttribute((isset($context["media"]) ? $context["media"] : $this->getContext($context, "media")), $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "id", array()), array(), "array")) {
                        // line 79
                        echo "                  <a href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_front_appartement_detail", array("url_bien" => $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "urlFr", array()))), "html", null, true);
                        echo "\" target=\"_blank\">
                    <div class=\"bienPhoto\">
                      <img src=\"";
                        // line 81
                        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/photos/biens"), "html", null, true);
                        echo "/";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "reference", array()), "html", null, true);
                        echo "/small/";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["media"]) ? $context["media"] : $this->getContext($context, "media")), $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "id", array()), array(), "array"), "fichier", array()), "html", null, true);
                        echo "\" alt=\"\" width=\"120\" height=\"120\"/>
                    </div>
                  </a><!-- /bienPhoto -->
                ";
                    } else {
                        // line 85
                        echo "                  <a href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_front_appartement_detail", array("url_bien" => $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "urlFr", array()))), "html", null, true);
                        echo "\" target=\"_blank\">
                    <div class=\"bienPhoto\">
                      <img src=\"";
                        // line 87
                        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images"), "html", null, true);
                        echo "/defaut_small.jpg\" alt=\"\" width=\"120\" height=\"120\"/>
                    </div>
                  </a><!-- /Photo par defaut -->
                ";
                    }
                    // line 91
                    echo "              ";
                }
                // line 92
                echo "
              <div class=\"DescriptionBiens\">
                ";
                // line 94
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "descriptionCourteFr", array()), "html", null, true);
                echo "<br/><br/>

                ";
                // line 96
                if ($this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "surfaceSol", array())) {
                    // line 97
                    echo "                  Surface : ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "surfaceSol", array()), "html", null, true);
                    echo "m<sup>2</sup>
                ";
                }
                // line 99
                echo "                <br/>

                ";
                // line 101
                if ($this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "etatInterieur", array())) {
                    // line 102
                    echo "                  État intérieur :
                  ";
                    // line 103
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable(range(0, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 104
                        echo "                    ";
                        if (($this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "etatInterieur", array()) <= (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")))) {
                            // line 105
                            echo "                      <img src=\"";
                            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/images/etoile_vide.png"), "html", null, true);
                            echo "\" alt=\"\"/>
                    ";
                        } else {
                            // line 107
                            echo "                      <img src=\"";
                            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/images/etoile_pleine.png"), "html", null, true);
                            echo "\" alt=\"*\"/>
                    ";
                        }
                        // line 109
                        echo "                  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 110
                    echo "                ";
                }
                // line 111
                echo "
                <br/>
              </div><!-- /DescriptionBiens -->

              <div class=\"bouton\">
                <a href=\"";
                // line 116
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_front_appartement_detail", array("url_bien" => $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "urlFr", array()))), "html", null, true);
                echo "\" class=\"boutonJ2\" target=\"_blank\">Voir</a>

                ";
                // line 118
                if (($this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "isResa", array()) == 1)) {
                    // line 119
                    echo "                  <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_select_appartement", array("id_appartement" => $this->getAttribute((isset($context["appart"]) ? $context["appart"] : $this->getContext($context, "appart")), "id", array()))), "html", null, true);
                    echo "\" class=\"boutonJ2\">Sélectionner</a>
                ";
                } else {
                    // line 121
                    echo "                  <span style=\"color:red; font-weight:bold\">Réservation indisponible </span>
                ";
                }
                // line 123
                echo "              </div><!-- /bouton -->
            </div><!-- /RecherchePixel1 -->
          </div><!-- /RecherchePixel -->
          <div id=\"RechercheBas\"></div><!-- /RechercheBas -->
        </div><!-- /ZoneRecherche -->

        <div style=\"float:left; width:100%; height:20px;\"></div>
        ";
                // line 130
                $context["i"] = ((isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")) + 1);
                // line 131
                echo "      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['appart'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 132
            echo "
      ";
            // line 133
            echo $this->env->getExtension('knp_pagination')->render((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")));
            echo "

    ";
        } else {
            // line 136
            echo "      <h2>Aucun bien ne correspond à votre recherche. </h2>
      <a href=\"";
            // line 137
            echo $this->env->getExtension('routing')->getPath("pat_admin_reservation_select_info_recherche");
            echo "\" class=\"boutonJ2\">Précédent</a>
    ";
        }
        // line 139
        echo "
  </div><!-- /contenuCentrale1 -->

";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:AdminReservation:selectAppartement_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  281 => 118,  197 => 83,  419 => 185,  308 => 130,  260 => 109,  228 => 96,  355 => 153,  346 => 150,  288 => 121,  692 => 459,  603 => 377,  561 => 347,  515 => 319,  475 => 300,  379 => 162,  348 => 227,  298 => 129,  1174 => 722,  1167 => 718,  1161 => 715,  1154 => 710,  1149 => 707,  1141 => 704,  1134 => 702,  1131 => 701,  1129 => 700,  1114 => 695,  1111 => 694,  1109 => 693,  1105 => 692,  1096 => 689,  1092 => 688,  1089 => 687,  1072 => 675,  1064 => 670,  1042 => 651,  1034 => 645,  1026 => 642,  1022 => 640,  1019 => 639,  1017 => 638,  1012 => 637,  1008 => 635,  1004 => 633,  1002 => 632,  996 => 629,  988 => 626,  984 => 624,  979 => 623,  977 => 622,  968 => 616,  962 => 613,  951 => 605,  917 => 586,  915 => 585,  912 => 584,  905 => 580,  897 => 575,  893 => 574,  881 => 568,  873 => 562,  871 => 561,  863 => 555,  853 => 496,  842 => 488,  831 => 480,  812 => 464,  800 => 456,  798 => 455,  787 => 447,  754 => 423,  747 => 418,  739 => 414,  731 => 410,  725 => 407,  718 => 404,  696 => 388,  681 => 378,  675 => 375,  665 => 369,  663 => 368,  651 => 359,  628 => 344,  606 => 334,  602 => 332,  600 => 331,  579 => 315,  572 => 311,  555 => 303,  542 => 295,  532 => 323,  513 => 282,  501 => 315,  499 => 271,  473 => 267,  441 => 245,  438 => 198,  416 => 231,  330 => 143,  289 => 133,  633 => 347,  627 => 291,  621 => 289,  619 => 288,  594 => 272,  576 => 263,  570 => 353,  562 => 255,  558 => 254,  552 => 251,  540 => 245,  508 => 228,  498 => 314,  486 => 308,  480 => 215,  454 => 201,  450 => 200,  410 => 265,  325 => 215,  220 => 93,  407 => 237,  402 => 234,  377 => 245,  313 => 172,  232 => 103,  465 => 197,  457 => 192,  417 => 270,  411 => 176,  408 => 225,  396 => 256,  391 => 168,  388 => 229,  372 => 160,  344 => 226,  332 => 220,  293 => 134,  274 => 124,  231 => 99,  200 => 82,  792 => 418,  788 => 416,  782 => 414,  776 => 439,  774 => 411,  762 => 402,  748 => 394,  742 => 391,  734 => 386,  730 => 385,  724 => 382,  716 => 403,  706 => 373,  698 => 389,  694 => 367,  688 => 364,  680 => 359,  676 => 358,  662 => 350,  658 => 349,  652 => 346,  644 => 341,  640 => 340,  634 => 337,  622 => 331,  616 => 386,  608 => 323,  598 => 273,  580 => 264,  564 => 297,  545 => 287,  541 => 286,  526 => 237,  507 => 317,  488 => 257,  462 => 259,  433 => 226,  424 => 235,  395 => 169,  382 => 168,  376 => 162,  341 => 188,  327 => 140,  320 => 153,  310 => 206,  291 => 118,  278 => 121,  259 => 109,  244 => 104,  448 => 250,  443 => 230,  429 => 259,  406 => 174,  366 => 206,  318 => 136,  282 => 119,  258 => 178,  222 => 93,  120 => 49,  272 => 115,  266 => 147,  226 => 100,  178 => 78,  111 => 44,  393 => 255,  386 => 166,  380 => 163,  362 => 157,  358 => 207,  342 => 139,  340 => 287,  334 => 284,  326 => 139,  319 => 138,  314 => 136,  299 => 163,  265 => 110,  252 => 107,  237 => 102,  194 => 93,  132 => 64,  23 => 3,  97 => 27,  81 => 35,  53 => 14,  654 => 223,  637 => 295,  632 => 206,  625 => 343,  623 => 342,  617 => 195,  612 => 337,  604 => 322,  593 => 327,  591 => 186,  586 => 365,  583 => 183,  578 => 180,  571 => 178,  557 => 177,  534 => 242,  522 => 236,  520 => 320,  504 => 316,  494 => 158,  463 => 145,  446 => 186,  440 => 136,  434 => 134,  431 => 240,  427 => 182,  405 => 176,  401 => 221,  397 => 114,  389 => 215,  381 => 210,  357 => 148,  349 => 193,  339 => 146,  303 => 131,  295 => 119,  287 => 121,  268 => 111,  74 => 29,  470 => 398,  452 => 236,  444 => 197,  435 => 405,  430 => 397,  414 => 183,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 887,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 864,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 825,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 808,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 763,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 732,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 697,  1387 => 694,  1378 => 688,  1374 => 687,  1368 => 684,  1359 => 678,  1355 => 677,  1349 => 674,  1340 => 668,  1336 => 667,  1330 => 664,  1321 => 658,  1317 => 657,  1311 => 654,  1302 => 648,  1298 => 647,  1292 => 644,  1283 => 638,  1279 => 637,  1273 => 634,  1264 => 628,  1260 => 627,  1254 => 624,  1245 => 618,  1241 => 617,  1235 => 614,  1226 => 608,  1222 => 607,  1216 => 604,  1207 => 598,  1203 => 597,  1197 => 594,  1188 => 588,  1184 => 587,  1178 => 584,  1169 => 578,  1165 => 577,  1159 => 574,  1150 => 568,  1146 => 567,  1140 => 564,  1123 => 699,  1119 => 697,  1113 => 546,  1104 => 540,  1100 => 690,  1094 => 536,  1085 => 686,  1081 => 529,  1075 => 526,  1066 => 671,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 599,  933 => 450,  929 => 588,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 569,  874 => 420,  870 => 419,  866 => 418,  860 => 510,  851 => 409,  847 => 408,  841 => 405,  832 => 399,  828 => 398,  822 => 395,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 431,  756 => 359,  752 => 395,  746 => 355,  737 => 413,  733 => 348,  727 => 408,  712 => 376,  708 => 332,  702 => 329,  693 => 323,  689 => 322,  683 => 379,  674 => 241,  670 => 355,  664 => 309,  649 => 297,  645 => 296,  639 => 350,  630 => 287,  626 => 332,  620 => 341,  611 => 277,  607 => 279,  601 => 376,  592 => 267,  588 => 269,  582 => 263,  563 => 348,  554 => 344,  550 => 246,  544 => 246,  535 => 283,  531 => 236,  516 => 233,  512 => 318,  506 => 163,  493 => 312,  478 => 253,  468 => 209,  459 => 144,  455 => 209,  449 => 193,  436 => 192,  428 => 278,  422 => 234,  409 => 180,  403 => 261,  390 => 230,  384 => 165,  351 => 152,  337 => 187,  311 => 118,  296 => 124,  256 => 108,  241 => 168,  215 => 91,  207 => 86,  192 => 80,  186 => 82,  175 => 80,  153 => 69,  118 => 48,  61 => 19,  34 => 4,  65 => 15,  77 => 32,  37 => 7,  190 => 77,  161 => 79,  137 => 65,  126 => 35,  261 => 106,  255 => 114,  247 => 105,  242 => 102,  214 => 92,  211 => 91,  191 => 85,  157 => 37,  145 => 71,  127 => 54,  373 => 111,  367 => 159,  363 => 155,  359 => 154,  354 => 152,  343 => 147,  335 => 145,  328 => 152,  322 => 138,  315 => 170,  309 => 132,  305 => 115,  302 => 267,  290 => 157,  284 => 121,  279 => 119,  271 => 113,  264 => 120,  248 => 103,  236 => 104,  223 => 96,  170 => 78,  110 => 40,  96 => 38,  84 => 32,  472 => 210,  467 => 148,  375 => 161,  371 => 160,  360 => 183,  356 => 232,  353 => 194,  350 => 151,  338 => 145,  336 => 221,  331 => 141,  321 => 137,  316 => 148,  307 => 142,  304 => 268,  301 => 130,  297 => 131,  292 => 123,  286 => 116,  283 => 130,  277 => 116,  275 => 116,  270 => 123,  263 => 112,  257 => 146,  253 => 107,  249 => 135,  245 => 110,  233 => 92,  225 => 97,  216 => 90,  206 => 149,  202 => 78,  198 => 85,  185 => 95,  180 => 82,  177 => 75,  165 => 65,  150 => 53,  124 => 61,  113 => 51,  100 => 39,  58 => 15,  251 => 113,  234 => 163,  213 => 113,  195 => 84,  174 => 78,  167 => 82,  146 => 59,  140 => 58,  128 => 62,  104 => 43,  90 => 24,  83 => 36,  52 => 22,  596 => 225,  590 => 314,  585 => 221,  577 => 357,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 210,  551 => 176,  546 => 207,  543 => 206,  539 => 205,  529 => 322,  525 => 173,  523 => 321,  518 => 193,  514 => 192,  509 => 189,  503 => 266,  500 => 160,  497 => 263,  495 => 313,  492 => 157,  490 => 219,  487 => 213,  484 => 256,  482 => 177,  479 => 176,  477 => 401,  474 => 206,  471 => 173,  469 => 172,  466 => 146,  464 => 292,  461 => 169,  458 => 239,  456 => 167,  451 => 189,  445 => 160,  442 => 199,  439 => 229,  437 => 262,  432 => 191,  426 => 188,  423 => 186,  420 => 219,  418 => 251,  413 => 172,  399 => 237,  394 => 162,  378 => 162,  370 => 159,  368 => 203,  365 => 212,  361 => 199,  347 => 148,  345 => 94,  333 => 131,  329 => 179,  323 => 139,  317 => 272,  312 => 133,  306 => 205,  300 => 110,  294 => 199,  285 => 193,  280 => 118,  276 => 116,  267 => 111,  250 => 107,  239 => 100,  229 => 116,  218 => 94,  212 => 88,  210 => 97,  205 => 83,  188 => 91,  184 => 75,  181 => 79,  169 => 87,  160 => 73,  152 => 61,  148 => 60,  139 => 57,  134 => 56,  114 => 45,  107 => 76,  76 => 27,  70 => 18,  273 => 127,  269 => 94,  254 => 139,  246 => 103,  243 => 88,  240 => 103,  238 => 100,  235 => 101,  230 => 127,  227 => 99,  224 => 94,  221 => 96,  219 => 112,  217 => 84,  208 => 91,  204 => 87,  179 => 71,  159 => 66,  143 => 58,  135 => 56,  131 => 87,  108 => 44,  102 => 26,  71 => 28,  67 => 29,  63 => 23,  59 => 15,  47 => 8,  94 => 25,  89 => 40,  85 => 33,  79 => 21,  75 => 32,  68 => 25,  56 => 19,  50 => 13,  38 => 5,  29 => 9,  87 => 38,  72 => 26,  55 => 10,  21 => 2,  26 => 2,  35 => 4,  31 => 5,  41 => 10,  28 => 2,  201 => 145,  196 => 81,  183 => 81,  171 => 77,  166 => 76,  163 => 75,  156 => 71,  151 => 69,  142 => 63,  138 => 44,  136 => 64,  123 => 50,  121 => 57,  115 => 54,  105 => 32,  101 => 44,  91 => 36,  69 => 30,  66 => 23,  62 => 14,  49 => 23,  98 => 40,  93 => 41,  88 => 33,  78 => 20,  46 => 15,  44 => 12,  32 => 3,  27 => 4,  43 => 7,  40 => 11,  25 => 4,  24 => 2,  172 => 85,  158 => 58,  155 => 77,  129 => 53,  119 => 49,  117 => 36,  20 => 1,  22 => 2,  19 => 1,  209 => 87,  203 => 95,  199 => 94,  193 => 76,  189 => 81,  187 => 81,  182 => 89,  176 => 77,  173 => 89,  168 => 72,  164 => 67,  162 => 85,  154 => 54,  149 => 68,  147 => 68,  144 => 59,  141 => 67,  133 => 48,  130 => 63,  125 => 55,  122 => 82,  116 => 46,  112 => 45,  109 => 51,  106 => 31,  103 => 43,  99 => 42,  95 => 43,  92 => 40,  86 => 32,  82 => 27,  80 => 31,  73 => 19,  64 => 25,  60 => 20,  57 => 17,  54 => 23,  51 => 9,  48 => 9,  45 => 13,  42 => 14,  39 => 16,  36 => 7,  33 => 6,  30 => 3,);
    }
}
