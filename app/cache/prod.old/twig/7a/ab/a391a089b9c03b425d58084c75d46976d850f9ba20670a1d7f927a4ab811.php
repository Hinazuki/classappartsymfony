<?php

/* PatCompteBundle:AdminProprietaire:ajouter_content.html.twig */
class __TwigTemplate_7aaba391a089b9c03b425d58084c75d46976d850f9ba20670a1d7f927a4ab811 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "
  <div id=\"contenuCentrale1\">
    <h1>Ajouter un compte propriétaire</h1>

    <div class=\"clear\"></div>

    ";
        // line 8
        if ((isset($context["message"]) ? $context["message"] : $this->getContext($context, "message"))) {
            // line 9
            echo "      <div class=\"alert-success\">
        <p>";
            // line 10
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
            echo "</p>
      </div>
    ";
        }
        // line 13
        echo "
    ";
        // line 14
        $context["__internal_0b9a578ed6176ab34809ccb429e144180c8aa7835da44eb36930bde04d396f77"] = $this->env->loadTemplate("MopaBootstrapBundle::flash.html.twig");
        // line 15
        echo "    ";
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "peekAll", array())) > 0)) {
            // line 16
            echo "      <div class=\"row\">
        <div class=\"span12\">
          ";
            // line 18
            echo $context["__internal_0b9a578ed6176ab34809ccb429e144180c8aa7835da44eb36930bde04d396f77"]->getsession_flash();
            echo "
        </div>
      </div>
    ";
        }
        // line 22
        echo "
    ";
        // line 23
        if (((isset($context["id_proprio"]) ? $context["id_proprio"] : $this->getContext($context, "id_proprio")) > 0)) {
            // line 24
            echo "      <form action=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_proprietaire_editer", array("id_proprio" => (isset($context["id_proprio"]) ? $context["id_proprio"] : $this->getContext($context, "id_proprio")))), "html", null, true);
            echo "\" ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
            echo " method=\"POST\" class=\"fos_user_registration_register\">
      ";
        } else {
            // line 26
            echo "        <form action=\"";
            echo $this->env->getExtension('routing')->getPath("pat_admin_proprietaire_ajouter");
            echo "\" ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
            echo " method=\"POST\" class=\"fos_user_registration_register\">
        ";
        }
        // line 28
        echo "
        <div class=\"alert-error\">
          ";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
        </div>

        <div class=\"blocForm speciForm\">
          <div class=\"content\">

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "civilite", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "civilite", array()), 'errors');
        echo "
                ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "civilite", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'errors');
        echo "
                ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'errors');
        echo "
                ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'widget');
        echo "\t</div>\t</div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "societe", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "societe", array()), 'errors');
        echo "
                ";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "societe", array()), 'widget');
        echo "\t</div>\t</div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "siret", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "siret", array()), 'errors');
        echo "
                ";
        // line 71
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "siret", array()), 'widget');
        echo "\t</div>\t</div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fonction", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 76
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fonction", array()), 'errors');
        echo "
                ";
        // line 77
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fonction", array()), 'widget');
        echo "\t</div>\t</div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_naissance", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_naissance", array()), 'errors');
        echo "
                ";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_naissance", array()), 'widget');
        echo "\t</div>\t</div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nationalite", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 88
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nationalite", array()), 'errors');
        echo "
                ";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nationalite", array()), 'widget');
        echo "\t</div>\t</div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 92
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "langue_parle", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "langue_parle", array()), 'errors');
        echo "
                ";
        // line 95
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "langue_parle", array()), 'widget');
        echo "</div></div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 98
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 100
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'errors');
        echo "
                ";
        // line 101
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'widget');
        echo "\t</div>\t</div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 104
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 106
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2", array()), 'errors');
        echo "
                ";
        // line 107
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2", array()), 'widget');
        echo "\t</div>\t</div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 110
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "code_postal", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 112
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "code_postal", array()), 'errors');
        echo "
                ";
        // line 113
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "code_postal", array()), 'widget');
        echo "</div>\t</div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 116
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 118
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'errors');
        echo "
                ";
        // line 119
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'widget');
        echo "</div>\t</div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 122
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 124
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays", array()), 'errors');
        echo "
                ";
        // line 125
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays", array()), 'widget');
        echo "\t</div>\t</div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 128
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "telephone", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 130
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "telephone", array()), 'errors');
        echo "
                ";
        // line 131
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "telephone", array()), 'widget');
        echo "</div>\t</div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 135
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobile", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 138
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobile", array()), 'errors');
        echo "
                ";
        // line 139
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobile", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 145
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobile2", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 148
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobile2", array()), 'errors');
        echo "
                ";
        // line 149
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobile2", array()), 'widget');
        echo "
              </div>
            </div>

            ";
        // line 159
        echo "            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 160
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "moyen_contact", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">    ";
        // line 162
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "moyen_contact", array()), 'errors');
        echo "
                ";
        // line 163
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "moyen_contact", array()), 'widget');
        echo "\t</div>\t</div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 166
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 168
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'errors');
        echo "
                ";
        // line 169
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget');
        echo "\t</div>\t</div>

            <div class=\"row-fluid\"><div class=\"span4\">
                <strong>";
        // line 172
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "second_email", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">\t";
        // line 174
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "second_email", array()), 'errors');
        echo "
                ";
        // line 175
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "second_email", array()), 'widget');
        echo "\t</div>\t</div>

          </div>
        </div>


        <div class=\"blocForm\">
          <div class=\"title\">
            Informations bancaire
          </div>
          <div class=\"content\">
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 188
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_etab", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 191
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_etab", array()), 'errors');
        echo "
                ";
        // line 192
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_etab", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 197
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_code", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 200
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_code", array()), 'errors');
        echo "
                ";
        // line 201
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_code", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 206
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_compte", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 209
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_compte", array()), 'errors');
        echo "
                ";
        // line 210
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_compte", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 215
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_rice", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 218
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_rice", array()), 'errors');
        echo "
                ";
        // line 219
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_rice", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 224
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_domiciliation", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 227
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_domiciliation", array()), 'errors');
        echo "
                ";
        // line 228
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_domiciliation", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 233
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_iban", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 236
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_iban", array()), 'errors');
        echo "
                ";
        // line 237
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_iban", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 242
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_bic", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 245
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_bic", array()), 'errors');
        echo "
                ";
        // line 246
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_bic", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 251
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_agence_resp", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 254
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_agence_resp", array()), 'errors');
        echo "
                ";
        // line 255
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_agence_resp", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 260
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_pays", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 263
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_pays", array()), 'errors');
        echo "
                ";
        // line 264
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_pays", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 269
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_titulaire", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 272
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_titulaire", array()), 'errors');
        echo "
                ";
        // line 273
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_titulaire", array()), 'widget');
        echo "
              </div>
            </div>
          </div>
        </div>

        ";
        // line 279
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "

        <div class=\"row-fluid\">
          <small>Après validation du formulaire, un mot de passe sera généré et un mail sera envoyé au locataire contenant ses informations de connexion.</small>
        </div>
        <br/>

        <div class=\"row-fluid\">
          <div class=\"span6\">
            ";
        // line 288
        if (((isset($context["id_proprio"]) ? $context["id_proprio"] : $this->getContext($context, "id_proprio")) > 0)) {
            // line 289
            echo "              <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_proprietaire_afficher", array("id_proprio" => (isset($context["id_proprio"]) ? $context["id_proprio"] : $this->getContext($context, "id_proprio")))), "html", null, true);
            echo "\" class=\"btn btn-large\">Revenir</a>
            ";
        } else {
            // line 291
            echo "              <a href=\"";
            echo $this->env->getExtension('routing')->getPath("pat_admin_dashboard");
            echo "\" class=\"btn btn-large\">Annuler</a>
            ";
        }
        // line 293
        echo "          </div>
          <div class=\"span6 right\">
            <input type=\"submit\" class=\"btn btn-primary btn-large\" value=\"";
        // line 295
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
          </div>
        </div>

      </form>

  </div><!-- /contenuCentrale1 -->

";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:AdminProprietaire:ajouter_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  633 => 293,  627 => 291,  621 => 289,  619 => 288,  594 => 272,  576 => 263,  570 => 260,  562 => 255,  558 => 254,  552 => 251,  540 => 245,  508 => 228,  498 => 224,  486 => 218,  480 => 215,  454 => 201,  450 => 200,  410 => 175,  325 => 131,  220 => 89,  407 => 237,  402 => 234,  377 => 221,  313 => 169,  232 => 107,  465 => 197,  457 => 192,  417 => 178,  411 => 176,  408 => 175,  396 => 232,  391 => 168,  388 => 229,  372 => 160,  344 => 149,  332 => 135,  293 => 130,  274 => 120,  231 => 94,  200 => 83,  792 => 418,  788 => 416,  782 => 414,  776 => 412,  774 => 411,  762 => 402,  748 => 394,  742 => 391,  734 => 386,  730 => 385,  724 => 382,  716 => 377,  706 => 373,  698 => 368,  694 => 367,  688 => 364,  680 => 359,  676 => 358,  662 => 350,  658 => 349,  652 => 346,  644 => 341,  640 => 340,  634 => 337,  622 => 331,  616 => 328,  608 => 323,  598 => 273,  580 => 264,  564 => 297,  545 => 287,  541 => 286,  526 => 237,  507 => 267,  488 => 257,  462 => 206,  433 => 226,  424 => 220,  395 => 169,  382 => 199,  376 => 162,  341 => 173,  327 => 167,  320 => 153,  310 => 125,  291 => 118,  278 => 121,  259 => 122,  244 => 113,  448 => 267,  443 => 230,  429 => 259,  406 => 174,  366 => 206,  318 => 175,  282 => 147,  258 => 129,  222 => 91,  120 => 44,  272 => 129,  266 => 101,  226 => 92,  178 => 71,  111 => 27,  393 => 305,  386 => 166,  380 => 163,  362 => 294,  358 => 207,  342 => 139,  340 => 287,  334 => 284,  326 => 278,  319 => 273,  314 => 271,  299 => 266,  265 => 107,  252 => 137,  237 => 128,  194 => 103,  132 => 48,  23 => 3,  97 => 31,  81 => 25,  53 => 16,  654 => 223,  637 => 295,  632 => 206,  625 => 200,  623 => 199,  617 => 195,  612 => 192,  604 => 322,  593 => 187,  591 => 186,  586 => 313,  583 => 183,  578 => 180,  571 => 178,  557 => 177,  534 => 242,  522 => 236,  520 => 171,  504 => 227,  494 => 158,  463 => 145,  446 => 186,  440 => 136,  434 => 134,  431 => 260,  427 => 182,  405 => 210,  401 => 172,  397 => 114,  389 => 113,  381 => 112,  357 => 148,  349 => 194,  339 => 105,  303 => 99,  295 => 119,  287 => 127,  268 => 117,  74 => 23,  470 => 398,  452 => 236,  444 => 197,  435 => 405,  430 => 397,  414 => 241,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 887,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 864,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 825,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 808,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 763,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 732,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 697,  1387 => 694,  1378 => 688,  1374 => 687,  1368 => 684,  1359 => 678,  1355 => 677,  1349 => 674,  1340 => 668,  1336 => 667,  1330 => 664,  1321 => 658,  1317 => 657,  1311 => 654,  1302 => 648,  1298 => 647,  1292 => 644,  1283 => 638,  1279 => 637,  1273 => 634,  1264 => 628,  1260 => 627,  1254 => 624,  1245 => 618,  1241 => 617,  1235 => 614,  1226 => 608,  1222 => 607,  1216 => 604,  1207 => 598,  1203 => 597,  1197 => 594,  1188 => 588,  1184 => 587,  1178 => 584,  1169 => 578,  1165 => 577,  1159 => 574,  1150 => 568,  1146 => 567,  1140 => 564,  1123 => 550,  1119 => 549,  1113 => 546,  1104 => 540,  1100 => 539,  1094 => 536,  1085 => 530,  1081 => 529,  1075 => 526,  1066 => 520,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 456,  933 => 450,  929 => 449,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 426,  874 => 420,  870 => 419,  866 => 418,  860 => 415,  851 => 409,  847 => 408,  841 => 405,  832 => 399,  828 => 398,  822 => 395,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 365,  756 => 359,  752 => 395,  746 => 355,  737 => 349,  733 => 348,  727 => 345,  712 => 376,  708 => 332,  702 => 329,  693 => 323,  689 => 322,  683 => 319,  674 => 241,  670 => 355,  664 => 309,  649 => 297,  645 => 296,  639 => 293,  630 => 287,  626 => 332,  620 => 283,  611 => 277,  607 => 279,  601 => 189,  592 => 267,  588 => 269,  582 => 263,  563 => 253,  554 => 293,  550 => 246,  544 => 246,  535 => 283,  531 => 236,  516 => 233,  512 => 165,  506 => 163,  493 => 216,  478 => 253,  468 => 209,  459 => 144,  455 => 271,  449 => 193,  436 => 192,  428 => 181,  422 => 395,  409 => 117,  403 => 168,  390 => 230,  384 => 165,  351 => 145,  337 => 185,  311 => 118,  296 => 109,  256 => 104,  241 => 98,  215 => 99,  207 => 66,  192 => 82,  186 => 76,  175 => 71,  153 => 65,  118 => 47,  61 => 24,  34 => 8,  65 => 17,  77 => 26,  37 => 7,  190 => 77,  161 => 60,  137 => 58,  126 => 52,  261 => 106,  255 => 135,  247 => 130,  242 => 127,  214 => 87,  211 => 86,  191 => 76,  157 => 62,  145 => 59,  127 => 50,  373 => 111,  367 => 173,  363 => 155,  359 => 171,  354 => 153,  343 => 135,  335 => 104,  328 => 152,  322 => 175,  315 => 170,  309 => 140,  305 => 115,  302 => 267,  290 => 157,  284 => 154,  279 => 129,  271 => 110,  264 => 122,  248 => 118,  236 => 77,  223 => 82,  170 => 68,  110 => 42,  96 => 44,  84 => 32,  472 => 210,  467 => 148,  375 => 152,  371 => 160,  360 => 183,  356 => 182,  353 => 99,  350 => 151,  338 => 138,  336 => 146,  331 => 169,  321 => 130,  316 => 128,  307 => 269,  304 => 268,  301 => 122,  297 => 131,  292 => 110,  286 => 116,  283 => 105,  277 => 104,  275 => 103,  270 => 138,  263 => 123,  257 => 121,  253 => 108,  249 => 21,  245 => 92,  233 => 88,  225 => 103,  216 => 88,  206 => 87,  202 => 92,  198 => 20,  185 => 75,  180 => 73,  177 => 79,  165 => 65,  150 => 58,  124 => 50,  113 => 38,  100 => 38,  58 => 15,  251 => 93,  234 => 111,  213 => 113,  195 => 78,  174 => 66,  167 => 68,  146 => 63,  140 => 54,  128 => 51,  104 => 42,  90 => 30,  83 => 26,  52 => 14,  596 => 225,  590 => 314,  585 => 221,  577 => 218,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 210,  551 => 176,  546 => 207,  543 => 206,  539 => 205,  529 => 174,  525 => 173,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 266,  500 => 160,  497 => 263,  495 => 182,  492 => 157,  490 => 219,  487 => 213,  484 => 256,  482 => 177,  479 => 176,  477 => 401,  474 => 206,  471 => 173,  469 => 172,  466 => 146,  464 => 397,  461 => 169,  458 => 239,  456 => 167,  451 => 189,  445 => 160,  442 => 159,  439 => 229,  437 => 262,  432 => 191,  426 => 188,  423 => 180,  420 => 219,  418 => 251,  413 => 172,  399 => 237,  394 => 162,  378 => 162,  370 => 159,  368 => 159,  365 => 212,  361 => 149,  347 => 136,  345 => 94,  333 => 131,  329 => 179,  323 => 102,  317 => 272,  312 => 114,  306 => 124,  300 => 110,  294 => 156,  285 => 105,  280 => 113,  276 => 112,  267 => 60,  250 => 101,  239 => 91,  229 => 116,  218 => 101,  212 => 87,  210 => 97,  205 => 83,  188 => 59,  184 => 99,  181 => 74,  169 => 65,  160 => 65,  152 => 63,  148 => 53,  139 => 50,  134 => 49,  114 => 42,  107 => 34,  76 => 23,  70 => 17,  273 => 127,  269 => 94,  254 => 92,  246 => 100,  243 => 88,  240 => 101,  238 => 113,  235 => 95,  230 => 111,  227 => 99,  224 => 103,  221 => 96,  219 => 112,  217 => 84,  208 => 86,  204 => 91,  179 => 71,  159 => 61,  143 => 55,  135 => 57,  131 => 44,  108 => 30,  102 => 33,  71 => 21,  67 => 23,  63 => 16,  59 => 15,  47 => 12,  94 => 40,  89 => 30,  85 => 28,  79 => 24,  75 => 22,  68 => 18,  56 => 15,  50 => 15,  38 => 14,  29 => 4,  87 => 30,  72 => 23,  55 => 14,  21 => 2,  26 => 2,  35 => 6,  31 => 3,  41 => 11,  28 => 2,  201 => 82,  196 => 80,  183 => 72,  171 => 70,  166 => 68,  163 => 52,  156 => 64,  151 => 62,  142 => 50,  138 => 55,  136 => 56,  123 => 39,  121 => 48,  115 => 36,  105 => 40,  101 => 39,  91 => 25,  69 => 24,  66 => 16,  62 => 16,  49 => 13,  98 => 32,  93 => 31,  88 => 33,  78 => 20,  46 => 12,  44 => 12,  32 => 5,  27 => 4,  43 => 11,  40 => 11,  25 => 4,  24 => 2,  172 => 106,  158 => 59,  155 => 48,  129 => 47,  119 => 47,  117 => 43,  20 => 1,  22 => 103,  19 => 1,  209 => 87,  203 => 109,  199 => 79,  193 => 76,  189 => 76,  187 => 74,  182 => 68,  176 => 67,  173 => 69,  168 => 73,  164 => 72,  162 => 64,  154 => 59,  149 => 63,  147 => 119,  144 => 56,  141 => 58,  133 => 52,  130 => 46,  125 => 50,  122 => 41,  116 => 28,  112 => 42,  109 => 47,  106 => 41,  103 => 36,  99 => 30,  95 => 30,  92 => 34,  86 => 29,  82 => 26,  80 => 31,  73 => 22,  64 => 22,  60 => 16,  57 => 18,  54 => 14,  51 => 13,  48 => 14,  45 => 13,  42 => 11,  39 => 10,  36 => 9,  33 => 9,  30 => 4,);
    }
}
