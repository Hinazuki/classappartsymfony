<?php

/* PatCompteBundle:Appartement:ajouter_content.html.twig */
class __TwigTemplate_32ab22cf26c1f650f4a8ae560cab44e5962556dc426d40a12f827c93d843bba4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "
  <div id=\"contenuCentrale\">
    ";
        // line 4
        if ((isset($context["validation"]) ? $context["validation"] : $this->getContext($context, "validation"))) {
            // line 5
            echo "      ";
            // line 6
            echo "        <h1>Demande de Validation</h1>
        <p style=\"font-size:14px;\">";
            // line 7
            echo (isset($context["validation"]) ? $context["validation"] : $this->getContext($context, "validation"));
            echo "</p>
      ";
            // line 9
            echo "      <br/><br/>
      <a href=\"";
            // line 10
            echo $this->env->getExtension('routing')->getPath("pat_appartement_index");
            echo "\" class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Revenir à la liste des biens"), "html", null, true);
            echo "</a>
    ";
        } else {
            // line 12
            echo "
      <h1>";
            // line 13
            if ((isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))) {
                echo "Modifier votre bien <small>(Réf : ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "reference", array()), "html", null, true);
                echo ")</small>";
            } else {
                echo "Ajouter un bien";
            }
            echo "</h1>

      <div class=\"etapesBien\">
        <div class=\"linksEtapesBien\">
          ";
            // line 17
            if ((isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))) {
                // line 18
                echo "            <ul>
              <li><a class=\"active\" href=\"#\">Description</a></li>
              <li><a href=\"";
                // line 20
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_tarif_ajouter", array("id_bien" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
                echo "\">Loyers</a></li>
              <li><a href=\"";
                // line 21
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_piece_index", array("id" => (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")))), "html", null, true);
                echo "\">Pièces</a></li>
            </ul>
          ";
            } else {
                // line 24
                echo "            <ul>
              <li><a class=\"active\" href=\"#\">Description</a></li>
              <li><a href=\"#\">Tarifs</a></li>
              <li><a href=\"#\">Pièces</a></li>
              <li><a href=\"#\">Images</a></li>
            </ul>
          ";
            }
            // line 31
            echo "        </div>
      </div>


      ";
            // line 35
            if ((isset($context["message"]) ? $context["message"] : $this->getContext($context, "message"))) {
                // line 36
                echo "        <div class=\"alert alert-success\">
          ";
                // line 37
                echo nl2br(twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true));
                echo "
        </div>
      ";
            }
            // line 40
            echo "
      ";
            // line 41
            if (((isset($context["erreur"]) ? $context["erreur"] : $this->getContext($context, "erreur")) == 1)) {
                // line 42
                echo "        <div class=\"alert alert-danger\">
          Vous devez accepter les conditions du mandat de commercialisation de Class Appart
        </div>
      ";
            }
            // line 46
            echo "

      <div class=\"formBienAdministration\">
        <h3>Administration :</h3>
        ";
            // line 50
            if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "createdBy", array())) {
                // line 51
                echo "          Création :
          ";
                // line 52
                if (($this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "createdBy", array()), "typeUtilisateur", array()) == 9)) {
                    // line 53
                    echo "            Administrateur
          ";
                } elseif (($this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "createdBy", array()), "typeUtilisateur", array()) == 2)) {
                    // line 55
                    echo "            Propriétaire
          ";
                }
                // line 57
                echo "          ";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "createdBy", array()), "prenom", array())), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "createdBy", array()), "Nom", array())), "html", null, true);
                echo " le ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "createdAt", array()), "d/m/Y H:i:s"), "html", null, true);
                echo "
          <br/>
        ";
            }
            // line 60
            echo "
        ";
            // line 61
            if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "updatedBy", array())) {
                // line 62
                echo "          Dernière modification :
          ";
                // line 63
                if (($this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "updatedBy", array()), "typeUtilisateur", array()) == 9)) {
                    // line 64
                    echo "            Administrateur
          ";
                } elseif (($this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "updatedBy", array()), "typeUtilisateur", array()) == 2)) {
                    // line 66
                    echo "            Propriétaire
          ";
                }
                // line 68
                echo "          ";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "updatedBy", array()), "prenom", array())), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "updatedBy", array()), "Nom", array())), "html", null, true);
                echo " le ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "updatedAt", array()), "d/m/Y H:i:s"), "html", null, true);
                echo "
          <br/>
        ";
            }
            // line 71
            echo "      </div>

      <div class=\"formBien\">
        <form action=\"\" method=\"post\" ";
            // line 74
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
            echo ">

          <div class=\"error\">
            ";
            // line 77
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
            echo "
          </div>

          <div class=\"blocForm active\">
            <div class=\"title\">Type de bien</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                <div class=\"span4\">
                  <strong>Changer le type du Bien* : </strong>
                </div>
                <div class=\"span8\">
                  ";
            // line 89
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "type", array()), 'errors');
            echo "
                  ";
            // line 90
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "type", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  <strong>Nombre de pièces du Bien* : </strong>
                </div>
                <div class=\"span8\">
                  ";
            // line 99
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_pieces", array()), 'errors');
            echo "
                  ";
            // line 100
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_pieces", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  <strong>";
            // line 106
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_meuble", array()), 'label');
            echo "</strong>
                </div>
                <div class=\"span8\">
                  ";
            // line 109
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_meuble", array()), 'errors');
            echo "
                  ";
            // line 110
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "is_meuble", array()), 'widget');
            echo "
                </div>
              </div>
            </div>
          </div>

          <div class=\"blocForm active\">
            <div class=\"title\">Description du bien</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                ";
            // line 121
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description_fr", array()), 'label');
            echo "<br/>
                ";
            // line 122
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description_fr", array()), 'errors');
            echo "
                ";
            // line 123
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description_fr", array()), 'widget', array("attr" => array("class" => "span12", "rows" => "5")));
            echo "
              </div>
              <div class=\"row-fluid\">
                ";
            // line 126
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description_courte_fr", array()), 'label');
            echo "<br/>
                ";
            // line 127
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description_courte_fr", array()), 'errors');
            echo "
                ";
            // line 128
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description_courte_fr", array()), 'widget', array("attr" => array("class" => "span12")));
            echo "
              </div>
            </div>
          </div>

          <div class=\"blocForm\">
            <div class=\"title\">Localisation*</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 139
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom_residence", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 142
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom_residence", array()), 'errors');
            echo "
                  ";
            // line 143
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom_residence", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 149
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 152
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'errors');
            echo "
                  ";
            // line 153
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 159
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 162
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2", array()), 'errors');
            echo "
                  ";
            // line 163
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 169
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 172
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'errors');
            echo "
                  ";
            // line 173
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'widget');
            echo "
                  <input type=\"hidden\" id=\"url_ville_search\" value=\"";
            // line 174
            echo $this->env->getExtension('routing')->getPath("pat_front_contenu_all_ville");
            echo "\">
                  <!-- affichage des résultas des villes ajax -->
                  <div id=\"resultats_recherche_villes_ajax\">
                    <div id=\"resultats_recherche_villes\"></div>
                  </div>
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 184
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_aeroport", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 187
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_aeroport", array()), 'errors');
            echo "
                  ";
            // line 188
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_aeroport", array()), 'widget');
            echo " km
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 194
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_autoroute", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 197
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_autoroute", array()), 'errors');
            echo "
                  ";
            // line 198
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_autoroute", array()), 'widget');
            echo " km
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 204
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_voie_rapide", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 207
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_voie_rapide", array()), 'errors');
            echo "
                  ";
            // line 208
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_voie_rapide", array()), 'widget');
            echo " km
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 214
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_commerce", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 217
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_commerce", array()), 'errors');
            echo "
                  ";
            // line 218
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "distance_commerce", array()), 'widget');
            echo " km
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 224
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_gare", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 227
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_gare", array()), 'errors');
            echo "
                  ";
            // line 228
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_gare", array()), 'widget');
            echo " min
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 234
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_marche", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 237
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_marche", array()), 'errors');
            echo "
                  ";
            // line 238
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_marche", array()), 'widget');
            echo " min
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 244
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_bus", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 247
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_bus", array()), 'errors');
            echo "
                  ";
            // line 248
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_bus", array()), 'widget');
            echo " min
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 254
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_tram", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 257
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_tram", array()), 'errors');
            echo "
                  ";
            // line 258
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_tram", array()), 'widget');
            echo " min
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 264
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 267
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram", array()), 'errors');
            echo "
                  ";
            // line 268
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 274
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram2", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 277
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram2", array()), 'errors');
            echo "
                  ";
            // line 278
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram2", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 284
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram3", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 287
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram3", array()), 'errors');
            echo "
                  ";
            // line 288
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "arret_tram3", array()), 'widget');
            echo "
                </div>
              </div>
            </div>
          </div>

          <div class=\"blocForm\">
            <div class=\"title\">Surface*</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 300
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_sol", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 303
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_sol", array()), 'errors');
            echo "
                  ";
            // line 304
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_sol", array()), 'widget');
            echo " m<sup>2</sup>
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 310
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_carrez", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 313
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_carrez", array()), 'errors');
            echo "
                  ";
            // line 314
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_carrez", array()), 'widget');
            echo " m<sup>2</sup> <i>(mesurage loi Carrez-DPE)</i>
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 320
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_terrain", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 323
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_terrain", array()), 'errors');
            echo "
                  ";
            // line 324
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "surface_terrain", array()), 'widget');
            echo " m<sup>2</sup> environ
                </div>
              </div>
            </div>
          </div>

          <div class=\"blocForm\">
            <div class=\"title\">Prestations*</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 336
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "entree_immeuble", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 339
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "entree_immeuble", array()), 'errors');
            echo "
                  ";
            // line 340
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "entree_immeuble", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 346
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "serrure", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 349
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "serrure", array()), 'errors');
            echo "
                  ";
            // line 350
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "serrure", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 356
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_personne", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 359
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_personne", array()), 'errors');
            echo "
                  ";
            // line 360
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_personne", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 366
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_simple", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 369
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_simple", array()), 'errors');
            echo "
                  ";
            // line 370
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_simple", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 376
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_double", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 379
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_double", array()), 'errors');
            echo "
                  ";
            // line 380
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_double", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 386
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_simple_double", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 389
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_simple_double", array()), 'errors');
            echo "
                  ";
            // line 390
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_lit_simple_double", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 396
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_canape_convertible", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 399
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_canape_convertible", array()), 'errors');
            echo "
                  ";
            // line 400
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_canape_convertible", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 406
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "etage", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 409
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "etage", array()), 'errors');
            echo "
                  ";
            // line 410
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_etage", array()), 'errors');
            echo "
                  ";
            // line 411
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "etage", array()), 'widget');
            echo "<br/>sur ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nb_etage", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 417
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cave", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 420
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cave", array()), 'errors');
            echo "
                  ";
            // line 421
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cave", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 427
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parking", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 430
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parking", array()), 'errors');
            echo "
                  ";
            // line 431
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parking", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 437
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "stationnement", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 440
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "stationnement", array()), 'errors');
            echo "
                  ";
            // line 441
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "stationnement", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 447
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "terrasse", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 450
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "terrasse", array()), 'errors');
            echo "
                  ";
            // line 451
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "terrasse", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 457
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "balcon", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 460
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "balcon", array()), 'errors');
            echo "
                  ";
            // line 461
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "balcon", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 467
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "garage", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 470
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "garage", array()), 'errors');
            echo "
                  ";
            // line 471
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "garage", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 477
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "escalier", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 480
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "escalier", array()), 'errors');
            echo "
                  ";
            // line 481
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "escalier", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 487
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ascenseur", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 490
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ascenseur", array()), 'errors');
            echo "
                  ";
            // line 491
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ascenseur", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 497
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "grenier", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 500
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "grenier", array()), 'errors');
            echo "
                  ";
            // line 501
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "grenier", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 507
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "jardin", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 510
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "jardin", array()), 'errors');
            echo "
                  ";
            // line 511
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "jardin", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 517
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vide_ordure", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 520
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vide_ordure", array()), 'errors');
            echo "
                  ";
            // line 521
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vide_ordure", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 527
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "interphone", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 530
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "interphone", array()), 'errors');
            echo "
                  ";
            // line 531
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "interphone", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 537
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "has_digicode", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 540
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "has_digicode", array()), 'errors');
            echo "
                  ";
            // line 541
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "has_digicode", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid has_digicod\">
                <div class=\"span4\">
                </div>
                <div class=\"span8\">
                  ";
            // line 549
            if (((isset($context["has_digicode"]) ? $context["has_digicode"] : $this->getContext($context, "has_digicode")) == true)) {
                // line 550
                echo "                    <div class=\"info\">Pour modifier votre digicode, saisissez le nouveau dans les champs ci-dessous</div>
                  ";
            }
            // line 552
            echo "                  <small>Ces informations ne seront visibles que par nos services et ne seront pas diffusées sur internet</small>
                </div>
              </div>

              <div class=\"row-fluid has_digicod\">
                <div class=\"span4\">
                  ";
            // line 558
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "digicode", array()), "first", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 561
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "digicode", array()), "first", array()), 'errors');
            echo "
                  ";
            // line 562
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "digicode", array()), "first", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid has_digicod\">
                <div class=\"span4\">
                  ";
            // line 568
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "digicode", array()), "second", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 571
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "digicode", array()), "second", array()), 'errors');
            echo "
                  ";
            // line 572
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "digicode", array()), "second", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 578
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_handicapes", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 581
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_handicapes", array()), 'errors');
            echo "
                  ";
            // line 582
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "acces_handicapes", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 588
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "climatisation", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 591
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "climatisation", array()), 'errors');
            echo "
                  ";
            // line 592
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "climatisation", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 598
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gardien", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 601
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gardien", array()), 'errors');
            echo "
                  ";
            // line 602
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gardien", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 608
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "animal", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 611
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "animal", array()), 'errors');
            echo "
                  ";
            // line 612
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "animal", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 618
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fumeur", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 621
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fumeur", array()), 'errors');
            echo "
                  ";
            // line 622
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fumeur", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 628
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "internet", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 631
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "internet", array()), 'errors');
            echo "
                  ";
            // line 632
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "internet", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 638
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "type_chauffage", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 641
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "type_chauffage", array()), 'errors');
            echo "
                  ";
            // line 642
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "type_chauffage", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 648
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mode_chauffage", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 651
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mode_chauffage", array()), 'errors');
            echo "
                  ";
            // line 652
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mode_chauffage", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 658
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "compteur_edf", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 661
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "compteur_edf", array()), 'errors');
            echo "
                  ";
            // line 662
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "compteur_edf", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 668
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "compteur_gdf", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 671
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "compteur_gdf", array()), 'errors');
            echo "
                  ";
            // line 672
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "compteur_gdf", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 678
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_compteurs", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 681
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_compteurs", array()), 'errors');
            echo "
                  ";
            // line 682
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_compteurs", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 688
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_compteur_eau", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 691
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_compteur_eau", array()), 'errors');
            echo "
                  ";
            // line 692
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_compteur_eau", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 698
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_boite_lettres", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 701
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_boite_lettres", array()), 'errors');
            echo "
                  ";
            // line 702
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emplacement_boite_lettres", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 708
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom_boite_lettres", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 711
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom_boite_lettres", array()), 'errors');
            echo "
                  ";
            // line 712
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom_boite_lettres", array()), 'widget');
            echo "
                </div>
              </div>
            </div>
          </div>

          <div class=\"blocForm\">
            <div class=\"title\">Diagnostic Performance Energétique*</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                <div class=\"span12\">
                  ";
            // line 724
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "statut_diagnostic", array()), 'errors');
            echo "
                  ";
            // line 725
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "statut_diagnostic", array()), 'widget');
            echo "
                </div>
              </div>

              <div id=\"tableDiagnostic\">
                <div class=\"row-fluid\">
                  <div class=\"span4\">
                    ";
            // line 732
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_diagnostic", array()), 'label');
            echo "
                  </div>
                  <div class=\"span8\">
                    ";
            // line 735
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_diagnostic", array()), 'errors');
            echo "
                    ";
            // line 736
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_diagnostic", array()), 'widget');
            echo " <i><small>(jj/mm/yyyy)</small></i>
                  </div>
                </div>

                <div class=\"row-fluid\">
                  <div class=\"span4\">
                    ";
            // line 742
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fichier_diagnostic", array()), 'label');
            echo "
                  </div>
                  <div class=\"span8\">
                    ";
            // line 745
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fichier_diagnostic", array()), 'errors');
            echo "
                    ";
            // line 746
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fichier_diagnostic", array()), 'widget');
            echo "
                    ";
            // line 747
            if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "fichierDiagnostic", array())) {
                // line 748
                echo "                      <br/><a id=\"linkFormulaire\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/photos/biens/"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "reference", array()), "html", null, true);
                echo "/diagnostic/";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "fichierDiagnostic", array()), "html", null, true);
                echo "\" target=\"_blank\">Télécharger votre DPE</a>
                    ";
            }
            // line 750
            echo "                  </div>
                </div>

                <div class=\"row-fluid\">
                  <div class=\"span4\">
                    ";
            // line 755
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "consommation_lettre", array()), 'label');
            echo "
                  </div>
                  <div class=\"span8\">
                    ";
            // line 758
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "consommation_lettre", array()), 'errors');
            echo "
                    ";
            // line 759
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "consommation_lettre", array()), 'widget');
            echo "
                    ";
            // line 760
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "consommation_chiffre", array()), 'errors');
            echo "
                    ";
            // line 761
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "consommation_chiffre", array()), 'widget');
            echo " kWh/m2/an
                  </div>
                </div>

                <div class=\"row-fluid\">
                  <div class=\"span4\">
                    ";
            // line 767
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gaz_serre_lettre", array()), 'label');
            echo "
                  </div>
                  <div class=\"span8\">
                    ";
            // line 770
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gaz_serre_lettre", array()), 'errors');
            echo "
                    ";
            // line 771
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gaz_serre_lettre", array()), 'widget');
            echo "
                    ";
            // line 772
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gaz_serre_chiffre", array()), 'errors');
            echo "
                    ";
            // line 773
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gaz_serre_chiffre", array()), 'widget');
            echo " kg/m2/an
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class=\"blocForm\">
            <div class=\"title\">Visites</div>

            <div class=\"content\">
              <small>Ces informations ne seront visibles que par nos services et ne seront pas diffusées sur internet</small>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 788
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_porte", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 791
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_porte", array()), 'errors');
            echo "
                  ";
            // line 792
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_porte", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 798
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_description", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 801
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_description", array()), 'errors');
            echo "
                  ";
            // line 802
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_description", array()), 'widget');
            echo "
                  <br/><i><small>Indiquez ici comment accéder à l’appartement ou la maison (position sur le palier, dans le lotissement) Vous pouvez également ajouter les informations utiles comme le téléphone du concierge ou celui du syndic.</small></i>
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 809
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_cle", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 812
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_cle", array()), 'errors');
            echo "
                  ";
            // line 813
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_cle", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 819
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_cave", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 822
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_cave", array()), 'errors');
            echo "
                  ";
            // line 823
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_cave", array()), 'widget');
            echo "
                </div>
              </div>

              <div class=\"row-fluid\">
                <div class=\"span4\">
                  ";
            // line 829
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_parking", array()), 'label');
            echo "
                </div>
                <div class=\"span8\">
                  ";
            // line 832
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_parking", array()), 'errors');
            echo "
                  ";
            // line 833
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "visite_parking", array()), 'widget');
            echo "
                </div>
              </div>
            </div>
          </div>

          ";
            // line 874
            echo "
          <div class=\"blocForm\">
            <div class=\"title\">Observations</div>

            <div class=\"content\">
              <div class=\"row-fluid\">
                ";
            // line 880
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commentaire", array()), 'label');
            echo "
                ";
            // line 881
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commentaire", array()), 'errors');
            echo "
                ";
            // line 882
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commentaire", array()), 'widget', array("attr" => array("class" => "span12", "rows" => "5")));
            echo "
              </div>

              <div class=\"row-fluid\">
                ";
            // line 886
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "confidentiel", array()), 'label');
            echo "
                ";
            // line 887
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "confidentiel", array()), 'errors');
            echo "
                ";
            // line 888
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "confidentiel", array()), 'widget', array("attr" => array("class" => "span12", "rows" => "5")));
            echo "
              </div>

              <div class=\"row-fluid\">
                ";
            // line 892
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "coordonnees_syndic", array()), 'label');
            echo "
                ";
            // line 893
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "coordonnees_syndic", array()), 'errors');
            echo "
                ";
            // line 894
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "coordonnees_syndic", array()), 'widget', array("attr" => array("class" => "span12", "rows" => "5")));
            echo "
              </div>
            </div>
          </div>

          ";
            // line 899
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
            echo "

          <br/><br/>

          <div class=\"row-fluid\">
            <div class=\"span6\">
              <a href=\"";
            // line 905
            echo $this->env->getExtension('routing')->getPath("pat_appartement_index");
            echo "\" class=\"btn btn-large\">Revenir</a>
            </div>
            <div class=\"span6 right\">
              <input class=\"btn btn-primary btn-large\" type=\"submit\" value=\"";
            // line 908
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Continuer"), "html", null, true);
            echo "\"/>
            </div>
          </div>

        </form>

        ";
            // line 914
            if (((isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")) && ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "statut", array()) == 0))) {
                // line 915
                echo "          <hr/><br/>
          <form action=\"";
                // line 916
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_appartement_validation", array("id" => (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")))), "html", null, true);
                echo "\" method=\"post\" ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formValid"]) ? $context["formValid"] : $this->getContext($context, "formValid")), 'enctype');
                echo ">

            <div class=\"row-fluid\">
              <div class=\"span1 right\">
                ";
                // line 920
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formValid"]) ? $context["formValid"] : $this->getContext($context, "formValid")), "choice1", array()), 'widget');
                echo "
              </div>
              <div class=\"span11\">
                J’ai lu et j’accepte les conditions du mandat de commercialisation de Class Appart
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span1 right\">
                ";
                // line 929
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formValid"]) ? $context["formValid"] : $this->getContext($context, "formValid")), "choice2", array()), 'widget');
                echo "
              </div>
              <div class=\"span11\">
                Je souhaite simplement faire apparaître mon bien sur le site www.class-appart.com mais je ne souhaite pas faire certifier mon bien. Par conséquent, la présence du bien sur le site ne sera pas optimisée et la réservation ne sera pas possible via le site internet.
              </div>
            </div>

            ";
                // line 936
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formValid"]) ? $context["formValid"] : $this->getContext($context, "formValid")), 'rest');
                echo "

            <br/><br/>
            <div class=\"row-fluid center\">
              <input type=\"submit\" value=\"";
                // line 940
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Soumettre votre bien pour validation"), "html", null, true);
                echo "\" class=\"btn btn-primary\"/>
            </div>
          </form>
        ";
            }
            // line 944
            echo "      </div>

      <!-- compte le nombre de caractères dans la version courte de la description-->
      <script type=\"text/javascript\" src=\"";
            // line 947
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/js/charCount.js"), "html", null, true);
            echo "\"></script>
      <script type=\"text/javascript\">
        \$(document).ready(function () {
          \$(\"#appartement_description_courte_fr\").charCount({
            allowed: 200,
            exceeded: 0,
            counterText: 'Caractères restants : '
          });


          \$(\".has_digicod\").hide();
          if (\$(\"#appartement_has_digicode\").is(':checked'))
            \$(\".has_digicod\").show();

          \$(\"#appartement_has_digicode\").change(function () {
            if (\$(\"#appartement_has_digicode\").is(':checked'))
              \$(\".has_digicod\").show(500);
            else
              \$(\".has_digicod\").hide(500);
          });
        });


        /* Calcul du prix indicatif du séjour */
        function calculPrixSejourAppart(prix) {
          caracteresValides = \"0,123456789.\";
          for (i = 0; i < prix.length; i++) {
            if (caracteresValides.indexOf(prix.charAt(i)) == -1) {
              return false;
            }
          }

          prix = prix.replace(\",\", \".\");
          document.getElementById(\"prix_mois\").value = (prix * 1.18).toFixed(2).replace(\".\", \",\");
        }

        \$('#appartement_loyer').change(function () {
          calculPrixSejourAppart(document.getElementById(\"appartement_loyer\").value);
        });

        \$('#appartement_loyer').keyup(function () {
          calculPrixSejourAppart(document.getElementById(\"appartement_loyer\").value);
        });

        if (document.getElementById(\"appartement_loyer\") != null)
          calculPrixSejourAppart(document.getElementById(\"appartement_loyer\").value);


        /* Calcul du prix indicatif du séjour */
        function displayDiagnostic(valeur) {
          if (valeur == 1) {
            document.getElementById(\"tableDiagnostic\").style.display = \"block\";
          } else {
            document.getElementById(\"tableDiagnostic\").style.display = \"none\";
            document.getElementById(\"appartement_consommation_chiffre\").value = \"\";
            document.getElementById(\"appartement_gaz_serre_chiffre\").value = \"\";
            document.getElementById(\"appartement_date_diagnostic\").value = \"\";
          }
        }

        \$('#appartement_statut_diagnostic').change(function () {
          displayDiagnostic(document.getElementById(\"appartement_statut_diagnostic\").value);
        });

        displayDiagnostic(document.getElementById(\"appartement_statut_diagnostic\").value)



        \$(window).load(function () {
          setTimeout(\"scrollBas()\", 300);
        });

        function scrollBas() {
          var chaine = document.location + \"\";
          var longueur = chaine.length - 10;

          if (chaine.substr(longueur, 10) == \"validation\") {
            \$(\"html,body\").animate({scrollTop: \$('body').css('height')}, 0);
          }
        }
      </script>


    ";
        }
        // line 1030
        echo "<!-- end if validation -->

  </div><!-- /contenuCentrale -->

  <div id=\"contenuDroite\">
    <div id=\"blocAide\"><div class=\"blocAideTitre\">Aide</div>
      Pour toute question concernant la procédure d'ajout d'un bien, merci de nous contacter via le formulaire de contact.<br/>
      Consultez les <a href=\"";
        // line 1037
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_front_contenu_page", array("lg" => "fr", "url_page" => "faq")), "html", null, true);
        echo "\">FAQ</a> et les Guides.
    </div><!-- /blocAide -->

  </div><!-- /contentDroit -->

";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:Appartement:ajouter_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  1843 => 1037,  1834 => 1030,  1747 => 947,  1742 => 944,  1728 => 936,  1718 => 929,  1706 => 920,  1694 => 915,  1692 => 914,  1683 => 908,  1677 => 905,  1668 => 899,  1660 => 894,  1656 => 893,  1652 => 892,  1645 => 888,  1641 => 887,  1630 => 882,  1626 => 881,  1622 => 880,  1614 => 874,  1601 => 832,  1595 => 829,  1586 => 823,  1582 => 822,  1576 => 819,  1567 => 813,  1563 => 812,  1557 => 809,  1547 => 802,  1543 => 801,  1537 => 798,  1528 => 792,  1518 => 788,  1500 => 773,  1496 => 772,  1492 => 771,  1488 => 770,  1482 => 767,  1473 => 761,  1469 => 760,  1465 => 759,  1461 => 758,  1448 => 750,  1438 => 748,  1436 => 747,  1432 => 746,  1428 => 745,  1422 => 742,  1413 => 736,  1409 => 735,  1403 => 732,  1389 => 724,  1370 => 711,  1364 => 708,  1351 => 701,  1345 => 698,  1332 => 691,  1326 => 688,  1313 => 681,  1307 => 678,  1294 => 671,  1288 => 668,  1275 => 661,  1269 => 658,  1256 => 651,  1250 => 648,  1237 => 641,  1231 => 638,  1218 => 631,  1212 => 628,  1199 => 621,  1193 => 618,  1180 => 611,  1155 => 598,  1142 => 591,  1136 => 588,  1127 => 582,  1117 => 578,  1108 => 572,  1098 => 568,  1079 => 558,  1071 => 552,  1067 => 550,  1065 => 549,  1054 => 541,  1050 => 540,  1044 => 537,  1035 => 531,  1031 => 530,  1025 => 527,  1016 => 521,  1006 => 517,  997 => 511,  993 => 510,  987 => 507,  978 => 501,  974 => 500,  959 => 491,  955 => 490,  949 => 487,  940 => 481,  936 => 480,  930 => 477,  921 => 471,  911 => 467,  902 => 461,  898 => 460,  892 => 457,  883 => 451,  879 => 450,  864 => 441,  854 => 437,  845 => 431,  835 => 427,  826 => 421,  816 => 417,  805 => 411,  801 => 410,  797 => 409,  791 => 406,  778 => 399,  772 => 396,  763 => 390,  759 => 389,  753 => 386,  744 => 380,  740 => 379,  721 => 369,  715 => 366,  687 => 350,  677 => 346,  668 => 340,  643 => 324,  624 => 314,  614 => 310,  605 => 304,  595 => 300,  538 => 267,  519 => 257,  485 => 238,  481 => 237,  447 => 218,  262 => 114,  281 => 118,  197 => 83,  419 => 185,  308 => 130,  260 => 121,  228 => 96,  355 => 153,  346 => 150,  288 => 121,  692 => 459,  603 => 377,  561 => 278,  515 => 319,  475 => 234,  379 => 162,  348 => 227,  298 => 132,  1174 => 608,  1167 => 718,  1161 => 601,  1154 => 710,  1149 => 707,  1141 => 704,  1134 => 702,  1131 => 701,  1129 => 700,  1114 => 695,  1111 => 694,  1109 => 693,  1105 => 692,  1096 => 689,  1092 => 688,  1089 => 562,  1072 => 675,  1064 => 670,  1042 => 651,  1034 => 645,  1026 => 642,  1022 => 640,  1019 => 639,  1017 => 638,  1012 => 520,  1008 => 635,  1004 => 633,  1002 => 632,  996 => 629,  988 => 626,  984 => 624,  979 => 623,  977 => 622,  968 => 497,  962 => 613,  951 => 605,  917 => 470,  915 => 585,  912 => 584,  905 => 580,  897 => 575,  893 => 574,  881 => 568,  873 => 447,  871 => 561,  863 => 555,  853 => 496,  842 => 488,  831 => 480,  812 => 464,  800 => 456,  798 => 455,  787 => 447,  754 => 423,  747 => 418,  739 => 414,  731 => 410,  725 => 370,  718 => 404,  696 => 356,  681 => 378,  675 => 375,  665 => 369,  663 => 368,  651 => 359,  628 => 344,  606 => 334,  602 => 332,  600 => 331,  579 => 315,  572 => 311,  555 => 303,  542 => 268,  532 => 264,  513 => 254,  501 => 315,  499 => 271,  473 => 267,  441 => 245,  438 => 198,  416 => 231,  330 => 143,  289 => 133,  633 => 320,  627 => 291,  621 => 289,  619 => 288,  594 => 272,  576 => 287,  570 => 284,  562 => 255,  558 => 254,  552 => 251,  540 => 245,  508 => 228,  498 => 314,  486 => 308,  480 => 215,  454 => 201,  450 => 200,  410 => 265,  325 => 153,  220 => 93,  407 => 237,  402 => 234,  377 => 245,  313 => 172,  232 => 103,  465 => 197,  457 => 192,  417 => 270,  411 => 176,  408 => 225,  396 => 256,  391 => 168,  388 => 229,  372 => 160,  344 => 163,  332 => 220,  293 => 134,  274 => 126,  231 => 99,  200 => 82,  792 => 418,  788 => 416,  782 => 400,  776 => 439,  774 => 411,  762 => 402,  748 => 394,  742 => 391,  734 => 376,  730 => 385,  724 => 382,  716 => 403,  706 => 360,  698 => 389,  694 => 367,  688 => 364,  680 => 359,  676 => 358,  662 => 350,  658 => 336,  652 => 346,  644 => 341,  640 => 340,  634 => 337,  622 => 331,  616 => 386,  608 => 323,  598 => 273,  580 => 288,  564 => 297,  545 => 287,  541 => 286,  526 => 237,  507 => 317,  488 => 257,  462 => 227,  433 => 226,  424 => 207,  395 => 169,  382 => 168,  376 => 162,  341 => 188,  327 => 140,  320 => 153,  310 => 206,  291 => 129,  278 => 127,  259 => 109,  244 => 104,  448 => 250,  443 => 217,  429 => 259,  406 => 174,  366 => 206,  318 => 136,  282 => 128,  258 => 178,  222 => 93,  120 => 46,  272 => 115,  266 => 147,  226 => 100,  178 => 81,  111 => 55,  393 => 255,  386 => 187,  380 => 184,  362 => 157,  358 => 207,  342 => 139,  340 => 162,  334 => 159,  326 => 139,  319 => 138,  314 => 136,  299 => 163,  265 => 110,  252 => 107,  237 => 102,  194 => 93,  132 => 57,  23 => 3,  97 => 43,  81 => 32,  53 => 13,  654 => 223,  637 => 295,  632 => 206,  625 => 343,  623 => 342,  617 => 195,  612 => 337,  604 => 322,  593 => 327,  591 => 186,  586 => 365,  583 => 183,  578 => 180,  571 => 178,  557 => 277,  534 => 242,  522 => 236,  520 => 320,  504 => 248,  494 => 244,  463 => 145,  446 => 186,  440 => 136,  434 => 134,  431 => 240,  427 => 182,  405 => 197,  401 => 221,  397 => 114,  389 => 215,  381 => 210,  357 => 148,  349 => 193,  339 => 146,  303 => 131,  295 => 119,  287 => 128,  268 => 123,  74 => 29,  470 => 398,  452 => 236,  444 => 197,  435 => 405,  430 => 397,  414 => 183,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 940,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 916,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 886,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 833,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 791,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 755,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 725,  1387 => 694,  1378 => 688,  1374 => 712,  1368 => 684,  1359 => 678,  1355 => 702,  1349 => 674,  1340 => 668,  1336 => 692,  1330 => 664,  1321 => 658,  1317 => 682,  1311 => 654,  1302 => 648,  1298 => 672,  1292 => 644,  1283 => 638,  1279 => 662,  1273 => 634,  1264 => 628,  1260 => 652,  1254 => 624,  1245 => 618,  1241 => 642,  1235 => 614,  1226 => 608,  1222 => 632,  1216 => 604,  1207 => 598,  1203 => 622,  1197 => 594,  1188 => 588,  1184 => 612,  1178 => 584,  1169 => 578,  1165 => 602,  1159 => 574,  1150 => 568,  1146 => 592,  1140 => 564,  1123 => 581,  1119 => 697,  1113 => 546,  1104 => 571,  1100 => 690,  1094 => 536,  1085 => 561,  1081 => 529,  1075 => 526,  1066 => 671,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 599,  933 => 450,  929 => 588,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 569,  874 => 420,  870 => 419,  866 => 418,  860 => 440,  851 => 409,  847 => 408,  841 => 430,  832 => 399,  828 => 398,  822 => 420,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 431,  756 => 359,  752 => 395,  746 => 355,  737 => 413,  733 => 348,  727 => 408,  712 => 376,  708 => 332,  702 => 359,  693 => 323,  689 => 322,  683 => 349,  674 => 241,  670 => 355,  664 => 339,  649 => 297,  645 => 296,  639 => 323,  630 => 287,  626 => 332,  620 => 313,  611 => 277,  607 => 279,  601 => 303,  592 => 267,  588 => 269,  582 => 263,  563 => 348,  554 => 344,  550 => 246,  544 => 246,  535 => 283,  531 => 236,  516 => 233,  512 => 318,  506 => 163,  493 => 312,  478 => 253,  468 => 209,  459 => 144,  455 => 209,  449 => 193,  436 => 192,  428 => 208,  422 => 234,  409 => 198,  403 => 261,  390 => 188,  384 => 165,  351 => 152,  337 => 187,  311 => 118,  296 => 139,  256 => 108,  241 => 105,  215 => 91,  207 => 89,  192 => 77,  186 => 74,  175 => 80,  153 => 69,  118 => 57,  61 => 19,  34 => 6,  65 => 17,  77 => 21,  37 => 7,  190 => 77,  161 => 70,  137 => 55,  126 => 50,  261 => 106,  255 => 110,  247 => 105,  242 => 109,  214 => 93,  211 => 90,  191 => 85,  157 => 62,  145 => 63,  127 => 54,  373 => 111,  367 => 174,  363 => 173,  359 => 172,  354 => 152,  343 => 147,  335 => 145,  328 => 152,  322 => 138,  315 => 149,  309 => 137,  305 => 115,  302 => 142,  290 => 157,  284 => 121,  279 => 119,  271 => 117,  264 => 122,  248 => 103,  236 => 106,  223 => 99,  170 => 68,  110 => 46,  96 => 38,  84 => 24,  472 => 210,  467 => 148,  375 => 161,  371 => 160,  360 => 183,  356 => 232,  353 => 169,  350 => 151,  338 => 145,  336 => 221,  331 => 141,  321 => 152,  316 => 148,  307 => 142,  304 => 134,  301 => 130,  297 => 131,  292 => 123,  286 => 116,  283 => 130,  277 => 116,  275 => 116,  270 => 123,  263 => 112,  257 => 146,  253 => 107,  249 => 135,  245 => 110,  233 => 92,  225 => 98,  216 => 90,  206 => 149,  202 => 96,  198 => 86,  185 => 95,  180 => 82,  177 => 77,  165 => 65,  150 => 65,  124 => 61,  113 => 43,  100 => 36,  58 => 19,  251 => 109,  234 => 101,  213 => 95,  195 => 84,  174 => 79,  167 => 76,  146 => 58,  140 => 47,  128 => 51,  104 => 43,  90 => 36,  83 => 24,  52 => 22,  596 => 225,  590 => 314,  585 => 221,  577 => 357,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 210,  551 => 274,  546 => 207,  543 => 206,  539 => 205,  529 => 322,  525 => 173,  523 => 258,  518 => 193,  514 => 192,  509 => 189,  503 => 266,  500 => 247,  497 => 263,  495 => 313,  492 => 157,  490 => 219,  487 => 213,  484 => 256,  482 => 177,  479 => 176,  477 => 401,  474 => 206,  471 => 173,  469 => 172,  466 => 228,  464 => 292,  461 => 169,  458 => 239,  456 => 224,  451 => 189,  445 => 160,  442 => 199,  439 => 229,  437 => 214,  432 => 191,  426 => 188,  423 => 186,  420 => 219,  418 => 204,  413 => 172,  399 => 194,  394 => 162,  378 => 162,  370 => 159,  368 => 203,  365 => 212,  361 => 199,  347 => 148,  345 => 94,  333 => 131,  329 => 179,  323 => 139,  317 => 272,  312 => 133,  306 => 143,  300 => 110,  294 => 199,  285 => 193,  280 => 118,  276 => 116,  267 => 116,  250 => 107,  239 => 100,  229 => 116,  218 => 94,  212 => 88,  210 => 97,  205 => 83,  188 => 85,  184 => 75,  181 => 71,  169 => 69,  160 => 63,  152 => 60,  148 => 66,  139 => 65,  134 => 63,  114 => 42,  107 => 34,  76 => 21,  70 => 25,  273 => 127,  269 => 94,  254 => 139,  246 => 110,  243 => 106,  240 => 103,  238 => 100,  235 => 101,  230 => 100,  227 => 100,  224 => 94,  221 => 96,  219 => 112,  217 => 84,  208 => 91,  204 => 91,  179 => 71,  159 => 66,  143 => 58,  135 => 56,  131 => 52,  108 => 43,  102 => 52,  71 => 26,  67 => 17,  63 => 21,  59 => 16,  47 => 11,  94 => 49,  89 => 28,  85 => 31,  79 => 21,  75 => 27,  68 => 25,  56 => 18,  50 => 16,  38 => 5,  29 => 9,  87 => 35,  72 => 26,  55 => 18,  21 => 2,  26 => 2,  35 => 5,  31 => 3,  41 => 9,  28 => 2,  201 => 145,  196 => 87,  183 => 81,  171 => 77,  166 => 66,  163 => 73,  156 => 71,  151 => 69,  142 => 63,  138 => 62,  136 => 64,  123 => 48,  121 => 57,  115 => 47,  105 => 32,  101 => 39,  91 => 36,  69 => 18,  66 => 21,  62 => 22,  49 => 11,  98 => 35,  93 => 41,  88 => 37,  78 => 33,  46 => 14,  44 => 10,  32 => 5,  27 => 4,  43 => 7,  40 => 6,  25 => 4,  24 => 2,  172 => 85,  158 => 62,  155 => 61,  129 => 56,  119 => 52,  117 => 45,  20 => 1,  22 => 2,  19 => 1,  209 => 100,  203 => 95,  199 => 94,  193 => 84,  189 => 81,  187 => 81,  182 => 85,  176 => 82,  173 => 89,  168 => 72,  164 => 66,  162 => 64,  154 => 60,  149 => 68,  147 => 67,  144 => 66,  141 => 57,  133 => 53,  130 => 63,  125 => 55,  122 => 59,  116 => 53,  112 => 41,  109 => 40,  106 => 45,  103 => 37,  99 => 42,  95 => 31,  92 => 31,  86 => 46,  82 => 23,  80 => 33,  73 => 20,  64 => 22,  60 => 20,  57 => 25,  54 => 13,  51 => 12,  48 => 9,  45 => 16,  42 => 6,  39 => 10,  36 => 10,  33 => 7,  30 => 4,);
    }
}
