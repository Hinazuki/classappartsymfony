<?php

/* MopaBootstrapBundle:Form:fields.html.twig */
class __TwigTemplate_aee4e462ad34ae26f77e5325257f11ec090c90ee4c3445d1a8bf8f004df7423b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("form_div_layout.html.twig");

        $this->blocks = array(
            'button_attributes' => array($this, 'block_button_attributes'),
            'button_widget' => array($this, 'block_button_widget'),
            'form_widget_simple' => array($this, 'block_form_widget_simple'),
            'form_widget_compound' => array($this, 'block_form_widget_compound'),
            'collection_widget' => array($this, 'block_collection_widget'),
            'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
            'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
            'checkbox_widget' => array($this, 'block_checkbox_widget'),
            'date_widget' => array($this, 'block_date_widget'),
            'time_widget' => array($this, 'block_time_widget'),
            'datetime_widget' => array($this, 'block_datetime_widget'),
            'percent_widget' => array($this, 'block_percent_widget'),
            'money_widget' => array($this, 'block_money_widget'),
            'form_legend' => array($this, 'block_form_legend'),
            'form_label' => array($this, 'block_form_label'),
            'help_label' => array($this, 'block_help_label'),
            'help_label_tooltip' => array($this, 'block_help_label_tooltip'),
            'help_label_popover' => array($this, 'block_help_label_popover'),
            'form_rows_visible' => array($this, 'block_form_rows_visible'),
            'form_row' => array($this, 'block_form_row'),
            'form_message' => array($this, 'block_form_message'),
            'form_help' => array($this, 'block_form_help'),
            'form_widget_add_btn' => array($this, 'block_form_widget_add_btn'),
            'form_widget_remove_btn' => array($this, 'block_form_widget_remove_btn'),
            'collection_button' => array($this, 'block_collection_button'),
            'label_asterisk' => array($this, 'block_label_asterisk'),
            'widget_addon' => array($this, 'block_widget_addon'),
            '_form_errors' => array($this, 'block__form_errors'),
            'form_errors' => array($this, 'block_form_errors'),
            'error_type' => array($this, 'block_error_type'),
            'widget_control_group_start' => array($this, 'block_widget_control_group_start'),
            'widget_control_group_end' => array($this, 'block_widget_control_group_end'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "form_div_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["__internal_d16a683aba225eb5b14ce6cdafb95da20a5a47c762b4d13a2c5c610d4e17a18f"] = $this->env->loadTemplate("MopaBootstrapBundle::flash.html.twig");
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_button_attributes($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => ("btn " . (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")))));
        // line 7
        echo "    ";
        $this->displayParentBlock("button_attributes", $context, $blocks);
        echo "
";
    }

    // line 10
    public function block_button_widget($context, array $blocks = array())
    {
        // line 11
        ob_start();
        // line 12
        echo "    ";
        if (twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")))) {
            // line 13
            echo "        ";
            $context["label"] = $this->env->getExtension('form')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
            // line 14
            echo "    ";
        }
        // line 15
        echo "    <button type=\"";
        echo twig_escape_filter($this->env, ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "button")) : ("button")), "html", null, true);
        echo "\" ";
        $this->displayBlock("button_attributes", $context, $blocks);
        echo ">
    ";
        // line 16
        if ((!twig_test_empty((isset($context["icon"]) ? $context["icon"] : $this->getContext($context, "icon"))))) {
            echo " <i class=\"icon-";
            echo twig_escape_filter($this->env, (isset($context["icon"]) ? $context["icon"] : $this->getContext($context, "icon")), "html", null, true);
            if ((!twig_test_empty((isset($context["icon_color"]) ? $context["icon_color"] : $this->getContext($context, "icon_color"))))) {
                echo " icon-";
                echo twig_escape_filter($this->env, (isset($context["icon_color"]) ? $context["icon_color"] : $this->getContext($context, "icon_color")), "html", null, true);
            }
            echo "\"></i> ";
        }
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
        echo "</button>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 22
    public function block_form_widget_simple($context, array $blocks = array())
    {
        // line 23
        ob_start();
        // line 24
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "text")) : ("text"));
        // line 25
        echo "    ";
        if ((((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")) != "hidden") && (!(null === (($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "type", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "type", array()), null)) : (null)))))) {
            // line 26
            echo "    <div class=\"input-";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : $this->getContext($context, "widget_addon")), "type", array()), "html", null, true);
            echo "\">
        ";
            // line 27
            if (($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : $this->getContext($context, "widget_addon")), "type", array()) == "prepend")) {
                // line 28
                echo "        ";
                $this->displayBlock("widget_addon", $context, $blocks);
                echo "
        ";
            }
            // line 30
            echo "    ";
        }
        // line 31
        echo "    ";
        if ((!((array_key_exists("widget_remove_btn", $context)) ? (_twig_default_filter((isset($context["widget_remove_btn"]) ? $context["widget_remove_btn"] : $this->getContext($context, "widget_remove_btn")), null)) : (null)))) {
            // line 32
            echo "        ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => ((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " not-removable")));
            // line 33
            echo "    ";
        }
        // line 34
        echo "    <input type=\"";
        echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "html", null, true);
        echo "\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " ";
        if ((!twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))))) {
            echo "value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "\" ";
        }
        echo "/>
    ";
        // line 35
        if ((((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")) != "hidden") && (!(null === (($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "type", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "type", array()), null)) : (null)))))) {
            // line 36
            echo "        ";
            if (($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : $this->getContext($context, "widget_addon")), "type", array()) == "append")) {
                // line 37
                echo "        ";
                $this->displayBlock("widget_addon", $context, $blocks);
                echo "
        ";
            }
            // line 39
            echo "    </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 44
    public function block_form_widget_compound($context, array $blocks = array())
    {
        // line 45
        ob_start();
        // line 46
        echo "    ";
        if (($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()) == null)) {
            // line 47
            echo "        ";
            if ((isset($context["render_fieldset"]) ? $context["render_fieldset"] : $this->getContext($context, "render_fieldset"))) {
                echo "<fieldset>";
            }
            // line 48
            echo "        ";
            if ((isset($context["show_legend"]) ? $context["show_legend"] : $this->getContext($context, "show_legend"))) {
                $this->displayBlock("form_legend", $context, $blocks);
            }
            // line 49
            echo "    ";
        }
        // line 50
        echo "    ";
        $this->displayBlock("form_rows_visible", $context, $blocks);
        echo "
    ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
    ";
        // line 52
        if (($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()) == null)) {
            // line 53
            echo "        ";
            if ((isset($context["render_fieldset"]) ? $context["render_fieldset"] : $this->getContext($context, "render_fieldset"))) {
                echo "</fieldset>";
            }
            // line 54
            echo "    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 58
    public function block_collection_widget($context, array $blocks = array())
    {
        // line 59
        ob_start();
        // line 60
        echo "    ";
        $this->displayBlock("form_widget", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 64
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        // line 65
        ob_start();
        // line 66
        echo "    ";
        $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => (((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " ") . (((isset($context["multiple"]) ? $context["multiple"] : $this->getContext($context, "multiple"))) ? ("checkbox") : ("radio")))));
        // line 67
        echo "    ";
        $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), "class", array()) . " ") . (((isset($context["widget_type"]) ? $context["widget_type"] : $this->getContext($context, "widget_type"))) ? ((isset($context["widget_type"]) ? $context["widget_type"] : $this->getContext($context, "widget_type"))) : ("")))));
        // line 68
        echo "    ";
        $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => trim((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), "class", array()) . " ") . (((array_key_exists("inline", $context) && (isset($context["inline"]) ? $context["inline"] : $this->getContext($context, "inline")))) ? ("inline") : (""))))));
        // line 69
        echo "    <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
    ";
        // line 70
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 71
            echo "        <label";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, (isset($context["attrname"]) ? $context["attrname"] : $this->getContext($context, "attrname")), "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, (isset($context["attrvalue"]) ? $context["attrvalue"] : $this->getContext($context, "attrvalue")), "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">
            ";
            // line 72
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["child"]) ? $context["child"] : $this->getContext($context, "child")), 'widget', array("attr" => array("class" => (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class", array()), "")) : ("")))));
            echo "
            ";
            // line 73
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getAttribute((isset($context["child"]) ? $context["child"] : $this->getContext($context, "child")), "vars", array()), "label", array()), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
            echo "
        </label>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 80
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        // line 81
        echo "    ";
        ob_start();
        // line 82
        echo "        ";
        if (($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : $this->getContext($context, "widget_addon")), "type", array()) == "prepend")) {
            // line 83
            echo "            ";
            $this->displayBlock("widget_addon", $context, $blocks);
            echo "
        ";
        }
        // line 85
        echo "        <select ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if ((isset($context["multiple"]) ? $context["multiple"] : $this->getContext($context, "multiple"))) {
            echo " multiple=\"multiple\"";
        }
        echo ">
            ";
        // line 86
        if ((!(null === (isset($context["empty_value"]) ? $context["empty_value"] : $this->getContext($context, "empty_value"))))) {
            // line 87
            echo "                <option value=\"\"";
            if (((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required")) && twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))))) {
                echo " selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["empty_value"]) ? $context["empty_value"] : $this->getContext($context, "empty_value")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
            echo "</option>
            ";
        }
        // line 89
        echo "            ";
        if ((twig_length_filter($this->env, (isset($context["preferred_choices"]) ? $context["preferred_choices"] : $this->getContext($context, "preferred_choices"))) > 0)) {
            // line 90
            echo "                ";
            $context["options"] = (isset($context["preferred_choices"]) ? $context["preferred_choices"] : $this->getContext($context, "preferred_choices"));
            // line 91
            echo "                ";
            $this->displayBlock("choice_widget_options", $context, $blocks);
            echo "
                ";
            // line 92
            if (((twig_length_filter($this->env, (isset($context["choices"]) ? $context["choices"] : $this->getContext($context, "choices"))) > 0) && (!(null === (isset($context["separator"]) ? $context["separator"] : $this->getContext($context, "separator")))))) {
                // line 93
                echo "                    <option disabled=\"disabled\">";
                echo twig_escape_filter($this->env, (isset($context["separator"]) ? $context["separator"] : $this->getContext($context, "separator")), "html", null, true);
                echo "</option>
                ";
            }
            // line 95
            echo "            ";
        }
        // line 96
        echo "            ";
        $context["options"] = (isset($context["choices"]) ? $context["choices"] : $this->getContext($context, "choices"));
        // line 97
        echo "            ";
        $this->displayBlock("choice_widget_options", $context, $blocks);
        echo "
        </select>
        ";
        // line 99
        if (($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : $this->getContext($context, "widget_addon")), "type", array()) == "append")) {
            // line 100
            echo "            ";
            $this->displayBlock("widget_addon", $context, $blocks);
            echo "
        ";
        }
        // line 102
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 105
    public function block_checkbox_widget($context, array $blocks = array())
    {
        // line 106
        ob_start();
        // line 107
        if (((!((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false)) && twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"))))) {
            // line 108
            echo "    ";
            $context["label"] = $this->env->getExtension('form')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
        }
        // line 110
        if (((($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()) != null) && !twig_in_filter("choice", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()), "vars", array()), "block_prefixes", array()))) && (isset($context["label_render"]) ? $context["label_render"] : $this->getContext($context, "label_render")))) {
            // line 111
            echo "    <label class=\"checkbox";
            if ((array_key_exists("inline", $context) && (isset($context["inline"]) ? $context["inline"] : $this->getContext($context, "inline")))) {
                echo " inline";
            }
            echo "\">
";
        }
        // line 113
        echo "        <input type=\"checkbox\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (array_key_exists("value", $context)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "\"";
        }
        if ((isset($context["checked"]) ? $context["checked"] : $this->getContext($context, "checked"))) {
            echo " checked=\"checked\"";
        }
        echo "/> ";
        echo $this->env->getExtension('translator')->trans((isset($context["help_inline"]) ? $context["help_inline"] : $this->getContext($context, "help_inline")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")));
        echo "
";
        // line 114
        if ((($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()) != null) && !twig_in_filter("choice", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()), "vars", array()), "block_prefixes", array())))) {
            // line 115
            echo "    ";
            if (((isset($context["label_render"]) ? $context["label_render"] : $this->getContext($context, "label_render")) && twig_in_filter((isset($context["widget_checkbox_label"]) ? $context["widget_checkbox_label"] : $this->getContext($context, "widget_checkbox_label")), array(0 => "both", 1 => "widget")))) {
                // line 116
                echo "        ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
                echo "
        ";
                // line 117
                if (((isset($context["widget_checkbox_label"]) ? $context["widget_checkbox_label"] : $this->getContext($context, "widget_checkbox_label")) == "widget")) {
                    // line 118
                    echo "            ";
                    $this->displayBlock("label_asterisk", $context, $blocks);
                    echo "
        ";
                }
                // line 120
                echo "    ";
            }
            // line 121
            echo "    ";
            $context["help_inline"] = false;
            // line 122
            echo "    ";
            if ((isset($context["label_render"]) ? $context["label_render"] : $this->getContext($context, "label_render"))) {
                // line 123
                echo "    </label>
    ";
            }
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 129
    public function block_date_widget($context, array $blocks = array())
    {
        // line 130
        ob_start();
        // line 131
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 132
            echo "    ";
            if (array_key_exists("datepicker", $context)) {
                // line 133
                echo "        <div class=\"input-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : $this->getContext($context, "widget_addon")), "type", array()), "html", null, true);
                echo " date\" ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo " data-date=\"";
                echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
                echo "\" data-date-format=\"";
                echo twig_escape_filter($this->env, twig_lower_filter($this->env, (isset($context["format"]) ? $context["format"] : $this->getContext($context, "format"))), "html", null, true);
                echo "\" data-form=\"datepicker\">
            ";
                // line 134
                if (($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : $this->getContext($context, "widget_addon")), "type", array()) == "prepend")) {
                    // line 135
                    echo "                ";
                    $this->displayBlock("widget_addon", $context, $blocks);
                    echo "
            ";
                }
                // line 137
                echo "            ";
                $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => ((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " not-removable grd-white")));
                // line 138
                echo "            <input type=\"text\" ";
                $this->displayBlock("widget_attributes", $context, $blocks);
                echo " value=\"";
                echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
                echo "\"  data-form=\"datepicker\" data-date-format=\"";
                echo twig_escape_filter($this->env, twig_lower_filter($this->env, (isset($context["format"]) ? $context["format"] : $this->getContext($context, "format"))), "html", null, true);
                echo "\"/>
            ";
                // line 139
                if (($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : $this->getContext($context, "widget_addon")), "type", array()) == "append")) {
                    // line 140
                    echo "                ";
                    $this->displayBlock("widget_addon", $context, $blocks);
                    echo "
            ";
                }
                // line 142
                echo "            <script type=\"text/javascript\">
                \$(document).ready(function () {
                    \$(";
                // line 144
                echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
                echo ").datepicker();
                    \$(";
                // line 145
                echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
                echo ").datepicker().on(
                            \"changeDate\",
                            function(event){
                                \$(";
                // line 148
                echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
                echo ").datepicker('hide');
                            }
                    )
                });
            </script>
        </div>
    ";
            } else {
                // line 155
                echo "        ";
                $this->displayBlock("form_widget_simple", $context, $blocks);
                echo "
    ";
            }
        } else {
            // line 158
            echo "        ";
            $context["attrYear"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => ((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "inline")) : ("inline")) . " input-small")));
            // line 159
            echo "        ";
            $context["attrMonth"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => ((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "inline")) : ("inline")) . " input-mini")));
            // line 160
            echo "        ";
            $context["attrDay"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => ((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "inline")) : ("inline")) . " input-mini")));
            // line 161
            echo "
            ";
            // line 162
            echo strtr((isset($context["date_pattern"]) ? $context["date_pattern"] : $this->getContext($context, "date_pattern")), array("{{ year }}" =>             // line 163
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "year", array()), 'widget', array("attr" => (isset($context["attrYear"]) ? $context["attrYear"] : $this->getContext($context, "attrYear")))), "{{ month }}" =>             // line 164
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "month", array()), 'widget', array("attr" => (isset($context["attrMonth"]) ? $context["attrMonth"] : $this->getContext($context, "attrMonth")))), "{{ day }}" =>             // line 165
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "day", array()), 'widget', array("attr" => (isset($context["attrDay"]) ? $context["attrDay"] : $this->getContext($context, "attrDay"))))));
            // line 166
            echo "
        ";
            // line 167
            $this->displayBlock("help", $context, $blocks);
            echo "
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 172
    public function block_time_widget($context, array $blocks = array())
    {
        // line 173
        ob_start();
        // line 174
        echo "    ";
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 175
            echo "        ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        } else {
            // line 177
            echo "        ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "inline")) : ("inline"))));
            // line 178
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 179
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "hour", array()), 'widget', array("attr" => array("size" => "1", "class" => "input-mini")));
            if ((isset($context["with_minutes"]) ? $context["with_minutes"] : $this->getContext($context, "with_minutes"))) {
                echo ":";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "minute", array()), 'widget', array("attr" => array("size" => "1", "class" => "input-mini")));
            }
            if ((isset($context["with_seconds"]) ? $context["with_seconds"] : $this->getContext($context, "with_seconds"))) {
                echo ":";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "second", array()), 'widget', array("attr" => array("size" => "1", "class" => "input-mini")));
            }
            // line 180
            echo "        </div>
        ";
            // line 181
            $this->displayBlock("help", $context, $blocks);
            echo "
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 186
    public function block_datetime_widget($context, array $blocks = array())
    {
        // line 187
        ob_start();
        // line 188
        echo "    ";
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 189
            echo "        ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        } else {
            // line 191
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : (""))));
            // line 192
            echo "            <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
                ";
            // line 193
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'errors');
            echo "
                ";
            // line 194
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "time", array()), 'errors');
            echo "
                ";
            // line 195
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'widget', array("attr" => array("class" => (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class", array()), "")) : ("")))));
            echo "
                ";
            // line 196
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "time", array()), 'widget', array("attr" => array("class" => (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class", array()), "")) : ("")))));
            echo "
            </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 202
    public function block_percent_widget($context, array $blocks = array())
    {
        // line 203
        ob_start();
        // line 204
        echo "    ";
        $context["widget_addon"] = twig_array_merge((isset($context["widget_addon"]) ? $context["widget_addon"] : $this->getContext($context, "widget_addon")), array("text" => (($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "text", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "text", array()), "%")) : ("%"))));
        // line 205
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 209
    public function block_money_widget($context, array $blocks = array())
    {
        // line 210
        ob_start();
        // line 211
        echo "    ";
        $context["widget_addon"] = twig_array_merge((isset($context["widget_addon"]) ? $context["widget_addon"] : $this->getContext($context, "widget_addon")), array("text" => strtr((isset($context["money_pattern"]) ? $context["money_pattern"] : $this->getContext($context, "money_pattern")), array("{{ widget }}" => ""))));
        // line 212
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 218
    public function block_form_legend($context, array $blocks = array())
    {
        // line 219
        ob_start();
        // line 220
        echo "    ";
        if (twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")))) {
            // line 221
            echo "        ";
            $context["label"] = $this->env->getExtension('form')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
            // line 222
            echo "    ";
        }
        // line 223
        echo "    <legend>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
        echo "</legend>
    ";
        // line 224
        if ((isset($context["widget_add_btn"]) ? $context["widget_add_btn"] : $this->getContext($context, "widget_add_btn"))) {
            // line 225
            echo "        ";
            $this->displayBlock("form_widget_add_btn", $context, $blocks);
            echo "
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 230
    public function block_form_label($context, array $blocks = array())
    {
        // line 231
        if ((!twig_in_filter("checkbox", (isset($context["block_prefixes"]) ? $context["block_prefixes"] : $this->getContext($context, "block_prefixes"))) || twig_in_filter((isset($context["widget_checkbox_label"]) ? $context["widget_checkbox_label"] : $this->getContext($context, "widget_checkbox_label")), array(0 => "label", 1 => "both")))) {
            // line 232
            ob_start();
            // line 233
            echo "    ";
            if ((!((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false))) {
                // line 234
                echo "        ";
                if (twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")))) {
                    // line 235
                    echo "            ";
                    $context["label"] = $this->env->getExtension('form')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
                    // line 236
                    echo "        ";
                }
                // line 237
                echo "        ";
                if ((!(isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound")))) {
                    // line 238
                    echo "            ";
                    $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("for" => (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))));
                    // line 239
                    echo "        ";
                }
                // line 240
                echo "        ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => (((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " control-label") . (((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) ? (" required") : (" optional")))));
                // line 241
                echo "        <label";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")));
                foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                    echo " ";
                    echo twig_escape_filter($this->env, (isset($context["attrname"]) ? $context["attrname"] : $this->getContext($context, "attrname")), "html", null, true);
                    echo "=\"";
                    echo twig_escape_filter($this->env, (isset($context["attrvalue"]) ? $context["attrvalue"] : $this->getContext($context, "attrvalue")), "html", null, true);
                    echo "\"";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo ">
        ";
                // line 242
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
                echo "
        ";
                // line 243
                $this->displayBlock("label_asterisk", $context, $blocks);
                echo "
        ";
                // line 244
                if ((isset($context["widget_add_btn"]) ? $context["widget_add_btn"] : $this->getContext($context, "widget_add_btn"))) {
                    // line 245
                    echo "            ";
                    $this->displayBlock("form_widget_add_btn", $context, $blocks);
                    echo "
        ";
                }
                // line 247
                echo "        ";
                if ((isset($context["help_label_tooltip_title"]) ? $context["help_label_tooltip_title"] : $this->getContext($context, "help_label_tooltip_title"))) {
                    // line 248
                    echo "            ";
                    $this->displayBlock("help_label_tooltip", $context, $blocks);
                    echo "
        ";
                }
                // line 250
                echo "        ";
                if ((isset($context["help_label_popover_title"]) ? $context["help_label_popover_title"] : $this->getContext($context, "help_label_popover_title"))) {
                    // line 251
                    echo "            ";
                    $this->displayBlock("help_label_popover", $context, $blocks);
                    echo "
        ";
                }
                // line 253
                echo "        ";
                if ((isset($context["help_label"]) ? $context["help_label"] : $this->getContext($context, "help_label"))) {
                    // line 254
                    echo "            ";
                    $this->displayBlock("help_label", $context, $blocks);
                    echo "
        ";
                }
                // line 256
                echo "        </label>
    ";
            }
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        }
    }

    // line 262
    public function block_help_label($context, array $blocks = array())
    {
        // line 263
        echo "    <p class=\"help-block\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["help_label"]) ? $context["help_label"] : $this->getContext($context, "help_label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
        echo "</p>
";
    }

    // line 266
    public function block_help_label_tooltip($context, array $blocks = array())
    {
        // line 267
        echo "    <p class=\"help-inline\">
        <a href=\"#\" id=\"";
        // line 268
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_tooltip\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["help_label_tooltip_title"]) ? $context["help_label_tooltip_title"] : $this->getContext($context, "help_label_tooltip_title")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
        echo "\" tabindex=\"-1\" data-toggle=\"tooltip\" data-placement=\"";
        echo twig_escape_filter($this->env, (isset($context["help_label_tooltip_placement"]) ? $context["help_label_tooltip_placement"] : $this->getContext($context, "help_label_tooltip_placement")), "html", null, true);
        echo "\"><i class=\"";
        echo twig_escape_filter($this->env, (isset($context["help_label_tooltip_icon"]) ? $context["help_label_tooltip_icon"] : $this->getContext($context, "help_label_tooltip_icon")), "html", null, true);
        echo "\"></i></a>
    </p>
";
    }

    // line 272
    public function block_help_label_popover($context, array $blocks = array())
    {
        // line 273
        echo "    <p class=\"help-inline\">
        <a href=\"#\" id=\"";
        // line 274
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "_popover\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["help_label_popover_title"]) ? $context["help_label_popover_title"] : $this->getContext($context, "help_label_popover_title")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
        echo "\" tabindex=\"-1\" data-toggle=\"popover\" data-content=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["help_label_popover_content"]) ? $context["help_label_popover_content"] : $this->getContext($context, "help_label_popover_content")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
        echo "\" data-placement=\"";
        echo twig_escape_filter($this->env, (isset($context["help_label_popover_placement"]) ? $context["help_label_popover_placement"] : $this->getContext($context, "help_label_popover_placement")), "html", null, true);
        echo "\" data-trigger=\"hover\" data-html=\"true\"><i class=\"";
        echo twig_escape_filter($this->env, (isset($context["help_label_popover_icon"]) ? $context["help_label_popover_icon"] : $this->getContext($context, "help_label_popover_icon")), "html", null, true);
        echo "\"></i></a>
    </p>
";
    }

    // line 281
    public function block_form_rows_visible($context, array $blocks = array())
    {
        // line 282
        ob_start();
        // line 283
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 284
            echo "        ";
            if (!twig_in_filter("hidden", $this->getAttribute($this->getAttribute((isset($context["child"]) ? $context["child"] : $this->getContext($context, "child")), "vars", array()), "block_prefixes", array()))) {
                // line 285
                echo "            ";
                if ((twig_in_filter("collection", $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "block_prefixes", array())) && (!(isset($context["omit_collection_item"]) ? $context["omit_collection_item"] : $this->getContext($context, "omit_collection_item"))))) {
                    // line 286
                    echo "            <div class=\"collection-item ";
                    echo twig_escape_filter($this->env, twig_join_filter((($this->getAttribute((isset($context["widget_items_attr"]) ? $context["widget_items_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_items_attr"]) ? $context["widget_items_attr"] : null), "class", array()))) : ("")), " "), "html", null, true);
                    echo "\" id=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["child"]) ? $context["child"] : $this->getContext($context, "child")), "vars", array()), "id", array()), "html", null, true);
                    echo "_control_group\">
            ";
                }
                // line 288
                echo "            ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["child"]) ? $context["child"] : $this->getContext($context, "child")), 'row');
                echo "
            ";
                // line 289
                if ((twig_in_filter("collection", $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "block_prefixes", array())) && (!(isset($context["omit_collection_item"]) ? $context["omit_collection_item"] : $this->getContext($context, "omit_collection_item"))))) {
                    // line 290
                    echo "            </div>
            ";
                }
                // line 292
                echo "        ";
            }
            // line 293
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 297
    public function block_form_row($context, array $blocks = array())
    {
        // line 298
        ob_start();
        // line 299
        echo "    ";
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => ((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . (((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) ? (" error") : ("")))));
        // line 300
        echo "    ";
        $this->displayBlock("widget_control_group_start", $context, $blocks);
        echo "
    ";
        // line 301
        echo $this->env->getExtension('translator')->trans((isset($context["widget_prefix"]) ? $context["widget_prefix"] : $this->getContext($context, "widget_prefix")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")));
        echo " ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget', $context);
        echo " ";
        echo $this->env->getExtension('translator')->trans((isset($context["widget_suffix"]) ? $context["widget_suffix"] : $this->getContext($context, "widget_suffix")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")));
        echo "
    ";
        // line 302
        if (array_key_exists("widget_remove_btn", $context)) {
            // line 303
            echo "        ";
            $this->displayBlock("form_widget_remove_btn", $context, $blocks);
            echo "
    ";
        }
        // line 305
        $this->displayBlock("form_message", $context, $blocks);
        echo "
    ";
        // line 306
        $this->displayBlock("widget_control_group_end", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 312
    public function block_form_message($context, array $blocks = array())
    {
        // line 313
        ob_start();
        // line 314
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
    ";
        // line 315
        $this->displayBlock("form_help", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 321
    public function block_form_help($context, array $blocks = array())
    {
        // line 322
        ob_start();
        // line 323
        if (!twig_in_filter("checkbox", $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "block_prefixes", array()))) {
            // line 324
            echo "    ";
            if ((isset($context["help_inline"]) ? $context["help_inline"] : $this->getContext($context, "help_inline"))) {
                echo "<p class=\"help-inline\">";
                echo $this->env->getExtension('translator')->trans((isset($context["help_inline"]) ? $context["help_inline"] : $this->getContext($context, "help_inline")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")));
                echo "</p>";
            }
        }
        // line 326
        if ((isset($context["help_block"]) ? $context["help_block"] : $this->getContext($context, "help_block"))) {
            echo "<p class=\"help-block\">";
            echo $this->env->getExtension('translator')->trans((isset($context["help_block"]) ? $context["help_block"] : $this->getContext($context, "help_block")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")));
            echo "</p>";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 330
    public function block_form_widget_add_btn($context, array $blocks = array())
    {
        // line 331
        ob_start();
        // line 332
        echo "    ";
        if ((isset($context["widget_add_btn"]) ? $context["widget_add_btn"] : $this->getContext($context, "widget_add_btn"))) {
            // line 333
            echo "    ";
            $context["button_type"] = "add";
            // line 334
            echo "    ";
            $context["button_values"] = (isset($context["widget_add_btn"]) ? $context["widget_add_btn"] : $this->getContext($context, "widget_add_btn"));
            // line 335
            echo "    ";
            $this->displayBlock("collection_button", $context, $blocks);
            echo "
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 340
    public function block_form_widget_remove_btn($context, array $blocks = array())
    {
        // line 341
        ob_start();
        // line 342
        echo "    ";
        if ((isset($context["widget_remove_btn"]) ? $context["widget_remove_btn"] : $this->getContext($context, "widget_remove_btn"))) {
            // line 343
            echo "    ";
            $context["button_type"] = "remove";
            // line 344
            echo "    ";
            $context["button_values"] = (isset($context["widget_remove_btn"]) ? $context["widget_remove_btn"] : $this->getContext($context, "widget_remove_btn"));
            // line 345
            echo "    ";
            $this->displayBlock("collection_button", $context, $blocks);
            echo "
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 350
    public function block_collection_button($context, array $blocks = array())
    {
        // line 351
        echo "<a ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["button_values"]) ? $context["button_values"] : $this->getContext($context, "button_values")), "attr", array()));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["attrname"]) ? $context["attrname"] : $this->getContext($context, "attrname")), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, (isset($context["attrvalue"]) ? $context["attrvalue"] : $this->getContext($context, "attrvalue")), "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " data-collection-";
        echo twig_escape_filter($this->env, (isset($context["button_type"]) ? $context["button_type"] : $this->getContext($context, "button_type")), "html", null, true);
        echo "-btn=\"#";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "id", array(), "array"), "html", null, true);
        echo "_control_group\">
";
        // line 352
        if ($this->getAttribute((isset($context["button_values"]) ? $context["button_values"] : null), "icon", array(), "any", true, true)) {
            // line 353
            echo "<i class=\"icon-";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["button_values"]) ? $context["button_values"] : $this->getContext($context, "button_values")), "icon", array()), "html", null, true);
            echo " ";
            if ($this->getAttribute((isset($context["button_values"]) ? $context["button_values"] : null), "icon_color", array(), "any", true, true)) {
                echo "icon-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["button_values"]) ? $context["button_values"] : $this->getContext($context, "button_values")), "icon_color", array()), "html", null, true);
            }
            echo "\"></i>
";
        }
        // line 355
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["button_values"]) ? $context["button_values"] : $this->getContext($context, "button_values")), "label", array()), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
        echo "
</a>

";
    }

    // line 360
    public function block_label_asterisk($context, array $blocks = array())
    {
        // line 361
        if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
            // line 362
            echo "    ";
            if ((isset($context["render_required_asterisk"]) ? $context["render_required_asterisk"] : $this->getContext($context, "render_required_asterisk"))) {
                echo "<span>*</span>";
            }
        } else {
            // line 364
            echo "    ";
            if ((isset($context["render_optional_text"]) ? $context["render_optional_text"] : $this->getContext($context, "render_optional_text"))) {
                echo "<span>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("(optional)", array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
                echo "</span>";
            }
        }
    }

    // line 368
    public function block_widget_addon($context, array $blocks = array())
    {
        // line 369
        ob_start();
        // line 371
        $context["__internal_2c2ad03a83963c4a9fb501e23efa33ffbc96c54803c2f01a86377c2c54300c13"] = $this->env->loadTemplate("MopaBootstrapBundle::icons.html.twig");
        // line 372
        echo "<span class=\"add-on\">";
        echo (((($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "text", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "text", array()), false)) : (false))) ? ($this->env->getExtension('translator')->trans($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : $this->getContext($context, "widget_addon")), "text", array()), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")))) : ($context["__internal_2c2ad03a83963c4a9fb501e23efa33ffbc96c54803c2f01a86377c2c54300c13"]->geticon($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : $this->getContext($context, "widget_addon")), "icon", array()))));
        echo " ";
        echo (((($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "html", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "html", array()), false)) : (false))) ? ($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : $this->getContext($context, "widget_addon")), "html", array())) : (""));
        echo "</span>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 378
    public function block__form_errors($context, array $blocks = array())
    {
        // line 379
        ob_start();
        // line 380
        echo "    ";
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 381
            echo "    <ul>
        ";
            // line 382
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 383
                echo "            <li>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "message", array()), "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 385
            echo "    </ul>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 390
    public function block_form_errors($context, array $blocks = array())
    {
        // line 391
        ob_start();
        // line 392
        if (((isset($context["errors_on_forms"]) ? $context["errors_on_forms"] : $this->getContext($context, "errors_on_forms")) && ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()) == null))) {
            // line 393
            echo "    ";
            // line 394
            echo "    ";
            $context["__internal_1c7befa4d480569370c621aafa261c529921e977c0fee107f699c666975ad1b1"] = $this->env->loadTemplate("MopaBootstrapBundle::flash.html.twig");
            // line 395
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 396
                echo "        ";
                echo $context["__internal_1c7befa4d480569370c621aafa261c529921e977c0fee107f699c666975ad1b1"]->getflash("error", (((null === $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messagePluralization", array()))) ? ($this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageTemplate", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageParameters", array()), "validators")) : ($this->env->getExtension('translator')->transchoice($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageTemplate", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messagePluralization", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageParameters", array()), "validators"))));
                // line 403
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } elseif ((isset($context["error_delay"]) ? $context["error_delay"] : $this->getContext($context, "error_delay"))) {
            // line 406
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 407
                echo "        ";
                if (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index", array()) == 1)) {
                    // line 408
                    echo "            ";
                    if ($this->getAttribute((isset($context["child"]) ? $context["child"] : $this->getContext($context, "child")), "set", array(0 => "errors", 1 => (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))), "method")) {
                    }
                    // line 409
                    echo "        ";
                }
                // line 410
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 412
            echo "    ";
            if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
                // line 413
                echo "    <span class=\"help-";
                $this->displayBlock("error_type", $context, $blocks);
                echo "\">
        <span class=\"text-error\">
            ";
                // line 415
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
                foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                    // line 416
                    echo "                ";
                    echo twig_escape_filter($this->env, (((null === $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messagePluralization", array()))) ? ($this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageTemplate", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageParameters", array()), "validators")) : ($this->env->getExtension('translator')->transchoice($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageTemplate", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messagePluralization", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageParameters", array()), "validators"))), "html", null, true);
                    // line 420
                    echo " <br>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 422
                echo "        </span>
    </span>
    ";
            }
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 432
    public function block_error_type($context, array $blocks = array())
    {
        // line 433
        ob_start();
        // line 434
        if ((isset($context["error_type"]) ? $context["error_type"] : $this->getContext($context, "error_type"))) {
            // line 435
            echo "    ";
            echo twig_escape_filter($this->env, (isset($context["error_type"]) ? $context["error_type"] : $this->getContext($context, "error_type")), "html", null, true);
            echo "
";
        } elseif (($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()) == null)) {
            // line 437
            echo "    ";
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array(), "any", false, true), "error_type", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array(), "any", false, true), "error_type", array()), "inline")) : ("inline")), "html", null, true);
            echo "
";
        } else {
            // line 439
            echo "inline
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 446
    public function block_widget_control_group_start($context, array $blocks = array())
    {
        // line 447
        ob_start();
        // line 448
        if ((((array_key_exists("widget_control_group", $context)) ? (_twig_default_filter((isset($context["widget_control_group"]) ? $context["widget_control_group"] : $this->getContext($context, "widget_control_group")), false)) : (false)) || ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()) == null))) {
            // line 449
            echo "    ";
            if (array_key_exists("prototype", $context)) {
                // line 450
                echo "        ";
                $context["data_prototype"] = (((twig_in_filter("collection", $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "block_prefixes", array())) && (!(isset($context["omit_collection_item"]) ? $context["omit_collection_item"] : $this->getContext($context, "omit_collection_item"))))) ? ((((((("<div class=\"collection-item " . twig_join_filter((($this->getAttribute((isset($context["widget_items_attr"]) ? $context["widget_items_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_items_attr"]) ? $context["widget_items_attr"] : null), "class", array()))) : ("")), " ")) . "\" id=\"") . $this->getAttribute($this->getAttribute((isset($context["prototype"]) ? $context["prototype"] : $this->getContext($context, "prototype")), "vars", array()), "id", array())) . "_control_group\">") . $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["prototype"]) ? $context["prototype"] : $this->getContext($context, "prototype")), 'row')) . "</div>")) : ($this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["prototype"]) ? $context["prototype"] : $this->getContext($context, "prototype")), 'row')));
                // line 451
                echo "        ";
                $context["data_prototype_name"] = (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array(), "any", false, true), "form", array(), "any", false, true), "vars", array(), "any", false, true), "prototype", array(), "any", false, true), "vars", array(), "any", false, true), "name", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array(), "any", false, true), "form", array(), "any", false, true), "vars", array(), "any", false, true), "prototype", array(), "any", false, true), "vars", array(), "any", false, true), "name", array()), "__name__")) : ("__name__"));
                // line 452
                echo "        ";
                $context["widget_control_group_attr"] = twig_array_merge(twig_array_merge((isset($context["widget_control_group_attr"]) ? $context["widget_control_group_attr"] : $this->getContext($context, "widget_control_group_attr")), array("data-prototype" => (isset($context["data_prototype"]) ? $context["data_prototype"] : $this->getContext($context, "data_prototype")), "data-prototype-name" => (isset($context["data_prototype_name"]) ? $context["data_prototype_name"] : $this->getContext($context, "data_prototype_name")), "data-widget-controls" => ((((array_key_exists("widget_controls", $context)) ? (_twig_default_filter((isset($context["widget_controls"]) ? $context["widget_controls"] : $this->getContext($context, "widget_controls")), false)) : (false))) ? ("true") : ("false")))), (isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")));
                // line 453
                echo "    ";
            }
            // line 454
            echo "    ";
            $context["widget_control_group_attr"] = twig_array_merge((isset($context["widget_control_group_attr"]) ? $context["widget_control_group_attr"] : $this->getContext($context, "widget_control_group_attr")), array("id" => ((isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")) . "_control_group"), "class" => ((($this->getAttribute((isset($context["widget_control_group_attr"]) ? $context["widget_control_group_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_control_group_attr"]) ? $context["widget_control_group_attr"] : null), "class", array()), "")) : ("")) . " control-group")));
            // line 455
            echo "    ";
            if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
                // line 456
                echo "        ";
                $context["widget_control_group_attr"] = twig_array_merge((isset($context["widget_control_group_attr"]) ? $context["widget_control_group_attr"] : $this->getContext($context, "widget_control_group_attr")), array("class" => ((($this->getAttribute((isset($context["widget_control_group_attr"]) ? $context["widget_control_group_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_control_group_attr"]) ? $context["widget_control_group_attr"] : null), "class", array()), "")) : ("")) . " error")));
                // line 457
                echo "    ";
            }
            // line 458
            echo "\t";
            if ((twig_in_filter("collection", $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "block_prefixes", array())) && $this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true))) {
                // line 459
                echo "\t\t";
                $context["widget_control_group_attr"] = twig_array_merge((isset($context["widget_control_group_attr"]) ? $context["widget_control_group_attr"] : $this->getContext($context, "widget_control_group_attr")), array("class" => (((($this->getAttribute((isset($context["widget_control_group_attr"]) ? $context["widget_control_group_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_control_group_attr"]) ? $context["widget_control_group_attr"] : null), "class", array()), "")) : ("")) . " ") . $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "class", array()))));
                // line 460
                echo "\t";
            }
            // line 461
            echo "    <div ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["widget_control_group_attr"]) ? $context["widget_control_group_attr"] : $this->getContext($context, "widget_control_group_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, (isset($context["attrname"]) ? $context["attrname"] : $this->getContext($context, "attrname")), "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, (isset($context["attrvalue"]) ? $context["attrvalue"] : $this->getContext($context, "attrvalue")), "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">
    ";
            // line 463
            echo "    ";
            if ((((twig_length_filter($this->env, (isset($context["form"]) ? $context["form"] : $this->getContext($context, "form"))) > 0) && ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()) != null)) && !twig_in_filter("field", $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "vars", array()), "block_prefixes", array())))) {
                // line 465
                echo "        ";
                if ((isset($context["show_child_legend"]) ? $context["show_child_legend"] : $this->getContext($context, "show_child_legend"))) {
                    // line 466
                    echo "            ";
                    $this->displayBlock("form_legend", $context, $blocks);
                    echo "
        ";
                } elseif ((isset($context["label_render"]) ? $context["label_render"] : $this->getContext($context, "label_render"))) {
                    // line 468
                    echo "            ";
                    echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', (twig_test_empty($_label_ = ((array_key_exists("label", $context)) ? (_twig_default_filter((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), null)) : (null))) ? array() : array("label" => $_label_)));
                    echo "
        ";
                }
                // line 470
                echo "    ";
            } else {
                // line 471
                echo "        ";
                if ((isset($context["label_render"]) ? $context["label_render"] : $this->getContext($context, "label_render"))) {
                    // line 472
                    echo "            ";
                    echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', (twig_test_empty($_label_ = ((array_key_exists("label", $context)) ? (_twig_default_filter((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), null)) : (null))) ? array() : array("label" => $_label_)));
                    echo "
        ";
                }
                // line 474
                echo "    ";
            }
            // line 475
            echo "    ";
            if ((((array_key_exists("widget_controls", $context)) ? (_twig_default_filter((isset($context["widget_controls"]) ? $context["widget_controls"] : $this->getContext($context, "widget_controls")), false)) : (false)) || ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()) == null))) {
                // line 476
                echo "        ";
                $context["widget_controls_attr"] = twig_array_merge((isset($context["widget_controls_attr"]) ? $context["widget_controls_attr"] : $this->getContext($context, "widget_controls_attr")), array("class" => ((($this->getAttribute((isset($context["widget_controls_attr"]) ? $context["widget_controls_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_controls_attr"]) ? $context["widget_controls_attr"] : null), "class", array()), "")) : ("")) . " controls")));
                // line 477
                echo "        <div ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["widget_controls_attr"]) ? $context["widget_controls_attr"] : $this->getContext($context, "widget_controls_attr")));
                foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                    echo twig_escape_filter($this->env, (isset($context["attrname"]) ? $context["attrname"] : $this->getContext($context, "attrname")), "html", null, true);
                    echo "=\"";
                    echo twig_escape_filter($this->env, (isset($context["attrvalue"]) ? $context["attrvalue"] : $this->getContext($context, "attrvalue")), "html", null, true);
                    echo "\" ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo ">
    ";
            }
        } else {
            // line 480
            echo "    ";
            if ((isset($context["label_render"]) ? $context["label_render"] : $this->getContext($context, "label_render"))) {
                // line 481
                echo "        ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', (twig_test_empty($_label_ = ((array_key_exists("label", $context)) ? (_twig_default_filter((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), null)) : (null))) ? array() : array("label" => $_label_)));
                echo "
    ";
            }
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 487
    public function block_widget_control_group_end($context, array $blocks = array())
    {
        // line 488
        ob_start();
        // line 489
        if ((((array_key_exists("widget_control_group", $context)) ? (_twig_default_filter((isset($context["widget_control_group"]) ? $context["widget_control_group"] : $this->getContext($context, "widget_control_group")), false)) : (false)) || ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()) == null))) {
            // line 490
            echo "    ";
            if ((((array_key_exists("widget_controls", $context)) ? (_twig_default_filter((isset($context["widget_controls"]) ? $context["widget_controls"] : $this->getContext($context, "widget_controls")), false)) : (false)) || ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()) == null))) {
                // line 491
                echo "        </div>
    ";
            }
            // line 493
            echo "    </div>
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "MopaBootstrapBundle:Form:fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1405 => 493,  1401 => 491,  1398 => 490,  1396 => 489,  1394 => 488,  1391 => 487,  1381 => 481,  1361 => 477,  1358 => 476,  1352 => 474,  1346 => 472,  1343 => 471,  1334 => 468,  1328 => 466,  1325 => 465,  1322 => 463,  1306 => 461,  1303 => 460,  1300 => 459,  1297 => 458,  1291 => 456,  1285 => 454,  1282 => 453,  1276 => 451,  1270 => 449,  1268 => 448,  1266 => 447,  1263 => 446,  1244 => 435,  1242 => 434,  1240 => 433,  1228 => 422,  1221 => 420,  1214 => 415,  1208 => 413,  1205 => 412,  1190 => 410,  1187 => 409,  1183 => 408,  1162 => 406,  1151 => 396,  1143 => 394,  1139 => 392,  1137 => 391,  1118 => 383,  1106 => 379,  1103 => 378,  1093 => 372,  1091 => 371,  1086 => 368,  1076 => 364,  1070 => 362,  1068 => 361,  1057 => 355,  1046 => 353,  1021 => 350,  1003 => 342,  1001 => 341,  998 => 340,  989 => 335,  983 => 333,  975 => 330,  966 => 326,  958 => 324,  956 => 323,  954 => 322,  944 => 315,  939 => 314,  937 => 313,  934 => 312,  927 => 306,  907 => 301,  899 => 299,  894 => 297,  882 => 292,  878 => 290,  876 => 289,  857 => 284,  852 => 283,  829 => 273,  810 => 267,  807 => 266,  783 => 254,  780 => 253,  750 => 243,  709 => 234,  699 => 230,  690 => 225,  653 => 209,  599 => 188,  597 => 187,  565 => 177,  536 => 163,  496 => 144,  385 => 111,  502 => 279,  723 => 375,  717 => 372,  701 => 359,  679 => 349,  673 => 346,  655 => 337,  521 => 268,  489 => 251,  635 => 330,  631 => 328,  574 => 286,  566 => 283,  533 => 274,  505 => 244,  387 => 213,  364 => 64,  352 => 196,  991 => 405,  908 => 300,  906 => 299,  901 => 296,  887 => 280,  880 => 257,  862 => 243,  856 => 240,  830 => 224,  820 => 218,  806 => 217,  799 => 214,  789 => 256,  785 => 209,  779 => 207,  743 => 197,  720 => 192,  714 => 191,  697 => 358,  666 => 178,  660 => 177,  647 => 332,  638 => 174,  517 => 158,  850 => 282,  843 => 383,  836 => 379,  825 => 221,  823 => 373,  817 => 370,  808 => 365,  802 => 363,  777 => 350,  768 => 346,  757 => 204,  729 => 329,  726 => 328,  719 => 325,  713 => 321,  707 => 318,  704 => 232,  691 => 355,  684 => 309,  678 => 305,  672 => 219,  669 => 218,  659 => 297,  656 => 210,  642 => 204,  610 => 270,  575 => 295,  527 => 231,  511 => 225,  491 => 215,  476 => 209,  421 => 210,  415 => 207,  383 => 110,  736 => 438,  732 => 437,  685 => 186,  648 => 380,  568 => 178,  528 => 314,  524 => 256,  460 => 219,  425 => 220,  374 => 214,  324 => 123,  661 => 212,  636 => 374,  629 => 323,  618 => 365,  589 => 303,  587 => 382,  559 => 175,  547 => 237,  537 => 164,  530 => 345,  510 => 302,  453 => 133,  392 => 226,  369 => 191,  1843 => 1037,  1834 => 1030,  1747 => 947,  1742 => 944,  1728 => 936,  1718 => 929,  1706 => 920,  1694 => 915,  1692 => 914,  1683 => 908,  1677 => 905,  1668 => 899,  1660 => 894,  1656 => 893,  1652 => 892,  1645 => 888,  1641 => 887,  1630 => 882,  1626 => 881,  1622 => 880,  1614 => 874,  1601 => 832,  1595 => 829,  1586 => 823,  1582 => 822,  1576 => 819,  1567 => 813,  1563 => 812,  1557 => 809,  1547 => 802,  1543 => 801,  1537 => 798,  1528 => 792,  1518 => 788,  1500 => 773,  1496 => 772,  1492 => 771,  1488 => 770,  1482 => 767,  1473 => 761,  1469 => 760,  1465 => 759,  1461 => 758,  1448 => 750,  1438 => 748,  1436 => 747,  1432 => 746,  1428 => 745,  1422 => 742,  1413 => 736,  1409 => 735,  1403 => 732,  1389 => 724,  1370 => 711,  1364 => 708,  1351 => 701,  1345 => 698,  1332 => 691,  1326 => 688,  1313 => 681,  1307 => 678,  1294 => 457,  1288 => 455,  1275 => 661,  1269 => 658,  1256 => 439,  1250 => 437,  1237 => 432,  1231 => 638,  1218 => 416,  1212 => 628,  1199 => 621,  1193 => 618,  1180 => 407,  1155 => 598,  1142 => 591,  1136 => 588,  1127 => 385,  1117 => 578,  1108 => 380,  1098 => 568,  1079 => 558,  1071 => 552,  1067 => 550,  1065 => 360,  1054 => 541,  1050 => 540,  1044 => 352,  1035 => 531,  1031 => 530,  1025 => 527,  1016 => 521,  1006 => 343,  997 => 511,  993 => 510,  987 => 507,  978 => 331,  974 => 500,  959 => 491,  955 => 490,  949 => 487,  940 => 481,  936 => 480,  930 => 477,  921 => 471,  911 => 467,  902 => 300,  898 => 460,  892 => 457,  883 => 258,  879 => 450,  864 => 441,  854 => 437,  845 => 431,  835 => 226,  826 => 272,  816 => 417,  805 => 411,  801 => 410,  797 => 262,  791 => 406,  778 => 399,  772 => 348,  763 => 343,  759 => 389,  753 => 386,  744 => 380,  740 => 379,  721 => 238,  715 => 236,  687 => 350,  677 => 221,  668 => 340,  643 => 331,  624 => 195,  614 => 393,  605 => 304,  595 => 266,  538 => 165,  519 => 257,  485 => 250,  481 => 154,  447 => 246,  262 => 136,  281 => 136,  197 => 49,  419 => 185,  308 => 85,  260 => 133,  228 => 117,  355 => 165,  346 => 193,  288 => 150,  692 => 188,  603 => 170,  561 => 287,  515 => 265,  475 => 138,  379 => 108,  348 => 111,  298 => 114,  1174 => 608,  1167 => 718,  1161 => 601,  1154 => 403,  1149 => 707,  1141 => 393,  1134 => 390,  1131 => 701,  1129 => 700,  1114 => 382,  1111 => 381,  1109 => 693,  1105 => 692,  1096 => 689,  1092 => 688,  1089 => 369,  1072 => 675,  1064 => 670,  1042 => 651,  1034 => 645,  1026 => 642,  1022 => 640,  1019 => 639,  1017 => 638,  1012 => 345,  1008 => 635,  1004 => 633,  1002 => 632,  996 => 629,  988 => 626,  984 => 624,  979 => 623,  977 => 622,  968 => 497,  962 => 613,  951 => 321,  917 => 303,  915 => 302,  912 => 328,  905 => 580,  897 => 298,  893 => 574,  881 => 568,  873 => 447,  871 => 288,  863 => 286,  853 => 496,  842 => 231,  831 => 376,  812 => 464,  800 => 263,  798 => 455,  787 => 447,  754 => 244,  747 => 390,  739 => 333,  731 => 410,  725 => 370,  718 => 237,  696 => 356,  681 => 401,  675 => 182,  665 => 341,  663 => 298,  651 => 359,  628 => 196,  606 => 269,  602 => 189,  600 => 356,  579 => 296,  572 => 311,  555 => 303,  542 => 269,  532 => 161,  513 => 254,  501 => 329,  499 => 271,  473 => 153,  441 => 243,  438 => 209,  416 => 200,  330 => 170,  289 => 108,  633 => 320,  627 => 398,  621 => 289,  619 => 319,  594 => 186,  576 => 344,  570 => 254,  562 => 163,  558 => 335,  552 => 251,  540 => 166,  508 => 283,  498 => 314,  486 => 140,  480 => 262,  454 => 249,  450 => 132,  410 => 115,  325 => 102,  220 => 111,  407 => 224,  402 => 194,  377 => 107,  313 => 153,  232 => 118,  465 => 258,  457 => 192,  417 => 215,  411 => 225,  408 => 114,  396 => 197,  391 => 214,  388 => 222,  372 => 105,  344 => 109,  332 => 182,  293 => 80,  274 => 72,  231 => 99,  200 => 50,  792 => 418,  788 => 356,  782 => 208,  776 => 439,  774 => 251,  762 => 247,  748 => 200,  742 => 334,  734 => 193,  730 => 241,  724 => 239,  716 => 403,  706 => 233,  698 => 314,  694 => 313,  688 => 224,  680 => 222,  676 => 358,  662 => 350,  658 => 211,  652 => 411,  644 => 405,  640 => 203,  634 => 337,  622 => 274,  616 => 193,  608 => 191,  598 => 167,  580 => 165,  564 => 338,  545 => 353,  541 => 321,  526 => 159,  507 => 260,  488 => 321,  462 => 227,  433 => 207,  424 => 184,  395 => 223,  382 => 219,  376 => 207,  341 => 93,  327 => 161,  320 => 166,  310 => 159,  291 => 129,  278 => 73,  259 => 120,  244 => 117,  448 => 131,  443 => 129,  429 => 121,  406 => 195,  366 => 182,  318 => 87,  282 => 147,  258 => 71,  222 => 58,  120 => 25,  272 => 140,  266 => 136,  226 => 114,  178 => 80,  111 => 34,  393 => 113,  386 => 196,  380 => 193,  362 => 184,  358 => 207,  342 => 108,  340 => 177,  334 => 91,  326 => 169,  319 => 134,  314 => 144,  299 => 82,  265 => 89,  252 => 129,  237 => 112,  194 => 104,  132 => 65,  23 => 2,  97 => 52,  81 => 47,  53 => 30,  654 => 176,  637 => 202,  632 => 280,  625 => 322,  623 => 324,  617 => 195,  612 => 362,  604 => 357,  593 => 327,  591 => 186,  586 => 181,  583 => 180,  578 => 379,  571 => 164,  557 => 286,  534 => 242,  522 => 288,  520 => 340,  504 => 248,  494 => 216,  463 => 262,  446 => 130,  440 => 136,  434 => 141,  431 => 223,  427 => 182,  405 => 233,  401 => 221,  397 => 114,  389 => 220,  381 => 210,  357 => 62,  349 => 205,  339 => 92,  303 => 149,  295 => 113,  287 => 76,  268 => 139,  74 => 10,  470 => 398,  452 => 248,  444 => 212,  435 => 123,  430 => 235,  414 => 183,  412 => 198,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 940,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 916,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 886,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 833,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 791,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 755,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 725,  1387 => 694,  1378 => 480,  1374 => 712,  1368 => 684,  1359 => 678,  1355 => 475,  1349 => 674,  1340 => 470,  1336 => 692,  1330 => 664,  1321 => 658,  1317 => 682,  1311 => 654,  1302 => 648,  1298 => 672,  1292 => 644,  1283 => 638,  1279 => 452,  1273 => 450,  1264 => 628,  1260 => 652,  1254 => 624,  1245 => 618,  1241 => 642,  1235 => 614,  1226 => 608,  1222 => 632,  1216 => 604,  1207 => 598,  1203 => 622,  1197 => 594,  1188 => 588,  1184 => 612,  1178 => 584,  1169 => 578,  1165 => 602,  1159 => 574,  1150 => 568,  1146 => 395,  1140 => 564,  1123 => 581,  1119 => 697,  1113 => 546,  1104 => 571,  1100 => 690,  1094 => 536,  1085 => 561,  1081 => 529,  1075 => 526,  1066 => 671,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 351,  1018 => 496,  1009 => 344,  1005 => 489,  999 => 486,  990 => 480,  986 => 334,  980 => 332,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 599,  933 => 450,  929 => 588,  923 => 305,  914 => 440,  910 => 439,  904 => 298,  895 => 430,  891 => 282,  885 => 293,  874 => 420,  870 => 419,  866 => 418,  860 => 285,  851 => 409,  847 => 281,  841 => 382,  832 => 274,  828 => 398,  822 => 420,  813 => 268,  809 => 388,  803 => 216,  794 => 359,  790 => 357,  784 => 354,  775 => 369,  771 => 250,  765 => 248,  756 => 245,  752 => 202,  746 => 242,  737 => 383,  733 => 330,  727 => 240,  712 => 235,  708 => 419,  702 => 231,  693 => 323,  689 => 187,  683 => 223,  674 => 220,  670 => 355,  664 => 339,  649 => 293,  645 => 205,  639 => 323,  630 => 371,  626 => 276,  620 => 194,  611 => 192,  607 => 313,  601 => 310,  592 => 298,  588 => 261,  582 => 347,  563 => 348,  554 => 173,  550 => 246,  544 => 161,  535 => 162,  531 => 294,  516 => 155,  512 => 318,  506 => 148,  493 => 294,  478 => 210,  468 => 255,  459 => 200,  455 => 209,  449 => 232,  436 => 216,  428 => 236,  422 => 233,  409 => 226,  403 => 223,  390 => 216,  384 => 213,  351 => 206,  337 => 126,  311 => 161,  296 => 81,  256 => 104,  241 => 80,  215 => 101,  207 => 96,  192 => 48,  186 => 92,  175 => 149,  153 => 69,  118 => 35,  61 => 5,  34 => 14,  65 => 35,  77 => 11,  37 => 10,  190 => 93,  161 => 35,  137 => 65,  126 => 63,  261 => 123,  255 => 197,  247 => 98,  242 => 125,  214 => 96,  211 => 53,  191 => 157,  157 => 59,  145 => 33,  127 => 61,  373 => 68,  367 => 102,  363 => 170,  359 => 99,  354 => 207,  343 => 174,  335 => 145,  328 => 89,  322 => 178,  315 => 150,  309 => 152,  305 => 146,  302 => 83,  290 => 148,  284 => 152,  279 => 132,  271 => 89,  264 => 122,  248 => 128,  236 => 110,  223 => 76,  170 => 77,  110 => 50,  96 => 48,  84 => 48,  472 => 137,  467 => 241,  375 => 106,  371 => 206,  360 => 210,  356 => 197,  353 => 97,  350 => 96,  338 => 156,  336 => 221,  331 => 90,  321 => 158,  316 => 86,  307 => 160,  304 => 97,  301 => 157,  297 => 141,  292 => 151,  286 => 147,  283 => 127,  277 => 132,  275 => 131,  270 => 137,  263 => 146,  257 => 143,  253 => 53,  249 => 69,  245 => 110,  233 => 110,  225 => 59,  216 => 54,  206 => 103,  202 => 103,  198 => 96,  185 => 86,  180 => 89,  177 => 81,  165 => 75,  150 => 71,  124 => 62,  113 => 55,  100 => 33,  58 => 21,  251 => 119,  234 => 97,  213 => 102,  195 => 94,  174 => 75,  167 => 76,  146 => 70,  140 => 66,  128 => 27,  104 => 51,  90 => 46,  83 => 44,  52 => 9,  596 => 225,  590 => 297,  585 => 221,  577 => 357,  573 => 179,  569 => 292,  560 => 296,  556 => 174,  553 => 162,  551 => 172,  546 => 207,  543 => 167,  539 => 277,  529 => 160,  525 => 269,  523 => 158,  518 => 287,  514 => 286,  509 => 278,  503 => 259,  500 => 145,  497 => 256,  495 => 313,  492 => 142,  490 => 271,  487 => 213,  484 => 139,  482 => 177,  479 => 247,  477 => 271,  474 => 206,  471 => 261,  469 => 276,  466 => 135,  464 => 134,  461 => 238,  458 => 251,  456 => 199,  451 => 247,  445 => 244,  442 => 258,  439 => 248,  437 => 214,  432 => 122,  426 => 120,  423 => 141,  420 => 118,  418 => 117,  413 => 116,  399 => 137,  394 => 217,  378 => 119,  370 => 183,  368 => 187,  365 => 203,  361 => 100,  347 => 95,  345 => 160,  333 => 159,  329 => 158,  323 => 182,  317 => 179,  312 => 133,  306 => 158,  300 => 155,  294 => 116,  285 => 193,  280 => 144,  276 => 176,  267 => 128,  250 => 126,  239 => 113,  229 => 103,  218 => 74,  212 => 107,  210 => 104,  205 => 51,  188 => 88,  184 => 46,  181 => 81,  169 => 79,  160 => 78,  152 => 74,  148 => 34,  139 => 31,  134 => 79,  114 => 59,  107 => 52,  76 => 20,  70 => 28,  273 => 131,  269 => 128,  254 => 70,  246 => 68,  243 => 67,  240 => 66,  238 => 65,  235 => 64,  230 => 115,  227 => 60,  224 => 104,  221 => 104,  219 => 98,  217 => 100,  208 => 106,  204 => 99,  179 => 44,  159 => 75,  143 => 65,  135 => 60,  131 => 61,  108 => 56,  102 => 47,  71 => 29,  67 => 7,  63 => 34,  59 => 36,  47 => 13,  94 => 51,  89 => 28,  85 => 14,  79 => 12,  75 => 43,  68 => 36,  56 => 2,  50 => 29,  38 => 16,  29 => 24,  87 => 49,  72 => 23,  55 => 20,  21 => 2,  26 => 9,  35 => 5,  31 => 12,  41 => 25,  28 => 10,  201 => 87,  196 => 88,  183 => 152,  171 => 81,  166 => 37,  163 => 36,  156 => 79,  151 => 70,  142 => 32,  138 => 69,  136 => 30,  123 => 26,  121 => 61,  115 => 23,  105 => 55,  101 => 41,  91 => 50,  69 => 40,  66 => 39,  62 => 19,  49 => 17,  98 => 39,  93 => 47,  88 => 15,  78 => 30,  46 => 13,  44 => 26,  32 => 7,  27 => 5,  43 => 12,  40 => 6,  25 => 3,  24 => 8,  172 => 39,  158 => 80,  155 => 75,  129 => 64,  119 => 34,  117 => 24,  20 => 1,  22 => 20,  19 => 1,  209 => 52,  203 => 95,  199 => 90,  193 => 93,  189 => 87,  187 => 47,  182 => 45,  176 => 91,  173 => 80,  168 => 80,  164 => 47,  162 => 74,  154 => 71,  149 => 73,  147 => 74,  144 => 73,  141 => 70,  133 => 69,  130 => 28,  125 => 59,  122 => 48,  116 => 44,  112 => 22,  109 => 40,  106 => 49,  103 => 34,  99 => 42,  95 => 16,  92 => 26,  86 => 45,  82 => 13,  80 => 43,  73 => 23,  64 => 6,  60 => 22,  57 => 11,  54 => 34,  51 => 18,  48 => 7,  45 => 30,  42 => 29,  39 => 8,  36 => 11,  33 => 22,  30 => 21,);
    }
}
