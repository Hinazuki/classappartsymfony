<?php

/* PatCompteBundle:AdminProprietaire:listeAppartement_content.html.twig */
class __TwigTemplate_7eb3695a47dfdf8bdfdb3e6433e63dd63c70a1ef5aaddcbdf1788e5aa9a37629 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "
  <div id=\"contenuCentrale1\">
    <h1>Biens <small>(";
        // line 4
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["proprietaire"]) ? $context["proprietaire"] : $this->getContext($context, "proprietaire")), "prenom", array())), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["proprietaire"]) ? $context["proprietaire"] : $this->getContext($context, "proprietaire")), "nom", array())), "html", null, true);
        echo ")</small></h1>

    <div class=\"clear\"></div>

    <div class=\"etapesBien\">
      <div class=\"linksEtapesBien\">
        <ul>
          <li><a href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_proprietaire_afficher", array("id_proprio" => $this->getAttribute((isset($context["proprietaire"]) ? $context["proprietaire"] : $this->getContext($context, "proprietaire")), "id", array()))), "html", null, true);
        echo "\">Profil</a></li>
          <li><a class=\"active\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_proprietaire_appartement_index", array("id_proprio" => $this->getAttribute((isset($context["proprietaire"]) ? $context["proprietaire"] : $this->getContext($context, "proprietaire")), "id", array()))), "html", null, true);
        echo "\">Biens</a></li>
          <li><a href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_proprietaire_reservation_index", array("id_proprio" => $this->getAttribute((isset($context["proprietaire"]) ? $context["proprietaire"] : $this->getContext($context, "proprietaire")), "id", array()))), "html", null, true);
        echo "\">Réservations Propriétaire</a></li>
          <li><a href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_proprietaire_resas_loc", array("id_proprio" => $this->getAttribute((isset($context["proprietaire"]) ? $context["proprietaire"] : $this->getContext($context, "proprietaire")), "id", array()))), "html", null, true);
        echo "\">Réservations Locataire</a></li>
          <li><a href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_list_payment_by_member", array("member_id" => $this->getAttribute((isset($context["proprietaire"]) ? $context["proprietaire"] : $this->getContext($context, "proprietaire")), "id", array()))), "html", null, true);
        echo "\">Paiements</a></li>
          <li><a href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("documentsAdmin_list", array("user_id" => $this->getAttribute((isset($context["proprietaire"]) ? $context["proprietaire"] : $this->getContext($context, "proprietaire")), "id", array()))), "html", null, true);
        echo "\">Documents</a></li>
        </ul>
      </div>
    </div>

    <div class=\"right\">
      <a href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_appartement_ajouter", array("id_proprio" => $this->getAttribute((isset($context["proprietaire"]) ? $context["proprietaire"] : $this->getContext($context, "proprietaire")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-primary\">Créer un bien pour ce propriétaire</a>
    </div>

    ";
        // line 25
        if ((isset($context["message"]) ? $context["message"] : $this->getContext($context, "message"))) {
            // line 26
            echo "      <div class=\"success\">
        <p>";
            // line 27
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
            echo "</p>
      </div>
    ";
        }
        // line 30
        echo "
    ";
        // line 31
        $context["__internal_9e22c65dbe56337bb73919f96c45cb732f9ea737c4a17da72de7c31d9923357a"] = $this->env->loadTemplate("MopaBootstrapBundle::flash.html.twig");
        // line 32
        echo "    ";
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "peekAll", array())) > 0)) {
            // line 33
            echo "      <div class=\"row\">
        <div class=\"span12\">
          ";
            // line 35
            echo $context["__internal_9e22c65dbe56337bb73919f96c45cb732f9ea737c4a17da72de7c31d9923357a"]->getsession_flash();
            echo "
        </div>
      </div>
    ";
        }
        // line 39
        echo "
    ";
        // line 40
        $this->env->loadTemplate("PatCompteBundle:AdminAppartement:listeAppart.html.twig")->display($context);
        // line 41
        echo "
  </div><!-- /contenuCentrale1 -->

";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:AdminProprietaire:listeAppartement_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  633 => 293,  627 => 291,  621 => 289,  619 => 288,  594 => 272,  576 => 263,  570 => 260,  562 => 255,  558 => 254,  552 => 251,  540 => 245,  508 => 228,  498 => 224,  486 => 218,  480 => 215,  454 => 201,  450 => 200,  410 => 175,  325 => 131,  220 => 89,  407 => 237,  402 => 234,  377 => 221,  313 => 169,  232 => 107,  465 => 197,  457 => 192,  417 => 178,  411 => 176,  408 => 175,  396 => 232,  391 => 168,  388 => 229,  372 => 160,  344 => 149,  332 => 135,  293 => 130,  274 => 120,  231 => 94,  200 => 50,  792 => 418,  788 => 416,  782 => 414,  776 => 412,  774 => 411,  762 => 402,  748 => 394,  742 => 391,  734 => 386,  730 => 385,  724 => 382,  716 => 377,  706 => 373,  698 => 368,  694 => 367,  688 => 364,  680 => 359,  676 => 358,  662 => 350,  658 => 349,  652 => 346,  644 => 341,  640 => 340,  634 => 337,  622 => 331,  616 => 328,  608 => 323,  598 => 273,  580 => 264,  564 => 297,  545 => 287,  541 => 286,  526 => 237,  507 => 267,  488 => 257,  462 => 206,  433 => 226,  424 => 220,  395 => 169,  382 => 199,  376 => 162,  341 => 173,  327 => 167,  320 => 153,  310 => 125,  291 => 118,  278 => 121,  259 => 122,  244 => 113,  448 => 267,  443 => 230,  429 => 259,  406 => 174,  366 => 206,  318 => 175,  282 => 147,  258 => 129,  222 => 91,  120 => 29,  272 => 129,  266 => 101,  226 => 92,  178 => 66,  111 => 44,  393 => 305,  386 => 166,  380 => 163,  362 => 294,  358 => 207,  342 => 139,  340 => 287,  334 => 284,  326 => 278,  319 => 273,  314 => 271,  299 => 266,  265 => 107,  252 => 137,  237 => 128,  194 => 75,  132 => 32,  23 => 3,  97 => 23,  81 => 25,  53 => 16,  654 => 223,  637 => 295,  632 => 206,  625 => 200,  623 => 199,  617 => 195,  612 => 192,  604 => 322,  593 => 187,  591 => 186,  586 => 313,  583 => 183,  578 => 180,  571 => 178,  557 => 177,  534 => 242,  522 => 236,  520 => 171,  504 => 227,  494 => 158,  463 => 145,  446 => 186,  440 => 136,  434 => 134,  431 => 260,  427 => 182,  405 => 210,  401 => 172,  397 => 114,  389 => 113,  381 => 112,  357 => 148,  349 => 194,  339 => 105,  303 => 99,  295 => 119,  287 => 127,  268 => 117,  74 => 18,  470 => 398,  452 => 236,  444 => 197,  435 => 405,  430 => 397,  414 => 241,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 887,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 864,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 825,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 808,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 763,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 732,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 697,  1387 => 694,  1378 => 688,  1374 => 687,  1368 => 684,  1359 => 678,  1355 => 677,  1349 => 674,  1340 => 668,  1336 => 667,  1330 => 664,  1321 => 658,  1317 => 657,  1311 => 654,  1302 => 648,  1298 => 647,  1292 => 644,  1283 => 638,  1279 => 637,  1273 => 634,  1264 => 628,  1260 => 627,  1254 => 624,  1245 => 618,  1241 => 617,  1235 => 614,  1226 => 608,  1222 => 607,  1216 => 604,  1207 => 598,  1203 => 597,  1197 => 594,  1188 => 588,  1184 => 587,  1178 => 584,  1169 => 578,  1165 => 577,  1159 => 574,  1150 => 568,  1146 => 567,  1140 => 564,  1123 => 550,  1119 => 549,  1113 => 546,  1104 => 540,  1100 => 539,  1094 => 536,  1085 => 530,  1081 => 529,  1075 => 526,  1066 => 520,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 456,  933 => 450,  929 => 449,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 426,  874 => 420,  870 => 419,  866 => 418,  860 => 415,  851 => 409,  847 => 408,  841 => 405,  832 => 399,  828 => 398,  822 => 395,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 365,  756 => 359,  752 => 395,  746 => 355,  737 => 349,  733 => 348,  727 => 345,  712 => 376,  708 => 332,  702 => 329,  693 => 323,  689 => 322,  683 => 319,  674 => 241,  670 => 355,  664 => 309,  649 => 297,  645 => 296,  639 => 293,  630 => 287,  626 => 332,  620 => 283,  611 => 277,  607 => 279,  601 => 189,  592 => 267,  588 => 269,  582 => 263,  563 => 253,  554 => 293,  550 => 246,  544 => 246,  535 => 283,  531 => 236,  516 => 233,  512 => 165,  506 => 163,  493 => 216,  478 => 253,  468 => 209,  459 => 144,  455 => 271,  449 => 193,  436 => 192,  428 => 181,  422 => 395,  409 => 117,  403 => 168,  390 => 230,  384 => 165,  351 => 145,  337 => 185,  311 => 118,  296 => 109,  256 => 104,  241 => 98,  215 => 99,  207 => 66,  192 => 82,  186 => 76,  175 => 71,  153 => 36,  118 => 47,  61 => 24,  34 => 8,  65 => 17,  77 => 25,  37 => 11,  190 => 77,  161 => 60,  137 => 58,  126 => 52,  261 => 106,  255 => 135,  247 => 130,  242 => 127,  214 => 87,  211 => 86,  191 => 76,  157 => 37,  145 => 59,  127 => 47,  373 => 111,  367 => 173,  363 => 155,  359 => 171,  354 => 153,  343 => 135,  335 => 104,  328 => 152,  322 => 175,  315 => 170,  309 => 140,  305 => 115,  302 => 267,  290 => 157,  284 => 154,  279 => 129,  271 => 110,  264 => 122,  248 => 118,  236 => 77,  223 => 82,  170 => 44,  110 => 40,  96 => 33,  84 => 32,  472 => 210,  467 => 148,  375 => 152,  371 => 160,  360 => 183,  356 => 182,  353 => 99,  350 => 151,  338 => 138,  336 => 146,  331 => 169,  321 => 130,  316 => 128,  307 => 269,  304 => 268,  301 => 122,  297 => 131,  292 => 110,  286 => 116,  283 => 105,  277 => 104,  275 => 103,  270 => 138,  263 => 123,  257 => 121,  253 => 108,  249 => 21,  245 => 92,  233 => 88,  225 => 103,  216 => 88,  206 => 87,  202 => 92,  198 => 20,  185 => 75,  180 => 73,  177 => 79,  165 => 65,  150 => 58,  124 => 30,  113 => 32,  100 => 35,  58 => 15,  251 => 93,  234 => 111,  213 => 113,  195 => 78,  174 => 66,  167 => 68,  146 => 63,  140 => 54,  128 => 31,  104 => 29,  90 => 30,  83 => 31,  52 => 11,  596 => 225,  590 => 314,  585 => 221,  577 => 218,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 210,  551 => 176,  546 => 207,  543 => 206,  539 => 205,  529 => 174,  525 => 173,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 266,  500 => 160,  497 => 263,  495 => 182,  492 => 157,  490 => 219,  487 => 213,  484 => 256,  482 => 177,  479 => 176,  477 => 401,  474 => 206,  471 => 173,  469 => 172,  466 => 146,  464 => 397,  461 => 169,  458 => 239,  456 => 167,  451 => 189,  445 => 160,  442 => 159,  439 => 229,  437 => 262,  432 => 191,  426 => 188,  423 => 180,  420 => 219,  418 => 251,  413 => 172,  399 => 237,  394 => 162,  378 => 162,  370 => 159,  368 => 159,  365 => 212,  361 => 149,  347 => 136,  345 => 94,  333 => 131,  329 => 179,  323 => 102,  317 => 272,  312 => 114,  306 => 124,  300 => 110,  294 => 156,  285 => 105,  280 => 113,  276 => 112,  267 => 60,  250 => 101,  239 => 91,  229 => 116,  218 => 101,  212 => 87,  210 => 97,  205 => 83,  188 => 71,  184 => 99,  181 => 74,  169 => 65,  160 => 58,  152 => 63,  148 => 44,  139 => 49,  134 => 49,  114 => 42,  107 => 39,  76 => 27,  70 => 17,  273 => 127,  269 => 94,  254 => 92,  246 => 100,  243 => 88,  240 => 101,  238 => 113,  235 => 95,  230 => 111,  227 => 99,  224 => 103,  221 => 96,  219 => 112,  217 => 84,  208 => 86,  204 => 51,  179 => 71,  159 => 61,  143 => 55,  135 => 37,  131 => 36,  108 => 30,  102 => 25,  71 => 22,  67 => 23,  63 => 16,  59 => 15,  47 => 12,  94 => 40,  89 => 30,  85 => 28,  79 => 26,  75 => 22,  68 => 18,  56 => 15,  50 => 13,  38 => 14,  29 => 4,  87 => 24,  72 => 25,  55 => 14,  21 => 2,  26 => 2,  35 => 4,  31 => 3,  41 => 12,  28 => 2,  201 => 82,  196 => 80,  183 => 72,  171 => 62,  166 => 59,  163 => 52,  156 => 64,  151 => 62,  142 => 50,  138 => 55,  136 => 33,  123 => 45,  121 => 48,  115 => 36,  105 => 40,  101 => 39,  91 => 31,  69 => 24,  66 => 16,  62 => 16,  49 => 13,  98 => 32,  93 => 32,  88 => 30,  78 => 20,  46 => 12,  44 => 12,  32 => 5,  27 => 4,  43 => 7,  40 => 6,  25 => 4,  24 => 2,  172 => 46,  158 => 57,  155 => 48,  129 => 47,  119 => 35,  117 => 43,  20 => 1,  22 => 103,  19 => 1,  209 => 87,  203 => 109,  199 => 79,  193 => 76,  189 => 76,  187 => 74,  182 => 68,  176 => 67,  173 => 69,  168 => 73,  164 => 72,  162 => 64,  154 => 59,  149 => 63,  147 => 119,  144 => 52,  141 => 35,  133 => 48,  130 => 46,  125 => 50,  122 => 41,  116 => 28,  112 => 41,  109 => 47,  106 => 26,  103 => 41,  99 => 30,  95 => 26,  92 => 35,  86 => 29,  82 => 27,  80 => 31,  73 => 19,  64 => 22,  60 => 17,  57 => 18,  54 => 14,  51 => 13,  48 => 14,  45 => 13,  42 => 11,  39 => 10,  36 => 9,  33 => 9,  30 => 4,);
    }
}
