<?php

/* PatCompteBundle:Facture:_new_facture_fr.html.twig */
class __TwigTemplate_b36fdc7599c7b76dd3f5345d0706aba4f2c95e3a2e6a132f91b728575e29cd86 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
  <head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        echo (((true == (isset($context["isAvoir"]) ? $context["isAvoir"] : $this->getContext($context, "isAvoir")))) ? ("Avoir") : ("Facture"));
        echo "</title>
    ";
        // line 7
        echo "    <style type=\"text/css\">
      /*RESET*/
      *{ margin:0; margin:0;}
      img { border:none;}
      a img { border:none;}
      a{ text-decoration:none;}
      a:hover{ text-decoration:underline;}


      .clearfix{ width:100%; height:0.000001em; clear:both; float:none; position:relative;}
      h1, h2{font-size:1.33em; margin:0 0 5px 0; padding:0; font-weight:600;}
      h3{font-size:1em; margin:0 0 5px 0; padding:0; font-weight:600;}
      p{font-size:1em; margin:0 0 5px 0; padding:0; font-weight:400; line-height:1.33em;}
      /**/
      body{ font-family: 'Raleway', sans-serif;font-size:1.2em; font-weight:normal; background-color:#CECECE;}
      .page{ width:940px; margin:auto; padding:0 20px; background-color:white;}
      .content{ width:100%; margin:90px 0 0px 0; padding-bottom: 50px;}
      .headInvoice{ width:100%; padding:20px 0 0;}
      .logo{ float:left;}
      .invoiceDescription{ float:right; margin:15px 40px 0 0; text-align:left; min-width:200px; font-size:0.875em/*14/16*/; }
      .infosInvoice{  margin:0px 10px 0px;}
      .renterAdress{ text-align:left; min-width:200px; font-size:0.875em/*14/16*/;}
      .renterAdress p{font-size:1em; line-height:1.33em;}
      .tenantAdress{ margin:40px 0px 0 0px; text-align:left; min-width:200px; font-size:0.875em/*14/16*/;}
      .tenantAdress p{font-size:1em; line-height:1.33em;}
      .description{margin:40px 10px 20px 10px ;font-size:0.875em/*12/16*/}
      .rentInfos{margin:0px 10px 20px 10px ;font-size:0.875em/*12/16*/}
      .gridInvoice{margin:0 0 20px 0;font-size:0.875em/*12/16*/}
      .comments{margin:30px 10px 0px 10px ;font-size:0.875em/*12/16*/}
      .comments p{font-size:0.875em; margin:0 10px;}
      .footerInvoice{width:100%; margin:0px 0px 10px; padding:10px 0 10px; border-top:1px solid #000; font-size:0.6875em/*11/16*/; text-align:center;}

      table {
        border:1px solid #dddddd;
        border-collapse: collapse;
        width:100%;
      }

      th {
        padding:8px;
        text-align:center;
        border:1px solid #dddddd;
      }

      td {
        padding:8px;
        border:1px solid #dddddd;
        text-align:left;
      }

      .totalTtc {
        text-align:right;
        font-weight: bolder;
        font-size:17px;
      }

      .dontTva {
        text-align:right;
      }

      #infostop {
        border:0px;
      }

      #infostop tr  {
        border:0px;
      }

      #infostop td  {
        border:0px;
      }
      .tenantAdress p{word-break:break-all;}
    </style>
  </head>

  <body>

    <div class=\"page\">
      <header class=\"headInvoice\">
        <div class=\"logo\">
          <img src=\"";
        // line 87
        echo twig_escape_filter($this->env, (isset($context["root"]) ? $context["root"] : $this->getContext($context, "root")), "html", null, true);
        echo "/../web/bundles/patcompte/images/class-appart_logo.png\" width=\"476\" height=\"77\" />
        </div>
        <div class=\"invoiceDescription\">
          <p><strong>";
        // line 90
        echo (((true == (isset($context["isAvoir"]) ? $context["isAvoir"] : $this->getContext($context, "isAvoir")))) ? ("Avoir sur facture") : ("Facture"));
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "number", array()), "html", null, true);
        echo "</strong></p>
          <p>Date : ";
        // line 91
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "created_at", array()), "d/m/Y"), "html", null, true);
        echo "</p>
        </div>
        <div class=\"clearfix\"></div>
      </header>
      <div class=\"content\">


        <div class=\"infosInvoice\">

          <table cellpadding=\"0\" cellspacing=\"0\" width=\"940\" id=\"infostop\">
            <tr><td style=\"width:300px;\">

                <div class=\"renterAdress\">
                  ";
        // line 104
        echo $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "caAddress", array());
        echo "
                </div>
              </td>
              <td>&nbsp;
              </td>
              <td style=\"width:370px;\">
                <div class=\"tenantAdress\" style=\"width:300px; word-wrap: break-word;\">
                  <h3>";
        // line 111
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "lastName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "firstName", array()), "html", null, true);
        echo "</h3>
                  <p>
                    ";
        // line 113
        if ($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "siret", array())) {
            // line 114
            echo "                      ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "siret", array()), "html", null, true);
            echo "<br/>
                    ";
        }
        // line 116
        echo "
                    Téléphone : ";
        // line 117
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "phone", array()), "html", null, true);
        echo " <br/>
                    Email : ";
        // line 118
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "email", array()), "html", null, true);
        echo "<br/>

                    Adresse :
                    ";
        // line 121
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "address", array()), "html", null, true);
        echo "
                    ";
        // line 122
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "address2", array()), "html", null, true);
        echo "<br/>
                    ";
        // line 123
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "zipCode", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "city", array()), "html", null, true);
        echo "<br/>
                    ";
        // line 124
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "country", array()), "html", null, true);
        echo "
                  </p>
                </div>
                <div class=\"clearfix\"></div>
              </td>
            </tr>
          </table>

        </div>


        <div class=\"description\">
          <p>
            ";
        // line 137
        echo $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "header", array());
        echo "
            <br/>
            Le service commercial<br/>
            <strong>CLASS APPART</strong>
          </p>
        </div>
        <div class=\"rentInfos\">
          Séjour de ";
        // line 144
        echo twig_escape_filter($this->env, (isset($context["nb_nuits"]) ? $context["nb_nuits"] : $this->getContext($context, "nb_nuits")), "html", null, true);
        echo " nuits, effectué du ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "reservation", array()), "dateDebut", array()), "d/m/Y"), "html", null, true);
        echo " au ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "reservation", array()), "dateFin", array()), "d/m/Y"), "html", null, true);
        echo ".<br/>
          Réf. réservation :  ";
        // line 145
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "reservation", array()), "reference", array()), "html", null, true);
        echo " <br />
          Réf : ";
        // line 146
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "reservation", array()), "appartement", array()), "reference", array()), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "reservation", array()), "appartement", array()), "surfaceSol", array()), "html", null, true);
        echo " m<sup>2</sup> situé à ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "reservation", array()), "appartement", array()), "adresse", array()), "html", null, true);
        echo " ";
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "reservation", array()), "appartement", array()), "adresse2", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "reservation", array()), "appartement", array()), "adresse2", array()), "html", null, true);
            echo " ";
        }
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "reservation", array()), "appartement", array()), "ville", array()), "nom", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "reservation", array()), "appartement", array()), "ville", array()), "codePostal", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "reservation", array()), "appartement", array()), "ville", array()), "pays", array()), "html", null, true);
        echo "
        </div>
        <div class=\"gridInvoice\">
          ";
        // line 159
        echo "        ";
        $context["tarif"] = $this->env->getExtension('my_twig_extension')->getTarif((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")));
        // line 160
        echo "
        <table>
          <tr>
            <th>
              Désignation
            </th>
            <th width=\"140\">
              Montant TTC
            </th>
          </tr>
          <tr>
            <td>
              Location
            </td>
            <td>
              ";
        // line 176
        echo "              ";
        echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((isset($context["tarif"]) ? $context["tarif"] : $this->getContext($context, "tarif"))), "html", null, true);
        echo " €
            </td>
          </tr>
          ";
        // line 179
        if ($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "promotion", array())) {
            // line 180
            echo "            <tr>
              <td>
                Promo/Majoration :
              </td>
              <td>
                ";
            // line 185
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "promotion", array())), "html", null, true);
            echo " €
              </td>
            </tr>
          ";
        }
        // line 189
        echo "          <tr>
            <td>
              <div  class='totalTtc'>
                <strong>Total TTC :</strong>
              </div>
            </td>
            <td>
              ";
        // line 197
        echo "              ";
        echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((isset($context["tarif"]) ? $context["tarif"] : $this->getContext($context, "tarif"))), "html", null, true);
        echo " €
            </td>
          </tr>
          <tr>
            <td>
              <div class='dontTva'>
                Dont TVA :
              </div>
            </td>
            <td>
              <div>
                ";
        // line 208
        $context["dont_tva"] = ((($this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "calculValues", array()), "tarif_net", array(), "array") * ($this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "calculValues", array()), "commission", array(), "array") / 100)) * $this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "calculValues", array()), "tva", array(), "array")) / 100);
        // line 209
        echo "                ";
        echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((isset($context["dont_tva"]) ? $context["dont_tva"] : $this->getContext($context, "dont_tva"))), "html", null, true);
        echo " €
              </div>
            </td>
          </tr>
          <tr>
            <td>
              Caution
            </td>
            <td>
              ";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "depot", array())), "html", null, true);
        echo " €
            </td>
          </tr>

        </table>

      </div>
      <div class=\"comments\">
        <h3>Commentaires :</h3>
        ";
        // line 227
        echo $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "comment", array());
        echo "
      </div>
      <div class=\"clearfix\"></div>
    </div>
    <footer class=\"footerInvoice\">
      ";
        // line 232
        echo $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "footer", array());
        echo "
    </footer>
    <div class=\"clearfix\"></div>
  </div>

</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:Facture:_new_facture_fr.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1843 => 1037,  1834 => 1030,  1747 => 947,  1742 => 944,  1728 => 936,  1718 => 929,  1706 => 920,  1694 => 915,  1692 => 914,  1683 => 908,  1677 => 905,  1668 => 899,  1660 => 894,  1656 => 893,  1652 => 892,  1645 => 888,  1641 => 887,  1630 => 882,  1626 => 881,  1622 => 880,  1614 => 874,  1601 => 832,  1595 => 829,  1586 => 823,  1582 => 822,  1576 => 819,  1567 => 813,  1563 => 812,  1557 => 809,  1547 => 802,  1543 => 801,  1537 => 798,  1528 => 792,  1518 => 788,  1500 => 773,  1496 => 772,  1492 => 771,  1488 => 770,  1482 => 767,  1473 => 761,  1469 => 760,  1465 => 759,  1461 => 758,  1448 => 750,  1438 => 748,  1436 => 747,  1432 => 746,  1428 => 745,  1422 => 742,  1413 => 736,  1409 => 735,  1403 => 732,  1389 => 724,  1370 => 711,  1364 => 708,  1351 => 701,  1345 => 698,  1332 => 691,  1326 => 688,  1313 => 681,  1307 => 678,  1294 => 671,  1288 => 668,  1275 => 661,  1269 => 658,  1256 => 651,  1250 => 648,  1237 => 641,  1231 => 638,  1218 => 631,  1212 => 628,  1199 => 621,  1193 => 618,  1180 => 611,  1155 => 598,  1142 => 591,  1136 => 588,  1127 => 582,  1117 => 578,  1108 => 572,  1098 => 568,  1079 => 558,  1071 => 552,  1067 => 550,  1065 => 549,  1054 => 541,  1050 => 540,  1044 => 537,  1035 => 531,  1031 => 530,  1025 => 527,  1016 => 521,  1006 => 517,  997 => 511,  993 => 510,  987 => 507,  978 => 501,  974 => 500,  959 => 491,  955 => 490,  949 => 487,  940 => 481,  936 => 480,  930 => 477,  921 => 471,  911 => 467,  902 => 461,  898 => 460,  892 => 457,  883 => 451,  879 => 450,  864 => 441,  854 => 437,  845 => 431,  835 => 427,  826 => 421,  816 => 417,  805 => 411,  801 => 410,  797 => 409,  791 => 406,  778 => 399,  772 => 396,  763 => 390,  759 => 389,  753 => 386,  744 => 380,  740 => 379,  721 => 369,  715 => 366,  687 => 350,  677 => 346,  668 => 340,  643 => 324,  624 => 314,  614 => 310,  605 => 304,  595 => 300,  538 => 267,  519 => 257,  485 => 238,  481 => 237,  447 => 218,  262 => 114,  281 => 118,  197 => 83,  419 => 185,  308 => 130,  260 => 120,  228 => 50,  355 => 153,  346 => 150,  288 => 121,  692 => 459,  603 => 377,  561 => 278,  515 => 319,  475 => 234,  379 => 162,  348 => 227,  298 => 132,  1174 => 608,  1167 => 718,  1161 => 601,  1154 => 710,  1149 => 707,  1141 => 704,  1134 => 702,  1131 => 701,  1129 => 700,  1114 => 695,  1111 => 694,  1109 => 693,  1105 => 692,  1096 => 689,  1092 => 688,  1089 => 562,  1072 => 675,  1064 => 670,  1042 => 651,  1034 => 645,  1026 => 642,  1022 => 640,  1019 => 639,  1017 => 638,  1012 => 520,  1008 => 635,  1004 => 633,  1002 => 632,  996 => 629,  988 => 626,  984 => 624,  979 => 623,  977 => 622,  968 => 497,  962 => 613,  951 => 605,  917 => 470,  915 => 585,  912 => 584,  905 => 580,  897 => 575,  893 => 574,  881 => 568,  873 => 447,  871 => 561,  863 => 555,  853 => 496,  842 => 488,  831 => 480,  812 => 464,  800 => 456,  798 => 455,  787 => 447,  754 => 423,  747 => 418,  739 => 414,  731 => 410,  725 => 370,  718 => 404,  696 => 356,  681 => 378,  675 => 375,  665 => 369,  663 => 368,  651 => 359,  628 => 344,  606 => 334,  602 => 332,  600 => 331,  579 => 315,  572 => 311,  555 => 303,  542 => 268,  532 => 264,  513 => 254,  501 => 315,  499 => 271,  473 => 267,  441 => 245,  438 => 198,  416 => 231,  330 => 143,  289 => 133,  633 => 320,  627 => 291,  621 => 289,  619 => 288,  594 => 272,  576 => 287,  570 => 284,  562 => 255,  558 => 254,  552 => 251,  540 => 245,  508 => 228,  498 => 314,  486 => 308,  480 => 215,  454 => 201,  450 => 200,  410 => 265,  325 => 161,  220 => 150,  407 => 237,  402 => 234,  377 => 245,  313 => 172,  232 => 103,  465 => 197,  457 => 192,  417 => 270,  411 => 176,  408 => 225,  396 => 256,  391 => 168,  388 => 229,  372 => 251,  344 => 163,  332 => 220,  293 => 134,  274 => 175,  231 => 99,  200 => 45,  792 => 418,  788 => 416,  782 => 400,  776 => 439,  774 => 411,  762 => 402,  748 => 394,  742 => 391,  734 => 376,  730 => 385,  724 => 382,  716 => 403,  706 => 360,  698 => 389,  694 => 367,  688 => 364,  680 => 359,  676 => 358,  662 => 350,  658 => 336,  652 => 346,  644 => 341,  640 => 340,  634 => 337,  622 => 331,  616 => 386,  608 => 323,  598 => 273,  580 => 288,  564 => 297,  545 => 287,  541 => 286,  526 => 237,  507 => 317,  488 => 257,  462 => 227,  433 => 226,  424 => 207,  395 => 169,  382 => 168,  376 => 162,  341 => 223,  327 => 230,  320 => 209,  310 => 206,  291 => 143,  278 => 179,  259 => 109,  244 => 127,  448 => 250,  443 => 217,  429 => 259,  406 => 174,  366 => 206,  318 => 208,  282 => 127,  258 => 178,  222 => 103,  120 => 45,  272 => 100,  266 => 121,  226 => 118,  178 => 81,  111 => 87,  393 => 255,  386 => 187,  380 => 184,  362 => 157,  358 => 207,  342 => 139,  340 => 162,  334 => 159,  326 => 139,  319 => 138,  314 => 204,  299 => 193,  265 => 110,  252 => 107,  237 => 125,  194 => 83,  132 => 58,  23 => 2,  97 => 22,  81 => 31,  53 => 17,  654 => 223,  637 => 295,  632 => 206,  625 => 343,  623 => 342,  617 => 195,  612 => 337,  604 => 322,  593 => 327,  591 => 186,  586 => 365,  583 => 183,  578 => 180,  571 => 178,  557 => 277,  534 => 242,  522 => 236,  520 => 320,  504 => 248,  494 => 244,  463 => 145,  446 => 186,  440 => 136,  434 => 134,  431 => 240,  427 => 182,  405 => 197,  401 => 221,  397 => 114,  389 => 215,  381 => 210,  357 => 148,  349 => 228,  339 => 239,  303 => 197,  295 => 119,  287 => 185,  268 => 123,  74 => 33,  470 => 398,  452 => 236,  444 => 197,  435 => 405,  430 => 397,  414 => 183,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 940,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 916,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 886,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 833,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 791,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 755,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 725,  1387 => 694,  1378 => 688,  1374 => 712,  1368 => 684,  1359 => 678,  1355 => 702,  1349 => 674,  1340 => 668,  1336 => 692,  1330 => 664,  1321 => 658,  1317 => 682,  1311 => 654,  1302 => 648,  1298 => 672,  1292 => 644,  1283 => 638,  1279 => 662,  1273 => 634,  1264 => 628,  1260 => 652,  1254 => 624,  1245 => 618,  1241 => 642,  1235 => 614,  1226 => 608,  1222 => 632,  1216 => 604,  1207 => 598,  1203 => 622,  1197 => 594,  1188 => 588,  1184 => 612,  1178 => 584,  1169 => 578,  1165 => 602,  1159 => 574,  1150 => 568,  1146 => 592,  1140 => 564,  1123 => 581,  1119 => 697,  1113 => 546,  1104 => 571,  1100 => 690,  1094 => 536,  1085 => 561,  1081 => 529,  1075 => 526,  1066 => 671,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 599,  933 => 450,  929 => 588,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 569,  874 => 420,  870 => 419,  866 => 418,  860 => 440,  851 => 409,  847 => 408,  841 => 430,  832 => 399,  828 => 398,  822 => 420,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 431,  756 => 359,  752 => 395,  746 => 355,  737 => 413,  733 => 348,  727 => 408,  712 => 376,  708 => 332,  702 => 359,  693 => 323,  689 => 322,  683 => 349,  674 => 241,  670 => 355,  664 => 339,  649 => 297,  645 => 296,  639 => 323,  630 => 287,  626 => 332,  620 => 313,  611 => 277,  607 => 279,  601 => 303,  592 => 267,  588 => 269,  582 => 263,  563 => 348,  554 => 344,  550 => 246,  544 => 246,  535 => 283,  531 => 236,  516 => 233,  512 => 318,  506 => 163,  493 => 312,  478 => 253,  468 => 209,  459 => 144,  455 => 209,  449 => 193,  436 => 192,  428 => 208,  422 => 234,  409 => 198,  403 => 261,  390 => 188,  384 => 165,  351 => 152,  337 => 164,  311 => 118,  296 => 139,  256 => 118,  241 => 53,  215 => 101,  207 => 137,  192 => 84,  186 => 74,  175 => 119,  153 => 67,  118 => 50,  61 => 16,  34 => 8,  65 => 23,  77 => 13,  37 => 10,  190 => 91,  161 => 72,  137 => 102,  126 => 57,  261 => 133,  255 => 130,  247 => 155,  242 => 112,  214 => 93,  211 => 90,  191 => 124,  157 => 80,  145 => 51,  127 => 47,  373 => 111,  367 => 248,  363 => 173,  359 => 172,  354 => 152,  343 => 167,  335 => 145,  328 => 152,  322 => 160,  315 => 149,  309 => 137,  305 => 115,  302 => 142,  290 => 185,  284 => 121,  279 => 139,  271 => 176,  264 => 185,  248 => 129,  236 => 106,  223 => 99,  170 => 68,  110 => 57,  96 => 46,  84 => 35,  472 => 210,  467 => 148,  375 => 252,  371 => 160,  360 => 183,  356 => 245,  353 => 232,  350 => 151,  338 => 145,  336 => 221,  331 => 141,  321 => 152,  316 => 205,  307 => 142,  304 => 134,  301 => 130,  297 => 131,  292 => 133,  286 => 129,  283 => 181,  277 => 116,  275 => 62,  270 => 123,  263 => 134,  257 => 146,  253 => 177,  249 => 90,  245 => 110,  233 => 92,  225 => 145,  216 => 111,  206 => 95,  202 => 46,  198 => 72,  185 => 123,  180 => 63,  177 => 121,  165 => 115,  150 => 67,  124 => 56,  113 => 48,  100 => 47,  58 => 19,  251 => 159,  234 => 84,  213 => 140,  195 => 84,  174 => 73,  167 => 117,  146 => 58,  140 => 62,  128 => 56,  104 => 49,  90 => 35,  83 => 33,  52 => 15,  596 => 225,  590 => 314,  585 => 221,  577 => 357,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 210,  551 => 274,  546 => 207,  543 => 206,  539 => 205,  529 => 322,  525 => 173,  523 => 258,  518 => 193,  514 => 192,  509 => 189,  503 => 266,  500 => 247,  497 => 263,  495 => 313,  492 => 157,  490 => 219,  487 => 213,  484 => 256,  482 => 177,  479 => 176,  477 => 401,  474 => 206,  471 => 173,  469 => 172,  466 => 228,  464 => 292,  461 => 169,  458 => 239,  456 => 224,  451 => 189,  445 => 160,  442 => 199,  439 => 229,  437 => 214,  432 => 191,  426 => 188,  423 => 186,  420 => 219,  418 => 204,  413 => 172,  399 => 194,  394 => 162,  378 => 162,  370 => 159,  368 => 203,  365 => 212,  361 => 199,  347 => 148,  345 => 227,  333 => 218,  329 => 214,  323 => 139,  317 => 223,  312 => 133,  306 => 215,  300 => 110,  294 => 189,  285 => 193,  280 => 180,  276 => 176,  267 => 172,  250 => 156,  239 => 100,  229 => 146,  218 => 94,  212 => 88,  210 => 76,  205 => 87,  188 => 83,  184 => 88,  181 => 122,  169 => 116,  160 => 63,  152 => 60,  148 => 65,  139 => 104,  134 => 63,  114 => 58,  107 => 17,  76 => 29,  70 => 28,  273 => 138,  269 => 136,  254 => 160,  246 => 55,  243 => 106,  240 => 126,  238 => 100,  235 => 124,  230 => 100,  227 => 106,  224 => 94,  221 => 141,  219 => 112,  217 => 144,  208 => 91,  204 => 134,  179 => 120,  159 => 68,  143 => 58,  135 => 59,  131 => 65,  108 => 46,  102 => 43,  71 => 25,  67 => 17,  63 => 21,  59 => 20,  47 => 16,  94 => 37,  89 => 36,  85 => 33,  79 => 30,  75 => 31,  68 => 27,  56 => 19,  50 => 16,  38 => 12,  29 => 7,  87 => 34,  72 => 9,  55 => 18,  21 => 2,  26 => 2,  35 => 9,  31 => 3,  41 => 9,  28 => 2,  201 => 85,  196 => 103,  183 => 121,  171 => 118,  166 => 86,  163 => 73,  156 => 113,  151 => 69,  142 => 65,  138 => 48,  136 => 56,  123 => 91,  121 => 62,  115 => 44,  105 => 43,  101 => 16,  91 => 36,  69 => 25,  66 => 8,  62 => 22,  49 => 20,  98 => 38,  93 => 37,  88 => 46,  78 => 33,  46 => 8,  44 => 13,  32 => 4,  27 => 6,  43 => 12,  40 => 13,  25 => 5,  24 => 2,  172 => 61,  158 => 114,  155 => 69,  129 => 55,  119 => 51,  117 => 90,  20 => 1,  22 => 2,  19 => 1,  209 => 100,  203 => 95,  199 => 94,  193 => 84,  189 => 122,  187 => 81,  182 => 80,  176 => 82,  173 => 77,  168 => 75,  164 => 116,  162 => 114,  154 => 111,  149 => 111,  147 => 109,  144 => 63,  141 => 62,  133 => 53,  130 => 56,  125 => 54,  122 => 59,  116 => 53,  112 => 47,  109 => 47,  106 => 60,  103 => 37,  99 => 40,  95 => 15,  92 => 45,  86 => 42,  82 => 43,  80 => 33,  73 => 26,  64 => 22,  60 => 7,  57 => 25,  54 => 20,  51 => 17,  48 => 10,  45 => 19,  42 => 12,  39 => 10,  36 => 9,  33 => 8,  30 => 7,);
    }
}
