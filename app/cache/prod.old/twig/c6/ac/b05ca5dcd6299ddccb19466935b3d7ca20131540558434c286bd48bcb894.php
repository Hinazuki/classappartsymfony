<?php

/* PatCompteBundle:AdminPromotion:admin_promotion_index.html.twig */
class __TwigTemplate_c6acb05ca5dcd6299ddccb19466935b3d7ca20131540558434c286bd48bcb894 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("PatCompteBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PatCompteBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
  <div id=\"contenuCentrale1\">
    <h1>Gestion des loyers <small>(Réf : ";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "reference", array()), "html", null, true);
        echo ")</small></h1>
    <div class=\"clear\"></div>
    <div class=\"etapesBien\">
      <div class=\"linksEtapesBien\">
        <ul>
          <li><a href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_appartement_editer", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Description</a></li>
          <li><a href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_appartement_modif_proprietaire", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Propriétaire</a></li>
          <li><a href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_tarif_ajouter", array("id_bien" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Loyers</a></li>
          <li><a class=\"active\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_promotion_index", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Promotion / Majoration</a></li>
          <li><a href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_piece_index", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Pièces</a></li>
          <li><a href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_media_ajouter", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Images</a></li>
          <li><a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_calendrier_index", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Disponibilités</a></li>
        </ul>
      </div>
    </div>

    ";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 23
            echo "      <br/>
      <div class=\"alert alert-error\">
        ";
            // line 25
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
      </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "
    ";
        // line 29
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 30
            echo "      <br/>
      <div class=\"alert alert-success\">
        ";
            // line 32
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
      </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "
    ";
        // line 36
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 37
            echo "      <br/>
      <div class=\"alert alert-danger\">
        ";
            // line 39
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
      </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "


    <div class=\"blocForm\" style=\"margin-top: 20px;\">
      <div class=\"title\">
        Ajouter une promotion / majoration
      </div>

      ";
        // line 50
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "


      <div class=\"content\">

        <div class=\"error\">
          ";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
        </div>

        <div class=\"row-fluid form-group\">
          <div class=\"span4 right\">
            ";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "type", array()), 'label');
        echo "
          </div>
          <div class=\"span8\">
            ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "type", array()), 'widget');
        echo "
            ";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "type", array()), 'errors');
        echo "
          </div>
        </div>

        <div class=\"row-fluid form-group\">
          <div class=\"span4 right\">
            ";
        // line 71
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "percentage", array()), 'label');
        echo "
            <i class=\"fas fa-info-circle\" data-toggle=\"popover\" data-placement=\"top\" data-html=\"true\" data-content=\"Pour une promotion insérer un nombre négatif\"></i>
          </div>
          <div class=\"span8 input-append\">
            ";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "percentage", array()), 'widget');
        echo "<span class=\"add-on\">%</span>
            <div>
              ";
        // line 77
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "percentage", array()), 'errors');
        echo "
            </div>
          </div>
        </div>

        <div class=\"row-fluid form-group\">
          <div class=\"span4 right\">
            ";
        // line 84
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "amount", array()), 'label');
        echo "
          </div>
          <div class=\"span8 input-append\">
            ";
        // line 87
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "amount", array()), 'widget');
        echo "<span class=\"add-on\">€</span>
            <div>
              ";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "amount", array()), 'errors');
        echo "
            </div>
          </div>
        </div>

        <div class=\"row-fluid form-group\">
          <div class=\"span4 right\">
            ";
        // line 96
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nbDaysBefore", array()), 'label');
        echo "
          </div>
          <div class=\"span8 input-prepend\">
            <span class=\"add-on\">J-</span>";
        // line 99
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nbDaysBefore", array()), 'widget');
        echo "
            ";
        // line 100
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nbDaysBefore", array()), 'errors');
        echo "
          </div>
        </div>

        <div class=\"row-fluid form-group\">
          <div class=\"span4 right\">
            ";
        // line 106
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "activated", array()), 'label');
        echo "
          </div>
          <div class=\"span8\">
            <input type=\"checkbox\" id=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "activated", array()), "vars", array()), "id", array()), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "activated", array()), "vars", array()), "full_name", array()), "html", null, true);
        echo "\" class=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "activated", array()), "vars", array()), "attr", array()), "class", array()), "html", null, true);
        echo "\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "activated", array()), "vars", array()), "value", array()), "html", null, true);
        echo "\">
            ";
        // line 110
        $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "activated", array()), "setRendered", array());
        // line 111
        echo "            ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "activated", array()), 'errors');
        echo "
          </div>
        </div>

        <div class=\"row-fluid form-group\">
          <div class=\"span4 right\">
            ";
        // line 117
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "begin", array()), 'label');
        echo "
          </div>
          <div class=\"span8\">
            ";
        // line 120
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "begin", array()), 'widget');
        echo "
            ";
        // line 121
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "begin", array()), 'errors');
        echo "
          </div>
        </div>

        <div class=\"row-fluid form-group\">
          <div class=\"span4 right\">
            ";
        // line 127
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "end", array()), 'label');
        echo "
          </div>
          <div class=\"span8\">
            ";
        // line 130
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "end", array()), 'widget');
        echo "
            ";
        // line 131
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "end", array()), 'errors');
        echo "
          </div>
        </div>

        <div class=\"text-right\">
          <button type=\"submit\" class=\"btn btn-primary\">Enregistrer</button>
        </div>
      </div>

      ";
        // line 140
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    </div>

    ";
        // line 143
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "promotions", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["promotion"]) {
            // line 144
            echo "      <div class=\"ListeAppart row-fluid\">
        <div class=\"span3 ListeAppart1\" style=\"min-height: 80px;\">
          ";
            // line 146
            if (($this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "type", array()) === constant(get_class((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")))."::"."TYPE_ACTIVABLE"))) {
                // line 147
                echo "            ";
                echo ((($this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "percentage", array()) > 0)) ? ("Majoration") : ("Promotion"));
                echo " par date de réservation
          ";
            } elseif (($this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "type", array()) === constant(get_class((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")))."::"."TYPE_PERIODE"))) {
                // line 149
                echo "            ";
                echo ((($this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "percentage", array()) > 0)) ? ("Majoration") : ("Promotion"));
                echo " par période
          ";
            }
            // line 151
            echo "        </div>

        <div class=\"span3 ListeAppart1 ";
            // line 153
            echo ((($this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "percentage", array()) > 0)) ? ("text-danger") : ("text-success"));
            echo "\" style=\"min-height: 80px; margin-left: 0\">
          ";
            // line 154
            echo ((($this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "percentage", array()) > 0)) ? ("+") : (""));
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "percentage", array())), "html", null, true);
            echo "&nbsp;%<br>
          ";
            // line 155
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute($this->getAttribute((isset($context["tarifs"]) ? $context["tarifs"] : $this->getContext($context, "tarifs")), $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index0", array()), array(), "array"), "tarif_net", array())), "html", null, true);
            echo "&nbsp;€ Net
        </div>

        <div class=\"span4 ListeAppart1\" style=\"min-height: 80px; margin-left: 0;\">
          ";
            // line 159
            if (($this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "type", array()) === constant(get_class((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")))."::"."TYPE_ACTIVABLE"))) {
                // line 160
                echo "            Réservation faite à J-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "nbDaysBefore", array()), "html", null, true);
                echo "<br>
            Etat :
            ";
                // line 162
                if ($this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "activated", array())) {
                    // line 163
                    echo "              <span class=\"text-success\">Activé</span>
            ";
                } else {
                    // line 165
                    echo "              <span class=\"text-danger\">Désactivé</span>
            ";
                }
                // line 167
                echo "          ";
            } elseif (($this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "type", array()) === constant(get_class((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")))."::"."TYPE_PERIODE"))) {
                // line 168
                echo "            Du : ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "begin", array()), "d/m/Y"), "html", null, true);
                echo "<br>
            Au : ";
                // line 169
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "end", array()), "d/m/Y"), "html", null, true);
                echo "
          ";
            }
            // line 171
            echo "        </div>

        <div class=\"span2 ListeAppart2 center\">
          ";
            // line 174
            if (($this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "type", array()) === constant(get_class((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")))."::"."TYPE_ACTIVABLE"))) {
                // line 175
                echo "            ";
                if ($this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "activated", array())) {
                    // line 176
                    echo "              <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_promotion_desactiver", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()), "promotionId" => $this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "id", array()))), "html", null, true);
                    echo "\" onclick=\"return confirmAction()\" class=\"text-danger\"><i class=\"fas fa-times\"></i> Désactiver</a><br>
            ";
                } else {
                    // line 178
                    echo "              <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_promotion_activer", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()), "promotionId" => $this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "id", array()))), "html", null, true);
                    echo "\" onclick=\"return confirmAction()\" class=\"text-success\"><i class=\"fas fa-check\"></i> Activer</a><br>
            ";
                }
                // line 180
                echo "
          ";
            }
            // line 182
            echo "          <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_promotion_supprimer", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()), "promotionId" => $this->getAttribute((isset($context["promotion"]) ? $context["promotion"] : $this->getContext($context, "promotion")), "id", array()))), "html", null, true);
            echo "\" onclick=\"return confirmAction()\"><i class=\"far fa-trash-alt fa-lg text-danger\" style=\"line-height: 40px;\"></i></a>
        </div>
      </div>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['promotion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 186
        echo "
    <div class=\"row-fluid\">
      <div class=\"span6\">
        <a href=\"";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_tarif_ajouter", array("id_bien" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-large\">Revenir</a>
      </div>
      <div class=\"span6 right\">
        <a href=\"";
        // line 192
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_piece_index", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-primary btn-large\">Continuer</a>
      </div>
    </div>
  </div><!-- /contenuCentrale -->

  <script type=\"text/javascript\" src=\"";
        // line 197
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/js/datepicker_admin_scripts.js"), "html", null, true);
        echo "\"></script>
  <script type=\"text/javascript\">
    \$(function () {
      var togglePromotionType = function () {
        var \$selector = \$('.js-promotion-type-select');
        console.log(\$selector.find('input:checked').val());
        if (\$selector.find('input:checked').val() == 1) {
          \$('.js-promotion-type-activable').parents('.form-group').removeClass('hidden');
          \$('.js-promotion-type-periode').parents('.form-group').addClass('hidden');
        } else if (\$selector.find('input:checked').val() == 2) {
          \$('.js-promotion-type-activable').parents('.form-group').addClass('hidden');
          \$('.js-promotion-type-periode').parents('.form-group').removeClass('hidden');
        } else {
          \$('.js-promotion-type-activable').parents('.form-group').removeClass('hidden');
          \$('.js-promotion-type-periode').parents('.form-group').removeClass('hidden');
        }
      }

      togglePromotionType()

      \$('.js-promotion-type-select').on('change', function () {
        togglePromotionType();
      })

      \$('[data-toggle=\"popover\"]').popover({
        trigger: 'hover'
      })
    });
  </script>

";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:AdminPromotion:admin_promotion_index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  465 => 197,  457 => 192,  417 => 178,  411 => 176,  408 => 175,  396 => 169,  391 => 168,  388 => 167,  372 => 160,  344 => 149,  332 => 144,  293 => 130,  274 => 120,  231 => 100,  200 => 84,  792 => 418,  788 => 416,  782 => 414,  776 => 412,  774 => 411,  762 => 402,  748 => 394,  742 => 391,  734 => 386,  730 => 385,  724 => 382,  716 => 377,  706 => 373,  698 => 368,  694 => 367,  688 => 364,  680 => 359,  676 => 358,  662 => 350,  658 => 349,  652 => 346,  644 => 341,  640 => 340,  634 => 337,  622 => 331,  616 => 328,  608 => 323,  598 => 319,  580 => 310,  564 => 297,  545 => 287,  541 => 286,  526 => 277,  507 => 267,  488 => 257,  462 => 240,  433 => 226,  424 => 220,  395 => 206,  382 => 199,  376 => 196,  341 => 173,  327 => 167,  320 => 153,  310 => 149,  291 => 139,  278 => 121,  259 => 122,  244 => 113,  448 => 267,  443 => 230,  429 => 259,  406 => 174,  366 => 206,  318 => 175,  282 => 133,  258 => 111,  222 => 90,  120 => 29,  272 => 129,  266 => 101,  226 => 87,  178 => 71,  111 => 27,  393 => 305,  386 => 200,  380 => 163,  362 => 294,  358 => 154,  342 => 188,  340 => 287,  334 => 284,  326 => 278,  319 => 273,  314 => 271,  299 => 266,  265 => 144,  252 => 137,  237 => 128,  194 => 103,  132 => 42,  23 => 3,  97 => 31,  81 => 25,  53 => 16,  654 => 223,  637 => 208,  632 => 206,  625 => 200,  623 => 199,  617 => 195,  612 => 192,  604 => 322,  593 => 187,  591 => 186,  586 => 313,  583 => 183,  578 => 180,  571 => 178,  557 => 177,  534 => 175,  522 => 276,  520 => 171,  504 => 162,  494 => 158,  463 => 145,  446 => 186,  440 => 136,  434 => 134,  431 => 260,  427 => 182,  405 => 210,  401 => 171,  397 => 114,  389 => 113,  381 => 112,  357 => 109,  349 => 108,  339 => 105,  303 => 99,  295 => 160,  287 => 127,  268 => 117,  74 => 23,  470 => 398,  452 => 236,  444 => 387,  435 => 405,  430 => 397,  414 => 216,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 887,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 864,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 825,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 808,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 763,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 732,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 697,  1387 => 694,  1378 => 688,  1374 => 687,  1368 => 684,  1359 => 678,  1355 => 677,  1349 => 674,  1340 => 668,  1336 => 667,  1330 => 664,  1321 => 658,  1317 => 657,  1311 => 654,  1302 => 648,  1298 => 647,  1292 => 644,  1283 => 638,  1279 => 637,  1273 => 634,  1264 => 628,  1260 => 627,  1254 => 624,  1245 => 618,  1241 => 617,  1235 => 614,  1226 => 608,  1222 => 607,  1216 => 604,  1207 => 598,  1203 => 597,  1197 => 594,  1188 => 588,  1184 => 587,  1178 => 584,  1169 => 578,  1165 => 577,  1159 => 574,  1150 => 568,  1146 => 567,  1140 => 564,  1123 => 550,  1119 => 549,  1113 => 546,  1104 => 540,  1100 => 539,  1094 => 536,  1085 => 530,  1081 => 529,  1075 => 526,  1066 => 520,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 456,  933 => 450,  929 => 449,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 426,  874 => 420,  870 => 419,  866 => 418,  860 => 415,  851 => 409,  847 => 408,  841 => 405,  832 => 399,  828 => 398,  822 => 395,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 365,  756 => 359,  752 => 395,  746 => 355,  737 => 349,  733 => 348,  727 => 345,  712 => 376,  708 => 332,  702 => 329,  693 => 323,  689 => 322,  683 => 319,  674 => 241,  670 => 355,  664 => 309,  649 => 297,  645 => 296,  639 => 293,  630 => 287,  626 => 332,  620 => 283,  611 => 277,  607 => 276,  601 => 189,  592 => 267,  588 => 185,  582 => 263,  563 => 253,  554 => 293,  550 => 246,  544 => 243,  535 => 283,  531 => 236,  516 => 273,  512 => 165,  506 => 163,  493 => 216,  478 => 253,  468 => 203,  459 => 144,  455 => 271,  449 => 193,  436 => 183,  428 => 181,  422 => 395,  409 => 117,  403 => 168,  390 => 224,  384 => 165,  351 => 137,  337 => 172,  311 => 118,  296 => 109,  256 => 110,  241 => 79,  215 => 99,  207 => 66,  192 => 88,  186 => 71,  175 => 93,  153 => 58,  118 => 33,  61 => 24,  34 => 4,  65 => 20,  77 => 24,  37 => 7,  190 => 77,  161 => 60,  137 => 52,  126 => 52,  261 => 86,  255 => 135,  247 => 130,  242 => 127,  214 => 87,  211 => 89,  191 => 76,  157 => 37,  145 => 49,  127 => 42,  373 => 111,  367 => 173,  363 => 155,  359 => 171,  354 => 153,  343 => 135,  335 => 104,  328 => 152,  322 => 176,  315 => 143,  309 => 140,  305 => 115,  302 => 267,  290 => 157,  284 => 154,  279 => 129,  271 => 147,  264 => 122,  248 => 118,  236 => 77,  223 => 82,  170 => 74,  110 => 37,  96 => 44,  84 => 26,  472 => 148,  467 => 148,  375 => 152,  371 => 151,  360 => 183,  356 => 182,  353 => 99,  350 => 151,  338 => 147,  336 => 146,  331 => 169,  321 => 89,  316 => 152,  307 => 269,  304 => 268,  301 => 143,  297 => 131,  292 => 110,  286 => 154,  283 => 105,  277 => 104,  275 => 103,  270 => 144,  263 => 123,  257 => 121,  253 => 119,  249 => 21,  245 => 92,  233 => 88,  225 => 103,  216 => 111,  206 => 87,  202 => 92,  198 => 20,  185 => 75,  180 => 79,  177 => 79,  165 => 64,  150 => 59,  124 => 46,  113 => 38,  100 => 24,  58 => 18,  251 => 93,  234 => 109,  213 => 113,  195 => 78,  174 => 72,  167 => 68,  146 => 56,  140 => 50,  128 => 48,  104 => 42,  90 => 23,  83 => 25,  52 => 11,  596 => 225,  590 => 314,  585 => 221,  577 => 218,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 210,  551 => 176,  546 => 207,  543 => 206,  539 => 205,  529 => 174,  525 => 173,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 266,  500 => 160,  497 => 263,  495 => 182,  492 => 157,  490 => 180,  487 => 213,  484 => 256,  482 => 177,  479 => 176,  477 => 401,  474 => 206,  471 => 173,  469 => 172,  466 => 146,  464 => 397,  461 => 169,  458 => 239,  456 => 167,  451 => 189,  445 => 160,  442 => 159,  439 => 229,  437 => 262,  432 => 398,  426 => 396,  423 => 180,  420 => 219,  418 => 251,  413 => 172,  399 => 237,  394 => 162,  378 => 162,  370 => 159,  368 => 295,  365 => 110,  361 => 131,  347 => 136,  345 => 94,  333 => 131,  329 => 130,  323 => 102,  317 => 272,  312 => 114,  306 => 166,  300 => 110,  294 => 68,  285 => 105,  280 => 99,  276 => 98,  267 => 60,  250 => 100,  239 => 91,  229 => 116,  218 => 82,  212 => 100,  210 => 97,  205 => 82,  188 => 59,  184 => 99,  181 => 65,  169 => 65,  160 => 66,  152 => 63,  148 => 38,  139 => 45,  134 => 49,  114 => 32,  107 => 34,  76 => 30,  70 => 17,  273 => 127,  269 => 94,  254 => 92,  246 => 109,  243 => 88,  240 => 106,  238 => 113,  235 => 94,  230 => 111,  227 => 99,  224 => 109,  221 => 96,  219 => 112,  217 => 84,  208 => 108,  204 => 97,  179 => 71,  159 => 61,  143 => 60,  135 => 41,  131 => 44,  108 => 30,  102 => 33,  71 => 20,  67 => 17,  63 => 16,  59 => 15,  47 => 12,  94 => 40,  89 => 36,  85 => 19,  79 => 23,  75 => 22,  68 => 18,  56 => 22,  50 => 13,  38 => 14,  29 => 4,  87 => 24,  72 => 25,  55 => 14,  21 => 2,  26 => 2,  35 => 6,  31 => 4,  41 => 11,  28 => 3,  201 => 80,  196 => 89,  183 => 72,  171 => 92,  166 => 63,  163 => 52,  156 => 56,  151 => 56,  142 => 50,  138 => 55,  136 => 46,  123 => 39,  121 => 44,  115 => 36,  105 => 40,  101 => 39,  91 => 25,  69 => 24,  66 => 16,  62 => 19,  49 => 13,  98 => 38,  93 => 29,  88 => 48,  78 => 20,  46 => 12,  44 => 7,  32 => 5,  27 => 4,  43 => 11,  40 => 6,  25 => 4,  24 => 2,  172 => 106,  158 => 49,  155 => 48,  129 => 119,  119 => 37,  117 => 39,  20 => 1,  22 => 103,  19 => 1,  209 => 82,  203 => 109,  199 => 79,  193 => 76,  189 => 76,  187 => 74,  182 => 70,  176 => 67,  173 => 67,  168 => 73,  164 => 72,  162 => 59,  154 => 59,  149 => 63,  147 => 119,  144 => 56,  141 => 35,  133 => 45,  130 => 53,  125 => 37,  122 => 41,  116 => 28,  112 => 35,  109 => 47,  106 => 26,  103 => 32,  99 => 30,  95 => 29,  92 => 28,  86 => 29,  82 => 22,  80 => 31,  73 => 19,  64 => 22,  60 => 19,  57 => 18,  54 => 15,  51 => 13,  48 => 14,  45 => 13,  42 => 11,  39 => 6,  36 => 9,  33 => 9,  30 => 3,);
    }
}
