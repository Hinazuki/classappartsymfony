<?php

/* KnpPaginatorBundle:Pagination:twitter_bootstrap_v3_pagination.html.twig */
class __TwigTemplate_c7be6c4f307ea2c98d0b8002d9ed968d81530d14f5631ee3a63e57816596e99f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 16
        echo "
";
        // line 17
        if (((isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount")) > 1)) {
            // line 18
            echo "    <ul class=\"pagination\">

    ";
            // line 20
            if (array_key_exists("previous", $context)) {
                // line 21
                echo "        <li>
            <a href=\"";
                // line 22
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")), twig_array_merge((isset($context["query"]) ? $context["query"] : $this->getContext($context, "query")), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : $this->getContext($context, "pageParameterName")) => (isset($context["previous"]) ? $context["previous"] : $this->getContext($context, "previous"))))), "html", null, true);
                echo "\">&laquo;&nbsp;";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Previous"), "html", null, true);
                echo "</a>
        </li>
    ";
            } else {
                // line 25
                echo "        <li class=\"disabled\">
            <span>&laquo;&nbsp;";
                // line 26
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Previous"), "html", null, true);
                echo "</span>
        </li>
    ";
            }
            // line 29
            echo "
    ";
            // line 30
            if (((isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage")) > 1)) {
                // line 31
                echo "        <li>
            <a href=\"";
                // line 32
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")), twig_array_merge((isset($context["query"]) ? $context["query"] : $this->getContext($context, "query")), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : $this->getContext($context, "pageParameterName")) => 1))), "html", null, true);
                echo "\">1</a>
        </li>
        ";
                // line 34
                if (((isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage")) == 3)) {
                    // line 35
                    echo "            <li>
                <a href=\"";
                    // line 36
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")), twig_array_merge((isset($context["query"]) ? $context["query"] : $this->getContext($context, "query")), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : $this->getContext($context, "pageParameterName")) => 2))), "html", null, true);
                    echo "\">2</a>
            </li>
        ";
                } elseif (((isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage")) != 2)) {
                    // line 39
                    echo "        <li class=\"disabled\">
            <span>&hellip;</span>
        </li>
        ";
                }
                // line 43
                echo "    ";
            }
            // line 44
            echo "
    ";
            // line 45
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["pagesInRange"]) ? $context["pagesInRange"] : $this->getContext($context, "pagesInRange")));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 46
                echo "        ";
                if (((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")) != (isset($context["current"]) ? $context["current"] : $this->getContext($context, "current")))) {
                    // line 47
                    echo "            <li>
                <a href=\"";
                    // line 48
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")), twig_array_merge((isset($context["query"]) ? $context["query"] : $this->getContext($context, "query")), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : $this->getContext($context, "pageParameterName")) => (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page"))))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "html", null, true);
                    echo "</a>
            </li>
        ";
                } else {
                    // line 51
                    echo "            <li class=\"active\">
                <span>";
                    // line 52
                    echo twig_escape_filter($this->env, (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "html", null, true);
                    echo "</span>
            </li>
        ";
                }
                // line 55
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "
    ";
            // line 58
            if (((isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount")) > (isset($context["endPage"]) ? $context["endPage"] : $this->getContext($context, "endPage")))) {
                // line 59
                echo "        ";
                if (((isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount")) > ((isset($context["endPage"]) ? $context["endPage"] : $this->getContext($context, "endPage")) + 1))) {
                    // line 60
                    echo "            ";
                    if (((isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount")) > ((isset($context["endPage"]) ? $context["endPage"] : $this->getContext($context, "endPage")) + 2))) {
                        // line 61
                        echo "                <li class=\"disabled\">
                    <span>&hellip;</span>
                </li>
            ";
                    } else {
                        // line 65
                        echo "                <li>
                    <a href=\"";
                        // line 66
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")), twig_array_merge((isset($context["query"]) ? $context["query"] : $this->getContext($context, "query")), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : $this->getContext($context, "pageParameterName")) => ((isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount")) - 1)))), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, ((isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount")) - 1), "html", null, true);
                        echo "</a>
                </li>
            ";
                    }
                    // line 69
                    echo "        ";
                }
                // line 70
                echo "        <li>
            <a href=\"";
                // line 71
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")), twig_array_merge((isset($context["query"]) ? $context["query"] : $this->getContext($context, "query")), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : $this->getContext($context, "pageParameterName")) => (isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount"))))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, (isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount")), "html", null, true);
                echo "</a>
        </li>
    ";
            }
            // line 74
            echo "
    ";
            // line 75
            if (array_key_exists("next", $context)) {
                // line 76
                echo "        <li>
            <a href=\"";
                // line 77
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")), twig_array_merge((isset($context["query"]) ? $context["query"] : $this->getContext($context, "query")), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : $this->getContext($context, "pageParameterName")) => (isset($context["next"]) ? $context["next"] : $this->getContext($context, "next"))))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Next"), "html", null, true);
                echo "&nbsp;&raquo;</a>
        </li>
    ";
            } else {
                // line 80
                echo "        <li class=\"disabled\">
            <span>";
                // line 81
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Next"), "html", null, true);
                echo "&nbsp;&raquo;</span>
        </li>
    ";
            }
            // line 84
            echo "    </ul>
";
        }
    }

    public function getTemplateName()
    {
        return "KnpPaginatorBundle:Pagination:twitter_bootstrap_v3_pagination.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  502 => 279,  723 => 375,  717 => 372,  701 => 359,  679 => 349,  673 => 346,  655 => 337,  521 => 268,  489 => 251,  635 => 330,  631 => 328,  574 => 286,  566 => 283,  533 => 274,  505 => 244,  387 => 213,  364 => 64,  352 => 196,  991 => 405,  908 => 300,  906 => 299,  901 => 296,  887 => 280,  880 => 257,  862 => 243,  856 => 240,  830 => 224,  820 => 218,  806 => 217,  799 => 214,  789 => 210,  785 => 209,  779 => 207,  743 => 197,  720 => 192,  714 => 191,  697 => 358,  666 => 178,  660 => 177,  647 => 332,  638 => 174,  517 => 158,  850 => 388,  843 => 383,  836 => 379,  825 => 221,  823 => 373,  817 => 370,  808 => 365,  802 => 363,  777 => 350,  768 => 346,  757 => 204,  729 => 329,  726 => 328,  719 => 325,  713 => 321,  707 => 318,  704 => 317,  691 => 355,  684 => 309,  678 => 305,  672 => 302,  669 => 301,  659 => 297,  656 => 296,  642 => 288,  610 => 270,  575 => 295,  527 => 231,  511 => 225,  491 => 215,  476 => 209,  421 => 210,  415 => 207,  383 => 181,  736 => 438,  732 => 437,  685 => 186,  648 => 380,  568 => 253,  528 => 314,  524 => 256,  460 => 219,  425 => 220,  374 => 214,  324 => 123,  661 => 340,  636 => 374,  629 => 323,  618 => 365,  589 => 303,  587 => 382,  559 => 364,  547 => 237,  537 => 232,  530 => 345,  510 => 302,  453 => 233,  392 => 226,  369 => 191,  1843 => 1037,  1834 => 1030,  1747 => 947,  1742 => 944,  1728 => 936,  1718 => 929,  1706 => 920,  1694 => 915,  1692 => 914,  1683 => 908,  1677 => 905,  1668 => 899,  1660 => 894,  1656 => 893,  1652 => 892,  1645 => 888,  1641 => 887,  1630 => 882,  1626 => 881,  1622 => 880,  1614 => 874,  1601 => 832,  1595 => 829,  1586 => 823,  1582 => 822,  1576 => 819,  1567 => 813,  1563 => 812,  1557 => 809,  1547 => 802,  1543 => 801,  1537 => 798,  1528 => 792,  1518 => 788,  1500 => 773,  1496 => 772,  1492 => 771,  1488 => 770,  1482 => 767,  1473 => 761,  1469 => 760,  1465 => 759,  1461 => 758,  1448 => 750,  1438 => 748,  1436 => 747,  1432 => 746,  1428 => 745,  1422 => 742,  1413 => 736,  1409 => 735,  1403 => 732,  1389 => 724,  1370 => 711,  1364 => 708,  1351 => 701,  1345 => 698,  1332 => 691,  1326 => 688,  1313 => 681,  1307 => 678,  1294 => 671,  1288 => 668,  1275 => 661,  1269 => 658,  1256 => 651,  1250 => 648,  1237 => 641,  1231 => 638,  1218 => 631,  1212 => 628,  1199 => 621,  1193 => 618,  1180 => 611,  1155 => 598,  1142 => 591,  1136 => 588,  1127 => 582,  1117 => 578,  1108 => 572,  1098 => 568,  1079 => 558,  1071 => 552,  1067 => 550,  1065 => 549,  1054 => 541,  1050 => 540,  1044 => 537,  1035 => 531,  1031 => 530,  1025 => 527,  1016 => 521,  1006 => 517,  997 => 511,  993 => 510,  987 => 507,  978 => 501,  974 => 500,  959 => 491,  955 => 490,  949 => 487,  940 => 481,  936 => 480,  930 => 477,  921 => 471,  911 => 467,  902 => 461,  898 => 460,  892 => 457,  883 => 258,  879 => 450,  864 => 441,  854 => 437,  845 => 431,  835 => 226,  826 => 421,  816 => 417,  805 => 411,  801 => 410,  797 => 213,  791 => 406,  778 => 399,  772 => 348,  763 => 343,  759 => 389,  753 => 386,  744 => 380,  740 => 379,  721 => 369,  715 => 366,  687 => 350,  677 => 183,  668 => 340,  643 => 331,  624 => 314,  614 => 393,  605 => 304,  595 => 266,  538 => 267,  519 => 257,  485 => 250,  481 => 154,  447 => 246,  262 => 136,  281 => 136,  197 => 92,  419 => 185,  308 => 173,  260 => 133,  228 => 117,  355 => 165,  346 => 193,  288 => 150,  692 => 188,  603 => 170,  561 => 287,  515 => 265,  475 => 262,  379 => 72,  348 => 111,  298 => 114,  1174 => 608,  1167 => 718,  1161 => 601,  1154 => 710,  1149 => 707,  1141 => 704,  1134 => 702,  1131 => 701,  1129 => 700,  1114 => 695,  1111 => 694,  1109 => 693,  1105 => 692,  1096 => 689,  1092 => 688,  1089 => 562,  1072 => 675,  1064 => 670,  1042 => 651,  1034 => 645,  1026 => 642,  1022 => 640,  1019 => 639,  1017 => 638,  1012 => 520,  1008 => 635,  1004 => 633,  1002 => 632,  996 => 629,  988 => 626,  984 => 624,  979 => 623,  977 => 622,  968 => 497,  962 => 613,  951 => 605,  917 => 470,  915 => 585,  912 => 328,  905 => 580,  897 => 575,  893 => 574,  881 => 568,  873 => 447,  871 => 249,  863 => 555,  853 => 496,  842 => 231,  831 => 376,  812 => 464,  800 => 362,  798 => 455,  787 => 447,  754 => 203,  747 => 390,  739 => 333,  731 => 410,  725 => 370,  718 => 404,  696 => 356,  681 => 401,  675 => 182,  665 => 341,  663 => 298,  651 => 359,  628 => 344,  606 => 269,  602 => 268,  600 => 356,  579 => 296,  572 => 311,  555 => 303,  542 => 269,  532 => 264,  513 => 254,  501 => 329,  499 => 271,  473 => 153,  441 => 243,  438 => 209,  416 => 200,  330 => 170,  289 => 108,  633 => 320,  627 => 398,  621 => 289,  619 => 319,  594 => 353,  576 => 344,  570 => 254,  562 => 163,  558 => 335,  552 => 251,  540 => 298,  508 => 283,  498 => 314,  486 => 270,  480 => 262,  454 => 249,  450 => 215,  410 => 133,  325 => 102,  220 => 111,  407 => 224,  402 => 194,  377 => 178,  313 => 153,  232 => 118,  465 => 258,  457 => 192,  417 => 215,  411 => 225,  408 => 196,  396 => 197,  391 => 214,  388 => 222,  372 => 188,  344 => 109,  332 => 182,  293 => 164,  274 => 90,  231 => 99,  200 => 100,  792 => 418,  788 => 356,  782 => 208,  776 => 439,  774 => 349,  762 => 206,  748 => 200,  742 => 334,  734 => 193,  730 => 385,  724 => 327,  716 => 403,  706 => 360,  698 => 314,  694 => 313,  688 => 364,  680 => 184,  676 => 358,  662 => 350,  658 => 384,  652 => 411,  644 => 405,  640 => 375,  634 => 337,  622 => 274,  616 => 273,  608 => 323,  598 => 167,  580 => 165,  564 => 338,  545 => 353,  541 => 321,  526 => 159,  507 => 260,  488 => 321,  462 => 227,  433 => 207,  424 => 184,  395 => 223,  382 => 219,  376 => 207,  341 => 223,  327 => 161,  320 => 166,  310 => 159,  291 => 129,  278 => 155,  259 => 120,  244 => 117,  448 => 222,  443 => 229,  429 => 212,  406 => 195,  366 => 182,  318 => 176,  282 => 147,  258 => 83,  222 => 114,  120 => 57,  272 => 140,  266 => 136,  226 => 114,  178 => 80,  111 => 34,  393 => 187,  386 => 196,  380 => 193,  362 => 184,  358 => 207,  342 => 108,  340 => 177,  334 => 196,  326 => 169,  319 => 134,  314 => 144,  299 => 95,  265 => 89,  252 => 129,  237 => 112,  194 => 104,  132 => 51,  23 => 2,  97 => 36,  81 => 33,  53 => 30,  654 => 176,  637 => 328,  632 => 280,  625 => 322,  623 => 324,  617 => 195,  612 => 362,  604 => 357,  593 => 327,  591 => 186,  586 => 348,  583 => 292,  578 => 379,  571 => 164,  557 => 286,  534 => 242,  522 => 288,  520 => 340,  504 => 248,  494 => 216,  463 => 262,  446 => 148,  440 => 136,  434 => 141,  431 => 223,  427 => 182,  405 => 233,  401 => 221,  397 => 114,  389 => 220,  381 => 210,  357 => 62,  349 => 205,  339 => 172,  303 => 149,  295 => 113,  287 => 161,  268 => 139,  74 => 39,  470 => 398,  452 => 248,  444 => 212,  435 => 224,  430 => 235,  414 => 183,  412 => 198,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 940,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 916,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 886,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 833,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 791,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 755,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 725,  1387 => 694,  1378 => 688,  1374 => 712,  1368 => 684,  1359 => 678,  1355 => 702,  1349 => 674,  1340 => 668,  1336 => 692,  1330 => 664,  1321 => 658,  1317 => 682,  1311 => 654,  1302 => 648,  1298 => 672,  1292 => 644,  1283 => 638,  1279 => 662,  1273 => 634,  1264 => 628,  1260 => 652,  1254 => 624,  1245 => 618,  1241 => 642,  1235 => 614,  1226 => 608,  1222 => 632,  1216 => 604,  1207 => 598,  1203 => 622,  1197 => 594,  1188 => 588,  1184 => 612,  1178 => 584,  1169 => 578,  1165 => 602,  1159 => 574,  1150 => 568,  1146 => 592,  1140 => 564,  1123 => 581,  1119 => 697,  1113 => 546,  1104 => 571,  1100 => 690,  1094 => 536,  1085 => 561,  1081 => 529,  1075 => 526,  1066 => 671,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 599,  933 => 450,  929 => 588,  923 => 446,  914 => 440,  910 => 439,  904 => 298,  895 => 430,  891 => 282,  885 => 259,  874 => 420,  870 => 419,  866 => 418,  860 => 440,  851 => 409,  847 => 233,  841 => 382,  832 => 225,  828 => 398,  822 => 420,  813 => 368,  809 => 388,  803 => 216,  794 => 359,  790 => 357,  784 => 354,  775 => 369,  771 => 368,  765 => 431,  756 => 359,  752 => 202,  746 => 355,  737 => 383,  733 => 330,  727 => 376,  712 => 420,  708 => 419,  702 => 416,  693 => 323,  689 => 187,  683 => 350,  674 => 241,  670 => 355,  664 => 339,  649 => 293,  645 => 296,  639 => 323,  630 => 371,  626 => 276,  620 => 172,  611 => 314,  607 => 313,  601 => 310,  592 => 298,  588 => 261,  582 => 347,  563 => 348,  554 => 344,  550 => 246,  544 => 161,  535 => 296,  531 => 294,  516 => 253,  512 => 318,  506 => 223,  493 => 294,  478 => 210,  468 => 255,  459 => 200,  455 => 209,  449 => 232,  436 => 216,  428 => 236,  422 => 233,  409 => 226,  403 => 223,  390 => 216,  384 => 213,  351 => 206,  337 => 126,  311 => 161,  296 => 142,  256 => 104,  241 => 80,  215 => 101,  207 => 96,  192 => 96,  186 => 92,  175 => 149,  153 => 69,  118 => 35,  61 => 12,  34 => 5,  65 => 35,  77 => 16,  37 => 10,  190 => 93,  161 => 83,  137 => 65,  126 => 63,  261 => 123,  255 => 197,  247 => 98,  242 => 125,  214 => 96,  211 => 97,  191 => 157,  157 => 59,  145 => 40,  127 => 61,  373 => 68,  367 => 154,  363 => 170,  359 => 187,  354 => 207,  343 => 174,  335 => 145,  328 => 181,  322 => 178,  315 => 150,  309 => 152,  305 => 146,  302 => 170,  290 => 148,  284 => 152,  279 => 132,  271 => 89,  264 => 122,  248 => 128,  236 => 110,  223 => 76,  170 => 77,  110 => 50,  96 => 48,  84 => 25,  472 => 210,  467 => 241,  375 => 207,  371 => 206,  360 => 210,  356 => 197,  353 => 178,  350 => 181,  338 => 156,  336 => 221,  331 => 103,  321 => 158,  316 => 157,  307 => 160,  304 => 97,  301 => 157,  297 => 141,  292 => 151,  286 => 147,  283 => 127,  277 => 132,  275 => 131,  270 => 137,  263 => 146,  257 => 143,  253 => 53,  249 => 117,  245 => 110,  233 => 110,  225 => 105,  216 => 117,  206 => 103,  202 => 103,  198 => 96,  185 => 86,  180 => 89,  177 => 81,  165 => 75,  150 => 71,  124 => 65,  113 => 55,  100 => 33,  58 => 32,  251 => 119,  234 => 97,  213 => 102,  195 => 94,  174 => 75,  167 => 76,  146 => 70,  140 => 66,  128 => 60,  104 => 51,  90 => 46,  83 => 44,  52 => 9,  596 => 225,  590 => 297,  585 => 221,  577 => 357,  573 => 257,  569 => 292,  560 => 296,  556 => 211,  553 => 162,  551 => 283,  546 => 207,  543 => 278,  539 => 277,  529 => 322,  525 => 269,  523 => 230,  518 => 287,  514 => 286,  509 => 278,  503 => 259,  500 => 278,  497 => 256,  495 => 313,  492 => 323,  490 => 271,  487 => 213,  484 => 275,  482 => 177,  479 => 247,  477 => 271,  474 => 206,  471 => 261,  469 => 276,  466 => 228,  464 => 254,  461 => 238,  458 => 251,  456 => 199,  451 => 247,  445 => 244,  442 => 258,  439 => 248,  437 => 214,  432 => 237,  426 => 234,  423 => 141,  420 => 231,  418 => 201,  413 => 227,  399 => 137,  394 => 217,  378 => 119,  370 => 183,  368 => 187,  365 => 203,  361 => 116,  347 => 195,  345 => 160,  333 => 159,  329 => 158,  323 => 182,  317 => 179,  312 => 133,  306 => 158,  300 => 155,  294 => 116,  285 => 193,  280 => 144,  276 => 176,  267 => 128,  250 => 126,  239 => 113,  229 => 103,  218 => 74,  212 => 107,  210 => 104,  205 => 104,  188 => 95,  184 => 97,  181 => 81,  169 => 79,  160 => 78,  152 => 74,  148 => 69,  139 => 80,  134 => 79,  114 => 39,  107 => 52,  76 => 20,  70 => 28,  273 => 131,  269 => 128,  254 => 121,  246 => 125,  243 => 114,  240 => 122,  238 => 126,  235 => 106,  230 => 115,  227 => 103,  224 => 104,  221 => 104,  219 => 98,  217 => 100,  208 => 106,  204 => 99,  179 => 83,  159 => 75,  143 => 65,  135 => 60,  131 => 61,  108 => 36,  102 => 47,  71 => 29,  67 => 26,  63 => 34,  59 => 11,  47 => 13,  94 => 30,  89 => 28,  85 => 37,  79 => 25,  75 => 29,  68 => 36,  56 => 13,  50 => 29,  38 => 10,  29 => 4,  87 => 22,  72 => 23,  55 => 31,  21 => 2,  26 => 6,  35 => 5,  31 => 4,  41 => 25,  28 => 20,  201 => 87,  196 => 88,  183 => 152,  171 => 78,  166 => 81,  163 => 76,  156 => 79,  151 => 70,  142 => 70,  138 => 70,  136 => 56,  123 => 58,  121 => 58,  115 => 33,  105 => 35,  101 => 41,  91 => 29,  69 => 19,  66 => 20,  62 => 19,  49 => 9,  98 => 39,  93 => 47,  88 => 25,  78 => 30,  46 => 13,  44 => 26,  32 => 7,  27 => 5,  43 => 12,  40 => 6,  25 => 3,  24 => 18,  172 => 85,  158 => 80,  155 => 74,  129 => 68,  119 => 34,  117 => 51,  20 => 1,  22 => 17,  19 => 16,  209 => 101,  203 => 95,  199 => 90,  193 => 93,  189 => 87,  187 => 84,  182 => 92,  176 => 91,  173 => 80,  168 => 84,  164 => 47,  162 => 74,  154 => 71,  149 => 68,  147 => 74,  144 => 73,  141 => 39,  133 => 69,  130 => 77,  125 => 59,  122 => 48,  116 => 44,  112 => 51,  109 => 40,  106 => 49,  103 => 34,  99 => 42,  95 => 38,  92 => 26,  86 => 45,  82 => 26,  80 => 43,  73 => 23,  64 => 25,  60 => 13,  57 => 11,  54 => 15,  51 => 13,  48 => 7,  45 => 12,  42 => 11,  39 => 8,  36 => 11,  33 => 22,  30 => 21,);
    }
}
