<?php

/* PatCompteBundle:AdminCalendrier:liste_content.html.twig */
class __TwigTemplate_eb81dc67dfaa9db182240297d3fd50b060e0a4925d1986819c8ed407053b3246 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "
  <div id=\"contenuCentrale1\">
    <h1>Disponibilités <small>(Réf : ";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "reference", array()), "html", null, true);
        echo ")</small></h1>

    <div class=\"clear\"></div>

    <div class=\"etapesBien\">
      <div class=\"linksEtapesBien\">
        <ul>
          <li><a href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_appartement_editer", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Description</a></li>
          <li><a href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_appartement_modif_proprietaire", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Propriétaire</a></li>
          <li><a href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_tarif_ajouter", array("id_bien" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Loyers</a></li>
          <li><a href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_promotion_index", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Promotion / Majoration</a></li>
          <li><a href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_piece_index", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Pièces</a></li>
          <li><a href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_media_ajouter", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Images</a></li>
          <li><a class=\"active\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_calendrier_index", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Disponibilités</a></li>
        </ul>
      </div>
    </div>

    ";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 23
            echo "      <div class=\"alert alert-success\">
        ";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage"))), "html", null, true);
            echo "
      </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "

    ";
        // line 29
        if ((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form"))) {
            // line 30
            echo "      <div class=\"blocForm\">
        <div class=\"content\">
          <form id=\"form_recherche\" action=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_calendrier_index", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
            echo "\" method=\"post\">
            <div class=\"row-fluid\">
              <div class=\"span5 center\">
                ";
            // line 35
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mois", array()), 'widget');
            echo "
              </div>
              <div class=\"span5 center\">
                ";
            // line 38
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "annee", array()), 'widget');
            echo "
              </div>
              <div class=\"span2 center\">
                <input type=\"submit\" value=\"Rechercher\" class=\"btn\"/>
              </div>
            </div>
            ";
            // line 44
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
            echo "
          </form>
        </div>
      </div>
    ";
        } else {
            // line 49
            echo "      <br/><br/>
      <div class=\"alert\">
        Vous devez avoir au moins un appartement pour pouvoir visualiser le calendrier.
      </div>
    ";
        }
        // line 54
        echo "

    ";
        // line 56
        if ((isset($context["tabCal"]) ? $context["tabCal"] : $this->getContext($context, "tabCal"))) {
            // line 57
            echo "      <br/>
      <div class=\"row-fluid\">
        <div class=\"span4\">
          <div style=\"width:20px; height:20px; background-color:";
            // line 60
            echo twig_escape_filter($this->env, twig_constant("\\Pat\\CompteBundle\\Tools\\CalendrierTools::CONSTANT_COLOR_BLOQUE_RESERVATION"), "html", null, true);
            echo "; display: inline-block;\"></div> Bloqué par une réservation en ligne
        </div>
        <div class=\"span4\">
          <div style=\"width:20px; height:20px; background-color:";
            // line 63
            echo twig_escape_filter($this->env, twig_constant("\\Pat\\CompteBundle\\Tools\\CalendrierTools::CONSTANT_COLOR_BLOQUE_PROPRIETAIRE"), "html", null, true);
            echo "; display: inline-block;\"></div> Bloqué par le propriétaire
        </div>
        <div class=\"span4\">
          <div style=\"width:20px; height:20px; background-color:";
            // line 66
            echo twig_escape_filter($this->env, twig_constant("\\Pat\\CompteBundle\\Tools\\CalendrierTools::CONSTANT_COLOR_BLOQUE_ADMIN"), "html", null, true);
            echo "; display: inline-block;\"></div> Bloqué par Class Appart ®
        </div>
      </div>
      <div class=\"row-fluid\">
        <div class=\"span4\">
          <div style=\"width:20px; height:20px; background-color:LightGray; display: inline-block;\"></div> Attente Acompte
        </div>
        <div class=\"span4\">
          <div style=\"width:20px; height:20px; background-color:DarkGray; display: inline-block;\"></div> Attente validation locataire
        </div>
        <div class=\"span4\">
          <div style=\"width:20px; height:20px; background-color:Gray; display: inline-block;\"></div> Attente validation propriétaire
        </div>
      </div>
      <br/>
      <div class=\"row-fluid\">
        <div class=\"span3\">
          <div style=\"width:20px; height:20px; background-color:Yellow; display: inline-block;\"></div> Acompte versé
        </div>
        <div class=\"span3\">
          <div style=\"width:20px; height:20px; background-color:DarkOrange; display: inline-block;\"></div> A solder
        </div>
        <div class=\"span3\">
          <div style=\"width:20px; height:20px; background-color:Green; display: inline-block;\"></div> Soldé
        </div>
        <div class=\"span3\">
          <div style=\"width:20px; height:20px; background-color:Purple; display: inline-block;\"></div> Excédant
        </div>
      </div>
      <div class=\"row-fluid\">
        <div class=\"span3\">
          ";
            // line 98
            echo "          <div class=\"bloc-menage\" style=\"width:20px; height:20px; display: inline-block;\"></div> Ménage
        </div>
        <div class=\"span4\">
          <div style=\"width:20px; height:20px; background-color:";
            // line 101
            echo twig_escape_filter($this->env, twig_constant("\\Pat\\CompteBundle\\Tools\\CalendrierTools::CONSTANT_COLOR_BLOQUE_ADMIN_TRAVAUX"), "html", null, true);
            echo "; display: inline-block;\"></div> Bloqué pour travaux
        </div>
      </div>

      ";
            // line 106
            echo "        ";
            echo (isset($context["tabCal"]) ? $context["tabCal"] : $this->getContext($context, "tabCal"));
            echo "
      ";
            // line 108
            echo "    ";
        }
        // line 109
        echo "
    ";
        // line 110
        if ((isset($context["tableauCalendrier"]) ? $context["tableauCalendrier"] : $this->getContext($context, "tableauCalendrier"))) {
            // line 111
            echo "      <script type=\"text/javascript\">
          \$(\"#calendrierrecherche_mois option[value=";
            // line 112
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tableauCalendrier"]) ? $context["tableauCalendrier"] : $this->getContext($context, "tableauCalendrier")), "mois", array(), "array"), "html", null, true);
            echo "]\").attr('selected', 'selected');
          \$(\"#calendrierrecherche_annee option[value=";
            // line 113
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tableauCalendrier"]) ? $context["tableauCalendrier"] : $this->getContext($context, "tableauCalendrier")), "annee", array(), "array"), "html", null, true);
            echo "]\").attr('selected', 'selected');
      </script>
    ";
        } else {
            // line 116
            echo "      <script type=\"text/javascript\">
        var d = new Date();
        var mois = d.getMonth() + 1 + \"\";
        var annee = d.getFullYear() + \"\";
        if (mois.length == \"1\") {
          mois = \"0\" + mois;
        }
        \$(\"#calendrierrecherche_mois option[value=\" + mois + \"]\").attr('selected', 'selected');
        \$(\"#calendrierrecherche_annee option[value=\" + annee + \"]\").attr('selected', 'selected');
      </script>
    ";
        }
        // line 127
        echo "

    <br/><br/>
    <a href=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_calendrier_ajouter", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-primary\">Bloquer / débloquer des dates</a>

    <br/><br/><br/>
    <div class=\"row-fluid\">
      <div class=\"span6\">
        <a href=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_media_ajouter", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-large\">Revenir</a>
      </div>
      <div class=\"span6 right\">
        <a href=\"";
        // line 138
        echo $this->env->getExtension('routing')->getPath("pat_admin_dashboard");
        echo "\" class=\"btn btn-primary btn-large\">Terminer</a>
      </div>
    </div>
  </div><!-- /contenuCentrale1 -->

";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:AdminCalendrier:liste_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  261 => 138,  255 => 135,  247 => 130,  242 => 127,  214 => 110,  211 => 109,  191 => 98,  157 => 66,  145 => 60,  127 => 49,  373 => 176,  367 => 173,  363 => 172,  359 => 171,  354 => 169,  343 => 161,  335 => 156,  328 => 152,  322 => 149,  315 => 144,  309 => 140,  305 => 138,  302 => 137,  290 => 133,  284 => 132,  279 => 129,  271 => 126,  264 => 122,  248 => 118,  236 => 110,  223 => 113,  170 => 74,  110 => 38,  96 => 32,  84 => 28,  472 => 149,  467 => 148,  375 => 146,  371 => 130,  360 => 99,  356 => 170,  353 => 99,  350 => 98,  338 => 101,  336 => 98,  331 => 95,  321 => 89,  316 => 87,  307 => 85,  304 => 84,  301 => 83,  297 => 70,  292 => 67,  286 => 65,  283 => 64,  277 => 71,  275 => 64,  270 => 61,  263 => 25,  257 => 121,  253 => 119,  249 => 21,  245 => 20,  233 => 23,  225 => 21,  216 => 111,  206 => 22,  202 => 91,  198 => 20,  185 => 13,  180 => 12,  177 => 11,  165 => 154,  150 => 63,  124 => 49,  113 => 83,  100 => 60,  58 => 30,  251 => 123,  234 => 112,  213 => 104,  195 => 93,  174 => 75,  167 => 77,  146 => 58,  140 => 57,  128 => 113,  104 => 35,  90 => 28,  83 => 24,  52 => 14,  596 => 225,  590 => 224,  585 => 221,  577 => 218,  573 => 216,  569 => 214,  560 => 212,  556 => 211,  553 => 210,  551 => 209,  546 => 207,  543 => 206,  539 => 205,  529 => 197,  525 => 195,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 185,  500 => 184,  497 => 183,  495 => 182,  492 => 181,  490 => 180,  487 => 179,  484 => 178,  482 => 177,  479 => 176,  477 => 175,  474 => 174,  471 => 173,  469 => 172,  466 => 171,  464 => 170,  461 => 169,  458 => 168,  456 => 167,  451 => 164,  445 => 160,  442 => 159,  439 => 158,  437 => 157,  432 => 154,  426 => 150,  423 => 149,  420 => 148,  418 => 147,  413 => 144,  399 => 143,  394 => 140,  378 => 137,  370 => 135,  368 => 129,  365 => 128,  361 => 131,  347 => 125,  345 => 94,  333 => 121,  329 => 94,  323 => 117,  317 => 116,  312 => 114,  306 => 113,  300 => 110,  294 => 68,  285 => 105,  280 => 103,  276 => 102,  267 => 60,  250 => 100,  239 => 95,  229 => 116,  218 => 82,  212 => 95,  210 => 23,  205 => 100,  188 => 89,  184 => 73,  181 => 85,  169 => 66,  160 => 59,  152 => 54,  148 => 53,  139 => 48,  134 => 54,  114 => 42,  107 => 33,  76 => 23,  70 => 33,  273 => 127,  269 => 94,  254 => 92,  246 => 117,  243 => 88,  240 => 19,  238 => 113,  235 => 94,  230 => 111,  227 => 81,  224 => 109,  221 => 20,  219 => 112,  217 => 75,  208 => 108,  204 => 72,  179 => 69,  159 => 61,  143 => 59,  135 => 53,  131 => 44,  108 => 36,  102 => 74,  71 => 21,  67 => 20,  63 => 13,  59 => 14,  47 => 9,  94 => 30,  89 => 20,  85 => 19,  79 => 24,  75 => 24,  68 => 14,  56 => 15,  50 => 10,  38 => 6,  29 => 3,  87 => 29,  72 => 22,  55 => 12,  21 => 2,  26 => 2,  35 => 5,  31 => 3,  41 => 7,  28 => 1,  201 => 92,  196 => 101,  183 => 81,  171 => 6,  166 => 61,  163 => 128,  156 => 66,  151 => 63,  142 => 49,  138 => 56,  136 => 56,  123 => 46,  121 => 46,  115 => 43,  105 => 40,  101 => 30,  91 => 27,  69 => 25,  66 => 32,  62 => 31,  49 => 27,  98 => 32,  93 => 9,  88 => 27,  78 => 22,  46 => 26,  44 => 12,  32 => 4,  27 => 4,  43 => 6,  40 => 11,  25 => 3,  24 => 3,  172 => 106,  158 => 67,  155 => 65,  129 => 119,  119 => 44,  117 => 43,  20 => 1,  22 => 2,  19 => 1,  209 => 82,  203 => 106,  199 => 71,  193 => 85,  189 => 84,  187 => 84,  182 => 70,  176 => 64,  173 => 68,  168 => 72,  164 => 71,  162 => 99,  154 => 58,  149 => 51,  147 => 119,  144 => 118,  141 => 117,  133 => 55,  130 => 52,  125 => 38,  122 => 37,  116 => 41,  112 => 41,  109 => 34,  106 => 33,  103 => 37,  99 => 31,  95 => 28,  92 => 29,  86 => 28,  82 => 22,  80 => 27,  73 => 23,  64 => 17,  60 => 16,  57 => 11,  54 => 29,  51 => 14,  48 => 13,  45 => 8,  42 => 6,  39 => 9,  36 => 6,  33 => 3,  30 => 4,);
    }
}
