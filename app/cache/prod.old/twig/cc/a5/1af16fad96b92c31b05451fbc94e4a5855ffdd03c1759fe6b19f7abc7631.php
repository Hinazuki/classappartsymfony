<?php

/* PatCompteBundle:AdminCalendrier:ajouter_content.html.twig */
class __TwigTemplate_cca51af16fad96b92c31b05451fbc94e4a5855ffdd03c1759fe6b19f7abc7631 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "
  <div id=\"contenuCentrale1\">
    <h1>Bloquer / Débloquer des dates <small>(Réf : ";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "reference", array()), "html", null, true);
        echo ")</small></h1>

    <div class=\"clear\"></div>

    <div class=\"etapesBien\">
      <div class=\"linksEtapesBien\">
        <ul>
          <li><a href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_appartement_editer", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Description</a></li>
          <li><a href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_appartement_modif_proprietaire", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Propriétaire</a></li>
          <li><a href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_tarif_ajouter", array("id_bien" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Loyers</a></li>
          <li><a href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_promotion_index", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Promotion / Majoration</a></li>s
          <li><a href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_piece_index", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Pièces</a></li>
          <li><a href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_media_ajouter", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Images</a></li>
          <li><a class=\"active\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_calendrier_index", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
        echo "\">Disponibilités</a></li>
        </ul>
      </div>
    </div>


    ";
        // line 23
        if ((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form"))) {
            // line 24
            echo "      <div class=\"blocForm\">
        <div class=\"content\">

          ";
            // line 27
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
                // line 28
                echo "            <div class=\"alert alert-success\">
              ";
                // line 29
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage"))), "html", null, true);
                echo "
            </div>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "
          <form id=\"form_recherche\" action=\"\" method=\"post\">

            <div class=\"row-fluid\">
              <div class=\"span4 right\">
                ";
            // line 37
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_debut", array()), 'label');
            echo "
              </div>
              <div class=\"span8\">
                <div>
                  ";
            // line 41
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_debut", array()), 'errors');
            echo "
                </div>
                ";
            // line 43
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_debut", array()), 'widget', array("attr" => array("class" => "cal_date_in_admin")));
            echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4 right\">
                ";
            // line 49
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "heure_debut", array()), 'label');
            echo "
              </div>
              <div class=\"span8\">
                ";
            // line 52
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "heure_debut", array()), 'errors');
            echo "
                ";
            // line 53
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "heure_debut", array()), 'widget');
            echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4 right\">
                ";
            // line 59
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_fin", array()), 'label');
            echo "
              </div>
              <div class=\"span8\">
                <div>
                  ";
            // line 63
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_fin", array()), 'errors');
            echo "
                </div>
                ";
            // line 65
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_fin", array()), 'widget', array("attr" => array("class" => "cal_date_out_admin")));
            echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4 right\">
                ";
            // line 71
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "heure_fin", array()), 'label');
            echo "
              </div>
              <div class=\"span8\">
                ";
            // line 74
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "heure_fin", array()), 'errors');
            echo "
                ";
            // line 75
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "heure_fin", array()), 'widget');
            echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4 right\">
                ";
            // line 81
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "status", array()), 'label');
            echo "
              </div>
              <div class=\"span8\">
                ";
            // line 84
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "status", array()), 'errors');
            echo "
                ";
            // line 85
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "status", array()), 'widget');
            echo "
              </div>
            </div>

            <div class=\"row-fluid\" id=\"intitule\">
              <div class=\"span4 right\">
                ";
            // line 91
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "intitule", array()), 'label');
            echo "
              </div>
              <div class=\"span8\">
                ";
            // line 94
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "intitule", array()), 'errors');
            echo "
                ";
            // line 95
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "intitule", array()), 'widget');
            echo "
              </div>
            </div>

            <br/><br/>

            <div class=\"row-fluid\">
              <div class=\"span6\">
                <a href=\"";
            // line 103
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_calendrier_index", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-large\">Revenir</a>
              </div>
              <div class=\"span6 right\">
                <input class=\"btn btn-primary btn-large\" type=\"submit\" value=\"";
            // line 106
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enregistrer"), "html", null, true);
            echo "\"/>
              </div>
            </div>

            ";
            // line 110
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
            echo "
          </form>
        </div>
      </div>

      <br/>

      ";
            // line 117
            if ((isset($context["dates_bloquees"]) ? $context["dates_bloquees"] : $this->getContext($context, "dates_bloquees"))) {
                // line 118
                echo "        ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["dates_bloquees"]) ? $context["dates_bloquees"] : $this->getContext($context, "dates_bloquees")));
                foreach ($context['_seq'] as $context["_key"] => $context["date_bloquee"]) {
                    // line 119
                    echo "          <div class=\"ListeAppart row-fluid\">
            <div class=\"span4 ListeAppart1\" style=\"min-height: 80px;\">
              Période bloquée <br/>du <b>";
                    // line 121
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["date_bloquee"]) ? $context["date_bloquee"] : $this->getContext($context, "date_bloquee")), "dateDebut", array()), "d/m/Y"), "html", null, true);
                    echo twig_escape_filter($this->env, (((!(null === $this->getAttribute((isset($context["date_bloquee"]) ? $context["date_bloquee"] : $this->getContext($context, "date_bloquee")), "heureDebut", array())))) ? ((" " . twig_date_format_filter($this->env, $this->getAttribute((isset($context["date_bloquee"]) ? $context["date_bloquee"] : $this->getContext($context, "date_bloquee")), "heureDebut", array()), "H:i"))) : ("")), "html", null, true);
                    echo "</b> au <b>";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["date_bloquee"]) ? $context["date_bloquee"] : $this->getContext($context, "date_bloquee")), "dateFin", array()), "d/m/Y"), "html", null, true);
                    echo "
                ";
                    // line 122
                    echo twig_escape_filter($this->env, (((!(null === $this->getAttribute((isset($context["date_bloquee"]) ? $context["date_bloquee"] : $this->getContext($context, "date_bloquee")), "heureFin", array())))) ? (twig_date_format_filter($this->env, $this->getAttribute((isset($context["date_bloquee"]) ? $context["date_bloquee"] : $this->getContext($context, "date_bloquee")), "heureFin", array()), "H:i")) : ("")), "html", null, true);
                    echo "</b>
            </div>

            <div class=\"span6 ListeAppart1\" style=\"min-height: 80px; margin-left: 0;\">
              ";
                    // line 126
                    if ($this->getAttribute((isset($context["date_bloquee"]) ? $context["date_bloquee"] : $this->getContext($context, "date_bloquee")), "intitule", array())) {
                        // line 127
                        echo "                <b>";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["date_bloquee"]) ? $context["date_bloquee"] : $this->getContext($context, "date_bloquee")), "intitule", array()), "html", null, true);
                        echo "</b>
              ";
                    }
                    // line 129
                    echo "            </div>

            <div class=\"span2 ListeAppart2 center\">
              <a href=\"";
                    // line 132
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_calendrier_modifier", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()), "id_cal" => $this->getAttribute((isset($context["date_bloquee"]) ? $context["date_bloquee"] : $this->getContext($context, "date_bloquee")), "id", array()))), "html", null, true);
                    echo "\"><img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/images/ico_edition.png"), "html", null, true);
                    echo "\" alt=\"Modifier\"/></a>
              <a href=\"";
                    // line 133
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_calendrier_supprimer", array("id" => $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "id", array()), "id_cal" => $this->getAttribute((isset($context["date_bloquee"]) ? $context["date_bloquee"] : $this->getContext($context, "date_bloquee")), "id", array()))), "html", null, true);
                    echo "\" onclick=\"return confirmAction()\"><img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/images/ico_supprimer.png"), "html", null, true);
                    echo "\" alt=\"Supprimer\"/></a>
            </div>
          </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['date_bloquee'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 137
                echo "      ";
            }
            // line 138
            echo "
    ";
        } else {
            // line 140
            echo "      <div class=\"alert alert-error\">
        Vous devez avoir au moins un appartement pour pouvoir bloquer/débloquer des dates.
      </div>
    ";
        }
        // line 144
        echo "
  </div><!-- /contenuCentrale1 -->

  <script type=\"text/javascript\">
    \$(\"#calendrieradminajout_date_debut, #calendrieradminajout_date_fin\").attr('readonly', \"true\");
    if (\$(\"#calendrieradminajout_status\").val() != ";
        // line 149
        echo twig_escape_filter($this->env, twig_constant("\\Pat\\CompteBundle\\Tools\\CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN"), "html", null, true);
        echo ")
            \$(\"#intitule\").hide();
    \$(\"#calendrieradminajout_status\").change(function() {
    if (\$(\"#calendrieradminajout_status\").val() == ";
        // line 152
        echo twig_escape_filter($this->env, twig_constant("\\Pat\\CompteBundle\\Tools\\CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN"), "html", null, true);
        echo ") {
    \$(\"#calendrieradminajout_intitule\").val(\"\");
    \$(\"#intitule\").fadeIn();
    }
    else if (\$(\"#calendrieradminajout_status\").val() == ";
        // line 156
        echo twig_escape_filter($this->env, twig_constant("\\Pat\\CompteBundle\\Tools\\CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE"), "html", null, true);
        echo ") {
    \$(\"#intitule\").fadeOut(400, function(){
    \$(\"#calendrieradminajout_intitule\").val(\"Ménage\");
    });
    }
    else if (\$(\"#calendrieradminajout_status\").val() == ";
        // line 161
        echo twig_escape_filter($this->env, twig_constant("\\Pat\\CompteBundle\\Tools\\CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_TRAVAUX"), "html", null, true);
        echo ") {
    \$(\"#intitule\").fadeOut(400, function(){
    \$(\"#calendrieradminajout_intitule\").val(\"Travaux\");
    });
    }
    });
  </script>

  ";
        // line 169
        if ((isset($context["tableau"]) ? $context["tableau"] : $this->getContext($context, "tableau"))) {
            // line 170
            echo "    <script type=\"text/javascript\">
      document.getElementById(\"calendrieradminajout_date_debut\").value = '";
            // line 171
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tableau"]) ? $context["tableau"] : $this->getContext($context, "tableau")), "date_debut", array(), "array"), "html", null, true);
            echo "';
      document.getElementById(\"calendrieradminajout_date_fin\").value = '";
            // line 172
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tableau"]) ? $context["tableau"] : $this->getContext($context, "tableau")), "date_fin", array(), "array"), "html", null, true);
            echo "';
      document.getElementById(\"calendrieradminajout_intitule\").value = '";
            // line 173
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tableau"]) ? $context["tableau"] : $this->getContext($context, "tableau")), "intitule", array(), "array"), "html", null, true);
            echo "';
    </script>
  ";
        }
        // line 176
        echo "
";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:AdminCalendrier:ajouter_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  373 => 176,  367 => 173,  363 => 172,  359 => 171,  354 => 169,  343 => 161,  335 => 156,  328 => 152,  322 => 149,  315 => 144,  309 => 140,  305 => 138,  302 => 137,  290 => 133,  284 => 132,  279 => 129,  271 => 126,  264 => 122,  248 => 118,  236 => 110,  223 => 103,  170 => 74,  110 => 41,  96 => 32,  84 => 28,  472 => 149,  467 => 148,  375 => 146,  371 => 130,  360 => 99,  356 => 170,  353 => 99,  350 => 98,  338 => 101,  336 => 98,  331 => 95,  321 => 89,  316 => 87,  307 => 85,  304 => 84,  301 => 83,  297 => 70,  292 => 67,  286 => 65,  283 => 64,  277 => 71,  275 => 64,  270 => 61,  263 => 25,  257 => 121,  253 => 119,  249 => 21,  245 => 20,  233 => 23,  225 => 21,  216 => 19,  206 => 22,  202 => 91,  198 => 20,  185 => 13,  180 => 12,  177 => 11,  165 => 154,  150 => 63,  124 => 49,  113 => 83,  100 => 60,  58 => 30,  251 => 123,  234 => 112,  213 => 104,  195 => 93,  174 => 75,  167 => 77,  146 => 58,  140 => 56,  128 => 113,  104 => 35,  90 => 28,  83 => 24,  52 => 14,  596 => 225,  590 => 224,  585 => 221,  577 => 218,  573 => 216,  569 => 214,  560 => 212,  556 => 211,  553 => 210,  551 => 209,  546 => 207,  543 => 206,  539 => 205,  529 => 197,  525 => 195,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 185,  500 => 184,  497 => 183,  495 => 182,  492 => 181,  490 => 180,  487 => 179,  484 => 178,  482 => 177,  479 => 176,  477 => 175,  474 => 174,  471 => 173,  469 => 172,  466 => 171,  464 => 170,  461 => 169,  458 => 168,  456 => 167,  451 => 164,  445 => 160,  442 => 159,  439 => 158,  437 => 157,  432 => 154,  426 => 150,  423 => 149,  420 => 148,  418 => 147,  413 => 144,  399 => 143,  394 => 140,  378 => 137,  370 => 135,  368 => 129,  365 => 128,  361 => 131,  347 => 125,  345 => 94,  333 => 121,  329 => 94,  323 => 117,  317 => 116,  312 => 114,  306 => 113,  300 => 110,  294 => 68,  285 => 105,  280 => 103,  276 => 102,  267 => 60,  250 => 100,  239 => 95,  229 => 106,  218 => 82,  212 => 95,  210 => 23,  205 => 100,  188 => 89,  184 => 73,  181 => 85,  169 => 66,  160 => 59,  152 => 54,  148 => 53,  139 => 48,  134 => 53,  114 => 42,  107 => 33,  76 => 18,  70 => 33,  273 => 127,  269 => 94,  254 => 92,  246 => 117,  243 => 88,  240 => 19,  238 => 113,  235 => 94,  230 => 111,  227 => 81,  224 => 109,  221 => 20,  219 => 76,  217 => 75,  208 => 94,  204 => 72,  179 => 69,  159 => 61,  143 => 59,  135 => 53,  131 => 44,  108 => 36,  102 => 74,  71 => 21,  67 => 20,  63 => 13,  59 => 14,  47 => 9,  94 => 28,  89 => 20,  85 => 19,  79 => 23,  75 => 24,  68 => 14,  56 => 15,  50 => 10,  38 => 6,  29 => 3,  87 => 29,  72 => 16,  55 => 12,  21 => 2,  26 => 2,  35 => 5,  31 => 3,  41 => 7,  28 => 1,  201 => 92,  196 => 90,  183 => 81,  171 => 6,  166 => 61,  163 => 128,  156 => 66,  151 => 63,  142 => 49,  138 => 116,  136 => 56,  123 => 46,  121 => 46,  115 => 43,  105 => 40,  101 => 30,  91 => 27,  69 => 25,  66 => 32,  62 => 31,  49 => 27,  98 => 33,  93 => 9,  88 => 6,  78 => 22,  46 => 26,  44 => 12,  32 => 4,  27 => 4,  43 => 6,  40 => 11,  25 => 3,  24 => 3,  172 => 106,  158 => 67,  155 => 65,  129 => 119,  119 => 42,  117 => 43,  20 => 1,  22 => 2,  19 => 1,  209 => 82,  203 => 78,  199 => 71,  193 => 85,  189 => 84,  187 => 84,  182 => 70,  176 => 64,  173 => 68,  168 => 72,  164 => 71,  162 => 99,  154 => 58,  149 => 51,  147 => 119,  144 => 118,  141 => 117,  133 => 55,  130 => 52,  125 => 38,  122 => 37,  116 => 41,  112 => 41,  109 => 34,  106 => 33,  103 => 37,  99 => 31,  95 => 28,  92 => 21,  86 => 28,  82 => 22,  80 => 27,  73 => 23,  64 => 17,  60 => 16,  57 => 11,  54 => 29,  51 => 14,  48 => 13,  45 => 8,  42 => 6,  39 => 9,  36 => 6,  33 => 3,  30 => 4,);
    }
}
