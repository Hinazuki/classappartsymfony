<?php

/* PatCompteBundle:AdminAppartement:detail_content.html.twig */
class __TwigTemplate_11b719c9c1c394cb1eae1ae4fdf37ce71fdf44a019e4026ba3d503b4d60a3c08 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "  <div id=\"contentDroit\">
    <div id=\"contenuCentrale\">
      <div id=\"titreAccueil\">
        <h1><span>Visualisation du bien :<br/></span></h1>
      </div>
      ";
        // line 7
        if ((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement"))) {
            // line 8
            echo "
        <div id=\"ZoneContenu\">
          <div id=\"ZoneContenuHaut\"></div><!-- /ZoneContenuHaut -->
          <div id=\"ZoneContenuPixel\">
            <div id=\"ZoneContenuPixel1\">
              <div class=\"TitreBien2\">
                <span class=\"BlocDetail\">RÉF : ";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "reference", array()), "html", null, true);
            echo "</span><br/>
                ";
            // line 15
            echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "type", array())), "html", null, true);
            echo "&nbsp;";
            if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbPieces", array())) {
                echo "T";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbPieces", array()), "html", null, true);
            }
            echo "&nbsp;";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "surfaceSol", array()), "html", null, true);
            echo " m<sup>2</sup>&nbsp;";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "ville", array()), "html", null, true);
            echo "<br/>
              </div><!-- /TitreBien -->




              ";
            // line 21
            if ((isset($context["media"]) ? $context["media"] : $this->getContext($context, "media"))) {
                // line 22
                echo "                <div class=\"Galerie\">
                  <div class=\"pikachoose\">
                    <ul id=\"pikame\" >
                      ";
                // line 25
                $context["k"] = "0";
                // line 26
                echo "                      ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["media"]) ? $context["media"] : $this->getContext($context, "media")));
                foreach ($context['_seq'] as $context["_key"] => $context["medias"]) {
                    // line 27
                    echo "                        <li><a href=\"#\"><img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/photos/biens"), "html", null, true);
                    echo "/";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "reference", array()), "html", null, true);
                    echo "/big/";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["media"]) ? $context["media"] : $this->getContext($context, "media")), (isset($context["k"]) ? $context["k"] : $this->getContext($context, "k")), array(), "array"), "fichier", array()), "html", null, true);
                    echo "\"/></a></li>
                            ";
                    // line 28
                    $context["k"] = ((isset($context["k"]) ? $context["k"] : $this->getContext($context, "k")) + 1);
                    // line 29
                    echo "                          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['medias'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 30
                echo "                    </ul>
                  </div>
                  <div class=\"clear\"></div>
                </div><!-- /Galerie -->
              ";
            }
            // line 35
            echo "

              <div class=\"clear\"></div>
              <div class=\"sepDetail\"></div>
              <div class=\"clear\"></div>
              <div class=\"HeaderLoyer\">
                <div class=\"LoyerBienDes\">
                  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                    <tr>
                      <td width=\"300\">
                        <span class=\"BlocDetail\">Référence : ";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "reference", array()), "html", null, true);
            echo "</span><br/>
                        ";
            // line 46
            if ((!(null === $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "tarif", array())))) {
                // line 47
                echo "                          <span class=\"LoyerBienDes1\">A partir de ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->env->getExtension('my_twig_extension')->getFromTarif((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")))), "html", null, true);
                echo " €/nuit </span>
                        ";
            }
            // line 49
            echo "                      </td>
                      <td>
                        ";
            // line 51
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "isLabeliser", array()) == "1")) {
                echo " <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/images/icone-Label.png"), "html", null, true);
                echo "\" border=\"0\" alt=\"t\"/> ";
            }
            // line 52
            echo "                        ";
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "isReloger", array()) == "1")) {
                echo " <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/images/icone-satisfaitReloge.png"), "html", null, true);
                echo "\" border=\"0\" alt=\"d\"/> ";
            }
            // line 53
            echo "                      </td>
                    </tr>
                  </table>


                </div>
              </div><!-- /HeaderLoyer -->

              <div class=\"DescriptionBien\">";
            // line 61
            echo nl2br(twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "descriptionFr", array()), "html", null, true));
            echo "</div><!-- /DescriptionBien -->
              <div class=\"sepDetail\"></div>



              <div class=\"BlocDetail\">
                Type de biens :  <b>";
            // line 67
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "type", array())), "html", null, true);
            echo "</b>  <br/>
                Surface :  <b>";
            // line 68
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "surfaceSol", array()), "html", null, true);
            echo "m<sup>2</sup></b><br/>
                Nombre de personne maximum : <b>";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbPersonne", array()), "html", null, true);
            echo "</b></br>
                Heure d’arrivée : <b>";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "heureArrivee", array()), "html", null, true);
            echo "</b><br/>
                Heure de sortie : <b>";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "heureDepart", array()), "html", null, true);
            echo "</b> au plus tard<br/>
              </div>
              <div class=\"sepDetail\"></div>


              ";
            // line 76
            if ((((((((($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceAeroport", array()) || $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceAutoroute", array())) || $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesMetro", array())) || $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesBus", array())) || $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesTram", array())) || $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "arretTram", array())) || $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "arretTram2", array())) || $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "arretTram3", array())) || $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceCommerce", array()))) {
                // line 77
                echo "                <div class=\"BlocDetail\">
                  <span class=\"BlocDetailTitre\">Localisation :</span><br/>
                  Distance aéroport :  <b>";
                // line 79
                if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceAeroport", array())) {
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceAeroport", array()), "html", null, true);
                    echo " km ";
                } else {
                    echo " NC ";
                }
                echo "</b><br/>
                  Distance autoroute :  <b>";
                // line 80
                if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceAutoroute", array())) {
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceAutoroute", array()), "html", null, true);
                    echo " km ";
                } else {
                    echo " NC ";
                }
                echo "</b><br/>
                  Distance commerces :  <b>";
                // line 81
                if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceCommerce", array())) {
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "distanceCommerce", array()), "html", null, true);
                    echo " km ";
                } else {
                    echo " NC ";
                }
                echo "</b><br/><br/>
                  Accès tram :  <b>";
                // line 82
                if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesTram", array())) {
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesTram", array()), "html", null, true);
                    echo " min ";
                } else {
                    echo " NC ";
                }
                echo "</b><br/>
                  ";
                // line 83
                if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "arretTram", array())) {
                    echo "Arrêt de tram à proximité : <b>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "arretTram", array()), "html", null, true);
                    echo "</b><br/>";
                }
                // line 84
                echo "                  ";
                if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "arretTram2", array())) {
                    echo "Arrêt de tram à proximité (2) : <b>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "arretTram2", array()), "html", null, true);
                    echo "</b><br/>";
                }
                // line 85
                echo "                  ";
                if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "arretTram3", array())) {
                    echo "Arrêt de tram à proximité (3) : <b>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "arretTram3", array()), "html", null, true);
                    echo "</b><br/>";
                }
                // line 86
                echo "                  Accès bus :  <b>";
                if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesBus", array())) {
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesBus", array()), "html", null, true);
                    echo " min ";
                } else {
                    echo " NC ";
                }
                echo "</b><br/>

                </div>
                <div class=\"sepDetail\"></div>
              ";
            }
            // line 91
            echo "

              <div class=\"BlocDetail\"><span class=\"BlocDetailTitre\">Prestations :</span><br/>
                <div class=\"blocDetailPrestaCol\">
                  <div class=\"BlocDetailCol\">
                    Etage : ";
            // line 96
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "etage", array()) == "0")) {
                echo " RDC /";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbEtage", array()), "html", null, true);
                echo " ";
            } else {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "etage", array()), "html", null, true);
                echo " ";
            }
            echo "<br/>
                    Cave(s) : ";
            // line 97
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "cave", array()) != "")) {
                echo " oui ";
            } else {
                echo " non ";
            }
            echo "<br/>
                    Vide ordure : ";
            // line 98
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "videOrdure", array()) != "")) {
                echo " oui ";
            } else {
                echo " non ";
            }
            echo "<br/>
                    Stationnement : ";
            // line 99
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "stationnement", array())), "html", null, true);
            echo "  <br/>
                    Digicode :   ";
            // line 100
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "digicode", array()) != "")) {
                echo " oui ";
            } else {
                echo " non ";
            }
            echo "<br/>
                    Climatisation :   ";
            // line 101
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "climatisation", array()) != "")) {
                echo " oui ";
            } else {
                echo " non ";
            }
            echo "<br/>
                    Animaux admis :   ";
            // line 102
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "animal", array()) != "")) {
                echo " oui ";
            } else {
                echo " non ";
            }
            echo "<br/>
                    Type chauffage : ";
            // line 103
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "typeChauffage", array())), "html", null, true);
            echo "  <br/>
                    Mode de chauffage : ";
            // line 104
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "modeChauffage", array())), "html", null, true);
            echo "<br/>
                    Ascenseur :   ";
            // line 105
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "ascenseur", array()) != "")) {
                echo " oui ";
            } else {
                echo " non ";
            }
            echo "<br/>
                  </div>
                  <div class=\"BlocDetailCol\">
                    Grenier :   ";
            // line 108
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "grenier", array()) != "")) {
                echo " oui ";
            } else {
                echo " non ";
            }
            echo "<br/>
                    Jardin :   ";
            // line 109
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "jardin", array()) != "")) {
                echo " oui ";
            } else {
                echo " non ";
            }
            echo "<br/>
                    Interphone :   ";
            // line 110
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "interphone", array()) != "")) {
                echo " oui ";
            } else {
                echo " non ";
            }
            echo "<br/>
                    Accès handicapés :   ";
            // line 111
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "accesHandicapes", array()) != "")) {
                echo " oui ";
            } else {
                echo " non ";
            }
            echo "<br/>
                    Gardien :   ";
            // line 112
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "gardien", array()) != "")) {
                echo " oui ";
            } else {
                echo " non ";
            }
            echo "<br/>
                    Fumeur :   ";
            // line 113
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "fumeur", array()) != "")) {
                echo " oui ";
            } else {
                echo " non ";
            }
            echo "<br/>
                    Nombre de lits simples : ";
            // line 114
            echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbLitSimple", array())), "html", null, true);
            echo "<br>
                    Nombre de lits doubles : ";
            // line 115
            echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbLitDouble", array())), "html", null, true);
            echo "<br>
                    Nombre de canapés convertibles : ";
            // line 116
            echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbCanapeConvertible", array())), "html", null, true);
            echo "<br>
                    Nombre de double lits simples / lit double : ";
            // line 117
            echo twig_escape_filter($this->env, $this->env->getExtension('pattwigext')->formatValFront($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "nbLitSimpleDouble", array())), "html", null, true);
            echo "<br>
                  </div>
                </div>

              </div>
              <div class=\"sepDetail\"></div>


              <div class=\"BlocDetail\"><br/>
                <div class=\"blocDetailNoteCol\">
                  <div class=\"BlocDetailCol\">



                    Clair :\t&nbsp;&nbsp;
                    ";
            // line 132
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(0, 5));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 133
                echo "                      ";
                if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "clair", array()) <= (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")))) {
                    // line 134
                    echo "                        <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/images/etoile_vide.png"), "html", null, true);
                    echo "\" alt=\"\"/>
                      ";
                } else {
                    // line 136
                    echo "                        <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/images/etoile_pleine.png"), "html", null, true);
                    echo "\" alt=\"*\"/>
                      ";
                }
                // line 138
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 139
            echo "


                    <br/>
                    Calme :
                    ";
            // line 144
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(0, 5));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 145
                echo "                      ";
                if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "calme", array()) <= (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")))) {
                    // line 146
                    echo "                        <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/images/etoile_vide.png"), "html", null, true);
                    echo "\" alt=\"\"/>
                      ";
                } else {
                    // line 148
                    echo "                        <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/images/etoile_pleine.png"), "html", null, true);
                    echo "\" alt=\"*\"/>
                      ";
                }
                // line 150
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 151
            echo "
                  </div>


                  <div class=\"BlocDetailCol\">
                    Standing :
                    ";
            // line 157
            if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "standingImmeuble", array())) {
                // line 158
                echo "                      ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "standingImmeuble", array())), "html", null, true);
                echo "
                    ";
            }
            // line 160
            echo "                    <br/>
                    État intérieur :
                    ";
            // line 162
            if ($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "etatInterieur", array())) {
                // line 163
                echo "                      ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "etatInterieur", array())), "html", null, true);
                echo "
                    ";
            }
            // line 165
            echo "                  </div>
                </div><br/>
              </div>
              <div class=\"sepDetail\"></div>


              ";
            // line 171
            if ((isset($context["pieces"]) ? $context["pieces"] : $this->getContext($context, "pieces"))) {
                // line 172
                echo "                <div class=\"BlocDetail\"><span class=\"BlocDetailTitre\">Pièces :</span><br/><br/>
                  ";
                // line 173
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["pieces"]) ? $context["pieces"] : $this->getContext($context, "pieces")));
                foreach ($context['_seq'] as $context["_key"] => $context["piece"]) {
                    // line 174
                    echo "                    <span class=\"BlocDetailTitre2\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["piece"]) ? $context["piece"] : $this->getContext($context, "piece")), "typepiece", array()), "type", array()), "html", null, true);
                    echo "</span> :
                    ";
                    // line 175
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["piece"]) ? $context["piece"] : $this->getContext($context, "piece")), "equipement", array()));
                    $context['loop'] = array(
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    );
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["equipement"]) {
                        // line 176
                        echo "                      ";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["equipement"]) ? $context["equipement"] : $this->getContext($context, "equipement")), "intitule", array()), "html", null, true);
                        if ((!$this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "last", array()))) {
                            echo ", ";
                        }
                        // line 177
                        echo "                    ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['equipement'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 178
                    echo "                    <br/>
                  ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['piece'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 180
                echo "                </div>
                <div class=\"sepDetail\"></div>
              ";
            }
            // line 183
            echo "
              ";
            // line 184
            if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "consommationLettre", array()) || $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "gazSerreLettre", array()))) {
                // line 185
                echo "                <div class=\"BlocDetail\"><span class=\"BlocDetailTitre\">Diagnostic Performance Energie :</span><br/>
                  ";
                // line 186
                if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "consommationLettre", array()) || $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "consommationChiffre", array()))) {
                    // line 187
                    echo "                    Consommation énergétique : Classe ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "consommationLettre", array()), "html", null, true);
                    echo " : ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "consommationChiffre", array()), "html", null, true);
                    echo " kWh/m2/an<br/>
                  ";
                }
                // line 189
                echo "                  ";
                if (($this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "gazSerreLettre", array()) || $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "gazSerreChiffre", array()))) {
                    // line 190
                    echo "                    Gaz à effet de serre : Classe ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "gazSerreLettre", array()), "html", null, true);
                    echo " : ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["appartement"]) ? $context["appartement"] : $this->getContext($context, "appartement")), "gazSerreChiffre", array()), "html", null, true);
                    echo " kg/m2/an <br/>
                  ";
                }
                // line 192
                echo "                </div>
                <div class=\"sepDetail\"></div>
              ";
            }
            // line 195
            echo "

              <div class=\"BlocDetail\"><span class=\"BlocDetailTitre\">Disponibilités :</span><br/>

                ";
            // line 199
            if ((isset($context["calendrier"]) ? $context["calendrier"] : $this->getContext($context, "calendrier"))) {
                // line 200
                echo "                  <br/>

                  <div id=\"slider\">
                    <ul>
                      <div id=\"DetailCalendrier\">
                        ";
                // line 206
                echo "                          ";
                echo (isset($context["calendrier"]) ? $context["calendrier"] : $this->getContext($context, "calendrier"));
                echo "
                        ";
                // line 208
                echo "                      </div>
                    </ul>
                  </div>

                  <table border=\"0\" cellpadding=\"5\" cellspacing=\"0\">
                    <tr>
                      <td><div style=\"width:20px;height:20px;background-color:#DD2525;\"></div></td>
                      <td>Réservé</td>
                      <td width=\"30\"></td>
                      <td><div style=\"width:20px;height:20px;background-color:#5BD835;\"></div></td>
                      <td>Disponible</td>
                    </tr>
                  </table>

                ";
            }
            // line 223
            echo "
                <br/><br/>
                <span class=\"BlocDetailTexte2\">

                  <i>* Le prix indiqué comprend le loyer, les charges, les consommations (eau, électricité, gaz éventuel), les honoraires de Class Appart ®. <br/>

                    La connexion internet peuvent faire l’objet d’une facturation supplémentaire. Reportez-vous à la fiche de l’appartement<br/>
                    ** Si vous choisissez l’assurance incluse de classAppart®, ce montant constitue le montant de la franchise qui devra être laissé en caution. <br/>
                  </i>
                </span>
              </div>

            </div><!-- /ZoneContenuPixel1 -->
          </div><!-- /ZoneContenuPixel -->
          <div id=\"ZoneContenuBas\"></div><!-- /ZoneContenuBas -->

        </div><!-- /ZoneContenu -->
      ";
        }
        // line 241
        echo "    </div><!-- /contenuCentrale -->






  </div><!-- /contentDroit -->

";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:AdminAppartement:detail_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  654 => 223,  637 => 208,  632 => 206,  625 => 200,  623 => 199,  617 => 195,  612 => 192,  604 => 190,  593 => 187,  591 => 186,  586 => 184,  583 => 183,  578 => 180,  571 => 178,  557 => 177,  534 => 175,  522 => 172,  520 => 171,  504 => 162,  494 => 158,  463 => 145,  446 => 138,  440 => 136,  434 => 134,  431 => 133,  427 => 132,  405 => 116,  401 => 115,  397 => 114,  389 => 113,  381 => 112,  357 => 109,  349 => 108,  339 => 105,  303 => 99,  295 => 98,  287 => 97,  268 => 91,  74 => 26,  470 => 398,  452 => 139,  444 => 387,  435 => 405,  430 => 397,  414 => 389,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 887,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 864,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 825,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 808,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 763,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 732,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 697,  1387 => 694,  1378 => 688,  1374 => 687,  1368 => 684,  1359 => 678,  1355 => 677,  1349 => 674,  1340 => 668,  1336 => 667,  1330 => 664,  1321 => 658,  1317 => 657,  1311 => 654,  1302 => 648,  1298 => 647,  1292 => 644,  1283 => 638,  1279 => 637,  1273 => 634,  1264 => 628,  1260 => 627,  1254 => 624,  1245 => 618,  1241 => 617,  1235 => 614,  1226 => 608,  1222 => 607,  1216 => 604,  1207 => 598,  1203 => 597,  1197 => 594,  1188 => 588,  1184 => 587,  1178 => 584,  1169 => 578,  1165 => 577,  1159 => 574,  1150 => 568,  1146 => 567,  1140 => 564,  1123 => 550,  1119 => 549,  1113 => 546,  1104 => 540,  1100 => 539,  1094 => 536,  1085 => 530,  1081 => 529,  1075 => 526,  1066 => 520,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 456,  933 => 450,  929 => 449,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 426,  874 => 420,  870 => 419,  866 => 418,  860 => 415,  851 => 409,  847 => 408,  841 => 405,  832 => 399,  828 => 398,  822 => 395,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 365,  756 => 359,  752 => 358,  746 => 355,  737 => 349,  733 => 348,  727 => 345,  712 => 333,  708 => 332,  702 => 329,  693 => 323,  689 => 322,  683 => 319,  674 => 241,  670 => 312,  664 => 309,  649 => 297,  645 => 296,  639 => 293,  630 => 287,  626 => 286,  620 => 283,  611 => 277,  607 => 276,  601 => 189,  592 => 267,  588 => 185,  582 => 263,  563 => 253,  554 => 247,  550 => 246,  544 => 243,  535 => 237,  531 => 236,  516 => 227,  512 => 165,  506 => 163,  493 => 216,  478 => 150,  468 => 203,  459 => 144,  455 => 196,  449 => 193,  436 => 183,  428 => 181,  422 => 395,  409 => 117,  403 => 168,  390 => 161,  384 => 158,  351 => 137,  337 => 132,  311 => 118,  296 => 109,  256 => 84,  241 => 79,  215 => 69,  207 => 66,  192 => 61,  186 => 58,  175 => 70,  153 => 41,  118 => 22,  61 => 1,  34 => 4,  65 => 21,  77 => 24,  37 => 8,  190 => 60,  161 => 47,  137 => 52,  126 => 24,  261 => 86,  255 => 135,  247 => 130,  242 => 127,  214 => 110,  211 => 109,  191 => 98,  157 => 46,  145 => 60,  127 => 49,  373 => 111,  367 => 173,  363 => 172,  359 => 171,  354 => 169,  343 => 135,  335 => 104,  328 => 152,  322 => 149,  315 => 101,  309 => 140,  305 => 115,  302 => 137,  290 => 133,  284 => 132,  279 => 129,  271 => 126,  264 => 122,  248 => 118,  236 => 77,  223 => 82,  170 => 74,  110 => 20,  96 => 30,  84 => 9,  472 => 148,  467 => 148,  375 => 152,  371 => 151,  360 => 99,  356 => 170,  353 => 99,  350 => 98,  338 => 101,  336 => 98,  331 => 103,  321 => 89,  316 => 87,  307 => 100,  304 => 84,  301 => 83,  297 => 70,  292 => 108,  286 => 65,  283 => 64,  277 => 71,  275 => 96,  270 => 61,  263 => 25,  257 => 121,  253 => 86,  249 => 21,  245 => 20,  233 => 83,  225 => 74,  216 => 111,  206 => 22,  202 => 91,  198 => 20,  185 => 13,  180 => 12,  177 => 11,  165 => 154,  150 => 63,  124 => 49,  113 => 83,  100 => 30,  58 => 17,  251 => 81,  234 => 112,  213 => 81,  195 => 93,  174 => 51,  167 => 68,  146 => 58,  140 => 40,  128 => 113,  104 => 31,  90 => 29,  83 => 24,  52 => 14,  596 => 225,  590 => 224,  585 => 221,  577 => 218,  573 => 257,  569 => 256,  560 => 212,  556 => 211,  553 => 210,  551 => 176,  546 => 207,  543 => 206,  539 => 205,  529 => 174,  525 => 173,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 185,  500 => 160,  497 => 217,  495 => 182,  492 => 157,  490 => 180,  487 => 213,  484 => 151,  482 => 177,  479 => 176,  477 => 401,  474 => 206,  471 => 173,  469 => 172,  466 => 146,  464 => 397,  461 => 169,  458 => 396,  456 => 167,  451 => 164,  445 => 160,  442 => 159,  439 => 158,  437 => 157,  432 => 398,  426 => 396,  423 => 149,  420 => 148,  418 => 147,  413 => 172,  399 => 143,  394 => 162,  378 => 137,  370 => 135,  368 => 129,  365 => 110,  361 => 131,  347 => 136,  345 => 94,  333 => 131,  329 => 130,  323 => 102,  317 => 116,  312 => 114,  306 => 113,  300 => 110,  294 => 68,  285 => 105,  280 => 99,  276 => 98,  267 => 60,  250 => 100,  239 => 84,  229 => 116,  218 => 82,  212 => 68,  210 => 67,  205 => 100,  188 => 59,  184 => 73,  181 => 56,  169 => 48,  160 => 59,  152 => 54,  148 => 38,  139 => 48,  134 => 54,  114 => 21,  107 => 33,  76 => 23,  70 => 23,  273 => 127,  269 => 94,  254 => 92,  246 => 85,  243 => 88,  240 => 19,  238 => 113,  235 => 94,  230 => 111,  227 => 81,  224 => 109,  221 => 72,  219 => 112,  217 => 70,  208 => 108,  204 => 72,  179 => 71,  159 => 61,  143 => 59,  135 => 38,  131 => 51,  108 => 32,  102 => 74,  71 => 21,  67 => 22,  63 => 16,  59 => 14,  47 => 15,  94 => 30,  89 => 11,  85 => 19,  79 => 27,  75 => 20,  68 => 21,  56 => 15,  50 => 10,  38 => 6,  29 => 3,  87 => 31,  72 => 25,  55 => 15,  21 => 2,  26 => 2,  35 => 8,  31 => 3,  41 => 7,  28 => 2,  201 => 92,  196 => 63,  183 => 57,  171 => 69,  166 => 61,  163 => 67,  156 => 66,  151 => 44,  142 => 41,  138 => 56,  136 => 28,  123 => 46,  121 => 47,  115 => 45,  105 => 40,  101 => 30,  91 => 27,  69 => 18,  66 => 17,  62 => 18,  49 => 11,  98 => 28,  93 => 9,  88 => 28,  78 => 22,  46 => 10,  44 => 12,  32 => 6,  27 => 4,  43 => 14,  40 => 6,  25 => 3,  24 => 1,  172 => 106,  158 => 43,  155 => 42,  129 => 119,  119 => 46,  117 => 43,  20 => 1,  22 => 1120,  19 => 1,  209 => 82,  203 => 80,  199 => 71,  193 => 79,  189 => 77,  187 => 76,  182 => 70,  176 => 64,  173 => 68,  168 => 72,  164 => 46,  162 => 99,  154 => 61,  149 => 51,  147 => 119,  144 => 53,  141 => 117,  133 => 55,  130 => 25,  125 => 38,  122 => 23,  116 => 34,  112 => 33,  109 => 34,  106 => 18,  103 => 35,  99 => 14,  95 => 33,  92 => 28,  86 => 27,  82 => 8,  80 => 24,  73 => 24,  64 => 2,  60 => 15,  57 => 16,  54 => 14,  51 => 13,  48 => 9,  45 => 10,  42 => 13,  39 => 1135,  36 => 9,  33 => 7,  30 => 3,);
    }
}
