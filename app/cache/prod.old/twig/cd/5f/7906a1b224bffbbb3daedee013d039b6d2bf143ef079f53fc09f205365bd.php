<?php

/* TwigBundle:Exception:error.html.twig */
class __TwigTemplate_cd5f7906a1b224bffbbb3daedee013d039b6d2bf143ef079f53fc09f205365bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"fr\" lang=\"fr\" dir=\"ltr\">
    <head>
        <title></title>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />   
        <meta name=\"keywords\" content=\"\" />
        <meta name=\"description\" content=\"\" />

        
    <link href=\"/bundles/patcompte/css/front.css\" type=\"text/css\" rel=\"stylesheet\" media=\"all\" />
                    <link href=\"/bundles/patcompte/css/main.css\" type=\"text/css\" rel=\"stylesheet\" media=\"all\" />
                <link href=\"/bundles/patcompte/js/jquery/themes/base/jquery.ui.all.css\" rel=\"stylesheet\" />
                            <link href=\"/css/143482a.css\" type=\"text/css\" rel=\"stylesheet\" media=\"screen\" />
                <link href=\"/bundles/patcompte/css/bootstrap-override.css\" type=\"text/css\" rel=\"stylesheet\" media=\"all\" />
                
                    

    <link href=\"/bundles/patfront/css/front.css\" type=\"text/css\" rel=\"stylesheet\" media=\"all\" />

                
        <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"/bundles/patcompte/images/favicon.ico\" />
        
        <script type=\"text/javascript\" src=\"/bundles/patcompte/js/jquery-1.8.3.min.js\"></script>
        <script type=\"text/javascript\" src=\"/bundles/patcompte/js/functions.js\"></script>            
        <script type=\"text/javascript\" src=\"/bundles/patcompte/js/jquery/ui/jquery.ui.core.js\"></script>
        <script type=\"text/javascript\" src=\"/bundles/patcompte/js/jquery/ui/jquery.ui.widget.js\"></script>
        <script type=\"text/javascript\" src=\"/bundles/patcompte/js/jquery/ui/jquery.ui.datepicker.js\"></script>
        <script type=\"text/javascript\" src=\"/bundles/patcompte/js/jquery.placeholder.js\"></script>            
        <script type=\"text/javascript\">
            \$(document).ready(function(){
                \$('[placeholder]').placeholder();
            });
            \$('table.formulaire ul').next(\":input\").css({'background-color' : '#FFD3D3', 'border' : 'solid 1px red'});
        </script>

    </head>
        
    <body>


    <div class=\"navbar navbar-inverse navbar-fixed-top\">
      <div class=\"navbar-inner\">


                                <div class=\"navbar navbar-inverse navbar-fixed-top\">
                    <div class=\"navbar-inner\">

                                                    <div id=\"logo\"><a href=\"/\"></a></div>

                                <div class=\"posMenu\">

        <ul class=\"nav \">
                                            <li class=\"dropdown\">
                    <a href=\"#\"  class=\"dropdown-toggle\" data-toggle=\"dropdown\">Nos services</a>
                        <!-- Bloc sous menu -->

                      <ul class=\"dropdown-menu\">
                                                            <li>
                                    <a class=\"msubm\" href=\"/fr/page/historique/\">Historique</a>
                                </li>
                                                            <li>
                                    <a class=\"msubm\" href=\"/fr/page/presentation/\">Présentation</a>
                                </li>
                                                </ul>
                </li>
                                                                <li><a href=\"/fr/page/proprietaires9/\">Louer un appartement</a>
                                                                <li><a href=\"/fr/page/locataires64/\">Locataires</a>
                                                                <li><a href=\"/fr/page/actualites23795841/\">Propriétaires</a>
                                                                <li><a href=\"/fr/page/condition-gn-nn-rales-d-utilisation377/\">Présentation</a>
                                    </ul>
        
        <ul class=\"nav pull-right\">
                            <li class=\"dropdown\" >
                    <button href=\"/fr/mon-compte/login\" class=\"dropdown-toggle btn btn-inverse\" data-toggle=\"dropdown\">Mon compte</button>
                    <ul class=\"dropdown-menu\">
                        <li><a href=\"/fr/mon-compte/login\">Connexion</a></li>
                        <li><a href=\"/fr/mon-compte/resetting/request\">Mot de passe oublié ?</a></li>
                    </ul>
                </li>
                    </ul>

    </div><!-- /menu -->

                                                                        </div> <!-- /header -->
                </div><!-- /headermenu -->
                


      </div>

    </div>


    <div class=\"container\">                

        ";
        // line 96
        $this->displayBlock('body', $context, $blocks);
        // line 111
        echo "                   
                    
                    
      <footer>
        <div class=\"row-fluid\" id=\"footer\">
          <div class=\"span6\">
            <h1>class appart</h1>
            <h2>Solutions de location d’appartements meublés<br />en courte ou moyenne durée à Montpellier </h2>
            <p class=\"legend\"><a href=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_front_contenu_page", array("lg" => "fr", "url_page" => "mentions-legales")), "html", null, true);
        echo "\">Mentions légales</a> | <a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_front_contenu_page", array("lg" => "fr", "url_page" => "conditions-generale-vente")), "html", null, true);
        echo "\">Conditions générales</a> | CGL | Copyright © ClassAppart</p>
          </div>
          <div class=\"span6 pull-right footer-right\">
            <a href=\"\">Mon besoin</a><br />
            <a href=\"\">Espace locataire</a><br />
            <a href=\"\">Espace propriétaire</a><br />
            <a href=\"\">Présentation de l’agence</a><br />
            <a href=\"\">Contactez-nous</a><br />

          </div>
      </div>
      </footer>

    </div>
        
            <script type=\"text/javascript\" src=\"/js/8b84ae1.js\"></script>            

    </body>
</html>
";
    }

    // line 96
    public function block_body($context, array $blocks = array())
    {
        // line 97
        echo "                <div id=\"content\" class=\"row\">
                    <div id=\"classappart\" class=\"row\">
                        <img border=\"0\" align=\"left\" style=\"padding-right: 25px\" src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/patcompte/images/classappart.jpg"), "html", null, true);
        echo "\">
                        <h1>class appart</h1>
                        <h2>Location d'appartements meublés courte et moyenne durée</h2>
                    </div>
                    <div id=\"contentBis\" class=\"row\">
                        
                        <h1>Oops! An Error Occurred</h1>
    <h2>The server returned a \"";
        // line 106
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\".</h2>

                    </div><!-- /contentBis -->
                </div>
        ";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 106,  158 => 97,  155 => 96,  129 => 119,  119 => 111,  117 => 96,  20 => 1,  22 => 2,  19 => 1,  209 => 82,  203 => 78,  199 => 76,  193 => 73,  189 => 71,  187 => 70,  182 => 68,  176 => 64,  173 => 63,  168 => 62,  164 => 60,  162 => 99,  154 => 54,  149 => 51,  147 => 50,  144 => 49,  141 => 48,  133 => 42,  130 => 41,  125 => 38,  122 => 37,  116 => 36,  112 => 35,  109 => 34,  106 => 33,  103 => 32,  99 => 30,  95 => 28,  92 => 27,  86 => 24,  82 => 22,  80 => 21,  73 => 19,  64 => 15,  60 => 13,  57 => 12,  54 => 11,  51 => 10,  48 => 9,  45 => 8,  42 => 7,  39 => 6,  36 => 5,  33 => 4,  30 => 3,);
    }
}
