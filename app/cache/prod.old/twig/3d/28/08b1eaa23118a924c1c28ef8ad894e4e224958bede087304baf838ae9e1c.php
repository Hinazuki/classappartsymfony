<?php

/* PatFrontBundle:Reservation:mail_refus_CB.html.twig */
class __TwigTemplate_3d2808b1eaa23118a924c1c28ef8ad894e4e224958bede087304baf838ae9e1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::email_layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'subtitle' => array($this, 'block_subtitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::email_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
    }

    // line 4
    public function block_subtitle($context, array $blocks = array())
    {
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "  <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Bonjour"), "html", null, true);
        echo ",</p>

  <p>Nous vous informons que <strong>votre paiement par carte bancaire a échoué</strong>, mais aucun montant n’a été prélevé sur votre carte.</p>

  <p>Nous vous conseillons d’utiliser une autre carte bancaire ou de changer de mode de paiement, par chèque ou par virement.</p>

  <p>N’hésitez pas à <a href=\"";
        // line 13
        echo twig_escape_filter($this->env, ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "getSchemeAndHttpHost", array(), "method") . $this->env->getExtension('routing')->getPath("pat_front_formulaire_contact")), "html", null, true);
        echo "\">nous contacter</a> si vous rencontrez à nouveau des difficultés.</p>


  <h3 class=\"titleResa\">Votre demande de réservation</h3>
  <hr>

  <p>Réservation <a href=\"";
        // line 19
        echo twig_escape_filter($this->env, ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "getSchemeAndHttpHost", array(), "method") . $this->env->getExtension('routing')->getPath("pat_reservation_show", array("id_reservation" => $this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "id", array())))), "html", null, true);
        echo "\" class=\"colorLink\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "Reference", array()), "html", null, true);
        echo "</a></p>

  <p>
    Date d’arrivée : ";
        // line 22
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "dateDebut", array()), "d/m/Y"), "html", null, true);
        echo "<br />
    Date de départ : ";
        // line 23
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "dateFin", array()), "d/m/Y"), "html", null, true);
        echo "<br />
    Nombre de personnes : ";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "nbPersonne", array()), "html", null, true);
        echo "<br />
    Nombre d'adultes : ";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "nbAdulte", array()), "html", null, true);
        echo "<br />
    Nombre d'enfants (-18 ans) : ";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "nbEnfant", array()), "html", null, true);
        echo "<br />
    Référence : ";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "appartement", array()), "reference", array()), "html", null, true);
        echo "<br />
    Adresse : ";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "appartement", array()), "adresse", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "appartement", array()), "adresse2", array()), "html", null, true);
        echo "<br />
    Ville : ";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "appartement", array()), "ville", array()), "html", null, true);
        echo "<br />
    Surface : ";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "appartement", array()), "surfaceSol", array()), "html", null, true);
        echo "m2<br />
  </p>

  <h3>Votre séjour :</h3>
  <p>
    <strong>Prix du séjour : ";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "tarif", array()), "html", null, true);
        echo " €</strong><br/>
    Dépôt de garantie : ";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "depot", array()), "html", null, true);
        echo " €<br/>
    ";
        // line 37
        if (($this->env->getExtension('my_twig_extension')->moins48h($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "dateDebut", array())) == false)) {
            echo "Acompte";
        } else {
            echo "Total";
        }
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "arrhes", array()), "html", null, true);
        echo " €
  </p>
  ";
        // line 78
        echo "  <h3>Options :</h3>
  <p>
    ";
        // line 80
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "options", array()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
            // line 81
            echo "      ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["option"]) ? $context["option"] : $this->getContext($context, "option")), "quantity", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["option"]) ? $context["option"] : $this->getContext($context, "option")), "name", array()), "html", null, true);
            if ((!(null === $this->getAttribute((isset($context["option"]) ? $context["option"] : $this->getContext($context, "option")), "amount", array())))) {
                echo " : ";
                echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["option"]) ? $context["option"] : $this->getContext($context, "option")), "amount", array()) * $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute((isset($context["option"]) ? $context["option"] : $this->getContext($context, "option")), "quantity", array()))), "html", null, true);
                echo " €";
            }
            echo "<br/>
    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 83
            echo "      Aucune option
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 85
        echo "  </p>


  <h3>Locataire :</h3>
  Identifiant : ";
        // line 89
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "utilisateur", array()), "username", array()), "html", null, true);
        echo "<br/>
  Nom : ";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "utilisateur", array()), "nom", array()), "html", null, true);
        echo "<br/>
  Prénom : ";
        // line 91
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "utilisateur", array()), "prenom", array()), "html", null, true);
        echo "<br/>
  Email : ";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "utilisateur", array()), "email", array()), "html", null, true);
        echo "<br/>
  Téléphone : ";
        // line 93
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "utilisateur", array()), "telephone", array()), "html", null, true);
        echo "<br/>
  Adresse : ";
        // line 94
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "utilisateur", array()), "adresseFact", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "utilisateur", array()), "adresse2Fact", array()), "html", null, true);
        echo "<br/>
  Code Postal : ";
        // line 95
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "utilisateur", array()), "codePostalFact", array()), "html", null, true);
        echo "<br/>
  Ville : ";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "utilisateur", array()), "villeFact", array()), "html", null, true);
        echo "<br/>
  Pays : ";
        // line 97
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reservation"]) ? $context["reservation"] : $this->getContext($context, "reservation")), "utilisateur", array()), "paysFact", array()), "html", null, true);
        echo "<br/>

  <hr>

  <p>Vous pouvez vous connecter dans la partie <a href=\"";
        // line 101
        echo twig_escape_filter($this->env, ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "getSchemeAndHttpHost", array(), "method") . $this->env->getExtension('routing')->getPath("fos_user_security_login")), "html", null, true);
        echo "\">Mon compte</a> sur class-appart.com pour consulter le détail de vos réservations.</p>
  <p>Si vous avez besoin d’aide supplémentaire, nous vous invitons à <a href=\"";
        // line 102
        echo twig_escape_filter($this->env, ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "getSchemeAndHttpHost", array(), "method") . $this->env->getExtension('routing')->getPath("pat_front_formulaire_contact")), "html", null, true);
        echo "\">nous contacter</a>.</p>
  <p>À bientôt sur class-appart.com.</p>

  <hr>

  <p class=\"legend\">
    Ce message vous a été envoyé d’une adresse qui ne peut recevoir d’e-mail, merci de ne pas y répondre.
  </p>

";
    }

    public function getTemplateName()
    {
        return "PatFrontBundle:Reservation:mail_refus_CB.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  387 => 124,  364 => 64,  352 => 174,  991 => 405,  908 => 300,  906 => 299,  901 => 296,  887 => 280,  880 => 257,  862 => 243,  856 => 240,  830 => 224,  820 => 218,  806 => 217,  799 => 214,  789 => 210,  785 => 209,  779 => 207,  743 => 197,  720 => 192,  714 => 191,  697 => 190,  666 => 178,  660 => 177,  647 => 175,  638 => 174,  517 => 158,  850 => 388,  843 => 383,  836 => 379,  825 => 221,  823 => 373,  817 => 370,  808 => 365,  802 => 363,  777 => 350,  768 => 346,  757 => 204,  729 => 329,  726 => 328,  719 => 325,  713 => 321,  707 => 318,  704 => 317,  691 => 312,  684 => 309,  678 => 305,  672 => 302,  669 => 301,  659 => 297,  656 => 296,  642 => 288,  610 => 270,  575 => 257,  527 => 231,  511 => 225,  491 => 215,  476 => 209,  421 => 210,  415 => 207,  383 => 122,  736 => 438,  732 => 437,  685 => 186,  648 => 380,  568 => 253,  528 => 314,  524 => 313,  460 => 149,  425 => 211,  374 => 214,  324 => 123,  661 => 417,  636 => 374,  629 => 173,  618 => 365,  589 => 166,  587 => 382,  559 => 364,  547 => 237,  537 => 232,  530 => 345,  510 => 302,  453 => 256,  392 => 226,  369 => 208,  1843 => 1037,  1834 => 1030,  1747 => 947,  1742 => 944,  1728 => 936,  1718 => 929,  1706 => 920,  1694 => 915,  1692 => 914,  1683 => 908,  1677 => 905,  1668 => 899,  1660 => 894,  1656 => 893,  1652 => 892,  1645 => 888,  1641 => 887,  1630 => 882,  1626 => 881,  1622 => 880,  1614 => 874,  1601 => 832,  1595 => 829,  1586 => 823,  1582 => 822,  1576 => 819,  1567 => 813,  1563 => 812,  1557 => 809,  1547 => 802,  1543 => 801,  1537 => 798,  1528 => 792,  1518 => 788,  1500 => 773,  1496 => 772,  1492 => 771,  1488 => 770,  1482 => 767,  1473 => 761,  1469 => 760,  1465 => 759,  1461 => 758,  1448 => 750,  1438 => 748,  1436 => 747,  1432 => 746,  1428 => 745,  1422 => 742,  1413 => 736,  1409 => 735,  1403 => 732,  1389 => 724,  1370 => 711,  1364 => 708,  1351 => 701,  1345 => 698,  1332 => 691,  1326 => 688,  1313 => 681,  1307 => 678,  1294 => 671,  1288 => 668,  1275 => 661,  1269 => 658,  1256 => 651,  1250 => 648,  1237 => 641,  1231 => 638,  1218 => 631,  1212 => 628,  1199 => 621,  1193 => 618,  1180 => 611,  1155 => 598,  1142 => 591,  1136 => 588,  1127 => 582,  1117 => 578,  1108 => 572,  1098 => 568,  1079 => 558,  1071 => 552,  1067 => 550,  1065 => 549,  1054 => 541,  1050 => 540,  1044 => 537,  1035 => 531,  1031 => 530,  1025 => 527,  1016 => 521,  1006 => 517,  997 => 511,  993 => 510,  987 => 507,  978 => 501,  974 => 500,  959 => 491,  955 => 490,  949 => 487,  940 => 481,  936 => 480,  930 => 477,  921 => 471,  911 => 467,  902 => 461,  898 => 460,  892 => 457,  883 => 258,  879 => 450,  864 => 441,  854 => 437,  845 => 431,  835 => 226,  826 => 421,  816 => 417,  805 => 411,  801 => 410,  797 => 213,  791 => 406,  778 => 399,  772 => 348,  763 => 343,  759 => 389,  753 => 386,  744 => 380,  740 => 379,  721 => 369,  715 => 366,  687 => 350,  677 => 183,  668 => 340,  643 => 324,  624 => 314,  614 => 393,  605 => 304,  595 => 266,  538 => 267,  519 => 257,  485 => 212,  481 => 154,  447 => 218,  262 => 126,  281 => 136,  197 => 95,  419 => 185,  308 => 130,  260 => 55,  228 => 71,  355 => 128,  346 => 127,  288 => 113,  692 => 188,  603 => 170,  561 => 278,  515 => 319,  475 => 234,  379 => 72,  348 => 111,  298 => 114,  1174 => 608,  1167 => 718,  1161 => 601,  1154 => 710,  1149 => 707,  1141 => 704,  1134 => 702,  1131 => 701,  1129 => 700,  1114 => 695,  1111 => 694,  1109 => 693,  1105 => 692,  1096 => 689,  1092 => 688,  1089 => 562,  1072 => 675,  1064 => 670,  1042 => 651,  1034 => 645,  1026 => 642,  1022 => 640,  1019 => 639,  1017 => 638,  1012 => 520,  1008 => 635,  1004 => 633,  1002 => 632,  996 => 629,  988 => 626,  984 => 624,  979 => 623,  977 => 622,  968 => 497,  962 => 613,  951 => 605,  917 => 470,  915 => 585,  912 => 328,  905 => 580,  897 => 575,  893 => 574,  881 => 568,  873 => 447,  871 => 249,  863 => 555,  853 => 496,  842 => 231,  831 => 376,  812 => 464,  800 => 362,  798 => 455,  787 => 447,  754 => 203,  747 => 446,  739 => 333,  731 => 410,  725 => 370,  718 => 404,  696 => 356,  681 => 401,  675 => 182,  665 => 369,  663 => 298,  651 => 359,  628 => 344,  606 => 269,  602 => 268,  600 => 356,  579 => 258,  572 => 311,  555 => 303,  542 => 268,  532 => 264,  513 => 254,  501 => 329,  499 => 156,  473 => 153,  441 => 146,  438 => 257,  416 => 139,  330 => 139,  289 => 108,  633 => 320,  627 => 398,  621 => 289,  619 => 288,  594 => 353,  576 => 344,  570 => 254,  562 => 163,  558 => 335,  552 => 251,  540 => 350,  508 => 157,  498 => 314,  486 => 308,  480 => 215,  454 => 201,  450 => 158,  410 => 133,  325 => 102,  220 => 75,  407 => 138,  402 => 200,  377 => 130,  313 => 153,  232 => 111,  465 => 203,  457 => 192,  417 => 239,  411 => 179,  408 => 178,  396 => 197,  391 => 168,  388 => 192,  372 => 210,  344 => 109,  332 => 220,  293 => 163,  274 => 90,  231 => 99,  200 => 94,  792 => 418,  788 => 356,  782 => 208,  776 => 439,  774 => 349,  762 => 206,  748 => 200,  742 => 334,  734 => 193,  730 => 385,  724 => 327,  716 => 403,  706 => 360,  698 => 314,  694 => 313,  688 => 364,  680 => 184,  676 => 358,  662 => 350,  658 => 384,  652 => 411,  644 => 405,  640 => 375,  634 => 337,  622 => 274,  616 => 273,  608 => 323,  598 => 167,  580 => 165,  564 => 338,  545 => 353,  541 => 321,  526 => 159,  507 => 317,  488 => 321,  462 => 227,  433 => 226,  424 => 184,  395 => 223,  382 => 216,  376 => 162,  341 => 223,  327 => 161,  320 => 181,  310 => 99,  291 => 140,  278 => 91,  259 => 122,  244 => 117,  448 => 222,  443 => 220,  429 => 212,  406 => 201,  366 => 182,  318 => 119,  282 => 92,  258 => 83,  222 => 70,  120 => 37,  272 => 130,  266 => 87,  226 => 77,  178 => 79,  111 => 34,  393 => 169,  386 => 187,  380 => 184,  362 => 204,  358 => 207,  342 => 108,  340 => 162,  334 => 104,  326 => 138,  319 => 134,  314 => 100,  299 => 95,  265 => 89,  252 => 82,  237 => 112,  194 => 95,  132 => 51,  23 => 2,  97 => 39,  81 => 22,  53 => 13,  654 => 176,  637 => 295,  632 => 280,  625 => 343,  623 => 342,  617 => 195,  612 => 362,  604 => 357,  593 => 327,  591 => 186,  586 => 348,  583 => 259,  578 => 379,  571 => 164,  557 => 277,  534 => 242,  522 => 236,  520 => 340,  504 => 248,  494 => 216,  463 => 262,  446 => 148,  440 => 136,  434 => 141,  431 => 144,  427 => 182,  405 => 233,  401 => 130,  397 => 114,  389 => 220,  381 => 210,  357 => 62,  349 => 148,  339 => 107,  303 => 149,  295 => 113,  287 => 139,  268 => 129,  74 => 23,  470 => 398,  452 => 236,  444 => 250,  435 => 246,  430 => 187,  414 => 183,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 940,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 916,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 886,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 833,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 791,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 755,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 725,  1387 => 694,  1378 => 688,  1374 => 712,  1368 => 684,  1359 => 678,  1355 => 702,  1349 => 674,  1340 => 668,  1336 => 692,  1330 => 664,  1321 => 658,  1317 => 682,  1311 => 654,  1302 => 648,  1298 => 672,  1292 => 644,  1283 => 638,  1279 => 662,  1273 => 634,  1264 => 628,  1260 => 652,  1254 => 624,  1245 => 618,  1241 => 642,  1235 => 614,  1226 => 608,  1222 => 632,  1216 => 604,  1207 => 598,  1203 => 622,  1197 => 594,  1188 => 588,  1184 => 612,  1178 => 584,  1169 => 578,  1165 => 602,  1159 => 574,  1150 => 568,  1146 => 592,  1140 => 564,  1123 => 581,  1119 => 697,  1113 => 546,  1104 => 571,  1100 => 690,  1094 => 536,  1085 => 561,  1081 => 529,  1075 => 526,  1066 => 671,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 599,  933 => 450,  929 => 588,  923 => 446,  914 => 440,  910 => 439,  904 => 298,  895 => 430,  891 => 282,  885 => 259,  874 => 420,  870 => 419,  866 => 418,  860 => 440,  851 => 409,  847 => 233,  841 => 382,  832 => 225,  828 => 398,  822 => 420,  813 => 368,  809 => 388,  803 => 216,  794 => 359,  790 => 357,  784 => 354,  775 => 369,  771 => 368,  765 => 431,  756 => 359,  752 => 202,  746 => 355,  737 => 413,  733 => 330,  727 => 408,  712 => 420,  708 => 419,  702 => 416,  693 => 323,  689 => 187,  683 => 349,  674 => 241,  670 => 355,  664 => 339,  649 => 293,  645 => 296,  639 => 323,  630 => 371,  626 => 276,  620 => 172,  611 => 171,  607 => 279,  601 => 303,  592 => 262,  588 => 261,  582 => 347,  563 => 348,  554 => 344,  550 => 246,  544 => 161,  535 => 160,  531 => 236,  516 => 228,  512 => 318,  506 => 223,  493 => 294,  478 => 210,  468 => 150,  459 => 200,  455 => 209,  449 => 193,  436 => 216,  428 => 208,  422 => 234,  409 => 232,  403 => 261,  390 => 188,  384 => 191,  351 => 112,  337 => 126,  311 => 116,  296 => 142,  256 => 104,  241 => 80,  215 => 73,  207 => 92,  192 => 84,  186 => 86,  175 => 90,  153 => 48,  118 => 35,  61 => 15,  34 => 5,  65 => 19,  77 => 21,  37 => 7,  190 => 87,  161 => 66,  137 => 70,  126 => 63,  261 => 84,  255 => 197,  247 => 98,  242 => 112,  214 => 96,  211 => 97,  191 => 58,  157 => 59,  145 => 80,  127 => 54,  373 => 68,  367 => 154,  363 => 173,  359 => 172,  354 => 152,  343 => 143,  335 => 145,  328 => 152,  322 => 156,  315 => 175,  309 => 152,  305 => 58,  302 => 96,  290 => 185,  284 => 152,  279 => 139,  271 => 89,  264 => 185,  248 => 118,  236 => 112,  223 => 76,  170 => 57,  110 => 33,  96 => 63,  84 => 25,  472 => 210,  467 => 148,  375 => 211,  371 => 160,  360 => 179,  356 => 114,  353 => 113,  350 => 151,  338 => 145,  336 => 221,  331 => 103,  321 => 158,  316 => 157,  307 => 98,  304 => 97,  301 => 115,  297 => 131,  292 => 141,  286 => 93,  283 => 181,  277 => 132,  275 => 92,  270 => 143,  263 => 200,  257 => 85,  253 => 53,  249 => 83,  245 => 110,  233 => 101,  225 => 102,  216 => 101,  206 => 95,  202 => 97,  198 => 96,  185 => 67,  180 => 92,  177 => 81,  165 => 74,  150 => 71,  124 => 35,  113 => 56,  100 => 29,  58 => 16,  251 => 118,  234 => 97,  213 => 102,  195 => 89,  174 => 62,  167 => 84,  146 => 70,  140 => 81,  128 => 49,  104 => 30,  90 => 27,  83 => 23,  52 => 14,  596 => 225,  590 => 314,  585 => 221,  577 => 357,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 162,  551 => 239,  546 => 207,  543 => 235,  539 => 205,  529 => 322,  525 => 173,  523 => 230,  518 => 229,  514 => 192,  509 => 224,  503 => 330,  500 => 219,  497 => 295,  495 => 313,  492 => 323,  490 => 155,  487 => 213,  484 => 275,  482 => 177,  479 => 176,  477 => 271,  474 => 206,  471 => 207,  469 => 276,  466 => 228,  464 => 292,  461 => 169,  458 => 239,  456 => 199,  451 => 189,  445 => 160,  442 => 258,  439 => 248,  437 => 214,  432 => 191,  426 => 242,  423 => 141,  420 => 240,  418 => 204,  413 => 172,  399 => 137,  394 => 129,  378 => 119,  370 => 183,  368 => 203,  365 => 117,  361 => 116,  347 => 195,  345 => 194,  333 => 188,  329 => 125,  323 => 101,  317 => 154,  312 => 133,  306 => 170,  300 => 143,  294 => 116,  285 => 193,  280 => 180,  276 => 176,  267 => 128,  250 => 81,  239 => 79,  229 => 103,  218 => 74,  212 => 89,  210 => 98,  205 => 104,  188 => 94,  184 => 93,  181 => 96,  169 => 79,  160 => 82,  152 => 84,  148 => 62,  139 => 80,  134 => 79,  114 => 61,  107 => 43,  76 => 23,  70 => 22,  273 => 131,  269 => 88,  254 => 121,  246 => 52,  243 => 113,  240 => 109,  238 => 126,  235 => 106,  230 => 78,  227 => 103,  224 => 97,  221 => 101,  219 => 98,  217 => 100,  208 => 100,  204 => 99,  179 => 91,  159 => 75,  143 => 57,  135 => 80,  131 => 78,  108 => 39,  102 => 28,  71 => 20,  67 => 20,  63 => 18,  59 => 17,  47 => 14,  94 => 28,  89 => 50,  85 => 33,  79 => 22,  75 => 20,  68 => 20,  56 => 16,  50 => 13,  38 => 14,  29 => 3,  87 => 25,  72 => 23,  55 => 16,  21 => 2,  26 => 2,  35 => 4,  31 => 3,  41 => 13,  28 => 2,  201 => 87,  196 => 85,  183 => 92,  171 => 89,  166 => 51,  163 => 76,  156 => 71,  151 => 82,  142 => 69,  138 => 68,  136 => 77,  123 => 45,  121 => 51,  115 => 61,  105 => 28,  101 => 60,  91 => 26,  69 => 23,  66 => 19,  62 => 19,  49 => 9,  98 => 58,  93 => 44,  88 => 25,  78 => 24,  46 => 8,  44 => 17,  32 => 9,  27 => 5,  43 => 7,  40 => 6,  25 => 4,  24 => 4,  172 => 90,  158 => 73,  155 => 83,  129 => 68,  119 => 59,  117 => 60,  20 => 1,  22 => 2,  19 => 1,  209 => 101,  203 => 86,  199 => 90,  193 => 94,  189 => 93,  187 => 79,  182 => 56,  176 => 91,  173 => 80,  168 => 89,  164 => 74,  162 => 85,  154 => 72,  149 => 73,  147 => 74,  144 => 85,  141 => 71,  133 => 65,  130 => 77,  125 => 46,  122 => 63,  116 => 36,  112 => 35,  109 => 60,  106 => 59,  103 => 59,  99 => 27,  95 => 27,  92 => 26,  86 => 26,  82 => 25,  80 => 24,  73 => 24,  64 => 20,  60 => 17,  57 => 16,  54 => 10,  51 => 11,  48 => 15,  45 => 11,  42 => 7,  39 => 12,  36 => 9,  33 => 8,  30 => 3,);
    }
}
