<?php

/* PatCompteBundle:AdminReservation:detail_content.html.twig */
class __TwigTemplate_c203bf43db97e5bbd6c2882d3bf690ea7e967e7abe1677256d9e5079f503022c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "
  <div id=\"contenuCentrale1\">
    <h1>Détail de la réservation</h1>

    <div class=\"clear\"></div>

    ";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 9
            echo "      <div class=\"alert alert-success\">
        ";
            // line 10
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
      </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "

    ";
        // line 15
        if ((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa"))) {
            // line 16
            echo "
      <div class=\"row-fluid\">
        <div class=\"span12 right\">
          ";
            // line 19
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : null), "calculValues", array(), "any", false, true), "tva", array(), "array", true, true)) {
                // line 20
                echo "            <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_facture_generation", array("id" => $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-info\">Générer une facture</a> &nbsp;
          ";
            }
            // line 22
            echo "          <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_generate_payment_resa", array("member_id" => $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "id", array()), "id_reservation" => $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-info\">Générer un e-mail de paiement</a>
        </div>
      </div>

      <div class=\"blocForm\">
        <div class=\"title\">
          <div class=\"row-fluid\">
            <div class=\"span6\">
              Réf. ";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "reference", array()), "html", null, true);
            echo "
            </div>
            <div class=\"span6 right\">
              ";
            // line 33
            $this->env->loadTemplate("PatCompteBundle:Reservation:_resa_status.html.twig")->display($context);
            // line 34
            echo "            </div>
          </div>
        </div>

        <div class=\"content\">
          ";
            // line 39
            if (($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "optionSupplementaires", array()) == "1")) {
                // line 40
                echo "            <br />
            <strong>Options :</strong> Le locataire souhaite être recontacté pour choisir des options    <br />
          ";
            }
            // line 43
            echo "
          <br/>
          <div class=\"row-fluid\">
            <strong>Réservation</strong>
          </div>



          ";
            // line 51
            if (($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "status", array()) == twig_constant("\\Pat\\CompteBundle\\Tools\\ReservationTools::CONSTANT_STATUS_CANCELED_BY_BANK"))) {
                // line 52
                echo "            <div class=\"row-fluid\">
              <div class=\"span4\">
                <p class=\"text-error\">Erreur de paiement :</p>
              </div>
              <div class=\"span8\">
                <p class=\"text-error\">";
                // line 57
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "paymentError", array()), "html", null, true);
                echo "</p>
              </div>
            </div>
          ";
            }
            // line 61
            echo "          <div class=\"row-fluid\">
            <div class=\"span4\">
              Réservation faite le :
            </div>
            <div class=\"span8\">
              ";
            // line 66
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "createdAt", array()), "d/m/Y à H:i:s"), "html", null, true);
            echo "
            </div>
          </div>

          <div class=\"row-fluid\">
            <div class=\"span4\">
              Commentaire :
            </div>
            <div class=\"span8\">
              ";
            // line 75
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["resa"]) ? $context["resa"] : null), "comment", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["resa"]) ? $context["resa"] : null), "comment", array()), "-")) : ("-")), "html", null, true);
            echo "
            </div>
          </div>

          <div class=\"row-fluid\">
            <div class=\"span4\">
              Site tiers :
            </div>
            <div class=\"span8\">
              ";
            // line 84
            if ((!(null === $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "siteTiers", array())))) {
                // line 85
                echo "                ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "siteTiers", array()), "name", array()), "html", null, true);
                echo "
                ";
                // line 86
                if ((true == $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "siteTiers", array()), "isAutoPayment", array()))) {
                    // line 87
                    echo "                  <i class=\"far fa-credit-card\" data-toggle=\"popover\" data-placement=\"top\" data-html=\"true\" data-content=\"Paiement automatique\"></i>
                ";
                }
                // line 89
                echo "              ";
            } else {
                // line 90
                echo "                -
              ";
            }
            // line 92
            echo "            </div>
          </div>

          ";
            // line 95
            if ((!(null === $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "parent", array())))) {
                // line 96
                echo "            <div class=\"row-fluid\">
              <strong>Extension de la réservation :</strong>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                Référence :
              </div>
              <div class=\"span8\">
                <a href=\"";
                // line 105
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_show", array("id_reservation" => $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "parent", array()), "id", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "parent", array()), "reference", array()), "html", null, true);
                echo "</a>
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                Effectuée le  :
              </div>
              <div class=\"span8\">
                ";
                // line 113
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "parent", array()), "createdAt", array()), "d/m/Y à H\\hi"), "html", null, true);
                echo "
              </div>
            </div>
          ";
            }
            // line 117
            echo "
          <br/>
          <div class=\"row-fluid\">
            <strong>Locataire</strong>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4\">
              Réservation faite par :
            </div>
            <div class=\"span8\">
              <a href=\"";
            // line 127
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_locataire_afficher", array("id_locataire" => $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "civilite", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "Nom", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "prenom", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "username", array()), "html", null, true);
            echo ")</a>
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4\">
              Réservation faite pour :
            </div>
            <div class=\"span8\">
              ";
            // line 135
            $context["nbPersonnes"] = $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "nbPersonne", array());
            // line 136
            echo "              ";
            echo $this->env->getExtension('translator')->getTranslator()->transChoice("{0} 0 personne | {1} 1 personne | ]1,Inf] %nbPersonnes% personnes", (isset($context["nbPersonnes"]) ? $context["nbPersonnes"] : $this->getContext($context, "nbPersonnes")), array("%nbPersonnes%" => (isset($context["nbPersonnes"]) ? $context["nbPersonnes"] : $this->getContext($context, "nbPersonnes"))), "messages");
            // line 139
            echo "              (Dont ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "nbEnfant", array()), "html", null, true);
            echo " enfant(s) (-18 ans))
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4\">
              Lits à préparer :
            </div>
            <div class=\"span8\">
              ";
            // line 147
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "NbLitSimple", array()), "html", null, true);
            echo " lits simples<br>
              ";
            // line 148
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "nbLitDouble", array()), "html", null, true);
            echo " lits doubles<br>
              ";
            // line 149
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "nbCanapeConvertible", array()), "html", null, true);
            echo " canapés convertibles
            </div>
          </div>
          ";
            // line 152
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "nationalite", array())) {
                // line 153
                echo "            <div class=\"row-fluid\">
              <div class=\"span4\">
                Nationalité :
              </div>
              <div class=\"span8\">
                ";
                // line 158
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "nationalite", array()), "html", null, true);
                echo "
              </div>
            </div>
          ";
            }
            // line 162
            echo "          ";
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "societe", array())) {
                // line 163
                echo "            <div class=\"row-fluid\">
              <div class=\"span4\">
                Société :
              </div>
              <div class=\"span8\">
                ";
                // line 168
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "societe", array()), "html", null, true);
                echo "
              </div>
            </div>
          ";
            }
            // line 172
            echo "          ";
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "siret", array())) {
                // line 173
                echo "            <div class=\"row-fluid\">
              <div class=\"span4\">
                SIRET :
              </div>
              <div class=\"span8\">
                ";
                // line 178
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "siret", array()), "html", null, true);
                echo "
              </div>
            </div>
          ";
            }
            // line 182
            echo "          <div class=\"row-fluid\">
            <div class=\"span4\">
              Adresse :
            </div>
            <div class=\"span8\">
              ";
            // line 187
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "adresse", array()), "html", null, true);
            echo "<br/>
              ";
            // line 188
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "adresse2", array()), "html", null, true);
            echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span8 offset4\">
              ";
            // line 193
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "codePostal", array()), "html", null, true);
            echo "
              ";
            // line 194
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "ville", array()), "html", null, true);
            echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span8 offset4\">
              ";
            // line 199
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "pays", array()), "html", null, true);
            echo "
            </div>
          </div>

          ";
            // line 203
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "villeFact", array())) {
                // line 204
                echo "            <div class=\"row-fluid\">
              <div class=\"span4\">
                Adresse de facturation :
              </div>
              <div class=\"span8\">
                ";
                // line 209
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "adresseFact", array()), "html", null, true);
                echo "<br/>
                ";
                // line 210
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "adresse2Fact", array()), "html", null, true);
                echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span8 offset4\">
                ";
                // line 215
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "codePostalFact", array()), "html", null, true);
                echo "
                ";
                // line 216
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "villeFact", array()), "html", null, true);
                echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span8 offset4\">
                ";
                // line 221
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "paysFact", array()), "html", null, true);
                echo "
              </div>
            </div>
          ";
            }
            // line 225
            echo "
          <div class=\"row-fluid\">
            <div class=\"span4\">
              Téléphone :
            </div>
            <div class=\"span8\">
              ";
            // line 231
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "telephone", array()), "html", null, true);
            echo "
            </div>
          </div>
          ";
            // line 234
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "mobile", array())) {
                // line 235
                echo "            <div class=\"row-fluid\">
              <div class=\"span4\">
                Mobile Mr :
              </div>
              <div class=\"span8\">
                ";
                // line 240
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "mobile", array()), "html", null, true);
                echo "
              </div>
            </div>
          ";
            }
            // line 244
            echo "          ";
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "mobile2", array())) {
                // line 245
                echo "            <div class=\"row-fluid\">
              <div class=\"span4\">
                Mobile Mme :
              </div>
              <div class=\"span8\">
                ";
                // line 250
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "mobile2", array()), "html", null, true);
                echo "
              </div>
            </div>
          ";
            }
            // line 254
            echo "          <div class=\"row-fluid\">
            <div class=\"span4\">
              Email :
            </div>
            <div class=\"span8\">
              ";
            // line 259
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "email", array()), "html", null, true);
            echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4\">
              Appartement Concerné :
            </div>
            <div class=\"span8\">
              ";
            // line 267
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "nomResidence", array()), "html", null, true);
            echo " | ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "reference", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "type", array()))), "html", null, true);
            echo " - ";
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "nbPieces", array())) {
                echo "T";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "nbPieces", array()), "html", null, true);
            }
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "surfaceSol", array()), "html", null, true);
            echo " m<sup>2</sup> à ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "ville", array()), "nom", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "ville", array()), "codePostal", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "ville", array()), "pays", array()), "html", null, true);
            echo " - <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_appartement_editer", array("id" => $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "id", array()))), "html", null, true);
            echo "\">Voir la fiche du Bien</a>
            </div>
          </div>

          ";
            // line 271
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "utilisateur", array())) {
                // line 272
                echo "            <br/>
            <div class=\"row-fluid\">
              <strong>Propriétaire</strong>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                Nom
              </div>
              <div class=\"span8\">
                <a href=\"";
                // line 282
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_proprietaire_afficher", array("id_proprio" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "utilisateur", array()), "id", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "utilisateur", array()), "civilite", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "utilisateur", array()), "Nom", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "utilisateur", array()), "prenom", array()), "html", null, true);
                echo " (";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "utilisateur", array()), "username", array()), "html", null, true);
                echo ")</a>
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                Adresse :
              </div>
              <div class=\"span8\">
                ";
                // line 290
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "utilisateur", array()), "adresse", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "utilisateur", array()), "adresse2", array()), "html", null, true);
                echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span8 offset4\">
                ";
                // line 295
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "utilisateur", array()), "codePostal", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "utilisateur", array()), "ville", array()), "html", null, true);
                echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                Téléphone :
              </div>
              <div class=\"span8\">
                ";
                // line 303
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "utilisateur", array()), "telephone", array()), "html", null, true);
                echo " ";
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "utilisateur", array()), "mobile", array())) {
                    echo " (";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "utilisateur", array()), "mobile", array()), "html", null, true);
                    echo ") ";
                }
                // line 304
                echo "              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                Email :
              </div>
              <div class=\"span8\">
                ";
                // line 311
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "utilisateur", array()), "email", array()), "html", null, true);
                echo "
              </div>
            </div>
          ";
            }
            // line 315
            echo "
          <hr>

          <div class=\"row-fluid\">
            <strong>Détail du tarif</strong>
          </div>

          <div class=\"row-fluid\">
            <div class=\"span4\">
              Tarif net propriétaire :
            </div>
            <div class=\"span8\">
              ";
            // line 327
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "tarif_net", array(), "array")), "html", null, true);
            echo " €
            </div>
          </div>

          ";
            // line 331
            if (($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : null), "calculValues", array(), "any", false, true), "promotion_activable", array(), "array", true, true) && ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "promotion_activable", array(), "array") != 0))) {
                // line 332
                echo "            <div class=\"row-fluid\">
              <div class=\"span4\">
                Dont ";
                // line 334
                echo ((($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "promotion_activable", array(), "array") > 0)) ? ("Majoration") : ("Promotion"));
                echo " :
              </div>
              <div class=\"span8\">
                ";
                // line 337
                echo ((($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "promotion_activable", array(), "array") > 0)) ? ("+") : (""));
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "promotion_activable", array(), "array"), "html", null, true);
                echo " %
              </div>
            </div>
          ";
            }
            // line 341
            echo "
          ";
            // line 342
            $context["commissionPourcent"] = $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "commission", array(), "array");
            // line 343
            echo "          ";
            $context["commission"] = ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "tarif_net", array(), "array") * ((isset($context["commissionPourcent"]) ? $context["commissionPourcent"] : $this->getContext($context, "commissionPourcent")) / 100));
            // line 344
            echo "
          <div class=\"row-fluid\">
            <div class=\"span4\">
              Comission (";
            // line 347
            echo twig_escape_filter($this->env, (isset($context["commissionPourcent"]) ? $context["commissionPourcent"] : $this->getContext($context, "commissionPourcent")), "html", null, true);
            echo " %) :
            </div>
            <div class=\"span8\">
              ";
            // line 350
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((isset($context["commission"]) ? $context["commission"] : $this->getContext($context, "commission"))), "html", null, true);
            echo " €
            </div>
          </div>

          <div class=\"row-fluid\">
            <div class=\"span4\">
              Tarif du ménage :
            </div>
            <div class=\"span8\">
              ";
            // line 359
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "menage_fixe", array(), "array")), "html", null, true);
            echo " €
            </div>
          </div>

          <div class=\"row-fluid\">
            <div class=\"span4\">
              Tarif EDF
            </div>
            <div class=\"span8\">
              ";
            // line 368
            $context["tarif_edf"] = ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "EDF", array(), "array") * (isset($context["nb_nuits"]) ? $context["nb_nuits"] : $this->getContext($context, "nb_nuits")));
            // line 369
            echo "              ";
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((isset($context["tarif_edf"]) ? $context["tarif_edf"] : $this->getContext($context, "tarif_edf"))), "html", null, true);
            echo " €
            </div>
          </div>

          <div class=\"row-fluid\">
            <div class=\"span4\">
              Tarif assurance (";
            // line 375
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "assurance", array(), "array"), "html", null, true);
            echo " %) :
            </div>
            <div class=\"span8\">
              ";
            // line 378
            $context["assurance"] = (($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "tarif_net", array(), "array") * (1 + ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "commission", array(), "array") / 100))) * ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "assurance", array(), "array") / 100));
            // line 379
            echo "              ";
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((isset($context["assurance"]) ? $context["assurance"] : $this->getContext($context, "assurance"))), "html", null, true);
            echo " €
            </div>
          </div>

          <div class=\"row-fluid\">
            <div class=\"span4\">
              Taxe de séjours :
            </div>
            <div class=\"span8\">
              ";
            // line 388
            $context["taxeSejour"] = (($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "taxe_sejour", array(), "array") * (isset($context["nb_nuits"]) ? $context["nb_nuits"] : $this->getContext($context, "nb_nuits"))) * $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "nbAdulte", array()));
            // line 389
            echo "              ";
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((isset($context["taxeSejour"]) ? $context["taxeSejour"] : $this->getContext($context, "taxeSejour"))), "html", null, true);
            echo " €
            </div>
          </div>

          <br/>
          <div class=\"row-fluid\">
            <strong>Tarif global</strong>
          </div>

          <div class=\"row-fluid\">
            <div class=\"span4\">
              Tarif séjour :
            </div>
            <div class=\"span8\">
              ";
            // line 403
            $context["tarif"] = $this->env->getExtension('my_twig_extension')->getTarif((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")));
            // line 404
            echo "              ";
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((isset($context["tarif"]) ? $context["tarif"] : $this->getContext($context, "tarif"))), "html", null, true);
            echo " €
            </div>
          </div>
          ";
            // line 407
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : null), "calculValues", array(), "any", false, true), "tva", array(), "array", true, true)) {
                // line 408
                echo "            <div class=\"row-fluid\">
              <div class=\"span4\">
                Dont TVA (";
                // line 410
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "tva", array(), "array"), "html", null, true);
                echo " %) :
              </div>
              <div class=\"span8\">
                ";
                // line 413
                $context["tva"] = ((($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "tarif_net", array(), "array") * ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "commission", array(), "array") / 100)) * $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "tva", array(), "array")) / 100);
                // line 414
                echo "                ";
                echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((isset($context["tva"]) ? $context["tva"] : $this->getContext($context, "tva"))), "html", null, true);
                echo " €
              </div>
            </div>
          ";
            }
            // line 418
            echo "          <div class=\"row-fluid\">
            <div class=\"span4\">
              Dont Promo/Majoration :
            </div>
            <div class=\"span8\">
              ";
            // line 423
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "promotion", array())), "html", null, true);
            echo " €
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4\">
              Dépôt de garantie :
            </div>
            <div class=\"span8\">
              ";
            // line 431
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "depot", array())), "html", null, true);
            echo " €
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4\">
              Solde total :
            </div>
            <div class=\"span8\">
              ";
            // line 439
            echo twig_escape_filter($this->env, ((isset($context["tarif"]) ? $context["tarif"] : $this->getContext($context, "tarif")) + $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "depot", array()))), "html", null, true);
            echo " €
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4\">
              Acompte :
            </div>
            <div class=\"span8\">
              ";
            // line 447
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "arrhes", array())), "html", null, true);
            echo " €
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4\">
              Déjà payé :
            </div>
            <div class=\"span8\">
              ";
            // line 455
            $context["payments"] = $this->env->getExtension('my_twig_extension')->getPayments((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")));
            // line 456
            echo "              ";
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((isset($context["payments"]) ? $context["payments"] : $this->getContext($context, "payments"))), "html", null, true);
            echo " €
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4\">
              Reste à payer :
            </div>
            <div class=\"span8\">
              ";
            // line 464
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((((isset($context["tarif"]) ? $context["tarif"] : $this->getContext($context, "tarif")) + $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "depot", array())) - (isset($context["payments"]) ? $context["payments"] : $this->getContext($context, "payments")))), "html", null, true);
            echo " €
            </div>
          </div>



          <br/>
          <div class=\"row-fluid\">
            <strong>Séjour</strong>
          </div>

          <div class=\"row-fluid\">
            <div class=\"span4\">
              Date d'arrivée :
            </div>
            <div class=\"span8\">
              ";
            // line 480
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "dateDebut", array()), "d/m/Y"), "html", null, true);
            echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4\">
              Date de départ :
            </div>
            <div class=\"span8\">
              ";
            // line 488
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "dateFin", array()), "d/m/Y"), "html", null, true);
            echo "
            </div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span4\">
              Nombre de nuits :
            </div>
            <div class=\"span8\">
              ";
            // line 496
            echo twig_escape_filter($this->env, (isset($context["nb_nuits"]) ? $context["nb_nuits"] : $this->getContext($context, "nb_nuits")), "html", null, true);
            echo "
            </div>
          </div>

          ";
            // line 510
            echo "
          ";
            // line 555
            echo "
          <br/>
          <div class=\"row-fluid\">
            <strong>Facturation</strong>
          </div>

          ";
            // line 561
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "villeFact", array())) {
                // line 562
                echo "
            <div class=\"row-fluid\">
              <div class=\"span4\">
                Adresse de Facturation :
              </div>
              <div class=\"span8\">
                ";
                // line 568
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "adresseFact", array()), "html", null, true);
                echo "<br/>
                ";
                // line 569
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "adresse2Fact", array()), "html", null, true);
                echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span8 offset4\">
                ";
                // line 574
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "codePostalFact", array()), "html", null, true);
                echo "
                ";
                // line 575
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "villeFact", array()), "html", null, true);
                echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span8 offset4\">
                ";
                // line 580
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "paysFact", array()), "html", null, true);
                echo "
              </div>
            </div>
          ";
            }
            // line 584
            echo "
          ";
            // line 585
            if (((!(null === $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "filenameFr", array()))) && (!(null === $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "filenameEn", array()))))) {
                // line 586
                echo "            <strong>Ancienne Facturation :</strong> &nbsp;  <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_download_facture", array("language" => "fr", "id" => $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "id", array()))), "html", null, true);
                echo "\">facture-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "filenameFr", array()), "html", null, true);
                echo "</a> &nbsp; &nbsp;  <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_download_facture", array("language" => "en", "id" => $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "id", array()))), "html", null, true);
                echo "\">facture-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "filenameEn", array()), "html", null, true);
                echo "</a>
          ";
            }
            // line 588
            echo "
        </div>
      </div>

      <div class=\"blocForm\">
        <div class=\"title\">
          <div class=\"row-fluid\">
            <div class=\"span6\">
              Options
            </div>
            <div class=\"span6 right\">
              <a href=\"";
            // line 599
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("option_new", array("reservation_id" => $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-small btn-primary\">Ajouter une option</a>
            </div>
          </div>
        </div>

        <div class=\"content\">
          ";
            // line 605
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("PatCompteBundle:Option:index", array("reservation_id" => $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "id", array()))));
            echo "
        </div>
      </div>

      <div class=\"blocForm\">
        <div class=\"title\">
          <div class=\"row-fluid\">
            <div class=\"span6\">
              <a href=\"";
            // line 613
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_list_payment_by_member", array("member_id" => $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "id", array()))), "html", null, true);
            echo "\">Paiements</a>
            </div>
            <div class=\"span6 right\">
              <a href=\"";
            // line 616
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_new_payment", array("member_id" => $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "id", array()), "id_reservation" => $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-small btn-primary\">Ajouter un paiement</a>
            </div>
          </div>
        </div>

        <div class=\"content\">
          ";
            // line 622
            $context["total"] = 0;
            // line 623
            echo "          ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "payments", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["payment"]) {
                // line 624
                echo "            <div class=\"row-fluid\">
              <div class=\"span7\">
                ";
                // line 626
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["payment"]) ? $context["payment"] : $this->getContext($context, "payment")), "name", array()), "html", null, true);
                echo " (";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["payment"]) ? $context["payment"] : $this->getContext($context, "payment")), "modePaiement", array())), "html", null, true);
                echo ")
              </div>
              <div class=\"span2 right\">
                ";
                // line 629
                echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute((isset($context["payment"]) ? $context["payment"] : $this->getContext($context, "payment")), "amount", array())), "html", null, true);
                echo " €
              </div>
              <div class=\"span3 center\">
                ";
                // line 632
                if (($this->getAttribute((isset($context["payment"]) ? $context["payment"] : $this->getContext($context, "payment")), "status", array()) == twig_constant("\\Pat\\CompteBundle\\Tools\\PaymentTools::CONSTANT_STATUS_WAIT"))) {
                    // line 633
                    echo "                  <span class=\"label label-info\">En attente</span>
                ";
                } elseif (($this->getAttribute((isset($context["payment"]) ? $context["payment"] : $this->getContext($context, "payment")), "status", array()) == twig_constant("\\Pat\\CompteBundle\\Tools\\PaymentTools::CONSTANT_STATUS_WAIT_RECEIPT"))) {
                    // line 635
                    echo "                  <span class=\"label label-warning\">En attente de réception</span>
                ";
                } elseif (($this->getAttribute((isset($context["payment"]) ? $context["payment"] : $this->getContext($context, "payment")), "status", array()) == twig_constant("\\Pat\\CompteBundle\\Tools\\PaymentTools::CONSTANT_STATUS_PAYED"))) {
                    // line 637
                    echo "                  <span class=\"label label-success\">Payé</span> le ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["payment"]) ? $context["payment"] : $this->getContext($context, "payment")), "payedAt", array()), "d/m/Y"), "html", null, true);
                    echo "
                  ";
                    // line 638
                    $context["total"] = ((isset($context["total"]) ? $context["total"] : $this->getContext($context, "total")) + $this->getAttribute((isset($context["payment"]) ? $context["payment"] : $this->getContext($context, "payment")), "amount", array()));
                    // line 639
                    echo "                ";
                } elseif (($this->getAttribute((isset($context["payment"]) ? $context["payment"] : $this->getContext($context, "payment")), "status", array()) == twig_constant("\\Pat\\CompteBundle\\Tools\\PaymentTools::CONSTANT_STATUS_CANCELED"))) {
                    // line 640
                    echo "                  <span class=\"label label-important\">Annulé</span>
                ";
                }
                // line 642
                echo "              </div>
            </div>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['payment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 645
            echo "          <hr>
          <div class=\"row-fluid\">
            <div class=\"span7\">
              <strong>Total payé</strong>
            </div>
            <div class=\"span2 right\">
              <strong>";
            // line 651
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((isset($context["total"]) ? $context["total"] : $this->getContext($context, "total"))), "html", null, true);
            echo " €</strong>
            </div>
          </div>
        </div>
      </div>

      <div class=\"blocForm\">
        <div class=\"title\">
          <div class=\"row-fluid\">
            <div class=\"span6\">
              Facture
            </div>
            <div class=\"span6 right\">

            </div>
          </div>
        </div>

        <div class=\"content\">
          ";
            // line 670
            if (twig_test_empty($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "factures", array()))) {
                // line 671
                echo "            <div class=\"alert alert-info\">
              <i class=\"fas fa-info\"> Aucunes factures pour cette réservation</i>
            </div>
          ";
            } else {
                // line 675
                echo "            <table class=\"table table-bordered table-striped\">
              <thead>
                <tr>
                  <th>Facture n°</th>
                  <th>Format</th>
                  <th>Date de création</th>
                  <th>Télécharger</th>
                  <th>Envoyer au locataire</th>
                </tr>
              </thead>
              <tbody>
                ";
                // line 686
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "factures", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["facture"]) {
                    // line 687
                    echo "                  <tr>
                    <td>";
                    // line 688
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "number", array()), "html", null, true);
                    echo "</td>
                    <td>";
                    // line 689
                    echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "locale", array())), "html", null, true);
                    echo "</td>
                    <td>";
                    // line 690
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "createdAt", array()), "d/m/Y à H\\hi"), "html", null, true);
                    echo "</td>
                    <td>
                      <a href=\"";
                    // line 692
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_download_facture_new", array("id" => $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "id", array()))), "html", null, true);
                    echo "\"><i class=\"fas fa-download\"></i> Facture</a>
                      ";
                    // line 693
                    if ((!(null === $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "avoirFileName", array())))) {
                        // line 694
                        echo "                        <br>
                        <a href=\"";
                        // line 695
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_download_avoir_new", array("id" => $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "id", array()))), "html", null, true);
                        echo "\"><i class=\"fas fa-download\"></i> Avoir</a>
                      ";
                    }
                    // line 697
                    echo "                    </td>
                    <td>
                      <a href=\"";
                    // line 699
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_send_facture", array("id" => $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "id", array()))), "html", null, true);
                    echo "\" onclick=\"return confirmAction('Voulez vous vraiment envoyer cette facture à ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "reservation", array()), "utilisateur", array()), "email", array()), "html", null, true);
                    echo "')\"><i class=\"far fa-paper-plane\"></i> Envoyer la facture</a>
                      ";
                    // line 700
                    if ((!(null === $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "avoirFileName", array())))) {
                        // line 701
                        echo "                        <br>
                        <a href=\"";
                        // line 702
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_send_avoir", array("id" => $this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "id", array()))), "html", null, true);
                        echo "\" onclick=\"return confirmAction('Voulez vous vraiment envoyer cet avoir à ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["facture"]) ? $context["facture"] : $this->getContext($context, "facture")), "reservation", array()), "utilisateur", array()), "email", array()), "html", null, true);
                        echo "')\"><i class=\"far fa-paper-plane\"></i> Avoir</a>
                      ";
                    }
                    // line 704
                    echo "                    </td>
                  </tr>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['facture'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 707
                echo "              </tbody>
            </table>
          ";
            }
            // line 710
            echo "        </div>
      </div>

      <div class=\"row-fluid\">
        <div class=\"span6\">
          <a href=\"";
            // line 715
            echo twig_escape_filter($this->env, (isset($context["backward_url"]) ? $context["backward_url"] : $this->getContext($context, "backward_url")), "html", null, true);
            echo "\" class=\"btn btn-large btn-secondary\">Retour</a>
        </div>
        <div class=\"span6 right\">
          <a href=\"";
            // line 718
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_modifier", array("id_reservation" => $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary\">Modifier</a>
        </div>
      </div>
    ";
        }
        // line 722
        echo "  </div><!-- /contenuCentrale1 -->

  <script>
    \$(document).ready(function () {
      \$('[data-toggle=\"popover\"]').popover({
        trigger: 'hover'
      })
    });
  </script>
";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:AdminReservation:detail_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  1174 => 722,  1167 => 718,  1161 => 715,  1154 => 710,  1149 => 707,  1141 => 704,  1134 => 702,  1131 => 701,  1129 => 700,  1114 => 695,  1111 => 694,  1109 => 693,  1105 => 692,  1096 => 689,  1092 => 688,  1089 => 687,  1072 => 675,  1064 => 670,  1042 => 651,  1034 => 645,  1026 => 642,  1022 => 640,  1019 => 639,  1017 => 638,  1012 => 637,  1008 => 635,  1004 => 633,  1002 => 632,  996 => 629,  988 => 626,  984 => 624,  979 => 623,  977 => 622,  968 => 616,  962 => 613,  951 => 605,  917 => 586,  915 => 585,  912 => 584,  905 => 580,  897 => 575,  893 => 574,  881 => 568,  873 => 562,  871 => 561,  863 => 555,  853 => 496,  842 => 488,  831 => 480,  812 => 464,  800 => 456,  798 => 455,  787 => 447,  754 => 423,  747 => 418,  739 => 414,  731 => 410,  725 => 407,  718 => 404,  696 => 388,  681 => 378,  675 => 375,  665 => 369,  663 => 368,  651 => 359,  628 => 344,  606 => 334,  602 => 332,  600 => 331,  579 => 315,  572 => 311,  555 => 303,  542 => 295,  532 => 290,  513 => 282,  501 => 272,  499 => 271,  473 => 267,  441 => 245,  438 => 244,  416 => 231,  330 => 182,  289 => 158,  633 => 347,  627 => 291,  621 => 289,  619 => 288,  594 => 272,  576 => 263,  570 => 260,  562 => 255,  558 => 254,  552 => 251,  540 => 245,  508 => 228,  498 => 224,  486 => 218,  480 => 215,  454 => 201,  450 => 200,  410 => 175,  325 => 131,  220 => 89,  407 => 237,  402 => 234,  377 => 209,  313 => 172,  232 => 107,  465 => 197,  457 => 192,  417 => 178,  411 => 176,  408 => 225,  396 => 232,  391 => 168,  388 => 229,  372 => 160,  344 => 149,  332 => 135,  293 => 130,  274 => 149,  231 => 94,  200 => 50,  792 => 418,  788 => 416,  782 => 414,  776 => 439,  774 => 411,  762 => 402,  748 => 394,  742 => 391,  734 => 386,  730 => 385,  724 => 382,  716 => 403,  706 => 373,  698 => 389,  694 => 367,  688 => 364,  680 => 359,  676 => 358,  662 => 350,  658 => 349,  652 => 346,  644 => 341,  640 => 340,  634 => 337,  622 => 331,  616 => 328,  608 => 323,  598 => 273,  580 => 264,  564 => 297,  545 => 287,  541 => 286,  526 => 237,  507 => 267,  488 => 257,  462 => 259,  433 => 226,  424 => 235,  395 => 169,  382 => 199,  376 => 162,  341 => 188,  327 => 167,  320 => 153,  310 => 125,  291 => 118,  278 => 121,  259 => 122,  244 => 113,  448 => 250,  443 => 230,  429 => 259,  406 => 174,  366 => 206,  318 => 175,  282 => 153,  258 => 129,  222 => 91,  120 => 29,  272 => 129,  266 => 147,  226 => 92,  178 => 85,  111 => 44,  393 => 216,  386 => 166,  380 => 163,  362 => 294,  358 => 207,  342 => 139,  340 => 287,  334 => 284,  326 => 278,  319 => 273,  314 => 271,  299 => 163,  265 => 107,  252 => 137,  237 => 128,  194 => 75,  132 => 32,  23 => 3,  97 => 23,  81 => 30,  53 => 16,  654 => 223,  637 => 295,  632 => 206,  625 => 343,  623 => 342,  617 => 195,  612 => 337,  604 => 322,  593 => 327,  591 => 186,  586 => 313,  583 => 183,  578 => 180,  571 => 178,  557 => 177,  534 => 242,  522 => 236,  520 => 171,  504 => 227,  494 => 158,  463 => 145,  446 => 186,  440 => 136,  434 => 134,  431 => 240,  427 => 182,  405 => 210,  401 => 221,  397 => 114,  389 => 215,  381 => 210,  357 => 148,  349 => 193,  339 => 105,  303 => 99,  295 => 119,  287 => 127,  268 => 117,  74 => 22,  470 => 398,  452 => 236,  444 => 197,  435 => 405,  430 => 397,  414 => 241,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 887,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 864,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 825,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 808,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 763,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 732,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 697,  1387 => 694,  1378 => 688,  1374 => 687,  1368 => 684,  1359 => 678,  1355 => 677,  1349 => 674,  1340 => 668,  1336 => 667,  1330 => 664,  1321 => 658,  1317 => 657,  1311 => 654,  1302 => 648,  1298 => 647,  1292 => 644,  1283 => 638,  1279 => 637,  1273 => 634,  1264 => 628,  1260 => 627,  1254 => 624,  1245 => 618,  1241 => 617,  1235 => 614,  1226 => 608,  1222 => 607,  1216 => 604,  1207 => 598,  1203 => 597,  1197 => 594,  1188 => 588,  1184 => 587,  1178 => 584,  1169 => 578,  1165 => 577,  1159 => 574,  1150 => 568,  1146 => 567,  1140 => 564,  1123 => 699,  1119 => 697,  1113 => 546,  1104 => 540,  1100 => 690,  1094 => 536,  1085 => 686,  1081 => 529,  1075 => 526,  1066 => 671,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 599,  933 => 450,  929 => 588,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 569,  874 => 420,  870 => 419,  866 => 418,  860 => 510,  851 => 409,  847 => 408,  841 => 405,  832 => 399,  828 => 398,  822 => 395,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 431,  756 => 359,  752 => 395,  746 => 355,  737 => 413,  733 => 348,  727 => 408,  712 => 376,  708 => 332,  702 => 329,  693 => 323,  689 => 322,  683 => 379,  674 => 241,  670 => 355,  664 => 309,  649 => 297,  645 => 296,  639 => 350,  630 => 287,  626 => 332,  620 => 341,  611 => 277,  607 => 279,  601 => 189,  592 => 267,  588 => 269,  582 => 263,  563 => 304,  554 => 293,  550 => 246,  544 => 246,  535 => 283,  531 => 236,  516 => 233,  512 => 165,  506 => 163,  493 => 216,  478 => 253,  468 => 209,  459 => 144,  455 => 254,  449 => 193,  436 => 192,  428 => 181,  422 => 234,  409 => 117,  403 => 168,  390 => 230,  384 => 165,  351 => 145,  337 => 187,  311 => 118,  296 => 162,  256 => 104,  241 => 98,  215 => 99,  207 => 98,  192 => 82,  186 => 76,  175 => 71,  153 => 50,  118 => 36,  61 => 19,  34 => 8,  65 => 17,  77 => 23,  37 => 7,  190 => 77,  161 => 60,  137 => 40,  126 => 35,  261 => 106,  255 => 135,  247 => 130,  242 => 127,  214 => 87,  211 => 113,  191 => 76,  157 => 37,  145 => 71,  127 => 47,  373 => 111,  367 => 173,  363 => 155,  359 => 171,  354 => 153,  343 => 135,  335 => 104,  328 => 152,  322 => 175,  315 => 170,  309 => 140,  305 => 115,  302 => 267,  290 => 157,  284 => 154,  279 => 129,  271 => 110,  264 => 122,  248 => 118,  236 => 77,  223 => 82,  170 => 44,  110 => 40,  96 => 39,  84 => 32,  472 => 210,  467 => 148,  375 => 152,  371 => 160,  360 => 183,  356 => 182,  353 => 194,  350 => 151,  338 => 138,  336 => 146,  331 => 169,  321 => 130,  316 => 173,  307 => 269,  304 => 268,  301 => 122,  297 => 131,  292 => 110,  286 => 116,  283 => 105,  277 => 104,  275 => 103,  270 => 148,  263 => 123,  257 => 121,  253 => 108,  249 => 135,  245 => 92,  233 => 92,  225 => 89,  216 => 84,  206 => 80,  202 => 78,  198 => 105,  185 => 95,  180 => 92,  177 => 57,  165 => 65,  150 => 53,  124 => 40,  113 => 51,  100 => 35,  58 => 15,  251 => 136,  234 => 111,  213 => 113,  195 => 91,  174 => 84,  167 => 86,  146 => 51,  140 => 54,  128 => 41,  104 => 29,  90 => 23,  83 => 31,  52 => 11,  596 => 225,  590 => 314,  585 => 221,  577 => 218,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 210,  551 => 176,  546 => 207,  543 => 206,  539 => 205,  529 => 174,  525 => 173,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 266,  500 => 160,  497 => 263,  495 => 182,  492 => 157,  490 => 219,  487 => 213,  484 => 256,  482 => 177,  479 => 176,  477 => 401,  474 => 206,  471 => 173,  469 => 172,  466 => 146,  464 => 397,  461 => 169,  458 => 239,  456 => 167,  451 => 189,  445 => 160,  442 => 159,  439 => 229,  437 => 262,  432 => 191,  426 => 188,  423 => 180,  420 => 219,  418 => 251,  413 => 172,  399 => 237,  394 => 162,  378 => 162,  370 => 204,  368 => 203,  365 => 212,  361 => 199,  347 => 136,  345 => 94,  333 => 131,  329 => 179,  323 => 178,  317 => 272,  312 => 114,  306 => 168,  300 => 110,  294 => 156,  285 => 105,  280 => 152,  276 => 112,  267 => 60,  250 => 101,  239 => 91,  229 => 116,  218 => 117,  212 => 82,  210 => 97,  205 => 83,  188 => 71,  184 => 99,  181 => 74,  169 => 87,  160 => 84,  152 => 63,  148 => 75,  139 => 41,  134 => 45,  114 => 50,  107 => 39,  76 => 27,  70 => 21,  273 => 127,  269 => 94,  254 => 139,  246 => 100,  243 => 88,  240 => 101,  238 => 113,  235 => 95,  230 => 127,  227 => 99,  224 => 112,  221 => 96,  219 => 112,  217 => 84,  208 => 86,  204 => 51,  179 => 71,  159 => 78,  143 => 70,  135 => 37,  131 => 36,  108 => 47,  102 => 26,  71 => 22,  67 => 27,  63 => 20,  59 => 15,  47 => 8,  94 => 25,  89 => 34,  85 => 28,  79 => 26,  75 => 22,  68 => 18,  56 => 16,  50 => 13,  38 => 9,  29 => 4,  87 => 33,  72 => 22,  55 => 14,  21 => 2,  26 => 2,  35 => 6,  31 => 3,  41 => 10,  28 => 2,  201 => 82,  196 => 75,  183 => 72,  171 => 64,  166 => 59,  163 => 79,  156 => 51,  151 => 48,  142 => 46,  138 => 44,  136 => 66,  123 => 45,  121 => 54,  115 => 52,  105 => 32,  101 => 39,  91 => 31,  69 => 22,  66 => 16,  62 => 16,  49 => 13,  98 => 40,  93 => 41,  88 => 30,  78 => 20,  46 => 12,  44 => 18,  32 => 5,  27 => 4,  43 => 7,  40 => 6,  25 => 4,  24 => 2,  172 => 54,  158 => 58,  155 => 77,  129 => 61,  119 => 31,  117 => 36,  20 => 1,  22 => 2,  19 => 1,  209 => 87,  203 => 97,  199 => 79,  193 => 76,  189 => 87,  187 => 96,  182 => 68,  176 => 90,  173 => 89,  168 => 52,  164 => 72,  162 => 85,  154 => 54,  149 => 73,  147 => 48,  144 => 47,  141 => 35,  133 => 48,  130 => 42,  125 => 55,  122 => 57,  116 => 28,  112 => 27,  109 => 47,  106 => 31,  103 => 43,  99 => 44,  95 => 26,  92 => 35,  86 => 26,  82 => 27,  80 => 31,  73 => 19,  64 => 22,  60 => 17,  57 => 22,  54 => 15,  51 => 13,  48 => 14,  45 => 13,  42 => 8,  39 => 16,  36 => 9,  33 => 9,  30 => 3,);
    }
}
