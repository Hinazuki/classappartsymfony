<?php

/* PatCompteBundle:AdminReservation:facture_content.html.twig */
class __TwigTemplate_feface9e6a9b304f7ec25624579d70f9cfe69fe7e09fe62320872459a461c20a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "
  <script type=\"text/javascript\">
    \$(document).ready(function () {
      tinyMCE.init({
        // General options
        mode: \"exact\",
        elements: \"CAadresse\",
        theme: \"simple\",
        relative_urls: false,
        width: \"300\"
      });
      tinyMCE.init({
        // General options
        mode: \"exact\",
        elements: \"Contenu_description1\",
        theme: \"simple\",
        relative_urls: false,
        width: \"300\"
      });
      tinyMCE.init({
        // General options
        mode: \"exact\",
        elements: \"Contenu_description1-1\",
        theme: \"simple\",
        relative_urls: false,
        width: \"300\"
      });

      tinyMCE.init({
        // General options
        mode: \"exact\",
        elements: \"Contenu_description2\",
        theme: \"simple\",
        relative_urls: false,
        width: \"535\"
      });


      tinyMCE.init({
        // General options
        mode: \"exact\",
        elements: \"Contenu_description2-2\",
        theme: \"simple\",
        relative_urls: false,
        width: \"535\"
      });

      tinyMCE.init({
        // General options
        mode: \"exact\",
        elements: \"Contenu_description3\",
        theme: \"simple\",
        width: \"300\"
      });

      tinyMCE.init({
        // General options
        mode: \"exact\",
        elements: \"Contenu_description3-3\",
        theme: \"simple\",
        width: \"300\"
      });

    });
  </script>
  <!-- /TinyMCE -->

  <div id=\"contenuCentrale1\">
    <h1>Facture</h1>

    <div class=\"clear\"></div>

    ";
        // line 74
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 75
            echo "      <div class=\"alert alert-success\">
        ";
            // line 76
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
      </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "

    ";
        // line 81
        if ((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa"))) {
            // line 82
            echo "      <form action=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_reservation_facture_generation_pdf", array("id" => $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "id", array()))), "html", null, true);
            echo "\" method=\"post\">
        <div class=\"blocForm\">
          <div class=\"title\">
            <div class=\"row-fluid\">
              <div class=\"span12\">
                Facture N° ";
            // line 87
            echo twig_escape_filter($this->env, (isset($context["factureNumber"]) ? $context["factureNumber"] : $this->getContext($context, "factureNumber")), "html", null, true);
            echo "
              </div>
            </div>
          </div>

          <div class=\"content\">
            <div class=\"row-fluid\">
              <div class=\"span6\">
                <strong>Adresse :</strong> <br />
                <textarea name=\"CAadresse\" id=\"CAaddresse\">
                            <h2>CLASS APPART</h2>
                            <p>
                                15 Passage Lonjon <br/>
                                34000 MONTPELLIER <br/>
                                Téléphone : + 33(0)6.10.64.33.08 <br/>
                                Email : <a href=\"mailto:info@class-appart.com\">info@class-appart.com</a><br/>
                                SIRET : 790 259 287 000 15 <br/>
                            </p>
                </textarea>
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span6\">
                <strong>Haut de page FR :</strong> <br />
                <textarea name=\"haut\" id=\"Contenu_description1\">
                        Chère cliente, cher client, <br />
                        Merci d'avoir choisi Class Appart pour votre séjour à Montpellier<br />
                        Pour plus d'informations, n'hésitez pas à nous contacter.<br />
                </textarea>
              </div>
              <div class=\"span6\">
                <strong>Haut de page EN :</strong> <br />
                <textarea name=\"haut2\" id=\"Contenu_description1-1\">
                        Dear customer, <br />
                        Thank you for choosing Class Appart as your stay in Montpellier FRANCE. <br />
                        Contact us for more information. <br />
                </textarea>
              </div>
            </div>

            <br/>
            ";
            // line 132
            echo "            <div class=\"row-fluid\">
              <div class=\"span4\">
                Réservation faite par :
              </div>
              <div class=\"span8\">
                <a href=\"";
            // line 137
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_locataire_afficher", array("id_locataire" => $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "civilite", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "Nom", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "prenom", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "username", array()), "html", null, true);
            echo ")</a>
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                Réservation faite pour :
              </div>
              <div class=\"span8\">
                ";
            // line 145
            $context["nbPersonnes"] = $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "nbPersonne", array());
            // line 146
            echo "                ";
            echo $this->env->getExtension('translator')->getTranslator()->transChoice("{0} 0 personne | {1} 1 personne | ]1,Inf] %nbPersonnes% personnes", (isset($context["nbPersonnes"]) ? $context["nbPersonnes"] : $this->getContext($context, "nbPersonnes")), array("%nbPersonnes%" => (isset($context["nbPersonnes"]) ? $context["nbPersonnes"] : $this->getContext($context, "nbPersonnes"))), "messages");
            // line 149
            echo "                (Dont ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "nbEnfant", array()), "html", null, true);
            echo " enfant(s) (-18 ans))
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                Lits à préparer :
              </div>
              <div class=\"span8\">
                ";
            // line 157
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "NbLitSimple", array()), "html", null, true);
            echo " lits simples<br>
                ";
            // line 158
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "nbLitDouble", array()), "html", null, true);
            echo " lits doubles<br>
                ";
            // line 159
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "nbCanapeConvertible", array()), "html", null, true);
            echo " canapés convertibles
              </div>
            </div>
            ";
            // line 162
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "nationalite", array())) {
                // line 163
                echo "              <div class=\"row-fluid\">
                <div class=\"span4\">
                  Nationalité :
                </div>
                <div class=\"span8\">
                  ";
                // line 168
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "nationalite", array()), "html", null, true);
                echo "
                </div>
              </div>
            ";
            }
            // line 172
            echo "            ";
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "societe", array())) {
                // line 173
                echo "              <div class=\"row-fluid\">
                <div class=\"span4\">
                  Société :
                </div>
                <div class=\"span8\">
                  ";
                // line 178
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "societe", array()), "html", null, true);
                echo "
                </div>
              </div>
            ";
            }
            // line 182
            echo "            ";
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "siret", array())) {
                // line 183
                echo "              <div class=\"row-fluid\">
                <div class=\"span4\">
                  SIRET :
                </div>
                <div class=\"span8\">
                  ";
                // line 188
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "siret", array()), "html", null, true);
                echo "
                </div>
              </div>
            ";
            }
            // line 192
            echo "
            ";
            // line 193
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "villeFact", array())) {
                // line 194
                echo "              <div class=\"row-fluid\">
                <div class=\"span4\">
                  Adresse de facturation :
                </div>
                <div class=\"span8\">
                  ";
                // line 199
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "adresseFact", array()), "html", null, true);
                echo "<br/>
                  ";
                // line 200
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "adresse2Fact", array()), "html", null, true);
                echo "
                </div>
              </div>
              <div class=\"row-fluid\">
                <div class=\"span8 offset4\">
                  ";
                // line 205
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "codePostalFact", array()), "html", null, true);
                echo "
                  ";
                // line 206
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "villeFact", array()), "html", null, true);
                echo "
                </div>
              </div>
              <div class=\"row-fluid\">
                <div class=\"span8 offset4\">
                  ";
                // line 211
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "paysFact", array()), "html", null, true);
                echo "
                </div>
              </div>
            ";
            } else {
                // line 215
                echo "              <div class=\"row-fluid\">
                <div class=\"span4\">
                  Adresse :
                </div>
                <div class=\"span8\">
                  ";
                // line 220
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "adresse", array()), "html", null, true);
                echo "<br/>
                  ";
                // line 221
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "adresse2", array()), "html", null, true);
                echo "
                </div>
              </div>
              <div class=\"row-fluid\">
                <div class=\"span8 offset4\">
                  ";
                // line 226
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "codePostal", array()), "html", null, true);
                echo "
                  ";
                // line 227
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "ville", array()), "html", null, true);
                echo "
                </div>
              </div>
              <div class=\"row-fluid\">
                <div class=\"span8 offset4\">
                  ";
                // line 232
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "pays", array()), "html", null, true);
                echo "
                </div>
              </div>
            ";
            }
            // line 236
            echo "
            <div class=\"row-fluid\">
              <div class=\"span4\">
                Téléphone :
              </div>
              <div class=\"span8\">
                ";
            // line 242
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "telephone", array()), "html", null, true);
            echo "
              </div>
            </div>
            ";
            // line 245
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "mobile", array())) {
                // line 246
                echo "              <div class=\"row-fluid\">
                <div class=\"span4\">
                  Téléphone Mr :
                </div>
                <div class=\"span8\">
                  ";
                // line 251
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "mobile", array()), "html", null, true);
                echo "
                </div>
              </div>
            ";
            }
            // line 255
            echo "            ";
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "mobile2", array())) {
                // line 256
                echo "              <div class=\"row-fluid\">
                <div class=\"span4\">
                  Téléphone Mme :
                </div>
                <div class=\"span8\">
                  ";
                // line 261
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "mobile2", array()), "html", null, true);
                echo "
                </div>
              </div>
            ";
            }
            // line 265
            echo "            <div class=\"row-fluid\">
              <div class=\"span4\">
                Email :
              </div>
              <div class=\"span8\">
                ";
            // line 270
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "utilisateur", array()), "email", array()), "html", null, true);
            echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                Appartement Concerné :
              </div>
              <div class=\"span8\">
                ";
            // line 278
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "nomResidence", array()), "html", null, true);
            echo " | ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "reference", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "type", array()))), "html", null, true);
            echo " - ";
            if ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "nbPieces", array())) {
                echo "T";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "nbPieces", array()), "html", null, true);
            }
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "surfaceSol", array()), "html", null, true);
            echo " m<sup>2</sup> à ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "ville", array()), "nom", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "ville", array()), "codePostal", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "ville", array()), "pays", array()), "html", null, true);
            echo " - <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_appartement_editer", array("id" => $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "appartement", array()), "id", array()))), "html", null, true);
            echo "\">Voir la fiche du Bien</a>
              </div>
            </div>

            <br/>
            <div class=\"row-fluid\">
              <strong>Séjour</strong>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                Date d'arrivée :
              </div>
              <div class=\"span8\">
                ";
            // line 292
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "dateDebut", array()), "d/m/Y"), "html", null, true);
            echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                Date de départ :
              </div>
              <div class=\"span8\">
                ";
            // line 300
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "dateFin", array()), "d/m/Y"), "html", null, true);
            echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                Nombre de nuits :
              </div>
              <div class=\"span8\">
                ";
            // line 308
            echo twig_escape_filter($this->env, (isset($context["nb_nuits"]) ? $context["nb_nuits"] : $this->getContext($context, "nb_nuits")), "html", null, true);
            echo "
              </div>
            </div>

            ";
            // line 312
            $context["hebergement"] = (($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "tarif_net", array(), "array") * (1 + ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "commission", array(), "array") / 100))) + $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "menage_fixe", array(), "array"));
            // line 313
            echo "            ";
            $context["assurance"] = (($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "tarif_net", array(), "array") * (1 + ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "commission", array(), "array") / 100))) * ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "assurance", array(), "array") / 100));
            // line 314
            echo "            ";
            $context["forfait_energie"] = ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "EDF", array(), "array") * (isset($context["nb_nuits"]) ? $context["nb_nuits"] : $this->getContext($context, "nb_nuits")));
            // line 315
            echo "            ";
            $context["taxe_sejour"] = (($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "taxe_sejour", array(), "array") * (isset($context["nb_nuits"]) ? $context["nb_nuits"] : $this->getContext($context, "nb_nuits"))) * $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "nbAdulte", array()));
            // line 316
            echo "            ";
            $context["total_ttc"] = $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "tarif_total", array(), "array");
            // line 317
            echo "          ";
            if ($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "promotion", array())) {
                $context["total_ttc"] = ((isset($context["total_ttc"]) ? $context["total_ttc"] : $this->getContext($context, "total_ttc")) + $this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "promotion", array()));
            }
            // line 318
            echo "          ";
            $context["total_options"] = 0;
            // line 319
            echo "          ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "options", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                // line 320
                echo "            ";
                $context["total_options"] = ((isset($context["total_options"]) ? $context["total_options"] : $this->getContext($context, "total_options")) + $this->getAttribute((isset($context["option"]) ? $context["option"] : $this->getContext($context, "option")), "amount", array()));
                // line 321
                echo "          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 322
            echo "          ";
            $context["tarif"] = $this->env->getExtension('my_twig_extension')->getTarif((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")));
            // line 323
            echo "
          <br />

          <div class=\"row-fluid\">
            <div class=\"span1\"></div>
            <div class=\"span10\">
              <table class=\"table table-bordered\">
                <tr class='tableau-facture'>
                  <th>
                    Désignation
                  </th>
                  <th width=\"120\">
                    Montant TTC
                  </th>
                </tr>
                <tr>
                  <td>
                    Location
                  </td>
                  <td>
                    ";
            // line 344
            echo "                    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((isset($context["tarif"]) ? $context["tarif"] : $this->getContext($context, "tarif"))), "html", null, true);
            echo " €
                  </td>
                </tr>
                ";
            // line 347
            if ($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "promotion", array())) {
                // line 348
                echo "                  <tr>
                    <td>
                      Promo/Majoration :
                    </td>
                    <td>
                      ";
                // line 353
                echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "promotion", array())), "html", null, true);
                echo " €
                    </td>
                  </tr>
                ";
            }
            // line 357
            echo "                <tr>
                  <td>
                    <div  class='totalTtc'>
                      Total TTC :
                    </div>
                  </td>
                  <td>
                    ";
            // line 365
            echo "                    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((isset($context["tarif"]) ? $context["tarif"] : $this->getContext($context, "tarif"))), "html", null, true);
            echo " €
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class='dontTva'>
                      Dont TVA :
                    </div>
                  </td>
                  <td>
                    <div class='dontTvaContent'>
                      ";
            // line 376
            $context["dont_tva"] = ((($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "tarif_net", array(), "array") * ($this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "commission", array(), "array") / 100)) * $this->getAttribute($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "calculValues", array()), "tva", array(), "array")) / 100);
            // line 377
            echo "                      ";
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format((isset($context["dont_tva"]) ? $context["dont_tva"] : $this->getContext($context, "dont_tva"))), "html", null, true);
            echo " €
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    Caution
                  </td>
                  <td>
                    ";
            // line 386
            echo twig_escape_filter($this->env, $this->env->getExtension('my_twig_extension')->price_format($this->getAttribute((isset($context["resa"]) ? $context["resa"] : $this->getContext($context, "resa")), "depot", array())), "html", null, true);
            echo " €
                  </td>
                </tr>
              </table>
            </div>
            <div class=\"span1\"></div>
          </div>
          <div class=\"row-fluid\">
            <div class=\"span10 offset1\">
              <strong>Commentaires FR :</strong> <br />
              <textarea name=\"comm\" id=\"Contenu_description2\">
                    Dans le prix total de votre séjour, Class Appart inclut : <br />
                    - Le loyer <br />
                    - La taxe de séjour <br />
                    - L'assurance <br />
                    - Une provision sur electricité et gaz. Les contrats d'electricité et gaz sont déjà en place lors de votre arrivée. Il n'y a
                    pas de formalité à votre charge lorsque vous quittez l'appartement. <br />
                    - Le linge de maison (draps et serviettes) disponible dans l'appartement.<br />
                    - Un kit d'accueil pour démarrer votre séjour. <br />
                    - Une assistance durant les heures d'ouvertures. <br />
              </textarea>
              <br />

              <strong>Commentaires EN :</strong> <br />
              <textarea name=\"comm2\" id=\"Contenu_description2-2\">
                    In the total of your stay, Class Appart includes: <br />
                    - The rent <br />
                    - The tourist tax <br />
                    - The insurance <br />
                    - A reserve on electricity and gas. The electricity  and gases contract’s are already ready for your arrival. There is no formality under your responsibility when you leave the apartment <br />
                    - The linen (sheets and towels) available in the apartment.<br />
                    - A kit of welcome to start your stay <br />
                    - Support during open hours <br />
              </textarea>
              <br />



            </div>


            <div class=\"row\" style=\"margin-left:10px;\">
              <div class=\"span6\">
                <strong>Pied de page FR:</strong> <br />
                <textarea name=\"pied\" id=\"Contenu_description3\">
                            CLASS APPART - 15 Passage Lonjon 34000 MONTPELLIER - Tél : + 33(0)6.10.64.33.08 - Email : <a href=\"mailto:info@class-apart.com\">info@class-appart.com</a><br/>
                            internet : <a href=\"http://class-appart.com/\">class-appart.com</a>
                            SIRET : 790 259 287 000 15
                </textarea>
              </div>
              <div class=\"span6\">
                <strong>Pied de page EN:</strong> <br />
                <textarea name=\"pied2\" id=\"Contenu_description3-3\">
                            CLASS APPART - 15 Passage Lonjon 34000 MONTPELLIER, FRANCE - Phone : + 33(0)6.10.64.33.08 - Email : <a href=\"mailto:info@class-apart.com\">info@class-appart.com</a><br/>
                            internet : <a href=\"http://class-appart.com/\">class-appart.com</a>
                            SIRET : 790 259 287 000 15
                </textarea>
              </div>
            </div>
          </div>
          <br />
        </div>
      </div>

      <div class=\"row-fluid\">
        <div class=\"span9 offset4\">
          <button type=\"submit\" class=\"btn btn-large btn-primary\">
            Générer une facture
          </button>
        </div>
      </div>
    </form>
  ";
        }
        // line 459
        echo "</div><!-- /contenuCentrale1 -->

";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:AdminReservation:facture_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  692 => 459,  603 => 377,  561 => 347,  515 => 319,  475 => 300,  379 => 246,  348 => 227,  298 => 200,  1174 => 722,  1167 => 718,  1161 => 715,  1154 => 710,  1149 => 707,  1141 => 704,  1134 => 702,  1131 => 701,  1129 => 700,  1114 => 695,  1111 => 694,  1109 => 693,  1105 => 692,  1096 => 689,  1092 => 688,  1089 => 687,  1072 => 675,  1064 => 670,  1042 => 651,  1034 => 645,  1026 => 642,  1022 => 640,  1019 => 639,  1017 => 638,  1012 => 637,  1008 => 635,  1004 => 633,  1002 => 632,  996 => 629,  988 => 626,  984 => 624,  979 => 623,  977 => 622,  968 => 616,  962 => 613,  951 => 605,  917 => 586,  915 => 585,  912 => 584,  905 => 580,  897 => 575,  893 => 574,  881 => 568,  873 => 562,  871 => 561,  863 => 555,  853 => 496,  842 => 488,  831 => 480,  812 => 464,  800 => 456,  798 => 455,  787 => 447,  754 => 423,  747 => 418,  739 => 414,  731 => 410,  725 => 407,  718 => 404,  696 => 388,  681 => 378,  675 => 375,  665 => 369,  663 => 368,  651 => 359,  628 => 344,  606 => 334,  602 => 332,  600 => 331,  579 => 315,  572 => 311,  555 => 303,  542 => 295,  532 => 323,  513 => 282,  501 => 315,  499 => 271,  473 => 267,  441 => 245,  438 => 244,  416 => 231,  330 => 182,  289 => 158,  633 => 347,  627 => 291,  621 => 289,  619 => 288,  594 => 272,  576 => 263,  570 => 353,  562 => 255,  558 => 254,  552 => 251,  540 => 245,  508 => 228,  498 => 314,  486 => 308,  480 => 215,  454 => 201,  450 => 200,  410 => 265,  325 => 215,  220 => 89,  407 => 237,  402 => 234,  377 => 245,  313 => 172,  232 => 162,  465 => 197,  457 => 192,  417 => 270,  411 => 176,  408 => 225,  396 => 256,  391 => 168,  388 => 229,  372 => 160,  344 => 226,  332 => 220,  293 => 130,  274 => 149,  231 => 94,  200 => 50,  792 => 418,  788 => 416,  782 => 414,  776 => 439,  774 => 411,  762 => 402,  748 => 394,  742 => 391,  734 => 386,  730 => 385,  724 => 382,  716 => 403,  706 => 373,  698 => 389,  694 => 367,  688 => 364,  680 => 359,  676 => 358,  662 => 350,  658 => 349,  652 => 346,  644 => 341,  640 => 340,  634 => 337,  622 => 331,  616 => 386,  608 => 323,  598 => 273,  580 => 264,  564 => 297,  545 => 287,  541 => 286,  526 => 237,  507 => 317,  488 => 257,  462 => 259,  433 => 226,  424 => 235,  395 => 169,  382 => 199,  376 => 162,  341 => 188,  327 => 167,  320 => 153,  310 => 206,  291 => 118,  278 => 121,  259 => 122,  244 => 113,  448 => 250,  443 => 230,  429 => 259,  406 => 174,  366 => 206,  318 => 211,  282 => 192,  258 => 178,  222 => 158,  120 => 81,  272 => 129,  266 => 147,  226 => 159,  178 => 85,  111 => 44,  393 => 255,  386 => 251,  380 => 163,  362 => 294,  358 => 207,  342 => 139,  340 => 287,  334 => 284,  326 => 278,  319 => 273,  314 => 271,  299 => 163,  265 => 182,  252 => 137,  237 => 128,  194 => 75,  132 => 32,  23 => 3,  97 => 45,  81 => 30,  53 => 24,  654 => 223,  637 => 295,  632 => 206,  625 => 343,  623 => 342,  617 => 195,  612 => 337,  604 => 322,  593 => 327,  591 => 186,  586 => 365,  583 => 183,  578 => 180,  571 => 178,  557 => 177,  534 => 242,  522 => 236,  520 => 320,  504 => 316,  494 => 158,  463 => 145,  446 => 186,  440 => 136,  434 => 134,  431 => 240,  427 => 182,  405 => 210,  401 => 221,  397 => 114,  389 => 215,  381 => 210,  357 => 148,  349 => 193,  339 => 105,  303 => 99,  295 => 119,  287 => 194,  268 => 183,  74 => 22,  470 => 398,  452 => 236,  444 => 197,  435 => 405,  430 => 397,  414 => 241,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 887,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 864,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 825,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 808,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 763,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 732,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 697,  1387 => 694,  1378 => 688,  1374 => 687,  1368 => 684,  1359 => 678,  1355 => 677,  1349 => 674,  1340 => 668,  1336 => 667,  1330 => 664,  1321 => 658,  1317 => 657,  1311 => 654,  1302 => 648,  1298 => 647,  1292 => 644,  1283 => 638,  1279 => 637,  1273 => 634,  1264 => 628,  1260 => 627,  1254 => 624,  1245 => 618,  1241 => 617,  1235 => 614,  1226 => 608,  1222 => 607,  1216 => 604,  1207 => 598,  1203 => 597,  1197 => 594,  1188 => 588,  1184 => 587,  1178 => 584,  1169 => 578,  1165 => 577,  1159 => 574,  1150 => 568,  1146 => 567,  1140 => 564,  1123 => 699,  1119 => 697,  1113 => 546,  1104 => 540,  1100 => 690,  1094 => 536,  1085 => 686,  1081 => 529,  1075 => 526,  1066 => 671,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 599,  933 => 450,  929 => 588,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 569,  874 => 420,  870 => 419,  866 => 418,  860 => 510,  851 => 409,  847 => 408,  841 => 405,  832 => 399,  828 => 398,  822 => 395,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 431,  756 => 359,  752 => 395,  746 => 355,  737 => 413,  733 => 348,  727 => 408,  712 => 376,  708 => 332,  702 => 329,  693 => 323,  689 => 322,  683 => 379,  674 => 241,  670 => 355,  664 => 309,  649 => 297,  645 => 296,  639 => 350,  630 => 287,  626 => 332,  620 => 341,  611 => 277,  607 => 279,  601 => 376,  592 => 267,  588 => 269,  582 => 263,  563 => 348,  554 => 344,  550 => 246,  544 => 246,  535 => 283,  531 => 236,  516 => 233,  512 => 318,  506 => 163,  493 => 312,  478 => 253,  468 => 209,  459 => 144,  455 => 254,  449 => 193,  436 => 192,  428 => 278,  422 => 234,  409 => 117,  403 => 261,  390 => 230,  384 => 165,  351 => 145,  337 => 187,  311 => 118,  296 => 162,  256 => 104,  241 => 168,  215 => 99,  207 => 98,  192 => 82,  186 => 76,  175 => 132,  153 => 50,  118 => 36,  61 => 19,  34 => 8,  65 => 28,  77 => 32,  37 => 7,  190 => 77,  161 => 60,  137 => 40,  126 => 35,  261 => 106,  255 => 135,  247 => 130,  242 => 127,  214 => 87,  211 => 113,  191 => 76,  157 => 37,  145 => 71,  127 => 47,  373 => 111,  367 => 173,  363 => 236,  359 => 171,  354 => 153,  343 => 135,  335 => 104,  328 => 152,  322 => 175,  315 => 170,  309 => 140,  305 => 115,  302 => 267,  290 => 157,  284 => 154,  279 => 129,  271 => 110,  264 => 122,  248 => 172,  236 => 77,  223 => 82,  170 => 44,  110 => 40,  96 => 39,  84 => 32,  472 => 210,  467 => 148,  375 => 152,  371 => 242,  360 => 183,  356 => 232,  353 => 194,  350 => 151,  338 => 138,  336 => 221,  331 => 169,  321 => 130,  316 => 173,  307 => 269,  304 => 268,  301 => 122,  297 => 131,  292 => 110,  286 => 116,  283 => 105,  277 => 104,  275 => 188,  270 => 148,  263 => 123,  257 => 121,  253 => 108,  249 => 135,  245 => 92,  233 => 92,  225 => 89,  216 => 84,  206 => 149,  202 => 78,  198 => 105,  185 => 95,  180 => 92,  177 => 57,  165 => 65,  150 => 53,  124 => 40,  113 => 51,  100 => 74,  58 => 15,  251 => 173,  234 => 163,  213 => 113,  195 => 91,  174 => 84,  167 => 86,  146 => 51,  140 => 54,  128 => 41,  104 => 75,  90 => 23,  83 => 31,  52 => 11,  596 => 225,  590 => 314,  585 => 221,  577 => 357,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 210,  551 => 176,  546 => 207,  543 => 206,  539 => 205,  529 => 322,  525 => 173,  523 => 321,  518 => 193,  514 => 192,  509 => 189,  503 => 266,  500 => 160,  497 => 263,  495 => 313,  492 => 157,  490 => 219,  487 => 213,  484 => 256,  482 => 177,  479 => 176,  477 => 401,  474 => 206,  471 => 173,  469 => 172,  466 => 146,  464 => 292,  461 => 169,  458 => 239,  456 => 167,  451 => 189,  445 => 160,  442 => 159,  439 => 229,  437 => 262,  432 => 191,  426 => 188,  423 => 180,  420 => 219,  418 => 251,  413 => 172,  399 => 237,  394 => 162,  378 => 162,  370 => 204,  368 => 203,  365 => 212,  361 => 199,  347 => 136,  345 => 94,  333 => 131,  329 => 179,  323 => 178,  317 => 272,  312 => 114,  306 => 205,  300 => 110,  294 => 199,  285 => 193,  280 => 152,  276 => 112,  267 => 60,  250 => 101,  239 => 91,  229 => 116,  218 => 157,  212 => 82,  210 => 97,  205 => 83,  188 => 71,  184 => 99,  181 => 74,  169 => 87,  160 => 84,  152 => 63,  148 => 75,  139 => 41,  134 => 45,  114 => 56,  107 => 76,  76 => 27,  70 => 21,  273 => 127,  269 => 94,  254 => 139,  246 => 100,  243 => 88,  240 => 101,  238 => 113,  235 => 95,  230 => 127,  227 => 99,  224 => 112,  221 => 96,  219 => 112,  217 => 84,  208 => 86,  204 => 51,  179 => 71,  159 => 78,  143 => 70,  135 => 37,  131 => 87,  108 => 53,  102 => 26,  71 => 22,  67 => 27,  63 => 20,  59 => 15,  47 => 8,  94 => 25,  89 => 34,  85 => 28,  79 => 26,  75 => 31,  68 => 18,  56 => 25,  50 => 13,  38 => 9,  29 => 4,  87 => 33,  72 => 22,  55 => 14,  21 => 2,  26 => 2,  35 => 6,  31 => 3,  41 => 10,  28 => 2,  201 => 145,  196 => 75,  183 => 72,  171 => 64,  166 => 59,  163 => 79,  156 => 51,  151 => 48,  142 => 46,  138 => 44,  136 => 66,  123 => 45,  121 => 54,  115 => 52,  105 => 32,  101 => 39,  91 => 31,  69 => 30,  66 => 16,  62 => 16,  49 => 23,  98 => 40,  93 => 41,  88 => 30,  78 => 20,  46 => 12,  44 => 18,  32 => 5,  27 => 4,  43 => 7,  40 => 6,  25 => 4,  24 => 2,  172 => 54,  158 => 58,  155 => 77,  129 => 61,  119 => 31,  117 => 36,  20 => 1,  22 => 2,  19 => 1,  209 => 87,  203 => 146,  199 => 79,  193 => 76,  189 => 87,  187 => 96,  182 => 137,  176 => 90,  173 => 89,  168 => 52,  164 => 72,  162 => 85,  154 => 54,  149 => 73,  147 => 48,  144 => 47,  141 => 35,  133 => 48,  130 => 42,  125 => 55,  122 => 82,  116 => 79,  112 => 27,  109 => 47,  106 => 31,  103 => 43,  99 => 44,  95 => 26,  92 => 35,  86 => 36,  82 => 27,  80 => 33,  73 => 19,  64 => 22,  60 => 17,  57 => 22,  54 => 15,  51 => 13,  48 => 14,  45 => 13,  42 => 8,  39 => 16,  36 => 9,  33 => 9,  30 => 3,);
    }
}
