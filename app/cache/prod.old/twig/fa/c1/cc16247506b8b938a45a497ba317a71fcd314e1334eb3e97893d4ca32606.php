<?php

/* PatCompteBundle:Admin:menu.html.twig */
class __TwigTemplate_fac1cc16247506b8b938a45a497ba317a71fcd314e1334eb3e97893d4ca32606 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["menu"]) ? $context["menu"] : $this->getContext($context, "menu"))) {
            // line 2
            echo "  <div class=\"posMenu\">

    <ul class=\"nav \">
      ";
            // line 5
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["menu"]) ? $context["menu"] : $this->getContext($context, "menu")));
            foreach ($context['_seq'] as $context["_key"] => $context["pitem"]) {
                // line 6
                echo "        ";
                if ($this->getAttribute((isset($context["pitem"]) ? $context["pitem"] : $this->getContext($context, "pitem")), "sous_titre", array())) {
                    // line 7
                    echo "          <li class=\"dropdown\"";
                    if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "schemeAndHttpHost", array()) == $this->env->getExtension('routing')->getPath("fos_user_profile_show"))) {
                        echo " class=\"active\" ";
                    }
                    echo ">
            <a href=\"#\"  class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                    // line 8
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pitem"]) ? $context["pitem"] : $this->getContext($context, "pitem")), "titre", array()), "html", null, true);
                    echo "</a>
            <!-- Bloc sous menu -->

            <ul class=\"dropdown-menu\">
              ";
                    // line 12
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["pitem"]) ? $context["pitem"] : $this->getContext($context, "pitem")), "sous_titre", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["sitem"]) {
                        // line 13
                        echo "                <li>
                  <a class=\"msubm\" href=\"";
                        // line 14
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_front_contenu_page", array("url_page" => $this->getAttribute((isset($context["sitem"]) ? $context["sitem"] : $this->getContext($context, "sitem")), "url", array()))), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sitem"]) ? $context["sitem"] : $this->getContext($context, "sitem")), "titre", array()), "html", null, true);
                        echo "</a>
                </li>
              ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sitem'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 17
                    echo "            </ul>
          </li>
        ";
                } else {
                    // line 20
                    echo "          <li><a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_front_contenu_page", array("url_page" => $this->getAttribute((isset($context["pitem"]) ? $context["pitem"] : $this->getContext($context, "pitem")), "url", array()))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pitem"]) ? $context["pitem"] : $this->getContext($context, "pitem")), "titre", array()), "html", null, true);
                    echo "</a>
          ";
                }
                // line 22
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pitem'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "    </ul>

    <ul class=\"nav pull-right\">
      ";
            // line 26
            if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
                // line 27
                echo "        <li class=\"dropdown\" ";
                if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "schemeAndHttpHost", array()) == $this->env->getExtension('routing')->getPath("fos_user_profile_show"))) {
                    echo " class=\"active\" ";
                }
                echo ">
          <button href=\"";
                // line 28
                echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
                echo "\">Quitter</button>
        </li>
      ";
            }
            // line 31
            echo "    </ul>

  </div><!-- /menu -->
";
        }
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:Admin:menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 17,  77 => 24,  37 => 8,  190 => 60,  161 => 47,  137 => 39,  126 => 36,  261 => 138,  255 => 135,  247 => 130,  242 => 127,  214 => 110,  211 => 109,  191 => 98,  157 => 46,  145 => 60,  127 => 49,  373 => 176,  367 => 173,  363 => 172,  359 => 171,  354 => 169,  343 => 161,  335 => 156,  328 => 152,  322 => 149,  315 => 144,  309 => 140,  305 => 138,  302 => 137,  290 => 133,  284 => 132,  279 => 129,  271 => 126,  264 => 122,  248 => 118,  236 => 110,  223 => 113,  170 => 74,  110 => 38,  96 => 32,  84 => 23,  472 => 149,  467 => 148,  375 => 146,  371 => 130,  360 => 99,  356 => 170,  353 => 99,  350 => 98,  338 => 101,  336 => 98,  331 => 95,  321 => 89,  316 => 87,  307 => 85,  304 => 84,  301 => 83,  297 => 70,  292 => 67,  286 => 65,  283 => 64,  277 => 71,  275 => 64,  270 => 61,  263 => 25,  257 => 121,  253 => 119,  249 => 21,  245 => 20,  233 => 23,  225 => 21,  216 => 111,  206 => 22,  202 => 91,  198 => 20,  185 => 13,  180 => 12,  177 => 11,  165 => 154,  150 => 63,  124 => 49,  113 => 83,  100 => 60,  58 => 30,  251 => 123,  234 => 112,  213 => 104,  195 => 93,  174 => 51,  167 => 49,  146 => 58,  140 => 40,  128 => 113,  104 => 31,  90 => 28,  83 => 24,  52 => 14,  596 => 225,  590 => 224,  585 => 221,  577 => 218,  573 => 216,  569 => 214,  560 => 212,  556 => 211,  553 => 210,  551 => 209,  546 => 207,  543 => 206,  539 => 205,  529 => 197,  525 => 195,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 185,  500 => 184,  497 => 183,  495 => 182,  492 => 181,  490 => 180,  487 => 179,  484 => 178,  482 => 177,  479 => 176,  477 => 175,  474 => 174,  471 => 173,  469 => 172,  466 => 171,  464 => 170,  461 => 169,  458 => 168,  456 => 167,  451 => 164,  445 => 160,  442 => 159,  439 => 158,  437 => 157,  432 => 154,  426 => 150,  423 => 149,  420 => 148,  418 => 147,  413 => 144,  399 => 143,  394 => 140,  378 => 137,  370 => 135,  368 => 129,  365 => 128,  361 => 131,  347 => 125,  345 => 94,  333 => 121,  329 => 94,  323 => 117,  317 => 116,  312 => 114,  306 => 113,  300 => 110,  294 => 68,  285 => 105,  280 => 103,  276 => 102,  267 => 60,  250 => 100,  239 => 95,  229 => 116,  218 => 82,  212 => 95,  210 => 23,  205 => 100,  188 => 89,  184 => 73,  181 => 53,  169 => 66,  160 => 59,  152 => 54,  148 => 43,  139 => 48,  134 => 54,  114 => 42,  107 => 33,  76 => 23,  70 => 20,  273 => 127,  269 => 94,  254 => 92,  246 => 117,  243 => 88,  240 => 19,  238 => 113,  235 => 94,  230 => 111,  227 => 81,  224 => 109,  221 => 20,  219 => 112,  217 => 75,  208 => 108,  204 => 72,  179 => 69,  159 => 61,  143 => 59,  135 => 38,  131 => 44,  108 => 36,  102 => 74,  71 => 21,  67 => 20,  63 => 16,  59 => 14,  47 => 12,  94 => 30,  89 => 26,  85 => 19,  79 => 24,  75 => 20,  68 => 14,  56 => 15,  50 => 10,  38 => 6,  29 => 3,  87 => 31,  72 => 22,  55 => 12,  21 => 2,  26 => 5,  35 => 5,  31 => 3,  41 => 7,  28 => 2,  201 => 92,  196 => 101,  183 => 81,  171 => 6,  166 => 61,  163 => 128,  156 => 66,  151 => 44,  142 => 41,  138 => 56,  136 => 56,  123 => 46,  121 => 46,  115 => 33,  105 => 40,  101 => 30,  91 => 27,  69 => 18,  66 => 17,  62 => 18,  49 => 27,  98 => 28,  93 => 9,  88 => 27,  78 => 22,  46 => 26,  44 => 12,  32 => 6,  27 => 4,  43 => 6,  40 => 8,  25 => 3,  24 => 3,  172 => 106,  158 => 67,  155 => 65,  129 => 119,  119 => 34,  117 => 43,  20 => 1,  22 => 2,  19 => 1,  209 => 82,  203 => 106,  199 => 71,  193 => 85,  189 => 84,  187 => 84,  182 => 70,  176 => 64,  173 => 68,  168 => 72,  164 => 71,  162 => 99,  154 => 58,  149 => 51,  147 => 119,  144 => 118,  141 => 117,  133 => 55,  130 => 37,  125 => 38,  122 => 35,  116 => 41,  112 => 32,  109 => 34,  106 => 33,  103 => 30,  99 => 29,  95 => 33,  92 => 25,  86 => 28,  82 => 22,  80 => 23,  73 => 23,  64 => 17,  60 => 15,  57 => 16,  54 => 14,  51 => 13,  48 => 11,  45 => 10,  42 => 10,  39 => 8,  36 => 7,  33 => 7,  30 => 6,);
    }
}
