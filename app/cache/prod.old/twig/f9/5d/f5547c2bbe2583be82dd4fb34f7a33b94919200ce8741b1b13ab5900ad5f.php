<?php

/* PatCompteBundle:AdminLocataire:ajouter_content.html.twig */
class __TwigTemplate_f95df5547c2bbe2583be82dd4fb34f7a33b94919200ce8741b1b13ab5900ad5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "
  <div id=\"contenuCentrale1\">
    <h1>Ajouter un compte locataire</h1>

    <div class=\"clear\"></div>

    ";
        // line 8
        if ((isset($context["message"]) ? $context["message"] : $this->getContext($context, "message"))) {
            // line 9
            echo "      <div class=\"alert alert-success\">
        ";
            // line 10
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
            echo "
      </div>
    ";
        }
        // line 13
        echo "
    ";
        // line 14
        $context["__internal_2ec3f8b437e62b0e886cebd2750f48feb8167753cac789e9316d0d13a16408db"] = $this->env->loadTemplate("MopaBootstrapBundle::flash.html.twig");
        // line 15
        echo "    ";
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "peekAll", array())) > 0)) {
            // line 16
            echo "      <div class=\"row\">
        <div class=\"span12\">
          ";
            // line 18
            echo $context["__internal_2ec3f8b437e62b0e886cebd2750f48feb8167753cac789e9316d0d13a16408db"]->getsession_flash();
            echo "
        </div>
      </div>
    ";
        }
        // line 22
        echo "

    ";
        // line 24
        if (((isset($context["id_locataire"]) ? $context["id_locataire"] : $this->getContext($context, "id_locataire")) > 0)) {
            // line 25
            echo "      <form action=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_locataire_editer", array("id_locataire" => (isset($context["id_locataire"]) ? $context["id_locataire"] : $this->getContext($context, "id_locataire")))), "html", null, true);
            echo "\" ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
            echo " method=\"POST\" class=\"fos_user_registration_register\">
      ";
        } else {
            // line 27
            echo "        <form action=\"";
            echo $this->env->getExtension('routing')->getPath("pat_admin_locataire_ajouter");
            echo "\" ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
            echo " method=\"POST\" class=\"fos_user_registration_register\">
        ";
        }
        // line 29
        echo "

        <div class=\"alert-error\">
          ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
        </div>

        <div class=\"blocForm speciForm\">
          <div class=\"content\">
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "civilite", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "civilite", array()), 'errors');
        echo "
                ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "civilite", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'errors');
        echo "
                ";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'errors');
        echo "
                ";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "societe", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "societe", array()), 'errors');
        echo "
                ";
        // line 73
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "societe", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "siret", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "siret", array()), 'errors');
        echo "
                ";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "siret", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fonction", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 92
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fonction", array()), 'errors');
        echo "
                ";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fonction", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 99
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_naissance", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 102
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_naissance", array()), 'errors');
        echo "
                ";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date_naissance", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 109
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nationalite", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 112
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nationalite", array()), 'errors');
        echo "
                ";
        // line 113
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nationalite", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 119
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "langue_parle", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 122
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "langue_parle", array()), 'errors');
        echo "
                ";
        // line 123
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "langue_parle", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 129
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "telephone", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 132
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "telephone", array()), 'errors');
        echo "
                ";
        // line 133
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "telephone", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 139
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobile", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 142
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobile", array()), 'errors');
        echo "
                ";
        // line 143
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobile", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 149
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobile2", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 152
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobile2", array()), 'errors');
        echo "
                ";
        // line 153
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mobile2", array()), 'widget');
        echo "
              </div>
            </div>

            ";
        // line 167
        echo "            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 169
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 172
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'errors');
        echo "
                ";
        // line 173
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 179
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "second_email", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 182
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "second_email", array()), 'errors');
        echo "
                ";
        // line 183
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "second_email", array()), 'widget');
        echo "
              </div>
            </div>
          </div>
        </div>

        <div class=\"blocForm\">
          <div class=\"title\">
            Adresse
          </div>
          <div class=\"content\">
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 196
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 199
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'errors');
        echo "
                ";
        // line 200
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 206
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 209
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2", array()), 'errors');
        echo "
                ";
        // line 210
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 216
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "code_postal", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 219
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "code_postal", array()), 'errors');
        echo "
                ";
        // line 220
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "code_postal", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 226
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 229
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'errors');
        echo "
                ";
        // line 230
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 236
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 239
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays", array()), 'errors');
        echo "
                ";
        // line 240
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays", array()), 'widget');
        echo "
              </div>
            </div>
          </div>
        </div>

        <div class=\"blocForm\">
          <div class=\"title\">
            Adresse de facturation
          </div>
          <div class=\"content\">
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 253
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse_fact", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 256
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse_fact", array()), 'errors');
        echo "
                ";
        // line 257
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse_fact", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 263
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2_fact", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 266
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2_fact", array()), 'errors');
        echo "
                ";
        // line 267
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse2_fact", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 273
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "code_postal_fact", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 276
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "code_postal_fact", array()), 'errors');
        echo "
                ";
        // line 277
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "code_postal_fact", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 283
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville_fact", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 286
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville_fact", array()), 'errors');
        echo "
                ";
        // line 287
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville_fact", array()), 'widget');
        echo "
              </div>
            </div>

            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 293
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays_fact", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 296
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays_fact", array()), 'errors');
        echo "
                ";
        // line 297
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays_fact", array()), 'widget');
        echo "
              </div>
            </div>
          </div>
        </div>

        <div class=\"blocForm\">
          <div class=\"title\">
            Informations bancaire
          </div>
          <div class=\"content\">
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 310
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_etab", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 313
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_etab", array()), 'errors');
        echo "
                ";
        // line 314
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_etab", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 319
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_code", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 322
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_code", array()), 'errors');
        echo "
                ";
        // line 323
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_code", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 328
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_compte", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 331
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_compte", array()), 'errors');
        echo "
                ";
        // line 332
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_compte", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 337
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_rice", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 340
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_rice", array()), 'errors');
        echo "
                ";
        // line 341
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_rice", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 346
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_domiciliation", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 349
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_domiciliation", array()), 'errors');
        echo "
                ";
        // line 350
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_domiciliation", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 355
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_iban", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 358
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_iban", array()), 'errors');
        echo "
                ";
        // line 359
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_iban", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 364
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_bic", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 367
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_bic", array()), 'errors');
        echo "
                ";
        // line 368
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_bic", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 373
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_agence_resp", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 376
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_agence_resp", array()), 'errors');
        echo "
                ";
        // line 377
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_agence_resp", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 382
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_pays", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 385
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_pays", array()), 'errors');
        echo "
                ";
        // line 386
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_pays", array()), 'widget');
        echo "
              </div>
            </div>
            <div class=\"row-fluid\">
              <div class=\"span4\">
                <strong>";
        // line 391
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_titulaire", array()), 'label');
        echo "</strong>
              </div>
              <div class=\"span8\">
                ";
        // line 394
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_titulaire", array()), 'errors');
        echo "
                ";
        // line 395
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "rib_titulaire", array()), 'widget');
        echo "
              </div>
            </div>
          </div>
        </div>


        ";
        // line 402
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "

        <div class=\"row-fluid\">
          <small>Après validation du formulaire, un mot de passe sera généré et un mail sera envoyé au locataire contenant ses informations de connexion.</small>
        </div>
        <br/>

        <div class=\"row-fluid\">
          <div class=\"span6\">
            ";
        // line 411
        if (((isset($context["id_locataire"]) ? $context["id_locataire"] : $this->getContext($context, "id_locataire")) > 0)) {
            // line 412
            echo "              <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pat_admin_locataire_afficher", array("id_locataire" => (isset($context["id_locataire"]) ? $context["id_locataire"] : $this->getContext($context, "id_locataire")))), "html", null, true);
            echo "\" class=\"btn btn-large\">Revenir</a>
            ";
        } else {
            // line 414
            echo "              <a href=\"";
            echo $this->env->getExtension('routing')->getPath("pat_admin_dashboard");
            echo "\" class=\"btn btn-large\">Annuler</a>
            ";
        }
        // line 416
        echo "          </div>
          <div class=\"span6 right\">
            <input type=\"submit\" class=\"btn btn-primary btn-large\" value=\"";
        // line 418
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
          </div>
        </div>
      </form>
  </div><!-- /contenuCentrale1 -->

";
    }

    public function getTemplateName()
    {
        return "PatCompteBundle:AdminLocataire:ajouter_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  792 => 418,  788 => 416,  782 => 414,  776 => 412,  774 => 411,  762 => 402,  748 => 394,  742 => 391,  734 => 386,  730 => 385,  724 => 382,  716 => 377,  706 => 373,  698 => 368,  694 => 367,  688 => 364,  680 => 359,  676 => 358,  662 => 350,  658 => 349,  652 => 346,  644 => 341,  640 => 340,  634 => 337,  622 => 331,  616 => 328,  608 => 323,  598 => 319,  580 => 310,  564 => 297,  545 => 287,  541 => 286,  526 => 277,  507 => 267,  488 => 257,  462 => 240,  433 => 226,  424 => 220,  395 => 206,  382 => 199,  376 => 196,  341 => 173,  327 => 167,  320 => 153,  310 => 149,  291 => 139,  278 => 132,  259 => 122,  244 => 113,  448 => 267,  443 => 230,  429 => 259,  406 => 242,  366 => 206,  318 => 175,  282 => 133,  258 => 135,  222 => 108,  120 => 49,  272 => 129,  266 => 101,  226 => 87,  178 => 69,  111 => 43,  393 => 305,  386 => 200,  380 => 301,  362 => 294,  358 => 292,  342 => 188,  340 => 287,  334 => 284,  326 => 278,  319 => 273,  314 => 271,  299 => 266,  265 => 144,  252 => 137,  237 => 128,  194 => 103,  132 => 71,  23 => 3,  97 => 35,  81 => 28,  53 => 16,  654 => 223,  637 => 208,  632 => 206,  625 => 200,  623 => 199,  617 => 195,  612 => 192,  604 => 322,  593 => 187,  591 => 186,  586 => 313,  583 => 183,  578 => 180,  571 => 178,  557 => 177,  534 => 175,  522 => 276,  520 => 171,  504 => 162,  494 => 158,  463 => 145,  446 => 138,  440 => 136,  434 => 134,  431 => 260,  427 => 132,  405 => 210,  401 => 209,  397 => 114,  389 => 113,  381 => 112,  357 => 109,  349 => 108,  339 => 105,  303 => 99,  295 => 160,  287 => 97,  268 => 91,  74 => 31,  470 => 398,  452 => 236,  444 => 387,  435 => 405,  430 => 397,  414 => 216,  412 => 387,  2069 => 1116,  2063 => 1113,  1994 => 1029,  1984 => 1022,  1978 => 1019,  1969 => 1013,  1960 => 1007,  1956 => 1006,  1950 => 1003,  1941 => 997,  1937 => 996,  1931 => 993,  1922 => 987,  1918 => 986,  1912 => 983,  1903 => 977,  1899 => 976,  1893 => 973,  1884 => 967,  1880 => 966,  1874 => 963,  1865 => 957,  1861 => 956,  1855 => 953,  1846 => 947,  1842 => 946,  1836 => 943,  1827 => 937,  1823 => 936,  1817 => 933,  1808 => 927,  1804 => 926,  1798 => 923,  1789 => 917,  1785 => 916,  1779 => 913,  1764 => 901,  1760 => 900,  1754 => 897,  1745 => 891,  1741 => 890,  1735 => 887,  1726 => 881,  1722 => 880,  1716 => 877,  1701 => 865,  1697 => 864,  1691 => 861,  1682 => 855,  1678 => 854,  1672 => 851,  1647 => 829,  1643 => 828,  1637 => 825,  1628 => 819,  1624 => 818,  1618 => 815,  1609 => 809,  1605 => 808,  1599 => 805,  1589 => 798,  1585 => 797,  1579 => 794,  1570 => 788,  1566 => 787,  1560 => 784,  1542 => 769,  1538 => 768,  1534 => 767,  1530 => 766,  1524 => 763,  1515 => 757,  1511 => 756,  1507 => 755,  1503 => 754,  1497 => 751,  1490 => 746,  1480 => 744,  1478 => 743,  1474 => 742,  1470 => 741,  1464 => 738,  1455 => 732,  1451 => 731,  1445 => 728,  1435 => 721,  1431 => 720,  1416 => 708,  1412 => 707,  1406 => 704,  1397 => 698,  1393 => 697,  1387 => 694,  1378 => 688,  1374 => 687,  1368 => 684,  1359 => 678,  1355 => 677,  1349 => 674,  1340 => 668,  1336 => 667,  1330 => 664,  1321 => 658,  1317 => 657,  1311 => 654,  1302 => 648,  1298 => 647,  1292 => 644,  1283 => 638,  1279 => 637,  1273 => 634,  1264 => 628,  1260 => 627,  1254 => 624,  1245 => 618,  1241 => 617,  1235 => 614,  1226 => 608,  1222 => 607,  1216 => 604,  1207 => 598,  1203 => 597,  1197 => 594,  1188 => 588,  1184 => 587,  1178 => 584,  1169 => 578,  1165 => 577,  1159 => 574,  1150 => 568,  1146 => 567,  1140 => 564,  1123 => 550,  1119 => 549,  1113 => 546,  1104 => 540,  1100 => 539,  1094 => 536,  1085 => 530,  1081 => 529,  1075 => 526,  1066 => 520,  1062 => 519,  1056 => 516,  1047 => 510,  1043 => 509,  1037 => 506,  1028 => 500,  1024 => 499,  1018 => 496,  1009 => 490,  1005 => 489,  999 => 486,  990 => 480,  986 => 479,  980 => 476,  971 => 470,  967 => 469,  961 => 466,  952 => 460,  948 => 459,  942 => 456,  933 => 450,  929 => 449,  923 => 446,  914 => 440,  910 => 439,  904 => 436,  895 => 430,  891 => 429,  885 => 426,  874 => 420,  870 => 419,  866 => 418,  860 => 415,  851 => 409,  847 => 408,  841 => 405,  832 => 399,  828 => 398,  822 => 395,  813 => 389,  809 => 388,  803 => 385,  794 => 379,  790 => 378,  784 => 375,  775 => 369,  771 => 368,  765 => 365,  756 => 359,  752 => 395,  746 => 355,  737 => 349,  733 => 348,  727 => 345,  712 => 376,  708 => 332,  702 => 329,  693 => 323,  689 => 322,  683 => 319,  674 => 241,  670 => 355,  664 => 309,  649 => 297,  645 => 296,  639 => 293,  630 => 287,  626 => 332,  620 => 283,  611 => 277,  607 => 276,  601 => 189,  592 => 267,  588 => 185,  582 => 263,  563 => 253,  554 => 293,  550 => 246,  544 => 243,  535 => 283,  531 => 236,  516 => 273,  512 => 165,  506 => 163,  493 => 216,  478 => 253,  468 => 203,  459 => 144,  455 => 271,  449 => 193,  436 => 183,  428 => 181,  422 => 395,  409 => 117,  403 => 168,  390 => 224,  384 => 158,  351 => 137,  337 => 172,  311 => 118,  296 => 109,  256 => 138,  241 => 79,  215 => 99,  207 => 66,  192 => 88,  186 => 71,  175 => 93,  153 => 60,  118 => 22,  61 => 24,  34 => 8,  65 => 25,  77 => 21,  37 => 7,  190 => 102,  161 => 56,  137 => 52,  126 => 52,  261 => 86,  255 => 135,  247 => 130,  242 => 127,  214 => 110,  211 => 109,  191 => 98,  157 => 65,  145 => 62,  127 => 47,  373 => 111,  367 => 173,  363 => 172,  359 => 171,  354 => 197,  343 => 135,  335 => 104,  328 => 152,  322 => 176,  315 => 101,  309 => 270,  305 => 115,  302 => 267,  290 => 157,  284 => 154,  279 => 129,  271 => 147,  264 => 122,  248 => 118,  236 => 77,  223 => 82,  170 => 74,  110 => 37,  96 => 44,  84 => 35,  472 => 148,  467 => 148,  375 => 152,  371 => 151,  360 => 183,  356 => 182,  353 => 99,  350 => 179,  338 => 101,  336 => 98,  331 => 169,  321 => 89,  316 => 152,  307 => 269,  304 => 268,  301 => 143,  297 => 142,  292 => 110,  286 => 154,  283 => 105,  277 => 104,  275 => 103,  270 => 144,  263 => 123,  257 => 121,  253 => 119,  249 => 21,  245 => 92,  233 => 88,  225 => 103,  216 => 111,  206 => 93,  202 => 92,  198 => 20,  185 => 13,  180 => 79,  177 => 79,  165 => 62,  150 => 59,  124 => 46,  113 => 61,  100 => 39,  58 => 19,  251 => 93,  234 => 109,  213 => 113,  195 => 93,  174 => 72,  167 => 68,  146 => 56,  140 => 32,  128 => 29,  104 => 35,  90 => 38,  83 => 24,  52 => 14,  596 => 225,  590 => 314,  585 => 221,  577 => 218,  573 => 257,  569 => 256,  560 => 296,  556 => 211,  553 => 210,  551 => 176,  546 => 207,  543 => 206,  539 => 205,  529 => 174,  525 => 173,  523 => 194,  518 => 193,  514 => 192,  509 => 189,  503 => 266,  500 => 160,  497 => 263,  495 => 182,  492 => 157,  490 => 180,  487 => 213,  484 => 256,  482 => 177,  479 => 176,  477 => 401,  474 => 206,  471 => 173,  469 => 172,  466 => 146,  464 => 397,  461 => 169,  458 => 239,  456 => 167,  451 => 164,  445 => 160,  442 => 159,  439 => 229,  437 => 262,  432 => 398,  426 => 396,  423 => 149,  420 => 219,  418 => 251,  413 => 172,  399 => 237,  394 => 162,  378 => 215,  370 => 135,  368 => 295,  365 => 110,  361 => 131,  347 => 136,  345 => 94,  333 => 131,  329 => 130,  323 => 102,  317 => 272,  312 => 114,  306 => 166,  300 => 110,  294 => 68,  285 => 105,  280 => 99,  276 => 98,  267 => 60,  250 => 100,  239 => 91,  229 => 116,  218 => 82,  212 => 100,  210 => 97,  205 => 100,  188 => 59,  184 => 99,  181 => 65,  169 => 70,  160 => 66,  152 => 63,  148 => 38,  139 => 59,  134 => 45,  114 => 21,  107 => 42,  76 => 30,  70 => 25,  273 => 127,  269 => 94,  254 => 92,  246 => 126,  243 => 88,  240 => 112,  238 => 113,  235 => 94,  230 => 111,  227 => 124,  224 => 109,  221 => 102,  219 => 112,  217 => 84,  208 => 108,  204 => 97,  179 => 71,  159 => 61,  143 => 60,  135 => 38,  131 => 64,  108 => 41,  102 => 47,  71 => 18,  67 => 22,  63 => 16,  59 => 33,  47 => 13,  94 => 40,  89 => 36,  85 => 19,  79 => 26,  75 => 31,  68 => 24,  56 => 22,  50 => 15,  38 => 14,  29 => 3,  87 => 32,  72 => 29,  55 => 21,  21 => 2,  26 => 2,  35 => 7,  31 => 3,  41 => 7,  28 => 2,  201 => 80,  196 => 89,  183 => 82,  171 => 92,  166 => 69,  163 => 57,  156 => 66,  151 => 81,  142 => 41,  138 => 55,  136 => 50,  123 => 46,  121 => 44,  115 => 50,  105 => 40,  101 => 39,  91 => 32,  69 => 28,  66 => 23,  62 => 2,  49 => 13,  98 => 38,  93 => 35,  88 => 48,  78 => 27,  46 => 12,  44 => 7,  32 => 4,  27 => 4,  43 => 10,  40 => 8,  25 => 4,  24 => 2,  172 => 106,  158 => 69,  155 => 82,  129 => 119,  119 => 51,  117 => 44,  20 => 1,  22 => 103,  19 => 1,  209 => 82,  203 => 109,  199 => 79,  193 => 76,  189 => 77,  187 => 83,  182 => 70,  176 => 68,  173 => 68,  168 => 73,  164 => 72,  162 => 99,  154 => 59,  149 => 63,  147 => 119,  144 => 56,  141 => 53,  133 => 54,  130 => 53,  125 => 38,  122 => 41,  116 => 39,  112 => 23,  109 => 47,  106 => 48,  103 => 43,  99 => 43,  95 => 33,  92 => 37,  86 => 29,  82 => 11,  80 => 31,  73 => 39,  64 => 22,  60 => 20,  57 => 18,  54 => 15,  51 => 21,  48 => 14,  45 => 13,  42 => 11,  39 => 10,  36 => 9,  33 => 7,  30 => 4,);
    }
}
