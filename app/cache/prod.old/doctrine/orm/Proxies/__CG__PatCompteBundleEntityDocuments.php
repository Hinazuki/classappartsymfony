<?php

namespace Proxies\__CG__\Pat\CompteBundle\Entity;

/**
 * THIS CLASS WAS GENERATED BY THE DOCTRINE ORM. DO NOT EDIT THIS FILE.
 */
class Documents extends \Pat\CompteBundle\Entity\Documents implements \Doctrine\ORM\Proxy\Proxy
{
    private $_entityPersister;
    private $_identifier;
    public $__isInitialized__ = false;
    public function __construct($entityPersister, $identifier)
    {
        $this->_entityPersister = $entityPersister;
        $this->_identifier = $identifier;
    }
    /** @private */
    public function __load()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;

            if (method_exists($this, "__wakeup")) {
                // call this after __isInitialized__to avoid infinite recursion
                // but before loading to emulate what ClassMetadata::newInstance()
                // provides.
                $this->__wakeup();
            }

            if ($this->_entityPersister->load($this->_identifier, $this) === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            unset($this->_entityPersister, $this->_identifier);
        }
    }

    /** @private */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    
    public function __toString()
    {
        $this->__load();
        return parent::__toString();
    }

    public function setInitialValues()
    {
        $this->__load();
        return parent::setInitialValues();
    }

    public function setUpdateValues()
    {
        $this->__load();
        return parent::setUpdateValues();
    }

    public function getFile()
    {
        $this->__load();
        return parent::getFile();
    }

    public function setFile($file)
    {
        $this->__load();
        return parent::setFile($file);
    }

    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int) $this->_identifier["id"];
        }
        $this->__load();
        return parent::getId();
    }

    public function setDateUpdate($dateUpdate)
    {
        $this->__load();
        return parent::setDateUpdate($dateUpdate);
    }

    public function getDateUpdate()
    {
        $this->__load();
        return parent::getDateUpdate();
    }

    public function setFilename($filename)
    {
        $this->__load();
        return parent::setFilename($filename);
    }

    public function getFilename()
    {
        $this->__load();
        return parent::getFilename();
    }

    public function setDocumentName($documentName)
    {
        $this->__load();
        return parent::setDocumentName($documentName);
    }

    public function getDocumentName()
    {
        $this->__load();
        return parent::getDocumentName();
    }

    public function setFileSize($fileSize)
    {
        $this->__load();
        return parent::setFileSize($fileSize);
    }

    public function getFileSize()
    {
        $this->__load();
        return parent::getFileSize();
    }

    public function setVisible($visible)
    {
        $this->__load();
        return parent::setVisible($visible);
    }

    public function getVisible()
    {
        $this->__load();
        return parent::getVisible();
    }

    public function setUser(\Pat\UtilisateurBundle\Entity\Utilisateur $user)
    {
        $this->__load();
        return parent::setUser($user);
    }

    public function getUser()
    {
        $this->__load();
        return parent::getUser();
    }

    public function setAppartement(\Pat\CompteBundle\Entity\Appartement $appartement = NULL)
    {
        $this->__load();
        return parent::setAppartement($appartement);
    }

    public function getAppartement()
    {
        $this->__load();
        return parent::getAppartement();
    }


    public function __sleep()
    {
        return array('__isInitialized__', 'id', 'dateUpdate', 'filename', 'documentName', 'fileSize', 'visible', 'user', 'appartement');
    }

    public function __clone()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;
            $class = $this->_entityPersister->getClassMetadata();
            $original = $this->_entityPersister->load($this->_identifier);
            if ($original === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            foreach ($class->reflFields as $field => $reflProperty) {
                $reflProperty->setValue($this, $reflProperty->getValue($original));
            }
            unset($this->_entityPersister, $this->_identifier);
        }
        
    }
}