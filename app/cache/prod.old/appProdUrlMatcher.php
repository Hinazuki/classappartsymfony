<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        if (0 === strpos($pathinfo, '/css/143482a')) {
            // _assetic_143482a
            if ($pathinfo === '/css/143482a.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '143482a',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_143482a',);
            }

            if (0 === strpos($pathinfo, '/css/143482a_')) {
                // _assetic_143482a_0
                if ($pathinfo === '/css/143482a_mopabootstrapbundle_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '143482a',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_143482a_0',);
                }

                // _assetic_143482a_1
                if ($pathinfo === '/css/143482a_eyecon-bootstrap-datepicker_2.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '143482a',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_143482a_1',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/179a5c4')) {
            // _assetic_179a5c4
            if ($pathinfo === '/js/179a5c4.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_179a5c4',);
            }

            if (0 === strpos($pathinfo, '/js/179a5c4_')) {
                if (0 === strpos($pathinfo, '/js/179a5c4_bootstrap-')) {
                    // _assetic_179a5c4_0
                    if ($pathinfo === '/js/179a5c4_bootstrap-transition_1.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_179a5c4_0',);
                    }

                    // _assetic_179a5c4_1
                    if ($pathinfo === '/js/179a5c4_bootstrap-alert_2.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_179a5c4_1',);
                    }

                    // _assetic_179a5c4_2
                    if ($pathinfo === '/js/179a5c4_bootstrap-modal_3.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_179a5c4_2',);
                    }

                    // _assetic_179a5c4_3
                    if ($pathinfo === '/js/179a5c4_bootstrap-dropdown_4.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_179a5c4_3',);
                    }

                    // _assetic_179a5c4_4
                    if ($pathinfo === '/js/179a5c4_bootstrap-scrollspy_5.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_179a5c4_4',);
                    }

                    if (0 === strpos($pathinfo, '/js/179a5c4_bootstrap-t')) {
                        // _assetic_179a5c4_5
                        if ($pathinfo === '/js/179a5c4_bootstrap-tab_6.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => 5,  '_format' => 'js',  '_route' => '_assetic_179a5c4_5',);
                        }

                        // _assetic_179a5c4_6
                        if ($pathinfo === '/js/179a5c4_bootstrap-tooltip_7.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => 6,  '_format' => 'js',  '_route' => '_assetic_179a5c4_6',);
                        }

                    }

                    // _assetic_179a5c4_7
                    if ($pathinfo === '/js/179a5c4_bootstrap-popover_8.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => 7,  '_format' => 'js',  '_route' => '_assetic_179a5c4_7',);
                    }

                    // _assetic_179a5c4_8
                    if ($pathinfo === '/js/179a5c4_bootstrap-button_9.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => 8,  '_format' => 'js',  '_route' => '_assetic_179a5c4_8',);
                    }

                    if (0 === strpos($pathinfo, '/js/179a5c4_bootstrap-c')) {
                        // _assetic_179a5c4_9
                        if ($pathinfo === '/js/179a5c4_bootstrap-collapse_10.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => 9,  '_format' => 'js',  '_route' => '_assetic_179a5c4_9',);
                        }

                        // _assetic_179a5c4_10
                        if ($pathinfo === '/js/179a5c4_bootstrap-carousel_11.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => 10,  '_format' => 'js',  '_route' => '_assetic_179a5c4_10',);
                        }

                    }

                    // _assetic_179a5c4_11
                    if ($pathinfo === '/js/179a5c4_bootstrap-typeahead_12.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => 11,  '_format' => 'js',  '_route' => '_assetic_179a5c4_11',);
                    }

                }

                if (0 === strpos($pathinfo, '/js/179a5c4_mopabootstrap-')) {
                    // _assetic_179a5c4_12
                    if ($pathinfo === '/js/179a5c4_mopabootstrap-collection_13.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => 12,  '_format' => 'js',  '_route' => '_assetic_179a5c4_12',);
                    }

                    // _assetic_179a5c4_13
                    if ($pathinfo === '/js/179a5c4_mopabootstrap-subnav_14.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '179a5c4',  'pos' => 13,  '_format' => 'js',  '_route' => '_assetic_179a5c4_13',);
                    }

                }

            }

        }

        // PatFrontBundle_homepage
        if (preg_match('#^/(?P<lg>en|fr)?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'PatFrontBundle_homepage')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\ContenuController::indexAction',  'lg' => 'fr',));
        }

        // pat_front_selection
        if (preg_match('#^/(?P<lg>en|fr)/selection/(?P<page>[^/]++)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_selection');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_selection')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\AppartementController::selectionAction',  'lg' => 'fr',));
        }

        // pat_front_appartement_index_pagination
        if (preg_match('#^/(?P<lg>en|fr)/biens/page/(?P<num_page>[^/]++)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_appartement_index_pagination');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_appartement_index_pagination')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\AppartementController::affichePageAppartementBySessionAction',  'lg' => 'fr',));
        }

        // pat_front_appartement_index
        if (preg_match('#^/(?P<lg>en|fr)/biens/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_appartement_index');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_appartement_index')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\AppartementController::indexAction',  'lg' => 'fr',));
        }

        // pat_front_appartement_index_ville
        if (preg_match('#^/(?P<lg>en|fr)/biens/quartier/(?P<idVille>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_appartement_index_ville')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\AppartementController::villeSearchAction',  'lg' => 'fr',));
        }

        // pat_front_appartement_detail
        if (preg_match('#^/(?P<lg>en|fr)/biens/(?P<url_bien>[^/]++)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_appartement_detail');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_appartement_detail')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\AppartementController::detailAction',  'lg' => 'fr',));
        }

        // pat_front_appartement_detail_probleme_dates
        if (preg_match('#^/(?P<lg>en|fr)/biens/(?P<url_bien>[^/]++)/(?P<erreur>[^/]++)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_appartement_detail_probleme_dates');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_appartement_detail_probleme_dates')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\AppartementController::detailAction',  'lg' => 'fr',));
        }

        // pat_front_appartement_ajout_selection_detail_utilisateur
        if (preg_match('#^/(?P<lg>en|fr)/selectionAjout/(?P<id_appartement>[^/]++)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_appartement_ajout_selection_detail_utilisateur');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_appartement_ajout_selection_detail_utilisateur')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\AppartementController::ajoutSelectionDetailUserAction',  'lg' => 'fr',));
        }

        // pat_front_appartement_ajout_selection_recherche_utilisateur
        if (rtrim($pathinfo, '/') === '/selectionAjout') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_appartement_ajout_selection_recherche_utilisateur');
            }

            return array (  '_controller' => 'Pat\\FrontBundle\\Controller\\AppartementController::ajoutSelectionRechercheUserAction',  '_route' => 'pat_front_appartement_ajout_selection_recherche_utilisateur',);
        }

        // pat_front_appartement_supprime_selection_utilisateur
        if (preg_match('#^/(?P<lg>en|fr)/selectionSupprime/(?P<id_appartement>[^/]++)/(?P<retour_page>[^/]++)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_appartement_supprime_selection_utilisateur');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_appartement_supprime_selection_utilisateur')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\AppartementController::suppressionSelectionUserAction',  'lg' => 'fr',));
        }

        // pat_front_appartement_selection_utilisateur
        if (preg_match('#^/(?P<lg>en|fr)/selection/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_appartement_selection_utilisateur');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_appartement_selection_utilisateur')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\AppartementController::SelectionUserAction',  'lg' => 'fr',));
        }

        if (0 === strpos($pathinfo, '/apartment')) {
            // pat_front_busy
            if (preg_match('#^/apartment/(?P<ref>[^/]++)/busy\\.ics$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_busy')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\AppartementController::busyTimesAction',));
            }

            // pat_front_calendar
            if (preg_match('#^/apartment/(?P<ref>[^/]++)/calendar\\.ics$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_calendar')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\AppartementController::calendarAction',));
            }

        }

        // pat_front_contenu_page
        if (preg_match('#^/(?P<lg>en|fr)/page/(?P<url_page>[^/]++)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_contenu_page');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_contenu_page')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\ContenuController::pageAction',  'lg' => 'fr',));
        }

        // pat_front_contenu_message
        if (preg_match('#^/(?P<lg>en|fr)/message/(?P<url_page>[^/]++)/(?P<url_message>[^/]++)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_contenu_message');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_contenu_message')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\ContenuController::messageAction',  'lg' => 'fr',));
        }

        if (0 === strpos($pathinfo, '/recherche')) {
            // pat_front_contenu_ville
            if ($pathinfo === '/recherche/ville') {
                return array (  '_controller' => 'Pat\\FrontBundle\\Controller\\VilleController::listeVilleAction',  '_route' => 'pat_front_contenu_ville',);
            }

            // pat_front_contenu_all_ville
            if ($pathinfo === '/recherche/allville') {
                return array (  '_controller' => 'Pat\\FrontBundle\\Controller\\VilleController::listeAllVilleAction',  '_route' => 'pat_front_contenu_all_ville',);
            }

        }

        // pat_front_formulaire_contact
        if (preg_match('#^/(?P<lg>en|fr)/contact/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_formulaire_contact');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_formulaire_contact')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\FormulaireController::contactAction',  'lg' => 'fr',));
        }

        // pat_front_formulaire_email_success
        if (preg_match('#^/(?P<lg>en|fr)/emailSuccess/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_formulaire_email_success');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_formulaire_email_success')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\FormulaireController::emailSuccessAction',  'lg' => 'fr',));
        }

        // pat_front_formulaire_email_failed
        if (preg_match('#^/(?P<lg>en|fr)/emailFailed/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_formulaire_email_failed');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_formulaire_email_failed')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\FormulaireController::emailFailedAction',  'lg' => 'fr',));
        }

        // pat_front_formulaire_demande_devis
        if (preg_match('#^/(?P<lg>en|fr)/demande\\-de\\-devis/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_formulaire_demande_devis');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_formulaire_demande_devis')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\FormulaireController::demandeDevisAction',  'lg' => 'fr',));
        }

        // pat_front_formulaire_proposer_bien
        if (preg_match('#^/(?P<lg>en|fr)/proposer\\-un\\-bien/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_formulaire_proposer_bien');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_formulaire_proposer_bien')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\FormulaireController::proposerBienAction',  'lg' => 'fr',));
        }

        // pat_front_formulaire_proposer_bien_with_formule
        if (preg_match('#^/(?P<lg>en|fr)/proposer\\-un\\-bien(?:/(?P<formule>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_formulaire_proposer_bien_with_formule')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\FormulaireController::proposerBienAction',  'lg' => 'fr',  'formule' => 'standard',));
        }

        // pat_front_formulaire_demande_devis_valid
        if (preg_match('#^/(?P<lg>en|fr)/demande\\-de\\-devis\\-valide/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_formulaire_demande_devis_valid');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_formulaire_demande_devis_valid')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\FormulaireController::emailDemandeDevisValidAction',  'lg' => 'fr',));
        }

        // pat_front_formulaire_proposer_bien_valid
        if (preg_match('#^/(?P<lg>en|fr)/proposer\\-un\\-bien\\-valide/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_formulaire_proposer_bien_valid');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_formulaire_proposer_bien_valid')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\FormulaireController::emailProposerBienValidAction',  'lg' => 'fr',));
        }

        // pat_front_formulaire_email_demande_devis
        if (preg_match('#^/(?P<lg>en|fr)/email\\-demande\\-de\\-devis/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_formulaire_email_demande_devis');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_formulaire_email_demande_devis')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\FormulaireController::email_demandeDevisAction',  'lg' => 'fr',));
        }

        // pat_front_formulaire_email_proposer_bien
        if (preg_match('#^/(?P<lg>en|fr)/email\\-proposer\\-un\\-bien/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pat_front_formulaire_email_proposer_bien');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_formulaire_email_proposer_bien')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\FormulaireController::email_proposerBienAction',  'lg' => 'fr',));
        }

        // pat_front_reservation_selection_date
        if (preg_match('#^/(?P<lg>en|fr)/reservation/selectiondate$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_reservation_selection_date')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\ReservationController::selectionDateResaAction',  'lg' => 'fr',));
        }

        // pat_front_reservation_calcul_montant
        if (preg_match('#^/(?P<lg>en|fr)/reservation/montant$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_reservation_calcul_montant')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\ReservationController::calculMontantAction',  'lg' => 'fr',));
        }

        // pat_front_reservation_actualise_nbpersonne
        if (preg_match('#^/(?P<lg>en|fr)/reservation/nbpersonne$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_reservation_actualise_nbpersonne')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\ReservationController::actualiseNbPersonneAction',  'lg' => 'fr',));
        }

        // pat_front_reservation_options
        if (preg_match('#^/(?P<lg>en|fr)/reservation/options$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_reservation_options')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\ReservationController::optionsAction',  'lg' => 'fr',));
        }

        // pat_front_reservation_actualise_options
        if (preg_match('#^/(?P<lg>en|fr)/reservation/maj_options$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_reservation_actualise_options')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\ReservationController::actualiseOptionsAction',  'lg' => 'fr',));
        }

        // pat_front_reservation_inscription
        if (preg_match('#^/(?P<lg>en|fr)/reservation/inscription$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_reservation_inscription')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\ReservationController::inscriptionAction',  'lg' => 'fr',));
        }

        // pat_front_reservation_recapitulatif
        if (preg_match('#^/(?P<lg>en|fr)/reservation/recapitulatif$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_reservation_recapitulatif')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\ReservationController::recapitulatifAction',  'lg' => 'fr',));
        }

        // pat_front_reservation_paiement
        if (preg_match('#^/(?P<lg>en|fr)/reservation/paiement(?:/(?P<resa_id>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_reservation_paiement')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\ReservationController::paiementAction',  'lg' => 'fr',  'resa_id' => NULL,));
        }

        // pat_front_reservation_confirmation
        if (preg_match('#^/(?P<lg>en|fr)/reservation/confirmation(?:/(?P<status>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_reservation_confirmation')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\ReservationController::confirmationAction',  'lg' => 'fr',  'status' => NULL,));
        }

        // pat_front_reservation_payment_result
        if ($pathinfo === '/reservation/payment_result') {
            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\ReservationController::paymentResultAction',  '_route' => 'pat_front_reservation_payment_result',);
        }

        // pat_payment_result
        if ($pathinfo === '/payment/payment_result') {
            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\PaymentController::paymentResultAction',  '_route' => 'pat_payment_result',);
        }

        // pat_front_inscription_resident
        if (preg_match('#^/(?P<lg>en|fr)/inscription$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_inscription_resident')), array (  '_controller' => 'Pat\\UtilisateurBundle\\Controller\\RegistrationController::registerResidentAction',  'lg' => 'fr',));
        }

        if (0 === strpos($pathinfo, '/f')) {
            // pat_front_documents_filedownload
            if (0 === strpos($pathinfo, '/filedownload') && preg_match('#^/filedownload/(?P<fileid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_front_documents_filedownload')), array (  '_controller' => 'Pat\\FrontBundle\\Controller\\DocumentsController::DownloadFileAction',));
            }

            if (0 === strpos($pathinfo, '/fr/mon-compte')) {
                // pat_dispatch
                if ($pathinfo === '/fr/mon-compte/dispatch') {
                    return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminController::dispatchAction',  '_route' => 'pat_dispatch',);
                }

                if (0 === strpos($pathinfo, '/fr/mon-compte/admin/option')) {
                    // option
                    if (preg_match('#^/fr/mon\\-compte/admin/option/(?P<reservation_id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'option')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\OptionController::indexAction',));
                    }

                    // option_new
                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/option/new') && preg_match('#^/fr/mon\\-compte/admin/option/new/(?P<reservation_id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'option_new')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\OptionController::newAction',));
                    }

                    // option_edit
                    if (preg_match('#^/fr/mon\\-compte/admin/option/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'option_edit')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\OptionController::editAction',));
                    }

                    // option_update
                    if (preg_match('#^/fr/mon\\-compte/admin/option/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_option_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'option_update')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\OptionController::updateAction',));
                    }
                    not_option_update:

                    // option_delete
                    if (preg_match('#^/fr/mon\\-compte/admin/option/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'option_delete')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\OptionController::deleteAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/fr/mon-compte/documents')) {
                    // documents_list
                    if ($pathinfo === '/fr/mon-compte/documents/list') {
                        return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\DocumentsController::ListFileAction',  '_route' => 'documents_list',);
                    }

                    // documents_add_file
                    if ($pathinfo === '/fr/mon-compte/documents/ajouter') {
                        return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\DocumentsController::AddFileAction',  '_route' => 'documents_add_file',);
                    }

                    // documents_edit_file
                    if (0 === strpos($pathinfo, '/fr/mon-compte/documents/modifier') && preg_match('#^/fr/mon\\-compte/documents/modifier/(?P<fileid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'documents_edit_file')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\DocumentsController::EditFileAction',));
                    }

                    // documents_delete_file
                    if (0 === strpos($pathinfo, '/fr/mon-compte/documents/supprimer') && preg_match('#^/fr/mon\\-compte/documents/supprimer/(?P<fileid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'documents_delete_file')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\DocumentsController::DeleteFileAction',));
                    }

                    // documents_filedownload
                    if (0 === strpos($pathinfo, '/fr/mon-compte/documents/filedownload') && preg_match('#^/fr/mon\\-compte/documents/filedownload/(?P<fileid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'documents_filedownload')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\DocumentsController::DownloadFileAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/fr/mon-compte/admin/proprietaire/documents')) {
                    // documentsAdmin_list
                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/proprietaire/documents/list') && preg_match('#^/fr/mon\\-compte/admin/proprietaire/documents/list/(?P<user_id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'documentsAdmin_list')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminDocumentsController::ListFileAction',));
                    }

                    // documentsAdmin_add_file
                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/proprietaire/documents/ajouter') && preg_match('#^/fr/mon\\-compte/admin/proprietaire/documents/ajouter/(?P<user_id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'documentsAdmin_add_file')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminDocumentsController::AddFileAction',));
                    }

                    // documentsAdmin_edit_file
                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/proprietaire/documents/modifier') && preg_match('#^/fr/mon\\-compte/admin/proprietaire/documents/modifier/(?P<user_id>[^/]++)/(?P<fileid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'documentsAdmin_edit_file')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminDocumentsController::EditFileAction',));
                    }

                    // documentsAdmin_delete_file
                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/proprietaire/documents/supprimer') && preg_match('#^/fr/mon\\-compte/admin/proprietaire/documents/supprimer/(?P<user_id>[^/]++)/(?P<fileid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'documentsAdmin_delete_file')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminDocumentsController::DeleteFileAction',));
                    }

                    // documentsAdmin_filedownload
                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/proprietaire/documents/filedownload') && preg_match('#^/fr/mon\\-compte/admin/proprietaire/documents/filedownload/(?P<user_id>[^/]++)/(?P<fileid>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'documentsAdmin_filedownload')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminDocumentsController::DownloadFileAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/fr/mon-compte/mes-reservations')) {
                    // pat_loc_reservation_index
                    if (rtrim($pathinfo, '/') === '/fr/mon-compte/mes-reservations') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'pat_loc_reservation_index');
                        }

                        return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\ReservationController::indexLocataireAction',  '_route' => 'pat_loc_reservation_index',);
                    }

                    // pat_loc_reservation_show
                    if (preg_match('#^/fr/mon\\-compte/mes\\-reservations/(?P<id_reservation>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_loc_reservation_show')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\ReservationController::detailLocataireAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/fr/mon-compte/appartement')) {
                    // pat_appartement_index
                    if (rtrim($pathinfo, '/') === '/fr/mon-compte/appartement') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'pat_appartement_index');
                        }

                        return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AppartementController::indexAction',  '_route' => 'pat_appartement_index',);
                    }

                    // pat_appartement_index_pagination
                    if (0 === strpos($pathinfo, '/fr/mon-compte/appartement/page') && preg_match('#^/fr/mon\\-compte/appartement/page/(?P<num_page>[^/]++)/?$#s', $pathinfo, $matches)) {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'pat_appartement_index_pagination');
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_appartement_index_pagination')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AppartementController::paginationBienAction',));
                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/appartement/modifier')) {
                        // pat_appartement_editer
                        if (preg_match('#^/fr/mon\\-compte/appartement/modifier/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_appartement_editer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AppartementController::ajouterAction',));
                        }

                        // pat_appartement_editer_error
                        if (preg_match('#^/fr/mon\\-compte/appartement/modifier/(?P<id>[^/]++)/erreur/(?P<erreur>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_appartement_editer_error')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AppartementController::ajouterAction',));
                        }

                    }

                    // pat_piece_index
                    if (preg_match('#^/fr/mon\\-compte/appartement/(?P<id>[^/]++)/pieces$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_piece_index')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\PieceController::indexAction',));
                    }

                    // pat_piece_ajouter
                    if (preg_match('#^/fr/mon\\-compte/appartement/(?P<id>[^/]++)/pieces/ajouter/(?P<categorie_id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_piece_ajouter')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\PieceController::ajouterAction',));
                    }

                    // pat_piece_editer
                    if (preg_match('#^/fr/mon\\-compte/appartement/(?P<id>[^/]++)/pieces/modifier/(?P<id_piece>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_piece_editer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\PieceController::ajouterAction',));
                    }

                    // pat_piece_supprimer
                    if (preg_match('#^/fr/mon\\-compte/appartement/(?P<id>[^/]++)/pieces/supprimer/(?P<id_piece>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_piece_supprimer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\PieceController::supprimerAction',));
                    }

                    // pat_media_tri
                    if (preg_match('#^/fr/mon\\-compte/appartement/(?P<id>[^/]++)/medias/tri$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_pat_media_tri;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_media_tri')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\MediaController::triAction',));
                    }
                    not_pat_media_tri:

                }

                // pat_tarif_ajouter
                if (0 === strpos($pathinfo, '/fr/mon-compte/tarif') && preg_match('#^/fr/mon\\-compte/tarif/(?P<id_bien>[^/]++)/?$#s', $pathinfo, $matches)) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'pat_tarif_ajouter');
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_tarif_ajouter')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\TarifController::gestionByAppAction',));
                }

                // pat_calendrier_index
                if (rtrim($pathinfo, '/') === '/fr/mon-compte/calendrier') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'pat_calendrier_index');
                    }

                    return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\CalendrierController::indexAction',  '_route' => 'pat_calendrier_index',);
                }

                // pat_calendrier_affiche
                if (0 === strpos($pathinfo, '/fr/mon-compte/disponibilite') && preg_match('#^/fr/mon\\-compte/disponibilite/(?P<id_bien>[^/]++)/(?P<annee>[^/]++)/?$#s', $pathinfo, $matches)) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'pat_calendrier_affiche');
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_calendrier_affiche')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\CalendrierController::DisplayCalByAppAction',));
                }

                if (0 === strpos($pathinfo, '/fr/mon-compte/calendrier')) {
                    // pat_calendrier_ajout_by_app
                    if (preg_match('#^/fr/mon\\-compte/calendrier/(?P<id_bien>[^/]++)/bloquer/?$#s', $pathinfo, $matches)) {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'pat_calendrier_ajout_by_app');
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_calendrier_ajout_by_app')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\CalendrierController::AjouterCalByAppAction',));
                    }

                    // pat_calendrier_modifier_by_app
                    if (preg_match('#^/fr/mon\\-compte/calendrier/(?P<id_bien>[^/]++)/bloquer/(?P<id_cal>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_calendrier_modifier_by_app')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\CalendrierController::AjouterCalByAppAction',));
                    }

                    // pat_calendrier_supprimer_by_app
                    if (preg_match('#^/fr/mon\\-compte/calendrier/(?P<id_bien>[^/]++)/supprimer/(?P<id_cal>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_calendrier_supprimer_by_app')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\CalendrierController::supprimerCalByAppAction',));
                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/calendrier/bloquer')) {
                        // pat_calendrier_ajouter
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/calendrier/bloquer') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_calendrier_ajouter');
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\CalendrierController::ajouterAction',  '_route' => 'pat_calendrier_ajouter',);
                        }

                        // pat_calendrier_modifier
                        if (preg_match('#^/fr/mon\\-compte/calendrier/bloquer/(?P<id_cal>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_calendrier_modifier')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\CalendrierController::ajouterAction',));
                        }

                    }

                    // pat_calendrier_supprimer
                    if (0 === strpos($pathinfo, '/fr/mon-compte/calendrier/supprimer') && preg_match('#^/fr/mon\\-compte/calendrier/supprimer/(?P<id_cal>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_calendrier_supprimer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\CalendrierController::supprimerAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/fr/mon-compte/reservation')) {
                    // pat_reservation_index
                    if (rtrim($pathinfo, '/') === '/fr/mon-compte/reservation') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'pat_reservation_index');
                        }

                        return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\ReservationController::indexAction',  '_route' => 'pat_reservation_index',);
                    }

                    // pat_reservation_show
                    if (preg_match('#^/fr/mon\\-compte/reservation/(?P<id_reservation>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_reservation_show')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\ReservationController::detailAction',));
                    }

                    // pat_reservation_validation
                    if (0 === strpos($pathinfo, '/fr/mon-compte/reservation/validation') && preg_match('#^/fr/mon\\-compte/reservation/validation/(?P<ref_reservation>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_reservation_validation')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\ReservationController::validationAction',));
                    }

                    // pat_reservation_paiement
                    if (0 === strpos($pathinfo, '/fr/mon-compte/reservation/paiement') && preg_match('#^/fr/mon\\-compte/reservation/paiement/(?P<ref_reservation>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_reservation_paiement')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\ReservationController::paiementAction',));
                    }

                    // pat_reservation_valid_prop
                    if (preg_match('#^/fr/mon\\-compte/reservation/(?P<ref>[^/]++)/(?P<statut>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_reservation_valid_prop')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\ReservationController::validProprioAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/fr/mon-compte/payment')) {
                    // pat_payment
                    if (preg_match('#^/fr/mon\\-compte/payment/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_payment')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\PaymentController::paymentAction',));
                    }

                    // pat_payment_confirmation
                    if (0 === strpos($pathinfo, '/fr/mon-compte/payment/confirmation') && preg_match('#^/fr/mon\\-compte/payment/confirmation(?:/(?P<status>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_payment_confirmation')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\PaymentController::confirmationAction',  'status' => NULL,));
                    }

                }

                if (0 === strpos($pathinfo, '/fr/mon-compte/log')) {
                    if (0 === strpos($pathinfo, '/fr/mon-compte/login')) {
                        // fos_user_security_login
                        if ($pathinfo === '/fr/mon-compte/login') {
                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                        }

                        // fos_user_security_check
                        if ($pathinfo === '/fr/mon-compte/login_check') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_fos_user_security_check;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                        }
                        not_fos_user_security_check:

                    }

                    // fos_user_security_logout
                    if ($pathinfo === '/fr/mon-compte/logout') {
                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
                    }

                }

                if (0 === strpos($pathinfo, '/fr/mon-compte/profile')) {
                    // fos_user_profile_show
                    if (rtrim($pathinfo, '/') === '/fr/mon-compte/profile') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_profile_show;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                        }

                        return array (  '_controller' => 'Pat\\UtilisateurBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
                    }
                    not_fos_user_profile_show:

                    // fos_user_profile_edit
                    if ($pathinfo === '/fr/mon-compte/profile/edit') {
                        return array (  '_controller' => 'Pat\\UtilisateurBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
                    }

                }

                if (0 === strpos($pathinfo, '/fr/mon-compte/re')) {
                    if (0 === strpos($pathinfo, '/fr/mon-compte/register')) {
                        // fos_user_registration_register
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/register') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                            }

                            return array (  '_controller' => 'Pat\\UtilisateurBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                        }

                        if (0 === strpos($pathinfo, '/fr/mon-compte/register/c')) {
                            // fos_user_registration_check_email
                            if ($pathinfo === '/fr/mon-compte/register/check-email') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_fos_user_registration_check_email;
                                }

                                return array (  '_controller' => 'Pat\\UtilisateurBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                            }
                            not_fos_user_registration_check_email:

                            if (0 === strpos($pathinfo, '/fr/mon-compte/register/confirm')) {
                                // fos_user_registration_confirm
                                if (preg_match('#^/fr/mon\\-compte/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                        $allow = array_merge($allow, array('GET', 'HEAD'));
                                        goto not_fos_user_registration_confirm;
                                    }

                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'Pat\\UtilisateurBundle\\Controller\\RegistrationController::confirmAction',));
                                }
                                not_fos_user_registration_confirm:

                                // fos_user_registration_confirmed
                                if ($pathinfo === '/fr/mon-compte/register/confirmed') {
                                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                        $allow = array_merge($allow, array('GET', 'HEAD'));
                                        goto not_fos_user_registration_confirmed;
                                    }

                                    return array (  '_controller' => 'Pat\\UtilisateurBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                                }
                                not_fos_user_registration_confirmed:

                            }

                        }

                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/resetting')) {
                        // fos_user_resetting_request
                        if ($pathinfo === '/fr/mon-compte/resetting/request') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_resetting_request;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                        }
                        not_fos_user_resetting_request:

                        // fos_user_resetting_send_email
                        if ($pathinfo === '/fr/mon-compte/resetting/send-email') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_fos_user_resetting_send_email;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                        }
                        not_fos_user_resetting_send_email:

                        // fos_user_resetting_check_email
                        if ($pathinfo === '/fr/mon-compte/resetting/check-email') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_resetting_check_email;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                        }
                        not_fos_user_resetting_check_email:

                        // fos_user_resetting_reset
                        if (0 === strpos($pathinfo, '/fr/mon-compte/resetting/reset') && preg_match('#^/fr/mon\\-compte/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                                goto not_fos_user_resetting_reset;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                        }
                        not_fos_user_resetting_reset:

                    }

                }

                // fos_user_change_password
                if ($pathinfo === '/fr/mon-compte/change-password/change-password') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_change_password;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
                }
                not_fos_user_change_password:

                if (0 === strpos($pathinfo, '/fr/mon-compte/admin')) {
                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/site-tiers')) {
                        // pat_admin_site_tiers
                        if ($pathinfo === '/fr/mon-compte/admin/site-tiers') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminSiteTiersController::listSiteTiersAction',  '_route' => 'pat_admin_site_tiers',);
                        }

                        // pat_admin_site_tiers_ajouter
                        if ($pathinfo === '/fr/mon-compte/admin/site-tiers/ajouter') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminSiteTiersController::ajouterEditerSiteTiersAction',  '_route' => 'pat_admin_site_tiers_ajouter',);
                        }

                        // pat_admin_site_tiers_editer
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/site-tiers/editer') && preg_match('#^/fr/mon\\-compte/admin/site\\-tiers/editer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_site_tiers_editer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminSiteTiersController::ajouterEditerSiteTiersAction',));
                        }

                        // pat_admin_site_tiers_supprimer
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/site-tiers/supprimer') && preg_match('#^/fr/mon\\-compte/admin/site\\-tiers/supprimer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_site_tiers_supprimer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminSiteTiersController::supprimerSiteTiersAction',));
                        }

                    }

                    // pat_admin_dashboard
                    if ($pathinfo === '/fr/mon-compte/admin/dashboard') {
                        return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminController::indexAction',  '_route' => 'pat_admin_dashboard',);
                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/planning')) {
                        // pat_admin_planning
                        if ($pathinfo === '/fr/mon-compte/admin/planning') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminController::planningAction',  '_route' => 'pat_admin_planning',);
                        }

                        // pat_admin_planning_ajax
                        if ($pathinfo === '/fr/mon-compte/admin/planning_ajax') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_pat_admin_planning_ajax;
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminController::planningAjaxAction',  '_route' => 'pat_admin_planning_ajax',);
                        }
                        not_pat_admin_planning_ajax:

                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/appartement')) {
                        // pat_admin_appartement_index
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/appartement') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_appartement_index');
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminAppartementController::indexAction',  '_route' => 'pat_admin_appartement_index',);
                        }

                        // pat_admin_appartement_derniers_ajouts
                        if ($pathinfo === '/fr/mon-compte/admin/appartement/derniers_ajouts') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminAppartementController::derniersAjoutsAction',  '_route' => 'pat_admin_appartement_derniers_ajouts',);
                        }

                    }

                    // pat_admin_appartement_detail
                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/detail/bien') && preg_match('#^/fr/mon\\-compte/admin/detail/bien/(?P<id>[^/]++)/?$#s', $pathinfo, $matches)) {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'pat_admin_appartement_detail');
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_appartement_detail')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminAppartementController::detailAction',));
                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/appartement')) {
                        // pat_admin_appartement_ajouter
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/appartement/ajouter') && preg_match('#^/fr/mon\\-compte/admin/appartement/ajouter(?:/(?P<id_proprio>[^/]++))?$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_appartement_ajouter')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminAppartementController::ajouterAction',  'id_proprio' => NULL,));
                        }

                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/appartement/modifier')) {
                            // pat_admin_appartement_editer
                            if (preg_match('#^/fr/mon\\-compte/admin/appartement/modifier/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_appartement_editer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminAppartementController::ajouterAction',));
                            }

                            // pat_admin_appartement_editer_error
                            if (preg_match('#^/fr/mon\\-compte/admin/appartement/modifier/(?P<id>[^/]++)/erreur/(?P<erreur>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_appartement_editer_error')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminAppartementController::ajouterAction',));
                            }

                        }

                        // pat_admin_appartement_supprimer
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/appartement/supprimer') && preg_match('#^/fr/mon\\-compte/admin/appartement/supprimer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_appartement_supprimer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminAppartementController::supprimerAction',));
                        }

                        // pat_admin_appartement_validation
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/appartement/validation') && preg_match('#^/fr/mon\\-compte/admin/appartement/validation/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_appartement_validation')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminAppartementController::validerAction',));
                        }

                        // pat_admin_appartement_dupliquer
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/appartement/dupliquer') && preg_match('#^/fr/mon\\-compte/admin/appartement/dupliquer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_appartement_dupliquer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminAppartementController::dupliquerAction',));
                        }

                        // pat_admin_appartement_modif_proprietaire
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/modif/proprietaire$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_appartement_modif_proprietaire')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminAppartementController::modifProprietaireAction',));
                        }

                        // pat_admin_appartement_select_proprietaire
                        if ($pathinfo === '/fr/mon-compte/admin/appartement/select/proprietaire') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminAppartementController::selectProprietaireByAjaxAction',  '_route' => 'pat_admin_appartement_select_proprietaire',);
                        }

                        // pat_admin_appartement_association_proprietaire
                        if ($pathinfo === '/fr/mon-compte/admin/appartement/association/proprietaire') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminAppartementController::associationAppartementProprietaireAction',  '_route' => 'pat_admin_appartement_association_proprietaire',);
                        }

                        // pat_admin_piece_index
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/pieces$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_piece_index')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminPieceController::indexAction',));
                        }

                        // pat_admin_piece_ajouter
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/pieces/ajouter/(?P<categorie_id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_piece_ajouter')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminPieceController::ajouterAction',));
                        }

                        // pat_admin_piece_editer
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/pieces/modifier/(?P<id_piece>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_piece_editer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminPieceController::ajouterAction',));
                        }

                        // pat_admin_piece_supprimer
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/pieces/supprimer/(?P<id_piece>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_piece_supprimer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminPieceController::supprimerAction',));
                        }

                        // pat_admin_media_index
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/medias$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_media_index')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminMediaController::indexAction',));
                        }

                        // pat_admin_media_ajouter
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/medias/ajouter$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_media_ajouter')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminMediaController::ajouterAction',));
                        }

                        // pat_admin_media_supprimer
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/medias/supprimer/(?P<id_media>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_media_supprimer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminMediaController::supprimerAction',));
                        }

                        // pat_admin_media_tri
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/medias/tri$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_pat_admin_media_tri;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_media_tri')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminMediaController::triAction',));
                        }
                        not_pat_admin_media_tri:

                        // pat_admin_promotion_index
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/promotions$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_promotion_index')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminPromotionController::indexAction',));
                        }

                        // pat_admin_promotion_supprimer
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/promotion/(?P<promotionId>[^/]++)/supprimer$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_promotion_supprimer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminPromotionController::supprimerAction',));
                        }

                        // pat_admin_promotion_activer
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/promotion/(?P<promotionId>[^/]++)/activer$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_promotion_activer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminPromotionController::activerDesactiverAction',));
                        }

                        // pat_admin_promotion_desactiver
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/promotion/(?P<promotionId>[^/]++)/desactiver$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_promotion_desactiver')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminPromotionController::activerDesactiverAction',));
                        }

                        // pat_admin_calendrier_index
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/calendrier/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_calendrier_index');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_calendrier_index')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminCalendrierController::indexAction',));
                        }

                        // pat_admin_calendrier_ajouter
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/calendrier/bloquer$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_calendrier_ajouter')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminCalendrierController::ajouterAction',));
                        }

                        // pat_admin_calendrier_modifier
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/calendrier/bloquer/(?P<id_cal>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_calendrier_modifier')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminCalendrierController::ajouterAction',));
                        }

                        // pat_admin_calendrier_supprimer
                        if (preg_match('#^/fr/mon\\-compte/admin/appartement/(?P<id>[^/]++)/calendrier/supprimer/(?P<id_cal>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_calendrier_supprimer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminCalendrierController::supprimerAction',));
                        }

                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/reservation')) {
                        // pat_admin_reservation_index
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/reservation') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_reservation_index');
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::indexAction',  '_route' => 'pat_admin_reservation_index',);
                        }

                        // pat_reservation_text_options
                        if ($pathinfo === '/fr/mon-compte/admin/reservation/modifier/texte-options') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::editTexteOptionsAction',  '_route' => 'pat_reservation_text_options',);
                        }

                        // pat_admin_reservation_ajouter
                        if ($pathinfo === '/fr/mon-compte/admin/reservation/ajouter') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::ajouterAction',  '_route' => 'pat_admin_reservation_ajouter',);
                        }

                        // pat_admin_reservation_modifier
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/reservation/modifier') && preg_match('#^/fr/mon\\-compte/admin/reservation/modifier/(?P<id_reservation>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_reservation_modifier')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::editAction',));
                        }

                        // pat_admin_reservation_show
                        if (preg_match('#^/fr/mon\\-compte/admin/reservation/(?P<id_reservation>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_reservation_show')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::detailAction',));
                        }

                        // pat_admin_reservation_facture_generation
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/reservation/facture') && preg_match('#^/fr/mon\\-compte/admin/reservation/facture/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_reservation_facture_generation')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::factureAction',));
                        }

                        // pat_admin_reservation_download_facture
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/reservation/downloadfacture') && preg_match('#^/fr/mon\\-compte/admin/reservation/downloadfacture/(?P<language>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_reservation_download_facture')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::downloadFacturePdfAction',));
                        }

                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/reservation/facture')) {
                            // pat_admin_reservation_download_facture_new
                            if (preg_match('#^/fr/mon\\-compte/admin/reservation/facture/(?P<id>[^/]++)/download$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_reservation_download_facture_new')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::downloadFactureNewAction',));
                            }

                            // pat_admin_reservation_download_avoir_new
                            if (preg_match('#^/fr/mon\\-compte/admin/reservation/facture/(?P<id>[^/]++)/avoir/download$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_reservation_download_avoir_new')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::downloadAvoirNewAction',));
                            }

                            // pat_admin_reservation_send_facture
                            if (preg_match('#^/fr/mon\\-compte/admin/reservation/facture/(?P<id>[^/]++)/envoyer$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_reservation_send_facture')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::sendFactureAction',));
                            }

                        }

                        // pat_admin_reservation_send_avoir
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/reservation/avoir') && preg_match('#^/fr/mon\\-compte/admin/reservation/avoir/(?P<id>[^/]++)/envoyer$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_reservation_send_avoir')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::sendAvoirAction',));
                        }

                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/reservation/facture')) {
                            // pat_admin_reservation_facture_generation_pdf
                            if (0 === strpos($pathinfo, '/fr/mon-compte/admin/reservation/facture/pdf') && preg_match('#^/fr/mon\\-compte/admin/reservation/facture/pdf/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_reservation_facture_generation_pdf')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::facturePdfAction',));
                            }

                            // pat_admin_reservation_facture_email_send_pdf
                            if (0 === strpos($pathinfo, '/fr/mon-compte/admin/reservation/facture/email/send') && preg_match('#^/fr/mon\\-compte/admin/reservation/facture/email/send/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_reservation_facture_email_send_pdf')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::factureEmailPdfAction',));
                            }

                        }

                        // pat_admin_reservation_select_info_recherche
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/reservation/recherche') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_reservation_select_info_recherche');
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::selectInfoRechercheAction',  '_route' => 'pat_admin_reservation_select_info_recherche',);
                        }

                        // pat_admin_reservation_select_appartement
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/reservation/selection') && preg_match('#^/fr/mon\\-compte/admin/reservation/selection/(?P<id_appartement>[^/]++)/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_reservation_select_appartement');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_reservation_select_appartement')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::selectAppartementAction',));
                        }

                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/reservation/recherche')) {
                            // pat_admin_reservation_recherche_locataire
                            if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/reservation/recherche/locataire') {
                                if (substr($pathinfo, -1) !== '/') {
                                    return $this->redirect($pathinfo.'/', 'pat_admin_reservation_recherche_locataire');
                                }

                                return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::rechercheLocataireAction',  '_route' => 'pat_admin_reservation_recherche_locataire',);
                            }

                            // pat_admin_reservation_selection_locataire
                            if (preg_match('#^/fr/mon\\-compte/admin/reservation/recherche/(?P<id_locataire>[^/]++)/?$#s', $pathinfo, $matches)) {
                                if (substr($pathinfo, -1) !== '/') {
                                    return $this->redirect($pathinfo.'/', 'pat_admin_reservation_selection_locataire');
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_reservation_selection_locataire')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::selectLocataireAction',));
                            }

                        }

                        // pat_admin_reservation_inscription_locataire
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/reservation/inscription') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_reservation_inscription_locataire');
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::inscriptionLocataireAction',  '_route' => 'pat_admin_reservation_inscription_locataire',);
                        }

                        // pat_admin_reservation_informations_locataire
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/reservation/linformations') && preg_match('#^/fr/mon\\-compte/admin/reservation/linformations/(?P<id_locataire>[^/]++)/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_reservation_informations_locataire');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_reservation_informations_locataire')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::changeInformationsLocataireAction',));
                        }

                        // pat_admin_reservation_recapitulatif
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/reservation/recapitulatif') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_reservation_recapitulatif');
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::recapitulatifAction',  '_route' => 'pat_admin_reservation_recapitulatif',);
                        }

                        // pat_admin_reservation_confirmation
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/reservation/confirmation') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_reservation_confirmation');
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::confirmationAction',  '_route' => 'pat_admin_reservation_confirmation',);
                        }

                        // pat_admin_reservation_retour_recherche
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/reservation/retour-recherche') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_reservation_retour_recherche');
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::restitutionRechercheBySessionAction',  '_route' => 'pat_admin_reservation_retour_recherche',);
                        }

                        // pat_admin_reservation_calcul_montant
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/reservation/montant') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_reservation_calcul_montant');
                            }

                            return array (  '_controller' => 'Pat\\FrontBundle\\Controller\\ReservationController::calculMontantAdminAction',  '_route' => 'pat_admin_reservation_calcul_montant',);
                        }

                        // pat_admin_reservation_actualise_nbpersonne
                        if ($pathinfo === '/fr/mon-compte/admin/reservation/nbpersonne') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::actualiseNbPersonneAction',  '_route' => 'pat_admin_reservation_actualise_nbpersonne',);
                        }

                        // pat_admin_reservation_send_to_locataire
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/reservation/send-to-locataire') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_reservation_send_to_locataire');
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::sendToLocataireAction',  '_route' => 'pat_admin_reservation_send_to_locataire',);
                        }

                        // pat_admin_reservation_confirmation_send_to_locataire
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/reservation/confirmation-send-to-locataire') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_reservation_confirmation_send_to_locataire');
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminReservationController::confirmationSendToLocataireAction',  '_route' => 'pat_admin_reservation_confirmation_send_to_locataire',);
                        }

                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/ta')) {
                        // pat_admin_tarif_ajouter
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/tarif/ajouter') && preg_match('#^/fr/mon\\-compte/admin/tarif/ajouter/(?P<id_bien>[^/]++)/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_tarif_ajouter');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_tarif_ajouter')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\TarifController::gestionByAppAction',));
                        }

                        // pat_admin_taxes_editer
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/taxes/edit') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_taxes_editer');
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\TarifController::TaxesAction',  '_route' => 'pat_admin_taxes_editer',);
                        }

                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/contenu')) {
                        // pat_admin_contenu_index_lg
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/contenu/lg') && preg_match('#^/fr/mon\\-compte/admin/contenu/lg/(?P<langue>en|fr|de|es|it)/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_contenu_index_lg');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_contenu_index_lg')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminContenuController::indexAction',));
                        }

                        // pat_admin_contenu_ajouter
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/contenu/ajouter/lg') && preg_match('#^/fr/mon\\-compte/admin/contenu/ajouter/lg/(?P<langue>en|fr|de|es|it)/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_contenu_ajouter');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_contenu_ajouter')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminContenuController::ajouterAction',));
                        }

                        // pat_admin_contenu_editer
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/contenu/modifier') && preg_match('#^/fr/mon\\-compte/admin/contenu/modifier/(?P<id>[^/]++)/lg/(?P<langue>en|fr|de|es|it)/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_contenu_editer');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_contenu_editer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminContenuController::ajouterAction',));
                        }

                        // pat_admin_contenu_publier
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/contenu/publier') && preg_match('#^/fr/mon\\-compte/admin/contenu/publier/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_contenu_publier')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminContenuController::publierAction',));
                        }

                        // pat_admin_contenu_supprimer
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/contenu/supprimer') && preg_match('#^/fr/mon\\-compte/admin/contenu/supprimer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_contenu_supprimer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminContenuController::supprimerAction',));
                        }

                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/proprietaire')) {
                        // pat_admin_proprietaire_index
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/proprietaire') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_proprietaire_index');
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminProprietaireController::indexAction',  '_route' => 'pat_admin_proprietaire_index',);
                        }

                        // pat_admin_proprietaire_ajouter
                        if ($pathinfo === '/fr/mon-compte/admin/proprietaire/ajouter') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminProprietaireController::ajouterAction',  '_route' => 'pat_admin_proprietaire_ajouter',);
                        }

                        // pat_admin_proprietaire_editer
                        if (preg_match('#^/fr/mon\\-compte/admin/proprietaire/(?P<id_proprio>[^/]++)/modifier/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_proprietaire_editer');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_proprietaire_editer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminProprietaireController::ajouterAction',));
                        }

                        // pat_admin_proprietaire_activer
                        if (preg_match('#^/fr/mon\\-compte/admin/proprietaire/(?P<id_proprio>[^/]++)/activer/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_proprietaire_activer');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_proprietaire_activer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminProprietaireController::activerAction',));
                        }

                        // pat_admin_proprietaire_desactiver
                        if (preg_match('#^/fr/mon\\-compte/admin/proprietaire/(?P<id_proprio>[^/]++)/desactiver/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_proprietaire_desactiver');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_proprietaire_desactiver')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminProprietaireController::desactiverAction',));
                        }

                        // pat_admin_proprietaire_afficher
                        if (preg_match('#^/fr/mon\\-compte/admin/proprietaire/(?P<id_proprio>[^/]++)/afficher/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_proprietaire_afficher');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_proprietaire_afficher')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminProprietaireController::afficherAction',));
                        }

                        // pat_admin_proprietaire_appartement_index
                        if (preg_match('#^/fr/mon\\-compte/admin/proprietaire/(?P<id_proprio>[^/]++)/appartement/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_proprietaire_appartement_index');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_proprietaire_appartement_index')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminProprietaireController::listeAppartementAction',));
                        }

                        // pat_admin_proprietaire_reservation_index
                        if (preg_match('#^/fr/mon\\-compte/admin/proprietaire/(?P<id_proprio>[^/]++)/reservation/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_proprietaire_reservation_index');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_proprietaire_reservation_index')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminProprietaireController::listeReservationAction',));
                        }

                        // pat_admin_proprietaire_resas_loc
                        if (preg_match('#^/fr/mon\\-compte/admin/proprietaire/(?P<id_proprio>[^/]++)/reservations\\-loc/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_proprietaire_resas_loc');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_proprietaire_resas_loc')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminProprietaireController::listeResasLocAction',));
                        }

                        // pat_admin_send_infos_prop
                        if (preg_match('#^/fr/mon\\-compte/admin/proprietaire/(?P<id_proprio>[^/]++)/send\\-infos/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_send_infos_prop');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_send_infos_prop')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminProprietaireController::sendInfosAction',));
                        }

                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/locataire')) {
                        // pat_admin_locataire_index
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/locataire') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_locataire_index');
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminLocataireController::indexAction',  '_route' => 'pat_admin_locataire_index',);
                        }

                        // pat_admin_locataire_ajouter
                        if ($pathinfo === '/fr/mon-compte/admin/locataire/ajouter') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminLocataireController::ajouterAction',  '_route' => 'pat_admin_locataire_ajouter',);
                        }

                        // pat_admin_locataire_editer
                        if (preg_match('#^/fr/mon\\-compte/admin/locataire/(?P<id_locataire>[^/]++)/modifier/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_locataire_editer');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_locataire_editer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminLocataireController::ajouterAction',));
                        }

                        // pat_admin_locataire_activer
                        if (preg_match('#^/fr/mon\\-compte/admin/locataire/(?P<id_locataire>[^/]++)/activer/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_locataire_activer');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_locataire_activer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminLocataireController::activerAction',));
                        }

                        // pat_admin_locataire_desactiver
                        if (preg_match('#^/fr/mon\\-compte/admin/locataire/(?P<id_locataire>[^/]++)/desactiver/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_locataire_desactiver');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_locataire_desactiver')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminLocataireController::desactiverAction',));
                        }

                        // pat_admin_locataire_afficher
                        if (preg_match('#^/fr/mon\\-compte/admin/locataire/(?P<id_locataire>[^/]++)/afficher/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_locataire_afficher');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_locataire_afficher')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminLocataireController::afficherAction',));
                        }

                        // pat_admin_locataire_reservation_index
                        if (preg_match('#^/fr/mon\\-compte/admin/locataire/(?P<id_locataire>[^/]++)/reservation/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_locataire_reservation_index');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_locataire_reservation_index')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminLocataireController::listeReservationAction',));
                        }

                        // pat_admin_send_infos_loc
                        if (preg_match('#^/fr/mon\\-compte/admin/locataire/(?P<id_locataire>[^/]++)/send\\-infos/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_send_infos_loc');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_send_infos_loc')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminLocataireController::sendInfosAction',));
                        }

                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/utilisateur')) {
                        // pat_admin_utilisateur_index
                        if (rtrim($pathinfo, '/') === '/fr/mon-compte/admin/utilisateur') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_utilisateur_index');
                            }

                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminUtilisateurController::indexAction',  '_route' => 'pat_admin_utilisateur_index',);
                        }

                        // pat_admin_utilisateur_ajouter
                        if ($pathinfo === '/fr/mon-compte/admin/utilisateur/ajouter') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminUtilisateurController::ajouterAction',  '_route' => 'pat_admin_utilisateur_ajouter',);
                        }

                        // pat_admin_utilisateur_editer
                        if (preg_match('#^/fr/mon\\-compte/admin/utilisateur/(?P<id_user>[^/]++)/modifier/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_utilisateur_editer');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_utilisateur_editer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminUtilisateurController::ajouterAction',));
                        }

                        // pat_admin_utilisateur_activer
                        if (preg_match('#^/fr/mon\\-compte/admin/utilisateur/(?P<id_user>[^/]++)/activer/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_utilisateur_activer');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_utilisateur_activer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminUtilisateurController::activerAction',));
                        }

                        // pat_admin_utilisateur_desactiver
                        if (preg_match('#^/fr/mon\\-compte/admin/utilisateur/(?P<id_user>[^/]++)/desactiver/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_utilisateur_desactiver');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_utilisateur_desactiver')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminUtilisateurController::desactiverAction',));
                        }

                        // pat_admin_utilisateur_password
                        if (preg_match('#^/fr/mon\\-compte/admin/utilisateur/(?P<id_user>[^/]++)/password/?$#s', $pathinfo, $matches)) {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'pat_admin_utilisateur_password');
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_utilisateur_password')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminUtilisateurController::passwordAction',));
                        }

                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/equipement')) {
                        // pat_admin_equipement_ajouter
                        if ($pathinfo === '/fr/mon-compte/admin/equipement/ajouter') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\EquipementController::ajouterAction',  '_route' => 'pat_admin_equipement_ajouter',);
                        }

                        // pat_admin_equipement_editer
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/equipement/modifier') && preg_match('#^/fr/mon\\-compte/admin/equipement/modifier/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_equipement_editer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\EquipementController::ajouterAction',));
                        }

                        // pat_admin_equipement_supprimer
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/equipement/supprimer') && preg_match('#^/fr/mon\\-compte/admin/equipement/supprimer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_equipement_supprimer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\EquipementController::supprimerAction',));
                        }

                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/equipement/categorie')) {
                            // pat_admin_equipement_categorie_ajouter
                            if ($pathinfo === '/fr/mon-compte/admin/equipement/categorie/ajouter') {
                                return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\EquipementCategorieController::ajouterAction',  '_route' => 'pat_admin_equipement_categorie_ajouter',);
                            }

                            // pat_admin_equipement_categorie_editer
                            if (0 === strpos($pathinfo, '/fr/mon-compte/admin/equipement/categorie/modifier') && preg_match('#^/fr/mon\\-compte/admin/equipement/categorie/modifier/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_equipement_categorie_editer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\EquipementCategorieController::ajouterAction',));
                            }

                            // pat_admin_equipement_categorie_supprimer
                            if (0 === strpos($pathinfo, '/fr/mon-compte/admin/equipement/categorie/supprimer') && preg_match('#^/fr/mon\\-compte/admin/equipement/categorie/supprimer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_equipement_categorie_supprimer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\EquipementCategorieController::supprimerAction',));
                            }

                        }

                    }

                    // pat_admin_contenu_ville
                    if ($pathinfo === '/fr/mon-compte/admin/recherche/ville') {
                        return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminVilleController::listeVilleAction',  '_route' => 'pat_admin_contenu_ville',);
                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/ville')) {
                        // pat_admin_ville_index
                        if ($pathinfo === '/fr/mon-compte/admin/ville') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminVilleController::indexAction',  '_route' => 'pat_admin_ville_index',);
                        }

                        // pat_admin_ville_ajouter
                        if ($pathinfo === '/fr/mon-compte/admin/ville/ajouter') {
                            return array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminVilleController::ajouterAction',  '_route' => 'pat_admin_ville_ajouter',);
                        }

                        // pat_admin_ville_editer
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/ville/modifier') && preg_match('#^/fr/mon\\-compte/admin/ville/modifier/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_ville_editer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminVilleController::ajouterAction',));
                        }

                        // pat_admin_ville_supprimer
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/ville/supprimer') && preg_match('#^/fr/mon\\-compte/admin/ville/supprimer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_ville_supprimer')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\AdminVilleController::supprimerAction',));
                        }

                    }

                    if (0 === strpos($pathinfo, '/fr/mon-compte/admin/payment')) {
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/payment/generate')) {
                            // pat_admin_generate_payment
                            if (preg_match('#^/fr/mon\\-compte/admin/payment/generate/(?P<member_id>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_generate_payment')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\PaymentController::generatePaymentAction',));
                            }

                            // pat_admin_generate_payment_resa
                            if (preg_match('#^/fr/mon\\-compte/admin/payment/generate/(?P<member_id>[^/]++)/(?P<id_reservation>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_generate_payment_resa')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\PaymentController::generatePaymentAction',));
                            }

                        }

                        // pat_admin_list_payment_by_member
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/payment/list') && preg_match('#^/fr/mon\\-compte/admin/payment/list/(?P<member_id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_list_payment_by_member')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\PaymentController::listByMemberAction',));
                        }

                        // pat_admin_valider_payment
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/payment/valider') && preg_match('#^/fr/mon\\-compte/admin/payment/valider/(?P<payment_id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_valider_payment')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\PaymentController::validerAction',));
                        }

                        // pat_admin_new_payment
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/payment/new') && preg_match('#^/fr/mon\\-compte/admin/payment/new/(?P<member_id>[^/]++)/(?P<id_reservation>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_new_payment')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\PaymentController::newAction',));
                        }

                        // pat_admin_edit_payment
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/payment/edit') && preg_match('#^/fr/mon\\-compte/admin/payment/edit/(?P<id_payment>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_edit_payment')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\PaymentController::editAction',));
                        }

                        // pat_admin_delete_payment
                        if (0 === strpos($pathinfo, '/fr/mon-compte/admin/payment/delete') && preg_match('#^/fr/mon\\-compte/admin/payment/delete/(?P<id_payment>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pat_admin_delete_payment')), array (  '_controller' => 'Pat\\CompteBundle\\Controller\\PaymentController::deleteAction',));
                        }

                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/media/cache/resolve')) {
            // liip_imagine_filter_runtime
            if (preg_match('#^/media/cache/resolve/(?P<filter>[A-z0-9_\\-]*)/rc/(?P<hash>[^/]++)/(?P<path>.+)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_liip_imagine_filter_runtime;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'liip_imagine_filter_runtime')), array (  '_controller' => 'liip_imagine.controller:filterRuntimeAction',));
            }
            not_liip_imagine_filter_runtime:

            // liip_imagine_filter
            if (preg_match('#^/media/cache/resolve/(?P<filter>[A-z0-9_\\-]*)/(?P<path>.+)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_liip_imagine_filter;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'liip_imagine_filter')), array (  '_controller' => 'liip_imagine.controller:filterAction',));
            }
            not_liip_imagine_filter:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
