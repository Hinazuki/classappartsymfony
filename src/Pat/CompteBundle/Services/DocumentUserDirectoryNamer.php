<?php

namespace Pat\CompteBundle\Services;

use Vich\UploaderBundle\Naming\DirectoryNamerInterface;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * DocumentUserDirectoryNamer extends Vich DirectoryNamerInterface to provide a directory name by object user id
 */
class DocumentUserDirectoryNamer implements DirectoryNamerInterface
{

  public function directoryName($object, PropertyMapping $mapping)
  {
    $uploadDir = "";

    if (!is_null($object->getUser()))
      return $uploadDir.'/'.$object->getUser()->getId();
    else
      return $uploadDir;
  }

}
