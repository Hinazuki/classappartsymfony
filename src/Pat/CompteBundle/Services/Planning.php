<?php

namespace Pat\CompteBundle\Services;

use Doctrine\ORM\EntityManager;
use Pat\CompteBundle\Tools\CalendrierTools;
use Pat\CompteBundle\Tools\ReservationTools;

class Planning
{

  protected $em;

  public function __construct(EntityManager $em)
  {
    $this->em = $em;
  }

  public function getPlanning($date = null)
  {
    // Si la date n'est pas renseignée, on prends le mois actuel
    if (is_null($date))
      $date = date("m-Y");

    // On calcul le nombre de jours dans le mois en cours
    $date_explode = explode('-', $date);
    $nb_jours = cal_days_in_month(CAL_GREGORIAN, $date_explode[0], $date_explode[1]);

    $biens = $this->em->getRepository('PatCompteBundle:Appartement')->findBy(array('statut' => 4), array('reference' => 'ASC'));

    // On crée un tableau par bien correspondant au mois sélectionné
    foreach ($biens as $bien) {
      if ($bien->getVille())
        $ref = 'T'.$bien->getNbPieces().' - '.$bien->getSurfaceSol().' m2 - '.$bien->getVille()->getCodePostal().' | Réf : '.$bien->getReference();
      else
        $ref = 'T'.$bien->getNbPieces().' - '.$bien->getSurfaceSol().' m2 | Réf : '.$bien->getReference();

      $planning[$ref] = array_pad(array(), $nb_jours + 1, null);
      unset($planning[$ref][0]); // On supprime l'indice 0

      $resas = $this->em->getRepository('PatCompteBundle:Reservation')->getResaValidByBien($bien->getId(), $date_explode[1].'-'.$date_explode[0]);
      foreach ($resas as $resa) {
        $debut_resa = explode('-', $resa->getDateDebut());

        // Si le mois de début est différent (donc appartient au mois précédent, on fixe le jour de début au 1er du mois
        if ($debut_resa[1] == $date_explode[0] && $debut_resa[0] == $date_explode[1])
          $jour_debut = (int) $debut_resa[2];
        else
          $jour_debut = 1;

        $fin_resa = explode('-', $resa->getDateFin());

        // Si le mois de fin est différent (donc appartient au mois suivant, on fixe le jour de fin au dernier du mois
        if ($fin_resa[1] == $date_explode[0] && $fin_resa[0] == $date_explode[1])
          $jour_fin = (int) $fin_resa[2];
        else
          $jour_fin = $nb_jours;

        // On stock la class pour l'affichage de la couleur
        for ($i = $jour_debut; $i <= $jour_fin; $i++) {
          switch ($resa->getStatus()) {
            case ReservationTools::CONSTANT_STATUS_WAIT_VALIDATION:
              $planning[$ref][$i] .= " planning-resa-wait-valid";
              if ($i == $jour_debut && $debut_resa[1] == $date_explode[0] && $debut_resa[0] == $date_explode[1])
                $planning[$ref][$jour_debut] .= ' planning-resa-debut planning-resa-debut-wait-valid'; // On ajoute une classe particulière pour le premier jour
              if ($i == $jour_fin && $fin_resa[1] == $date_explode[0] && $fin_resa[0] == $date_explode[1])
                $planning[$ref][$jour_fin] .= ' planning-resa-fin planning-resa-fin-wait-valid'; // On ajoute une classe particulière pour le dernier jour
              break;
            case ReservationTools::CONSTANT_STATUS_WAIT:
              $planning[$ref][$i] .= " planning-resa-wait";
              if ($i == $jour_debut && $debut_resa[1] == $date_explode[0] && $debut_resa[0] == $date_explode[1])
                $planning[$ref][$jour_debut] .= ' planning-resa-debut planning-resa-debut-wait'; // On ajoute une classe particulière pour le premier jour
              if ($i == $jour_fin && $fin_resa[1] == $date_explode[0] && $fin_resa[0] == $date_explode[1])
                $planning[$ref][$jour_fin] .= ' planning-resa-fin planning-resa-fin-wait'; // On ajoute une classe particulière pour le dernier jour
              break;
            case ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED:
              if ($resa->getValidProp() == ReservationTools::CONSTANT_VALID_RESA_PROP || $resa->getValidProp() == ReservationTools::CONSTANT_VALID_RESA_48) {
                $planning[$ref][$i] .= " planning-resa-deposit-payed";
                if ($i == $jour_debut && $debut_resa[1] == $date_explode[0] && $debut_resa[0] == $date_explode[1])
                  $planning[$ref][$jour_debut] .= ' planning-resa-debut planning-resa-debut-deposit-payed'; // On ajoute une classe particulière pour le premier jour
                if ($i == $jour_fin && $fin_resa[1] == $date_explode[0] && $fin_resa[0] == $date_explode[1])
                  $planning[$ref][$jour_fin] .= ' planning-resa-fin planning-resa-fin-deposit-payed'; // On ajoute une classe particulière pour le dernier jour
              }
              else {
                $planning[$ref][$i] .= " planning-resa-deposit-payed-wait-prop";
                if ($i == $jour_debut && $debut_resa[1] == $date_explode[0] && $debut_resa[0] == $date_explode[1])
                  $planning[$ref][$jour_debut] .= ' planning-resa-debut planning-resa-debut-deposit-payed-wait-prop'; // On ajoute une classe particulière pour le premier jour
                if ($i == $jour_fin && $fin_resa[1] == $date_explode[0] && $fin_resa[0] == $date_explode[1])
                  $planning[$ref][$jour_fin] .= ' planning-resa-fin planning-resa-fin-deposit-payed-wait-prop'; // On ajoute une classe particulière pour le dernier jour
              }
              break;
            case ReservationTools::CONSTANT_STATUS_TO_PAYED:
              $planning[$ref][$i] .= " planning-resa-to-payed";
              if ($i == $jour_debut && $debut_resa[1] == $date_explode[0] && $debut_resa[0] == $date_explode[1])
                $planning[$ref][$jour_debut] .= ' planning-resa-debut planning-resa-debut-to-payed'; // On ajoute une classe particulière pour le premier jour
              if ($i == $jour_fin && $fin_resa[1] == $date_explode[0] && $fin_resa[0] == $date_explode[1])
                $planning[$ref][$jour_fin] .= ' planning-resa-fin planning-resa-fin-to-payed'; // On ajoute une classe particulière pour le dernier jour
              break;
            case ReservationTools::CONSTANT_STATUS_PAYED:
              $planning[$ref][$i] .= " planning-resa-payed";
              if ($i == $jour_debut && $debut_resa[1] == $date_explode[0] && $debut_resa[0] == $date_explode[1])
                $planning[$ref][$jour_debut] .= ' planning-resa-debut planning-resa-debut-payed'; // On ajoute une classe particulière pour le premier jour
              if ($i == $jour_fin && $fin_resa[1] == $date_explode[0] && $fin_resa[0] == $date_explode[1])
                $planning[$ref][$jour_fin] .= ' planning-resa-fin planning-resa-fin-payed'; // On ajoute une classe particulière pour le dernier jour
              break;
            case ReservationTools::CONSTANT_STATUS_EXCEEDING:
              $planning[$ref][$i] .= " planning-resa-exceeding";
              if ($i == $jour_debut && $debut_resa[1] == $date_explode[0] && $debut_resa[0] == $date_explode[1])
                $planning[$ref][$jour_debut] .= ' planning-resa-debut planning-resa-debut-exceeding'; // On ajoute une classe particulière pour le premier jour
              if ($i == $jour_fin && $fin_resa[1] == $date_explode[0] && $fin_resa[0] == $date_explode[1])
                $planning[$ref][$jour_fin] .= ' planning-resa-fin planning-resa-fin-exceeding'; // On ajoute une classe particulière pour le dernier jour
              break;
            default:
              ;
          }
        }
      }


      $bloquages = $this->em->getRepository('PatCompteBundle:Calendrier')->getDatesByBien($bien->getId(), $date_explode[1].'-'.$date_explode[0]);
      foreach ($bloquages as $bloquage) {
        $debut_bloquage = explode('-', $bloquage->getDateDebut());

        // Si le mois de début est différent (donc appartient au mois précédent, on fixe le jour de début au 1er du mois
        if ($debut_bloquage[1] == $date_explode[0] && $debut_bloquage[0] == $date_explode[1])
          $jour_debut = (int) $debut_bloquage[2];
        else
          $jour_debut = 1;

        $fin_bloquage = explode('-', $bloquage->getDateFin());

        // Si le mois de fin est différent (donc appartient au mois suivant, on fixe le jour de fin au dernier du mois
        if ($fin_bloquage[1] == $date_explode[0] && $fin_bloquage[0] == $date_explode[1])
          $jour_fin = (int) $fin_bloquage[2];
        else
          $jour_fin = $nb_jours;

        // On stock la class pour l'affichage de la couleur
        for ($i = $jour_debut; $i <= $jour_fin; $i++) {
          switch ($bloquage->getStatus()) {
            case CalendrierTools::CONSTANT_STATUS_BLOQUE_RESERVATION:
              $class = "planning-resa";
              break;

            case CalendrierTools::CONSTANT_STATUS_BLOQUE_PROPRIETAIRE:
              $class = "planning-bloc-prop";
              if ($i == $jour_debut && $debut_bloquage[1] == $date_explode[0] && $debut_bloquage[0] == $date_explode[1])
                $class .= ' planning-bloc-debut planning-bloc-debut-prop';
              if ($i == $jour_fin && $fin_bloquage[1] == $date_explode[0] && $fin_bloquage[0] == $date_explode[1])
                $class .= ' planning-bloc-fin planning-bloc-fin-prop';
              break;

            case CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN:
              $class = "planning-bloc-ca";
              if ($i == $jour_debut && $debut_bloquage[1] == $date_explode[0] && $debut_bloquage[0] == $date_explode[1])
                $class .= ' planning-bloc-debut planning-bloc-debut-ca';
              if ($i == $jour_fin && $fin_bloquage[1] == $date_explode[0] && $fin_bloquage[0] == $date_explode[1])
                $class .= ' planning-bloc-fin planning-bloc-fin-ca';
              break;

            case CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE:
              $class = "planning-bloc-menage";
              break;

            case CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_TRAVAUX:
              $class = "planning-bloc-travaux";
              if ($i == $jour_debut && $debut_bloquage[1] == $date_explode[0] && $debut_bloquage[0] == $date_explode[1])
                $class .= ' planning-bloc-debut planning-bloc-debut-travaux';
              if ($i == $jour_fin && $fin_bloquage[1] == $date_explode[0] && $fin_bloquage[0] == $date_explode[1])
                $class .= ' planning-bloc-fin planning-bloc-fin-travaux';
              break;

            default:
              $class = null;
              break;
          }
//                    if($i == $jour_fin && $bloquage->getStatus() != CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE && $fin_bloquage[1] == $date_explode[0] && $fin_bloquage[0] == $date_explode[1])
//                        $class .= ' planning-resa-fin';

          $planning[$ref][$i] .= ' '.$class;
        }
      }
    }

    return $planning;
  }

}
