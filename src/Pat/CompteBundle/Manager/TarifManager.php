<?php

namespace Pat\CompteBundle\Manager;

use Doctrine\ORM\EntityManager;
use Pat\CompteBundle\Entity\Tarif;
use Pat\CompteBundle\Lib\DateFunctions;
use Symfony\Component\Validator\Constraints\DateTime;
use Psr\Log\LoggerInterface;


class TarifManager
{

  protected $prixRoses = 40;
  protected $prixChampagne = 40;
  protected $prixLitBebe = 40;
  protected $menageStudio = 25;
  protected $menageT2 = 50;
  protected $menageT3 = 75;
  protected $menageT4 = 100;
  protected $em;

  public function __construct(EntityManager $em, DateFunctions $dateTools)
  {
    $this->em = $em;
    $this->dateTools = $dateTools;
  }

  public function getRepository()
  {
    return $this->em->getRepository('PatCompteBundle:Tarif');
  }

  public function calculLoyer($tarif, $taxes, $nb_pieces, $nb_nuits, $nb_adultes, $resaDateDebut, $resaDateFin, $parentReservation = null)
  {
    $values = array();
    $menageNbNuits = $nb_nuits;

    //si le reservation est une extention
    if ($parentReservation != null) {
      $parentReservation = $this->em->getRepository('PatCompteBundle:Reservation')->find($parentReservation);
      $parentNbJours = $this->dateTools->nbJours($parentReservation->getDateDebut(), $parentReservation->getDateFin());
      $menageNbNuits = $nb_nuits + $parentNbJours;
      $loyer_net = $this->getLoyerByDate($tarif, $nb_nuits + $parentNbJours);
    }
    else {
      $loyer_net = $this->getLoyerByDate($tarif, $nb_nuits);
    }

    // On récupère les loyers correspondants aux nombres de personnes
    switch ($nb_pieces) {
      case '1':
        if ($nb_adultes <= 2) {
          $menage = $menageNbNuits < 15 ? $taxes['menage']['studio']['deuxpersonnes']['inf15j'] : $taxes['menage']['studio']['deuxpersonnes']['sup15j'];
        }
        else {
          $menage = $menageNbNuits < 15 ? $taxes['menage']['studio']['quatrepersonnes']['inf15j'] : $taxes['menage']['studio']['quatrepersonnes']['sup15j'];
        }

        $edf = $taxes['EDF']['studio'];
        break;

      case '2':
        if ($nb_adultes <= 2) {
          $menage = $menageNbNuits < 15 ? $taxes['menage']['T2']['deuxpersonnes']['inf15j'] : $taxes['menage']['T2']['deuxpersonnes']['sup15j'];
        }
        else {
          $menage = $menageNbNuits < 15 ? $taxes['menage']['T2']['quatrepersonnes']['inf15j'] : $taxes['menage']['T2']['quatrepersonnes']['sup15j'];
        }

        $edf = $taxes['EDF']['T2'];
        break;

      case '3':
        if ($nb_adultes <= 4) {
          $menage = $menageNbNuits < 15 ? $taxes['menage']['T3']['quatrepersonnes']['inf15j'] : $taxes['menage']['T3']['quatrepersonnes']['sup15j'];
        }
        else {
          $menage = $menageNbNuits < 15 ? $taxes['menage']['T3']['sixpersonnes']['inf15j'] : $taxes['menage']['T3']['sixpersonnes']['sup15j'];
        }

        $edf = $taxes['EDF']['T3'];
        break;

      case '4':
        if ($nb_adultes <= 6) {
          $menage = $menageNbNuits < 15 ? $taxes['menage']['T4']['sixpersonnes']['inf15j'] : $taxes['menage']['T4']['sixpersonnes']['sup15j'];
        }
        else {
          $menage = $menageNbNuits < 15 ? $taxes['menage']['T4']['huitpersonnes']['inf15j'] : $taxes['menage']['T4']['huitpersonnes']['sup15j'];
        }

        $edf = $taxes['EDF']['T4'];
        break;

      case '5':
        if ($nb_adultes <= 8) {
          $menage = $menageNbNuits < 15 ? $taxes['menage']['T5']['huitpersonnes']['inf15j'] : $taxes['menage']['T5']['huitpersonnes']['sup15j'];
        }
        else {
          $menage = $menageNbNuits < 15 ? $taxes['menage']['T5']['dixpersonnes']['inf15j'] : $taxes['menage']['T5']['dixpersonnes']['sup15j'];
        }

        $edf = $taxes['EDF']['T5'];
        break;

      // default = T6
      default:
        if ($nb_adultes <= 10) {
          $menage = $menageNbNuits < 15 ? $taxes['menage']['T6']['dixpersonnes']['inf15j'] : $taxes['menage']['T6']['dixpersonnes']['sup15j'];
        }
        else {
          $menage = $menageNbNuits < 15 ? $taxes['menage']['T6']['douzepersonnes']['inf15j'] : $taxes['menage']['T6']['douzepersonnes']['sup15j'];
        }

        $edf = $taxes['EDF']['T6'];
        break;
    }

    if (null != $parentReservation) {
      $menage = ($menage / $parentNbJours) * $nb_nuits;
    }

    // On récupère le pourcentage de commission correspondant suivant le nombre de nuits
    if ($nb_nuits <= 6)
      $commission = $taxes['commission']['nuit'];
    elseif ($nb_nuits <= 24)
      $commission = $taxes['commission']['semaine'];
    else
      $commission = $taxes['commission']['mois'];

    $activePromotion = $tarif->getAppartement()->getActivePromotion();

    if (null !== $activePromotion && new \DateTime($resaDateDebut) < $activePromotion->getActiveLimitDate()) {
      $loyer_net = $loyer_net * (1 + ($activePromotion->getPercentage() / 100));

      $values['promotion_activable'] = $activePromotion->getPercentage();
    }

    $periodePromotions = $this->em->getRepository('PatCompteBundle:Promotion')->findActivePeriodePromotion($tarif->getAppartement()->getId(), $resaDateDebut, $resaDateFin);

    $totalLoyerNetParNuits = 0;

    $totalNuits = 0;
    $originalNbNuits = $nb_nuits;

    foreach ($periodePromotions as $periodePromotion) {
      if (false === $resaDateDebut instanceof \DateTime) {
        $resaDateDebut = new \DateTime($resaDateDebut);
      }

      if (false === $resaDateFin instanceof \DateTime) {
        $resaDateFin = new \DateTime($resaDateFin);
      }

      $promotionBegin = $periodePromotion->getBegin();
      $promotionEnd = $periodePromotion->getEnd();

      // following http://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
      if ($resaDateDebut < $promotionEnd && $resaDateFin > $promotionBegin) {
        $nb_nuits = min($resaDateFin, $promotionEnd)->diff(max($promotionBegin, $resaDateDebut))->days;
      }

      // On calcul le loyer Net avec le nombre Total de nuits qu'on divise pas le nombre total de nuits
      // Pour avoir le bon loyer (nuit/semaine/mois) pour une nuit
      $percentage = $periodePromotion->getPercentage();

      if (true === isset($values['promotion_activable'])) {
        $percentage += $values['promotion_activable'];
      }

      $tmpLoyerNet = $loyer_net * (1 + ($percentage / 100));

      $totalLoyerNetParNuits += $tmpLoyerNet * $nb_nuits;

      if (false === isset($values['periode_promotion'])) {
        $values['periode_promotion'] = 0;
      }

      $values['periode_promotion'] += $periodePromotion->getPercentage();
      $totalNuits += $nb_nuits;
    }

    $nbNuitSansPromo = $originalNbNuits - $totalNuits;

    if ($nbNuitSansPromo > 0) {
      $totalLoyerNetParNuits += $loyer_net * $nbNuitSansPromo;
    }

    $tarif_total = ($totalLoyerNetParNuits * (1 + ($commission / 100))) * (1 + ($taxes['assurance'] / 100)) + ($edf * $nb_nuits) + ($taxes['sejour'] * $nb_nuits * $nb_adultes) + $menage;

    $values['tarif_total'] = $tarif_total;
    $values['tarif_net'] = $totalLoyerNetParNuits;
    $values['commission'] = $commission;
    $values['assurance'] = $taxes['assurance'];
    $values['EDF'] = $edf;
    $values['taxe_sejour'] = $taxes['sejour'];
    $values['menage_fixe'] = $menage;
    $values['tva'] = $taxes['tva'];
    return $values;
  }

  // Return loyer within type

  /**
   * @param Tarif  $tarif
   * @param string $type
   *
   * @return float|int|null
   */
  public function getLoyer($tarif, $type = "nuit")
  {
    $loyer = null;

    if (Tarif::LOYER_LIBRE === $tarif->getLoyerActif()) {
      switch ($type) {
        case "mois":
          $loyer = $tarif->getLoyerMois() / 30; // On récupère le loyer à la nuit
          break;

        case "semaine1":
          $loyer = $tarif->getLoyerSemaine1() / 7; // On récupère le loyer à la nuit
          break;

        case "nuit":
          $loyer = $tarif->getLoyerNuit();
          break;
      }
    }
    elseif (Tarif::LOYER_AUTO === $tarif->getLoyerActif()) {
      switch ($type) {
        // Cas pour le plus grand nombre de personnes
        case "mois":
          $loyer = $tarif->getLoyerMois2() / 30; // On récupère le loyer à la nuit
          break;

        case "semaine1":
          $loyer = $tarif->getLoyerSemaine12() / 7; // On récupère le loyer à la nuit
          break;

        case "nuit":
          $loyer = $tarif->getLoyerNuit2();
          break;
      }
    }
    else {
      $loyer = $tarif->getLoyerNuit();
    }

    return $loyer;
  }

  // Return loyer within number of days
  public function getLoyerByDate($tarif, $nb_jours)
  {
    $loyer = null;

    if ($nb_jours > 21) {
      $loyer = $this->getLoyer($tarif, "mois");
    }
    elseif ($nb_jours > 6) {
      $loyer = $this->getLoyer($tarif, "semaine1");
    }
    else {
      $loyer = $this->getLoyer($tarif, "nuit");
    }

    return $loyer;
  }

  // Return the cheapest loyer
  public function getLowerLoyer($tarif)
  {
    $loyers = array(
      $this->getLoyer($tarif, "mois"),
      $this->getLoyer($tarif, "semaine1"),
      $this->getLoyer($tarif, "nuit"),
    );

    return min($loyers);
  }

  public function getPrixRoses()
  {
    return $this->prixRoses;
  }

  public function getPrixChampagne()
  {
    return $this->prixChampagne;
  }

  public function getPrixLitBebe()
  {
    return $this->prixLitBebe;
  }

  public function getPrixMenageStudio()
  {
    return $this->menageStudio;
  }

  public function getPrixMenageT2()
  {
    return $this->menageT2;
  }

  public function getPrixMenageT3()
  {
    return $this->menageT3;
  }

  public function getPrixMenageT4()
  {
    return $this->menageT4;
  }

  public function getPrixMenage($nb_pieces)
  {
    switch ($nb_pieces) {
      case 1:
        $prix = $this->getPrixMenageStudio();
        break;
      case 2:
        $prix = $this->getPrixMenageT2();
        break;
      case 3:
        $prix = $this->getPrixMenageT3();
        break;
      default:
        $prix = $this->getPrixMenageT4();
        break;
    }
    return $prix;
  }

  // Calcul le tarif "A partir de"
  public function getFromTarif($tarif, $taxes, $nb_pieces)
  {
    switch ($nb_pieces) {
      case '1':
        $menage = $taxes['menage']['studio']['deuxpersonnes']['sup15j'];
        $edf = $taxes['EDF']['studio'];
        break;

      case '2':
        $menage = $taxes['menage']['T2']['deuxpersonnes']['sup15j'];
        $edf = $taxes['EDF']['T2'];
        break;

      case '3':
        $menage = $taxes['menage']['T3']['quatrepersonnes']['sup15j'];
        $edf = $taxes['EDF']['T3'];
        break;

      case '4':
        $menage = $taxes['menage']['T4']['sixpersonnes']['sup15j'];
        $edf = $taxes['EDF']['T4'];
        break;

      case '5':
        $menage = $taxes['menage']['T5']['huitpersonnes']['sup15j'];
        $edf = $taxes['EDF']['T5'];
        break;

      // default = T6
      default:
        $menage = $taxes['menage']['T6']['dixpersonnes']['sup15j'];
        $edf = $taxes['EDF']['T6'];
        break;
    }

    // (3 x ([Tarif Mensuel] + [Comission(%)] + 30 x ( [Taxe séjour] + [Tarif EDF] ) ) + [Ménage > 15j]) / 90 + [Assurance(%)]
    $from_tarif = ((3 * 30 * ($this->getLoyer($tarif, "mois") * (1 + $taxes['commission']['mois'] / 100) + ($taxes['sejour'] + $edf)) + $menage) / 90) * (1 + $taxes['assurance'] / 100);

    return $from_tarif;
  }

}
