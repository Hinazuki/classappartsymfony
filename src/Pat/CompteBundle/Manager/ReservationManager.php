<?php

namespace Pat\CompteBundle\Manager;

use Doctrine\ORM\EntityManager;
use Pat\CompteBundle\Entity\Reservation;
use Pat\CompteBundle\Tools\ReservationTools;
use Pat\CompteBundle\Tools\PaymentTools;

class ReservationManager
{

  protected $em;
  protected $mailer;
  protected $templating;

  public function __construct(EntityManager $em, $mailer, $templating)
  {
    $this->em = $em;
    $this->mailer = $mailer;
    $this->templating = $templating;
  }

  public function getRepository()
  {
    return $this->em->getRepository('PatCompteBundle:Reservation');
  }

  public function sumPayments(Reservation $reservation)
  {
    $total = 0;
    $payments = $this->em->getRepository('PatCompteBundle:Payment')->findBy(array('reservation' => $reservation, 'status' => PaymentTools::CONSTANT_STATUS_PAYED));

    foreach ($payments as $payment)
      $total += $payment->getAmount();

    return $total;
  }

  public function updateStatus(Reservation $reservation)
  {
    // On ne modifie pas le statut des réservations annulées
    if (in_array($reservation->getStatus(), array(
        ReservationTools::CONSTANT_STATUS_WAIT,
        ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED,
        ReservationTools::CONSTANT_STATUS_TO_PAYED,
        ReservationTools::CONSTANT_STATUS_PAYED,
        ReservationTools::CONSTANT_STATUS_EXCEEDING))) {

      $old_status = $reservation->getStatus();

      $sum = $this->sumPayments($reservation);

      $tarif = $reservation->getTarif() + $reservation->getPromotion() + $reservation->getDepot();
      foreach ($reservation->getOptions() as $option)
        $tarif += $option->getQuantity() * $option->getAmount();

      $acompte = $reservation->getArrhes();

      //Arrhes = Acompte
      if (round($sum, 2) == round($acompte, 2)) {
        $reservation->setStatus(ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED);
      }
      // Acompte > Arrhes < Solde total
      if (round($sum, 2) > round($acompte, 2) && round($sum, 2) < round($tarif, 2)) {
        $reservation->setStatus(ReservationTools::CONSTANT_STATUS_TO_PAYED);
      }
      // Arrhes = Solde total
      if (round($sum, 2) == round($tarif, 2)) {
        $reservation->setStatus(ReservationTools::CONSTANT_STATUS_PAYED);
      }
      // Arrhes > Solde total
      if (round($sum, 2) > round($tarif, 2)) {
        $reservation->setStatus(ReservationTools::CONSTANT_STATUS_EXCEEDING);
      }

      $this->em->persist($reservation);
      $this->em->flush();

      //si l'accompte est versé
      if ($reservation->getStatus() == ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED) {

        $message_email = \Swift_Message::newInstance()
          ->setSubject("Confirmation de réservation Class Appart")
          ->setFrom('contact@class-appart.com')
          ->setTo($reservation->getAppartement()->getUtilisateur()->getEmail());

//                if($reservation->getAppartement()->getUtilisateur()->getMailsSecond())
//                    $message_email->setCc(explode(',', $reservation->getAppartement()->getUtilisateur()->getMailsSecond()));
        if ($reservation->getAppartement()->getUtilisateur()->getSecondEmail()) {
          $message_email->setCc($reservation->getAppartement()->getUtilisateur()->getSecondEmail());
        }

        $otherSiteTextTab= array( 3 => "Cette réservation a été effectuée via AirBnB, le client aura donc jusqu'a 5 jours avant la date de début de réservation pour l'annuler tout en bénéficiant d'un remboursement intégral", 
        4 => "Cette réservation a été effectuée via Booking , le client aura donc jusqu'a 30 jours avant la date de début de réservation pour l'annuler en bénéficiant d'un remboursement intégral", 
        5 => "Cette réservation a été effectuée via Abritel, le client aura donc jusqu'a 30 jours avant la date de début de réservation pour l'annuler en bénéficiant d'un remboursement intégral 
        et jusqu'à 14 jours avant la date de début de réservation pour l'annuler en bénéficiant d'un remboursement de l'acompte qu'il aura versés (50% du montant total de la révervation)");
        
        $textBody = $this->templating->render('PatCompteBundle:Reservation:email_valid_prop.txt.twig', array('reservation' => $reservation, 'tabOtherSite' => $otherSiteTextTab));
        $htmlBody = $this->templating->render('PatCompteBundle:Reservation:email_valid_prop.html.twig', array('reservation' => $reservation, 'tabOtherSite' => $otherSiteTextTab));

        if (!empty($htmlBody)) {
          $message_email->setBody($htmlBody, 'text/html')
            ->addPart($textBody, 'text/plain');
        }
        else
          $message_email->setBody($textBody);

        $this->mailer->send($message_email);
      }

      // Si la réservation passe à "Soldé"
      if ($reservation->getStatus() == ReservationTools::CONSTANT_STATUS_PAYED && $old_status != ReservationTools::CONSTANT_STATUS_PAYED) {

        $message_email = \Swift_Message::newInstance()
          ->setSubject("Confirmation du solde de réservation Class Appart")
          ->setFrom('contact@class-appart.com')
          ->setTo('info@class-appart.com');

        $textBody = $this->templating->render('PatCompteBundle:Reservation:email_payed.txt.twig', array('reservation' => $reservation));
        $htmlBody = $this->templating->render('PatCompteBundle:Reservation:email_payed.html.twig', array('reservation' => $reservation));

        if (!empty($htmlBody)) {
          $message_email->setBody($htmlBody, 'text/html')
            ->addPart($textBody, 'text/plain');
        }
        else
          $message_email->setBody($textBody);

        $this->mailer->send($message_email);
      }
    }
  }

  /**
   * @return Reservation[]
   */
  public function getReservationNotValidated()
  {
    return $this->getRepository()->findReservationNotValidatedByProp()->getQuery()->getResult();
  }

  /**
   * @return Reservation[]
   */
  public function getReservationWithDepositNotPayed($date, $maxFlag)
  {
    return $this->getRepository()->findReservationWithDepositNotPayed($date, $maxFlag)->getQuery()->getResult();
  }

    /**
   * @return Reservation[]
   */
  public function getBookingWithDepositNotPayedForCanceled()
  {
    return $this->getRepository()->findBookingWithDepositNotPayedForCanceled()->getQuery()->getResult();
  }

  public function getBookingNotPayedAfterTwoDays(){
    return $this->getRepository()->findBookingForFirstPaymentReminder()->getQuery()->getResult();
  }

  public function getBookingNotPayedAfterTwoWeeks(){
    return $this->getRepository()->findBookingForOtherPaymentReminder()->getQuery()->getResult();
  }

  public function getBookingNotPayedFiveDayBeforeBegin(){
    return $this->getRepository()->findBookingForLastFiveDayReminder()->getQuery()->getResult();
  }
}

