<?php

namespace Pat\CompteBundle\Twig\Extension;

use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Bundle\TwigBundle\Loader\FilesystemLoader;

class TwigExtension extends \Twig_Extension
{

  public function getName()
  {
    return 'pattwigext';
  }

  public function getFilters()
  {
    return array(
      'format_decimal' => new \Twig_Filter_Method($this, 'formatDecimal'),
      'testValeur' => new \Twig_Filter_Method($this, 'formatValFront'),
      'formatEtage' => new \Twig_Filter_Method($this, 'affEtageBien'),
      'supSlash' => new \Twig_Filter_Method($this, 'supprimer_slashs'),
      'limitBlocDesc' => new \Twig_Filter_Method($this, 'chaine_reduite'),
      'ConvertTailleFichier' => new \Twig_Filter_Method($this, 'ConvertTailleFichier'),
    );
  }

  public function ConvertTailleFichier($taille)
  {
    $taille = $taille * 0.000977;
    $taille = number_format($taille, 2, ',', '');
    return $taille."ko";
  }

  public function myIsInt($x)
  {
    return(is_numeric($x) ? intval(0 + $x) == $x : false);
  }

  // Fonction qui test si le nombre donné est un entier ou un float et affiche les chiffres après la virgule selon le cas
  public function formatDecimal($info)
  {

    if ($this->myIsInt($info)) {
      return round($info, 0);
    }
    else {
      return round($info, 2);
    }
  }

  // Fonction qui teste si la valeur est renseignée et qui renvoie "NC" sinon.
  public function formatValFront($valeur)
  {
    if ($valeur == null || $valeur == "") {
      return "NC";
    }
    else {
      return $valeur;
    }
  }

  // Fonction qui format l'affichage de l'étage d'un bien
  public function affEtageBien($etage)
  {
    $valeur = intval($etage);
    if ($valeur == 0) {
      return "RDC";
    }
    else if ($valeur == '1') {
      return "1er étage";
    }
    else if ($valeur > 1) {
      return $valeur."ème étage";
    }
    else {
      return "NC";
    }
  }

  public function supprimer_slashs($chaine)
  {
    return stripslashes($chaine);
  }

  public function chaine_reduite($chaine)
  {
    $lg_max = 130; //nombre de caractère autoriser

    if (strlen($chaine) > $lg_max) {
      $chaine = substr($chaine, 0, $lg_max);
      $last_space = strrpos($chaine, " ");
      $chaine = substr($chaine, 0, $last_space)."...";
    }

    return $chaine;
  }

}
