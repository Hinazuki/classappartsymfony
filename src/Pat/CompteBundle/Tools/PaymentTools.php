<?php

namespace Pat\CompteBundle\Tools;

class PaymentTools
{

  const CONSTANT_STATUS_WAIT = 10;
  const CONSTANT_STATUS_WAIT_RECEIPT = 20;
  const CONSTANT_STATUS_PAYED = 30;
  const CONSTANT_STATUS_CANCELED = 40;
  const BASE_URL_GENERATE_LINK = 'localhost';

  public static function getStatusArray()
  {

    $mStatus = array(
      self::CONSTANT_STATUS_PAYED => "Payé",
      self::CONSTANT_STATUS_WAIT => "En attente",
      self::CONSTANT_STATUS_WAIT_RECEIPT => "En attente de réception",
      self::CONSTANT_STATUS_CANCELED => "Annulé"
    );

    return $mStatus;
  }

  public static function getStatus($status)
  {

    $a = self::getStatusArray();

    if (!array_key_exists($status, $a))
      return "AUCUN STATUT";
    else
      return $a[$status];
  }

}
