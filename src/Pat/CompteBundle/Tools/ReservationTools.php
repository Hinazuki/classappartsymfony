<?php

namespace Pat\CompteBundle\Tools;

class ReservationTools
{

  const CONSTANT_CANCEL_RESA_PROP = 0; // Réservation refusée par le propriétaire
  const CONSTANT_VALID_RESA_PROP = 1; // Réservation validée par le propriétaire
  const CONSTANT_VALID_RESA_48 = 2; // Réservation validée automatiquement après 48h
  const CONSTANT_STATUS_WAIT_VALIDATION = 5;
  const CONSTANT_STATUS_WAIT = 10;
  const CONSTANT_STATUS_DEPOSIT_PAYED = 15;
  const CONSTANT_STATUS_TO_PAYED = 20;
  const CONSTANT_STATUS_PAYED = 30;
  const CONSTANT_STATUS_EXCEEDING = 35;
  const CONSTANT_STATUS_CANCELED_BY_CA = 40;
  const CONSTANT_STATUS_CANCELED_BY_PROP = 45;
  const CONSTANT_STATUS_CANCELED_BY_LOC = 50;
  const CONSTANT_STATUS_CANCELED_BY_BANK = 55;
  const NOTIFICATION_ACOMPTE_1H = 1;
  const NOTIFICATION_ACOMPTE_24H = 5;
  const NOTIFICATION_ACOMPTE_48H = 10;

  public static function getStatusArray()
  {

    $mStatus = array(
      self::CONSTANT_STATUS_WAIT_VALIDATION => "En attente de validation locataire",
      self::CONSTANT_STATUS_WAIT => "En attente", // Aucun paiement effectué
      self::CONSTANT_STATUS_DEPOSIT_PAYED => "Acompte versé", // Acompte payé par le locataire
      self::CONSTANT_STATUS_TO_PAYED => "A solder", // Acompte et une partie du solde payés par le locataire
      self::CONSTANT_STATUS_PAYED => "Soldé", // Solde total payé par le locataire
      self::CONSTANT_STATUS_EXCEEDING => "Excédant", // Somme total payée par le locataire supérieure au solde de la réservation
      self::CONSTANT_STATUS_CANCELED_BY_CA => "Annulée par Class Appart",
      self::CONSTANT_STATUS_CANCELED_BY_PROP => "Annulée par le propriétaire",
      self::CONSTANT_STATUS_CANCELED_BY_LOC => "Annulée par le locataire",
      self::CONSTANT_STATUS_CANCELED_BY_BANK => "Annulée par la banque",
    );

    return $mStatus;
  }

  public static function getStatus($status)
  {

    $a = self::getStatusArray();

    if (!array_key_exists($status, $a))
      return "AUCUN STATUT";
    else
      return $a[$status];
  }

}
