<?php

namespace Pat\CompteBundle\Tools;

use Doctrine\ORM\EntityManager;
use Pat\CompteBundle\Entity\Facture;
use Pat\CompteBundle\Lib\DateFunctions;

class FactureTools
{

  /**
   * @var EntityManager
   */
  private $em;

  /**
   * @var string
   */
  private $rootDir;

  /**
   * @var DateFunctions
   */
  private $dateFunctions;
  private $snappy;
  private $templating;

  /**
   * FactureTools constructor.
   *
   * @param               $em
   * @param               $rootDir
   * @param DateFunctions $dateFunctions
   * @param               $snappy
   * @param               $templating
   */
  public function __construct($em, $rootDir, DateFunctions $dateFunctions, $snappy, $templating)
  {
    $this->em = $em;
    $this->rootDir = $rootDir;
    $this->dateFunctions = $dateFunctions;
    $this->snappy = $snappy;
    $this->templating = $templating;
  }

  /**
   * @param $fileName
   *
   * @return string
   */
  public function getFacturePath($fileName)
  {
    return $this->rootDir.'/../factures/'.$fileName;
  }

  /**
   * @return string
   */
  public function generateNewFactureNumber()
  {
    $factureRepository = $this->em->getRepository('PatCompteBundle:Facture');

    $now = new \DateTime();

    $nbFactureThisYear = $factureRepository->countFacturesInYear($now->format('Y'));

    return $now->format('Y').'-'.str_pad($nbFactureThisYear + 1, 4, '0', STR_PAD_LEFT);
  }

  /**
   * @param \Pat\CompteBundle\Entity\Reservation $reservation
   * @param $locale
   * @param $factureNumber
   * @param $header
   * @param $footer
   * @param $comment
   * @param $CaAddress
   *
   * @return Facture
   */
  public function generateFacture($reservation, $locale, $factureNumber, $header, $footer, $comment, $CaAddress)
  {
    $utilisateur = $reservation->getUtilisateur();
    $filename = 'v'.strtoupper($locale).'-'.$factureNumber.'.pdf';
    $filePath = $this->getFacturePath($filename);

    $options = [];

    /** @var Option $option */
    foreach ($reservation->getOptions() as $option) {
      $options[] = [
        'quantity' => $option->getQuantity(),
        'name' => $option->getName(),
        'amount' => $option->getAmount(),
      ];
    }

    $facture = new Facture();

    $prenom = $utilisateur->getPrenom();
    $city = $utilisateur->getVilleFact() ? $utilisateur->getVilleFact() : $utilisateur->getVille();
    $address = $utilisateur->getAdresseFact() ? $utilisateur->getAdresseFact() : $utilisateur->getAdresse();
    $zipCode = $utilisateur->getCodePostalFact() ? $utilisateur->getCodePostalFact() : $utilisateur->getCodePostal();
    $country = $utilisateur->getPaysFact() ? $utilisateur->getPaysFact() : $utilisateur->getPays();
    $values = $reservation->getCalculValues();

    $values['sejour_debut'] = $reservation->getDateDebut();
    $values['sejour_fin'] = $reservation->getDateFin();
    $start = new \DateTime($reservation->getDateDebut());
    $end = new \DateTime($reservation->getDateFin());
    $interval = $start->diff($end);
    $values['sejour_nuits'] = $interval->days;
    $values['sejour_montant'] = $reservation->getTotalAmount();
    $values['sejour_tva'] = $facture->getVatAmount($values);

    $facture
      ->setReservation($reservation)
      ->setLocale($locale)
      ->setNumber($factureNumber)
      ->setHeader($header)
      ->setFooter($footer)
      ->setComment($comment)
      ->setCaAddress($CaAddress)
      ->setCalculValues($values)
      ->setNbAdultes($reservation->getNbAdulte())
      ->setOptions($options)
      ->setFirstName($prenom ? $prenom : '-')
      ->setLastName($utilisateur->getNom())
      ->setAddress($address ? $address : '-')
      ->setAddress2($utilisateur->getAdresse2Fact() ? $utilisateur->getAdresse2Fact() : $utilisateur->getAdresse2())
      ->setCity($city ? $city : '-')
      ->setZipCode($zipCode ? $zipCode : '-')
      ->setCountry($country ? $country : '-')
      ->setEmail($utilisateur->getEmail())
      ->setPhone($utilisateur->getTelephone())
      ->setSiret($utilisateur->getSiret())
      ->setPromotion($reservation->getPromotion())
      ->setDepot($reservation->getDepot())
      ->setFileName($filename)
    ;

    $reservation->addFacture($facture);

//    $nb_nuits = $this->dateFunctions->nbJours($reservation->getDateDebut(), $reservation->getDateFin());

    $html = $this->templating->render('PatCompteBundle:Facture:_new_facture_'.$locale.'.html.twig', array(
      'resa' => $reservation,
      'facture' => $facture,
      'values' => $values,
      'root' => $this->rootDir,
      'isAvoir' => false,
    ));

    if (file_exists($filePath)) {
      unlink($filePath);
    }

    $this->snappy->generateFromHtml(
      $html, $filePath
    );

    return $facture;
  }

  /**
   * @param Facture $facture
   * @param         $locale
   */
  public function generateAvoir(Facture $facture, $locale, $reservation)
  {
    $avoirFileName = 'v'.strtoupper($locale).'-'.$facture->getNumber().'-Avoir.pdf';
    $avoirFilePath = $this->getFacturePath($avoirFileName);

    $facture->setAvoirFileName($avoirFileName);

//    $nb_nuits = $this->dateFunctions->nbJours($facture->getReservation()->getDateDebut(), $facture->getReservation()->getDateFin());

    $html = $this->templating->render('PatCompteBundle:Facture:_new_facture_'.$locale.'.html.twig', array(
      'resa' => $reservation,
      'facture' => $facture,
      'values' => $facture->getCalculValues(),
      'root' => $this->rootDir,
      'isAvoir' => true,
    ));

    if (file_exists($avoirFilePath)) {
      unlink($avoirFilePath);
    }

    $this->snappy->generateFromHtml(
      $html, $avoirFilePath
    );
  }

}
