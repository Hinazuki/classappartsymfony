<?php

namespace Pat\CompteBundle\Tools;

class CalendrierTools
{

  const CONSTANT_STATUS_BLOQUE_RESERVATION = 0;
  const CONSTANT_STATUS_BLOQUE_PROPRIETAIRE = 1;
  const CONSTANT_STATUS_BLOQUE_ADMIN = 2;
  const CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE = 3;
  const CONSTANT_STATUS_BLOQUE_ADMIN_TRAVAUX = 4;
  const CONSTANT_COLOR_BLOQUE_RESERVATION = "YellowGreen";
  const CONSTANT_COLOR_BLOQUE_PROPRIETAIRE = "SteelBlue";
  const CONSTANT_COLOR_BLOQUE_ADMIN = "LightSeaGreen";
  const CONSTANT_COLOR_BLOQUE_ADMIN_MENAGE = "";
  const CONSTANT_COLOR_BLOQUE_ADMIN_TRAVAUX = "FireBrick";

  public static function getStatusArray()
  {

    $cStatus = array(
      self::CONSTANT_STATUS_BLOQUE_RESERVATION => "RESERVATION EN LIGNE",
      self::CONSTANT_STATUS_BLOQUE_PROPRIETAIRE => "BLOQUE PROPRIETAIRE",
      self::CONSTANT_STATUS_BLOQUE_ADMIN => "BLOQUE ADMIN",
      self::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE => "BLOQUE ADMIN MENAGE",
      self::CONSTANT_STATUS_BLOQUE_ADMIN_TRAVAUX => "BLOQUE ADMIN TRAVAUX"
    );

    return $cStatus;
  }

  public static function getStatus($status)
  {

    $a = self::getStatusArray();

    if (!array_key_exists($status, $a))
      return "AUCUN STATUT";
    else
      return $a[$status];
  }

}
