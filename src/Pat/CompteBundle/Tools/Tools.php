<?php

namespace Pat\CompteBundle\Tools;

class Tools
{

  public static $monthesEn = array(
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December');
  public static $monthesFr = array(
    'Janvier',
    'Février',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Août',
    'Septembre',
    'Octobre',
    'Novembre',
    'Décembre');

  /**
   * Translate a date in french (monthes)
   */
  public static function translateDate($dateStr)
  {
    return str_replace(self::$monthesEn, self::$monthesFr, $dateStr);
  }

}
