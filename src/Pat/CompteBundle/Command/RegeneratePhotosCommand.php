<?php

namespace Pat\CompteBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

require_once(__DIR__.'/../Lib/Image.php');

use Pat\CompteBundle\Lib\Image;
use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\CompteBundle\Entity\Appartement;
use Pat\CompteBundle\Entity\Media;
use Pat\CompteBundle\Form\MediaForm;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class RegeneratePhotosCommand extends ContainerAwareCommand
{

  protected function configure()
  {
    $this
      ->setName('compte:RegeneratePhotos')
      ->setDescription('Regenerate photos')
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $dir_root = __DIR__.'/../../../../web/images/photos/biens/';

    // On scan le dossier d'images
    if (is_dir($dir_root)) {
      $dir = opendir($dir_root);
      while (false !== ($folder = readdir($dir))) {
        if ($folder == "." || $folder == "..")
          continue;
        if (is_dir($dir_root.$folder) == true) {
          // On supprime les dossiers de miniatures et leur contenu
          if (rmdirr($dir_root.$folder.'/small') == false) {
            $output->writeln($folder." : Suppression impossible du dossier small");
            continue;
          }

          if (rmdirr($dir_root.$folder.'/big') == false) {
            $output->writeln($folder." : Suppression impossible du dossier big");
            continue;
          }

          // On recrée les dossiers
          if (mkdir($dir_root.$folder.'/small') == false)
            $errors[] = "Erreur de création du dossier small";

          if (mkdir($dir_root.$folder.'/big') == false)
            $errors[] = "Erreur de création du dossier big";

          $dir2 = opendir($dir_root.$folder);
          while (false !== ($file = readdir($dir2))) {
            // On ne traite que les images
            if (preg_match("/\.(png|jpe?g|gif)$/i", $file)) {
              $infos_image = getImageSize($dir_root.$folder.'/'.$file);
              // Redimensionnement de l'image et recadrage (big)
              if ($infos_image[0] == $infos_image[1]) {
                $h = 285;
                $w = 450;

                $y = 0;
                $x = ($w - 450) / 2;
              }
              else if ($infos_image[0] >= $infos_image[1]) {
                $h = 285;
                $w = 450 * $infos_image[0] / $infos_image[1];

                $y = 0;
                $x = ($w - 450) / 2;
              }
              else {
                $w = 450;
                $h = 285 * $infos_image[1] / $infos_image[0];

                $x = 0;
                $y = ($h - 285) / 2;
              }

              $img = Image::open($dir_root.$folder."/".$file)
                ->resize($w, $h)
                ->crop($x, $y, 450, 285)
                ->save($dir_root.$folder.'/big/'.$file);


              // Redimensionnement de l'image et recadrage (small)
              if ($infos_image[0] == $infos_image[1]) {
                $h = 120;
                $w = 120;

                $y = 0;
                $x = ($w - 120) / 2;
              }
              else if ($infos_image[0] >= $infos_image[1]) {
                $h = 120;
                $w = 120 * $infos_image[0] / $infos_image[1];

                $y = 0;
                $x = ($w - 120) / 2;
              }
              else {
                $w = 120;
                $h = 120 * $infos_image[1] / $infos_image[0];

                $x = 0;
                $y = ($h - 120) / 2;
              }

              Image::open($dir_root.$folder."/".$file)
                ->resize($w, $h)
                ->crop($x, $y, 120, 120)
                ->save($dir_root.$folder.'/small/'.$file);
            }
          }
          $output->writeln($folder." : OK");
        }
      }
    }
    else
      $output->writeln("Le dossier n'existe pas");
  }

}
