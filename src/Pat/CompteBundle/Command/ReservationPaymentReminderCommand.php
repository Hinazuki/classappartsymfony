<?php

namespace Pat\CompteBundle\Command;

use Pat\CompteBundle\Tools\ReservationTools;
use Pat\CompteBundle\Tools\PaymentTools;
use Pat\CompteBundle\Entity\Payment;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReservationPaymentReminderCommand extends ContainerAwareCommand
{

  private $parsedReservations = [];
  private $output;
  private $adminMail;
  private $em;
  private $reservationManager;
  private $twig;

  protected function configure()
  {
    $this
      ->setName('class-appart:reservation:reminder:payment')
      ->setDescription('Send email to Locataire when booking is not payed after 48h');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $this->output = $output;
    $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
    $this->reservationManager = $this->getContainer()->get('pat.reservation_manager');
    $this->twig = $this->getContainer()->get('templating');
    $this->adminMail = 'info@class-appart.com';


    //// ---------------------------------------------------------------------------------------------------------- //
    $output->writeln('48h reminder');

    $this->processFirstReminder();
    //// ---------------------------------------------------------------------------------------------------------- //
    $output->writeln('2 week reminder');

    $this->processOtherReminder();

    $output->writeln('5 days before reminder');

    $this->processFiveDayBeforeReminder();
  }

  private function processFirstReminder()
  {
    $parsedReservations = [];
    //
        $reservations = $this->reservationManager->getBookingNotPayedAfterTwoDays();

    //
      foreach ($reservations as $reservation) {
    //
        $this->parsedReservations[] = $reservation->getId();
        $parsedReservations[] = $reservation;
        $solde = $reservation->getTarif();
        $final_solde = $reservation->getTarif();
        $payments = $reservation->getPayments();
        $urls = [];
        foreach($payments as $payment){
          if($payment->getStatus() == PaymentTools::CONSTANT_STATUS_PAYED){
            $final_solde -= $payment->getAmount();
          }elseif($payment->getStatus() == PaymentTools::CONSTANT_STATUS_WAIT){
            $urls[] = [PaymentTools::BASE_URL_GENERATE_LINK.$this->getContainer()
            ->get('router')
            ->generate('pat_payment', array('token' => $payment->getToken())), $payment];
          }
          $solde -= $payment->getAmount();
        }

        if($solde > 0 || $solde == $reservation->getTarif()){
          $payment = new Payment();
          $payment->setAmount($solde);
          $payment->setName('solde');
          $payment->setMsgContent("Paiement de la somme de ".$solde."€");
          $payment->setMember($reservation->getUtilisateur());
          $payment->setReservation($reservation);

          $this->em->persist($payment);
          $this->em->flush();

          $urls[] = [PaymentTools::BASE_URL_GENERATE_LINK.$this->getContainer()
          ->get('router')
          ->generate('pat_payment', array('token' => $payment->getToken())), $payment];
        }
        
        $htmlBody = $this->twig->render(
            'PatCompteBundle:Reservation:email_notif_payment.html.twig',
            [
                'reservation' => $reservation,
                'solde' => $final_solde,
                'urls' => $urls
            ]);

        $textBody = $this->twig->render(
            'PatCompteBundle:Reservation:email_notif_payment.txt.twig',
            [
                'reservation' => $reservation,
                'solde' => $final_solde,
                'urls' => $urls
            ]);
    
          $cc = null;
    
          if ($reservation->getUtilisateur()->getSecondEmail()) {
            $cc = $reservation->getUtilisateur()->getSecondEmail();
          }

          try{
            $this->sendMail(
              $reservation->getUtilisateur()->getEmail(),
                'Demande de paiment pour réservation Class Appart (Rappel 2 jours)',
              $htmlBody,
              $textBody,
              $cc);
          }catch(\Exception $e){
            return false;
          }
          $date = new \DateTime();
          $reservation->setNotificationPayment($date->format('Y-m-d'));
          $this->em->flush();
    }

        $this->output->writeLn(count($parsedReservations) . ' reminded for first time');
  }

  private function processOtherReminder()
  {
    $parsedReservations = [];
    //
      $reservations = $this->reservationManager->getBookingNotPayedAfterTwoWeeks();

    //
      foreach ($reservations as $reservation) {
    //
        $this->parsedReservations[] = $reservation->getId();
        $parsedReservations[] = $reservation;
        $solde = $reservation->getTarif();
        $payments = $reservation->getPayments();
        $urls = [];
        foreach($payments as $payment){
          if($payment->getStatus() == PaymentTools::CONSTANT_STATUS_PAYED){
            $solde -= $payment->getAmount();
          }elseif($payment->getStatus() == PaymentTools::CONSTANT_STATUS_WAIT){
            $urls[] = [PaymentTools::BASE_URL_GENERATE_LINK.$this->getContainer()
            ->get('router')
            ->generate('pat_payment', array('token' => $payment->getToken())), $payment];
          }
        }

    //
        $htmlBody = $this->twig->render(
            'PatCompteBundle:Reservation:email_notif_payment.html.twig',
            [
                'reservation' => $reservation,
                'solde' => $solde,
                'urls' => $urls
            ]);

        $textBody = $this->twig->render(
            'PatCompteBundle:Reservation:email_notif_payment.txt.twig',
            [
                'reservation' => $reservation,
                'solde' => $solde,
                'urls' => $urls
            ]);
    
          $cc = null;
    
          if ($reservation->getUtilisateur()->getSecondEmail()) {
            $cc = $reservation->getUtilisateur()->getSecondEmail();
          }
    
          try{
            $this->sendMail(
              $reservation->getUtilisateur()->getEmail(),
                'Demande de paiment pour réservation Class Appart (Rappel 2 semaines)',
              $htmlBody,
              $textBody,
              $cc);
          }catch(\Exception $e){
            return $this->output->writeLn($e);
          }
          $date = new \DateTime();
          $reservation->setNotificationPayment($date->format('Y-m-d'));
          $this->em->flush();
    }
        $this->output->writeLn(count($parsedReservations) . ' reminded after two weeks');
  }

  private function processFiveDayBeforeReminder()
  {
    $parsedReservations = [];
    //
        $reservations = $this->reservationManager->getBookingNotPayedAfterTwoDays();
    //
      foreach ($reservations as $reservation) {
    //
        $this->parsedReservations[] = $reservation->getId();
        $parsedReservations[] = $reservation;
        $solde = $reservation->getTarif();
        $final_solde = $reservation->getTarif();
        $payments = $reservation->getPayments();
        $urls = [];
        foreach($payments as $payment){
          if($payment->getStatus() == PaymentTools::CONSTANT_STATUS_PAYED){
            $final_solde -= $payment->getAmount();
          }elseif($payment->getStatus() == PaymentTools::CONSTANT_STATUS_WAIT){
            $urls[] = [PaymentTools::BASE_URL_GENERATE_LINK.$this->getContainer()
            ->get('router')
            ->generate('pat_payment', array('token' => $payment->getToken())), $payment];
          }
          $solde -= $payment->getAmount();
        }

        if($solde > 0 || $solde == $reservation->getTarif()){
          $payment = new Payment();
          $payment->setAmount($solde);
          $payment->setName('solde');
          $payment->setMsgContent("Paiement de la somme de ".$solde."€");
          $payment->setMember($reservation->getUtilisateur());
          $payment->setReservation($reservation);

          $this->em->persist($payment);
          $this->em->flush();

          $urls[] = [PaymentTools::BASE_URL_GENERATE_LINK.$this->getContainer()
          ->get('router')
          ->generate('pat_payment', array('token' => $payment->getToken())), $payment];
        }
        
        $htmlBody = $this->twig->render(
            'PatCompteBundle:Reservation:email_notif_payment.html.twig',
            [
                'reservation' => $reservation,
                'solde' => $final_solde,
                'urls' => $urls
            ]);

        $textBody = $this->twig->render(
            'PatCompteBundle:Reservation:email_notif_payment.txt.twig',
            [
                'reservation' => $reservation,
                'solde' => $final_solde,
                'urls' => $urls
            ]);
    
          $cc = null;
    
          if ($reservation->getUtilisateur()->getSecondEmail()) {
            $cc = $reservation->getUtilisateur()->getSecondEmail();
          }

          try{
            $this->sendMail(
              $reservation->getUtilisateur()->getEmail(),
                'Demande de paiment pour réservation Class Appart (Rappel avant début)',
              $htmlBody,
              $textBody,
              $cc);
          }catch(\Exception $e){
            return false;
          }
    }

        $this->output->writeLn(count($parsedReservations) . ' reminded for first time');
  }

  /**
   * @param      $to
   * @param      $subject
   * @param      $htmlBody
   * @param      $textBody
   * @param null $cc
   *
   * @return bool
   */
  private function sendMail($to, $subject, $htmlBody, $textBody, $cc = null)
  {
    try {
      $message_email = \Swift_Message::newInstance()
        ->setSubject($subject)
        ->setFrom('contact@class-appart.com')
        ->setTo($to)
        ->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain');

      if (null !== $cc) {
        $message_email->setCc($cc);
      }

      $this->getContainer()->get('mailer')->send($message_email);
    }
    catch (\Exception $e) {
      return $this->output->writeLn($e);
    }
  }

}
