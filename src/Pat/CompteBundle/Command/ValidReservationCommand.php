<?php

namespace Pat\CompteBundle\Command;

use Pat\CompteBundle\Tools\ReservationTools;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ValidReservationCommand extends ContainerAwareCommand
{

  public function configure()
  {
    $this
      ->setName('class-appart:reservation:auto-valid')
      ->setDescription('Automatically valid Réservation after 48H');
  }

  public function execute(InputInterface $input, OutputInterface $output)
  {
    $output->writeln('Valid réservation after 48H');

    $em = $this->getContainer()->get('doctrine.orm.entity_manager');
    $reservationManager = $this->getContainer()->get('pat.reservation_manager');
    $twig = $this->getContainer()->get('templating');

    $reservations = $reservationManager->getReservationNotValidated();

    $output->writeln(count($reservations).' found');

    foreach ($reservations as $reservation) {
      // TODO: Ne pas utiliser cette constante
      $reservation->setValidProp(ReservationTools::CONSTANT_VALID_RESA_48);

      $em->flush();

      $user = $reservation->getAppartement()->getUtilisateur();

      $to = $user->getEmail();
      $cc = null;
      $subject = 'Confirmation de réservation Class Appart';

      $htmlBody = $twig->render('PatCompteBundle:Reservation:email_valid_command.html.twig', [
        'reservation' => $reservation,
        'statut' => 'validé',
      ]);

      $textBody = $twig->render('PatCompteBundle:Reservation:email_valid_command.txt.twig', [
        'reservation' => $reservation,
        'statut' => 'validé',
      ]);

      if ($user->getSecondEmail()) {
        $cc = $user->getSecondEmail();
      }

      // Send Mail to Locataire
      $this->sendMail($to, $subject, $htmlBody, $textBody, $cc);

      // Send Mail to Admin
      $to = 'info@class-appart.com';
      $subject = 'Validation automatique de réservation Class Appart';

      $this->sendMail($to, $subject, $htmlBody, $textBody);
    }
  }

  /**
   * @param      $to
   * @param      $subject
   * @param      $htmlBody
   * @param      $textBody
   * @param null $cc
   */
  private function sendMail($to, $subject, $htmlBody, $textBody, $cc = null)
  {
    $message_email = \Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom('contact@class-appart.com')
      ->setTo($to)
      ->setBody($htmlBody, 'text/html')
      ->addPart($textBody, 'text/plain');

    if (null !== $cc) {
      $message_email->setCc($cc);
    }

    $this->getContainer()->get('mailer')->send($message_email);
  }

}
