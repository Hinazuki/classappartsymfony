<?php

namespace Pat\CompteBundle\Command;

use Pat\CompteBundle\Tools\ReservationTools;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReservationCancelerCommand extends ContainerAwareCommand
{

  private $parsedReservations = [];
  private $output;
  private $adminMail;
  private $em;
  private $reservationManager;
  private $twig;

  protected function configure()
  {
    $this
      ->setName('class-appart:reservation:canceler')
      ->setDescription('Cancel booking after 48hour with deposit not payed');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $this->output = $output;
    $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
    $this->reservationManager = $this->getContainer()->get('pat.reservation_manager');
    $this->twig = $this->getContainer()->get('templating');
    $this->adminMail = 'info@class-appart.com';


    // ---------------------------------------------------------------------------------------------------------- //
    $output->writeln('48h canceler');

    $this->processCanceler();
  }

  private function processCanceler()
  {
    $parsedReservations = [];

    $reservations = $this->reservationManager->getBookingWithDepositNotPayedForCanceled();

    foreach ($reservations as $reservation) {
      if (true === in_array($reservation->getId(), $this->parsedReservations)) {
        continue;
      }
      $reservation->setStatus(ReservationTools::CONSTANT_STATUS_CANCELED_BY_CA);
      $this->parsedReservations[] = $reservation->getId();
      $parsedReservations[] = $reservation;

      $htmlBody = $this->twig->render(
        'PatCompteBundle:Reservation:email_cancel.html.twig', [
        'reservation' => $reservation
        ]
      );
      $textBody = $this->twig->render(
        'PatCompteBundle:Reservation:email_cancel.txt.twig', [
        'reservation' => $reservation
        ]
      );

      $cc = null;

      if ($reservation->getUtilisateur()->getSecondEmail()) {
        $cc = $reservation->getUtilisateur()->getSecondEmail();
      }

      $this->sendMail(
        $reservation->getUtilisateur()->getEmail(), 'Annulation de réservation chez Class Appart', $htmlBody, $textBody, $cc
      );

      $this->em->flush();
    }

    if (count($parsedReservations) > 0) {
      $htmlBody = $this->twig->render(
        'PatCompteBundle:Reservation:email_cancel_admin.html.twig', [
        'reservations' => $parsedReservations
        ]
      );
      $textBody = $this->twig->render(
        'PatCompteBundle:Reservation:email_cancel_admin.txt.twig', [
        'reservations' => $parsedReservations
        ]
      );

      $this->sendMail($this->adminMail, "Annulations réservations", $htmlBody, $textBody);
    }

    $this->output->writeLn(count($parsedReservations).' canceled');
  }

  /**
   * @param      $to
   * @param      $subject
   * @param      $htmlBody
   * @param      $textBody
   * @param null $cc
   *
   * @return bool
   */
  private function sendMail($to, $subject, $htmlBody, $textBody, $cc = null)
  {
    try {
      $message_email = \Swift_Message::newInstance()
        ->setSubject($subject)
        ->setFrom('contact@class-appart.com')
        ->setTo($to)
        ->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain');

      if (null !== $cc) {
        $message_email->setCc($cc);
      }

      $this->getContainer()->get('mailer')->send($message_email);
    }
    catch (\Exception $e) {
      return false;
    }
  }

}
