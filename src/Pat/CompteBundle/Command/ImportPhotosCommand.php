<?php

namespace Pat\CompteBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

require_once(__DIR__.'/../Lib/Image.php');

use Pat\CompteBundle\Lib\Image;
use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\CompteBundle\Entity\Appartement;
use Pat\CompteBundle\Entity\Media;
use Pat\CompteBundle\Form\MediaForm;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImportPhotosCommand extends ContainerAwareCommand
{

  protected function configure()
  {
    $this
      ->setName('compte:ImportPhotos')
      ->setDescription('Import and resize photos')
      ->addArgument('folder', InputArgument::REQUIRED, 'Path to the folder')
      ->addOption('resetall', null, InputOption::VALUE_NONE, 'If set, the task will delete all previous image before adding news')
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $em = $this->getContainer()->get('doctrine')->getManager();

    //upload file directory
    $dir_img = __DIR__.'/../../../../'.$input->getArgument('folder').'/';
    $dir_root = __DIR__.'/../../../../web/images/photos/biens/';

    if ($input->getOption('resetall')) {
      // On vide la table
      $connection = $em->getConnection();
      $platform = $connection->getDatabasePlatform();
      $connection->executeUpdate($platform->getTruncateTableSQL('Media'));

      // et on supprime le contenu du répertoire
      rmdirr($dir_root); // Supprime le répertoire et son contenu
      mkdir($dir_root); // On recrée donc le répertoire
    }

    //$biens = $em->getRepository('PatCompteBundle:Appartement')->findAll();
    //$biens = $em->createQuery('SELECT a FROM Pat\CompteBundle\Entity\Appartement a WHERE a.reference < 60')->getResult();

    if (is_dir($dir_img)) {
      $dir2 = opendir($dir_img);
      while (false !== ($folder = readdir($dir2))) {
        if ($folder == "." || $folder == "..")
          continue;
        if (is_dir($dir_img.$folder) == true) {
          if ($bien = $em->getRepository('PatCompteBundle:Appartement')->findOneByReference($folder)) {
            $errors = array();
            $photos = array();

            $ref_appart = $bien->getReference();

            if (!is_dir($dir_root.$ref_appart)) {
              if (mkdir($dir_root.$ref_appart) == false)
                $errors[] = "Erreur de création du dossier";
            }

            // On crée le dossier small
            if (!is_dir($dir_root.$ref_appart.'/small') == true) {
              if (mkdir($dir_root.$ref_appart.'/small') == false)
                $errors[] = "Erreur de création du dossier small";
            }


            // On crée le dossier big
            if (!is_dir($dir_root.$ref_appart.'/big') == true) {
              if (mkdir($dir_root.$ref_appart.'/big') == false)
                $errors[] = "Erreur de création du dossier big";
            }

            // On recherche les éventuelles erreurs
            $dir = opendir($dir_img.$ref_appart);

            // On récupère le dernier indice pour ajouter à la suite
            $query = $em->createQuery('SELECT m.fichier FROM Pat\CompteBundle\Entity\Media m WHERE m.appartement = :appart ORDER BY m.fichier DESC');
            $query->setParameter('appart', $bien);
            $query->setMaxResults(1);
            $last_media = $query->getResult();
            if ($last_media) {
              $tmp = explode('.', $last_media[0]['fichier']);
              $last_ind = explode('-', $tmp[0]);
              $last_ind = $last_ind[2];
              $i = ord($last_ind) - 95;
            }
            else
              $i = 1;


            while (false !== ($file = readdir($dir))) {
              if (preg_match("/\.(png|jpe?g|gif)$/i", $file)) {
                /* if(!in_array($file->guessType(), array('jpg', 'jpeg', 'png', 'gif')))
                  $errors[] = "Format de photo incorrect"; */

                $infos_image = getImageSize($dir_img.$ref_appart.'/'.$file);
                if ($infos_image[0] > 2500)
                  $errors[] = "Largeur de photo supérieure à 2500px";

                if (empty($infos_image[0]) or empty($infos_image[1]))
                  $errors[] = "Largeur ou hauteur de photo nulle";

                if (filesize($dir_img.$ref_appart.'/'.$file) > 5000000)
                  $errors[] = "Photo supérieure à 5 Mo";

                $mime_type = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $dir_img.$ref_appart.'/'.$file);
                if ($mime_type == 'image/jpeg')
                  $extension = 'jpg';
                elseif ($mime_type == 'image/png')
                  $extension = 'png';
                elseif ($mime_type == 'image/gif')
                  $extension = 'gif';
                else
                  $errors[] = "Format de photo incorrect (MIME type incorrect)";


                if (empty($errors)) {
                  //on conserve la version de base de la photo

                  $nom_fichier = 'pat-'.$ref_appart.'-'.chr($i + 96).'.'.$extension;

                  copy($dir_img.$ref_appart.'/'.$file, $dir_root.$ref_appart.'/'.$nom_fichier);

                  // Redimensionnement de l'image et recadrage (big)
                  if ($infos_image[0] == $infos_image[1]) {
                    $h = 285;
                    $w = 450;

                    $y = 0;
                    $x = ($w - 450) / 2;
                  }
                  else if ($infos_image[0] >= $infos_image[1]) {
                    $h = 285;
                    $w = 450 * $infos_image[0] / $infos_image[1];

                    $y = 0;
                    $x = ($w - 450) / 2;
                  }
                  else {
                    $w = 450;
                    $h = 285 * $infos_image[1] / $infos_image[0];

                    $x = 0;
                    $y = ($h - 285) / 2;
                  }

                  $img = Image::open($dir_root.$ref_appart."/".$nom_fichier)
                    ->resize($w, $h)
                    ->crop($x, $y, 450, 285)
                    ->save($dir_root.$ref_appart.'/big/'.$nom_fichier);


                  // Redimensionnement de l'image et recadrage (small)
                  if ($infos_image[0] == $infos_image[1]) {
                    $h = 120;
                    $w = 120;

                    $y = 0;
                    $x = ($w - 120) / 2;
                  }
                  else if ($infos_image[0] >= $infos_image[1]) {
                    $h = 120;
                    $w = 120 * $infos_image[0] / $infos_image[1];

                    $y = 0;
                    $x = ($w - 120) / 2;
                  }
                  else {
                    $w = 120;
                    $h = 120 * $infos_image[1] / $infos_image[0];

                    $x = 0;
                    $y = ($h - 120) / 2;
                  }

                  Image::open($dir_root.$ref_appart."/".$nom_fichier)
                    ->resize($w, $h)
                    ->crop($x, $y, 120, 120)
                    ->save($dir_root.$ref_appart.'/small/'.$nom_fichier);

                  //enregistrement
                  $photos[$i] = new Media();
                  $photos[$i]->setFichier($nom_fichier);
                  $photos[$i]->setTitre("");
                  $photos[$i]->setTri($i);
                  $photos[$i]->setAppartement($bien);
                  $em->persist($photos[$i]);

                  $i++;
                }
              }
            } // end while
            // S'il y a des erreurs on les affiche et on s'arrête
            if (!empty($errors)) {
              $output->writeln("Bien ref.".$ref_appart);
              foreach ($errors as $error)
                $output->writeln($error);
              $output->writeln('');
              unset($errors);
            }
            // Sinon on continue le traitement des photos
            else {
              $output->writeln("Bien ref.".$ref_appart." : OK");
              $output->writeln('');

              $em->flush();
              foreach ($photos as $photo)
                $em->detach($photo);
              unset($photos);
            }
          }
          else
            $output->writeln("Aucun bien ne correspond à la référence ".$folder);
        }
      }
    }
    else
      $output->writeln("Le dossier n'existe pas");

    //$output->writeln($dir);
  }

}

/**
 * Delete a file, or a folder and its contents (recursive algorithm)
 *
 * @author      Aidan Lister <aidan@php.net>
 * @version     1.0.3
 * @link        http://aidanlister.com/repos/v/function.rmdirr.php
 * @param       string   $dirname    Directory to delete
 * @return      bool     Returns TRUE on success, FALSE on failure
 */
function rmdirr($dirname)
{
  // Sanity check
  if (!file_exists($dirname)) {
    return false;
  }

  // Simple delete for a file
  if (is_file($dirname) || is_link($dirname)) {
    return unlink($dirname);
  }

  // Loop through the folder
  $dir = dir($dirname);
  while (false !== $entry = $dir->read()) {
    // Skip pointers
    if ($entry == '.' || $entry == '..') {
      continue;
    }

    // Recurse
    rmdirr($dirname.DIRECTORY_SEPARATOR.$entry);
  }

  // Clean up
  $dir->close();
  return rmdir($dirname);
}
