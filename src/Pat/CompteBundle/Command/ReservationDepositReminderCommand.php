<?php

namespace Pat\CompteBundle\Command;

use Pat\CompteBundle\Tools\ReservationTools;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReservationDepositReminderCommand extends ContainerAwareCommand
{

  private $parsedReservations = [];
  private $output;
  private $adminMail;
  private $em;
  private $reservationManager;
  private $twig;

  protected function configure()
  {
    $this
      ->setName('class-appart:reservation:reminder:deposit')
      ->setDescription('Send email to Locataire when deposit is not payed after 1h, 24h and 48h');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $this->output = $output;
    $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
    $this->reservationManager = $this->getContainer()->get('pat.reservation_manager');
    $this->twig = $this->getContainer()->get('templating');
    $this->adminMail = 'info@class-appart.com';


    // ---------------------------------------------------------------------------------------------------------- //
    $output->writeln('48h reminder');

    $date = new \DateTime('-48 hours');

    $this->processReminder($date, '48H', ReservationTools::NOTIFICATION_ACOMPTE_48H);

    // ---------------------------------------------------------------------------------------------------------- //
    $output->writeln('24h reminder');

    $date = new \DateTime('-24 hours');

    $this->processReminder($date, '24H', ReservationTools::NOTIFICATION_ACOMPTE_24H);

    // ---------------------------------------------------------------------------------------------------------- //
    $output->writeln('1h reminder');

    $date = new \DateTime('-1 hour');

    $this->processReminder($date, '1H', ReservationTools::NOTIFICATION_ACOMPTE_1H);
  }

  private function processReminder($time, $timeStr, $flag)
  {
    $parsedReservations = [];

    $reservations = $this->reservationManager->getReservationWithDepositNotPayed($time, $flag);

    foreach ($reservations as $reservation) {
      if (true === in_array($reservation->getId(), $this->parsedReservations)) {
        continue;
      }

      $this->parsedReservations[] = $reservation->getId();
      $parsedReservations[] = $reservation;

      $htmlBody = $this->twig->render(
        'PatCompteBundle:Reservation:email_relance_accompte.html.twig', [
        'reservation' => $reservation,
        'time' => $timeStr,
        ]
      );
      $textBody = $this->twig->render(
        'PatCompteBundle:Reservation:email_relance_accompte.txt.twig', [
        'reservation' => $reservation,
        'time' => $timeStr
        ]
      );

      $cc = null;
      
      if ($reservation->getUtilisateur()->getSecondEmail()) {
        $cc = $reservation->getUtilisateur()->getSecondEmail();
      }

      $this->sendMail(
        $reservation->getUtilisateur()->getEmail(), 
        'Relance demande acompte pour réservation Class Appart',
        $htmlBody, 
        $textBody,
        $cc
      );

      $reservation->setNotificationAcompte($flag);

      $this->em->flush();
    }

    if (count($parsedReservations) > 0) {
      $htmlBody = $this->twig->render(
        'PatCompteBundle:Reservation:email_relance_accompte_admin.html.twig', [
        'reservations' => $parsedReservations,
        'time' => $timeStr
        ]
      );
      $textBody = $this->twig->render(
        'PatCompteBundle:Reservation:email_relance_accompte_admin.txt.twig', [
        'reservations' => $parsedReservations,
        'time' => $timeStr
        ]
      );

      $this->sendMail($this->adminMail, "Notification demande d'accompte (".$timeStr.') Class Appart', $htmlBody, $textBody);
    }

    $this->output->writeLn(count($parsedReservations).' reminded');
  }

  /**
   * @param      $to
   * @param      $subject
   * @param      $htmlBody
   * @param      $textBody
   * @param null $cc
   *
   * @return bool
   */
  private function sendMail($to, $subject, $htmlBody, $textBody, $cc = null)
  {
    try {
      $message_email = \Swift_Message::newInstance()
        ->setSubject($subject)
        ->setFrom('contact@class-appart.com')
        ->setTo($to)
        ->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain');

      if (null !== $cc) {
        $message_email->setCc($cc);
      }

      $this->getContainer()->get('mailer')->send($message_email);
    }
    catch (\Exception $e) {
      return false;
    }
  }

}
