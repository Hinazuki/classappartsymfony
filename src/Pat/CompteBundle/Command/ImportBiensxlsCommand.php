<?php

namespace Pat\CompteBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityRepository;
use Pat\CompteBundle\Entity\Piece;
use Pat\CompteBundle\Entity\TypePiece;
use Pat\CompteBundle\Entity\Appartement;
use Pat\CompteBundle\Entity\Ville;
use Pat\CompteBundle\Entity\Tarif;
use Pat\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PHPExcel;
use PHPExcel_IOFactory;

function stripAccents($string)
{
  return str_replace(
    array(
    'à', 'â', 'ä', 'á', 'ã', 'å',
    'î', 'ï', 'ì', 'í',
    'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
    'ù', 'û', 'ü', 'ú',
    'é', 'è', 'ê', 'ë',
    'ç', 'ÿ', 'ñ',
    ), array(
    'a', 'a', 'a', 'a', 'a', 'a',
    'i', 'i', 'i', 'i',
    'o', 'o', 'o', 'o', 'o', 'o',
    'u', 'u', 'u', 'u',
    'e', 'e', 'e', 'e',
    'c', 'y', 'n',
    ), $string
  );
}

class ImportBiensxlsCommand extends ContainerAwareCommand
{

  protected function configure()
  {
    $this
      ->setName('compte:ImportBiensxls')
      ->setDescription('Import Appartements XLS file')
      ->addArgument('file', InputArgument::REQUIRED, 'Path to the xls file')
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $fichier_excel = $input->getArgument('file');

    //$erreurs_par_ligne = array();
    //$erreurs_par_colonne = null;
    $nb_ajouts = 0;

    $em = $this->getContainer()->get('doctrine')->getManager();

    /* $user_context = $this->getContainer()->get('security.context')->getToken()->getUser();
      $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
      if(!$user)
      {
      throw new AccessDeniedException('This user does not have access to this section.');
      } */

    $dir = __DIR__.'/../../../../web/excel/';


    /**  Identify the type of $inputFileName  * */
    $inputFileType = PHPExcel_IOFactory::identify($dir.$fichier_excel);
    /**  Create a new Reader of the type that has been identified  * */
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    /**  Advise the Reader that we only want to load cell data  * */
    $objReader->setReadDataOnly(true);
    /**  Load $inputFileName to a PHPExcel Object  * */
    $objPHPExcel = $objReader->load($dir.$fichier_excel);

    $worksheet = $objPHPExcel->getActiveSheet();
    $nbLignes = $worksheet->getHighestRow();
    //$nbColonnes = $worksheet->getHighestColumn();

    $data = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);


    // On parcours les lignes (Les lignes 1 et 2 servent aux intitulés excel)
    for ($i = 3; $i <= $nbLignes; $i++) {

      $info = "";
      $bloque_ajout = false;
      $erreurs = array();

      //$erreurs_par_ligne[$i] = array();
      //$erreurs_par_ligne[$i]['ligne'] = $i;
      //$erreurs_par_colonne = array();
      // On vérifie si la référence du bien n'existe pas déjà
      $info = $data[$i]['B'];
      if ($info == null)
        $erreurs['B'] = "Référence nulle";
      else {
        $bien = $em->getRepository('PatCompteBundle:Appartement')->findOneByReference($info);
        if ($bien != null) {
          $erreurs['B'] = "La référence de ce bien existe déjà";
        }
      }


      // On recherche le propriétaire du bien
      if (!$erreurs) {
        $info = $data[$i]['C'];
        $proprietaire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneByUsername($info);
        if (!$proprietaire)
          $erreurs['C'] = "Propriétaire inconnu";
      }


      if (!$erreurs) {
        $bien = new Appartement();
        $bien->setUtilisateur($proprietaire);

        $bien->setStatutDiagnostic(2);
        $bien->setStatut(4);
        $bien->setCmc(1);
        $bien->setIsSelection(0);
        $bien->setIsResa(0);
        //$bien->setIsLabeliser(0);
        $bien->setIsReloger(0);

        $bien->setCreatedAt(date("Y-m-d H:i:s"));
        $bien->setUpdatedAt(date("Y-m-d H:i:s"));

        $bien->setReference($data[$i]['B']);

        $bien->setNomResidence($data[$i]['E']);

        if ($data[$i]['F'] != null)
          $bien->setDescriptionCourteFr($data[$i]['F']);
        else
          $erreurs['F'] = "Veuillez renseigner une description courte du bien";

        if ($data[$i]['G'] != null)
          $bien->setDescriptionFr($data[$i]['G']);
        else
          $erreurs['G'] = "Veuillez renseigner une description du bien";

        if ($data[$i]['H'] != null)
          $bien->setAdresse($data[$i]['H']);
        else
          $erreurs['H'] = "Veuillez renseigner une adresse";


        // Ville
        $nom_ville = $data[$i]['I'];
        $cp = $data[$i]['J'];
        if ($info != null) {
          $ville = $em->getRepository('PatCompteBundle:Ville')->findBy(array('code_postal' => $cp));

          // Si la ville n'existe pas on la crée
          if (!$ville) {
            $ville = new Ville();
            $ville->setNom($nom_ville);
            $ville->setCodePostal($cp);
            $ville->setPays($data[$i]['K']);

            $em->persist($ville);

            $bien->setVille($ville);
          }
          else {
            // Si plus d'une ville possèdent ce code postal, on recherche le nom
            if (sizeof($ville) > 1) {
              $villes = $ville;
              unset($ville);

              foreach ($villes as $v) {
                if ($v->getNom() == $nom_ville) {
                  $ville = $v;
                  $bien->setVille($ville);
                }
              }
              if (!isset($ville)) {
                $ville = new Ville();
                $ville->setNom($nom_ville);
                $ville->setCodePostal($cp);
                $ville->setPays($data[$i]['K']);

                $em->persist($ville);

                $bien->setVille($ville);
              }
            }
            // S'il n'y a qu'une ville correspondante
            elseif (sizeof($ville) == 1)
              $bien->setVille($ville[0]);
            else
              $erreurs['J'] = "Erreur lors de la recherche de la ville";
          }
        }
        else
          $erreurs['J'] = "Veuillez renseigner une ville";



        $info = strtoupper(stripAccents(trim($data[$i]['L'])));
        if (in_array($info, array('APPARTEMENT', 'MAISON', 'STUDIO', 'LOFT', 'GARAGE', 'CHAMBRE')))
          $bien->setType($info);
        else
          $erreurs['L'] = "Type de bien incorrect";


        $info = $data[$i]['M'];
        if ($info == false || $info == true)
          $bien->setIsMeuble($info);
        else
          $bien->setIsMeuble(null);


        $info = $data[$i]['N'];
        if (in_array($info, array('Clef', 'Code', null)))
          $bien->setEntreeImmeuble($info);
        else
          $erreurs['N'] = "Entrée immeuble incorrecte";


        $info = $data[$i]['O'];
        if ($info == false || $info == true)
          $bien->setSerrure($info);
        else
          $bien->setSerrure(null);


        $info = $data[$i]['P'];
        if ($info == null || is_numeric($info))
          $bien->setSurfaceCarrez($info);
        else
          $erreurs['P'] = "Surface Carrez incorrecte";


        $info = $data[$i]['Q'];
        if (is_numeric($info) && !is_null($info))
          $bien->setSurfaceSol($info);
        else
          $erreurs['Q'] = "Surface incorrecte";


        $info = $data[$i]['R'];
        if ($info == null || is_numeric($info))
          $bien->setSurfaceTerrain($info);
        else
          $erreurs['R'] = "Surface terrain incorrecte";


        // Si un loyer tout compris est renseigné on l'utilise
        $info = is_null($data[$i]['T']) ? $data[$i]['S'] : $data[$i]['T'];
        if (is_numeric($info)) {
          $loyer_base = new Tarif();
          $loyer_base->setType('base');
          $loyer_base->setAppartement($bien);
          $loyer_base->setLoyer($info);
          $loyer_base->setIsToutCompris(is_null($data[$i]['T']) ? false : true);

          $loyer_base->setCreatedAt(date_create(date("Y-m-d H:i:s")));
          $loyer_base->setUpdatedAt(date_create(date("Y-m-d H:i:s")));

          $em->persist($loyer_base);
        }
        else
          $erreurs['S'] = "Loyer de base incorrecte";


        // Si au moins un des champs n'est pas nul
        if (!is_null($data[$i]['U']) || !is_null($data[$i]['V']) || !is_null($data[$i]['W'])) {
          // On s'assure que les 3 champs sont renseignés
          if (!is_null($data[$i]['U']) && !is_null($data[$i]['V']) && !is_null($data[$i]['W'])) {
            $loyer_periode = new Tarif();
            $loyer_periode->setType('periode');
            $loyer_periode->setAppartement($bien);
            $loyer_periode->setLoyer($data[$i]['U']);
            $loyer_periode->setDateDebut($data[$i]['V']);
            $loyer_periode->setDateFin($data[$i]['W']);

            $loyer_periode->setCreatedAt(date("Y-m-d H:i:s"));
            $loyer_periode->setUpdatedAt(date("Y-m-d H:i:s"));

            $em->persist($loyer_periode);
          }
          else
            $erreurs['U'] = "Loyer période incorrecte";
        }


        // Si au moins un des champs n'est pas nul
        if (!is_null($data[$i]['X']) || !is_null($data[$i]['Y'])) {
          // On s'assure que les 3 champs sont renseignés
          if (!is_null($data[$i]['X']) && !is_null($data[$i]['Y'])) {
            $loyer_duree = new Tarif();
            $loyer_duree->setType('duree');
            $loyer_duree->setAppartement($bien);
            $loyer_duree->setLoyer($data[$i]['X']);
            $loyer_duree->setDureeMin($data[$i]['Y']);

            $loyer_duree->setCreatedAt(date("Y-m-d H:i:s"));
            $loyer_duree->setUpdatedAt(date("Y-m-d H:i:s"));

            $em->persist($loyer_duree);
          }
          else
            $erreurs['X'] = "Loyer durée incorrecte";
        }


        $info = $data[$i]['Z'];
        if ($info == null || is_numeric($info))
          $bien->setProvisionSurCharge($info);
        else
          $erreurs['Z'] = "Provisions sur charge incorrecte";


        $info = $data[$i]['AA'];
        if ($info == null || is_numeric($info))
          $bien->setDepot($info);
        else
          $erreurs['AA'] = "Dépôt de garantie incorrecte";



        $info = $data[$i]['AB'];
        if ($info == false || $info == true)
          $bien->setIsEncaisse($info);
        else
          $bien->setIsEncaisse(null);


        $info = $data[$i]['AC'];
        if ($info == null || is_numeric($info))
          $bien->setNbPersonne($info);
        else
          $erreurs['AC'] = "Nombre de personnes incorrecte";


        $info = $data[$i]['AD'];
        if ($info == null || is_numeric($info))
          $bien->setNbPieces($info);
        else
          $erreurs['AD'] = "Nombre de pièces incorrecte";



        $info = $data[$i]['AN'];
        if ($info == false || $info == true)
          $bien->setEscalier($info);
        else
          $bien->setEscalier(null);



        /* $info = $data[$i]['AO'];
          if($info == "gauche" || $info == "droite" || $info == null)
          $bien->setTypeEscalier($info);
          else
          $erreurs['AO'] = "Type d'escalier incorrect"; */



        $info = $data[$i]['AP'];
        if (is_numeric($info) || $info == null)
          $bien->setEtage($info);
        else
          $erreurs['AP'] = "Etage incorrect";



        $info = $data[$i]['AQ'];
        if (is_numeric($info) || $info == null)
          $bien->setNbEtage($info);
        else
          $erreurs['AQ'] = "Nombre etage incorrect";



        $info = $data[$i]['AR'];
        if ($info == false || $info == true)
          $bien->setAscenseur($info);
        else
          $bien->setAscenseur(null);


        /* $info = $data[$i]['AS'];
          if($info == "gauche" || $info == "droite" || $info == null)
          $bien->setTypeAscenseur($info);
          else
          $erreurs['AS'] = "Type d'ascenceur incorrect"; */



        $info = $data[$i]['AT'];
        if (is_numeric($info) || $info == null)
          $bien->setNbBalcon($info);
        else
          $erreurs['AT'] = "Nombre balcons incorrect";



        $info = $data[$i]['AU'];
        if (is_numeric($info) || $info == null)
          $bien->setNbTerrasse($info);
        else
          $erreurs['AU'] = "Nombre terrasses incorrect";



        $info = $data[$i]['AV'];
        if (is_numeric($info) || $info == null)
          $bien->setNbGarage($info);
        else
          $erreurs['AV'] = "Nombre garages incorrect";



        $info = $data[$i]['AW'];
        if (is_numeric($info) || $info == null)
          $bien->setNbParking($info);
        else
          $erreurs['AW'] = "Nombre parkings incorrect";



        $info = $data[$i]['AX'];
        if (is_numeric($info) || $info == null)
          $bien->setNbCave($info);
        else
          $erreurs['AX'] = "Nombre caves incorrect";



        $info = strtoupper(stripAccents(trim($data[$i]['AY'])));
        if (in_array($info, array("COUVERT", "AERIEN", "SOUS-SOL", "GARAGE FERME", "FERME", "NC", null)))
          $bien->setStationnement($info);
        else
          $erreurs['AY'] = "Le champ type de stationement n'est pas une entrée valide";



        $info = strtoupper(stripAccents(trim($data[$i]['AZ'])));
        if (in_array($info, array("ELECTRIQUE",
            "GAZ DE VILLE",
            "GAZ",
            "FUEL",
            "CLIMATISATION REVERSIBLE",
            "GEOTHERMIE",
            "AEROTHERMIE",
            "CHAUFFAGE DE VILLE",
            "POMPE A CHALEUR",
            "REVERSIBLE",
            "BOIS",
            "CHAUDIERE A GRANULE",
            "CHEMINEE",
            "PANNEAU SOLAIRE",
            "POMPE A CHALEUR",
            "AUTRE",
            "AUTRES",
            "NC",
            null
          )))
          $bien->setModeChauffage($info);
        else
          $erreurs['AZ'] = "Le champ mode de chauffage n'est pas une entrée valide";



        $info = strtoupper(stripAccents(trim($data[$i]['BA'])));
        if ($info == "INDIVIDUEL" || $info == "COLLECTIF" || $info == "NC" || $info == null)
          $bien->setTypeChauffage($info);
        else
          $erreurs['BA'] = "Le champ type de chauffage n'est pas une entrée valide";




        $info = $data[$i]['BB'];
        if (is_numeric($info) || $info == "NC" || $info == null)
          $bien->setDistanceAeroport(is_numeric($info) ? $info : null);
        else
          $erreurs['BB'] = "Le champ distance aéroport est incorrect";




        $info = $data[$i]['BC'];
        if (is_numeric($info) || $info == "NC" || $info == null)
          $bien->setDistanceAutoroute(is_numeric($info) ? $info : null);
        else
          $erreurs['BC'] = "Le champ distance autoroute est incorrect";




        $info = $data[$i]['BD'];
        if (is_numeric($info) || $info == "NC" || $info == null)
          $bien->setAccesGare(is_numeric($info) ? $info : null);
        else
          $erreurs['BD'] = "Le champ distance gare est incorrect";




        $info = $data[$i]['BE'];
        if (is_numeric($info) || $info == "NC" || $info == null)
          $bien->setDistanceCommerce(is_numeric($info) ? $info : null);
        else
          $erreurs['BE'] = "Le champ distance commerces est incorrect";




        $info = $data[$i]['BF'];

        if (is_numeric($info) || $info == "NC" || $info == null)
          $bien->setAccesMarche(is_numeric($info) ? $info : null);
        else
          $erreurs['BF'] = "Le champ distance marche est incorrect";




        $info = $data[$i]['BG'];

        if (is_numeric($info) || $info == "NC" || $info == null)
          $bien->setAccesBus(is_numeric($info) ? $info : null);
        else
          $erreurs['BG'] = "Le champ distance bus est incorrect";




        $info = $data[$i]['BH'];

        if (is_numeric($info) || $info == "NC" || $info == null)
          $bien->setAccesMetro(is_numeric($info) ? $info : null);
        else
          $erreurs['BH'] = "Le champ distance métro est incorrect";




        $info = $data[$i]['BI'];
        if (is_numeric($info) || $info == "NC" || $info == null)
          $bien->setAccesTram(is_numeric($info) ? $info : null);
        else
          $erreurs['BI'] = "Le champ distance tram est incorrect";




        $info = $data[$i]['BJ'];
        if ($info == false || $info == true)
          $bien->setClimatisation($info);
        elseif ($info == "NC" || $info == null)
          $bien->setClimatisation(null);
        else
          $erreurs['BT'] = "Le champ climatisation est incorrect";




        $info = $data[$i]['BK'];
        if ($info == false || $info == true)
          $bien->setGrenier($info);
        elseif ($info == "NC" || $info == null)
          $bien->setGrenier(null);
        else
          $erreurs['BK'] = "Le champ grenier est incorrect";




        $info = $data[$i]['BL'];
        if ($info == false || $info == true)
          $bien->setDigicode($info);
        elseif ($info == "NC" || $info == null)
          $bien->setDigicode(null);
        else
          $erreurs['BL'] = "Le champ digicode est incorrect";




        $info = $data[$i]['BM'];
        if ($info == false || $info == true)
          $bien->setGardien($info);
        elseif ($info == "NC" || $info == null)
          $bien->setGardien(null);
        else
          $erreurs['BM'] = "Le champ gardien est incorrect";




        $info = $data[$i]['BN'];
        if ($info == false || $info == true)
          $bien->setInterphone($info);
        elseif ($info == "NC" || $info == null)
          $bien->setInterphone(null);
        else
          $erreurs['BN'] = "Le champ interphone est incorrect";




        $info = $data[$i]['BO'];
        if ($info == false || $info == true)
          $bien->setVideOrdure($info);
        elseif ($info == "NC" || $info == null)
          $bien->setVideOrdure(null);
        else
          $erreurs['BO'] = "Le champ vide-ordure est incorrect";




        $info = $data[$i]['BP'];
        if ($info == false || $info == true)
          $bien->setInternet($info);
        elseif ($info == "NC" || $info == null)
          $bien->setInternet(null);
        else
          $erreurs['BP'] = "Le champ internet est incorrect";




        $info = $data[$i]['BQ'];
        if ($info == false || $info == true)
          $bien->setFumeur($info);
        elseif ($info == "NC" || $info == null)
          $bien->setFumeur(null);
        else
          $erreurs['BQ'] = "Le champ fumeur est incorrect";




        $info = $data[$i]['BR'];
        if ($info == false || $info == true)
          $bien->setAccesHandicapes($info);
        elseif ($info == "NC" || $info == null)
          $bien->setAccesHandicapes(null);
        else
          $erreurs['BR'] = "Le champ accès handicapé est incorrect";




        $info = $data[$i]['BS'];
        if ($info == false || $info == true)
          $bien->setAnimal($info);
        elseif ($info == "NC" || $info == null)
          $bien->setAnimal(null);
        else
          $erreurs['BS'] = "Le champ animaux autorisés est incorrect";





        $nb_chambres = $data[$i]['AE'];
        $salon = $data[$i]['AF'];
        $salle_a_manger = $data[$i]['AG'];
        $bureau = $data[$i]['AH'];
        $nb_salle_bain = $data[$i]['AI'];
        $nb_salle_eau = $data[$i]['AJ'];
        $mezzanine = $data[$i]['AK'];
        $cuisine = $data[$i]['AL'];
        $nb_wc = $data[$i]['AM'];

        $refrigerateur = $data[$i]['BT'];
        $lave_vaisselle = $data[$i]['BU'];
        $congelateur = $data[$i]['BV'];
        $four = $data[$i]['BW'];
        $micro_onde = $data[$i]['BX'];
        $plaques_electriques = $data[$i]['BY'];
        $plaques_induction = $data[$i]['BZ'];
        $plaques_vitroceramiques = $data[$i]['CA'];
        $plaques_gaz = $data[$i]['CB'];
        $lave_linge = $data[$i]['CC'];
        $seche_linge = $data[$i]['CD'];
        $telephone = $data[$i]['CE'];
        $television = $data[$i]['CF'];
        $cheminee = $data[$i]['CG'];
        $climatiseur = $data[$i]['CH'];
        $lavabo_1vasque = $data[$i]['CI'];
        $lavabo_2vasque = $data[$i]['CJ'];
        $baignoire = $data[$i]['CK'];
        $douche = $data[$i]['CL'];

        $a = array(0, 1, "NC", null);

        if (!is_numeric($nb_chambres) && $nb_chambres != "NC" && $nb_chambres != null)
          $erreurs['AE'] = "Le champ nombre de chambres est incorrect";

        if (!in_array($salon, $a))
          $erreurs['AF'] = "Le champ salon est incorrect";

        if (!in_array($salle_a_manger, $a))
          $erreurs['AG'] = "Le champ salle à manger est incorrect";

        if (!in_array($bureau, $a))
          $erreurs['AH'] = "Le champ bureau est incorrect";

        if (!is_numeric($nb_salle_bain) && $nb_salle_bain != "NC" && $nb_salle_bain != null)
          $erreurs['AI'] = "Le champ salle de bain est incorrect";

        if (!is_numeric($nb_salle_eau) && $nb_salle_eau != "NC" && $nb_salle_eau != null)
          $erreurs['AJ'] = "Le champ salle d'eau est incorrect";

        if (!in_array($mezzanine, $a))
          $erreurs['AK'] = "Le champ mezzanine est incorrect";

        if (!is_numeric($nb_wc) && $nb_wc != "NC" && $nb_wc != null)
          $erreurs['AL'] = "Le champ WC est incorrect";


        if (!in_array($refrigerateur, $a))
          $erreurs['BT'] = "Le champ réfrigérateur est incorrect";

        if (!in_array($lave_vaisselle, $a))
          $erreurs['BU'] = "Le champ lave-vaisselle est incorrect";

        if (!in_array($congelateur, $a))
          $erreurs['BV'] = "Le champ congélateur est incorrect";

        if (!in_array($four, $a))
          $erreurs['BW'] = "Le champ four est incorrect";

        if (!in_array($micro_onde, $a))
          $erreurs['BX'] = "Le champ micro-onde est incorrect";

        if (!in_array($plaques_electriques, $a))
          $erreurs['BY'] = "Le champ plaques électriques est incorrect";

        if (!in_array($plaques_induction, $a))
          $erreurs['BZ'] = "Le champ plaques à induction est incorrect";

        if (!in_array($plaques_vitroceramiques, $a))
          $erreurs['CA'] = "Le champ plaques vitrocéramiques est incorrect";

        if (!in_array($plaques_gaz, $a))
          $erreurs['CB'] = "Le champ plaques gaz est incorrect";

        if (!in_array($lave_linge, $a))
          $erreurs['CC'] = "Le champ lave-linge est incorrect";

        if (!in_array($seche_linge, $a))
          $erreurs['CD'] = "Le champ sèche-linge est incorrect";

        if (!in_array($telephone, $a))
          $erreurs['CE'] = "Le champ téléphone( est incorrect";

        if (!in_array($television, $a))
          $erreurs['CF'] = "Le champ télévision est incorrect";

        if (!in_array($cheminee, $a))
          $erreurs['CG'] = "Le champ cheminée est incorrect";

        if (!in_array($climatiseur, $a))
          $erreurs['CH'] = "Le champ climatiseur est incorrect";

        if (!in_array($lavabo_1vasque, $a))
          $erreurs['CI'] = "Le champ lavabo 1 vasque est incorrect";

        if (!in_array($lavabo_2vasque, $a))
          $erreurs['CJ'] = "Le champ lavabo 2 vasques est incorrect";

        if (!in_array($baignoire, $a))
          $erreurs['CK'] = "Le champ baignoir est incorrect";

        if (!in_array($douche, $a))
          $erreurs['CL'] = "Le champ douche est incorrect";


        $pieces = array();
        $typepiece = array();
        $k = 0;

        if (is_numeric($nb_chambres) && $nb_chambres > 0) {
          for ($j = 0; $j < $nb_chambres; $j++) {
            $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Chambre"));

            $pieces[$k] = new Piece();
            $pieces[$k]->setTypepiece($typepiece[$k]);
            $pieces[$k]->setAppartement($bien);

            $em->persist($pieces[$k]);
            $em->persist($typepiece[$k]);
            $k++;
          }
        }

        if ($salon == 1) {
          $pieces[$k] = new Piece();
          $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Salon"));
          $pieces[$k]->setTypepiece($typepiece[$k]);
          $pieces[$k]->setAppartement($bien);

          if ($cheminee == 1) {
            $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Cheminée"));
            $pieces[$k]->addEquipement($equipement);
          }
          if ($television == 1) {
            $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Télévision"));
            $pieces[$k]->addEquipement($equipement);
          }
          if ($telephone == 1) {
            $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Téléphone"));
            $pieces[$k]->addEquipement($equipement);
          }
          if ($climatiseur == 1) {
            $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Climatiseur"));
            $pieces[$k]->addEquipement($equipement);
          }

          $em->persist($pieces[$k]);
          $em->persist($typepiece[$k]);
          $k++;
        }

        if ($salle_a_manger == 1) {
          $pieces[$k] = new Piece();
          $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Salle à manger"));
          $pieces[$k]->setTypepiece($typepiece[$k]);
          $pieces[$k]->setAppartement($bien);

          $em->persist($pieces[$k]);
          $em->persist($typepiece[$k]);
          $k++;
        }

        if ($bureau == 1) {
          $pieces[$k] = new Piece();
          $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Bureau"));
          $pieces[$k]->setTypepiece($typepiece[$k]);
          $pieces[$k]->setAppartement($bien);

          $em->persist($pieces[$k]);
          $em->persist($typepiece[$k]);
          $k++;
        }

        if (is_numeric($nb_salle_bain) && $nb_salle_bain > 0) {
          for ($j = 0; $j < $nb_salle_bain; $j++) {
            $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Salle de bain"));

            $pieces[$k] = new Piece();
            $pieces[$k]->setTypepiece($typepiece[$k]);
            $pieces[$k]->setAppartement($bien);

            // On n'ajoute les équipements uniquement s'il n'y a qu'une salle de bain
            if ($nb_salle_bain == 1) {
              if ($lavabo_1vasque == 1) {
                $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Lavabo 1 Vasque"));
                $pieces[$k]->addEquipement($equipement);
              }
              if ($lavabo_2vasque == 1) {
                $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Lavabo 2 Vasques"));
                $pieces[$k]->addEquipement($equipement);
              }
              if ($baignoire == 1) {
                $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Baignoire"));
                $pieces[$k]->addEquipement($equipement);
              }
              if ($douche == 1) {
                $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Douche"));
                $pieces[$k]->addEquipement($equipement);
              }
            }

            $em->persist($pieces[$k]);
            $em->persist($typepiece[$k]);
            $k++;
          }
        }
        /* if($salle_bain == 1) {
          $pieces[$k] = new Piece();
          $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Salle de bain"));
          $pieces[$k]->setTypepiece($typepiece[$k]);
          $pieces[$k]->setAppartement($bien); */

        /* if($lavabo_1vasque == 1) {
          $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Lavabo 1 Vasque"));
          $pieces[$k]->addEquipement($equipement);
          }
          if($lavabo_2vasque == 1) {
          $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Lavabo 2 Vasques"));
          $pieces[$k]->addEquipement($equipement);
          }
          if($baignoire == 1) {
          $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Baignoire"));
          $pieces[$k]->addEquipement($equipement);
          }
          if($douche == 1) {
          $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Douche"));
          $pieces[$k]->addEquipement($equipement);
          } */

        /* $em->persist($pieces[$k]);
          $em->persist($typepiece[$k]);
          $k++;
          } */

        if (is_numeric($nb_salle_eau) && $nb_salle_eau > 0) {
          for ($j = 0; $j < $nb_salle_eau; $j++) {
            $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Salle d'eau"));

            $pieces[$k] = new Piece();
            $pieces[$k]->setTypepiece($typepiece[$k]);
            $pieces[$k]->setAppartement($bien);

            // On n'ajoute les équipements uniquement s'il n'y a qu'une salle d'eau
            if ($nb_salle_eau == 1) {
              if ($lave_linge == 1) {
                $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Lave linge"));
                $pieces[$k]->addEquipement($equipement);
              }
              if ($seche_linge == 1) {
                $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Sèche Linge"));
                $pieces[$k]->addEquipement($equipement);
              }
            }

            $em->persist($pieces[$k]);
            $em->persist($typepiece[$k]);
            $k++;
          }
        }
        /* if($salle_eau == 1) {
          $pieces[$k] = new Piece();
          $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Salle d'eau"));
          $pieces[$k]->setTypepiece($typepiece[$k]);
          $pieces[$k]->setAppartement($bien); */

        /* if($lave_linge == 1) {
          $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Lave linge"));
          $pieces[$k]->addEquipement($equipement);
          }
          if($seche_linge == 1) {
          $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Sèche Linge"));
          $pieces[$k]->addEquipement($equipement);
          } */

        /* $em->persist($pieces[$k]);
          $em->persist($typepiece[$k]);
          $k++;
          } */

        if ($mezzanine == 1) {
          $pieces[$k] = new Piece();
          $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Mezzanine"));
          $pieces[$k]->setTypepiece($typepiece[$k]);
          $pieces[$k]->setAppartement($bien);

          $em->persist($pieces[$k]);
          $em->persist($typepiece[$k]);
          $k++;
        }


        if (is_numeric($nb_wc) && $nb_wc > 0) {
          for ($j = 0; $j < $nb_wc; $j++) {
            $pieces[$k] = new Piece();
            $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "WC"));
            $pieces[$k]->setTypepiece($typepiece[$k]);
            $pieces[$k]->setAppartement($bien);

            $em->persist($pieces[$k]);
            $em->persist($typepiece[$k]);
            $k++;
          }
        }

        if ($cuisine != null) {
          $pieces[$k] = new Piece();
          $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Cuisine"));
          $pieces[$k]->setTypepiece($typepiece[$k]);
          $pieces[$k]->setCommentaire($cuisine);
          $pieces[$k]->setAppartement($bien);

          if ($refrigerateur == 1) {
            $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Réfrigerateur"));
            $pieces[$k]->addEquipement($equipement);
          }
          if ($lave_vaisselle == 1) {
            $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Lave Vaisselle"));
            $pieces[$k]->addEquipement($equipement);
          }
          if ($congelateur == 1) {
            $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Congélateur"));
            $pieces[$k]->addEquipement($equipement);
          }
          if ($four == 1) {
            $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Four"));
            $pieces[$k]->addEquipement($equipement);
          }
          if ($micro_onde == 1) {
            $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Micro-onde"));
            $pieces[$k]->addEquipement($equipement);
          }
          if ($plaques_electriques == 1) {
            $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Plaques Electriques"));
            $pieces[$k]->addEquipement($equipement);
          }
          if ($plaques_induction == 1) {
            $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Plaques à induction"));
            $pieces[$k]->addEquipement($equipement);
          }
          if ($plaques_vitroceramiques == 1) {
            $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Plaques Vitrocéramiques"));
            $pieces[$k]->addEquipement($equipement);
          }
          if ($plaques_gaz == 1) {
            $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Plaques Gaz"));
            $pieces[$k]->addEquipement($equipement);
          }

          $em->persist($pieces[$k]);
          $em->persist($typepiece[$k]);
          $k++;
        }




        $info = strtoupper(stripAccents($data[$i]['CM']));
        if (in_array($info, array('MOYEN', 'NORMAL', 'BON', 'TRES BON', 'STANDING', 'GRAND STANDING', 'TRES GRAND', 'EXCELLENT', null)))
          $bien->setStandingImmeuble($info);
        else
          $erreurs['CM'] = "Le champ standing est incorrect";


        $info = strtoupper(stripAccents($data[$i]['CN']));
        if (in_array($info, array("TRAVAUX A PREVOIR", "A RAFRAICHIR", "MAUVAIS", "HABITABLE EN L'ETAT", "ETAT MOYEN", "BON", "TRES BON", "EXCELLENT", "SOMPTUEUX", null)))
          $bien->setEtatInterieur($info);
        else
          $erreurs['CN'] = "Le champ état intérieur est incorrect";


        $info = $data[$i]['CO'];
        if (in_array($info, array(1, 2, 3, 4, 5, null)))
          $bien->setCalme($info);
        else
          $erreurs['CO'] = "Le champ calme est incorrect";


        $info = $data[$i]['CP'];
        if (in_array($info, array(1, 2, 3, 4, 5, null)))
          $bien->setClair($info);
        else
          $erreurs['CP'] = "Le champ clair est incorrect";


        $info = strtoupper(stripAccents($data[$i]['CQ']));
        if (in_array($info, array('NEUF', 'ANCIEN', 'RECENT', null)))
          $bien->setNeufAncien($info);
        else
          $erreurs['CQ'] = "Le champ immeuble neuf/ancien est incorrect";


        $info = $data[$i]['CR'];
        if (in_array($info, array(0, 1, null)))
          $bien->setIsLabeliser($info);
        else
          $erreurs['CR'] = "Le champ bien labellisé est incorrect";


        $bien->setCommentaire($data[$i]['CS']);
        $bien->setConfidentiel($data[$i]['CT']);
      }


      // Au moins une erreur est survenue
      if ($erreurs) {
        // On affiche les erreurs sur la sortie
        $output->writeln("Ligne ".$i." :");
        foreach ($erreurs as $erreur)
          $output->writeln($erreur);
        $output->writeln("");

        //continue;
      }
      else {
        //Génération des urls
        if ($bien->getIsMeuble() == true) {
          $meublefr = 'meuble';
          $meubleen = 'furnished';
          $meublede = 'mobliert';
        }
        else {
          $meublefr = 'non-meuble';
          $meubleen = 'unfurnished';
          $meublede = 'unmobliert';
        }

        switch ($bien->getType()) {
          case 'APPARTEMENT':
            $typefr = 'appartement';
            $typeen = 'apartment';
            $typede = 'wohnung';
            break;
          case 'MAISON':
            $typefr = 'maison';
            $typeen = 'house';
            $typede = 'haus';
            break;
          case 'STUDIO':
            $typefr = 'studio';
            $typeen = 'studio';
            $typede = 'studio';
            break;
          case 'LOFT':
            $typefr = 'loft';
            $typeen = 'loft';
            $typede = 'loft';
            break;
          case 'GARAGE':
            $typefr = 'garage';
            $typeen = 'garage';
            $typede = 'garage';
            break;
          case 'CHAMBRE':
            $typefr = 'chambre';
            $typeen = 'room';
            $typede = 'zimmer';
            break;

          default:
            break;
        }
        $url_fr = "location-".$typefr."-".$meublefr."-".strtolower($bien->getVille())."-".$bien->getReference();
        $url_fr = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_fr);
        $bien->setUrlFr($url_fr);

        $url_en = "renting-".$typeen."-".$meubleen."-".strtolower($bien->getVille())."-".$bien->getReference();
        $url_en = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_en);
        $bien->setUrlEn($url_en);

        $url_de = "mieten-".$typede."-".$meublede."-".strtolower($bien->getVille())."-".$bien->getReference();
        $url_de = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_de);
        $bien->setUrlDe($url_de);

        $bien->setCreatedBy($proprietaire);
        $bien->setUpdatedBy($proprietaire);

        $em->persist($bien);

        $em->flush();


        $nb_ajouts++;
      }



      if (isset($proprietaire)) {
        $em->detach($proprietaire);
        unset($proprietaire);
      }
      if (isset($bien)) {
        $em->detach($bien);
        unset($bien);
      }
      if (isset($ville)) {
        //$em->detach($ville);
        unset($ville);
      }
      if (isset($villes)) {
        //$em->detach($villes);
        unset($villes);
      }
      if (isset($loyer_base)) {
        $em->detach($loyer_base);
        unset($loyer_base);
      }
      if (isset($loyer_periode)) {
        $em->detach($loyer_periode);
        unset($loyer_periode);
      }
      if (isset($loyer_duree)) {
        $em->detach($loyer_duree);
        unset($loyer_duree);
      }
      if (isset($pieces)) {
        foreach ($pieces as $piece)
          $em->detach($piece);
        unset($pieces);
      }
      if (isset($typepiece)) {
        foreach ($typepiece as $type)
          $em->detach($type);
        unset($typepiece);
      }



      $em->clear();
      //$output->writeln(memory_get_usage());
    }

    $output->writeln("");
    $output->writeln(($i - 3)." lignes traitées");
    $output->writeln($nb_ajouts." biens ajoutés");

    //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $inputFileType);
    //$objWriter->save($dir."pat-bien-result.xls");
  }

}
