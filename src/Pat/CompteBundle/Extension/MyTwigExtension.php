<?php

namespace Pat\CompteBundle\Extension;

use Pat\CompteBundle\Tools\PaymentTools;

class MyTwigExtension extends \Twig_Extension
{

  protected $em;
  protected $tarif_manager;
  protected $date_functions;

  function __construct($em, $tarif_manager, $date_functions)
  {
    $this->em = $em;
    $this->tarif_manager = $tarif_manager;
    $this->date_functions = $date_functions;
  }

  public function getName()
  {
    return 'my_twig_extension';
  }

  public function getFilters()
  {
    return array(
      'price_format' => new \Twig_Filter_Method($this, 'price_format'),
      'price_format_en' => new \Twig_Filter_Method($this, 'price_format_en'),
      'convert_paiement' => new \Twig_Filter_Method($this, 'convert_paiement'),
    );
  }

  public function getFunctions()
  {
    return array(
      'get_img' => new \Twig_Function_Method($this, 'get_img'),
      'get_lower_loyer' => new \Twig_Function_Method($this, 'getLowerLoyer'),
      'get_loyer_by_date' => new \Twig_Function_Method($this, 'getLoyerByDate'),
      'get_prix_roses' => new \Twig_Function_Method($this, 'getPrixRoses'),
      'get_prix_champagne' => new \Twig_Function_Method($this, 'getPrixChampagne'),
      'get_prix_lit_bebe' => new \Twig_Function_Method($this, 'getPrixLitBebe'),
      'get_prix_menage_studio' => new \Twig_Function_Method($this, 'getPrixMenageStudio'),
      'get_prix_menageT2' => new \Twig_Function_Method($this, 'getPrixMenageT2'),
      'get_prix_menageT3' => new \Twig_Function_Method($this, 'getPrixMenageT3'),
      'get_prix_menageT4' => new \Twig_Function_Method($this, 'getPrixMenageT4'),
      'moins48h' => new \Twig_Function_Method($this, 'moins48h'),
      'get_tarif' => new \Twig_Function_Method($this, 'getTarif'),
      'get_payments' => new \Twig_Function_Method($this, 'getPayments'),
      'get_from_tarif' => new \Twig_Function_Method($this, 'getFromTarif'),
      'get_nb_nuits' => new \Twig_Function_Method($this, 'getNbNuits'),
    );
  }

  public function price_format($value)
  {
//    $str = round($value, 2);
//    if (preg_match("/^[0-9]*\.[0-9]*/", $str)) {
//      $price = explode('.', $str);
//      if ($price[1] == '00')
//        return $price[0];
//      else
//        return $price[0].','.$price[1];
//    }
//    else
//      return $str;
    return number_format(round((float) $value, 2), 2, ',', '.');
  }

  public function price_format_en($value)
  {
    return number_format(round((float) $value, 2), 2);
  }

  // format a price
  public function convert_paiement($str)
  {
    $str = round($str, 2);
    $str *= 100;

    return $str;
  }

  public function get_img($id_bien)
  {
    $media = $this->em->getRepository('PatCompteBundle:Media')->findOneBy(array('appartement' => $id_bien), array('tri' => 'ASC'));

    return is_null($media) ? null : $media->getFichier();
  }

  public function getLowerLoyer($tarif)
  {
    return $this->tarif_manager->getLowerLoyer($tarif);
  }

  public function getLoyerByDate($tarif, $nb_jours)
  {
    return $this->tarif_manager->getLoyerByDate($tarif, $nb_jours);
  }

  public function getPrixRoses()
  {
    return $this->tarif_manager->getPrixRoses();
  }

  public function getPrixChampagne()
  {
    return $this->tarif_manager->getPrixChampagne();
  }

  public function getPrixLitBebe()
  {
    return $this->tarif_manager->getPrixLitBebe();
  }

  public function getPrixMenageStudio()
  {
    return $this->tarif_manager->getPrixMenageStudio();
  }

  public function getPrixMenageT2()
  {
    return $this->tarif_manager->getPrixMenageT2();
  }

  public function getPrixMenageT3()
  {
    return $this->tarif_manager->getPrixMenageT3();
  }

  public function getPrixMenageT4()
  {
    return $this->tarif_manager->getPrixMenageT4();
  }

  public function moins48h($date)
  {
    $date = $this->date_functions->toDateEn($date);
    return $date <= date("Y-m-d", strtotime('+2 days'));
  }

  public function getNbNuits($debut, $fin)
  {
    return $this->date_functions->nbJours($debut, $fin);
  }

  /**
   *
   * @param \Pat\CompteBundle\Entity\Reservation $resa
   * @return type
   */
  public function getTarif($resa)
  {
//    $total = $resa->getTarif() + $resa->getPromotion();
//
//    foreach ($resa->getOptions() as $option)
//      $total += $option->getQuantity() * $option->getAmount();
//
//    return $total;
    return $resa->getTotalAmount();
  }

  public function getPayments($resa)
  {
    $payments = $this->em->getRepository('PatCompteBundle:Payment')->findBy(array('reservation' => $resa, 'status' => PaymentTools::CONSTANT_STATUS_PAYED));
    $total = 0;

    foreach ($payments as $payment)
      $total += $payment->getAmount();

    return $total;
  }

  public function getFromTarif($appartement)
  {
    $param_taxes = $this->em->getRepository('PatCompteBundle:Parametre')->findOneByName('taxes');
    $tarif = $this->tarif_manager->getFromTarif($appartement->getTarif(), $param_taxes->getValue(), $appartement->getNbPieces());

    return $tarif;
  }

}
