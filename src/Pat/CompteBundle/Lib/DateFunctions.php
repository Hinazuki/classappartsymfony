<?php

namespace Pat\CompteBundle\Lib;

class DateFunctions
{

  // Calcul le nombre de jours entre deux dates
  public function nbJours($debut, $fin)
  {
    $debut = $this->toDateEn($debut);
    $fin = $this->toDateEn($fin);

    //60 secondes X 60 minutes X 24 heures dans une journée
    $nbSecondes = 60 * 60 * 24;

    $debut_ts = strtotime($debut);
    $fin_ts = strtotime($fin);
    $diff = $fin_ts - $debut_ts;

    return round($diff / $nbSecondes);
  }

  // Transforme une date au format jj/mm/aaaa au format aaa-mm-jj
  public function toDateEn($date)
  {
    if (preg_match("#^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$#", $date))
      return substr($date, 6, 4)."-".substr($date, 3, 2)."-".substr($date, 0, 2);
    else
      return $date;
  }

}
