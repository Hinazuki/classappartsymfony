<?php

namespace Pat\CompteBundle\Lib;

use Pat\CompteBundle\Tools\CalendrierTools;
use Pat\CompteBundle\Tools\ReservationTools;

class CalendrierAppartement
{

  protected $mois;
  protected $annee;
  protected $appartement;
  protected $tabresa; // double tableau [date y-m-d][ [statut] [statut] ... ]

  function __construct($mois, $annee, $appartement, $tabresa)
  {
    $this->mois = $mois;
    $this->annee = $annee;
    $this->appartement = $appartement;
    $this->tabresa = $tabresa;
  }

  function modification($mois, $annee, $appartement)
  {
    $this->mois = $mois;
    $this->annee = $annee;
    $this->appartement = $appartement;
  }

  // Affiche le calendrier du coté back-office
  // Bundle = "User" => User
  // Bundle = "Admin" => Admin
  function affichage($bundle)
  {

    //Creating general vars
    $year = date("Y");
    if (empty($this->mois))
      $monthnb = date("n");
    else {
      $monthnb = $this->mois;
      $year = $this->annee;
      if ($monthnb <= 0) {
        $monthnb = 12;
        $year = $year - 1;
      }
      elseif ($monthnb > 12) {
        $monthnb = 1;
        $year = $year + 1;
      }
    }
    $day = date("w");
    $nbdays = date("t", mktime(0, 0, 0, $monthnb, 1, $year));
    $firstday = date("w", mktime(0, 0, 0, $monthnb, 1, $year));

    //Replace the number of the day by its french name
    $daytab[1] = 'Lu';
    $daytab[2] = 'Ma';
    $daytab[3] = 'Me';
    $daytab[4] = 'Je';
    $daytab[5] = 'Ve';
    $daytab[6] = 'Sa';
    $daytab[7] = 'Di';

    //Build the calendar table
    $calendar = array();
    $z = (int) $firstday;
    if ($z == 0)
      $z = 7;
    for ($i = 1; $i <= ($nbdays / 5); $i++) {
      for ($j = 1; $j <= 7 && $j - $z + 1 + (($i * 7) - 7) <= $nbdays; $j++) {
        if ($j < $z && ($j - $z + 1 + (($i * 7) - 7)) <= 0)
          $calendar[$i][$j] = null;
        else
          $calendar[$i][$j] = $j - $z + 1 + (($i * 7) - 7);
      }
    }

    //Replace the number of the month by its french name
    switch ($monthnb) {
      case 1: $month = 'Janvier';
        break;
      case 2: $month = 'F&eacute;vrier';
        break;
      case 3: $month = 'Mars';
        break;
      case 4: $month = 'Avril';
        break;
      case 5: $month = 'Mai';
        break;
      case 6: $month = 'Juin';
        break;
      case 7: $month = 'Juillet';
        break;
      case 8: $month = 'Ao&ucirc;t';
        break;
      case 9: $month = 'Septembre';
        break;
      case 10: $month = 'Octobre';
        break;
      case 11:$month = 'Novembre';
        break;
      case 12:$month = 'D&eacute;cembre';
        break;
    }

    $calendarDispo = "";
    $calendarDispo .= '
            <div class="calendrierDispo">
                <table border="0" cellpadding="6" cellspacing="2" bgcolor="#E1E0E0">
                    <tr bgcolor="#ffffff">
                        <th colspan="7" align="center" class="headcal">'.($month.' '.$year).'</th>
                    </tr>
            ';
    $calendarDispo .= '<tr bgcolor="#ffffff">';

    for ($i = 1; $i <= 7; $i++)
      $calendarDispo .= '<th class="jourCal" align="center">'.$daytab[$i].'</th>';

    $calendarDispo .= '</tr>';

    for ($i = 1; $i <= count($calendar); $i++) {
      $calendarDispo .= '<tr bgcolor="#ffffff">';
      for ($j = 1; $j <= 7 && $j - $z + 1 + (($i * 7) - 7) <= $nbdays; $j++) {

        if (strlen($monthnb) == 1)
          $monthnb = "0".$monthnb;

        if (strlen($calendar[$i][$j]) == 1)
          $calendar[$i][$j] = "0".$calendar[$i][$j];

        $dateResa = $year."-".$monthnb."-".$calendar[$i][$j];

        if (array_key_exists($dateResa, $this->tabresa)) { //on teste et affiche les dates de la BDD
          if ($bundle == "User") { // Utilisateur ne voit que des cases en rouge
            if (isset($this->tabresa[$dateResa])) {
              // Si l'entrée ne contient qu'une seule date et que c'est un ménage, on n'affiche rien car celà n'est pas bloquant
              if (in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE, $this->tabresa[$dateResa]) && sizeof($this->tabresa[$dateResa]) == 1)
                $calendarDispo .= '<th>'.$calendar[$i][$j].'</th>';
              else
                $calendarDispo .= '<th style="background-color:#D22C2C; color:#FFFFFF" bgcolor="#D22C2C">'.$calendar[$i][$j].'</th>';
            }
            else
              $calendarDispo .= '<th>'.$calendar[$i][$j].'</th>';
          }
          elseif ($bundle == "Admin") { // Admin voit les différents statuts des réservations
            if (in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE, $this->tabresa[$dateResa]))
              $class = "bloc-menage";
            else
              $class = '';



            // Date bloquée par une réservation
//                        if(in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_RESERVATION, $this->tabresa[$dateResa]))
//                            $calendarDispo .= '<th class="'.$class.'" style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_RESERVATION.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';


            if (in_array("res_".ReservationTools::CONSTANT_STATUS_WAIT_VALIDATION, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:DarkGray; color:#FFFFFF">'.$calendar[$i][$j].'</th>';

            elseif (in_array("res_".ReservationTools::CONSTANT_STATUS_WAIT, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:LightGray; color:#FFFFFF">'.$calendar[$i][$j].'</th>';

            elseif (in_array("res_".ReservationTools::CONSTANT_STATUS_TO_PAYED, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:DarkOrange; color:#FFFFFF">'.$calendar[$i][$j].'</th>';

            elseif (in_array("res_".ReservationTools::CONSTANT_STATUS_PAYED, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:Green; color:#FFFFFF">'.$calendar[$i][$j].'</th>';

            elseif (in_array("res_".ReservationTools::CONSTANT_STATUS_EXCEEDING, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:Purple; color:#FFFFFF">'.$calendar[$i][$j].'</th>';

            elseif (in_array("res_".ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED, $this->tabresa[$dateResa])) {
              if (in_array("res_vp_".ReservationTools::CONSTANT_VALID_RESA_PROP, $this->tabresa[$dateResa]) || in_array("res_vp_".ReservationTools::CONSTANT_VALID_RESA_48, $this->tabresa[$dateResa]))
                $calendarDispo .= '<th class="'.$class.'" style="background-color:Yellow; color:#FFFFFF">'.$calendar[$i][$j].'</th>';
              else
                $calendarDispo .= '<th class="'.$class.'" style="background-color:Gray; color:#FFFFFF">'.$calendar[$i][$j].'</th>';
            }


            // Date bloquée par Propriétaire
            elseif (in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_PROPRIETAIRE, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_PROPRIETAIRE.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';

            // Date bloquée par Admin
            elseif (in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_ADMIN.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';

            // Date bloquée par Admin pour ménage
//                        elseif($this->tabresa[$dateResa] == CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE)
//                            $calendarDispo .= '<th style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_ADMIN_MENAGE.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';
            // Date bloquée par Admin pour travaux
            elseif (in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_TRAVAUX, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_ADMIN_TRAVAUX.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';
            else
              $calendarDispo .= '<th class="'.$class.'">'.$calendar[$i][$j].'</th>';
          }
          else { // Proprio voient la distinction Reservation/Proprietaire/Administrateur
            if (in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE, $this->tabresa[$dateResa]))
              $class = "bloc-menage";
            else
              $class = '';


            if (in_array("res_".ReservationTools::CONSTANT_STATUS_WAIT_VALIDATION, $this->tabresa[$dateResa]) || in_array("res_".ReservationTools::CONSTANT_STATUS_WAIT, $this->tabresa[$dateResa]) || in_array("res_".ReservationTools::CONSTANT_STATUS_TO_PAYED, $this->tabresa[$dateResa]) || in_array("res_".ReservationTools::CONSTANT_STATUS_PAYED, $this->tabresa[$dateResa]) || in_array("res_".ReservationTools::CONSTANT_STATUS_EXCEEDING, $this->tabresa[$dateResa]) || in_array("res_".ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED, $this->tabresa[$dateResa])
            )
              $calendarDispo .= '<th class="'.$class.'" style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_RESERVATION.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';


            // Date bloquée par Propriétaire
            elseif (in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_PROPRIETAIRE, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_PROPRIETAIRE.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';

            // Date bloquée par Admin
            elseif (in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_ADMIN.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';

            // Date bloquée par Admin pour ménage
//                        elseif($this->tabresa[$dateResa] == CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE)
//                            $calendarDispo .= '<th style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_ADMIN_MENAGE.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';
            // Date bloquée par Admin pour travaux
            elseif (in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_TRAVAUX, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_ADMIN_TRAVAUX.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';
            else
              $calendarDispo .= '<th class="'.$class.'">'.$calendar[$i][$j].'</th>';
          }
        }
        else
          $calendarDispo .= '<th>'.$calendar[$i][$j].'</th>';
      }
      $calendarDispo .= '</tr>';
    }
    $calendarDispo .= '
                </table>
            </div>
            ';
    return $calendarDispo;
  }

  // Affiche le calendrier du coté front office
  // Bundle = "User" => User
  // Bundle = "Admin" => Admin
  function affichageFront($bundle)
  {
    //Creating general vars
    $year = date("Y");

    if (empty($this->mois))
      $monthnb = date("n");
    else {
      $monthnb = $this->mois;
      $year = $this->annee;
      if ($monthnb <= 0) {
        $monthnb = 12;
        $year = $year - 1;
      }
      elseif ($monthnb > 12) {
        $monthnb = 1;
        $year = $year + 1;
      }
    }

    $day = date("w");
    $nbdays = date("t", mktime(0, 0, 0, $monthnb, 1, $year));
    $firstday = date("w", mktime(0, 0, 0, $monthnb, 1, $year));

    //Replace the number of the day by its french name
    $daytab[1] = 'Lu';
    $daytab[2] = 'Ma';
    $daytab[3] = 'Me';
    $daytab[4] = 'Je';
    $daytab[5] = 'Ve';
    $daytab[6] = 'Sa';
    $daytab[7] = 'Di';

    //Build the calendar table
    $calendar = array();
    $z = (int) $firstday;

    if ($z == 0)
      $z = 7;

    for ($i = 1; $i <= ($nbdays / 5); $i++) {
      for ($j = 1; $j <= 7 && $j - $z + 1 + (($i * 7) - 7) <= $nbdays; $j++) {
        if ($j < $z && ($j - $z + 1 + (($i * 7) - 7)) <= 0)
          $calendar[$i][$j] = null;
        else
          $calendar[$i][$j] = $j - $z + 1 + (($i * 7) - 7);
      }
    }

    //Replace the number of the month by its french name
    switch ($monthnb) {
      case 1: $month = 'Janvier';
        break;
      case 2: $month = 'F&eacute;vrier';
        break;
      case 3: $month = 'Mars';
        break;
      case 4: $month = 'Avril';
        break;
      case 5: $month = 'Mai';
        break;
      case 6: $month = 'Juin';
        break;
      case 7: $month = 'Juillet';
        break;
      case 8: $month = 'Ao&ucirc;t';
        break;
      case 9: $month = 'Septembre';
        break;
      case 10: $month = 'Octobre';
        break;
      case 11:$month = 'Novembre';
        break;
      case 12:$month = 'D&eacute;cembre';
        break;
    }

    $calendarDispo = "";
    $calendarDispo .= '
            <div class="calendrierDispo">
                <table border="0" cellpadding="10" cellspacing="1" bgcolor="#E1E0E0">
                    <tr bgcolor="#ffffff">
                        <th colspan="7" align="center" class="headcal">'.($month.' '.$year).'</th>
                    </tr>
            ';

    $calendarDispo .= '<tr bgcolor="#ffffff">';

    for ($i = 1; $i <= 7; $i++)
      $calendarDispo .= '<th class="jourCal" align="center">'.$daytab[$i].'</th>';

    $calendarDispo .= '</tr>';

    for ($i = 1; $i <= count($calendar); $i++) {
      $calendarDispo .= '<tr bgcolor="#ffffff">';
      for ($j = 1; $j <= 7 && $j - $z + 1 + (($i * 7) - 7) <= $nbdays; $j++) {

        if (strlen($monthnb) == 1)
          $monthnb = "0".$monthnb;

        if (strlen($calendar[$i][$j]) == 1)
          $calendar[$i][$j] = "0".$calendar[$i][$j];

        $dateResa = $year."-".$monthnb."-".$calendar[$i][$j];

        if (array_key_exists($dateResa, $this->tabresa)) { //on teste et affiche les dates de la BDD
          if ($bundle == "User") { // Utilisateur ne voit que des cases en rouge
            // Bloqué.
            if (isset($this->tabresa[$dateResa])) {
              $blocked = in_array(
                CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN, $this->tabresa[$dateResa]
              );
              $cleaning = in_array(
                CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE, $this->tabresa[$dateResa]
              );
              $repairs = in_array(
                CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_TRAVAUX, $this->tabresa[$dateResa]
              );

              $class_tab_front = '';
              if (in_array('resa_start', $this->tabresa[$dateResa])) {
                $class_tab_front = 'resa-start';
              }
              elseif (in_array('resa_end', $this->tabresa[$dateResa])) {
                $class_tab_front = 'resa-end';
              }

              if ($blocked || $repairs) {
                $class_tab_front = '';
              }

              // Si l'entrée ne contient qu'une seule date et que c'est un ménage, on n'affiche rien car celà n'est pas bloquant
              if ($cleaning && count($this->tabresa[$dateResa]) == 1) {
                // Vert.
                $calendarDispo .= '<th class="'.$class_tab_front.'" style="background-color:#5BD835;color:#FFFFFF">'.$calendar[$i][$j].'</th>';
              }
              else {
                // Rouge.
                $calendarDispo .= '<th class="'.$class_tab_front.'" style="background-color:#DD2525; color:#FFFFFF">'.$calendar[$i][$j].'</th>';
              }
            }
            // Dispo.
            else {
              // Vert.
              $calendarDispo .= '<th style="background-color:#5BD835;color:#FFFFFF">'.$calendar[$i][$j].'</th>';
            }
          }
          else { // Admin et Proprio voient la distinction Reservation/Proprietaire/Administrateur
            //
                        // Date bloquée par une réservation
            if (in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE, $this->tabresa[$dateResa]))
              $class = "bloc-menage";
            else
              $class = '';

            // Date bloquée par une réservation
            if (in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_RESERVATION, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_RESERVATION.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';

            // Date bloquée par Propriétaire
            elseif (in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_PROPRIETAIRE, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_PROPRIETAIRE.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';

            // Date bloquée par Admin
            elseif (in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_ADMIN.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';

            // Date bloquée par Admin pour ménage
//                        elseif($this->tabresa[$dateResa] == CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE)
//                            $calendarDispo .= '<th style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_ADMIN_MENAGE.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';
            // Date bloquée par Admin pour travaux
            elseif (in_array(CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_TRAVAUX, $this->tabresa[$dateResa]))
              $calendarDispo .= '<th class="'.$class.'" style="background-color:'.CalendrierTools::CONSTANT_COLOR_BLOQUE_ADMIN_TRAVAUX.'; color:#FFFFFF">'.$calendar[$i][$j].'</th>';
            else
              $calendarDispo .= '<th class="'.$class.'">'.$calendar[$i][$j].'</th>';
          }
        }
        else {
          //on colore les cases qui contiennent des chiffres
          if (!empty($calendar[$i][$j]))
            $calendarDispo .= '<th style="background-color:#5BD835;color:#FFFFFF" bgcolor="#5BD835">'.$calendar[$i][$j].'</th>';
          else
            $calendarDispo .= '<th>'.$calendar[$i][$j].'</th>';
        }
      }
      $calendarDispo .= '</tr>';
    }
    $calendarDispo .= '
                </table>
            </div>
        ';

    return $calendarDispo;
  }

}
