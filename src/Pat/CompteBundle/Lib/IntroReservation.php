<?php

namespace Pat\CompteBundle\Lib;

class IntroReservation
{

  protected $datedebut;
  protected $datefin;
  protected $ref;

  function __construct($datedebut, $datefin, $ref)
  {
    $this->datedebut = $datedebut;
    $this->datefin = $datefin;
    $this->ref = $ref;
  }

  public function getRef()
  {
    return $this->ref;
  }

  public function setRef($ref)
  {
    $this->ref = $ref;
  }

  public function getDatedebut()
  {
    return $this->datedebut;
  }

  public function setDatedebut($datedebut)
  {
    $this->datedebut = $datedebut;
  }

  public function getDatefin()
  {
    return $this->datefin;
  }

  public function setDatefin($datefin)
  {
    $this->datefin = $datefin;
  }

}
