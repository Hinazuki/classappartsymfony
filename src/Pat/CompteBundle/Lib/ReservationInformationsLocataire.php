<?php

namespace Pat\CompteBundle\Lib;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Pat\UtilisateurBundle\Repository\UtilisateurRepository")
 */
class ReservationInformationsLocataire extends BaseUser
{

  protected $civilite;
  public $nom;
  protected $prenom;
  protected $societe;
  protected $date_naissance;
  protected $nationalite;
  protected $langue_parle;
  protected $adresse;
  protected $adresse2;
  protected $code_postal;
  protected $ville;
  protected $pays;
  protected $telephone;
  protected $mobile;
  protected $mobile2;
  protected $adresse_fact;
  protected $adresse2_fact;
  protected $code_postal_fact;
  protected $ville_fact;
  protected $pays_fact;

  function __construct($civilite, $nom, $prenom, $societe, $date_naissance, $nationalite, $langue_parle, $adresse, $adresse2, $code_postal, $ville, $pays, $telephone, $mobile, $mobile2, $adresse_fact, $adresse2_fact, $code_postal_fact, $ville_fact, $pays_fact)
  {
    $this->civilite = $civilite;
    $this->nom = $nom;
    $this->prenom = $prenom;
    $this->societe = $societe;
    $this->date_naissance = $date_naissance;
    $this->nationalite = $nationalite;
    $this->langue_parle = $langue_parle;
    $this->adresse = $adresse;
    $this->adresse2 = $adresse2;
    $this->code_postal = $code_postal;
    $this->ville = $ville;
    $this->pays = $pays;
    $this->telephone = $telephone;
    $this->mobile = $mobile;
    $this->mobile2 = $mobile2;
    $this->adresse_fact = $adresse_fact;
    $this->adresse2_fact = $adresse2_fact;
    $this->code_postal_fact = $code_postal_fact;
    $this->ville_fact = $ville_fact;
    $this->pays_fact = $pays_fact;
  }

  public function getCivilite()
  {
    return $this->civilite;
  }

  public function setCivilite($civilite)
  {
    $this->civilite = $civilite;
  }

  public function getNom()
  {
    return $this->nom;
  }

  public function setNom($nom)
  {
    $this->nom = $nom;
  }

  public function getPrenom()
  {
    return $this->prenom;
  }

  public function setPrenom($prenom)
  {
    $this->prenom = $prenom;
  }

  public function getSociete()
  {
    return $this->societe;
  }

  public function setSociete($societe)
  {
    $this->societe = $societe;
  }

  public function getDateNaissance()
  {
    return $this->date_naissance;
  }

  public function setDateNaissance($date_naissance)
  {
    $this->date_naissance = $date_naissance;
  }

  public function getNationalite()
  {
    return $this->nationalite;
  }

  public function setNationalite($nationalite)
  {
    $this->nationalite = $nationalite;
  }

  public function getLangueParle()
  {
    return $this->langue_parle;
  }

  public function setLangueParle($langue_parle)
  {
    $this->langue_parle = $langue_parle;
  }

  public function getAdresse()
  {
    return $this->adresse;
  }

  public function setAdresse($adresse)
  {
    $this->adresse = $adresse;
  }

  public function getAdresse2()
  {
    return $this->adresse2;
  }

  public function setAdresse2($adresse2)
  {
    $this->adresse2 = $adresse2;
  }

  public function getCodePostal()
  {
    return $this->code_postal;
  }

  public function setCodePostal($code_postal)
  {
    $this->code_postal = $code_postal;
  }

  public function getVille()
  {
    return $this->ville;
  }

  public function setVille($ville)
  {
    $this->ville = $ville;
  }

  public function getPays()
  {
    return $this->pays;
  }

  public function setPays($pays)
  {
    $this->pays = $pays;
  }

  public function getTelephone()
  {
    return $this->telephone;
  }

  public function setTelephone($telephone)
  {
    $this->telephone = $telephone;
  }

  public function getMobile()
  {
    return $this->mobile;
  }

  public function setMobile($mobile)
  {
    $this->mobile = $mobile;
  }

  public function getMobile2()
  {
    return $this->mobile2;
  }

  public function setMobile2($mobile2)
  {
    $this->mobile2 = $mobile2;
  }

  public function getAdresseFact()
  {
    return $this->adresse_fact;
  }

  public function setAdresseFact($adresse_fact)
  {
    $this->adresse_fact = $adresse_fact;
  }

  public function getAdresse2Fact()
  {
    return $this->adresse2_fact;
  }

  public function setAdresse2Fact($adresse2_fact)
  {
    $this->adresse2_fact = $adresse2_fact;
  }

  public function getCodePostalFact()
  {
    return $this->code_postal_fact;
  }

  public function setCodePostalFact($code_postal_fact)
  {
    $this->code_postal_fact = $code_postal_fact;
  }

  public function getVilleFact()
  {
    return $this->ville_fact;
  }

  public function setVilleFact($ville_fact)
  {
    $this->ville_fact = $ville_fact;
  }

  public function getPaysFact()
  {
    return $this->pays_fact;
  }

  public function setPaysFact($pays_fact)
  {
    $this->pays_fact = $pays_fact;
  }

}
