<?php

namespace Pat\CompteBundle\Lib;

class RechercheAppartement
{

  protected $ville;
  protected $rayon;
  protected $datearrivee;
  protected $datedepart;
  protected $type;
  protected $nb_pieces;
  protected $budgetmini;
  protected $budgetmaxi;
  protected $reference;
  protected $nbPersonnes;

  function __construct($ville, $rayon, $datearrivee, $datedepart, $type, $nb_pieces, $budgetmini, $budgetmaxi, $reference, $nbPersonnes)
  {
    $this->ville = $ville;
    $this->rayon = $rayon;
    $this->datearrivee = $datearrivee;
    $this->datedepart = $datedepart;
    $this->type = $type;
    $this->nb_pieces = $nb_pieces;
    $this->budgetmini = $budgetmini;
    $this->budgetmaxi = $budgetmaxi;
    $this->nbPersonnes = $nbPersonnes;
  }

  public function getVille()
  {
    return $this->ville;
  }

  public function setVille($ville)
  {
    $this->ville = $ville;
  }

  public function getRayon()
  {
    return $this->rayon;
  }

  public function setRayon($rayon)
  {
    $this->rayon = $rayon;
  }

  public function getDatearrivee()
  {
    return $this->datearrivee;
  }

  public function setDatearrivee($datearrivee)
  {
    $this->datearrivee = $datearrivee;
  }

  public function getDatedepart()
  {
    return $this->datedepart;
  }

  public function setDatedepart($datedepart)
  {
    $this->datedepart = $datedepart;
  }

  public function getType()
  {
    return $this->type;
  }

  public function setType($type)
  {
    $this->type = $type;
  }

  public function getNbPieces()
  {
    return $this->nb_pieces;
  }

  public function setNbPieces($nb_pieces)
  {
    $this->nb_pieces = $nb_pieces;
  }

  public function getBudgetmini()
  {
    return $this->budgetmini;
  }

  public function setBudgetmini($budgetmini)
  {
    $this->budgetmini = $budgetmini;
  }

  public function getBudgetmaxi()
  {
    return $this->budgetmaxi;
  }

  public function setBudgetmaxi($budgetmaxi)
  {
    $this->budgetmaxi = $budgetmaxi;
  }

  public function getReference()
  {
    return $this->reference;
  }

  public function setReference($reference)
  {
    $this->reference = $reference;
  }

  /**
   * @return mixed
   */
  public function getNbPersonnes()
  {
    return $this->nbPersonnes;
  }

  /**
   * @param mixed $nbPersonnes
   *
   * @return RechercheAppartement
   */
  public function setNbPersonnes($nbPersonnes)
  {
    $this->nbPersonnes = $nbPersonnes;

    return $this;
  }

}
