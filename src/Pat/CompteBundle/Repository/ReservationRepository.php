<?php

namespace Pat\CompteBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Entities;
use Pat\CompteBundle\Entity\SiteTiers;
use Pat\CompteBundle\Tools\PaymentTools;
use Pat\CompteBundle\Tools\ReservationTools;

class ReservationRepository extends EntityRepository
{

  public function searchReservation($reference_resa = null, $reference_appart = null, $reference_locataire = "", $prenom_locataire = "", $nom_locataire = "", $date_debut = "", $date_fin = "", $onlyBetwween = true)
  {
    $qb = $this->_em->createQueryBuilder();
    $qb->select('r')
      ->from('Pat\CompteBundle\Entity\Reservation ', 'r')
      ->from('Pat\CompteBundle\Entity\Appartement ', 'a')
      ->from('Pat\UtilisateurBundle\Entity\Utilisateur ', 'u')
      ->where('a.id = r.appartement')
      ->andWhere('u.id = r.utilisateur');

    if (!empty($reference_resa))
      $qb->andWhere("r.reference like '%".$reference_resa."'");
    if (!empty($reference_appart))
      $qb->andWhere("a.reference like '%".$reference_appart."'");
    if (!empty($reference_locataire))
      $qb->andWhere("u.username like '%".$reference_locataire."%'");
    if (!empty($nom_locataire))
      $qb->andWhere("u.nom like '%".$nom_locataire."%'");
    if (!empty($prenom_locataire))
      $qb->andWhere("u.prenom like '%".$prenom_locataire."%'");

    if (!empty($date_debut) && !empty($date_fin)) {
      if (true == $onlyBetwween) {
        $qb->andWhere('(r.date_debut BETWEEN :date_debut AND :date_fin) OR (r.date_fin BETWEEN :date_debut AND :date_fin)');
      }
      else {
        $qb->andWhere('
             (r.date_debut <= :date_debut AND r.date_fin > :date_debut)
          OR (r.date_debut >= :date_debut AND r.date_fin <= :date_fin)
          OR (r.date_debut < :date_fin AND r.date_fin >= :date_fin)
        ');
      }
      $qb->setParameter(':date_debut', $date_debut);
      $qb->setParameter(':date_fin', $date_fin);
    }

    $qb->add('orderBy', 'r.created_at DESC');

    return $qb->getQuery()->getResult();
  }

  // Transforme une date au format jj/mm/aaaa au format aaaa-mm-jj
  public function toDateEn($date)
  {
    if (preg_match("#^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$#", $date))
      return substr($date, 6, 4)."-".substr($date, 3, 2)."-".substr($date, 0, 2);
    else
      return $date;
  }

  // Fonction qui retroune la liste des reservations dont la date de validité est dépassée
  public function getResaInvalide()
  {
    $date_act = date("Y-m-d H:i:s");

    $qb = $this->_em->createQueryBuilder();
    $qb->select('r')
      ->from('Pat\CompteBundle\Entity\Reservation ', 'r')
//      ->leftJoin('r.payments', 'p')
//      ->groupBy('r.id')
      ->where(":date > r.date_validite")
      ->andWhere("r.status IS NULL")
//      ->having('count(p.id) = 0')
      ->setParameter("date", $date_act);

    return $qb->getQuery()->getResult();
  }

  public function getResaByProprio($id_user)
  {
    $qb = $this->_em->createQueryBuilder();
    $qb->select('r')
      ->from('Pat\CompteBundle\Entity\Reservation ', 'r')
      ->from('Pat\CompteBundle\Entity\Appartement ', 'a')
      ->from('Pat\UtilisateurBundle\Entity\Utilisateur ', 'u')
      ->where('u.id = :user')
      ->andWhere("a.utilisateur = u.id")
      ->andWhere("r.appartement = a.id")
      ->orderBy("r.created_at", "DESC")
      ->setParameter('user', $id_user);

    return $qb->getQuery()->getResult();
  }

  public function getResaByIdandProprio($id_reservation, $id_user)
  {
    $qb = $this->_em->createQueryBuilder();
    $qb->select('r')
      ->from('Pat\CompteBundle\Entity\Reservation ', 'r')
      ->from('Pat\CompteBundle\Entity\Appartement ', 'a')
      ->from('Pat\UtilisateurBundle\Entity\Utilisateur ', 'u')
      ->where('u.id = '.$id_user)
      ->andWhere("a.utilisateur = u.id")
      ->andWhere("r.appartement = a.id")
      ->andWhere('r.id = '.$id_reservation)
      ->orderBy("r.created_at", "DESC");
    return $qb->getQuery()->getResult();
  }

  // Liste des appartements qui ont une reservation durant la periode donnée
  public function ListeAppResPeriode($dateDebut, $dateFin)
  {
    $ListeIdAppart = array();
    $ListeDate = array();
    $i = 0;
    $date = "";

    if ($dateDebut)
      $date1 = $this->toDateEn($dateDebut);
    else
      $date1 = date("Y-m-d");

    $date2 = $this->toDateEn($dateFin);

    $query_res = $this->_em->createQuery("
      SELECT r
      FROM PatCompteBundle:Reservation r
      WHERE (
           (r.date_debut <= :date_debut AND r.date_fin > :date_debut)
        OR (r.date_debut >= :date_debut AND r.date_fin <= :date_fin)
        OR (r.date_debut < :date_fin AND r.date_fin >= :date_fin)
      )
      AND r.status < :cancel
    ");

    $query_res->setParameter('date_debut', $date1);
    $query_res->setParameter('date_fin', $date2);
    $query_res->setParameter('cancel', ReservationTools::CONSTANT_STATUS_CANCELED_BY_CA);

    $qresult = $query_res->getResult();

    $i = 0;
    foreach ($qresult as $qr) {
      $ListeIdAppart[$i] = $qr->getAppartement()->getId();
      $i++;
    }

    return $ListeIdAppart;
  }

  // Recherche si un appartement à une réservation dans une période donnée
  public function AppHasResa($id = null, $dateDebut, $dateFin)
  {
    if ($dateDebut)
      $date1 = $this->toDateEn($dateDebut);
    else
      $date1 = date("Y-m-d");

    $date2 = $this->toDateEn($dateFin);
//
//           (r.date_debut < :date_debut AND r.date_fin > :date_debut)
//        OR (r.date_debut > :date_debut AND r.date_fin < :date_fin )
//        OR (r.date_debut < :date_fin AND r.date_fin > :date_fin)

    $query_res = $this->_em->createQuery("
      SELECT r
      FROM PatCompteBundle:Reservation r
      WHERE (
           (r.date_debut <= :date_debut AND r.date_fin > :date_debut)
        OR (r.date_debut >= :date_debut AND r.date_fin <= :date_fin)
        OR (r.date_debut < :date_fin AND r.date_fin >= :date_fin)
      )
      AND r.appartement = :id
    ");

    $query_res->setParameter('date_debut', $date1);
    $query_res->setParameter('date_fin', $date2);
    $query_res->setParameter('id', $id);

    $result = $query_res->getResult();

    return $result;
  }

  public function getResaValidByBien($id_bien, $date)
  {
    $qb = $this->_em->createQueryBuilder();
    $qb->select('r')
      ->from('Pat\CompteBundle\Entity\Reservation ', 'r')
      ->from('Pat\CompteBundle\Entity\Appartement ', 'a')
      ->where('a.id = :bien')
      ->andWhere("r.appartement = a.id")
      ->andWhere("r.status < :cancel")
      ->andWhere("r.date_debut BETWEEN :debut_mois AND :fin_mois
                        OR r.date_fin BETWEEN :debut_mois AND :fin_mois
                        OR (r.date_debut < :debut_mois AND r.date_fin > :fin_mois)")
      ->orderBy("r.created_at", "DESC")
      ->setParameter('bien', $id_bien)
      ->setParameter('cancel', ReservationTools::CONSTANT_STATUS_CANCELED_BY_CA)
      ->setParameter('debut_mois', $date.'-01')
      ->setParameter('fin_mois', $date.'-31');

    return $qb->getQuery()->getResult();
  }

  public function getCurrentReservations($user = null)
  {
    $qb = $this->_em->createQueryBuilder();
    $qb->select('r')
      ->from('Pat\CompteBundle\Entity\Reservation ', 'r')
      ->where('r.utilisateur = :user')
      ->andWhere("r.status = :statut_wait OR r.status = :statut_deposit_payed OR r.status = :statut_to_payed")
      ->orderBy("r.reference", "ASC")
      ->setParameter('user', $user)
      ->setParameter('statut_wait', ReservationTools::CONSTANT_STATUS_WAIT)
      ->setParameter('statut_deposit_payed', ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED)
      ->setParameter('statut_to_payed', ReservationTools::CONSTANT_STATUS_TO_PAYED);

    return $qb->getQuery()->getResult();
  }

  /**
   * @param SiteTiers $siteTiers
   *
   * @return int
   */
  public function countReservationsWithSiteTiers(SiteTiers $siteTiers)
  {
    $qb = $this->createQueryBuilder('r');

    $qb
      ->select('COUNT(r.id)')
      ->innerJoin('r.site_tiers', 'st')
      ->andWhere('st.id = :siteTiers')
      ->setParameter('siteTiers', $siteTiers->getId())
    ;

    return (int) $qb->getQuery()->getSingleScalarResult();
  }

  /**
   * @param null $paymentType
   *
   * @return \Doctrine\ORM\QueryBuilder
   */
  public function findReservationNotValidatedByProp($paymentType = null)
  {
    $date = new \DateTime('-2 days');

    $qb = $this->createQueryBuilder('r');

    $qb
      ->join('r.payments', 'p')
      ->andWhere('p.status = :paymentStatus')
      ->setParameter('paymentStatus', PaymentTools::CONSTANT_STATUS_PAYED)
      ->andWhere('p.payedAt < :date')
      ->setParameter('date', $date)
      ->andWhere('r.valid_prop IS NULL')
      ->andWhere('r.status = :status')
      ->setParameter('status', ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED);

    if (null !== $paymentType) {
      $qb
        ->andWhere('r.mode_paiement = :paymentType')
        ->setParameter('paymentType', $paymentType);
    }

    return $qb;
  }

  public function findReservationWithDepositNotPayed($date, $maxFlag)
  {
    $qb = $this->createQueryBuilder('r');

    $today = new \DateTime('today');

    return $qb
        ->andWhere($qb->expr()->orX('r.status < :status', 'r.status IS NULL'))
        ->setParameter('status', ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED)
        ->andWhere('r.created_at < :date')
        ->setParameter('date', $date)
        ->andWhere('r.date_debut > :today')
        ->setParameter('today', $today)
        ->andWhere('DATE_DIFF(r.date_debut, r.created_at) > 2')
        ->andWhere($qb->expr()->orX(
            'r.notification_acompte IS NULL', 'r.notification_acompte < :flag'
        ))
        ->setParameter('flag', $maxFlag);
  }

  public function findBookingWithDepositNotPayedForCanceled(){
      $qb = $this->createQueryBuilder('r');

      return $qb
          ->andWhere('r.status = :status')
          ->setParameter('status', ReservationTools::CONSTANT_STATUS_WAIT)
          ->andWhere('DATE_DIFF(CURRENT_TIMESTAMP(), r.created_at) >= 2');
  }

  public function findBookingForFirstPaymentReminder(){
    $qb = $this->createQueryBuilder('r');

    return $qb
        ->andWhere('r.status = 15')
        ->andWhere($qb->expr()->isNotNull('r.valid_prop'))
        ->andWhere('DATE_DIFF(CURRENT_TIMESTAMP(), r.created_at) >= 2')
        ->andWhere('DATE_DIFF(r.date_debut, CURRENT_DATE()) >= 2')
        ->andWhere('r.notification_payment IS NULL');
}

public function findBookingForOtherPaymentReminder(){
  $qb = $this->createQueryBuilder('r');

  return $qb
      ->andWhere('r.status = 15')
      ->andWhere($qb->expr()->isNotNull('r.valid_prop'))
      ->andWhere('DATE_DIFF(CURRENT_TIMESTAMP(), r.created_at) >= 14')
      ->andWhere('DATE_DIFF(r.date_debut, CURRENT_DATE()) >= 2')
      ->andWhere('DATE_DIFF(CURRENT_DATE(), r.notification_payment) = 14');
}

public function findBookingForLastFiveDayReminder(){
  $qb = $this->createQueryBuilder('r');

  return $qb
      ->andWhere('r.status = 15')
      ->andWhere($qb->expr()->isNotNull('r.valid_prop'))
      ->andWhere('DATE_DIFF(r.date_debut, CURRENT_DATE()) <= 5')
      ->andWhere('DATE_DIFF(CURRENT_DATE(), r.notification_payment) >= 1');
}

  /**
   * @param $reference
   */
  public function findResaByReferenceAndAppartement($reference, $appartement)
  {
    $qb = $this->createQueryBuilder('r');

    return $qb
        ->andWhere('r.reference LIKE :reference')
        ->setParameter('reference', '%'.$reference)
        ->andWhere('r.appartement = :appartement')
        ->setParameter('appartement', $appartement)
        ->setMaxResults(1)
        ->getQuery()
        ->getOneOrNullResult()
    ;
  }


  /**
   * Liste des réservations pour un champ select.
   *
   * @param int $apartmentId
   * @return array
   */
  public function listForApartment($apartmentId)
  {
    $qb = $this->createQueryBuilder('r');

    $r = $qb
      ->where('r.appartement_id = :appartementId')
      ->andWhere('r.status > :cancelledByOwner')
      ->andWhere('r.status < :cancelledByCA')
      ->orderBy('r.date_debut', 'desc')
      ->setParameters(array(
        'appartementId' => $apartmentId,
        'cancelledByOwner' => ReservationTools::CONSTANT_CANCEL_RESA_PROP,
        'cancelledByCA' => ReservationTools::CONSTANT_STATUS_CANCELED_BY_CA
      ))
      ->getQuery()
      ->getResult()
      ;

    $list = array();
    foreach ($r as $item) {
      $user = $item->getUtilisateur();
      $list[$item->getReference()] = sprintf(
        'Réf. %s de %s %s (%s) du %s au %s',
        $item->getReference(),
        $user->getPrenom(),
        $user->getNom(),
        $user->getUsername(),
        date('d/m/Y', strtotime($item->getDateDebut())),
        date('d/m/Y', strtotime($item->getDateFin()))
      );
    }
    return $list;
  }

}
