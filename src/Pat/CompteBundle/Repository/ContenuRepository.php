<?php

namespace Pat\CompteBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Entities;

class ContenuRepository extends EntityRepository
{

  public function findContenuByIdParent($id_parent, $langue)
  {
    $qb = $this->_em->createQueryBuilder();
    $qb->select('c')
      ->from('Pat\CompteBundle\Entity\Contenu ', 'c')
      ->where("c.id_parent = '0'")
      ->andWhere("c.langue = '".$langue."'")
      ->add('orderBy', 'c.tri ASC');

    return $qb;
  }

  // find Contenu by url with id different
  public function findOthersUrl($url, $id)
  {
    $qb = $this->_em->createQueryBuilder();
    $qb->select('c')
      ->from('Pat\CompteBundle\Entity\Contenu ', 'c')
      ->where("c.url = :url")
      ->setParameter("url", $url);

    if (!is_null($id)) {
      $qb->andWhere("c.id <> :id")
        ->setParameter("id", $id);
    }

    return $qb->getQuery()->getResult();
  }

}
