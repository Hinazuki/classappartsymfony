<?php

namespace Pat\CompteBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
//use Entities;
//use Pat\CompteBundle\Tools\ReservationTools;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;
//use Doctrine\ORM\Query\Expr;
use Pat\CompteBundle\Tools\ReservationTools;

class AppartementRepository extends EntityRepository
{

  public function toDateEn($date)
  {
    return substr($date, 6, 4)."-".substr($date, 3, 2)."-".substr($date, 0, 2);
  }

  // fonction qui renvoie une liste d'appartements en fonction du formyulaire de recherche
  public function searchAppartementFront($data, $offset = null, $limit = null, $nombre = null)
  {
    $qb = $this->_em->createQueryBuilder();

    if ($nombre) {
      $qb = $this->_em->createQueryBuilder();
      $qb->select('COUNT(a) as nombre')
        ->from('Pat\CompteBundle\Entity\Appartement', 'a')
        ->from('Pat\CompteBundle\Entity\Ville', 'v')
        ->from('Pat\CompteBundle\Entity\Tarif', 't');
    }
    else {
      $qb = $this->_em->createQueryBuilder();
      $qb->select('a')
        ->from('Pat\CompteBundle\Entity\Appartement', 'a')
        ->from('Pat\CompteBundle\Entity\Ville', 'v')
        ->from('Pat\CompteBundle\Entity\Tarif', 't');
    }

    $qb
      ->where('a.ville = v.id')
      ->andWhere('a.id = t.appartement')
    ;


    if (!empty($data['budgetmini']) && $data['budgetmini']) {
      $qb->andWhere('t.loyer_nuit >= (:Lmini)')
        ->setParameter('Lmini', $data['budgetmini']);
    }
    if (!empty($data['budgetmaxi']) && $data['budgetmaxi']) {
      $qb->andWhere('t.loyer_nuit <= (:Lmaxi)')
        ->setParameter('Lmaxi', $data['budgetmaxi']);
    }

    if (!empty($data['type']) && $data['type']) {
      $condition_type = "(";
      for ($i = 1; $i < count($data['type']); $i++) {
        $condition_type = $condition_type."a.type = '".$data['type'][$i]."' OR ";
      }
      $condition_type = $condition_type."a.type = '".$data['type'][0]."')";
      $qb->andWhere($condition_type);
    }

    if (!empty($data['nb_pieces']) && $data['nb_pieces']) {
      $condition_nb_pieces = "(";
      for ($i = 1; $i < count($data['nb_pieces']); $i++) {
        $condition_nb_pieces = $condition_nb_pieces."a.nb_pieces = '".$data['nb_pieces'][$i]."' OR ";
      }
      $condition_nb_pieces = $condition_nb_pieces."a.nb_pieces = '".$data['nb_pieces'][0]."')";
      $qb->andWhere($condition_nb_pieces);
    }

    if (!empty($data['ville']) && $data['ville']) {
      $ville = $data['ville'];
      if ($data['rayon']) {
        $listeVilles = $this->getEntityManager()->getRepository('PatCompteBundle:Ville')->ListeVillesAutour($ville, $data['rayon']);
        if ($listeVilles) {
          $qb->andWhere('v.nom IN (:ListeV)')
            ->setParameter('ListeV', $listeVilles);
        }
      }
      else {
        $qb->andWhere("v.nom = '".$ville."'");
      }
    }

    if ($data['nbPersonnes']) {
      $qb
        ->andWhere('a.nb_personne >= :nbPersonnes')
        ->setParameter('nbPersonnes', $data['nbPersonnes'])
      ;
    }

    if ($data['datearrivee'] && $data['datedepart']) {

      // On met à jour les réservations invalides
      // on teste la validité des réservations pour la période donnée
      // on met à jour le calendrier
      $ListeIdApp = $this->getEntityManager()->getRepository('PatCompteBundle:Reservation')->ListeAppResPeriode($data['datearrivee'], $data['datedepart']);
      if ($ListeIdApp) {
        $qb->andWhere('a.id NOT IN (:ListeApp)') // On filtre les apparts qui ont une reservation dans la période donnée
          ->setParameter('ListeApp', $ListeIdApp);
      }

      $ListeIdAppBloc = $this->getEntityManager()->getRepository('PatCompteBundle:Calendrier')->ListeAppResPeriode($data['datearrivee'], $data['datedepart']);
      if ($ListeIdAppBloc) {
        $qb->andWhere('a.id NOT IN (:ListeAppBloc)') // On filtre les apparts qui ont des dates bloquées dans la période donnée
          ->setParameter('ListeAppBloc', $ListeIdAppBloc);
      }
    }
    if (!$data['datearrivee'] && $data['datedepart']) {
      $ListeIdApp = $this->getEntityManager()->getRepository('PatCompteBundle:Reservation')->ListeAppResPeriode(null, $data['datedepart']); {
        $qb->andWhere('a.id NOT IN (:ListeApp)') // On filtre les apparts qui ont une reservation dans la période donnée
          ->setParameter('ListeApp', $ListeIdApp);
      }

      $ListeIdAppBloc = $this->getEntityManager()->getRepository('PatCompteBundle:Calendrier')->ListeAppResPeriode($data['datearrivee'], $data['datedepart']);
      if ($ListeIdAppBloc) {
        $qb->andWhere('a.id NOT IN (:ListeAppBloc)') // On filtre les apparts qui ont des dates bloquées dans la période donnée
          ->setParameter('ListeAppBloc', $ListeIdAppBloc);
      }
    }
    if ($offset != null && $limit != null) {
      $qb->setFirstResult($offset);
      $qb->setMaxResults($limit);
    }

    $qb->andWhere("a.statut = '4'"); // On ne récupère que les appartements publiés

    $qb->addOrderBy("t.loyer_nuit"); // On ne récupère que les appartements publiés


    return $qb->getQuery()->getResult();
  }

  //recherche un ou plusieurs appartement par critères
  public function searchAppartementQuery($type, $nb_pieces, $ville, $rue, $reference, $statut, $offset = null, $limit = null, $nombre = null, $id_user = null)
  {
    if ($nombre) {
      $qb = $this->_em->createQueryBuilder();
      $qb->select('COUNT(a) as nombre')
        ->from('Pat\CompteBundle\Entity\Appartement', 'a')
        ->from('Pat\CompteBundle\Entity\Ville', 'v')
        ->where('a.ville = v.id');
    }
    else {
      $qb = $this->_em->createQueryBuilder();
      $qb->select('a')
        ->from('Pat\CompteBundle\Entity\Appartement', 'a')
        ->from('Pat\CompteBundle\Entity\Ville', 'v')
        ->where('a.ville = v.id');
    }


    if (!empty($type)) {
      $qb->andWhere("a.type = '".$type."'");
    }

    if (!empty($nb_pieces)) {
      $qb->andWhere("a.nb_pieces = '".$nb_pieces."'");
    }

    if (!empty($ville)) {
      $qb->andWhere("v.nom LIKE '%".$ville."%'");
    }

    if (!empty($rue)) {
      $qb->andWhere("a.adresse LIKE '%".$rue."%'");
    }

    if (!empty($reference)) {
      $qb->andWhere("a.reference = ".$reference);
    }

    if ($statut != "") {
      $qb->andWhere("a.statut = '".$statut."'");
    }

    if ($id_user) {
      $qb->andWhere("a.utilisateur = '".$id_user."'");
    }

    $qb->add('orderBy', 'a.id DESC');

    if ($offset != null && $limit != null) {
      $qb->setFirstResult($offset);
      $qb->setMaxResults($limit);
    }

    return $qb->getQuery();
  }

  public function searchAppartement($type, $nb_pieces, $ville, $rue, $reference, $statut, $offset = null, $limit = null, $nombre = null, $id_user = null)
  {
    $query = $this->searchAppartementQuery($type, $nb_pieces, $ville, $rue, $reference, $statut, $offset, $limit, $nombre, $id_user);

    return $query->getResult();
  }

  //recherche un ou plusieurs appartement par critères
  public function rechercheBienProprietaire($id_user, $type = null, $nb_pieces = null, $ville = null, $surface = null, $surface_type = null, $loyer = null, $loyer_type = null, $offset = null, $limit = null, $nombre = null)
  {

    if ($nombre) {
      $qb = $this->_em->createQueryBuilder();
      $qb->select('COUNT(a) as nombre')
        ->from('Pat\CompteBundle\Entity\Appartement', 'a')
        ->from('Pat\CompteBundle\Entity\Ville', 'v');
    }
    else {
      $qb = $this->_em->createQueryBuilder();
      $qb->select('a')
        ->from('Pat\CompteBundle\Entity\Appartement', 'a')
        ->from('Pat\CompteBundle\Entity\Ville', 'v');
    }

    if (!empty($loyer)) {
      // Cela impose que le bien ai un tarif associé
      $qb->from('Pat\CompteBundle\Entity\Tarif', 't')
        ->where('a.ville = v.id')
        ->andWhere('a.id = t.appartement');


      if ($loyer_type == ">") {
        $qb->andWhere('t.loyer_nuit > (:infoloyer)')
          ->setParameter('infoloyer', $loyer);
      }
      else if ($loyer_type == "<") {
        $qb->andWhere('t.loyer_nuit < (:infoloyer)')
          ->setParameter('infoloyer', $loyer);
      }
      else {
        $qb->andWhere('t.loyer_nuit = (:infoloyer)')
          ->setParameter('infoloyer', $loyer);
      }
    }
    else {
      $qb->where('a.ville = v.id');
    }


    if (!empty($type)) {
      $qb->andWhere("a.type = '".$type."'");
    }

    if (!empty($nb_pieces)) {
      $qb->andWhere("a.nb_pieces = '".$nb_pieces."'");
    }

    if (!empty($ville)) {
      $qb->andWhere("v.nom LIKE '%".$ville."%'");
    }

    if (!empty($surface)) {
      if ($surface_type == ">") {
        $qb->andWhere("a.surface_sol > ".str_replace(' ', '', $surface));
      }
      else if ($surface_type == "<") {
        $qb->andWhere("a.surface_sol < ".str_replace(' ', '', $surface));
      }
      else {
        $qb->andWhere("a.surface_sol = ".str_replace(' ', '', $surface));
      }
    }



    $qb->andWhere("a.utilisateur = '".$id_user."'");


    $qb->add('orderBy', 'a.id DESC');

    if ($offset != null && $limit != null) {
      $qb->setFirstResult($offset);
      $qb->setMaxResults($limit);
    }


    return $qb->getQuery()->getResult();
  }

  // récupère les appartements d'un utilisateur pour les afficher dans les listes déroulantes
  // permettant ensuite de visualiser un calendrier
  public function findAppartByUtilisateur($id_user)
  {
    $qb = $this->createQueryBuilder('a');
    $qb->where('a.utilisateur = :id_user')
      ->andWhere('a.statut < 9')
      ->setParameter('id_user', $id_user);
    //$qb->setMaxResults(100);
    //on renvoi à la liste déroulante un querybuilder et non pas une liste de résultats
    return $qb;
  }

  //clean la chaine (accent, espace, caractères spéciaux)
  public function string2url($chaine)
  {
    $chaine = trim($chaine);
    /* $chaine = strtr($chaine,
      "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ",
      "aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn"); */
    $chaine = htmlentities($chaine, ENT_NOQUOTES, 'utf-8');

    $chaine = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $chaine);
    $chaine = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $chaine); // pour les ligatures e.g. '&oelig;'
    $chaine = preg_replace('#&[^;]+;#', '', $chaine); // supprime les autres caractères
    //$chaine = strtr($chaine,"ABCDEFGHIJKLMNOPQRSTUVWXYZ","abcdefghijklmnopqrstuvwxyz");

    $chaine = strtolower($chaine);

    $chaine = preg_replace('#([^.a-z0-9]+)#i', '-', $chaine);
    $chaine = preg_replace('#-{2,}#', '-', $chaine);
    $chaine = preg_replace('#-$#', '', $chaine);
    $chaine = preg_replace('#^-#', '', $chaine);
    return $chaine;
  }

  // retourne une liste d'appartements en fonction d'un tableau d'identifiants. limite pour limiter le nombre de résultats (passer null sinon)
  public function getManyAppartementById($tabid, $limite)
  {
    $qb = $this->_em->createQueryBuilder();
    $qb->select('a')
      ->from('Pat\CompteBundle\Entity\Appartement', 'a')
      ->where('a.id IN (:TabId)')
      ->setParameter('TabId', $tabid);

    if ($limite) {
      $qb->setMaxResults($limite);
    }
    return $qb->getQuery()->getResult();
  }

  public function isAvailableForResa($id_bien, $date_debut, $date_fin, $id_resa = null)
  {

    // On formatte les dates au bon format si elles ne le sont pas
    if (preg_match("/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/", $date_debut))
      $date_debut = $this->toDateEn($date_debut);

    if (preg_match("/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/", $date_fin))
      $date_fin = $this->toDateEn($date_fin);


    /** @var Query $query_cal */
    $query_cal = $this->_em->createQuery("
      SELECT c FROM PatCompteBundle:Calendrier c
      WHERE c.appartement = :id_app
        AND (
             (c.date_debut <= :date_debut AND c.date_fin > :date_debut)
          OR (c.date_debut >= :date_debut AND c.date_fin <= :date_fin)
          OR (c.date_debut < :date_fin AND c.date_fin >= :date_fin)
        )
    ");

    $query_cal->setParameter('id_app', $id_bien);
    $query_cal->setParameter('date_debut', $date_debut);
    $query_cal->setParameter('date_fin', $date_fin);

    $result_cal = $query_cal->getResult();
    //si l'appartement est bloqué
    if (sizeof($result_cal) > 0)
      return false;
    else {
      // status < 40 : not canceled
      $text_req = "
        SELECT r FROM PatCompteBundle:Reservation r
        WHERE r.appartement = :id_app
          AND ( r.status IS NULL OR r.status < :cancel)
          AND (
               (r.date_debut <= :date_debut AND r.date_fin > :date_debut)
            OR (r.date_debut >= :date_debut AND r.date_fin <= :date_fin)
            OR (r.date_debut < :date_fin AND r.date_fin >= :date_fin)
          )
      ";

      if (!is_null($id_resa))
        $text_req .= " AND r.id <> :id_resa";

      $query_res = $this->_em->createQuery($text_req);

      $query_res->setParameter('id_app', $id_bien);
      $query_res->setParameter('date_debut', $date_debut);
      $query_res->setParameter('date_fin', $date_fin);
      $query_res->setParameter('cancel', ReservationTools::CONSTANT_STATUS_CANCELED_BY_CA);

      if (!is_null($id_resa))
        $query_res->setParameter('id_resa', $id_resa);

      $result_res = $query_res->getResult();

      //si l'appartement est réservé
      if (sizeof($result_res) > 0)
        return false;
      else
        return true;
    }
  }

  public function getDernierAjouts()
  {
    $qb = $this->_em->createQueryBuilder();
    $qb->select('a')
      ->from('Pat\CompteBundle\Entity\Appartement', 'a')
      ->from('Pat\CompteBundle\Entity\Tarif', 't')
      ->where('t.appartement = a.id')
      ->andWhere("a.statut = '4'")
      ->andWhere("a.is_selection = 1")
      ->orderBy("a.created_at", "ASC")
      ->setMaxResults(3);

    return $qb->getQuery()->getResult();
  }

  /**
   * Retourne les indisponibilités (blocages calendrier, réservations) d'un bien.
   *
   * @param string $ref
   * @param integer $upTo Timestamp
   * @return array
   */
  public function findBusyTime($ref, $upTo = null)
  {
    $upTo = date('Y-m-d', $upTo ? $upTo : strtotime('+3 months'));

    $parameters = array(
      new Parameter('ref', $ref),
      new Parameter('today', date('Y-m-d')),
      new Parameter('upto', $upTo),
    );

    $busy = array();

    $dql = '
      SELECT a.reference, c.id,
        c.date_debut, c.date_fin, c.heure_debut, c.heure_fin, c.commentaire
      FROM PatCompteBundle:Appartement a
      LEFT JOIN PatCompteBundle:Calendrier c
        WITH c.appartement_id = a.id
      WHERE a.reference = :ref
        AND c.date_fin >= :today AND c.date_debut <= :upto
      ORDER BY c.date_debut
    ';

    $query = $this->getEntityManager()
      ->createQuery($dql)
      ->setParameters(new ArrayCollection($parameters));

    $data = $query->getResult();

    $this->_processCalendarBusyTimes($busy, $data);

    $dql = '
      SELECT a.reference AS aref, r.reference AS rref,
        r.date_debut, r.date_fin
      FROM PatCompteBundle:Appartement a
      LEFT JOIN PatCompteBundle:Reservation r
        WITH r.appartement_id = a.id
      WHERE a.reference = :ref
        AND r.status < :status
        AND r.date_fin >= :today AND r.date_debut <= :upto
      ORDER BY r.date_debut
    ';

    $parameters[] = new Parameter(
      'status',
      ReservationTools::CONSTANT_STATUS_CANCELED_BY_CA
    );

    $query = $this->getEntityManager()
      ->createQuery($dql)
      ->setParameters(new ArrayCollection($parameters));

    $data = $query->getResult();

    $this->_processBookingBusyTimes($busy, $data);

    ksort($busy);

    return $this->_processOverlappings($busy);
  }

  /**
   * Traite les éléments de calendrier.
   *
   * @param array $busy
   * @param array $data
   * @author AG
   */
  protected function _processCalendarBusyTimes(&$busy, $data)
  {
    if (!empty($data)) {
      foreach ($data as $item) {
        $baseUid = $item['reference'].'-'.$item['id'].'-';

        $start = strtotime($item['date_debut'].' '.(
          $item['heure_debut'] ? $item['heure_debut']->format('H:i:s') : '16:00:00'
        ));

        // On ajoute les périodes intermédiaires en journée pleine.
        if ($item['date_fin'] != $item['date_debut']) {
          // De l'heure de début jusqu'à minuit.
          $busy[$start] = array(
            'value' => 'date-time',
            'start' => $start,
            'end' => strtotime($item['date_debut'].' 23:59:59'),
            'uid' => $baseUid.$start,
            'summary' => $item['commentaire']
          );

          // Jours suivants.
          $start = strtotime('+1 day', strtotime($item['date_debut']));
          $busy[$start] = array(
            'value' => 'date',
            'start' => $start,
            // = Jusqu'à minuit (date_fin - 1 jour) inclus.
            'end' => strtotime($item['date_fin']),
            'uid' => $baseUid.$start,
            'summary' => $item['commentaire']
          );

          // Jour de fin, de minuit à l'heure de fin.
          // Non pris en compte, sur demande de CA.
          /*
          $start = strtotime($item['date_fin'].' 00:00:00');
          $busy[$start] = array(
            'value' => 'date-time',
            'start' => $start,
            'end' => strtotime($item['date_fin'].' '.(
              $item['heure_fin'] ? $item['heure_fin']->format('H:i:s') : '11:00:00'
            )),
            'uid' => $baseUid.$start,
            'summary' => $item['commentaire']
          );
          */
        }
        else {
          $busy[$start] = array(
            'value' => 'date-time',
            'start' => $start,
            'end' => strtotime($item['date_fin'].' '.(
              $item['heure_fin'] ? $item['heure_fin']->format('H:i:s') : '23:59:59'
            )),
            'uid' => $baseUid.$start,
            'summary' => $item['commentaire']
          );
        }
      }
    }
  }

  /**
   * Traite les éléments de réservation.
   *
   * @param array $busy
   * @param array $data
   * @author AG
   */
  protected function _processBookingBusyTimes(&$busy, $data)
  {
    if (!empty($data)) {
      foreach ($data as $item) {
        $baseUid = $item['aref'].'-'.$item['rref'].'-';

        // De l'heure de début jusqu'à minuit.
        $start = strtotime($item['date_debut'].' '.'16:00:00');
        $busy[$start] = array(
          'value' => 'date-time',
          'start' => $start,
          'end' => strtotime($item['date_debut'].' 23:59:59'),
          'uid' => $baseUid.$start,
          'summary' => null
        );

        // On ajoute les périodes intermédiaires en journée pleine.
        // Jours suivants.
        $start = strtotime('+1 day', strtotime($item['date_debut']));
        $busy[$start] = array(
          'value' => 'date',
          'start' => $start,
          // = Jusqu'à minuit (date_fin - 1 jour) inclus.
          'end' => strtotime($item['date_fin']),
          'uid' => $baseUid.$start,
          'summary' => null
        );

        // Jour de fin, de minuit à l'heure de fin.
        // Non pris en compte, sur demande de CA.
        /*
        $start = strtotime($item['date_fin'].' 00:00:00');
        $busy[$start] = array(
          'value' => 'date-time',
          'start' => $start,
          'end' => strtotime($item['date_fin'].' '.'11:00:00'),
          'uid' => $baseUid.$start,
          'summary' => null
        );
        */
      }
    }
  }

  /**
   * Fusionne les plage de temps qui se chevauchent.
   *
   * @param array $busy
   * @return array
   */
  protected function _processOverlappings($busy)
  {
    $merged = array();
    $previousKey = null;
    $previous = null;

    foreach ($busy as $key => $item) {
      if ($previous && $previous['end'] > $item['start']) {
        $previous['end'] = $item['end'];
        $merged[$previousKey] = $previous;
      }
      else {
        $merged[$key] = $item;
        $previousKey = $key;
        $previous = $item;
      }
    }

    return $merged;
  }

}
