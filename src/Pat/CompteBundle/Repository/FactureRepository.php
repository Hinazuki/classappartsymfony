<?php

namespace Pat\CompteBundle\Repository;

use Doctrine\ORM\EntityRepository;

class FactureRepository extends EntityRepository
{

  /**
   * @param $year
   *
   * @return int
   */
  public function countFacturesInYear($year)
  {
    $qb = $this->createQueryBuilder('f');

    return $qb
        ->select('COUNT(f.id)')
        ->andWhere('f.number LIKE :year')
        ->setParameter('year', $year.'-%')
        ->andWhere('f.locale = :frLocale')
        ->setParameter('frLocale', 'fr')
        ->getQuery()
        ->getSingleScalarResult()
    ;
  }

}
