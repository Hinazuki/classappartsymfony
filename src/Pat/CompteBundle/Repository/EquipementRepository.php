<?php

namespace Pat\CompteBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Entities;

class EquipementRepository extends EntityRepository
{

  //récupère les équipements en fonction de la catégorie choisie
  public function findEquipementByCategorie($categorie_id)
  {


    $query = $this->createQueryBuilder('c')
      ->addSelect('e')
      ->leftJoin('c.equipement_categorie', 'e')
      ->where('e.id = :id ')
      ->setParameter('id', $categorie_id)
    //->orderBy('c.id', 'DESC')
    ;
    return $query;
  }

  /**
   * Test si l'équipement existe déjà
   */
  public function checkIntituleEquipement($equipement, $id = "")
  {

    $qb = $this->_em->createQueryBuilder();
    $qb->select("e")
      ->from("Pat\CompteBundle\Entity\Equipement", "e")
      ->where('e.intitule = :equipement');

    if (!empty($id)) {
      $qb->andWhere("e.id != '$id'");
    }

    $qb->setParameter('equipement', $equipement);

    $result = $qb->getQuery()->getResult();

    if (sizeof($result) > 0) {
      return true;
    }
    else {
      return false;
    }
  }

  public function findEquipementOrderedByCat()
  {
    $query = $this->createQueryBuilder('c')
      ->addSelect('e')
      ->leftJoin('c.equipement_categorie', 'e')
      ->andWhere('c.multi = false')
      ->orderBy('e.intitule_categorie', 'ASC')
    ;

    return $query;
  }

  /**
   * @return \Doctrine\ORM\QueryBuilder
   */
  public function findMultiEquipements()
  {
    $qb = $this->createQueryBuilder('e');

    return $qb
        ->andWhere('e.multi = true')
        ->getQuery()
        ->getResult()
    ;
  }

}
