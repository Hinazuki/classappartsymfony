<?php

namespace Pat\CompteBundle\Repository;

use Pat\CompteBundle\AST\Geo;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\Lexer;
use Entities;

class VilleRepository extends EntityRepository
{

  // Récupere toutes les villes
  public function getVillesBySearch($search_nom, $search_cp, $search_quartier = null)
  {
    $qb = $this->getVillesBySearchQuery($search_nom, $search_cp, $search_quartier);

    return $qb->getQuery();
  }

  // Récupere toutes les villes
  public function getVillesBySearchQuery($search_nom, $search_cp, $search_quartier = null)
  {
    $qb = $this->_em->createQueryBuilder();
    $qb->select('v')
      ->from('Pat\CompteBundle\Entity\Ville', 'v');

    if (!is_null($search_nom)) {
      $qb->andWhere("v.nom LIKE :nom")
        ->setParameter("nom", '%'.$search_nom.'%');
    }

    if (!is_null($search_cp)) {
      $qb->andWhere("v.code_postal LIKE :cp")
        ->setParameter("cp", $search_cp.'%');
    }

    if (!is_null($search_quartier)) {
      $qb->andWhere("v.quartier = 1");
    }

    $qb->orderBy('v.nom');

    return $qb;
  }

  // Fonction qui renvoie la liste des villes autour de celle donnée en parametre en fonction de la distance
  public function ListeVillesAutour($ville, $distance)
  {
    $listeVilles = array();
    $i = 0;
    $qb1 = $this->_em->createQueryBuilder();

    $qb1->select('v')
      ->from('Pat\CompteBundle\Entity\Ville', 'v')
      ->where("v.nom = '".$ville."'")
      ->setMaxResults(1); // Pour éviter les doublons dans la base des villes (Pour la ville temoin)

    $tab = $qb1->getQuery()->getResult();
    $info = $tab[0];

    $qb2 = $this->_em->createQueryBuilder()
      ->from('Pat\CompteBundle\Entity\Ville', 'v')
      ->select('DISTINCT(v.nom)') // Eviter les doublons dans la base (Pour les autres villes)
      ->where('GEO(v.latitude = :latitude, v.longitude = :longitude) < '.$distance)
      ->setParameter('latitude', $info->getLatitude())
      ->setParameter('longitude', $info->getLongitude());

    $ListeTemp = $qb2->getQuery()->getResult();

    foreach ($ListeTemp as $ville):
      $listeVilles[$i] = $ville['nom'];
      $i++;
    endforeach;

    return $listeVilles;
  }

  // Récupere les villes qui commencent par la variable texte
  public function getVillesByCaract($texte)
  {
    $qb = $this->_em->createQueryBuilder();
    $qb->select('DISTINCT(v.nom)')
      ->from('Pat\CompteBundle\Entity\Ville', 'v')
      ->where("v.nom like '".$texte."%'")
      ->orderBy('v.nom')
      ->setMaxResults('10');

    return $qb->getQuery()->getResult();
  }

  // Récupere les villes qui commencent par la variable texte
  public function getAllVillesByCaract($texte)
  {
    $qb = $this->_em->createQueryBuilder();
    $qb->select('DISTINCT(v.nom), v.code_postal')
      ->from('Pat\CompteBundle\Entity\Ville', 'v')
      ->where("v.nom like '".$texte."%'")
      ->orderBy('v.nom')
      ->setMaxResults('10');

    return $qb->getQuery()->getResult();
  }

  // Récupere les villes en fonction d'une liste de département sous la forme xx-xx...
  public function getVillesByListeDep($liste)
  {
    $tab_dep = explode("-", $liste);
    if (isset($tab_dep[0])) {
      $temp_limit = $tab_dep[0] + 1;
      $region_limit_1 = $tab_dep[0] * 1000;
      $region_limit_2 = $temp_limit * 1000;

      $qb = $this->_em->createQueryBuilder();
      $qb->select('v')
        ->from('Pat\CompteBundle\Entity\Ville', 'v')
        ->where("v.code_postal >= ".$region_limit_1." AND v.code_postal <= ".$region_limit_2);
      for ($i = 1; $i < count($tab_dep); $i++) {
        $temp_limit = $tab_dep[$i] + 1;
        $region_limit_1 = $tab_dep[$i] * 1000;
        $region_limit_2 = $temp_limit * 1000;
        $region_limit_2--;

        $qb->orWhere("v.code_postal >= ".$region_limit_1." AND v.code_postal <= ".$region_limit_2);
      }

      $qb->orderBy('v.nom');
      //echo $qb;

      return $qb->getQuery()->getResult();
    }
    else {
      return array();
    }
  }

}
