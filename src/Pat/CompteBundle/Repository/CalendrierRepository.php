<?php

namespace Pat\CompteBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Entities;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Pat\CompteBundle\Tools\CalendrierTools;
use Pat\CompteBundle\Tools\ReservationTools;

class CalendrierRepository extends EntityRepository
{

  // Transforme une date au format jj/mm/aaaa au format aaaa-mm-jj
  public function toDateEn($date)
  {
    if (preg_match("#^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$#", $date))
      return substr($date, 6, 4)."-".substr($date, 3, 2)."-".substr($date, 0, 2);
    else
      return $date;
  }

  // Liste des appartements qui ont une reservation durant la periode donnée
  public function ListeAppResPeriode($dateDebut, $dateFin)
  {
    $ListeIdAppart = array();
    $ListeDate = array();
    $i = 0;
    $date = "";

    if ($dateDebut)
      $date1 = $this->toDateEn($dateDebut);
    else
      $date1 = date("Y-m-d");

    $date2 = $this->toDateEn($dateFin);

    $query_cal = $this->_em->createQuery("
      SELECT c
      FROM PatCompteBundle:Calendrier c
      WHERE (
           (c.date_debut <= :date_debut AND c.date_fin >= :date_debut)
        OR (c.date_debut > :date_debut AND c.date_fin < :date_fin)
        OR (c.date_debut <= :date_fin AND c.date_fin >= :date_fin)
      )
      AND c.status <> :menage
    ");

    $query_cal->setParameter('date_debut', $date1);
    $query_cal->setParameter('date_fin', $date2);
    $query_cal->setParameter('menage', CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE);

    $qresult = $query_cal->getResult();

    $i = 0;
    foreach ($qresult as $qr) {
      $ListeIdAppart[$i] = $qr->getAppartement()->getId();
      $i++;
    }

    return $ListeIdAppart;
  }

  // Liste des appartements qui ont une date bloquée durant la periode donnée
  public function AppHasDateBloquee($id = null, $dateDebut, $dateFin)
  {
    if ($dateDebut)
      $date1 = $this->toDateEn($dateDebut);
    else
      $date1 = date("Y-m-d");

    $date2 = $this->toDateEn($dateFin);

    $query_cal = $this->_em->createQuery("
      SELECT c
      FROM PatCompteBundle:Calendrier c
      WHERE (
           (c.date_debut <= :date_debut AND c.date_fin > :date_debut)
        OR (c.date_debut >= :date_debut AND c.date_fin <= :date_fin)
        OR (c.date_debut < :date_fin AND c.date_fin >= :date_fin)
      )
      AND c.appartement = :id
      AND c.status <> :menage
    ");

    $query_cal->setParameter('date_debut', $date1);
    $query_cal->setParameter('date_fin', $date2);
    $query_cal->setParameter('id', $id);
    $query_cal->setParameter('menage', CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE);

    $result = $query_cal->getResult();

    return $result;
  }

  // Retourne un double tableau [date y-m-d][ [1ou2] [1ou2] ... ] en fonction d'un id appartement
  public function ListeResaByApp($id)
  {
    $tabresa = array();

    // On recherche les dates bloquées de l'appartement
    $rsm = new ResultSetMappingBuilder($this->getEntityManager());
    $rsm->addRootEntityFromClassMetadata('Pat\CompteBundle\Entity\Calendrier', 'c');
    $nativeQuery = $this->_em->createNativeQuery("SELECT * FROM Calendrier WHERE appartement_id = ".$id." ORDER BY date_debut", $rsm);
    $result = $nativeQuery->getResult();

    foreach ($result as $res) {
      $i = 0;
      $date = $res->getDateDebut();
      $datetab = explode("-", $res->getDateDebut());

      $value = $res->getStatus();

      if ($date == $res->getDateFin())
        $tabresa[$date][] = $value;
      else {
        while ($date < $res->getDateFin()) {
          $date = date("Y-m-d", mktime(0, 0, 0, $datetab[1], $datetab[2] + $i, $datetab[0]));
          $tabresa[$date][] = $value;

          // Adding entry on the first and the last day of reservation
          if ($date == date("Y-m-d", mktime(0, 0, 0, $datetab[1], $datetab[2], $datetab[0])))
            $tabresa[$date][] = 'resa_start';
          if ($date == $res->getDateFin())
            $tabresa[$date][] = 'resa_end';

          $i++;
        }
      }
    }

    // On recherche les réservations de l'appartement
    $rsm = new ResultSetMappingBuilder($this->getEntityManager());
    $rsm->addRootEntityFromClassMetadata('Pat\CompteBundle\Entity\Reservation', 'r');
    $nativeQuery = $this->_em->createNativeQuery("SELECT * FROM Reservation WHERE appartement_id = ".$id." AND status < ".ReservationTools::CONSTANT_STATUS_CANCELED_BY_CA." ORDER BY date_debut", $rsm);
    $result = $nativeQuery->getResult();

    foreach ($result as $res) {
      $i = 0;
      $date = $res->getDateDebut();
      $datetab = explode("-", $res->getDateDebut());
      while ($date < $res->getDateFin()) {
        $date = date("Y-m-d", mktime(0, 0, 0, $datetab[1], $datetab[2] + $i, $datetab[0]));

        $tabresa[$date][] = "res_".$res->getStatus(); // On ajoute un préfixe pour savoir que le statut est eclui associé à une réservation
        if ($res->getStatus() == ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED) {
          $tabresa[$date][] = "res_vp_".$res->getValidProp(); // On ajoute une variable pour connaitre l'état de la validation du propriétaire
        }

        // Adding entry on the first and the last day of reservation
        if ($date == date("Y-m-d", mktime(0, 0, 0, $datetab[1], $datetab[2], $datetab[0])))
          $tabresa[$date][] = 'resa_start';
        if ($date == $res->getDateFin())
          $tabresa[$date][] = 'resa_end';

        $i++;
      }
    }

    return $tabresa;
  }

  public function getDatesByBien($id_bien, $date)
  {
    $qb = $this->_em->createQueryBuilder();
    $qb->select('c')
      ->from('Pat\CompteBundle\Entity\Calendrier ', 'c')
      ->from('Pat\CompteBundle\Entity\Appartement ', 'a')
      ->where('a.id = :bien')
      ->andWhere("c.appartement = a.id")
      ->andWhere("c.date_debut BETWEEN :debut_mois AND :fin_mois
                        OR c.date_fin BETWEEN :debut_mois AND :fin_mois
                        OR (c.date_debut < :debut_mois AND c.date_fin > :fin_mois)")
      ->orderBy("c.created_at", "DESC")
      ->setParameter('bien', $id_bien)
      ->setParameter('debut_mois', $date.'-01')
      ->setParameter('fin_mois', $date.'-31');

    return $qb->getQuery()->getResult();
  }

}
