<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Pat\CompteBundle\Repository\VilleRepository;
use Pat\CompteBundle\Form\Type\VilleType;

class AdminAppartementForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('type', 'choice', array(
        'label' => "Type de bien",
        'choices' => array('APPARTEMENT' => 'Appartement',
          'STUDIO' => 'Studio',
          'MAISON' => 'Maison')))
      ->add('nb_pieces', 'choice', array(
        'label' => "Nombre de pièces",
        'choices' => array(1 => 'T1',
          2 => 'T2',
          3 => 'T3',
          4 => 'T4',
          5 => 'T5',
          6 => 'T6')))
      ->add('is_meuble', 'choice', array(
        'label' => "Meublé",
        'choices' => array(true => 'Meublé',
          false => 'Non meublé')))
      ->add('description_courte_fr', 'textarea', array(
        'label' => "Version courte",
        "required" => true,
        'max_length' => 200))
      ->add('description_fr', 'textarea', array(
        'label' => "Version longue",
        "required" => true))
      ->add('nom_residence', 'text', array(
        'label' => "Nom résidence",
        "required" => false))
      ->add('adresse', 'text', array('label' => "Adresse"))
      ->add('adresse2', 'text', array(
        'label' => "Adresse suite",
        'required' => false))
      ->add('ville', 'ville_selector')
      ->add('distance_aeroport', 'text', array('label' => "Distance aéroport", "required" => false))
      ->add('distance_autoroute', 'text', array('label' => "Distance autoroute", "required" => false))
      ->add('distance_voie_rapide', 'text', array('label' => "Distance voies rapides", "required" => false))
      ->add('distance_commerce', 'text', array('label' => "Distance des commerces", "required" => false))
      ->add('acces_gare', 'text', array('label' => "Accès gare", "required" => false))
      ->add('acces_marche', 'text', array('label' => "Accès marché", "required" => false))
      ->add('acces_bus', 'text', array('label' => "Accès Bus", "required" => false))
      //->add('acces_metro', 'text', array('label' => "Accès Métro", "required" => false))
      ->add('acces_tram', 'text', array('label' => "Accès Tram", "required" => false))
      ->add('arret_tram', 'text', array('label' => "Arrêt de Tram à proximité", "required" => false))
      ->add('arret_tram2', 'text', array('label' => "Arrêt de Tram à proximité (2)", "required" => false))
      ->add('arret_tram3', 'text', array('label' => "Arrêt de Tram à proximité (3)", "required" => false))
      ->add('surface_sol', 'text', array('label' => "Surface"))
      ->add('surface_carrez', 'text', array('label' => "Surface carrez", "required" => false))
      ->add('surface_terrain', 'text', array('label' => "Surface terrain", "required" => false))
      ->add('nb_personne', 'text', array('label' => "Nombre de personnes"))
      ->add('nb_personne', 'choice', array(
        'label' => "Nombre de personnes",
        'choices' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20')))
      ->add('nb_lit_simple', 'choice', [
        'label' => 'Nombre de lits simples',
        'choices' => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
      ])
      ->add('nb_lit_double', 'choice', [
        'label' => 'Nombre de lits doubles',
        'choices' => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
      ])
      ->add('nb_lit_simple_double', 'choice', [
        'label' => 'Nombre de double lit simples / Lit doubles',
        'choices' => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
      ])
      ->add('nb_canape_convertible', 'choice', [
        'label' => 'Nombre de canapés convertibles',
        'choices' => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
      ])
      ->add('etage', 'choice', array(
        'label' => "Etage",
        "required" => false,
        'choices' => array('0' => 'RDC', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20')))
      ->add('nb_etage', 'choice', array(
        'label' => "Nombre d'étage",
        "required" => false,
        'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20')))
      ->add('entree_immeuble', 'choice', array(
        'label' => "Entrée de l'immeuble",
        "required" => false,
        'choices' => array('' => 'Non communiqué',
          'clef' => 'Clef',
          'code' => 'Code'
      )))
      ->add('serrure', 'checkbox', array('label' => "Serrure", "required" => false))
      ->add('cave', 'text', array('label' => "Nombre de caves", "required" => false))
      ->add('parking', 'text', array('label' => "Nombre de parkings", "required" => false))
      ->add('terrasse', 'text', array('label' => "Nombre de terrasses", "required" => false))
      ->add('balcon', 'text', array('label' => "Nombre de balcons", "required" => false))
      ->add('garage', 'text', array('label' => "Nombre de garages", "required" => false))
      ->add('escalier', 'checkbox', array('label' => "Escalier", "required" => false))
      ->add('ascenseur', 'checkbox', array('label' => "Ascenceur", "required" => false))
      ->add('parking', 'text', array('label' => "Nombre de parkings", "required" => false))
      ->add('terrasse', 'text', array('label' => "Nombre de terrasses", "required" => false))
      ->add('balcon', 'text', array('label' => "Nombre de balcons", "required" => false))
      ->add('garage', 'text', array('label' => "Nombre de garages", "required" => false))
      ->add('grenier', 'checkbox', array('label' => "Grenier", "required" => false))
      ->add('jardin', 'checkbox', array('label' => "Jardin", "required" => false))
      ->add('vide_ordure', 'checkbox', array('label' => "Vide ordure", "required" => false))
      ->add('stationnement', 'choice', array(
        'label' => "Stationnement",
        'choices' => array(
          'aucun' => 'Aucun',
          'Parking découvert payant à proximité' => 'Parking découvert payant à proximité',
          'Parking couvert payant à proximité' => 'Parking couvert payant à proximité',
          'Parking découvert gratuit à proximité' => 'Parking découvert gratuit à proximité',
          'Parking couvert gratuit à proximité' => 'Parking couvert gratuit à proximité',
          'Parking découvert inclus à proximité' => 'Parking découvert inclus à proximité',
          'Parking couvert inclus à proximité' => 'Parking couvert inclus à proximité',
          'Parking privé couvert inclus' => 'Parking privé couvert inclus',
          'Parking privé découvert inclus' => 'Parking privé découvert inclus'
        )
      ))
      ->add('interphone', 'checkbox', array('label' => "Interphone", "required" => false))
      ->add('has_digicode', 'checkbox', array(
        'label' => "Digicode",
        "required" => false,
        "mapped" => false))
      ->add('digicode', 'text', array('label' => "Digicode", "required" => false))
      ->add('acces_handicapes', 'checkbox', array('label' => "Accès handicapés", "required" => false))
      ->add('climatisation', 'checkbox', array('label' => "Climatisation", "required" => false))
      ->add('gardien', 'checkbox', array('label' => "Gardien", "required" => false))
      ->add('animal', 'checkbox', array('label' => "Animaux admis", "required" => false))
      ->add('fumeur', 'checkbox', array('label' => "Fumeur", "required" => false))
      ->add('internet', 'checkbox', array('label' => "Internet", "required" => false))
      ->add('type_chauffage', 'choice', array(
        'label' => "Type de chauffage",
        "required" => false,
        'choices' => array('INDIVIDUEL' => 'Individuel',
          'COLLECTIF' => 'Collectif')))
      ->add('mode_chauffage', 'choice', array(
        'label' => "Mode de chauffage",
        "required" => false,
        'choices' => array('AEROTHERMIE' => 'Aérothermie',
          'BOIS' => 'Bois',
          'CHAUDIERE A GRANULE' => 'Chaudière à granule',
          'CHEMINEE' => 'Cheminée',
          'CLIMATISATION REVERSIBLE' => 'Climatisation réversible',
          'ELECTRIQUE' => 'Electrique',
          'FUEL' => 'Fuel',
          'GAZ' => 'Gaz',
          'GEOTHERMIE' => 'Géothermie',
          'PANNEAU SOLAIRE' => 'Panneau solaire',
          'POMPE A CHALEUR' => 'Pompe à chaleur',
          'AUTRE' => 'Autre'),
        'preferred_choices' => array('ELECTRIQUE', 'GAZ', 'FUEL'),
      ))
      ->add('compteur_edf', 'text', array(
        'label' => 'Relevé du compteur EDF',
        'required' => false,
      ))
      ->add('compteur_gdf', 'text', array(
        'label' => 'Relevé du compteur GDF',
        'required' => false,
      ))
      ->add('emplacement_compteurs', 'text', array(
        'label' => 'Emplacement des compteurs',
        'required' => false,
      ))
      ->add('emplacement_compteur_eau', 'text', array(
        'label' => 'Emplacement du compteur d\'eau',
        'required' => false,
      ))
      ->add('emplacement_boite_lettres', 'text', array(
        'label' => 'Emplacement de la boite aux lettres',
        'required' => false,
      ))
      ->add('nom_boite_lettres', 'text', array(
        'label' => 'Nom sur la boite aux lettres',
        'required' => false,
      ))
      ->add('statut_diagnostic', 'choice', array(
        'label' => " ",
        'choices' => array(
          '' => 'Choisir...',
          '1' => 'Diagnostic fait',
          '2' => 'Diagnostic en cours',
          '3' => 'Je souhaite recevoir des conseils pour choisir un diagnostiqueur agréé'),
        'expanded' => false,
        'multiple' => false,
        'required' => true
      ))
      ->add('date_diagnostic', 'text', array(
        'label' => 'Fait le ',
        'required' => false,
      ))
      ->add('fichier_diagnostic', 'file', array(
        'label' => "Ajouter votre DPE (format JPG, PDF)",
        'required' => false,
        'data_class' => null,
        'attr' => array(
          "size" => "5",
        )
        )
      )
      ->add('consommation_chiffre', 'text', array('label' => " ", 'required' => false))
      ->add('gaz_serre_chiffre', 'text', array('label' => " ", 'required' => false))
      ->add('consommation_lettre', 'choice', array(
        'label' => "Consommation énergique",
        "required" => false,
        'choices' => array('A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E', 'F' => 'F', 'G' => 'G')))
      ->add('gaz_serre_lettre', 'choice', array(
        'label' => "Consommation énergique",
        'required' => false,
        'choices' => array('A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E', 'F' => 'F', 'G' => 'G')))
      ->add('visite_porte', 'text', array('label' => "Porte", 'required' => false))
      ->add('visite_description', 'textarea', array('label' => "Consignes/Itinéraires", 'required' => false))
      ->add('visite_cle', 'text', array('label' => "N° clé", 'required' => false))
      ->add('visite_cave', 'text', array('label' => "N° cave", 'required' => false))
      ->add('visite_parking', 'text', array('label' => "N° parking", 'required' => false))
      ->add('heure_arrivee', 'text', array('label' => "Heure d'arrivée", 'required' => false))
      ->add('heure_depart', 'text', array('label' => "Heure de départ", 'required' => false))
      ->add('etat_interieur', 'choice', array(
        'label' => "État intérieur",
        'choices' => array(
          'HABITABLE EN L\'ETAT' => 'Habitable en l\'état',
          'MOYEN' => 'Moyen',
          'BON' => 'Bon',
          'TRES BON' => 'Très bon',
          'EXCELLENT' => 'Excellent',
          'SOMPTUEUX' => 'Somptueux'),
        "required" => false))
      ->add('calme', 'choice', array(
        'label' => "Calme",
        'required' => false,
        'choices' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5')))
      ->add('clair', 'choice', array(
        'label' => "Clair",
        'required' => false,
        'choices' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5')))
      ->add('neuf_ancien', 'choice', array(
        'label' => "Neuf - Ancien",
        'required' => false,
        'choices' => array(
          'NEUF' => 'Neuf',
          'RECENT' => 'Récent',
          'ANCIEN' => 'Ancien')))
      ->add('standing_immeuble', 'choice', array(
        'label' => "Standing",
        'required' => false,
        'choices' => array(
          'MOYEN' => 'Moyen',
          'NORMAL' => 'Normal',
          'BON' => 'Bon',
          'GRAND' => 'Grand',
          'TRES GRAND' => 'Très grand')))
      ->add('statut', 'choice', array(
        'label' => "Statut",
        'choices' => array('0' => 'En cours de rédaction',
          '1' => 'En attente de validation initiale',
          '2' => 'En attente régisseur',
          '3' => 'En attente de validation finale',
          '4' => 'Validé et publié',
          '8' => 'Suspendu',
          '9' => 'Supprimé'),
        'required' => true))
      ->add('is_resa', 'choice', array(
        'label' => "Disponible pour la réservation en ligne",
        'choices' => array('0' => 'Non',
          '1' => 'Oui'),
        'required' => true))
      ->add('is_labeliser', 'choice', array(
        'label' => "Bien labélisé",
        'choices' => array('0' => 'Non',
          '1' => 'Oui'),
        'required' => true))
      ->add('is_reloger', 'choice', array(
        'label' => "Garantie satisfait ou reloger",
        'choices' => array('0' => 'Non',
          '1' => 'Oui'),
        'required' => true))
      ->add('is_selection', 'choice', array(
        'label' => "En avant sur la page d'accueil",
        'choices' => array('0' => 'Non',
          '1' => 'Oui'),
        'required' => true))
      ->add('commentaire', 'textarea', array('label' => "Observations", 'required' => false))
      ->add('confidentiel', 'textarea', array('label' => "Note confidentielle", 'required' => false))
      ->add('coordonnees_syndic', 'textarea', array('label' => "Coordonnées du syndic", 'required' => false))
    ;
  }

  public function getName()
  {
    return 'appartement';
  }

}
