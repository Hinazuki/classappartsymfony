<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Pat\CompteBundle\Repository\VilleRepository;

class AdminAppartementRechercheAvanceeForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('type', 'choice', array(
        'label' => "Type de bien",
        'choices' => array('' => 'Tous',
          'Appartement' => 'Appartement',
          'Maison' => 'Maison',
          'Studio' => 'Studio'/* ,
          'Appartement T1'=>'Appartement T1',
          'Appartement T2'=>'Appartement T2',
          'Appartement T3'=>'Appartement T3',
          'Appartement T4'=>'Appartement T4',
          'Appartement T5'=>'Appartement T5',
          'Appartement T6'=>'Appartement T6' */),
        'required' => false))
      ->add('nb_pieces', 'choice', array(
        'label' => "Nombre de pièces",
        'choices' => array('' => 'Tous',
          1 => 'T1',
          2 => 'T2',
          3 => 'T3',
          4 => 'T4',
          5 => 'T5',
          6 => 'T6'),
        'required' => false))

      /* ->add('ville', 'entity', array(
        'label' => "Ville",
        'expanded' => false,
        'multiple' => false,
        'required' => false,
        'class' => 'Pat\CompteBundle\Entity\Ville',
        'query_builder' => function(VilleRepository $vr) {
        return $vr->createQueryBuilder('v')
        ->orderBy('v.nom', 'ASC');
        }
        )) */
      ->add('ville', 'text', array('label' => "Ville", "required" => false, 'max_length' => 50))
      ->add('rue', 'text', array('label' => "Rue", "required" => false, 'max_length' => 50))
      ->add('reference', 'text', array('label' => "Référence", "required" => false, 'max_length' => 5))
      ->add('statut', 'choice', array(
        'label' => "Statut",
        'choices' => array('' => 'Tous',
          '0' => 'En cours de rédaction',
          '1' => 'En attente de validation initiale',
          '2' => 'En attente régisseur',
          '3' => 'En attente de validation finale',
          '4' => 'Validé et publié',
          '8' => 'Suspendu',
          '9' => 'Supprimé'),
        'required' => false));
  }

  public function getName()
  {
    return 'appartementrechercheavancee';
  }

}
