<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class EquipementRechercheForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('type', 'choice', array(
        'label' => "Type de pièce",
        'choices' => array('' => 'Choix...',
          'Salon' => 'Salon',
          'Salle à manger' => 'Salle à manger',
          'Cuisine' => 'Cuisine',
          'Chambre' => 'Chambre',
          'Salle de bain' => 'Salle de bain',
          'Toilettes' => 'Toilettes',
          'Bureau' => 'Bureau',
          'Mezzanine' => 'Mezzanine')))
    ;
  }

  public function getName()
  {
    return 'EquipementRecherche';
  }

}
