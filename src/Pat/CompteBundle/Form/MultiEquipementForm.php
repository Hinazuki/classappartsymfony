<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MultiEquipementForm extends AbstractType
{

  /**
   * @param FormBuilderInterface $builder
   * @param array                $options
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('Equipement', 'entity', [
        'label' => "Appliquer au logement",
        'expanded' => false,
        'multiple' => false,
        'required' => false,
        'class' => 'Pat\CompteBundle\Entity\Equipement',
        'property' => 'intitule',
        'attr' => [
          'class' => 'hidden',
        ],
      ])
      ->add('quantity', 'number', [
        'label' => 'Quantité',
      ])
    ;
  }

  /**
   * @return string
   */
  public function getName()
  {
    return 'MultiEquipement';
  }

}
