<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class EquipementCategorieForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('intitule_categorie', 'text', array('label' => "Intitulé", 'required' => true))
    ;
  }

  public function getName()
  {
    return 'EquipementCategorie';
  }

}
