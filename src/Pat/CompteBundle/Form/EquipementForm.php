<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Pat\CompteBundle\Form\EquipementCategorieForm;
use Pat\CompteBundle\Entity\EquipementCategorie;

class EquipementForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('equipementcategorie', 'entity', array(
        'label' => "Catégorie",
        'expanded' => false,
        'multiple' => false,
        'required' => false, 'render_optional_text' => false,
        'class' => 'Pat\CompteBundle\Entity\EquipementCategorie',
        'property' => 'intituleCategorie'));

    $builder
      ->add('intitule', 'text', array('label' => "Intitulé", 'required' => true))
    ;
  }

  public function getName()
  {
    return 'Equipement';
  }

}
