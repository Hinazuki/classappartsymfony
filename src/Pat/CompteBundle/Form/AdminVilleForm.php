<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminVilleForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('nom', 'text', array('label' => "Nom", "required" => true))
      ->add('code_postal', 'text', array('label' => "Code Postal", "required" => true))
      ->add('latitude', 'text', array('label' => "Latitude", "required" => false))
      ->add('longitude', 'text', array('label' => "Longitude", "required" => false))
      ->add('nombre_habitants', 'integer', array('label' => "Nombre d'habitants", "required" => false))
      ->add('description', 'textarea', array('label' => "Description", "required" => false))
      ->add('quartier', 'choice', array(
        'choices' => array(true => 'Oui', false => 'Non'),
        'expanded' => true,
        'multiple' => false,
        'label' => "Quartier",
        'required' => false))
    ;
  }

  public function getName()
  {
    return 'ville';
  }

}
