<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OptionType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $tab = array();
    for ($i = 1; $i <= 10; $i++)
      $tab[$i] = $i;

    $builder
      ->add('quantity', 'choice', array(
        'label' => "Quantité",
        'choices' => $tab
      ))
      ->add('amount', 'number', array(
        'label' => "Montant",
        'required' => false
      ))
      ->add('name', 'text', array(
        'label' => "Intitulé"
      ))
    ;
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'Pat\CompteBundle\Entity\Option'
    ));
  }

  public function getName()
  {
    return 'pat_comptebundle_optiontype';
  }

}
