<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminLocataireRechercheForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('nom', 'text', array('label' => "Nom", "required" => false))
      ->add('prenom', 'text', array('label' => "Prénom", "required" => false))
      ->add('num_compte', 'text', array('label' => "Identifiant", "required" => false))
    ;
  }

  public function getName()
  {
    return 'locatairerecherche';
  }

}
