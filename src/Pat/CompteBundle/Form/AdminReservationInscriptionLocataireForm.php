<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminReservationInscriptionLocataireForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    // Add custom fields
    $builder
      ->add('civilite', 'choice', array(
        'label' => "Civilité",
        'choices' => array(
          'M.' => 'M.',
          'Mlle' => 'Mlle',
          'Mme' => 'Mme',
        ),
        'multiple' => false,
      ))
      ->add('prenom', 'text', array('label' => "Prénom", 'required' => false))
      ->add('nom', 'text', array('label' => "Nom", 'required' => true))
      ->add('societe', 'text', array('label' => "Société", 'required' => false))
      ->add('date_naissance', 'birthday', array(
        'widget' => 'choice',
        'years' => range(date('Y'), 1900),
        'format' => 'dd / MM / yyyy',
        'required' => false))
      ->add('nationalite', 'text', array('label' => "Nationalité", 'required' => false))
      ->add('langue_parle', 'choice', array(
        'label' => "Langue préférée",
        'choices' => array(
          'Français' => 'Français',
          'Anglais' => 'Anglais',
          'Allemand' => 'Allemand',
          'Espagnol' => 'Espagnol',
          'Italien' => 'Italien',
          'Russe' => 'Russe',
          'Chinois' => 'Chinois',
        ),
        'multiple' => false,
      ))
      ->add('adresse', 'text', array('label' => "Adresse", 'required' => true))
      ->add('adresse2', 'text', array('label' => "Adresse suite", 'required' => false))
      ->add('code_postal', 'text', array('label' => "Code postal", 'required' => true))
      ->add('ville', 'text', array('label' => "Ville", 'required' => true))
      ->add('pays', 'text', array('label' => "Pays", 'required' => true))
      ->add('telephone', 'text', array('label' => "Téléphone", 'required' => false))
      ->add('mobile', 'text', array('label' => "Mobile Mr", 'required' => false))
      ->add('mobile2', 'text', array('label' => "Mobile Mme", 'required' => false))
      //->add('username', 'text', array('label' => "Identifiant", 'required' => true ))
      ->add('email', 'text', array('label' => "E-mail", 'required' => true))
      ->add('adresse_fact', 'text', array('label' => "Adresse", 'required' => true))
      ->add('adresse2_fact', 'text', array('label' => "Adresse suite", 'required' => false))
      ->add('code_postal_fact', 'text', array('label' => "Code Postal", 'required' => true))
      ->add('ville_fact', 'text', array('label' => "Ville", 'required' => true))
      ->add('pays_fact', 'text', array('label' => "Pays", 'required' => true))

    ;
  }

  public function getName()
  {
    return 'adminreservationinscriptionlocataire';
  }

}
