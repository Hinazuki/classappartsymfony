<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TextOptionsForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('contenu', 'textarea', array(
        'label' => 'Contenu',
        'attr' => array(
          'class' => '',
          'style' => 'width:98%;'
        )
      ))
    ;
  }

  public function getName()
  {
    return 'TextOptions';
  }

}
