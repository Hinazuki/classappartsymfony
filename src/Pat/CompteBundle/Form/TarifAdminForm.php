<?php

namespace Pat\CompteBundle\Form;

use Pat\CompteBundle\Entity\Tarif;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class TarifAdminForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('loyer_nuit', 'number', [
        'label' => "Nuit",
        'required' => true
      ])
      ->add('loyer_semaine1', 'number', [
        'label' => "Semaine (7 à 21 J)",
        'required' => true
      ])
      ->add('loyer_mois', 'number', [
        'label' => "Mois (> 21 J)",
        'required' => true
      ])
      ->add('loyer_nuit_2', 'number', [
        'label' => "Nuit",
        'required' => true,
        'read_only' => true,
        'attr' => [
          'class' => 'js-loyer-nuit-auto',
        ],
      ])
      ->add('loyer_semaine1_2', 'number', [
        'label' => "Semaine (7 à 21 J)",
        'required' => true,
        'attr' => [
          'class' => 'js-loyer-semaine-auto',
        ],
      ])
      ->add('loyer_mois_2', 'number', [
        'label' => "Mois (> 21 J)",
        'required' => true,
        'read_only' => true,
        'attr' => [
          'class' => 'js-loyer-mois-auto',
        ],
      ])
      ->add('loyer_actif', 'choice', [
        'choices' => [
          Tarif::LOYER_LIBRE => 'Loyer libre',
          Tarif::LOYER_AUTO => 'Loyer automatique',
        ],
        'constraints' => [
          new Assert\NotBlank(['message' => 'Veuillez sélectioner un type de loyer']),
        ],
        'multiple' => false,
        'expanded' => true,
      ])
      ->add('type_contrat', 'choice', [
        'label' => "Type de contrat",
        'required' => true,
        'choices' => [
          'STANDARD' => 'Standard',
          'PREMIUM' => 'Premium'
        ],
      ])
      ->add('acompte', 'integer', [
        'label' => "Acompte",
        'required' => true
      ])
      ->add('depot', 'integer', [
        'label' => "Dépôt de garantie",
        'required' => true
      ])
      ->add('depot_min', 'number', [
        'label' => "Dépôt minimum",
        'required' => true
      ])
    ;
  }

  public function getName()
  {
    return 'TarifAdmin';
  }

}
