<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminReservationRechercheForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('reference', 'text', array('label' => "N° résa", "required" => false, 'max_length' => 20))
      ->add('reference_appart', 'text', array('label' => "N° Bien", "required" => false, 'max_length' => 10))
      ->add('date_debut', 'text', array('label' => "Début ", "required" => false, 'max_length' => 10))
      ->add('date_fin', 'text', array('label' => "Fin ", "required" => false, 'max_length' => 10))
    ;
  }

  public function getName()
  {
    return 'ReservationRecherche';
  }

}
