<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminSiteTiersForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('name', 'text', [
        'label' => 'Nom du site',
      ])
      ->add('description', 'textarea', [
        'label' => 'Description',
        'required' => false,
      ])
      ->add('url', 'text', [
        'label' => 'Url',
        'required' => false,
      ])
      ->add('autoPayment', 'checkbox', [
        'label' => 'Valider automatiquement le paiement',
        'required' => false,
      ])
    ;
  }

  /**
   * Returns the name of this type.
   *
   * @return string The name of this type
   */
  public function getName()
  {
    return 'site_tiers';
  }

}
