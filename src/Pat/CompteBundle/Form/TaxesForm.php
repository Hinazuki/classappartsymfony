<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TaxesForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('commission', 'collection', array(
        'type' => 'number',
        'options' => array(
          'required' => true,
          'attr' => array("style" => "width: 100px;")
        ),
      ))
      ->add('menage', 'collection', array(
        'type' => 'collection',
        'options' => array(
          'type' => 'collection',
          'options' => array(
            'type' => 'number',
            'required' => true,
          ),
        ),
      ))
      ->add('EDF', 'collection', array(
        'type' => 'number',
        'options' => array(
          'required' => true,
          'attr' => array("style" => "width: 100px;")
        ),
      ))
      ->add('tva', 'number', array(
        'attr' => array(
          'required' => true,
          "style" => "width: 100px;"
        ),
      ))
      ->add('assurance', 'number', array(
        'attr' => array(
          'required' => true,
          "style" => "width: 100px;"
        ),
      ))
      ->add('sejour', 'number', array(
        'attr' => array(
          'required' => true,
          "style" => "width: 100px;"
        ),
      ))
    ;
  }

  public function getName()
  {
    return 'Taxes';
  }

}
