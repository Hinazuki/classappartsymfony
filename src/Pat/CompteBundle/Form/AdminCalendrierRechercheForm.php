<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Pat\CompteBundle\Repository\AppartementRepository;

class AdminCalendrierRechercheForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('mois', 'choice', array(
        'label' => "Mois",
        'choices' => array('01' => 'Janvier',
          '02' => 'Février',
          '03' => 'Mars',
          '04' => 'Avril',
          '05' => 'Mai',
          '06' => 'Juin',
          '07' => 'Juillet',
          '08' => 'Août',
          '09' => 'Septembre',
          '10' => 'Octobre',
          '11' => 'Novembre',
          '12' => 'Décembre'
      )))
      ->add('annee', 'choice', array(
        'label' => "Année",
        'choices' => array(date("Y") - 3 => date("Y") - 3,
          date("Y") - 2 => date("Y") - 2,
          date("Y") - 1 => date("Y") - 1,
          date("Y") => date("Y"),
          date("Y") + 1 => date("Y") + 1,
          date("Y") + 2 => date("Y") + 2,
          date("Y") + 3 => date("Y") + 3,
          date("Y") + 4 => date("Y") + 4,
      )))
    ;
  }

  public function getName()
  {
    return 'calendrierrecherche';
  }

}
