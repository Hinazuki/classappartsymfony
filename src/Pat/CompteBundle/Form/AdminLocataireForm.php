<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminLocataireForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    // Add custom fields
    $builder
      ->add('civilite', 'choice', array(
        'label' => "Civilité",
        'choices' => array(
          'M.' => 'M.',
          'Mlle' => 'Mlle',
          'Mme' => 'Mme',
        ),
        'multiple' => false,
      ))
      ->add('prenom', 'text', array('label' => "Prénom", 'required' => false, 'render_optional_text' => false))
      ->add('nom', 'text', array('label' => "Nom", 'required' => true))
      ->add('societe', 'text', array('label' => "Société", 'required' => false, 'render_optional_text' => false))
      ->add('siret', 'text', array('label' => "Siret", 'required' => false, 'render_optional_text' => false))
      ->add('fonction', 'text', array('label' => "Fonction", 'required' => false, 'render_optional_text' => false))
      ->add('date_naissance', 'birthday', array(
        'widget' => 'choice',
        'years' => range(date('Y') - 18, 1900),
        'format' => 'dd / MM / yyyy',
        'empty_value' => '',
        'required' => false, 'render_optional_text' => false))
      ->add('nationalite', 'text', array('label' => "Nationalité", 'required' => false, 'render_optional_text' => false))
      ->add('langue_parle', 'choice', array(
        'label' => "Langue préférée",
        'choices' => array(
          'Français' => 'Français',
          'Anglais' => 'Anglais',
          'Allemand' => 'Allemand',
          'Espagnol' => 'Espagnol',
          'Italien' => 'Italien',
          'Russe' => 'Russe',
          'Chinois' => 'Chinois',
        ),
        'multiple' => false,
      ))
      ->add('adresse', 'text', array('label' => "Adresse", 'required' => true))
      ->add('adresse2', 'text', array('label' => "Adresse suite", 'required' => false, 'render_optional_text' => false))
      ->add('code_postal', 'text', array('label' => "Code postal", 'required' => true))
      ->add('ville', 'text', array('label' => "Ville", 'required' => true))
      ->add('pays', 'text', array('label' => "Pays", 'required' => true))
      ->add('adresse_fact', 'text', array(
        'label' => 'Adresse', 'required' => false
      ))
      ->add('adresse2_fact', 'text', array(
        'label' => 'Adresse suite', 'required' => false, 'render_optional_text' => false
      ))
      ->add('code_postal_fact', 'text', array(
        'label' => 'Code postal', 'required' => false
      ))
      ->add('ville_fact', 'text', array(
        'label' => 'Ville', 'required' => false
      ))
      ->add('pays_fact', 'text', array(
        'label' => 'Pays', 'required' => false
      ))
      ->add('telephone', 'text', array('label' => "Téléphone", 'required' => false, 'render_optional_text' => false))
      ->add('mobile', 'text', array('label' => "Mobile Mr", 'required' => false, 'render_optional_text' => false))
      ->add('mobile2', 'text', array('label' => "Mobile Mme", 'required' => false, 'render_optional_text' => false))
//            ->add('mails_second', 'textarea', array(
//                    'label' => "Adresses e-mail secondaires",
//                    'required' => false
//                ))
      ->add('second_email', 'text', array(
        'label' => "Adresse e-mail secondaire",
        'required' => false
      ))
      //->add('username', 'text', array('label' => "Identifiant", 'required' => true ))
      ->add('email', 'text', array('label' => "E-mail", 'required' => true))
      ->add('rib_etab', 'text', array("required" => false, 'max_length' => 5, 'label' => "Code Banque"))
      ->add('rib_code', 'text', array("required" => false, 'max_length' => 5, 'label' => "Code Guichet"))
      ->add('rib_compte', 'text', array("required" => false, 'max_length' => 11, 'label' => "Num compte"))
      ->add('rib_rice', 'text', array("required" => false, 'max_length' => 2, 'label' => "Clé RIB"))
      ->add('rib_domiciliation', 'text', array("required" => false, 'max_length' => 100, 'label' => "Domiciliation"))
      ->add('rib_iban', 'text', array("required" => false, 'max_length' => 27, 'label' => "Code IBAN"))
      ->add('rib_bic', 'text', array("required" => false, 'label' => "Code SWIFT (BIC)"))
      ->add('rib_agence_resp', 'text', array("required" => false, 'max_length' => 255, 'label' => "Agence responsable du compte"))
      ->add('rib_pays', 'text', array("required" => false, 'max_length' => 100, 'label' => "Pays"))
      ->add('rib_titulaire', 'text', array("required" => false, 'max_length' => 100, 'label' => "Titulaire"))
    ;
  }

  public function getName()
  {
    return 'proprietaire';
  }

}
