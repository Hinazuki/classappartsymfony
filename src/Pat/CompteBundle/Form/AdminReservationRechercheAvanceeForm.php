<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminReservationRechercheAvanceeForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('reference_resa', 'text', array('label' => "N° réservation", "required" => false, 'max_length' => 20))
      ->add('reference_appart', 'text', array('label' => "N° appartement", "required" => false, 'max_length' => 5))
      ->add('reference_locataire', 'text', array('label' => "N° locataire", "required" => false, 'max_length' => 10))
      ->add('nom_locataire', 'text', array('label' => "Nom locataire", "required" => false, 'max_length' => 50))
      ->add('prenom_locataire', 'text', array('label' => "Prénom locataire", "required" => false, 'max_length' => 50))
      ->add('date_debut', 'text', array('label' => "Date de début", "required" => false, 'max_length' => 10))
      ->add('date_fin', 'text', array('label' => "Date de fin", "required" => false, 'max_length' => 10))
      ->add('only_between', 'checkbox', array('label' => "Uniquement se commencant ou se terminant entre date de debut et date de fin", "required" => false))
    ;
  }

  public function getName()
  {
    return 'ReservationRechercheAvancee';
  }

}
