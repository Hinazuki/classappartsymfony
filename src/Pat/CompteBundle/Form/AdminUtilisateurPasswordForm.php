<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminUtilisateurPasswordForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder->add('password_new', 'text', array('label' => "Nouveau mot de passe", 'required' => true));
    $builder->add('password_new_again', 'text', array('label' => "Confirmer", 'required' => true));
  }

  public function getName()
  {
    return 'password';
  }

}
