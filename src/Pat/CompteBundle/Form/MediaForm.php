<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MediaForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      //->add('titre', 'text', array('label' => "Photo", "required" => false))
      ->add('fichier', 'file', array(
        "label" => "Photo : ",
        "required" => true,
        "attr" => array(
          "accept" => "image/*",
          "multiple" => "multiple",
          "size" => "15",
        )
      ))
    ;
  }

  public function getName()
  {
    return 'Media';
  }

}
