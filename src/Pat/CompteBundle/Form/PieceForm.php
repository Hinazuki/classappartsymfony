<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Pat\CompteBundle\Repository\EquipementRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PieceForm extends AbstractType
{

  /**
   * @var Equipement[]
   */
  private $multiEquipements;

  /**
   * @param OptionsResolverInterface $resolver
   */
  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults([
      'data_class' => 'Pat\CompteBundle\Entity\Piece',
    ]);

    $resolver->setRequired([
      'multiEquipements'
    ]);
  }

  /**
   * @param FormBuilderInterface $builder
   * @param array                $options
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $this->multiEquipements = $options['multiEquipements'];

    $builder
      ->add('typepiece', 'entity', [
        'label' => "Type de pièce",
        'expanded' => false,
        'multiple' => false,
        'required' => true,
        'class' => 'Pat\CompteBundle\Entity\TypePiece',
        'property' => 'type'
      ])
      ->add('Equipement', 'entity', [
        'label' => "Appliquer au logement",
        'expanded' => true,
        'multiple' => true,
        'required' => false,
        'class' => 'Pat\CompteBundle\Entity\Equipement',
        'property' => 'intitule',
        'query_builder' => function(EquipementRepository $er) {
          return $er->findEquipementOrderedByCat();
        }
      ])
    ;

    $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'onPreSetData']);
  }

  /**
   * @param FormEvent $event
   */
  public function onPreSetData(FormEvent $event)
  {
    $form = $event->getForm();
    $data = $event->getData();

    $this->addMultiEquipements($form, $data->getMultiEquipements());
  }

  /**
   * @param $form
   * @param $multiEquipements
   */
  public function addMultiEquipements($form, $multiEquipements)
  {
    $allMultiEquipements = [];

    foreach ($this->multiEquipements as $multiEquipement) {
      $allMultiEquipements[$multiEquipement->getId()] = [
        'Equipement' => $multiEquipement,
        'quantity' => 0,
      ];
    }

    foreach ($multiEquipements as $multiEquipement) {
      if ($multiEquipement->getEquipement()->getId())
        if (array_key_exists($multiEquipement->getEquipement()->getId(), $allMultiEquipements)) {
          $allMultiEquipements[$multiEquipement->getEquipement()->getId()]['quantity'] += $multiEquipement->getQuantity();
        }
    }

    $form->add('multiEquipements', 'collection', [
      'type' => new MultiEquipementForm(),
      'mapped' => false,
      'data' => $allMultiEquipements,
      'required' => false,
    ]);
  }

  /**
   * @return string
   */
  public function getName()
  {
    return 'Piece';
  }

}
