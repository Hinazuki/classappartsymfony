<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminUtilisateurForm extends AbstractType
{

  private $action; //id de l'utilisateur

  public function __construct($action = null)
  {
    $this->action = $action;
  }

  public function getDefaultOptions(array $options)
  {
    return array(
      'action' => '0'
    );
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    //on récupère l'action (0 pour ajouter et 1 pour modifier)
    $action = $this->action;

    // Add custom fields
    $builder
      ->add('civilite', 'choice', array(
        'label' => 'Civilité',
        'choices' => array(
          'M.' => 'M.',
          'Mlle' => 'Mlle',
          'Mme' => 'Mme',
        ),
        'multiple' => false,
      ))
      ->add('prenom', 'text', array('label' => 'Prénom', 'required' => true))
      ->add('nom', 'text', array('label' => 'Nom', 'required' => true))
      ->add('fonction', 'text', array('label' => 'Fonction', 'required' => false))
      ->add('telephone', 'text', array('label' => 'Téléphone', 'required' => false))
      ->add('email', 'text', array('label' => 'E-mail', 'required' => true))
//      ->add('date_naissance', 'birthday', array(
//        'widget' => 'choice',
//        'format' => 'dd / MM / yyyy',
//        'empty_value' => '',
//        'required' => false
//      ))
      ->add('role', 'choice', array(
        'label' => 'Rôle',
        'choices' => array(
          'ROLE_TRAINEE' => 'Stagiaire',
          'ROLE_ADMIN' => 'Administrateur'
        ),
        'multiple' => false,
        'required' => true,
        'mapped' => false,
      ));

    if ($action == '0') {
      $builder
        ->add('username', 'text', array('label' => 'Identifiant', 'required' => true))
        ->add('password', 'password', array('label' => 'Mot de passe', 'required' => true));
    }
  }

  public function getName()
  {
    return 'utilisateur';
  }

}
