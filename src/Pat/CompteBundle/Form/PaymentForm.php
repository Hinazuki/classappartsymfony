<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Pat\CompteBundle\Form\DataTransformer\DateTimeTransformer;

class PaymentForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $transformer = new DateTimeTransformer();

    $builder
      ->add('amount', 'number', array(
        'label' => 'Montant',
        'required' => true
      ))
      ->add('name', 'text', array(
        'label' => 'Sujet',
        'required' => true
      ))
      ->add('mode_paiement', 'choice', array(
        'choices' => array(
          'virement' => 'Virement',
          'cheque' => 'Chèque',
          'espece' => 'Espèce',
          'CB' => 'CB',
          'SWIKLY' => 'SWIKLY',
          'gateway' => 'Sites passerelles'
        ),
        'label' => 'Mode de paiement',
        'required' => false
      ))
      ->add('msgContent', 'textarea', array(
        'label' => 'Contenu du message',
        'required' => false
      ))
      ->add($builder->create('payed_at', 'text', array(
          'label' => 'Date de paiement',
          'required' => false
        ))
        ->addModelTransformer($transformer))
    ;
  }

  public function getName()
  {
    return 'payment';
  }

}
