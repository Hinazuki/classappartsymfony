<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Pat\CompteBundle\Repository\AppartementRepository;

class DocumentsType extends AbstractType
{

  private $userid;

  public function __construct($userid = null)
  {
    $this->userid = $userid;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $userid = $this->userid;

    $builder
      ->add('documentName', 'text', array(
        'required' => true,
        'label' => 'Nom du document :')
      )
      ->add('file', 'file', array(
        'required' => false,
        'label' => 'Document :',
        'attr' => array('title' => "Choisir un fichier",
          'class' => 'btn btn-primary'
        ))
      )
      ->add("appartement", "entity", array(
        'required' => false,
        'empty_data' => null,
        'empty_value' => 'Aucun',
        "label" => "Associé au bien :",
        "class" => "PatCompteBundle:Appartement",
        "query_builder" =>
        function(AppartementRepository $cr) use($userid) {
          return $cr->findAppartByUtilisateur($userid);
        }))
      ->add('visible', 'checkbox', array('required' => false, 'label' => ""))
    ;
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'Pat\CompteBundle\Entity\Documents'
    ));
  }

  public function getName()
  {
    return 'pat_comptebundle_documentstype';
  }

}
