<?php

namespace Pat\CompteBundle\Form;

use Doctrine\ORM\EntityManager;
use Pat\CompteBundle\Entity\Promotion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AdminPromotionForm extends AbstractType
{

  public function __construct(EntityManager $em)
  {
    $this->em = $em;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('type', 'choice', [
        'label' => 'Type de promotion',
        'choices' => [
          Promotion::TYPE_ACTIVABLE => 'Promotion par date de réservation',
          Promotion::TYPE_PERIODE => 'Promotion par période',
        ],
        'expanded' => true,
        'attr' => [
          'class' => 'js-promotion-type-select',
        ]
      ])
      ->add('percentage', 'number', [
        'label' => 'Pourcentage',
        'label_attr' => [
          'style' => 'display: inline-block',
        ],
        'required' => false
      ])
      ->add('amount', 'number', [
        'label' => 'Montant net pour 6 nuits',
        'label_attr' => [
          'style' => 'display: inline-block',
        ],
        'required' => false,
        'mapped' => false
      ])
      ->add('nbDaysBefore', 'number', [
        'label' => 'Date de réservation (J-)',
        'required' => false,
        'attr' => [
          'class' => 'js-promotion-type-activable',
        ]
      ])
      ->add('activated', 'checkbox', [
        'label' => 'Actif',
        'value' => true,
        'required' => false,
        'attr' => [
          'class' => 'js-promotion-type-activable',
        ]
      ])
      ->add('begin', 'text', [
        'label' => 'Date de début',
        'required' => false,
        'attr' => [
          'class' => 'js-promotion-type-periode cal_date_in_admin',
        ]
      ])
      ->add('end', 'text', [
        'label' => 'Date de fin',
        'required' => false,
        'attr' => [
          'class' => 'js-promotion-type-periode cal_date_out_admin',
        ]
      ])
    ;

    $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'onPreSubmit']);
  }

  public function onPreSubmit(FormEvent $event)
  {
    $form = $event->getForm();
    $data = $event->getData();
    $appartement = $form->getData()->getAppartement();

    if (null !== $appartement &&
      null !== $appartement->getActivePromotion() &&
      isset($data['activated']) &&
      true == $data['activated']
    ) {
      $form->addError(new FormError('Une Promotion / Majoration est déjà active pour cet appartement'));
    }

    if (true === isset($data['type']) && $data['type'] == Promotion::TYPE_PERIODE) {
      $begin = new \DateTime($this->toDateEn($data['begin']));
      $end = new \DateTime($this->toDateEn($data['end']));

      $periodePromotions = $this->em->getRepository('PatCompteBundle:Promotion')->findActivePeriodePromotion($appartement->getId(), $begin, $end);

      if (count($periodePromotions) > 0) {
        $form->addError(new FormError('Une Promotion / Majoration est déjà active pour cette période'));
      }
    }
  }

  public function toDateEn($date)
  {
    return substr($date, 6, 4)."-".substr($date, 3, 2)."-".substr($date, 0, 2);
  }

  public function getName()
  {
    return 'admin_promotion';
  }

}
