<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Pat\CompteBundle\Repository\AppartementRepository;

class CalendrierRechercheForm extends AbstractType
{

  private $user_id; //id de l'utilisateur

  public function __construct($user_id = null)
  {
    $this->user_id = $user_id;
  }

  public function getDefaultOptions(array $options)
  {
    return array(
      'user_id' => "0"
    );
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    //on récupère l'id du user pour n'afficher que les appartements qui lui appartiennent dans la liste déroulante
    $id_user = $this->user_id;

    $builder
      ->add('mois', 'choice', array(
        'label' => "Mois",
        'choices' => array('01' => 'Janvier',
          '02' => 'Février',
          '03' => 'Mars',
          '04' => 'Avril',
          '05' => 'Mai',
          '06' => 'Juin',
          '07' => 'Juillet',
          '08' => 'Août',
          '09' => 'Septembre',
          '10' => 'Octobre',
          '11' => 'Novembre',
          '12' => 'Décembre'
      )))
      ->add('annee', 'choice', array(
        'label' => "Année",
        'choices' => array(date("Y") - 3 => date("Y") - 3,
          date("Y") - 2 => date("Y") - 2,
          date("Y") - 1 => date("Y") - 1,
          date("Y") => date("Y"),
          date("Y") + 1 => date("Y") + 1,
          date("Y") + 2 => date("Y") + 2,
          date("Y") + 3 => date("Y") + 3,
          date("Y") + 4 => date("Y") + 4,
      )))
      ->add('Appartement', 'entity', array(
        'label' => "Appliquer au logement",
        'expanded' => false,
        'multiple' => false,
        'required' => true,
        'class' => 'Pat\CompteBundle\Entity\Appartement',
        'query_builder' => function(AppartementRepository $ar) use($id_user) {
          return $ar->findAppartByUtilisateur($id_user);
        }
      ));
  }

  public function getName()
  {
    return 'calendrierrecherche';
  }

}
