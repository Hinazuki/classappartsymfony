<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TarifSimpleForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('loyer_nuit', 'number', array('label' => "Nuit", 'required' => true))
      ->add('loyer_semaine1', 'number', array('label' => "Semaine (7 à 21 J)", 'required' => true))
      ->add('loyer_mois', 'number', array('label' => "Mois (> 21 J)", 'required' => true))
      ->add('loyer_nuit_2', 'number', array('label' => "Nuit", 'required' => true))
      ->add('loyer_semaine1_2', 'number', array('label' => "Semaine (7 à 21 J)", 'required' => true))
      ->add('loyer_mois_2', 'number', array('label' => "Mois (> 21 J)", 'required' => true))
    ;
  }

  public function getName()
  {
    return 'Tarif';
  }

}
