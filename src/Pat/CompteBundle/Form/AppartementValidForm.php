<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AppartementValidForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('choice1', 'choice', array(
        'label' => " ",
        'choices' => array('0' => " "),
        'expanded' => true,
        'multiple' => true,
        'required' => true
      ))
      ->add('choice2', 'choice', array(
        'label' => " ",
        'choices' => array('0' => " "),
        'expanded' => true,
        'multiple' => true,
        'required' => true
      ))
    ;
  }

  public function getName()
  {
    return 'appartementvalid';
  }

}
