<?php

namespace Pat\CompteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Pat\CompteBundle\Form\DataTransformer\DateTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DatefrType extends AbstractType
{

  /**
   * @var ObjectManager
   */
  private $om;

  /**
   * @param ObjectManager $om
   */
  public function __construct(ObjectManager $om)
  {
    $this->om = $om;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $transformer = new DateTransformer($this->om);
    $builder->addModelTransformer($transformer);
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {

  }

  public function getParent()
  {
    return 'text';
  }

  public function getName()
  {
    return 'datefr';
  }

}
