<?php

namespace Pat\CompteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Pat\CompteBundle\Form\DataTransformer\VilleToNameTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VilleSelectorType extends AbstractType
{

  /**
   * @var ObjectManager
   */
  private $om;

  /**
   * @param ObjectManager $om
   */
  public function __construct(ObjectManager $om)
  {
    $this->om = $om;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $transformer = new VilleToNameTransformer($this->om);
    $builder->addModelTransformer($transformer);
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
      'invalid_message' => 'La ville n\'a pas été trouvé',
    ));
  }

  public function getParent()
  {
    return 'text';
  }

  public function getName()
  {
    return 'ville_selector';
  }

}
