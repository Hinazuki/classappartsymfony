<?php

namespace Pat\CompteBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

class DateTimeTransformer implements DataTransformerInterface
{

  /**
   * Transforms a DateTime Object to string.
   *
   * @param  DateTime $datetime
   * @return string
   */
  public function transform($datetime)
  {
    return is_null($datetime) ? null : $datetime->format('d/m/Y');
  }

  /**
   * Transforms a string date to a DateTime.
   *
   * @param  string $date
   * @return DateTime
   */
  public function reverseTransform($date)
  {
    if (preg_match("/^([0-2]?[0-9]|3[01])\/(0?[0-9]|1[0-2])\/([0-9]{4})$/", $date, $matches))
      $datetime = new \DateTime($matches[3]."-".$matches[2]."-".$matches[1]);
    else
      $datetime = null;

    return $datetime;
  }

}
