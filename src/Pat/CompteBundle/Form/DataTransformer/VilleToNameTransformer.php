<?php

namespace Pat\CompteBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Pat\CompteBundle\Entity\Ville;

class VilleToNameTransformer implements DataTransformerInterface
{

  /**
   * @var ObjectManager
   */
  private $om;

  /**
   * @param ObjectManager $om
   */
  public function __construct(ObjectManager $om)
  {
    $this->om = $om;
  }

  /**
   * Transforms an object (Ville) to a string (name).
   *
   * @param  Ville|null $ville
   * @return string
   */
  public function transform($ville)
  {
    if (null === $ville) {
      return "";
    }

    return $ville->getNom().' ('.$ville->getCodePostal().')';
  }

  /**
   * Transforms a string (nom) to an object (ville).
   *
   * @param  string $name
   * @return Ville|null
   * @throws TransformationFailedException if object (ville) is not found.
   */
  public function reverseTransform($name)
  {
    if (!$name) {
      return null;
    }

    $cp = '';
    if (preg_match("/(.*) \(?([0-9]{5})?\)?$/", $name, $matches)) {
      $nom = $matches[1];
      $cp = $matches[2];

      $ville = $this->om
        ->getRepository('PatCompteBundle:Ville')
        ->findOneBy(array('nom' => $nom, 'code_postal' => $cp))
      ;
    }
    // Si l'expression n'est pas matchée c'est que le code postal n'a pas été renseigné
    else {
      $ville = $this->om
        ->getRepository('PatCompteBundle:Ville')
        ->findBy(array('nom' => $name))
      ;
      if (sizeof($ville) == 1)
        $ville = $ville[0];
      elseif (sizeof($ville) == 0)
        $ville = null;
      else
        throw new TransformationFailedException(sprintf(
          'Plusieurs villes possédant ce nom ont été trouvées'));
    }

    if (null === $ville) {
      throw new TransformationFailedException(sprintf(
        'La ville "%s" n\'existe pas', $name
      ));
    }

    return $ville;
  }

}
