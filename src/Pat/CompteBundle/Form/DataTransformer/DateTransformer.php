<?php

namespace Pat\CompteBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

class DateTransformer implements DataTransformerInterface
{

  /**
   * @var ObjectManager
   */
  private $om;

  /**
   * @param ObjectManager $om
   */
  public function __construct(ObjectManager $om)
  {
    $this->om = $om;
  }

  /**
   * Transforms an english date to a french .
   *
   * @param  string $date_en
   * @return string
   */
  public function transform($date_en)
  {
    if (preg_match("/^([0-9]{4})\-(0?[0-9]|1[0-2])\-([0-2]?[0-9]|3[01])$/", $date_en, $matches))
      return $matches[3]."/".$matches[2]."/".$matches[1];
    else
      return $date_en;
  }

  /**
   * Transforms a french date to an english date.
   *
   * @param  string $date_fr
   * @return string
   */
  public function reverseTransform($date_fr)
  {
    if (preg_match("/^([0-2]?[0-9]|3[01])\/(0?[0-9]|1[0-2])\/([0-9]{4})$/", $date_fr, $matches))
      return $matches[3]."-".$matches[2]."-".$matches[1];
    else
      return $date_fr;
  }

}
