<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminReservationRechercheAppartementForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      //LOCALISATION
      ->add('ville', 'text', array('label' => "Localisation", "required" => true, 'max_length' => 50))
      ->add('rayon', 'choice', array(
        'label' => "Rayon",
        'choices' => array('' => 'Rayon 0km',
          '5' => 'Rayon 5km',
          '10' => 'Rayon 10km',
          '15' => 'Rayon 15km',
          '20' => 'Rayon 20km',
          '25' => 'Rayon 25km',
          '30' => 'Rayon 30km'),
        "required" => false, 'render_optional_text' => false))

      //DATES
      ->add('datearrivee', 'text', array('label' => "Arrivée", "required" => true))
      ->add('datedepart', 'text', array('label' => "Départ", "required" => true))

      //TYPES DE BIENS
      ->add('type', 'choice', array(
        'label' => "Type de bien",
        'choices' => array('Appartement' => 'Appartement',
          'Studio' => 'Studio',
          'Maison' => 'Maison'),
        'expanded' => true,
        'multiple' => true,
        "required" => false, 'render_optional_text' => false))

      //NOMBRE DE PIECES
      ->add('nb_pieces', 'choice', array(
        'label' => "Nombre de pièces",
        'choices' => array(1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6'),
        'expanded' => true,
        'multiple' => true,
        "required" => false, 'render_optional_text' => false))

      //BUDGET
      ->add('budgetmini', 'number', array('label' => "Mini", "required" => false, 'render_optional_text' => false))
      ->add('budgetmaxi', 'number', array('label' => "Maxi", "required" => false, 'render_optional_text' => false))

      //PLUS
      ->add('photo', 'checkbox', array('label' => 'Photos', "required" => false, 'render_optional_text' => false));
  }

  public function getName()
  {
    return 'adminreservationrechercheappartement';
  }

}
