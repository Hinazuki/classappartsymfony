<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\FormBuilderInterface;
use Pat\CompteBundle\Form\Type\DatefrType;
use Symfony\Component\Validator\Constraints as Assert;

class CalendrierForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('intitule', 'text', array('label' => "Intitulé", 'required' => false))
      ->add('date_debut', 'datefr', [
        'label' => "Date d'arrivée",
        'required' => true,
        'constraints' => [
          new Assert\NotBlank(['message' => "Veuillez sélectionner une date d'arrivée"]),
        ],
      ])
      ->add('heure_debut', 'choice', [
        'label' => "Heure d'arrivée",
        'choice_list' => new ChoiceList(
          [
          null,
          (new \DateTime())->setTime(16, 0, 0),
          (new \DateTime())->setTime(17, 0, 0),
          (new \DateTime())->setTime(18, 0, 0),
          (new \DateTime())->setTime(19, 0, 0),
          (new \DateTime())->setTime(20, 0, 0),
          (new \DateTime())->setTime(21, 0, 0),
          (new \DateTime())->setTime(22, 0, 0),
          (new \DateTime())->setTime(23, 0, 0),
          ], [
          'Sélectionner une heure',
          '16:00',
          '17:00',
          '18:00',
          '19:00',
          '20:00',
          '21:00',
          '22:00',
          '23:00',
          ]
        ),
        'constraints' => [
          new Assert\NotBlank(['message' => "Veuillez sélectionner une heure d'arrivée"]),
        ],
      ])
      ->add('heure_fin', 'choice', [
        'label' => "Heure de départ",
        'choice_list' => new ChoiceList(
          [
          null,
          (new \DateTime())->setTime(9, 0, 0),
          (new \DateTime())->setTime(10, 0, 0),
          (new \DateTime())->setTime(11, 0, 0),
          ], [
          'Sélectionner une heure',
          '09:00',
          '10:00',
          '11:00',
          ]
        ),
        'constraints' => [
          new Assert\NotBlank(['message' => "Veuillez sélectionner une heure de départ"]),
        ],
      ])
      ->add('date_fin', 'datefr', [
        'label' => "Date de départ",
        'required' => true,
        'constraints' => [
          new Assert\NotBlank(['message' => "Veuillez sélectionner une date de départ"]),
        ],
      ])
      ->add('recontacter', 'checkbox', [
        'label' => 'Etre recontacté par Class-Appart',
        'mapped' => false,
        'required' => false,
        'attr' => [
          'class' => 'js-recontacter-input'
        ]
      ])
      ->add('commentaire', 'textarea', [
        'label' => 'Commentaire',
        'mapped' => false,
        'required' => false,
      ])
    ;
  }

  public function getName()
  {
    return 'calendrier';
  }

}
