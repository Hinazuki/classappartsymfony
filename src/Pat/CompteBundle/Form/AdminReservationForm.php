<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Pat\CompteBundle\Tools\ReservationTools;
use Pat\CompteBundle\Form\DataTransformer\DateTransformer;

class AdminReservationForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('date_debut', 'datefr', array(
        'label' => 'Date début',
        'required' => true,
        'max_length' => 10
      ))
      ->add('date_fin', 'datefr', array(
        'label' => 'Date fin',
        'required' => true,
        'max_length' => 10
      ))
      ->add('status', 'choice', array(
        'label' => 'Etat',
        'choices' => ReservationTools::getStatusArray(),
        'required' => true
      ))
      ->add('depot', 'number', array(
        'label' => 'Dépôt de garantie'
      ))
      ->add('arrhes', 'number', array(
        'label' => 'Acompte'
      ))
      ->add('email_customer', 'choice', array(
        'label' => 'Notifier le client',
        'choices' => array(1 => 'Oui', 0 => 'Non'),
        'data' => 1,
        'mapped' => false,
        'expanded' => true,
        'multiple' => false
      ))
      ->add('email_owner', 'choice', array(
        'label' => 'Notifier le propriétaire',
        'choices' => array(1 => 'Oui', 0 => 'Non'),
        'data' => 1,
        'mapped' => false,
        'expanded' => true,
        'multiple' => false
      ))
      ->add('comment', 'textarea', [
        'label' => 'Commentaire',
        'required' => false,
      ]);
  }

  public function getName()
  {
    return 'Reservation';
  }

}
