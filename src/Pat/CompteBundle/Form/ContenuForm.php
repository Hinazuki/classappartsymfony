<?php

namespace Pat\CompteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Pat\CompteBundle\Repository\ContenuRepository;

class ContenuForm extends AbstractType
{

  private $langue; //langue utilisée
  private $id_parent; //id de la rubrique parente

  public function __construct($id_parent = "0", $langue = null)
  {
    $this->langue = $langue;
    $this->id_parent = $id_parent;
  }

  public function getDefaultOptions(array $options)
  {
    return array(
      'langue' => "fr",
      'id_parent' => "0"
    );
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $langue = $this->langue; //on récupère la langue utilisée
    $id_parent = $this->id_parent; //on récupère l'id du parent

    $builder
      ->add('titre', 'text', array('label' => "Titre", 'required' => true, 'max_length' => '250'))
      ->add('description', 'textarea', array('label' => "Corps", 'required' => false, 'render_optional_text' => false, 'attr' => array('class' => "ckeditor")))
      ->add('url', 'text', array('label' => "URL de la page", 'required' => false, 'max_length' => '60', 'render_optional_text' => false))
      ->add('meta_titre', 'text', array('label' => "Méta titre", 'required' => false, 'max_length' => '100', 'render_optional_text' => false))
      ->add('meta_description', 'textarea', array('label' => "Méta description", 'required' => false, 'max_length' => '250', 'render_optional_text' => false))
      ->add('type', 'choice', array(
        'label' => "Type de contenu",
        'choices' => array('Page' => 'Page',
          'Message' => 'Message',
        ), 'required' => true))
      ->add('statut', 'choice', array(
        'label' => "Statut",
        'choices' => array('1' => 'Publié',
          '0' => 'Désactivé',
        ), 'required' => true,
        'expanded' => true,
        'multiple' => false,))
      ->add('affiche_menu', 'choice', array(
        'label' => "Afficher dans le menu",
        'choices' => array('1' => 'Oui',
          '0' => 'Non',
        ), 'required' => true,
        'expanded' => true,
        'multiple' => false,))

//		->add('langue', 'choice', array(
//			'label' => "Langue",
//			'choices' => array( 'fr'=>'fr',
//								'en'=>'en',)))
      ->add('on_sidebar', 'choice', array(
        'label' => "Afficher en sélection",
        'choices' => array('1' => 'Oui',
          '0' => 'Non',
        ), 'required' => true,
        'expanded' => true,
        'multiple' => false,))
      ->add('id_parent', 'entity', array(
        'class' => 'Pat\CompteBundle\Entity\Contenu',
        'label' => "Page parente",
        'property' => 'TitreContenu',
        'expanded' => false,
        'multiple' => false,
        'required' => false,
        'render_optional_text' => false,
        'query_builder' => function(ContenuRepository $cr) use($id_parent, $langue) {
          return $cr->findContenuByIdParent($id_parent, $langue);
        }
      ))

    ;
  }

  public function getName()
  {
    return 'Contenu';
  }

}
