<?php

namespace Pat\CompteBundle\Controller;

require_once(__DIR__.'/../Lib/Image.php');

use Pat\CompteBundle\Lib\Image;
use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Pat\CompteBundle\Entity\Appartement;
use Pat\CompteBundle\Entity\Media;
use Pat\CompteBundle\Form\MediaForm;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MediaController extends ContainerAware
{

  //appel ajax pour trier les photos
  public function triAction($id)
  {
    $request = $this->container->get('request');

    $em = $this->container->get('doctrine')->getManager();

    if ($request->isXmlHttpRequest()) {
      $list_array = '';
      $list_array = $request->request->get('list_array');
      $list_array = str_replace('arrayorder_', '', $list_array); //on récupère la chaine envoyée
      $tab_tri = explode(",", $list_array); //on crée un tableau

      for ($i = 0; $i < sizeof($tab_tri) - 1; $i++) {
        $em->createQueryBuilder()
          ->update('PatCompteBundle:Media', 'p')
          ->set('p.tri', $i)
          ->where('p.id='.$tab_tri[$i])
          ->getQuery()
          ->getResult();
      }

      //permet de retournée "success"
      $response = new Response(json_encode(array('success' => true)));
    }
    else {
      $response = new Response(json_encode(array('success' => false)));
    }

    return $response;
  }

}
