<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\CompteBundle\Form\AdminProprietaireRechercheForm;
use Pat\CompteBundle\Form\AdminProprietaireForm;
use Pat\UtilisateurBundle\Entity\Utilisateur;
use Pat\UtilisateurBundle\Repository\UtilisateurRepository;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class AdminProprietaireController extends ContainerAware
{

  //afficher le formulaire d'ajout d'un propriétaire
  public function ajouterAction($id_proprio = null, $message = null, $erreur = null)
  {

    // On stock le referer s'il n'est pas déjà en session
    if (is_null($this->container->get('session')->get('previous_page'))) {
      $url = $this->container->get('request')->headers->get("referer");
      $this->container->get('session')->set('previous_page', $url);
    }

    $this->locale = $this->container->get('request')->getLocale();
    $validation = '';
    $em = $this->container->get('doctrine')->getManager();


    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    if (!$user) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    if (isset($id_proprio)) {
      $utilisateur = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array("type_utilisateur" => "2", "id" => $id_proprio));
      if (!$utilisateur) {
        throw new AccessDeniedException("Cet utilisateur n'existe pas");
      }
    }
    else {
      // ajout d'un nouvel utilisateur
      $utilisateur = new Utilisateur();
    }


    $form = $this->container->get('form.factory')->create(new AdminProprietaireForm(), $utilisateur);
    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST') {

      $form->bind($request);

      if ($form->isValid()) {

        $patternEmail = '#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';

        if (!preg_match($patternEmail, $form['email']->getData())) {
          $message = "L'adresse mail n'est pas valide";
        }
        else {

          //s'il s'agit d'un nouveau proprio
          if (!isset($id_proprio)) {

            $isExistEmail = $em->getRepository('PatUtilisateurBundle:Utilisateur')->checkEmailCompteAction($form['email']->getData());

            if (!$isExistEmail) {
              $utilisateur->setEmailCanonical($form['email']->getData());
              $utilisateur->setConfirmationToken(null);
              $utilisateur->setEnabled(true);
              $utilisateur->setProprietaire();
              $utilisateur->setCGU("1");

              //gestion du mot de passe
              $password = $em->getRepository('PatUtilisateurBundle:Utilisateur')->generatePassword();
              $encoder = $this->container->get('security.encoder_factory')->getEncoder($utilisateur);
              $utilisateur->setPassword($encoder->encodePassword($password, $utilisateur->getSalt()));
              $utilisateur->setConfirmationToken($this->container->get('fos_user.util.token_generator')->generateToken());
              $url = $this->container->get('router')->generate('fos_user_registration_confirm', array(
                'token' => $utilisateur->getConfirmationToken()
              ), true);

              $em->persist($utilisateur);
              $em->flush();

              $utilisateur->generateUsername();

              $em->flush();

              //envoi du mail au proprietaire
              $message_email = \Swift_Message::newInstance()
                ->setSubject("Création de votre compte propriétaire - classAppart ®")
                ->setFrom('contact@class-appart.com')
                ->setBcc($this->container->getParameter('mail_admin'))
                ->setTo($utilisateur->getEmail());

//                            if($utilisateur->getMailsSecond())
//                                $message_email->setCc(explode(',', $utilisateur->getMailsSecond()));
              if ($utilisateur->getSecondEmail()) {
                $message_email->setCc($utilisateur->getSecondEmail());
              }

              $textBody = $this->container->get('templating')->render(
                'PatCompteBundle:AdminProprietaire:email_validation.txt.twig',
                compact('utilisateur', 'password', 'url')
              );
              $htmlBody = $this->container->get('templating')->render(
                'PatCompteBundle:AdminProprietaire:email_validation.html.twig',
                compact('utilisateur', 'password', 'url')
              );

              if (!empty($htmlBody)) {
                $message_email->setBody($htmlBody, 'text/html')
                  ->addPart($textBody, 'text/plain');
              }
              else
                $message_email->setBody($textBody);

              $this->container->get('mailer')->send($message_email);


              $message = 'Propriétaire ajouté avec succès !';
              $this->container->get('session')->getFlashBag()->add('flash', $message);

              //return new RedirectResponse($this->container->get('router')->generate('pat_admin_proprietaire_editer', array('id_proprio' => $utilisateur->getId())));
              return new RedirectResponse($this->container->get('router')->generate('pat_admin_proprietaire_afficher', array('id_proprio' => $utilisateur->getId())));
            }
            else {
              $message = 'Cette adresse mail existe déjà';
            }
          }
          else {  //s'il s'agit d'une modification de compte
            $isExistEmail = $em->getRepository('PatUtilisateurBundle:Utilisateur')->checkEmailCompteAction($form['email']->getData(), $id_proprio);
            if (!$isExistEmail) {
              $utilisateur->setEmailCanonical($form['email']->getData());
              $utilisateur->setUpdatedAt(new \DateTime("now"));

              // gestion du mot de passe
              // $password = $em->getRepository('PatUtilisateurBundle:Utilisateur')->generatePassword();
              // $encoder = $this->container->get('security.encoder_factory')->getEncoder($utilisateur);
              // $utilisateur->setPassword($encoder->encodePassword($password, $utilisateur->getSalt()));

              $em->flush();

              $this->container->get('session')->getFlashBag()->add('success', 'Propriétaire modifié avec succès !');

              // On récupère le referer stocké, le supprime de la session et redirige vers cette page
              $previous_page = $this->container->get('session')->get('previous_page');
              $this->container->get('session')->remove('previous_page');

              if ($previous_page)
                return new RedirectResponse($previous_page);
              else
                return new RedirectResponse($this->container->get('router')->generate('pat_admin_proprietaire_afficher', array('id_proprio' => $utilisateur->getId())));
            }
            else {
              $message = 'Cette adresse mail existe déjà';
            }
          }//end if proprio
        }
      }
    }


    return $this->container->get('templating')->renderResponse(
        'PatCompteBundle:AdminProprietaire:ajouter.html.twig', array(
        'form' => $form->createView(),
        'message' => $message,
        'utilisateur' => $utilisateur,
        'id_proprio' => $id_proprio,
    ));
  }

  public function testTabVide($tab)
  {
    foreach ($tab as $t):
      if ($t != null && $t != "") {
        return false;
      }
    endforeach;
    return true;
  }

  //lister les propriétaire
  public function indexAction($message = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $session = $this->container->get('request')->getSession();

    $formRecherche = $this->container->get('form.factory')->create(new AdminProprietaireRechercheForm());
    $request = $this->container->get('request');
    $type = "2"; //on recherche un propriétaire
    // Si requête POST, alors c'est une nouvelle recherche, on stock les valeurs en session si le formulaire est valide
    if ($request->getMethod() == 'POST') {

      $formRecherche->bind($request);

      if ($formRecherche->isValid()) {
        $proprietairerecherche = $request->request->get('proprietairerecherche');

        $admin_search_prop = array();
        $admin_search_prop['nom'] = str_replace("'", " ", $proprietairerecherche["nom"]);
        $admin_search_prop['prenom'] = str_replace("'", " ", $proprietairerecherche["prenom"]);
        $admin_search_prop['num_compte'] = str_replace("'", " ", $proprietairerecherche["num_compte"]);

        $session->set('admin_search_prop', $admin_search_prop);
      }
      else {
        $pagination = array();
        //$message = "Veuillez entrer au moins un des critères !";
      }
    }
    else {
      if ($this->container->get('request')->query->get('page')) {
        // On récupère les éléments de recherche en session
        $admin_search_prop = $session->get('admin_search_prop');
      }
      else {
        // Si page n'est pas définie, on souhaite afficher tous les propriétaires
        $admin_search_prop = array();
        $admin_search_prop['nom'] = "";
        $admin_search_prop['prenom'] = "";
        $admin_search_prop['num_compte'] = "";

        $session->set('admin_search_prop', $admin_search_prop);
      }
    }

    $query = $em->getRepository('PatUtilisateurBundle:Utilisateur')->searchUtilisateurQuery($admin_search_prop['nom'], $admin_search_prop['prenom'], $admin_search_prop['num_compte'], $type);

    $paginator = $this->container->get('knp_paginator');
    $pagination = $paginator->paginate(
      $query, $this->container->get('request')->query->get('page', 1), 20
    );

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminProprietaire:liste.html.twig', array('proprietaire' => $pagination, 'message' => $message,
        "isSearchProprio" => $admin_search_prop['nom'])
    );
  }

  //afficher le détail d'un propriétaire
  public function afficherAction($id_proprio)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $proprietaire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($id_proprio);
    if (!$proprietaire) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $reservations = $em->getRepository('PatCompteBundle:Reservation')->getCurrentReservations($proprietaire);

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminProprietaire:afficher.html.twig', array(
        'proprietaire' => $proprietaire,
        'reservations' => $reservations
    ));
  }

  //Rechercher un propriétaire
  public function rechercherAction($message = null)
  {

    $formRecherche = $this->container->get('form.factory')->create(new AdminProprietaireRechercheForm());
    $request = $this->container->get('request');
    if ($request->getMethod() == 'POST') {
      $formRecherche->bind($request); //on conserve les données dans le formulaire
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminProprietaire:rechercher.html.twig', array('message' => $message, 'formRecherche' => $formRecherche->createView())
    );
  }

  public function desactiverAction($id_proprio)
  {
    $em = $this->container->get('doctrine')->getManager();
    $message = "";

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());


    // modification d'un utilisateur
    $utilisateur = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($id_proprio);
    if (!$utilisateur) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $utilisateur->setEnabled('0');
    $em->persist($utilisateur);
    $em->flush();

    header("Location:".$_SERVER["HTTP_REFERER"]);
    exit;
  }

  public function activerAction($id_proprio)
  {
    $em = $this->container->get('doctrine')->getManager();
    $message = "";

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());


    // modification d'un utilisateur
    $utilisateur = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($id_proprio);
    if (!$utilisateur) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $utilisateur->setEnabled('1');
    $em->persist($utilisateur);
    $em->flush();

    header("Location:".$_SERVER["HTTP_REFERER"]);
    exit;
  }

  //lister les appartements d'un propriétaire
  public function listeAppartementAction($id_proprio = null, $message = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $proprietaire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($id_proprio);
    if (!$proprietaire) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findByUtilisateur($id_proprio);

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminProprietaire:listeAppartement.html.twig', array('appartement' => $appartement, 'proprietaire' => $proprietaire, 'message' => $message)
    );
  }

  //lister les réservations d'un propriétaire
  public function listeReservationAction($id_proprio = null, $message = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $proprietaire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($id_proprio);
    if (!$proprietaire) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $reservations = "";

    //on récupère les résas
    $reservations = $em->getRepository('PatCompteBundle:Reservation')->findBy(array('utilisateur' => $proprietaire), array('created_at' => 'DESC'));

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminProprietaire:listeReservation.html.twig', array('reservations' => $reservations,
        'proprietaire' => $proprietaire
    ));
  }

  //lister les réservations faites dans les Biens du propriétaire
  public function listeResasLocAction($id_proprio = null, $message = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $proprietaire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($id_proprio);
    if (!$proprietaire) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $reservations = "";

    //on récupère les résas
    $reservations = $em->getRepository('PatCompteBundle:Reservation')->getResaByProprio($id_proprio);

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminProprietaire:listeResasLoc.html.twig', array('reservations' => $reservations,
        'proprietaire' => $proprietaire
    ));
  }

  public function menuProprietaireAction()
  {
    $em = $this->container->get('doctrine')->getManager();
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    $menu_style = 1;

    $nb_app = $em->getRepository('PatCompteBundle:Appartement')->searchAppartement(null, null, null, null, null, null, null, null, "recup_nombre", $user->getId());

    if ($nb_app && $nb_app[0]['nombre'] > 50) {
      $menu_style = 2;
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminProprietaire:menuProprietaire.html.twig', array('menu_style' => $menu_style)
    );
  }

  public function sendInfosAction($id_proprio = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    $proprietaire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($id_proprio);
    if (!$proprietaire) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $biens = $em->getRepository('PatCompteBundle:Appartement')->findBy(array('utilisateur' => $proprietaire, 'statut' => 4));

    $message_email = \Swift_Message::newInstance()
      ->setSubject("Vos informations - classAppart ®")
      ->setFrom('contact@class-appart.com')
      ->setBcc($this->container->getParameter('mail_admin'))
      ->setTo($proprietaire->getEmail());

//            if($proprietaire->getMailsSecond())
//                $message_email->setCc(explode(',', $proprietaire->getMailsSecond()));
    if ($proprietaire->getSecondEmail()) {
      $message_email->setCc($proprietaire->getSecondEmail());
    }

    $textBody = $this->container->get('templating')->render('PatCompteBundle:AdminProprietaire:email_infos.txt.twig', array('utilisateur' => $proprietaire, 'biens' => $biens));
    $htmlBody = $this->container->get('templating')->render('PatCompteBundle:AdminProprietaire:email_infos.html.twig', array('utilisateur' => $proprietaire, 'biens' => $biens));

    if (!empty($htmlBody)) {
      $message_email->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain');
    }
    else
      $message_email->setBody($textBody);

    $this->container->get('mailer')->send($message_email);

    $this->container->get('session')->getFlashBag()->add(
      'success', 'Les informations ont été envoyées au propriétaire.'
    );

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_proprietaire_afficher', array('id_proprio' => $proprietaire->getId())));
  }

}
