<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\CompteBundle\Entity\Appartement;

class DefaultController extends ContainerAware
{

  public function indexAction()
  {
    $em = $this->container->get('doctrine')->getManager();
    $message = 'Vous êtes sur la page d\'accueil du compte';

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Default:index.html.twig', array('message' => $message)
    );
  }

}
