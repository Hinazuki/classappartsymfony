<?php

namespace Pat\CompteBundle\Controller;

require_once(__DIR__.'/../Lib/Image.php');

use Pat\CompteBundle\Lib\Image;
use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Pat\CompteBundle\Entity\Appartement;
use Pat\CompteBundle\Entity\Media;
use Pat\CompteBundle\Form\MediaForm;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AdminMediaController extends ContainerAware
{

  //afficher le formulaire d'ajout d'un média
  public function ajouterAction($id = null)
  {

    $message = '';
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $appartement = $em->find('PatCompteBundle:Appartement', $id);

    $media = new Media();
    $form = $this->container->get('form.factory')->create(new MediaForm(), $media);
    $formView = $form->createView();

    // Fixing previous full_name modification
    $formView->children['fichier']->vars['full_name'] = 'Media[fichier][]';
//                $formView->getChild('fichier')->set('full_name', 'Media[fichier][]');


    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST') {
      $file = $form['fichier']->getData();
      $form->bind($request);

      if ($form->isValid()) {
        //upload file directory
        $dir = __DIR__.'/../../../../web/images/photos/biens/';
        $ref_appart = $appartement->getReference();

        $file = $form['fichier']->getData();

        if (sizeof($file) <= 1 and $file[0] == "") {
          $message = "Vous devez charger au moins un fichier";
        }
        elseif (sizeof($file) > 10) {
          $message = "Vous ne pouvez pas télécharger plus de 10 photos en même temps.";
        }
        else {

          //on test le poids et la taille des photos
          $error = false;
          for ($i = 0; $i < sizeof($file); $i++) {


            //gestion du type de fichier
            if ($file[$i]->guessExtension() != "jpg" and $file[$i]->guessExtension() != "jpeg" and $file[$i]->guessExtension() != "gif" and $file[$i]->guessExtension() != "png") {
              $message = "Les photos doivent être au format JPG, GIF ou PNG";
              $error = true;
              break;
            }

            //gestion de la taille du fichier
            $infos_image = getImageSize($file[$i]);
            if ($infos_image[0] > 2500) {
              $message = "Les photos ne doivent pas dépasser 2500 pixels de large";
              $error = true;
              break;
            }

            //gestion de la taille du fichier
            if (empty($infos_image[0]) or empty($infos_image[1])) {
              $message = "Les photos doivent avoir une largeur ou hauteur supérieure à 0px";
              $error = true;
              break;
            }


            //gestion du poids du fichier
            if (filesize($file[$i]) > 5000000) {
              $message = "Le poids d'une photo ne doit pas dépasser 5Mo";
              $error = true;
              break;
            }
          }


          if (!$error) {
            if (!file_exists($dir.$ref_appart)) {
              mkdir($dir.$ref_appart);
            }
            if (!file_exists($dir.$ref_appart."/small")) {
              mkdir($dir.$ref_appart."/small", 0777);
            }
            if (!file_exists($dir.$ref_appart."/big")) {
              mkdir($dir.$ref_appart."/big", 0777);
            }

            for ($i = 0; $i < sizeof($file); $i++) {

              //on conserve la version de base de la photo
              $extension = $file[$i]->guessExtension();
              //$nom_fichier = $file->getClientOriginalName();
              $nom_fichier = "pat-".rand("1000000", "9999999").'.'.$extension;
              $file[$i]->move($dir.$ref_appart, $nom_fichier);


              // Redimensionnement de l'image et recadrage (big)
              $infos_image = @getImageSize($dir.$ref_appart."/".$nom_fichier); // info sur la dimension de l'image



              if ($infos_image[0] == $infos_image[1]) {
                $h = 285;
                $w = 450;

                $y = 0;
                $x = ($w - 450) / 2;

                Image::open($dir.$ref_appart."/".$nom_fichier)
                  ->resize($w, $h)
                  ->crop($x, $y, 450, 285)
                  ->save('images/photos/biens/'.$ref_appart.'/big/'.$nom_fichier);
              }
              else if ($infos_image[0] >= $infos_image[1]) {
                $h = 285;
                $w = 450 * $infos_image[0] / $infos_image[1];

                $y = 0;
                $x = ($w - 450) / 2;


                Image::open($dir.$ref_appart."/".$nom_fichier)
                  ->resize($w, $h)
                  ->crop($x, $y, 450, 285)
                  ->save('images/photos/biens/'.$ref_appart.'/big/'.$nom_fichier);
              }
              else {
                $w = 450;
                $h = 285 * $infos_image[1] / $infos_image[0];

                $x = 0;
                $y = ($h - 285) / 2;

                Image::open($dir.$ref_appart."/".$nom_fichier)
                  ->resize($w, $h)
                  ->crop($x, $y, 450, 285)
                  ->save('images/photos/biens/'.$ref_appart.'/big/'.$nom_fichier);
              }




              // Redimensionnement de l'image et recadrage (small)
              if ($infos_image[0] == $infos_image[1]) {
                $h = 120;
                $w = 120;

                $y = 0;
                $x = ($w - 120) / 2;
              }
              else if ($infos_image[0] >= $infos_image[1]) {
                $h = 120;
                $w = 120 * $infos_image[0] / $infos_image[1];
                //echo $w. "".$h."<br/>";exit;

                $y = 0;
                $x = ($w - 120) / 2;
              }
              else {
                $w = 120;
                $h = 120 * $infos_image[1] / $infos_image[0];

                $x = 0;
                $y = ($h - 120) / 2;
              }

              Image::open($dir.$ref_appart."/".$nom_fichier)
                ->resize($w, $h)
                ->crop($x, $y, 120, 120)
                ->save('images/photos/biens/'.$ref_appart.'/small/'.$nom_fichier);




              //enregistrement
              $photo = new Media();
              $photo->setFichier($nom_fichier);
              $photo->setTitre("");
              $photo->setTri(rand("100", "999"));
              $photo->setAppartement($appartement);
              $em->persist($photo);
            }
            $em->flush();
            $message = 'Photos ajoutées avec succès !';
          }
        }
      }
    }

    $repository = $em->getRepository('PatCompteBundle:Media');
    $media = $repository->findBy(array('appartement' => $id), array('tri' => 'asc'));


    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminMedia:ajouter.html.twig', array(
        'form' => $formView,
        'message' => $message,
        'media' => $media,
        'appartement' => $appartement,
    ));
  }

  //lister les medias
  public function indexAction($id)
  {

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_media_ajouter', array('id' => $id)));
  }

  public function supprimerAction($id, $id_media)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());


    $media = $em->find('PatCompteBundle:Media', $id_media);

    if (!$media) {
      throw new NotFoundHttpException("Photo non trouvée");
    }

    @unlink(__DIR__.'/../../../../web/images/photos/biens/'.$media->getFichier());
    @unlink(__DIR__.'/../../../../web/images/photos/biens/'.$media->getFichier());
    @unlink(__DIR__.'/../../../../web/images/photos/biens/'.$media->getFichier());

    $em->remove($media);
    $em->flush();



    return new RedirectResponse($this->container->get('router')->generate('pat_admin_media_ajouter', array('id' => $id)));
  }

  //appel ajax pour trier les photos
  public function triAction($id)
  {
    $request = $this->container->get('request');

    $em = $this->container->get('doctrine')->getManager();

    if ($request->isXmlHttpRequest()) {
      $list_array = '';
      $list_array = $request->request->get('list_array');
      $list_array = str_replace('arrayorder_', '', $list_array); //on récupère la chaine envoyée
      $tab_tri = explode(",", $list_array); //on crée un tableau

      for ($i = 0; $i < sizeof($tab_tri) - 1; $i++) {
        $em->createQueryBuilder()
          ->update('PatCompteBundle:Media', 'p')
          ->set('p.tri', $i)
          ->where('p.id='.$tab_tri[$i])
          ->getQuery()
          ->getResult();
      }

      // permet de retournée "success"
      $response = new Response(json_encode(array('success' => true)));
    }
    else {
      $response = new Response(json_encode(array('success' => false)));
    }

    return $response;
  }

}
