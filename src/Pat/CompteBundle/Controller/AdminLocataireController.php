<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\CompteBundle\Form\AdminLocataireRechercheForm;
use Pat\CompteBundle\Form\AdminLocataireForm;
use Pat\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminLocataireController extends ContainerAware
{

  //afficher le formulaire d'ajout d'un locataire
  public function ajouterAction($id_locataire = null, $message = null, $erreur = null)
  {

    // On stock le referer s'il n'est pas déjà en session
    if (is_null($this->container->get('session')->get('previous_page'))) {
      $url = $this->container->get('request')->headers->get("referer");
      $this->container->get('session')->set('previous_page', $url);
    }

    $this->locale = $this->container->get('request')->getLocale();
    $validation = '';
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }


    if (isset($id_locataire)) {
      $utilisateur = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array("type_utilisateur" => "1", "id" => $id_locataire));
      if (!$utilisateur) {
        throw new AccessDeniedException("Cet utilisateur n'existe pas");
      }
    }
    else {
      // ajout d'un nouvel utilisateur
      $utilisateur = new Utilisateur();
    }



    $form = $this->container->get('form.factory')->create(new AdminLocataireForm(), $utilisateur);
    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST') {
      $form->bind($request);
      if ($form->isValid()) {

        $patternEmail = '#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';

        if (!preg_match($patternEmail, $form['email']->getData())) {
          $message = "L'adresse mail n'est pas valide";
        }
        else {

          //s'il s'agit d'un nouveau locataire
          if (!isset($id_locataire)) {

            $isExistEmail = $em->getRepository('PatUtilisateurBundle:Utilisateur')->checkEmailCompteAction($form['email']->getData());
            if (!$isExistEmail) {
              $utilisateur->setEmailCanonical($form['email']->getData());
              $utilisateur->setEnabled(true);
              $utilisateur->setLocataire();
              $utilisateur->setCGU("1");


              //gestion du mot de passe
              $password = $em->getRepository('PatUtilisateurBundle:Utilisateur')->generatePassword();
              $encoder = $this->container->get('security.encoder_factory')->getEncoder($utilisateur);
              $utilisateur->setPassword($encoder->encodePassword($password, $utilisateur->getSalt()));

              $em->persist($utilisateur);
              $em->flush();

              $utilisateur->generateUsername();

              $em->flush();

              //envoi du mail au locataire
              $message_email = \Swift_Message::newInstance()
                ->setSubject("Création de votre compte locataire - ClassAppart ®")
                ->setFrom('contact@class-appart.com')
                ->setTo($utilisateur->getEmail())
                ->setBcc($this->container->getParameter('mail_admin'))
              ;

//                            if($utilisateur->getMailsSecond())
//                                $message_email->setCc(explode(',', $utilisateur->getMailsSecond()));
              if ($utilisateur->getSecondEmail()) {
                $message_email->setCc($utilisateur->getSecondEmail());
              }

              $textBody = $this->container->get('templating')->render('PatCompteBundle:AdminLocataire:email_validation.txt.twig', array('utilisateur' => $utilisateur, 'password' => $password));
              $htmlBody = $this->container->get('templating')->render('PatCompteBundle:AdminLocataire:email_validation.html.twig', array('utilisateur' => $utilisateur, 'password' => $password));

              if (!empty($htmlBody)) {
                $message_email->setBody($htmlBody, 'text/html')
                  ->addPart($textBody, 'text/plain');
              }
              else
                $message_email->setBody($textBody);

              $this->container->get('mailer')->send($message_email);


              $message = 'Locataire ajouté avec succès !';
              $this->container->get('session')->getFlashBag()->add('flash', $message);

              //return new RedirectResponse($this->container->get('router')->generate('pat_admin_locataire_editer', array('id_locataire' => $utilisateur->getId())));
              return new RedirectResponse($this->container->get('router')->generate('pat_admin_locataire_afficher', array('id_locataire' => $utilisateur->getId())));
            }
            else {
              $message = 'Cette adresse mail existe déjà';
            }
          }
          else { //s'il s'agit d'une modification de compte
            $isExistEmail = $em->getRepository('PatUtilisateurBundle:Utilisateur')->checkEmailCompteAction($form['email']->getData(), $id_locataire);
            if (!$isExistEmail) {
              $utilisateur->setEmailCanonical($form['email']->getData());
              $utilisateur->setUpdatedAt(new \DateTime("now"));

              $em->flush();

              $this->container->get('session')->getFlashBag()->add('success', 'Locataire modifié avec succès !');

              // On récupère le referer stocké, le supprime de la session et redirige vers cette page
              $previous_page = $this->container->get('session')->get('previous_page');
              $this->container->get('session')->remove('previous_page');

              if ($previous_page)
                return new RedirectResponse($previous_page);
              else
                return new RedirectResponse($this->container->get('router')->generate('pat_admin_locataire_afficher', array('id_locataire' => $utilisateur->getId())));
            }
            else {
              $message = 'Cette adresse mail existe déjà';
            }
          }//end if locataire
        }
      }
    }


    return $this->container->get('templating')->renderResponse(
        'PatCompteBundle:AdminLocataire:ajouter.html.twig', array(
        'form' => $form->createView(),
        'message' => $message,
        'utilisateur' => $utilisateur,
        'id_locataire' => $id_locataire,
    ));
  }

  public function testTabVide($tab)
  {
    foreach ($tab as $t):
      if ($t != null && $t != "") {
        return false;
      }
    endforeach;
    return true;
  }

  //lister les locataire
  public function indexAction($message = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $session = $this->container->get('request')->getSession();

    $formRecherche = $this->container->get('form.factory')->create(new AdminLocataireRechercheForm());
    $request = $this->container->get('request');
    $type = "1"; //on recherche un locataire
    // Si requête POST, alors c'est une nouvelle recherche, on stock les valeurs en session si le formulaire est valide
    if ($request->getMethod() == 'POST') {
      $formRecherche->bind($request);

      if ($formRecherche->isValid()) {
        $locatairerecherche = $request->request->get('locatairerecherche');

        $admin_search_loc = array();
        $admin_search_loc['nom'] = str_replace("'", " ", $locatairerecherche["nom"]);
        $admin_search_loc['prenom'] = str_replace("'", " ", $locatairerecherche["prenom"]);
        $admin_search_loc['num_compte'] = str_replace("'", " ", $locatairerecherche["num_compte"]);

        $session->set('admin_search_loc', $admin_search_loc);
      }
      else {
        $pagination = array();
        //$message = "Veuillez entrer au moins un des critères !";
      }
    }
    else {
      if ($this->container->get('request')->query->get('page')) {
        // On récupère les éléments de recherche en session
        $admin_search_loc = $session->get('admin_search_loc');
      }
      else {
        // Si page n'est pas définie, on souhaite afficher tous les locataires
        $admin_search_loc = array();
        $admin_search_loc['nom'] = "";
        $admin_search_loc['prenom'] = "";
        $admin_search_loc['num_compte'] = "";

        $session->set('admin_search_loc', $admin_search_loc);
      }
    }

    $query = $em->getRepository('PatUtilisateurBundle:Utilisateur')->searchUtilisateurQuery($admin_search_loc['nom'], $admin_search_loc['prenom'], $admin_search_loc['num_compte'], $type);

    $paginator = $this->container->get('knp_paginator');
    $pagination = $paginator->paginate(
      $query, $this->container->get('request')->query->get('page', 1), 20
    );

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminLocataire:liste.html.twig', array('locataire' => $pagination, 'message' => $message, "isSearchResident" => $admin_search_loc['nom'])
    );
  }

  //afficher le détail d'un locataire
  public function afficherAction($id_locataire)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $locataire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($id_locataire);
    if (!$locataire) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $reservations = $em->getRepository('PatCompteBundle:Reservation')->getCurrentReservations($locataire);

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminLocataire:afficher.html.twig', array(
        'locataire' => $locataire,
        'reservations' => $reservations
    ));
  }

  //Rechercher un locataire
  public function rechercherAction($message = null)
  {

    $formRecherche = $this->container->get('form.factory')->create(new AdminLocataireRechercheForm());
    $request = $this->container->get('request');
    if ($request->getMethod() == 'POST') {
      $formRecherche->bind($request); //on conserve les données dans le formulaire
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminLocataire:rechercher.html.twig', array('message' => $message, 'formRecherche' => $formRecherche->createView())
    );
  }

  public function desactiverAction($id_locataire)
  {
    $em = $this->container->get('doctrine')->getManager();
    $message = "";

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());


    // modification d'un appartement existant : on recherche ses données en vérifiant que l'utilisateur à le droit me modifier l'appart
    $utilisateur = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($id_locataire);
    if (!$utilisateur) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $utilisateur->setEnabled('0');
    $em->persist($utilisateur);
    $em->flush();

    header("Location:".$_SERVER["HTTP_REFERER"]);
    exit;
  }

  public function activerAction($id_locataire)
  {
    $em = $this->container->get('doctrine')->getManager();
    $message = "";

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());


    // modification d'un appartement existant : on recherche ses données en vérifiant que l'utilisateur à le droit me modifier l'appart
    $utilisateur = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($id_locataire);
    if (!$utilisateur) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $utilisateur->setEnabled('1');
    $em->persist($utilisateur);
    $em->flush();

    header("Location:".$_SERVER["HTTP_REFERER"]);
    exit;
  }

  //lister les réservations d'un locataire
  public function listeReservationAction($id_locataire = null, $message = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $locataire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($id_locataire);
    if (!$locataire) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $reservations = "";

    //on récupère les résas
    $reservations = $em->getRepository('PatCompteBundle:Reservation')->findBy(array('utilisateur' => $locataire), array('created_at' => 'DESC'));

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminLocataire:listeReservation.html.twig', array('reservations' => $reservations,
        'locataire' => $locataire
    ));
  }

  public function sendInfosAction($id_locataire = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    $locataire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($id_locataire);
    if (!$locataire) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $message_email = \Swift_Message::newInstance()
      ->setSubject("Vos informations - classAppart ®")
      ->setFrom('contact@class-appart.com')
      ->setBcc($this->container->getParameter('mail_admin'))
      ->setTo($locataire->getEmail());

//            if($locataire->getMailsSecond())
//                $message_email->setCc(explode(',', $locataire->getMailsSecond()));
    if ($locataire->getSecondEmail()) {
      $message_email->setCc($locataire->getSecondEmail());
    }

    $textBody = $this->container->get('templating')->render('PatCompteBundle:AdminLocataire:email_infos.txt.twig', array('utilisateur' => $locataire));
    $htmlBody = $this->container->get('templating')->render('PatCompteBundle:AdminLocataire:email_infos.html.twig', array('utilisateur' => $locataire));

    if (!empty($htmlBody)) {
      $message_email->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain');
    }
    else
      $message_email->setBody($textBody);

    $this->container->get('mailer')->send($message_email);

    $this->container->get('session')->getFlashBag()->add(
      'success', 'Les informations ont été envoyées au locataire.'
    );

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_locataire_afficher', array('id_locataire' => $locataire->getId())));
  }

}
