<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Request;
use Pat\CompteBundle\Lib\CalendrierAppartement;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
//use Doctrine\Common\Collections\ArrayCollection;
use Pat\CompteBundle\Form\CalendrierRechercheForm;
use Pat\CompteBundle\Form\CalendrierAjouterForm;
use Pat\CompteBundle\Form\CalendrierForm;
use Pat\CompteBundle\Entity\Calendrier;
use Pat\CompteBundle\Tools\CalendrierTools;
use Pat\CompteBundle\Tools\ReservationTools;

class CalendrierController extends Controller
{

  //calendrier standard (pour proprietaire avec < 50 biens
  public function indexAction($id = null)
  {
    $tabCal = $calendrier = $formView = "";
    $tableauCalendrier = array();
    $calendrier = null;

    $em = $this->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $arrayRoles = $user->getRoles(); //on test les droits
    if ($arrayRoles[0] == "ROLE_ADMIN" or $user->getTypeUtilisateur() != "2")
      throw new AccessDeniedException('This user does not have access to this section.');


    //on récupère les infos	s'il y a au moins 1 appartement
    $recup_test_app = $em->getRepository('PatCompteBundle:Appartement')->searchAppartement(null, null, null, null, null, null, null, null, "recup_nombre", $user->getId());
    if ($recup_test_app)
      $test_app = $recup_test_app[0]['nombre'];

    if ($test_app > 0) {
      //affichage du calendrier
      $request = $this->container->get('request');

      if ($request->getMethod() == 'POST') {
        $tableauCalendrier = $request->request->get('calendrierrecherche');
        $mois = $tableauCalendrier["mois"];
        $annee = $tableauCalendrier["annee"];
        $id = $tableauCalendrier["Appartement"];

        $tabCal .= '<br/><table border="0" cellpadding="10" cellspacing="0" align="center"><tr valign="top">';
        $month[0] = $mois;
        $year[0] = $annee;
        $month1[0] = $this->recupMois($month[0]);

        //$tabresa = $em->getRepository('PatCompteBundle:Calendrier')->findBy(array('appartement' => $id_appart,'date' => $dateResa, 'statut' => "1"));
        $calendrier = new CalendrierAppartement($month[0], $year[0], $id, $em->getRepository('PatCompteBundle:Calendrier')->ListeResaByApp($id));
        $tabCal .= "<td><div class='calendarResa' style=''>".$calendrier->affichage("Propriétaire")."</div></td>";

        for ($i = 1; $i <= 5; $i++) {
          if ($i % 3 == 0)
            $tabCal .= "</tr><tr valign='top'>";

          if ($month[$i - 1] == "12") {
            $month[$i] = "01";
            $year[$i] = $year[$i - 1] + 1;
          }
          else {
            $month[$i] = "0".(($month[$i - 1]) * 1) + 1;
            $year[$i] = $year[$i - 1];
          }

          $calendrier->modification($month[$i], $year[$i], $id);
          $month1[$i] = $this->recupMois($month[$i]);

          $tabCal .= "<td><div class='calendarResa'>".$calendrier->affichage("Propriétaire")."</div></td>";
        }
        $tabCal .= '</tr></table>';
      }
      //$calendrier = $em->getRepository('PatCompteBundle:Calendrier')->findBy(array('appartement' => $id));
      //$appartement = $em->getRepository('PatCompteBundle:Appartement')->find($id);

      $form = $this->container->get('form.factory')->create(new CalendrierRechercheForm($user->getId()));
      $formView = $form->createView();
    }

    if ($test_app > 50) { // Si un utilisateur de plus de 50 Biens essaye d'afficher le formulaire de gestion de calendrier standard.
      return $this->container->get('templating')->renderResponse('PatCompteBundle:Calendrier:liste.html.twig', array(
          'tabCal' => "",
          'tableauCalendrier' => "",
          'form' => null
      ));
    }
    else {
      return $this->container->get('templating')->renderResponse('PatCompteBundle:Calendrier:liste.html.twig', array(
          'tabCal' => $tabCal,
          'tableauCalendrier' => $tableauCalendrier,
          'form' => $formView
      ));
    }
  }

  // Fonction qui affiche le calendrier des disponibilité d'un bien sur une année et propose un formulaire de blocage/déblocage de dates.
  public function DisplayCalByAppAction($annee, $id_bien)
  {
    $mois = 1;
    $tabCal = "";
    $calendrier = null;
    $message = "";

    $em = $this->get('doctrine')->getManager();

    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $id_bien));
    if (!$appartement)
      throw new NotFoundHttpException("Bien non trouvé");

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $arrayRoles = $user->getRoles(); //on test les droits
    if ($arrayRoles[0] == "ROLE_ADMIN" or $user->getTypeUtilisateur() != "2") {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    //on récupère les infos	s'il y a au moins 1 appartement
    $test_app = $em->getRepository('PatCompteBundle:Appartement')->searchAppartement(null, null, null, null, null, null, null, null, "recup_nombre", $user->getId());
    if ($test_app && $test_app[0]['nombre'] > 0) {

      //$form = $this->container->get('form.factory')->create(new CalendrierForm());

      $request = $this->container->get('request');

      /* if ($request->getMethod() == 'POST') {

        $form->bind($request);

        $tableau = $request->request->get('calendrier');
        $date_debut =  $tableau["date_debut"];
        $date_fin =  $tableau["date_fin"];
        $intitule =  $tableau["intitule"];


        //passage de la date au format EN
        $date_debut_fr = $this->toDateEn($date_debut);
        $date_fin_fr = $this->toDateEn($date_fin);

        $test_date_debut_fr = $date_debut_fr;
        $test_date_fin_fr = $date_fin_fr;

        if(str_replace("-","",$test_date_debut_fr) < date("Ymd"))
        $message='La date de début doit être supérieure à la date du jour !';

        else if(str_replace("-","",$test_date_fin_fr) <= $test_date_debut_fr)
        $message='La date de fin doit être supérieure à la date de début !';

        else {
        //si on bloque une période
        if($action == "1") {
        //on vérifie que la période n'est pas bloquée
        $sqlbis = "";
        $sql = "SELECT c FROM PatCompteBundle:Calendrier c
        WHERE c.appartement = ".$appartement->getId()."
        AND c.statut = '1'
        AND (";

        $nbnuits = $this->calculNbNuits($date_debut, $date_fin);
        $nbnuitsFormNow = $this->calculNbNuits(date("d/m/Y"), $date_debut);

        $i=0;
        while($i<$nbnuits){
        $date_resa = str_replace("/","-",$this->toDateEn(date("d/m/Y", mktime(0, 0, 0, date("m"), date("d")+$i+$nbnuitsFormNow, date("Y")))));
        if($i==0){
        $sqlbis .= " c.date = '$date_resa' ";
        }else{
        $sqlbis .= " OR c.date = '$date_resa' ";
        }
        $i++;

        }//end while
        $sql.= $sqlbis.')';
        $result = $em->createQuery($sql)->getResult();


        $message = "L'appartement n'est pas disponible pour ces dates !";
        //si l'appartement est bloqué
        if(sizeof($result) > 0){
        foreach($result as $item){
        if($item->getReservation() != null){	//si bloqué par une résa
        $message = "L'appartement n'est pas disponible pour ces dates : bloqué par une réservation en ligne !";
        break;
        }
        }


        }else{	//si la période est libre, on enregistre

        for($i=0;$i<=$nbnuits;$i++){

        $date_resa = str_replace("/","-",$this->toDateEn(date("d/m/Y", mktime(0, 0, 0, date("m"), date("d")+$i+$nbnuitsFormNow, date("Y"))))); 	//date + x jours

        $calendrier = new Calendrier();
        $calendrier->setDate($date_resa);
        $calendrier->setStatut(1);
        $calendrier->setIntitule($intitule);
        $calendrier->setUtilisateur($user);
        $calendrier->setAppartement($appartement);
        $calendrier->setCreatedAt(new \DateTime("now"));
        $em->persist($calendrier);

        }//end for

        $em->flush();

        $message = "La période a été bloquée avec succès !";

        }



        //si on débloque une période
        }else{

        $sqlbis = "";
        $sql = "SELECT c FROM PatCompteBundle:Calendrier c
        WHERE c.appartement = ".$appartement->getId()."
        AND c.statut = '1'
        AND c.reservation != ''
        AND (";

        $nbnuits = $this->calculNbNuits($date_debut, $date_fin); //nombre de nuits entre entre date debut et date fin
        $nbnuitsFormNow = $this->calculNbNuits(date("d/m/Y"), $date_debut); //nombre de nuits entre date du jour et date début
        $i=0;
        while($i<=$nbnuits){
        $date_resa = str_replace("/","-",$this->toDateEn(date("d/m/Y", mktime(0, 0, 0, date("m"), date("d")+$i+$nbnuitsFormNow, date("Y")))));
        if($i==0){
        $sqlbis .= " c.date = '$date_resa' ";
        }else{
        $sqlbis .= " OR c.date = '$date_resa' ";
        }
        $i++;

        }//end while


        $sql.= $sqlbis.')';
        $result = $em->createQuery($sql)->getResult();
        //						echo  $sql."<br/><br/>";

        if(sizeof($result)>0){
        $message = "Impossible de débloquer la période.<br/>Certaines dates sont réservées par des Résidents !";
        }else{


        for($i=0 ; $i<=$nbnuits ; $i++){

        $date = str_replace("/","-",$this->toDateEn(date("d/m/Y", mktime(0, 0, 0, date("m"), date("d")+$i+$nbnuitsFormNow, date("Y"))))); 	//date + x jours

        $cal = $em->getRepository('PatCompteBundle:Calendrier')->findOneBy(array('appartement' => $appartement->getId(), 'reservation' => null,'utilisateur' => $user_context->getId(), 'statut' => "1", 'date' => $date));

        if($cal){
        $em->remove($cal);
        }



        }//end for
        $em->flush();
        $message = "La période a été débloquée avec succès !";
        }


        }

        }//end if date non ok



        }//end if $request->getMethod() */

      $tabCal .= '<br/><table border="0" cellpadding="10" cellspacing="0" align="center"><tr valign="top">';
      $month[0] = $mois;
      $year[0] = $annee;
      $month1[0] = $this->recupMois($month[0]);

      //$tabresa = $em->getRepository('PatCompteBundle:Calendrier')->findBy(array('appartement' => $id_appart,'date' => $dateResa, 'statut' => "1"));
      $calendrier = new CalendrierAppartement($month[0], $year[0], $appartement->getId(), $em->getRepository('PatCompteBundle:Calendrier')->ListeResaByApp($appartement->getId()));
      $tabCal .= "<td><div class='calendarResa' style=''>".$calendrier->affichage("Propriétaire")."</div></td>";

      for ($i = 1; $i < 12; $i++) {
        if ($i % 3 == 0) {
          $tabCal .= "</tr><tr valign='top'>";
        }
        if ($month[$i - 1] == "12") {
          $month[$i] = "01";
          $year[$i] = $year[$i - 1] + 1;
        }
        else {
          $month[$i] = "0".(($month[$i - 1]) * 1) + 1;
          $year[$i] = $year[$i - 1];
        }
        $calendrier->modification($month[$i], $year[$i], $appartement->getId());
        $month1[$i] = $this->recupMois($month[$i]);

        $tabCal .= "<td><div class='calendarResa'>".$calendrier->affichage("Propriétaire")."</div></td>";
      }
      $tabCal .= '</tr></table>';
    }


    return $this->container->get('templating')->renderResponse('PatCompteBundle:Calendrier:calendrierByApp.html.twig', array(
        'message' => $message,
        'appartement' => $appartement,
        'mois' => $mois,
        'annee' => $annee,
        'tabCal' => $tabCal
    ));
  }

  // Fonction qui affiche le calendrier des disponibilité d'un bien sur une année et propose un formulaire de blocage/déblocage de dates.
  public function AjouterCalByAppAction($id_bien = null, $id_cal = null)
  {
    $message = '';

    $em = $this->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $arrayRoles = $user->getRoles(); //on test les droits
    if ($arrayRoles[0] == "ROLE_ADMIN" or $user->getTypeUtilisateur() != "2") {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    $appartement = $em->getRepository('PatCompteBundle:Appartement')->find($id_bien);
    if (!$appartement)
      throw new NotFoundHttpException("Bien non trouvé");

    if ($id_cal) {
      // Si l'on modifie un bloquage de date existant, on le récupère, crée le formulaire
      $calendrier = $em->getRepository('PatCompteBundle:Calendrier')->find(array('id' => $id_cal, 'utilisateur' => $user));

      if (!$calendrier)
        throw new NotFoundHttpException("Dates bloquées non trouvées");

      if ($calendrier->getUtilisateur() != $user)
        throw new AccessDeniedException('This user does not have access to this section.');

      $form = $this->container->get('form.factory')->create(new CalendrierForm(), $calendrier);
    }
    else {
      $calendrier = new Calendrier();
      $form = $this->container->get('form.factory')->create(new CalendrierForm(), $calendrier);
    }

    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST') {

      $form->bind($request);

      if ($form->isValid()) {
        $tableau = $request->request->get('calendrier');
        $date_debut = $tableau["date_debut"];
        $date_fin = $tableau["date_fin"];

        //passage de la date au format EN
        $date_debut_en = $this->toDateEn($date_debut);
        $date_fin_en = $this->toDateEn($date_fin);

        if (strtotime($date_debut_en) < time()) {
          $message = "La date de début doit être supérieure à la date du jour";
        }
        else if (strtotime($date_fin_en) <= strtotime($date_debut_en)) {
          $message = "La date de fin doit être supérieure à la date de début";
        }
        else {
          //on vérifie que la période n'est pas bloquée
          $query_cal = $em->createQuery("SELECT c FROM PatCompteBundle:Calendrier c
            WHERE c.appartement = :id_app
            AND c.id <> :id_cal
            AND (
                 (c.date_debut <= :date_debut AND c.date_fin > :date_debut)
              OR (c.date_debut >= :date_debut AND c.date_fin <= :date_fin)
              OR (c.date_debut < :date_fin AND c.date_fin >= :date_fin)
            )");

          $query_cal->setParameter('id_app', $id_bien);
          $query_cal->setParameter('date_debut', $date_debut_en);
          $query_cal->setParameter('date_fin', $date_fin_en);
          // Si on édite une plage de dates, on ne tient pas compte des anciennes dates, sinon on fixe id à 0 pour que la condition n'influe pas
          $query_cal->setParameter('id_cal', isset($id_cal) ? $id_cal : 0);

          $result_cal = $query_cal->getResult();
          //si l'appartement est bloqué
          if (count($result_cal) > 0) {
            $message = "L'appartement n'est pas disponible pour ces dates";
            // Si l'appartement n'est pas bloqué, on recherche s'il est réservé
          }
          else {
            $query_res = $em->createQuery("
              SELECT r FROM PatCompteBundle:Reservation r
              WHERE r.appartement = :id_app
              AND (
                   (r.date_debut <= :date_debut AND r.date_fin > :date_debut)
                OR (r.date_debut >= :date_debut AND r.date_fin <= :date_fin)
                OR (r.date_debut < :date_fin AND r.date_fin >= :date_fin)
              )
              AND r.status < :cancel");

            $query_res->setParameter('id_app', $id_bien);
            $query_res->setParameter('date_debut', $date_debut_en);
            $query_res->setParameter('date_fin', $date_fin_en);
            $query_res->setParameter('cancel', ReservationTools::CONSTANT_STATUS_CANCELED_BY_CA);

            $result_res = $query_res->getResult();

            //si l'appartement est réservé
            if (count($result_res) > 0) {
              $message = "L'appartement n'est pas disponible pour ces dates : bloqué par une réservation en ligne";
            }
            else {
              // si la période est libre, on enregistre
              if (!isset($id_cal)) {
                $calendrier->setUtilisateur($user);
                $calendrier->setAppartement($appartement);
                $calendrier->setStatus(CalendrierTools::CONSTANT_STATUS_BLOQUE_PROPRIETAIRE);
                $calendrier->setCreatedAt(new \DateTime("now"));
              }

              $this->sendLockingEmailToCA($user, $calendrier, $form);
              $this->sendLockingEmailToOwner($user, $calendrier, $form);

              $em->persist($calendrier);

              $em->flush();

              $this->container->get('session')->getFlashBag()->add('notice', "La période a été bloquée avec succès");
              return new RedirectResponse($this->container->get('router')->generate('pat_calendrier_affiche', array('id_bien' => $id_bien, 'annee' => date("Y"))));
            }
          }
        }
      }
    }//end if $request->getMethod()

    $dates_bloquees = $em->getRepository('PatCompteBundle:Calendrier')->findBy(
      array('appartement' => $appartement, 'utilisateur' => $user),
      array('date_debut' => 'DESC')
    );

    $this->container->get('session')->getFlashBag()->add('notice', $message);

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Calendrier:ajouterByApp.html.twig', array(
        'appartement' => $appartement,
        'form' => $form->createView(),
        'dates_bloquees' => $dates_bloquees
    ));
  }

  public function supprimerCalByAppAction($id_bien = null, $id_cal = null)
  {
    $em = $this->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $arrayRoles = $user->getRoles(); //on test les droits

    $cal = $em->getRepository('PatCompteBundle:Calendrier')->find($id_cal);
    if (!$cal) {
      throw new NotFoundHttpException("Dates bloquées non trouvées");
    }

    if ($cal->getUtilisateur() != $user) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    $this->sendUnlockingEmailToCA($user, $cal);
    $this->sendUnlockingEmailToOwner($user, $cal);

    $em->remove($cal);
    $em->flush();

    $this->container->get('session')->getFlashBag()->add('notice', "Les dates ont correctement été débloquées");

    return new RedirectResponse($this->container->get('router')->generate('pat_calendrier_affiche', array('id_bien' => $id_bien, 'annee' => date("Y"))));
  }

  public function recupMois($monthnb)
  {
    switch ($monthnb) {
      case 1: $month = 'Janvier';
        break;
      case 2: $month = 'F&eacute;vrier';
        break;
      case 3: $month = 'Mars';
        break;
      case 4: $month = 'Avril';
        break;
      case 5: $month = 'Mai';
        break;
      case 6: $month = 'Juin';
        break;
      case 7: $month = 'Juillet';
        break;
      case 8: $month = 'Ao&ucirc;t';
        break;
      case 9: $month = 'Septembre';
        break;
      case 10: $month = 'Octobre';
        break;
      case 11: $month = 'Novembre';
        break;
      case 12: $month = 'D&eacute;cembre';
        break;
    }
    return $month;
  }

//
//
////affiche le calendrier
//public function calendarDispo($moisC,$anneeC,$id_appart){
//
//$em = $this->get('doctrine')->getManager();
//
////Creating general vars
//$year = date("Y");
//if(empty($moisC)) $monthnb = date("n");
//else {
//    $monthnb = $moisC;
//    $year = $anneeC;
//    if($monthnb <= 0) {
//        $monthnb = 12;
//        $year = $year - 1;
//    }
//    elseif($monthnb > 12) {
//        $monthnb = 1;
//        $year = $year + 1;
//    }
//}
//$day = date("w");
//$nbdays = date("t", mktime(0,0,0,$monthnb,1,$year));
//$firstday = date("w",mktime(0,0,0,$monthnb,1,$year));
//
////Replace the number of the day by its french name
//$daytab[1] = 'Lu';
//$daytab[2] = 'Ma';
//$daytab[3] = 'Me';
//$daytab[4] = 'Je';
//$daytab[5] = 'Ve';
//$daytab[6] = 'Sa';
//$daytab[7] = 'Di';
//
////Build the calendar table
//$calendar = array();
//$z = (int)$firstday;
//if($z == 0) $z =7;
//for($i = 1; $i <= ($nbdays/5); $i++){
//    for($j = 1; $j <= 7 && $j-$z+1+(($i*7)-7) <= $nbdays; $j++){
//        if($j < $z && ($j-$z+1+(($i*7)-7)) <= 0){
//                $calendar[$i][$j] = null;
//        }
//        else {
//            $calendar[$i][$j] = $j-$z+1+(($i*7)-7);
//        }
//    }
//}
//
////Replace the number of the month by its french name
//switch($monthnb) {
//    case 1: $month = 'Janvier'; break;
//    case 2: $month = 'F&eacute;vrier'; break;
//    case 3: $month = 'Mars'; break;
//    case 4: $month = 'Avril'; break;
//    case 5: $month = 'Mai'; break;
//    case 6: $month = 'Juin'; break;
//    case 7: $month = 'Juillet'; break;
//    case 8: $month = 'Ao&ucirc;t'; break;
//    case 9: $month = 'Septembre';    break;
//    case 10: $month = 'Octobre'; break;
//    case 11:$month = 'Novembre';    break;
//    case 12:$month = 'D&eacute;cembre';    break;
//}
//
//$calendarDispo="";
//$calendarDispo.='
//
//<div class="calendrierDispo">
//    <table border="0" cellpadding="6" cellspacing="2" bgcolor="#E1E0E0">
//        <tr bgcolor="#ffffff">
//            <th colspan="7" align="center" class="headcal">'.($month.' '.$year).'</th>
//        </tr>
//        ';
//            $calendarDispo.='<tr bgcolor="#ffffff">';
//            for($i = 1; $i <= 7; $i++){
//                $calendarDispo.='<th class="jourCal" align="center">'.$daytab[$i].'</th>';
//            }
//            $calendarDispo.='</tr>';
//            for($i = 1; $i <= count($calendar); $i++) {
//                $calendarDispo.='<tr bgcolor="#ffffff">';
//                for($j = 1; $j <= 7 && $j-$z+1+(($i*7)-7) <= $nbdays; $j++){
//
//                	/*
//                    if($j-$z+1+(($i*7)-7) == date("j") && $monthnb == date("n") && $year == date("Y")){
//                   		$calendarDispo.='<th class="current"><font color="red">'.$calendar[$i][$j].'</font></th>';
//                    }
//                    */
//                   	//on ajoute un zéro devant les chiffre de 1 à 9
//                   	if(strlen($monthnb) == 1){ $monthnb = "0".$monthnb; }
//                   	if(strlen($calendar[$i][$j]) == 1){ $calendar[$i][$j] = "0".$calendar[$i][$j]; }
//
//                   	$dateResa = $year."-".$monthnb."-".$calendar[$i][$j];
//
////                    if($dateResa == "2009-01-11"){
////						echo $dateResa."<br/>";
////                    }
//						$calendrier = $em->getRepository('PatCompteBundle:Calendrier')->findBy(array('appartement' => $id_appart,'date' => $dateResa, 'statut' => "1"));
//
//
//                    if($calendrier) //on teste et affiche les dates de la BDD
//                    {
//                    	for($k=0; $k <sizeof($calendrier); $k++ ){
//	                    	if($calendrier[$k]->getReservation() != null and $calendrier[$k]->getStatut() == "1"){
//	                    		$calendarDispo.='<th style="background-color:#D22C2C;color:#FFFFFF" bgcolor="#D22C2C">'.$calendar[$i][$j].'</th>';
//	                    	}
//	                    	else if($calendrier[$k]->getStatut() == "1"){
//	                    		$calendarDispo.='<th style="background-color:#F0AE0E;color:#FFFFFF" bgcolor="#F0AE0E">'.$calendar[$i][$j].'</th>';
//	                   		}
//	                   		else{
//                   				$calendarDispo.='<th>'.$calendar[$i][$j].'</th>';
//	                   		}
//	                    	break;
//                    	}
//
//                    }
//                	else
//                	{
//                		$calendarDispo.='<th>'.$calendar[$i][$j].'</th>';
//                	}
//                }
//                $calendarDispo.='</tr>';
//            }
//            $calendarDispo.='
//    </table>
//
//</div>
//	';
//	return $calendarDispo;
//}
//
//
  //bloquer débloquer des dates
  public function ajouterAction($id_cal = null)
  {
    $message = '';
    $tableau = array();

    $em = $this->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    if (!$user)
      throw new AccessDeniedException('This user does not have access to this section.');

    $arrayRoles = $user->getRoles(); //on test les droits
    if ($arrayRoles[0] == "ROLE_ADMIN" or $user->getTypeUtilisateur() != "2")
      throw new AccessDeniedException('This user does not have access to this section.');


    if ($id_cal) {
      // Si l'on modifie un bloquage de date existant, on le récupère, crée le formulaire puis transpose les dates au format français
      $calendrier = $em->getRepository('PatCompteBundle:Calendrier')->find($id_cal);

      if ($calendrier->getUtilisateur() != $user)
        throw new AccessDeniedException("Vous navez pas accès à ces dates.");

      $form = $this->container->get('form.factory')->create(new CalendrierAjouterForm($user->getId()), $calendrier);
    }
    else {
      $calendrier = new Calendrier();
      $form = $this->container->get('form.factory')->create(new CalendrierAjouterForm($user->getId()), $calendrier);
    }

    //on récupère les résas	s'il y a au moins 1 appartement
    $test_app = $em->getRepository('PatCompteBundle:Appartement')->searchAppartement(null, null, null, null, null, null, null, null, "recup_nombre", $user->getId());
    if ($test_app && $test_app[0]['nombre'] < 1) {
      return $this->container->get('templating')->renderResponse('PatCompteBundle:Calendrier:ajouter.html.twig', array(
          'message' => "",
          'tableau' => "",
          'form' => ""
      ));
    }

    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {

        $tableau = $request->request->get('calendrierajouter');

        $date_debut = $tableau["date_debut"];
        $date_fin = $tableau["date_fin"];
        $id_appartement = $tableau["Appartement"];

        //passage de la date au format EN
        $date_debut_en = $this->toDateEn($date_debut);
        $date_fin_en = $this->toDateEn($date_fin);

        if (strtotime($date_debut_en) < time()) {
          $message = "La date de début doit être supérieure à la date du jour";
        }
        else if (strtotime($date_fin_en) <= strtotime($date_debut_en)) {
          $message = "La date de fin doit être supérieure à la date de début";
        }
        else {
          //on vérifie que la période n'est pas bloquée
          $query_cal = $em->createQuery("
            SELECT c FROM PatCompteBundle:Calendrier c
            WHERE c.appartement = :id_app
              AND c.id <> :id_cal
              AND (
                   (c.date_debut <= :date_debut AND c.date_fin > :date_debut)
                OR (c.date_debut >= :date_debut AND c.date_fin <= :date_fin)
                OR (c.date_debut < :date_fin AND c.date_fin >= :date_fin)
              )
              AND c.status <> :menage
          ");

          $query_cal->setParameter('id_app', $id_appartement);
          $query_cal->setParameter('date_debut', $date_debut_en);
          $query_cal->setParameter('date_fin', $date_fin_en);
          // Si on édite une plage de dates, on ne tient pas compte des anciennes dates, sinon on fixe id à 0 pour que la condition n'influe pas
          $query_cal->setParameter('id_cal', isset($id_cal) ? $id_cal : 0);
          $query_cal->setParameter('menage', CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE); // On ignore les dates bloquées pour ménage

          $result_cal = $query_cal->getResult();
          //si l'appartement est bloqué
          if (count($result_cal) > 0)
            $message = "L'appartement n'est pas disponible pour ces dates";
          // Si l'appartement n'est pas bloqué, on recherche s'il est réservé
          else {
            $query_res = $em->createQuery("
              SELECT r FROM PatCompteBundle:Reservation r
              WHERE r.appartement = :id_app
                AND (
                     (r.date_debut <= :date_debut AND r.date_fin > :date_debut)
                  OR (r.date_debut >= :date_debut AND r.date_fin <= :date_fin)
                  OR (r.date_debut < :date_fin AND r.date_fin >= :date_fin)
                )
            ");

            $query_res->setParameter('id_app', $id_appartement);
            $query_res->setParameter('date_debut', $date_debut_en);
            $query_res->setParameter('date_fin', $date_fin_en);

            $result_res = $query_res->getResult();

            //si l'appartement est réservé
            if (count($result_res) > 0) {
              $message = "L'appartement n'est pas disponible pour ces dates : bloqué par une réservation en ligne";
            }
            else { // si la période est libre, on enregistre
              if (!isset($id_cal)) {
                $calendrier->setUtilisateur($user);
                $calendrier->setCreatedAt(new \DateTime("now"));
                $calendrier->setStatus(CalendrierTools::CONSTANT_STATUS_BLOQUE_PROPRIETAIRE);
              }

              $this->sendLockingEmailToCA($user, $calendrier, $form);
              $this->sendLockingEmailToOwner($user, $calendrier, $form);

              $em->persist($calendrier);
              $em->flush();

              $this->container->get('session')->getFlashBag()->add('notice', "La période a été bloquée avec succès");
              return new RedirectResponse($this->container->get('router')->generate('pat_calendrier_ajouter'));
            }
          } //end if date non ok
        } //end if $request->getMethod()
      }
    }

    $dates_bloquees = $em->getRepository('PatCompteBundle:Calendrier')->findBy(
      array('utilisateur' => $user), array('date_debut' => 'DESC')
    );

    $this->container->get('session')->getFlashBag()->add('notice', $message);

    return $this->render('PatCompteBundle:Calendrier:ajouter.html.twig', array(
        'tableau' => $tableau,
        'form' => $form->createView(),
        'dates_bloquees' => $dates_bloquees
    ));
  }

  /**
   * Envoie un courriel d'information à ClassAppart, sur blocage de date(s).
   *
   * @param Pat\UtilisateurBundle\Entity\Utilisateur $user
   * @param Pat\CompteBundle\Entity\Calendrier $calendar
   * @param Pat\CompteBundle\Form\CalendrierAjouterForm $form
   * @author refactoring AG
   */
  protected function sendLockingEmailToCA($user, $calendar, $form)
  {
    $subject = 'Blocage de date(s) - ClassAppart ®';
    $params = array(
      'user' => $user,
      'calendrier' => $calendar
    );

    if ($form->get('recontacter')->getData()) {
      $subject = 'Demande de contact pour bloquage de date - ClassAppart ®';
      $params['commentaire'] = $form->get('commentaire')->getData();
      $params['recontacter'] = true;
    }

    $emailMessage = \Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom('contact@class-appart.com')
      ->setTo('info@class-appart.com');

    $textBody = $this->renderView(
      'PatCompteBundle:Calendrier:mail_contact_bloquage_admin.txt.twig', $params
    );
    $htmlBody = $this->renderView(
      'PatCompteBundle:Calendrier:mail_contact_bloquage_admin.html.twig', $params
    );

    if (!empty($htmlBody)) {
      $emailMessage
        ->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain');
    }
    else {
      $emailMessage->setBody($textBody);
    }

    $this->get('mailer')->send($emailMessage);
  }

  /**
   * Envoie un courriel d'information au propriétaire, sur blocage de date(s).
   *
   * @param Pat\UtilisateurBundle\Entity\Utilisateur $user
   * @param Pat\CompteBundle\Entity\Calendrier $calendar
   * @param Pat\CompteBundle\Form\CalendrierAjouterForm $form
   * @author AG
   */
  protected function sendLockingEmailToOwner($user, $calendar, $form)
  {
    $subject = 'Blocage de date(s) - ClassAppart ®';
    $params = array(
      'user' => $user,
      'calendrier' => $calendar
    );

    if ($form->get('recontacter')->getData()) {
      $subject = 'Demande de contact pour bloquage de date - ClassAppart ®';
      $params['commentaire'] = $form->get('commentaire')->getData();
      $params['recontacter'] = true;
    }

    $emailMessage = \Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom('contact@class-appart.com')
      ->setTo($user->getEmail());

    $secondEmail = $user->getSecondEmail();
    if ($secondEmail) {
      $emailMessage->setCc($secondEmail);
    }

    $htmlBody = $this->renderView(
      'PatCompteBundle:Calendrier:owner_locking_email.html.twig', $params
    );
    $emailMessage->setBody($htmlBody, 'text/html');

    $this->get('mailer')->send($emailMessage);
  }

  public function supprimerAction($id_cal = null)
  {
    $em = $this->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $arrayRoles = $user->getRoles(); //on test les droits

    $cal = $em->getRepository('PatCompteBundle:Calendrier')->find($id_cal);
    if (!$cal) {
      throw new NotFoundHttpException("Dates bloquées non trouvées");
    }

    if ($cal->getUtilisateur() != $user) {
      throw new AccessDeniedException("Vous navez pas accès à ces dates.");
    }

    $this->sendUnlockingEmailToCA($user, $cal);
    $this->sendUnlockingEmailToOwner($user, $cal);

    $em->remove($cal);
    $em->flush();

    $this->container->get('session')->getFlashBag()->add('notice', "Les dates ont correctement été débloquées");

    return new RedirectResponse($this->container->get('router')->generate('pat_calendrier_ajouter'));
  }

  /**
   * Envoie un courriel d'information à ClassAppart, sur déblocage de date(s).
   *
   * @param Pat\UtilisateurBundle\Entity\Utilisateur $user
   * @param Pat\CompteBundle\Entity\Calendrier $calendar
   * @author AG
   */
  protected function sendUnlockingEmailToCA($user, $calendar)
  {
    $subject = 'Déblocage de date(s) - ClassAppart ®';
    $params = array(
      'user' => $user,
      'calendrier' => $calendar
    );

    $emailMessage = \Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom('contact@class-appart.com')
      ->setTo('info@class-appart.com');

    $htmlBody = $this->renderView(
      'PatCompteBundle:Calendrier:ca_unlocking_email.html.twig', $params
    );
    $emailMessage->setBody($htmlBody, 'text/html');

    $this->get('mailer')->send($emailMessage);
  }

  /**
   * Envoie un courriel d'information au propriétaire, sur déblocage de date(s).
   *
   * @param Pat\UtilisateurBundle\Entity\Utilisateur $user
   * @param Pat\CompteBundle\Entity\Calendrier $calendar
   * @author AG
   */
  protected function sendUnlockingEmailToOwner($user, $calendar)
  {
    $subject = 'Déblocage de date(s) - ClassAppart ®';
    $params = array(
      'user' => $user,
      'calendrier' => $calendar
    );

    $emailMessage = \Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom('contact@class-appart.com')
      ->setTo($user->getEmail());

    $secondEmail = $user->getSecondEmail();
    if ($secondEmail) {
      $emailMessage->setCc($secondEmail);
    }

    $htmlBody = $this->renderView(
      'PatCompteBundle:Calendrier:owner_unlocking_email.html.twig', $params
    );
    $emailMessage->setBody($htmlBody, 'text/html');

    $this->get('mailer')->send($emailMessage);
  }

  //passer les dates au format EN
  public function toDateEn($date)
  {
    return substr($date, 6, 4)."-".substr($date, 3, 2)."-".substr($date, 0, 2);
  }

  //passer les dates au format FR
  public function toDateFR($date)
  {
    return substr($date, 8, 2)."/".substr($date, 5, 2)."/".substr($date, 0, 4);
  }

  //calcul le nombre de nuits entre 2 date
  public function calculNbNuits($date_arrivee1, $date_depart1)
  {
    $date_arrivee1 = $this->toDateEn($date_arrivee1);
    $date_arrivee1 = str_replace("/", "", $date_arrivee1);

    $date_depart1 = $this->toDateEn($date_depart1);
    $date_depart1 = str_replace("/", "", $date_depart1);

    $nbjours = abs(round((strtotime($date_arrivee1) - strtotime($date_depart1)) / (60 * 60 * 24) - 1));
    $nbnuits = $nbjours - 1;

    return $nbnuits;
  }

}
