<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Pat\CompteBundle\Entity\Option;
use Pat\CompteBundle\Entity\Reservation;
use Pat\CompteBundle\Entity\Payment;
use Pat\CompteBundle\Form\OptionType;
use Pat\CompteBundle\Form\PaymentForm;

/**
 * Option controller.
 *
 */
class OptionController extends Controller
{

  /**
   * Lists all Option entities from a reservation.
   *
   */
  public function indexAction(Reservation $reservation_id)
  {
    $em = $this->container->get('doctrine')->getManager();

    $entities = $em->getRepository('PatCompteBundle:Option')->findByReservation($reservation_id);

    return $this->render('PatCompteBundle:Option:index.html.twig', array(
        'entities' => $entities,
    ));
  }

  /**
   * Creates a new Option entity.
   *
   */
  public function createAction(Request $request)
  {
    $em = $this->container->get('doctrine')->getManager();
    $reservation = $em->getRepository('PatCompteBundle:Reservation')->find($request->get('reservation_id'));

    if (!$reservation)
      throw $this->createNotFoundException('Réservation introuvable');

    $entity = new Option();

    $form = $this->createForm(new OptionType(), $entity, array('reservation_id' => $request->get('reservation_id')));
    $form->bind($request);

    if ($form->isValid()) {
      $entity->setReservation($reservation);
      $em->persist($entity);
      $em->flush();

      $this->get('session')->getFlashBag()->add(
        'success', "L'option a correctement été ajoutée."
      );

//            return $this->redirect($this->generateUrl('option_success', array('id' => $entity->getId())));

      return $this->render('PatCompteBundle:Option:success.html.twig', array(
          'option' => $entity,
      ));
    }

    return $this->render('PatCompteBundle:Option:new.html.twig', array(
        'option' => $entity,
        'form' => $form->createView(),
    ));
  }

  /**
   * Displays a form to create a new Option entity.
   *
   */
  public function newAction(Request $request, $reservation_id = null)
  {
    $em = $this->container->get('doctrine')->getManager();
    $reservation = $em->getRepository('PatCompteBundle:Reservation')->find($reservation_id);

    if (!$reservation)
      throw $this->createNotFoundException('Réservation introuvable');

    $entity = new Option();
    $entity->setReservation($reservation);

    $form = $this->createForm(new OptionType(), $entity);

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {
        $em->persist($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
          'success', "L'option a correctement été ajoutée."
        );

        $this->get('pat.reservation_manager')->updateStatus($reservation);

//              return $this->redirect($this->generateUrl('option_success', array('id' => $entity->getId())));
        return $this->render('PatCompteBundle:Option:success.html.twig', array(
            'option' => $entity,
        ));
      }
    }

    return $this->render('PatCompteBundle:Option:new.html.twig', array(
        'option' => $entity,
        'form' => $form->createView(),
    ));
  }

  /**
   * Finds and displays a Option entity.
   *
   */
//    public function showAction($id)
//    {
//        $em = $this->container->get('doctrine')->getManager();
//
//        $entity = $em->getRepository('PatCompteBundle:Option')->find($id);
//
//        if (!$entity) {
//            throw $this->createNotFoundException('Unable to find Option entity.');
//        }
//
//        $deleteForm = $this->createDeleteForm($id);
//
//        return $this->render('PatCompteBundle:Option:show.html.twig', array(
//            'entity'      => $entity,
//            'delete_form' => $deleteForm->createView(),        ));
//    }

  /**
   * Displays a form to edit an existing Option entity.
   *
   */
  public function editAction(Request $request, $id)
  {
    $em = $this->container->get('doctrine')->getManager();

    $entity = $em->getRepository('PatCompteBundle:Option')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find Option entity.');
    }

    $editForm = $this->createForm(new OptionType(), $entity);
//        $deleteForm = $this->createDeleteForm($id);

    if ($request->getMethod() == 'POST') {
      $editForm->bind($request);

      if ($editForm->isValid()) {
        $em->persist($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
          'success', "L'option a correctement été modifiée."
        );

        $reservation = $entity->getReservation();
        $this->get('pat.reservation_manager')->updateStatus($reservation);

        //            return $this->redirect($this->generateUrl('option_edit', array('id' => $id)));
        return $this->redirect($this->generateUrl('pat_admin_reservation_show', array('id_reservation' => $reservation->getId())));
      }
    }

    return $this->render('PatCompteBundle:Option:new.html.twig', array(
        'option' => $entity,
        'form' => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
    ));
  }

  /**
   * Edits an existing Option entity.
   *
   */
  public function updateAction(Request $request, $id)
  {
    $em = $this->container->get('doctrine')->getManager();

    $entity = $em->getRepository('PatCompteBundle:Option')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find Option entity.');
    }

    $deleteForm = $this->createDeleteForm($id);
    $editForm = $this->createForm(new OptionType(), $entity);
    $editForm->bind($request);

    if ($request->getMethod() == 'POST') {
      if ($editForm->isValid()) {
        $em->persist($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
          'success', "L'option a correctement été modifiée."
        );

        $reservation = $entity->getReservation();
        $this->get('pat.reservation_manager')->updateStatus($reservation);

        //            return $this->redirect($this->generateUrl('option_edit', array('id' => $id)));
        return $this->redirect($this->generateUrl('pat_admin_reservation_show', array('id_reservation' => $reservation->getId())));
      }
    }

    return $this->render('PatCompteBundle:Option:new.html.twig', array(
        'option' => $entity,
        'form' => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
    ));
  }

  /**
   * Deletes a Option entity.
   *
   */
  public function deleteAction(Request $request, $id)
  {
//        $form = $this->createDeleteForm($id);
//        $form->bind($request);
//        if ($form->isValid()) {
    $em = $this->container->get('doctrine')->getManager();
    $entity = $em->getRepository('PatCompteBundle:Option')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find Option entity.');
    }
    $reservation = $entity->getReservation();

    $em->remove($entity);
    $em->flush();

    $this->get('pat.reservation_manager')->updateStatus($reservation);

    $this->get('session')->getFlashBag()->add(
      'success', "L'option a correctement été supprimée."
    );
//        }
//        return $this->redirect($this->generateUrl('option'));
    return $this->redirect($this->generateUrl('pat_admin_reservation_show', array('id_reservation' => $reservation->getId())));
  }

  /**
   * Creates a form to delete a Option entity by id.
   *
   * @param mixed $id The entity id
   *
   * @return Symfony\Component\Form\Form The form
   */
  private function createDeleteForm($id)
  {
    return $this->createFormBuilder(array('id' => $id))
        ->add('id', 'hidden')
        ->getForm()
    ;
  }

}
