<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;

class AdminController extends ContainerAware
{

  public function indexAction($message = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());


    $nb_prop = $em->createQueryBuilder()
      ->from('\Pat\UtilisateurBundle\Entity\Utilisateur', 'u')
      ->select('COUNT(u)')
      ->where('u.type_utilisateur = 2')
      ->getQuery()
      ->getSingleScalarResult();

    $nb_biens = $em->createQueryBuilder()
      ->from('\Pat\CompteBundle\Entity\Appartement', 'a')
      ->select('COUNT(a)')
      ->getQuery()
      ->getSingleScalarResult();

    $nb_resas = $em->createQueryBuilder()
      ->from('\Pat\CompteBundle\Entity\Reservation', 'r')
      ->select('COUNT(r)')
      ->getQuery()
      ->getSingleScalarResult();


    return $this->container->get('templating')->renderResponse('PatCompteBundle:Admin:index.html.twig', array('nb_prop' => $nb_prop,
        'nb_biens' => $nb_biens,
        'nb_resas' => $nb_resas));
  }

  //après la connexion, dispatch vers la bonne page en fonction du type d'utilisateur
  public function dispatchAction(Request $request)
  {
    $em = $this->container->get('doctrine')->getManager();

    $session = $request->getSession();
    $path_spec = $session->get('path_spec');


    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    if ($user->getType() == "Administrateur") {
      return new RedirectResponse($this->container->get('router')->generate('pat_admin_dashboard'));
    }
    elseif ($user->getType() == "Propriétaire") {
      if ($path_spec != "") // si on a spécifié un chemin particulier
        return new RedirectResponse($this->container->get('router')->generate($path_spec));
      else
        return new RedirectResponse($this->container->get('router')->generate('pat_appartement_index'));
    }
    else {
      if ($path_spec != "") // si on a spécifié un chemin particulier pour le locataire
        return new RedirectResponse($this->container->get('router')->generate($path_spec));
      else
        return new RedirectResponse($this->container->get('router')->generate('pat_loc_reservation_index'));
    }
  }

  //affiche le menu principal en haut de page
  public function menuAction()
  {
    return $this->container->get('templating')->renderResponse('PatCompteBundle:Admin:menu.html.twig');
  }

  public function planningAction($date = null)
  {

    // Si la date n'est pas renseignée, on prends le mois actuel
    if (is_null($date))
      $date = date("m-Y");

    // On calcul le nombre de jours dans le mois en cours
    $date_explode = explode('-', $date);
    $nb_jours = cal_days_in_month(CAL_GREGORIAN, $date_explode[0], $date_explode[1]);

    $planning = $this->container->get('pat.planning')->getPlanning();

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Admin:planning.html.twig', array(
        'planning' => $planning,
        'nb_jours' => $nb_jours,
        'date' => mktime(0, 0, 0, $date_explode[0], 1, $date_explode[1]),
    ));
  }

  public function planningAjaxAction(Request $request)
  {
    if ($request->isXmlHttpRequest()) {
      $date = $request->request->get('date');

      // Si la date n'est pas renseignée, on prends le mois actuel
      if (is_null($date))
        $date = date("m-Y");

      // On calcul le nombre de jours dans le mois en cours
      $date_explode = explode('-', $date);
      $nb_jours = cal_days_in_month(CAL_GREGORIAN, $date_explode[0], $date_explode[1]);

      $planning = $this->container->get('pat.planning')->getPlanning($date);

      return $this->container->get('templating')->renderResponse('PatCompteBundle:Admin:_planning_month.html.twig', array(
          'planning' => $planning,
          'nb_jours' => $nb_jours,
          'date' => mktime(0, 0, 0, $date_explode[0], 1, $date_explode[1]),
      ));
    }
  }

}
