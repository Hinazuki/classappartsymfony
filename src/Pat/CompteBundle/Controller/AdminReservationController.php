<?php

namespace Pat\CompteBundle\Controller;

use Pat\CompteBundle\Entity\Facture;
use Pat\CompteBundle\Entity\Option;
use Pat\CompteBundle\Tools\FactureTools;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\CompteBundle\Entity\Reservation;
use Pat\CompteBundle\Entity\Payment;
use Pat\CompteBundle\Lib\ReservationInformationsLocataire;
use Pat\CompteBundle\Form\AdminReservationRechercheAppartementForm;
use Pat\CompteBundle\Form\AdminLocataireRechercheForm;
use Pat\CompteBundle\Form\AdminReservationForm;
use Pat\CompteBundle\Form\AdminReservationRechercheForm;
use Pat\CompteBundle\Form\AdminReservationRechercheAvanceeForm;
use Pat\CompteBundle\Form\AdminReservationInscriptionLocataireForm;
use Pat\CompteBundle\Form\AdminReservationInformationsLocataireForm;
use Pat\CompteBundle\Repository\ReservationRepository;
use Pat\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;
use Pat\CompteBundle\Entity\Calendrier;
use Pat\CompteBundle\Tools\ReservationTools;
use Pat\CompteBundle\Tools\PaymentTools;
use Pat\CompteBundle\Lib\PDFConverter;
use Pat\CompteBundle\Form\TextOptionsForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\File\File;

class AdminReservationController extends ContainerAware
{

  public function testTabVide($tab)
  {
    foreach ($tab as $t):
      if ($t != null && $t != "") {
        return false;
      }
    endforeach;
    return true;
  }

  //lister les réservations
  public function indexAction()
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $session = $this->container->get('request')->getSession();

    $recherche_simple = false;

    $formRecherche = $this->container->get('form.factory')->create(new AdminReservationRechercheForm());
    $formRechercheAvancee = $this->container->get('form.factory')->create(new AdminReservationRechercheAvanceeForm());

    $request = $this->container->get('request');

    // Si requête POST, alors c'est une nouvelle recherche, on stock les valeurs en session si le formulaire est valide
    if ($request->getMethod() == 'POST') {

      $formRecherche->bind($request);
      $formRechercheAvancee->bind($request);

      if ($formRecherche->isValid()) {
        $reservationrecherche = $request->request->get('ReservationRecherche');

        $admin_search_ref = array();
        $admin_search_ref['ref_resa'] = str_replace("'", " ", $reservationrecherche["reference"]);
        $admin_search_ref['ref_bien'] = str_replace("'", " ", $reservationrecherche["reference_appart"]);
        $admin_search_ref['ref_resident'] = "";
        $admin_search_ref['prenom_resident'] = "";
        $admin_search_ref['nom_resident'] = "";
        $admin_search_ref['date_debut'] = $this->container->get('pat.date_functions')->toDateEn($reservationrecherche["date_debut"]);
        $admin_search_ref['date_fin'] = $this->container->get('pat.date_functions')->toDateEn($reservationrecherche["date_fin"]);
        $admin_search_ref['only_between'] = false;

        $session->set('admin_search_ref', $admin_search_ref);
      }
      elseif ($formRechercheAvancee->isValid()) {
        $message = "";
        $reservationrechercheavancee = $request->request->get('ReservationRechercheAvancee');

        $admin_search_ref = array();
        $admin_search_ref['ref_resa'] = str_replace("'", " ", $reservationrechercheavancee["reference_resa"]);
        $admin_search_ref['ref_bien'] = str_replace("'", " ", $reservationrechercheavancee["reference_appart"]);
        $admin_search_ref['ref_resident'] = str_replace("'", " ", $reservationrechercheavancee["reference_locataire"]);
        $admin_search_ref['prenom_resident'] = str_replace("'", " ", $reservationrechercheavancee["prenom_locataire"]);
        $admin_search_ref['nom_resident'] = str_replace("'", " ", $reservationrechercheavancee["nom_locataire"]);
        $admin_search_ref['date_debut'] = $this->container->get('pat.date_functions')->toDateEn($reservationrechercheavancee["date_debut"]);
        $admin_search_ref['date_fin'] = $this->container->get('pat.date_functions')->toDateEn($reservationrechercheavancee["date_fin"]);
        $admin_search_ref['only_between'] = isset($reservationrechercheavancee['only_between']) ? $reservationrechercheavancee['only_between'] : false;

        $session->set('admin_search_ref', $admin_search_ref);
      }
      else
        $pagination = array();
    }
    else {
      if ($this->container->get('request')->query->get('page')) {
        // On récupère les éléments de recherche en session
        $admin_search_ref = $session->get('admin_search_ref');
      }
      else {
        // Si page n'est pas définie, on souhaite afficher toutes les réservations non payées
        $admin_search_ref = array();
        $admin_search_ref['ref_resa'] = "";
        $admin_search_ref['ref_bien'] = "";
        $admin_search_ref['ref_resident'] = "";
        $admin_search_ref['prenom_resident'] = "";
        $admin_search_ref['nom_resident'] = "";
        $admin_search_ref['date_debut'] = "";
        $admin_search_ref['date_fin'] = "";
        $admin_search_ref['only_between'] = false;

        $session->set('admin_search_ref', $admin_search_ref);
      }
    }

    $query = $em->getRepository('PatCompteBundle:Reservation')->searchReservation($admin_search_ref['ref_resa'], $admin_search_ref['ref_bien'], $admin_search_ref['ref_resident'], $admin_search_ref['prenom_resident'], $admin_search_ref['nom_resident'], $admin_search_ref['date_debut'], $admin_search_ref['date_fin'], $admin_search_ref['only_between']);

    $paginator = $this->container->get('knp_paginator');
    $pagination = $paginator->paginate(
      $query, $this->container->get('request')->query->get('page', 1), 20
    );

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:liste.html.twig', array('reservation' => $pagination,
        'formRecherche' => $formRecherche->createView(),
        'formRechercheAvancee' => $formRechercheAvancee->createView(),
        "isSearchResa" => $admin_search_ref['ref_resa'])
    );
  }

  //détail de la réservation
  public function detailAction(Request $request, $id_reservation)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère les résas
    $reservation = $em->find('PatCompteBundle:Reservation', $id_reservation);
    if (!$reservation) {
      throw new NotFoundHttpException("Réservation non trouvée");
    }

    $nb_nuits = $this->container->get('pat.date_functions')->nbJours($reservation->getDateDebut(), $reservation->getDateFin());

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:detail.html.twig', array(
        'resa' => $reservation,
        'nb_nuits' => $nb_nuits,
        'backward_url' => $request->headers->get('referer')
    ));
  }

  //Rechercher un appartement
  public function rechercherAction($message = null)
  {

    $formRecherche = $this->container->get('form.factory')->create(new AdminReservationRechercheForm());
    $request = $this->container->get('request');
    if ($request->getMethod() == 'POST') {
      $formRecherche->bind($request); //on conserve les données dans le formulaire
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:rechercher.html.twig', array('message' => $message, 'formRecherche' => $formRecherche->createView())
    );
  }

  public function desactiverAction($id_reservation)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    // modification d'un appartement existant : on recherche ses données en vérifiant que l'utilisateur à le droit me modifier l'appart
    $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneBy(array('id' => $id_reservation));

    if (!$reservation)
      throw new NotFoundHttpException("Réservation non trouvée");

    if ($reservation->getIsAnnule() == '1')
      return new RedirectResponse($this->container->get('router')->generate('pat_admin_reservation_index'));

    // On Change le statut de la réservation
    $reservation->setIsAnnule(1);
    $em->flush();

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_reservation_index'));
  }

  public function isDispo($resa)
  {
    $em = $this->container->get('doctrine')->getManager();

    return $em->getRepository('PatCompteBundle:Appartement')->isAvailableForResa($resa->getAppartement(), $resa->getDateDebut(), $resa->getDateFin(), $resa->getId());
  }

  public function editAction(Request $request, $id_reservation)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());


    $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneBy(array('id' => $id_reservation));

    if (!$reservation) {
      throw new NotFoundHttpException('Réservation non trouvée');
    }

    $old_status = (int) $reservation->getStatus();

    $form = $this->container->get('form.factory')->create(new AdminReservationForm(), $reservation);

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {
        $emailCustomer = $form->get('email_customer')->getData() == 1;
        $emailOwner = $form->get('email_owner')->getData() == 1;

        // On ne modifie la réservation que si les dates sont disponibles, ou si on l'annule
        if ($this->isDispo($reservation) ||
          $reservation->getStatus() == ReservationTools::CONSTANT_STATUS_CANCELED_BY_BANK ||
          $reservation->getStatus() == ReservationTools::CONSTANT_STATUS_CANCELED_BY_CA ||
          $reservation->getStatus() == ReservationTools::CONSTANT_STATUS_CANCELED_BY_PROP ||
          $reservation->getStatus() == ReservationTools::CONSTANT_STATUS_CANCELED_BY_LOC) {
          // On stock le fait que l'admin ai modifié la réservation
          $reservation->setUpdatedByAdmin(true);

          // Si l'on modifie la réservation à "Acompte versé"
          if ($reservation->getStatus() == ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED) {
            // On set à null la validation du propriétaire
            $reservation->setValidProp(null);

            if ($emailOwner) {
              $message_email = \Swift_Message::newInstance()
                ->setSubject('Confirmation de réservation Class Appart')
                ->setFrom('contact@class-appart.com')
                ->setBcc($this->container->getParameter('mail_admin'))
                ->setTo($reservation->getAppartement()->getUtilisateur()->getEmail());

              if ($reservation->getAppartement()->getUtilisateur()->getSecondEmail()) {
                $message_email->setCc(
                  $reservation->getAppartement()->getUtilisateur()->getSecondEmail()
                );
              }

              $otherSiteTextTab= array( 3 => "Cette réservation a été effectuée via AirBnB, le client aura donc jusqu'a 5 jours avant la date de début de réservation pour l'annuler tout en bénéficiant d'un remboursement intégral", 
              4 => "Cette réservation a été effectuée via Booking , le client aura donc jusqu'a 30 jours avant la date de début de réservation pour l'annuler en bénéficiant d'un remboursement intégral", 
              5 => "Cette réservation a été effectuée via Abritel, le client aura donc jusqu'a 30 jours avant la date de début de réservation pour l'annuler en bénéficiant d'un remboursement intégral 
              et jusqu'à 14 jours avant la date de début de réservation pour l'annuler en bénéficiant d'un remboursement de l'acompte qu'il aura versés (50% du montant total de la révervation)");
        
              $textBody = $this->container->get('templating')->render('PatCompteBundle:Reservation:email_valid_prop.txt.twig', array('reservation' => $reservation, 'tabOtherSite' => $otherSiteTextTab));
              $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Reservation:email_valid_prop.html.twig', array('reservation' => $reservation, 'tabOtherSite' => $otherSiteTextTab));

              if (!empty($htmlBody)) {
                $message_email->setBody($htmlBody, 'text/html')
                  ->addPart($textBody, 'text/plain');
              }
              else {
                $message_email->setBody($textBody);
              }

              $this->container->get('mailer')->send($message_email);
            }
          }

          // Sauvegarde, à priori.
          $em->flush();

          
          // Si l'on vient de modifier un statut à 'validé'
          if ($reservation->getStatus() == ReservationTools::CONSTANT_STATUS_PAYED && (
            $old_status == ReservationTools::CONSTANT_STATUS_WAIT ||
            $old_status == ReservationTools::CONSTANT_STATUS_WAIT_VALIDATION)) {
            // On envoi un mail à la validation des paiement par chèque ou virement
            //if($reservation->getModePaiement() == "cheque" || $reservation->getModePaiement() == "virement") {
            if ($emailCustomer) {
              $message_email = \Swift_Message::newInstance()
                ->setSubject('Le paiement de votre réservation '.$reservation->getReference().' chez Class Appart est arrivé')
                ->setFrom('contact@class-appart.com')
                ->setBcc($this->container->getParameter('mail_admin'))
                ->setTo($reservation->getUtilisateur()->getEmail());

              if ($reservation->getUtilisateur()->getSecondEmail()) {
                $message_email->setCc($reservation->getUtilisateur()->getSecondEmail());
              }

              $textBody = $this->container->get('templating')->render(
                'PatFrontBundle:Reservation:mail_confirmation_cheque-virement.txt.twig', array('reservation' => $reservation)
              );
              $htmlBody = $this->container->get('templating')->render(
                'PatFrontBundle:Reservation:mail_confirmation_cheque-virement.html.twig', array('reservation' => $reservation)
              );

              if (!empty($htmlBody)) {
                $message_email->setBody($htmlBody, 'text/html')
                  ->addPart($textBody, 'text/plain');
              }
              else {
                $message_email->setBody($textBody);
              }

              $this->container->get('mailer')->send($message_email);
            }
            //}
          }
          // Si l'on vient d'annuler une réservation
          if ($reservation->getStatus() == ReservationTools::CONSTANT_STATUS_CANCELED_BY_CA &&
            $old_status != ReservationTools::CONSTANT_STATUS_CANCELED_BY_CA) {

            if ($emailCustomer) {
              $message_email = \Swift_Message::newInstance()
                ->setSubject('Reservation  '.$reservation->getReference().' annulée chez Class Appart')
                ->setFrom('contact@class-appart.com')
                ->setBcc($this->container->getParameter('mail_admin'))
                ->setTo($reservation->getUtilisateur()->getEmail());

//                        if ($reservation->getUtilisateur()->getMailsSecond()) {
//                            $message_email->setCc(explode(',', $reservation->getUtilisateur()->getMailsSecond()));
//                        }
              if ($reservation->getUtilisateur()->getSecondEmail()) {
                $message_email->setCc($reservation->getUtilisateur()->getSecondEmail());
              }

              $textBody = $this->container->get('templating')->render(
                'PatFrontBundle:Reservation:mail_annulation.txt.twig', array('reservation' => $reservation)
              );
              $htmlBody = $this->container->get('templating')->render(
                'PatFrontBundle:Reservation:mail_annulation.html.twig', array('reservation' => $reservation)
              );

              if (!empty($htmlBody)) {
                $message_email->setBody($htmlBody, 'text/html')
                  ->addPart($textBody, 'text/plain');
              }
              else {
                $message_email->setBody($textBody);
              }

              $this->container->get('mailer')->send($message_email);
            }

            if ($emailOwner) {
              $message_email_proprio = \Swift_Message::newInstance()
                ->setSubject('Reservation  '.$reservation->getReference().' annulée chez Class Appart')
                ->setFrom('contact@class-appart.com')
                ->setBcc($this->container->getParameter('mail_admin'))
                ->setTo($reservation->getAppartement()->getUtilisateur()->getEmail());

//                        if ($reservation->getAppartement()->getUtilisateur()->getMailsSecond()) {
//                            $message_email_proprio->setCc(explode(',',$reservation->getAppartement()->getUtilisateur()->getMailsSecond()));
//                        }
              if ($reservation->getAppartement()->getUtilisateur()->getSecondEmail()) {
                $message_email_proprio->setCc($reservation->getAppartement()->getUtilisateur()->getSecondEmail());
              }

              $textBodyProprio = $this->container->get('templating')->render(
                'PatFrontBundle:Reservation:mail_annulation_proprietaire.txt.twig', array('reservation' => $reservation)
              );
              $htmlBodyProprio = $this->container->get('templating')->render(
                'PatFrontBundle:Reservation:mail_annulation_proprietaire.html.twig', array('reservation' => $reservation)
              );

              if (!empty($htmlBodyProprio)) {
                $message_email_proprio->setBody($htmlBodyProprio, 'text/html')
                  ->addPart($textBodyProprio, 'text/plain');
              }
              else {
                $message_email_proprio->setBody($textBodyProprio);
              }
              $this->container->get('mailer')->send($message_email_proprio);
            }
            $factureTools = $this->container->get('pat.facture');

            $lastFactureFR = $reservation->getLastFacture('fr');

            // If reservation has invoice, make an avoir
            if ($lastFactureFR) {
              $factureTools->generateAvoir(
                $lastFactureFR, $lastFactureFR->getLocale(), $reservation
              );
            }
            $em->flush();
          }

          // Si l'on modifie la réservation à "Soldé"
          if ($reservation->getStatus() == ReservationTools::CONSTANT_STATUS_PAYED &&
            $old_status != ReservationTools::CONSTANT_STATUS_PAYED) {
            if ($emailOwner) {
              $message_email = \Swift_Message::newInstance()
                ->setSubject('Confirmation du solde de réservation Class Appart')
                ->setFrom('contact@class-appart.com')
                ->setBcc($this->container->getParameter('mail_admin'))
                ->setTo($reservation->getAppartement()->getUtilisateur()->getEmail());

//                        if ($reservation->getAppartement()->getUtilisateur()->getMailsSecond()) {
//                            $message_email->setCc(explode(',', $reservation->getAppartement()->getUtilisateur()->getMailsSecond()));
//                        }
              if ($reservation->getAppartement()->getUtilisateur()->getSecondEmail()) {
                $message_email->setCc($reservation->getAppartement()->getUtilisateur()->getSecondEmail());
              }

              $textBody = $this->container->get('templating')->render(
                'PatCompteBundle:Reservation:email_payed.txt.twig', array('reservation' => $reservation)
              );
              $htmlBody = $this->container->get('templating')->render(
                'PatCompteBundle:Reservation:email_payed.html.twig', array('reservation' => $reservation)
              );

              if (!empty($htmlBody)) {
                $message_email->setBody($htmlBody, 'text/html')
                  ->addPart($textBody, 'text/plain');
              }
              else {
                $message_email->setBody($textBody);
              }

              $this->container->get('mailer')->send($message_email);
            }
          }

          $this->container->get('session')->getFlashBag()->add('success', 'La réservation a correctement été modifiée');

          return new RedirectResponse($this->container->get('router')->generate('pat_admin_reservation_show', array("id_reservation" => $reservation->getId())));
        }
        else {
          $this->container->get('session')->getFlashBag()->add('error', 'Les dates ne sont pas disponibles');
        }
      }
    }

    return $this->container->get('templating')->renderResponse(
        'PatCompteBundle:AdminReservation:modifier.html.twig', array(
        'form' => $form->createView(),
        'reservation' => $reservation
        )
    );
  }

  // Fonction qui affiche le formulaire de recherche d'appartement
  public function selectInfoRechercheAction(Request $request)
  {
    $session = $request->getSession();
    $em = $this->container->get('doctrine')->getManager();

    // On nettoie les réservations non terminées
    $resas_invalides = $em->getRepository('PatCompteBundle:Reservation')->getResaInvalide();

    if ($resas_invalides) {
      foreach ($resas_invalides as $resa)
        $em->remove($resa);

      $em->flush();
    }

    $formRecherche = $this->container->get('form.factory')->create(new AdminReservationRechercheAppartementForm(), array('datearrivee' => $session->get('admin_resa_date_debut'),
      'datedepart' => $session->get('admin_resa_date_fin'),
      'ville' => $session->get('admin_resa_ville'),
      'rayon' => $session->get('admin_resa_rayon'),
      'type' => $session->get('admin_resa_type'),
      'nb_pieces' => $session->get('admin_resa_nb_pieces'),
      'budgetmini' => $session->get('admin_resa_budgetmini'),
      'budgetmaxi' => $session->get('admin_resa_budgetmaxi'),
      'photo' => $session->get('admin_resa_photo'),
      'reference' => $session->get('admin_resa_reference')));


    if ($request->getMethod() == 'POST' || $this->container->get('request')->query->get('page')) {
      // Si l'on arrive en Get avec la variable page, alors on navigue dans les résultats à l'aide du pager
      if ($this->container->get('request')->query->get('page')) {
        // On récupère les éléments de la recherche
        $data_form['datearrivee'] = $session->get('admin_resa_date_debut');
        $data_form['datedepart'] = $session->get('admin_resa_date_fin');
        $data_form['ville'] = $session->get('admin_resa_ville');
        $data_form['rayon'] = $session->get('admin_resa_rayon');
        $data_form['type'] = $session->get('admin_resa_type');
        $data_form['nb_pieces'] = $session->get('admin_resa_nb_pieces');
        $data_form['budgetmini'] = $session->get('admin_resa_budgetmini');
        $data_form['budgetmaxi'] = $session->get('admin_resa_budgetmaxi');
        $data_form['photo'] = $session->get('admin_resa_photo');
        $data_form['reference'] = $session->get('admin_resa_reference');
      }

      if ($request->getMethod() == 'POST') {
        $formRecherche->bind($request);

        if ($formRecherche->isValid()) {
          $data_form = $formRecherche->getData();

          $session->set('admin_resa_date_debut', $data_form['datearrivee']);
          $session->set('admin_resa_date_fin', $data_form['datedepart']);
          $session->set('admin_resa_ville', $data_form['ville']);
          $session->set('admin_resa_rayon', $data_form['rayon']);
          $session->set('admin_resa_type', $data_form['type']);
          $session->set('admin_resa_nb_pieces', $data_form['nb_pieces']);
          $session->set('admin_resa_budgetmini', $data_form['budgetmini']);
          $session->set('admin_resa_budgetmaxi', $data_form['budgetmaxi']);
          $session->set('admin_resa_photo', $data_form['photo']);
          $session->set('admin_resa_reference', $data_form['reference']);
        }
      }

      $appartement = $em->getRepository('PatCompteBundle:Appartement')->searchAppartementFront($data_form);

      /* $save_recherche = "";

        foreach($appartement as $appart)
        $save_recherche = $save_recherche."-".$appart->getId();

        $session->set('admin_save_search', $save_recherche); */


      $paginator = $this->container->get('knp_paginator');
      $pagination = $paginator->paginate(
        $appartement, $this->container->get('request')->query->get('page', 1), 20
      );

      $nb_total_appartement = $pagination->getTotalItemCount();

      // On récupère les photos et loyers respectives de chaque appartement
      $media = array();
      $loyers = array();
      $photo = array();

      $app = $pagination->getItems();
      for ($i = 0; $i < sizeof($app); $i++) {
        $recup_loyer = $em->getRepository('PatCompteBundle:Tarif')->findBy(array('appartement' => $app[$i]->getId()));
        $loyer_base = 'NC';
        $loyer_min = $recup_loyer[0];

        foreach ($recup_loyer as $infol) {
          if ($infol->getType() == 'base')
            $loyer_base = $infol->getLoyer(); // on récupère le loyer de base de l'appartement

          if ($infol->getLoyer() < $loyer_min)
            $loyer_min = $infol->getLoyer(); // on cherche le loyer minimum
        }

        $loyers[$app[$i]->getId()] = $loyer_min;

        $photo[$app[$i]->getId()] = $em->getRepository('PatCompteBundle:Media')->findBy(array('appartement' => $app[$i]->getId()), array('tri' => 'asc'));

        if ($photo[$app[$i]->getId()])
          $media[$app[$i]->getId()] = $photo[$app[$i]->getId()][0];
        else
          $media[$app[$i]->getId()] = null;
      }

      return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:selectAppartement.html.twig', array('appartement' => $pagination, 'loyers' => $loyers, 'media' => $media, 'nb_resultat' => $nb_total_appartement));
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:selectInfoRecherche.html.twig', array('formRecherche' => $formRecherche->createView())
    );
  }

  public function restitutionRechercheBySessionAction(Request $request)
  {
    $em = $this->container->get('doctrine')->getManager();
    $session = $request->getSession();
    if ($session->get('admin_save_search')) {

      $recherche = $session->get('admin_save_search');

      $tabid = explode("-", $recherche);
      $appartement = $em->getRepository('PatCompteBundle:Appartement')->getManyAppartementById($tabid, null);

      // On récupère les photos et loyers respectives de chaque appartement
      $media = array();
      $loyers = array();
      $photo = array();
      for ($i = 0; $i < sizeof($appartement); $i++) {
        $recup_loyer = $em->getRepository('PatCompteBundle:Tarif')->findBy(array('appartement' => $appartement[$i]->getId()));
        $loyer_base = 'NC';
        $loyer_min = $recup_loyer[0];
        foreach ($recup_loyer as $infol):
          if ($infol->getType() == 'base') {
            $loyer_base = $infol->getLoyer(); // on récupère le loyer de base de l'appartement
          }
          if ($infol->getLoyer() < $loyer_min) {
            $loyer_min = $infol->getLoyer(); // on cherche le loyer minimum
          }
        endforeach;

        if ($loyer_base == 'NC') { // Si l'appartement na pas de loyer on ne le considere pas
          $loyers[$i] = $loyer_base;
          unset($appartement[$i]);
        }
        else {
          $loyers[$i] = $loyer_min;
          //echo $appartement[$i]->getId();
          $photo[$i] = $em->getRepository('PatCompteBundle:Media')->findBy(array('appartement' => $appartement[$i]->getId()), array('tri' => 'asc'));
          if ($photo[$i]) {
            $media[$i] = $photo[$i][0];
          }
          else {
            $media[$i] = null;
          }
        }
      }

      return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:selectAppartement.html.twig', array('appartement' => $appartement, 'loyers' => $loyers, 'media' => $media));
    }
    else {
      return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:sessionExpire.html.twig');
    }
  }

  public function selectAppartementAction(Request $request, $id_appartement)
  {
    $session = $request->getSession();
    $session->set('admin_resa_appartement', $id_appartement);
    return new RedirectResponse($this->container->get('router')->generate('pat_admin_reservation_calcul_montant'));
  }

  // Fonction qui intervient quand on sélectionne la nombre de personne dans le process de réservation
  // Cette fonction recoit un appel ajax et met à jout ce nombre en session
  public function actualiseNbPersonneAction(Request $request)
  {
    if ($request->isXmlHttpRequest()) {
      $session = $request->getSession();

      $nb_personne = $request->request->get('resa_nbpersonne');
      $nb_adulte = $request->request->get('resa_nbadulte');
      $nb_enfant = $request->request->get('resa_nbenfant');
      $nb_lit_simple = $request->request->get('resa_nb_lit_simple');
      $nb_lit_double = $request->request->get('resa_nb_lit_double');
      $nb_canape_convertible = $request->request->get('resa_nb_canape_convertible');

      if ($nb_personne != '') {
        $session->set('admin_nbpersonne', $nb_personne);
      }

      if ($nb_adulte != '') {
        $session->set('admin_nbadultes', $nb_adulte);
      }

      if ($nb_enfant != '') {
        $session->set('admin_nbenfant', $nb_enfant);
      }

      if ($nb_lit_simple != '') {
        $session->set('admin_nb_lit_simple', $nb_lit_simple);
      }

      if ($nb_lit_double != '') {
        $session->set('admin_nb_lit_double', $nb_lit_double);
      }

      if ($nb_canape_convertible != '') {
        $session->set('admin_nb_canape_convertible', $nb_canape_convertible);
      }
    }
    return $this->container->get('templating')->renderResponse('PatFrontBundle:Reservation:blank.html.twig');
  }

  public function rechercheLocataireAction(Request $request)
  {
    $session = $request->getSession();
    $em = $this->container->get('doctrine')->getManager();
    $formRecherche = $this->container->get('form.factory')->create(new AdminLocataireRechercheForm());
    $locataires = null;

    if ($request->getMethod() == 'POST') {
      $formRecherche->bind($request);

      if ($formRecherche->isValid()) {
        $locatairerecherche = $request->request->get('locatairerecherche');
        $nom = str_replace("'", " ", $locatairerecherche["nom"]);
        $prenom = str_replace("'", " ", $locatairerecherche["prenom"]);
        $num_compte = str_replace("'", " ", $locatairerecherche["num_compte"]);
        $locataires = $em->getRepository('PatUtilisateurBundle:Utilisateur')->searchUtilisateur($nom, $prenom, $num_compte);
      }

      if (count($locataires) < 1)
        $this->container->get('session')->getFlashBag()->add('warning', 'Votre recherche n\'a retournée aucun résultat');
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:rechercheLocataire.html.twig', array(
        'formRecherche' => $formRecherche->createView(),
        'locataires' => $locataires
    ));
  }

  public function selectLocataireAction(Request $request, $id_locataire)
  {
    $session = $request->getSession();
    $em = $this->container->get('doctrine')->getManager();

    $user = $em->find('PatUtilisateurBundle:Utilisateur', $id_locataire);

    if (!$user)
      throw new NotFoundHttpException("L'utilisateur n'existe pas");

    //$session->set('admin_resa_locataire', $id_locataire);
    //return new RedirectResponse($this->container->get('router')->generate('pat_admin_reservation_recapitulatif'));
    $session->set('resa_locataire', $user->getUsername());
    return new RedirectResponse($this->container->get('router')->generate('pat_front_reservation_recapitulatif'));
  }

  // Permet à l'admin de mettre à jour les informations du locataire
  public function changeInformationsLocataireAction(Request $request, $id_locataire)
  {
    $session = $request->getSession();
    $em = $this->container->get('doctrine')->getManager();
    $locataire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array('id' => $id_locataire));

    $infos_locataire = new ReservationInformationsLocataire($locataire->getCivilite(), $locataire->getPrenom(), $locataire->getNom(), $locataire->getSociete(), $locataire->getDateNaissance(), $locataire->getNationalite(), $locataire->getLangueParle(), $locataire->getAdresse(), $locataire->getAdresse2(), $locataire->getCodePostal(), $locataire->getVille(), $locataire->getPays(), $locataire->getTelephone(), $locataire->getMobile(), $locataire->getMobile2(), $locataire->getAdresseFact(), $locataire->getAdresse2Fact(), $locataire->getCodePostalFact(), $locataire->getVilleFact(), $locataire->getPaysFact());

    $formInformation = $this->container->get('form.factory')->create(new AdminReservationInformationsLocataireForm(), $infos_locataire);

    if ($request->getMethod() == 'POST') {
      $formInformation->bind($request);
      if ($formInformation->isValid()) {
        // Mise à jour des informations Locataire
        $data_form = $formInformation->getData();
        $locataire->setCivilite($data_form->getCivilite());
        $locataire->setPrenom($data_form->getPrenom());
        $locataire->setNom($data_form->getNom());
        $locataire->setSociete($data_form->GetSociete());
        $locataire->setDateNaissance($data_form->getDateNaissance());
        $locataire->setNationalite($data_form->getNationalite());
        $locataire->setLangueParle($data_form->getLangueParle());
        $locataire->setAdresse($data_form->getAdresse());
        $locataire->setAdresse2($data_form->getAdresse2());
        $locataire->setCodePostal($data_form->getCodePostal());
        $locataire->setVille($data_form->getVille());
        $locataire->setPays($data_form->getPays());
        $locataire->setTelephone($data_form->getTelephone());
        $locataire->setMobile($data_form->getMobile());
        $locataire->setMobile2($data_form->getMobile2());
        $locataire->setAdresseFact($data_form->getAdresseFact());
        $locataire->setAdresse2Fact($data_form->getAdresse2Fact());
        $locataire->setCodePostalFact($data_form->getCodePostalFact());
        $locataire->setVilleFact($data_form->getVilleFact());
        $locataire->setPaysFact($data_form->getPaysFact());
        $locataire->setUpdatedAt(new \DateTime("now"));

        $em->persist($locataire);
        $em->flush();

        return new RedirectResponse($this->container->get('router')->generate('pat_admin_reservation_recapitulatif'));
      }
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:informationsLocataire.html.twig', array('locataire_id' => $locataire->getId(), 'formInformation' => $formInformation->createView())
    );
  }

  // Permet d'inscrire un nouveau locataire
  public function inscriptionLocataireAction(Request $request)
  {
    $session = $request->getSession();
    $em = $this->container->get('doctrine')->getManager();
    $message = null;

    $locataire = new Utilisateur();
    $formInscription = $this->container->get('form.factory')->create(new AdminReservationInscriptionLocataireForm(), $locataire);

    if ($request->getMethod() == 'POST' && $request->request->get('Inscription')) {
      $formInscription->bind($request);
      if ($formInscription->isValid()) {

        $patternEmail = '#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';

        if (!preg_match($patternEmail, $formInscription['email']->getData())) {
          $message = "L'adresse mail n'est pas valide";
        }
        else {
          $isExistEmail = $em->getRepository('PatUtilisateurBundle:Utilisateur')->checkEmailCompteAction($formInscription['email']->getData());
          if (!$isExistEmail) {

            $locataire->setEmailCanonical($formInscription['email']->getData());
            $locataire->setConfirmationToken(null);
            $locataire->setEnabled(true);
            $locataire->setLocataire();
            $locataire->setCGU("1");

            //gestion du mot de passe
            // On le génère Automatiquement
            $password = $em->getRepository('PatUtilisateurBundle:Utilisateur')->generatePassword();
            $encoder = $this->container->get('security.encoder_factory')->getEncoder($locataire);
            $locataire->setPassword($encoder->encodePassword($password, $locataire->getSalt()));

            $em->persist($locataire);
            $em->flush();

            $locataire->generateUsername();

            $em->flush();

            $user = $this->container->get('security.context')->getToken()->getUser();

            $session->set('admin_resa_locataire', $locataire->getId());

            //envoi du mail au locataire
            $message_email = \Swift_Message::newInstance()
              ->setSubject("Création de votre compte locataire - ClassAppart ®")
              ->setFrom('contact@class-appart.fr')
              ->setBcc($this->container->getParameter('mail_admin'))
              ->setTo($locataire->getEmail());

//                        if($locataire->getMailsSecond())
//                            $message_email->setCc(explode(',', $locataire->getMailsSecond()));
            if ($locataire->getSecondEmail()) {
              $message_email->setCc($locataire->getSecondEmail());
            }

            $textBody = $this->container->get('templating')->render('PatCompteBundle:AdminLocataire:email_validation.txt.twig', array('utilisateur' => $locataire, 'password' => $password));
            $htmlBody = $this->container->get('templating')->render('PatCompteBundle:AdminLocataire:email_validation.html.twig', array('utilisateur' => $locataire, 'password' => $password));

            if (!empty($htmlBody)) {
              $message_email->setBody($htmlBody, 'text/html')
                ->addPart($textBody, 'text/plain');
            }
            else
              $message_email->setBody($textBody);

            $this->container->get('mailer')->send($message_email);

            return new RedirectResponse($this->container->get('router')->generate('pat_admin_reservation_recapitulatif'));
          }
          else {
            $message = 'Cette adresse mail existe déjà';
          }
        }
      }
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:inscriptionLocataire.html.twig', array('message' => $message, 'formInscription' => $formInscription->createView()));
  }

  //calcul le nombre de nuits entre 2 date
  public function calculNbNuits($date_arrivee1, $date_depart1)
  {

    $date_arrivee1 = $this->toDateEn($date_arrivee1);
    $date_arrivee1 = str_replace("/", "", $date_arrivee1);

    $date_depart1 = $this->toDateEn($date_depart1);
    $date_depart1 = str_replace("/", "", $date_depart1);


    $nbjours = abs(round((strtotime($date_arrivee1) - strtotime($date_depart1)) / (60 * 60 * 24) - 1));
    $nbnuits = $nbjours - 1;

    return $nbnuits;
  }

  // Affiche le récapitulatif de la réservation
  public function recapitulatifAction(Request $request)
  {
    $bloque = false;
    $session = $request->getSession();
    $em = $this->container->get('doctrine')->getManager();
    $message = "";

    if ($session->get('admin_resa_date_debut') && $session->get('admin_resa_date_fin') && $session->get('admin_resa_appartement') && $session->get('admin_resa_prix') && $session->get('admin_resa_arrhes') && $session->get('admin_resa_nbnuits') && $session->get('admin_resa_nbpersonne') && $session->get('admin_resa_locataire')) {
      $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $session->get('admin_resa_appartement')));
      $media = $em->getRepository('PatCompteBundle:Media')->findOneBy(array('appartement' => $appartement->getId()), array('tri' => 'asc'));
      $locataire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array('id' => $session->get('admin_resa_locataire')));


      if ($request->getMethod() == 'POST') {
        // On vérifie qu'une réservation ou un blocage de dates n'a pas été effectué entre temps
        if ($em->getRepository('PatCompteBundle:Appartement')->isAvailableForResa($session->get('admin_resa_appartement'), $session->get('admin_resa_date_debut'), $session->get('admin_resa_date_fin')) == false) {
          $bloque = true;
          $message = "L'appartement n'est pas disponible pour ces dates";
        }


        if ($bloque == false) {
          // On inscrit la Réservation en Base
          $reservation = new Reservation();
          $reservation->setAppartement($appartement);
          $reservation->setStatus(ReservationTools::CONSTANT_STATUS_WAIT);
          $reservation->setUtilisateur($locataire);
          $reservation->setDateDebut($this->toDateEn($session->get('admin_resa_date_debut')));
          $reservation->setDateFin($this->toDateEn($session->get('admin_resa_date_fin')));
          $reservation->setTarif($session->get('admin_resa_prix'));
          $reservation->setArrhes($session->get('admin_resa_arrhes'));
          $reservation->setCalculValues($session->get('admin_resa_tab_calcul'));
          $reservation->setNbPersonne($session->get('admin_resa_nbpersonne'));
          $reservation->setNbAdulte($session->get('admin_resa_nbadulte'));
          $reservation->setNbEnfant($session->get('admin_resa_nbenfant'));
          $reservation->setNbLitSimple($session->get('admin_resa_nb_lit_simple'));
          $reservation->setNbLitDouble($session->get('admin_resa_nb_lit_double'));
          $reservation->setNbCanapeConvertible($session->get('admin_resa_nb_canape_convertible'));
          $reservation->setIsAnnule(0);
          $reservation->setIsPaye(0);
          $reservation->setReference("RefResa");
          $reservation->setDateValidite($this->toDateEn($request->request->get('date_validite')).' 23:59:59');
          $reservation->setCreatedAt(date("Y-m-d H:i:s"));
          $reservation->setUpdatedAt(date("Y-m-d H:i:s"));

          // on check si le réservation est autorisée (même si, normalement si la reservation n'est pas autorisée on ne peut accéder à cette page)
          if ($appartement->getIsResa() == 1) {
            $em->persist($reservation);

            $em->flush();

            // on stocke l'id de la réservation
            $session->set('admin_resa_id', $reservation->getId());

            //throw new NotFoundHttpException("Cette page est en cours de construction !");
            return new RedirectResponse($this->container->get('router')->generate('pat_admin_reservation_confirmation'));
          }
          else
            $message = "Cet appartement n'est pas disponible à la réservation";
        }
        else {
          $this->container->get('session')->getFlashBag()->add('notice', $message);

          return new RedirectResponse($this->container->get('router')->generate('pat_admin_reservation_select_info_recherche'));
        }
      }

      return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:recapitulatif.html.twig', array('message' => $message,
          'locataire' => $locataire,
          'appartement' => $appartement,
          'prix' => $session->get('admin_resa_prix'),
          'arrhes' => $session->get('admin_resa_arrhes'),
          'nuits' => $session->get('admin_resa_nbnuits'),
          'media' => $media,
          'date_arrivee' => $session->get('admin_resa_date_debut'),
          'date_depart' => $session->get('admin_resa_date_fin'),
          'max_personne' => $appartement->getNbPersonne())
      );
    }
    else
      return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:sessionExpire.html.twig');
  }

  public function clearSession(Request $request)
  {
    $session = $request->getSession();
    $session->remove('admin_resa_id');
    $session->remove('admin_resa_date_debut');
    $session->remove('admin_resa_date_fin');
    $session->remove('admin_resa_appartement');
    $session->remove('admin_resa_prix');
    $session->remove('admin_resa_arrhes');
    $session->remove('admin_resa_nbnuits');
    $session->remove('admin_resa_nbpersonne');
    $session->remove('admin_resa_nbadulte');
    $session->remove('admin_resa_nbenfant');
    $session->remove('admin_resa_nb_lit_simple');
    $session->remove('admin_resa_nb_lit_double');
    $session->remove('admin_resa_nb_canape_convertible');
    $session->remove('admin_resa_locataire');
  }

  // Fonction qui affiche le mail envoyé au locataire
  public function confirmationAction(Request $request)
  {
    $session = $request->getSession();
    $em = $this->container->get('doctrine')->getManager();

    if ($session->get('admin_resa_id')) { // on controle qu'une réservation a bien eu lieu
      // On récupere les infos de réservation
      $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneBy(array('id' => $session->get('admin_resa_id')));
      $appartement = $reservation->getAppartement();
      $locataire = $reservation->getUtilisateur();
      //
      // envoie d'un mail avec tout le descriptif de la réservation

      $message_email = \Swift_Message::newInstance()
        ->setSubject("Récapitulatif de votre réservation - classAppart ®")
        ->setFrom('contact@class-appart.com')
        ->setBcc($this->container->getParameter('mail_admin'))
        ->setTo($locataire->getEmail());

//            if($locataire->getMailsSecond())
//                $message_email->setCc(explode(',', $locataire->getMailsSecond()));
      if ($locataire->getSecondEmail()) {
        $message_email->setCc($locataire->getSecondEmail());
      }

      $textBody = $this->container->get('templating')->render('PatFrontBundle:Reservation:recapitulatif_reservation.txt.twig', array('locataire' => $locataire, 'appartement' => $appartement, 'reservation' => $reservation));
      $htmlBody = $this->container->get('templating')->render('PatFrontBundle:Reservation:recapitulatif_reservation.html.twig', array('locataire' => $locataire, 'appartement' => $appartement, 'reservation' => $reservation));

      if (!empty($htmlBody)) {
        $message_email->setBody($htmlBody, 'text/html')
          ->addPart($textBody, 'text/plain');
      }
      else
        $message_email->setBody($textBody);

      $this->container->get('mailer')->send($message_email);

      $this->clearSession($request);
      return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:confirmation.html.twig', array('locataire' => $locataire, 'appartement' => $appartement, 'reservation' => $reservation));
    }
    else {
      throw new NotFoundHttpException("Les informations de réservation sont introuvables");
    }
  }

  public function sendToLocataireAction(Request $request)
  {
    return $this->container->get('templating')->renderResponse('PatFrontBundle:Reservation:admin_demande_confirmation.html.twig');
  }

  // Confirmation de l'envoie du mail
  public function confirmationSendToLocataireAction(Request $request)
  {
    $session = $request->getSession();
    $em = $this->container->get('doctrine')->getManager();

    if ($session->get('resa_id')) { // on controle qu'une réservation a bien eu lieu
      $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneBy(array('id' => $session->get('resa_id')));

      $reservation->setStatus(ReservationTools::CONSTANT_STATUS_WAIT_VALIDATION);

      $em->flush();

      $message_email = \Swift_Message::newInstance()
        ->setSubject("Votre demande de réservation ".$reservation->getReference()." chez Class Appart")
        ->setFrom('contact@class-appart.com')
        ->setBcc($this->container->getParameter('mail_admin'))
        ->setTo($reservation->getUtilisateur()->getEmail());

//            if($reservation->getUtilisateur()->getMailsSecond())
//                $message_email->setCc(explode(',', $reservation->getUtilisateur()->getMailsSecond()));
      if ($reservation->getUtilisateur()->getSecondEmail()) {
        $message_email->setCc($reservation->getUtilisateur()->getSecondEmail());
      }

      $textBody = $this->container->get('templating')->render('PatFrontBundle:Reservation:mail_demande_finalisation.txt.twig', array('reservation' => $reservation));
      $htmlBody = $this->container->get('templating')->render('PatFrontBundle:Reservation:mail_demande_finalisation.html.twig', array('reservation' => $reservation));

      if (!empty($htmlBody)) {
        $message_email->setBody($htmlBody, 'text/html')
          ->addPart($textBody, 'text/plain');
      }
      else
        $message_email->setBody($textBody);

      $this->container->get('mailer')->send($message_email);

      $this->clearSession($request);

      return $this->container->get('templating')->renderResponse('PatFrontBundle:Reservation:admin_confirmation.html.twig');
    }
    else
      throw new NotFoundHttpException("Les informations de réservation sont introuvables");
  }

  //passer les dates au format EN
  public function toDateEn($date)
  {
    return substr($date, 6, 4)."-".substr($date, 3, 2)."-".substr($date, 0, 2);
  }

  //passer les dates au format FR
  public function toDateFR($date)
  {
    return substr($date, 8, 2)."/".substr($date, 5, 2)."/".substr($date, 0, 4);
  }

  /**
   * Formulaire de la facture à compléter.
   */
  public function factureAction(Request $request, $id)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère les résas
    $reservation = $em->find('PatCompteBundle:Reservation', $id);
    if (!$reservation) {
      throw new NotFoundHttpException("Réservation non trouvée");
    }

    $nb_nuits = $this->container->get('pat.date_functions')->nbJours($reservation->getDateDebut(), $reservation->getDateFin());

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:facture.html.twig', [
        'resa' => $reservation,
        'nb_nuits' => $nb_nuits,
        'backward_url' => $request->headers->get('referer'),
        'factureNumber' => $this->container->get('pat.facture')->generateNewFactureNumber(),
    ]);
  }

  /**
   * Génération de l'enregistrement et des PDF.
   *
   * @param Request $request
   * @param         $id
   *
   * @return mixed
   */
  public function facturePdfAction(Request $request, $id)
  {
    $em = $this->container->get('doctrine')->getManager();
    // FR
    $adresse = $request->request->get('CAadresse');
    $commentaires = $request->request->get('comm');
    $haut = $request->request->get('haut');
    $pied = $request->request->get('pied');
    // EN
    $commentaires2 = $request->request->get('comm2');
    $haut2 = $request->request->get('haut2');
    $pied2 = $request->request->get('pied2');
    //on récupère les résas
    /** @var Reservation $reservation */
    $reservation = $em->find('PatCompteBundle:Reservation', $id);
    if (!$reservation) {
      throw new NotFoundHttpException("Réservation non trouvée");
    }

    $nb_nuits = $this->container->get('pat.date_functions')->nbJours($reservation->getDateDebut(), $reservation->getDateFin());

    /** @var FactureTools $factureTools */
    $factureTools = $this->container->get('pat.facture');

    $lastFactureFR = $reservation->getLastFacture('fr');

    // If reservation has invoice, make an avoir
    if ($lastFactureFR) {
      $factureTools->generateAvoir(
        $lastFactureFR, $lastFactureFR->getLocale(), $reservation
      );
    }

    $factureNumber = $factureTools->generateNewFactureNumber();

    // Save new Facture
    $factureFr = $factureTools
      ->generateFacture(
      $reservation, 'fr', $factureNumber, $haut, $pied, $commentaires, $adresse
      );

    $em->persist($factureFr);
    $em->flush();

    $em->persist($reservation);
    $em->flush();

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Facture:facture.html.twig', [
        'resa' => $reservation,
        'nb_nuits' => $nb_nuits,
        'root' => $this->container->get('kernel')->getRootDir(),
        'factureNumber' => $factureNumber,
    ]);
  }

  public function FactureEmailPdfAction(Request $request, $id)
  {

    $em = $this->container->get('doctrine')->getManager();

    $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneBy(array('id' => $id));
    if (!$reservation) {
      throw new AccessDeniedException("Cette réservation n'existe pas");
    }

    $lastFactureFR = $reservation->getLastFacture('fr');

    if (null === $lastFactureFR) {
      throw new AccessDeniedException('Aucune factures à envoyer');
    }

    $path_fr = $this->container->get('kernel')->getRootDir().'/../factures/'.$lastFactureFR->getFileName();

    $message_email = \Swift_Message::newInstance()
      ->setSubject("Facture à retourner signée ".$lastFactureFR->getNumber()." chez Class Appart")
      ->setFrom('contact@class-appart.com')
      ->setTo($reservation->getUtilisateur()->getEmail())
      ->setBcc($this->container->getParameter('mail_admin'))
      ->attach(\Swift_Attachment::fromPath($path_fr));

    $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Facture:mail_facture.html.twig', [
      'factureNumber' => $lastFactureFR->getNumber(),
      'values' => $lastFactureFR->getCalculValues(),
      'reservation' => $lastFactureFR->getReservation(),
      'isAvoir' => null !== $lastFactureFR->getAvoirFileName(),
      'toSign' => true
    ]);

    $message_email->setBody($htmlBody, 'text/html');

    $this->container->get('mailer')->send($message_email);

    $this->container->get('session')->getFlashBag()->add('success', 'La facture a correctement été envoyée');

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_reservation_show', array("id_reservation" => $reservation->getId())));
  }

  public function downloadFactureNewAction(Facture $facture)
  {
    /** @var FactureTools $factureTools */
    $factureTools = $this->container->get('pat.facture');
    $filePath = $factureTools->getFacturePath($facture->getFileName());

    $response = new Response();

    $content = file_get_contents($filePath);
    $response->headers->set('Content-Type', 'application/pdf');
    $response->headers->set('Content-Disposition', 'attachment;filename="'.$facture->getFileName());

    $response->setContent($content);

    return $response;
  }

  public function downloadAvoirNewAction(Facture $facture)
  {
    if (null === $facture->getAvoirFileName()) {
      throw new NotFoundHttpException('Aucun avoirs pour cette facture.');
    }

    /** @var FactureTools $factureTools */
    $factureTools = $this->container->get('pat.facture');
    $filePath = $factureTools->getFacturePath($facture->getAvoirFileName());

    $response = new Response();

    $content = file_get_contents($filePath);
    $response->headers->set('Content-Type', 'application/pdf');
    $response->headers->set('Content-Disposition', 'attachment;filename="'.$facture->getAvoirFileName());

    $response->setContent($content);

    return $response;
  }

  public function sendFactureAction(Facture $facture)
  {
    /** @var FactureTools $factureTools */
    $factureTools = $this->container->get('pat.facture');
    $filePath = $factureTools->getFacturePath($facture->getFileName());

    $message_email = \Swift_Message::newInstance()
    ->setSubject("Facture à retourner signée ".$facture->getNumber()." chez Class Appart")
    ->setFrom('contact@class-appart.com')
    ->setTo($facture->getReservation()->getUtilisateur()->getEmail())
    ->setBcc($this->container->getParameter('mail_admin'))
    ->attach(\Swift_Attachment::fromPath($filePath));

    $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Facture:mail_facture.html.twig', [
    'factureNumber' => $facture->getNumber(),
    'values' => $facture->getCalculValues(),
    'reservation' => $facture->getReservation(),
    'isAvoir' => null !== $facture->getAvoirFileName(),
    'toSign' => true
    ]);

    $message_email->setBody($htmlBody, 'text/html');

    $this->container->get('mailer')->send($message_email);

    $this->container->get('session')->getFlashBag()->add('success', 'La facture a correctement été envoyée');

    return new RedirectResponse($this
        ->container
        ->get('router')
        ->generate('pat_admin_reservation_show', [
          'id_reservation' => $facture->getReservation()->getId()
          ]
    ));
  }

  public function sendAvoirAction(Facture $facture)
  {
    if (null === $facture->getAvoirFileName()) {
      throw new NotFoundHttpException('Aucun avoirs pour cette facture.');
    }

    /** @var FactureTools $factureTools */
    $factureTools = $this->container->get('pat.facture');
    $filePath = $factureTools->getFacturePath($facture->getAvoirFileName());

    $message_email = \Swift_Message::newInstance()
      ->setSubject("Avoir sur facture ".$facture->getNumber()." chez Class Appart")
      ->setFrom('contact@class-appart.com')
      ->setTo($facture->getReservation()->getUtilisateur()->getEmail())
      ->setBcc($this->container->getParameter('mail_admin'))
      ->attach(\Swift_Attachment::fromPath($filePath))
    ;

    $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Facture:mail_facture.html.twig', [
      'factureNumber' => $facture->getNumber(),
      'values' => $facture->getCalculValues(),
      'reservation' => $facture->getReservation(),
      'isAvoir' => true,
      'toSign' => false
    ]);

    $message_email->setBody($htmlBody, 'text/html');

    $this->container->get('mailer')->send($message_email);

    $this->container->get('session')->getFlashBag()->add('success', "L'avoir a correctement été envoyée");

    return new RedirectResponse($this
        ->container
        ->get('router')
        ->generate('pat_admin_reservation_show', [
          'id_reservation' => $facture->getReservation()->getId()
          ]
    ));
  }

  public function downloadFacturePdfAction($language, $id)
  {
    // Ancien systeme de facture
    $em = $this->container->get('doctrine')->getManager();

    $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneBy(array('id' => $id));
    if (!$reservation) {
      throw new AccessDeniedException("Cet réservation n'existe pas");
    }

    $path_fr = $this->container->get('kernel')->getRootDir().'/../factures/'.$reservation->getFilenameFr();
    $path_en = $this->container->get('kernel')->getRootDir().'/../factures/'.$reservation->getFilenameEn();

    $response = new Response();

    if ($language == "fr") {
      $content = file_get_contents($path_fr);
      $response->headers->set('Content-Type', 'application/pdf');
      $response->headers->set('Content-Disposition', 'attachment;filename="'.$reservation->getFilenameFr());
    }
    else {
      $content = file_get_contents($path_en);
      $response->headers->set('Content-Type', 'application/pdf');
      $response->headers->set('Content-Disposition', 'attachment;filename="'.$reservation->getFilenameEn());
    }

    $response->setContent($content);
    return $response;
  }

  /*
   *  Modification du texte des options de réservation
   */

  public function editTexteOptionsAction(Request $request)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user || $user->getType() != "Administrateur") {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    $param_options = $em->getRepository('PatCompteBundle:Parametre')->findOneByName('text_options_reservation');
    $contenu = $param_options->getValue();

    $form = $this->container->get('form.factory')->create(new TextOptionsForm(), $contenu);

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {
        $data = $form->getData();

        $param_options->setValue($data);
        $em->flush();

        $this->container->get('session')->getFlashBag()->add('success', 'Vos changements ont été sauvegardés !');
      }
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:editTextOptions.html.twig', array(
        'form' => $form->createView(),
        'param_options' => $param_options
    ));
  }

}
