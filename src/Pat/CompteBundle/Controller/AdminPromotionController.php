<?php

namespace Pat\CompteBundle\Controller;

use Doctrine\ORM\EntityManager;
use Pat\CompteBundle\Entity\Appartement;
use Pat\CompteBundle\Entity\Promotion;
use Pat\CompteBundle\Form\AdminPromotionForm;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class AdminPromotionController extends ContainerAware
{

  /**
   * @param Request     $request
   * @param Appartement $appartement
   *
   * @return mixed
   */
  public function indexAction(Request $request, Appartement $appartement)
  {
    /** @var EntityManager $em */
    $em = $this->container->get('doctrine.orm.entity_manager');
    $tarifManager = $this->container->get('pat.tarif_manager');

    $promotion = new Promotion();
    $promotion->setAppartement($appartement);

    $form = $this->container->get('form.factory')->create('admin_promotion', $promotion);

    if ('POST' === $request->getMethod()) {
      $form->handleRequest($request);

      if (true === $form->isValid()) {
        $promotion->updateWithType();

        if ($promotion->getType() === Promotion::TYPE_PERIODE) {
          $begin = $promotion->getBegin();
          $end = $promotion->getEnd();

          $begin = new \DateTime($this->toDateEn($begin));
          $end = new \DateTime($this->toDateEn($end));

          $promotion
            ->setBegin($begin)
            ->setEnd($end)
          ;
        }

        $amount = $form->get('amount')->getData();
        if ($amount) {
          $price = $tarifManager->getLoyer($appartement->getTarif());
          $newPrice = ($amount / 6) - $price;
          $promotion->setPercentage($newPrice / $price * 100);
        }

        $em->persist($promotion);
        $em->flush();

        $type = $promotion->getPercentage() > 0 ? 'majoration' : 'promotion';

        $this->container->get('session')->getFlashBag()->add('success', 'La '.$type.' a bien été ajouté.');

        return new RedirectResponse($this->container->get('router')->generate('pat_admin_promotion_index', [
            'id' => $appartement->getId(),
            ]
        ));
      }
    }

    // Voir
    // src/Pat/FrontBundle/Controller/ReservationController::montantTotal()
    $paramTaxes = $em->getRepository('PatCompteBundle:Parametre')->findOneByName('taxes');
    $dateTimeFormat = 'Y-m-d H:i:s';
    $tarifs = array();
    foreach ($appartement->getPromotions() as $promotion) {
      $begin = false;
      $end = false;
      if ($promotion->getType() === Promotion::TYPE_PERIODE) {
        $begin = $promotion->getBegin();
        $end = \DateTime::createFromFormat(
            $dateTimeFormat, $promotion->getBegin()->format($dateTimeFormat)
          )->modify('+6 days');
      }
      $tarifs[] = $tarifManager->calculLoyer(
        $appartement->getTarif(), $paramTaxes->getValue(), $appartement->getNbPieces(), 1, 1, $begin, $end
      );
    }

    return $this
        ->container
        ->get('templating')
        ->renderResponse('PatCompteBundle:AdminPromotion:admin_promotion_index.html.twig', [
          'appartement' => $appartement,
          'form' => $form->createView(),
          'tarifs' => $tarifs
    ]);
  }

  /**
   * @param Appartement $appartement
   * @param null        $promotionId
   *
   * @return RedirectResponse
    s */
  public function supprimerAction(Appartement $appartement, $promotionId)
  {
    $em = $this->container->get('doctrine.orm.entity_manager');

    $promotion = $em->find('PatCompteBundle:Promotion', $promotionId);

    if (!$promotion) {
      throw new NotFoundHttpException("Promotion non trouvée");
    }

    $type = $promotion->getPercentage() > 0 ? 'majoration' : 'promotion';

    $em->remove($promotion);
    $em->flush();

    $this->container->get('session')->getFlashBag()->add('success', 'La '.$type.' a bien été supprimé.');

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_promotion_index', [
        'id' => $appartement->getId(),
        ]
    ));
  }

  /**
   * @param Request     $request
   * @param Appartement $appartement
   * @param             $promotionId
   *
   * @return RedirectResponse
   */
  public function activerDesactiverAction(Request $request, Appartement $appartement, $promotionId)
  {
    $em = $this->container->get('doctrine.orm.entity_manager');

    $promotion = $em->find('PatCompteBundle:Promotion', $promotionId);

    $isForActivate = 'pat_admin_promotion_activer' === $request->get('_route');

    if (!$promotion) {
      throw new NotFoundHttpException("Promotion non trouvée");
    }

    $type = $promotion->getPercentage() > 0 ? 'majoration' : 'promotion';

    if (Promotion::TYPE_ACTIVABLE !== $promotion->getType()) {
      throw new InvalidParameterException("Cette ".$type." n'est pas ".true === $isForActivate ? 'activable' : 'désactivable');
    }

    $flush = true;

    if (true === $isForActivate) {
      if (null !== $promotion->getAppartement()->getActivePromotion()) {
        $this->container->get('session')->getFlashBag()->add('error', 'Une Promotion / Majoration est déjà active pour cet appartement');
        $flush = false;
      }
      else {

        $promotion->setActivated(true);

        $this->container->get('session')->getFlashBag()->add('success', 'La '.$type.' a bien été activée.');
      }
    }
    else {
      $promotion->setActivated(false);

      $this->container->get('session')->getFlashBag()->add('success', 'La '.$type.' a bien été désactivée.');
    }

    if (true === $flush) {
      $em->flush();
    }

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_promotion_index', [
        'id' => $appartement->getId(),
        ]
    ));
  }

  //passer les dates au format EN
  private function toDateEn($date)
  {
    return substr($date, 6, 4)."-".substr($date, 3, 2)."-".substr($date, 0, 2);
  }

}
