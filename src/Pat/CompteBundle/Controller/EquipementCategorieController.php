<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\CompteBundle\Entity\EquipementCategorie;
use Pat\CompteBundle\Form\EquipementCategorieForm;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EquipementCategorieController extends ContainerAware
{

  //afficher le formulaire d'ajout d'un équipement
  public function ajouterAction($id = null)
  {
    $this->locale = $this->container->get('request')->getLocale();
    $validation = '';
    $message = '';
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user)
      throw new AccessDeniedException('This user does not have access to this section.');

    if (isset($id)) {
      // modification d'une catégorie existante : on recherche ses données
      $categorie = $em->find('PatCompteBundle:EquipementCategorie', $id);

      if (!$categorie)
        throw new AccessDeniedException('Aucune catégorie trouvée');
    }
    else // ajout d'une nouvelle catégorie
      $categorie = new EquipementCategorie();

    $form = $this->container->get('form.factory')->create(new EquipementCategorieForm(), $categorie);
    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {
        $isExistCategorie = $em->getRepository('PatCompteBundle:EquipementCategorie')->findBy(array("intitule_categorie" => $form['intitule_categorie']->getData()));
        if (!$isExistCategorie) {
          $em->persist($categorie);
          $em->flush();

          if (isset($id))
            $message = 'Catégorie modifiée avec succès !';
          else
            $message = 'Catégorie ajoutée avec succès !';

          $this->container->get('session')->getFlashBag()->add('notice', $message);
          return new RedirectResponse($this->container->get('router')->generate('pat_admin_equipement_categorie_ajouter'));
        }
        else
          $message = "Cette catégorie existe déjà !";
      }
    }

    $categories = $em->getRepository('PatCompteBundle:EquipementCategorie')->findAll();

    return $this->container->get('templating')->renderResponse('PatCompteBundle:EquipementCategorie:ajouter.html.twig', array(
        'form' => $form->createView(),
        'categories' => $categories,
        'message' => $message,
    ));
  }

  // Supprimer un équipement
  public function supprimerAction($id)
  {
    $em = $this->container->get('doctrine')->getManager();
    $categorie = $em->find('PatCompteBundle:EquipementCategorie', $id);

    if (!$categorie)
      throw new NotFoundHttpException("Equipement non trouvé");

    $em->remove($categorie);
    $em->flush();
    $message = "Catégorie supprimée avec succès !";

    $this->container->get('session')->getFlashBag()->add('flash', $message);
    return new RedirectResponse($this->container->get('router')->generate('pat_admin_equipement_categorie_ajouter'));
  }

}
