<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Pat\CompteBundle\Form\DocumentsType;
use Pat\CompteBundle\Entity\Documents;

class DocumentsController extends Controller
{

  // <summary>
  // Liste des documents
  // </summary>
  public function ListFileAction(Request $request)
  {
    $em = $this->container->get('doctrine')->getManager();
    $documents = $em->getRepository('PatCompteBundle:Documents')->findBy(array('user' => $this->getUser()));
    return $this->render('PatCompteBundle:Documents:list.html.twig', array('documents' => $documents));
  }

  // <summary>
  // Ajouter un document
  // </summary>
  public function AddFileAction(Request $request)
  {
    $user = $this->getUser();
    $userid = $user->getId();

    $documents = new Documents();
    $documents->setUser($user);
    $em = $this->container->get('doctrine')->getManager();

    $form = $this->createForm(new DocumentsType($userid), $documents, array('validation_groups' => array('creation')));

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {
        $file = $request->files->get('pat_comptebundle_documentstype');
        $file = $file['file'];
        if (!is_null($file))
          $documents->setFileSize($file->getSize());

        $em->persist($documents);
        $em->flush();

        $message_email = \Swift_Message::newInstance()
          ->setSubject("Nouveau document - ClassAppart ®")
          ->setFrom('info@class-appart.com')
          ->setTo('info@class-appart.com');

        $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Documents:email_administration_done.html.twig', array('utilisateur' => $user, 'document' => $documents));
        $message_email->setBody($htmlBody, 'text/html');
        $this->container->get('mailer')->send($message_email);

        $this->container->get('session')->getFlashBag()->add('success', 'Document ajouté avec succès');

        return $this->redirect($this->generateUrl('documents_list'));
      }
    }

    return $this->render('PatCompteBundle:Documents:add.html.twig', array('form' => $form->createView()));
  }

  // <summary>
  // Editer un documents
  // </summary>
  public function EditFileAction(Request $request, $fileid)
  {
    $user = $this->getUser();
    $userid = $user->getId();
    $em = $this->container->get('doctrine')->getManager();

    $documents = $em->getRepository('PatCompteBundle:Documents')->findOneBy(array('id' => $fileid, 'user' => $user));

    if (!$documents) {
      throw $this->createNotFoundException('Unable to find Document entity.');
    }

    $form = $this->createForm(new DocumentsType($userid), $documents, array('validation_groups' => array('edition')));

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {
        $file = $request->files->get('pat_comptebundle_documentstype');
        $file = $file['file'];
        if (!is_null($file))
          $documents->setFileSize($file->getSize());

        $em->persist($documents);
        $em->flush();

        $message_email = \Swift_Message::newInstance()
          ->setSubject("Nouveau document - ClassAppart ®")
          ->setFrom('info@class-appart.com')
          ->setTo('info@class-appart.com');

        $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Documents:email_administration_done.html.twig', array('utilisateur' => $user, 'document' => $documents));
        $message_email->setBody($htmlBody, 'text/html');
        $this->container->get('mailer')->send($message_email);

        $this->container->get('session')->getFlashBag()->add('success', 'Vos changements ont correctement été sauvegardés');

        return $this->redirect($this->generateUrl('documents_list'));
      }
    }

    return $this->render('PatCompteBundle:Documents:edit.html.twig', array('form' => $form->createView()));
  }

  // <summary>
  // Supprimer un document
  // </summary>
  public function DeleteFileAction($fileid)
  {

    $em = $this->container->get('doctrine')->getManager();
    $user = $this->getUser();
    $document = $em->getRepository('PatCompteBundle:Documents')->findOneBy(array('id' => $fileid, 'user' => $user));

    if (!$document) {
      throw $this->createNotFoundException('Unable to find Document entity.');
    }

    $em->remove($document);
    $em->flush();

    $this->container->get('session')->getFlashBag()->add('success', 'Document supprimé avec succès');
    return $this->redirect($this->generateUrl('documents_list'));
  }

  // <summary>
  // Téléchargement d'un document
  // <summary>
  public function DownloadFileAction($fileid)
  {
    $em = $this->container->get('doctrine')->getManager();
    $user = $this->getUser();
    $document = $em->getRepository('PatCompteBundle:Documents')->findOneBy(array('id' => $fileid, 'user' => $user));

    if (!$document) {
      throw $this->createNotFoundException('Unable to find Document entity.');
    }

    $file = $document->getFile();
    $helper = $this->get('vich_uploader.templating.helper.uploader_helper');
    $path = $this->get('kernel')->getRootDir().'/..'.$helper->asset($document, 'file');

    // Opening file
    $content = file_get_contents($path);
    $response = new Response();
    $response->headers->set('Content-Type', $file->getMimeType());
    $response->headers->set('Content-Disposition', 'attachment;filename="'.$document->getDocumentName().'.'.$file->getExtension());

    $response->setContent($content);

    return $response;
  }

}
