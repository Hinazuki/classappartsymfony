<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\CompteBundle\Form\AdminUtilisateurForm;
use Pat\CompteBundle\Form\AdminUtilisateurPasswordForm;
use Pat\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
//use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
//use Doctrine\Common\Collections\ArrayCollection;
//use Symfony\Component\HttpFoundation\Session;

class AdminUtilisateurController extends Controller
{

  /**
   * Ajoute un compte.
   *
   * @param Request $request
   * @param int $id_user
   * @return RedirectResponse
   * @throws AccessDeniedException
   * @author small refactoring AG
   */
  public function ajouterAction(Request $request, $id_user = null)
  {
    $this->locale = $request->getLocale();
    $em = $this->container->get('doctrine')->getManager();
    $message = null;

    //on récupère l'utilisateur courant
    $user_context = $this->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    $arrayRoles = $user->getRoles(); //on test les droits
    //il faut être super admin ou bien accèder à son propre compte
    if (!in_array('ROLE_SUPER_ADMIN', $arrayRoles) && $id_user != $user_context->getId()) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    if (isset($id_user)) {
      $utilisateur = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array(
        'type_utilisateur' => Utilisateur::ADMIN, 'id' => $id_user
      ));
      if (!$utilisateur) {
        throw new AccessDeniedException('Cet utilisateur n\'existe pas');
      }
      $form = $this->get('form.factory')->create(
        new AdminUtilisateurForm('1'), $utilisateur
      );
    }
    else {
      // ajout d'un nouvel utilisateur
      $utilisateur = new Utilisateur();
      $form = $this->get('form.factory')->create(
        new AdminUtilisateurForm('0'), $utilisateur
      );
    }

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {
        /* @var $userManipulator \FOS\UserBundle\Util\UserManipulator */
        $userManipulator = $this->get('fos_user.util.user_manipulator');
        $patternUsername = '#^[a-z0-9]+$#i';
        $patternEmail = '#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';

        //s'il s'agit d'un nouvel utilisateur
        if (!isset($id_user)) {

          if (!preg_match($patternUsername, $form['username']->getData())) {
            $message = 'L\'identifiant contient des caractères interdits';
          }
          elseif (!preg_match($patternEmail, $form['email']->getData())) {
            $message = 'L\'adresse mail n\'est pas valide';
          }
          elseif (!preg_match($patternUsername, $form['password']->getData()) or strlen($form['password']->getData()) < 6) {
            $message = 'Le mot de passe n\'est pas valide et doit contenir au moins 6 caractères alphanumériques';
          }
          else {

            $isExistEmail = $em->getRepository('PatUtilisateurBundle:Utilisateur')->checkEmailCompteAction(
              $form['email']->getData()
            );
            if (!$isExistEmail) {

              $isExistUsername = $em->getRepository('PatUtilisateurBundle:Utilisateur')->checkUsernameCompteAction(
                $form['username']->getData()
              );

              if (!$isExistUsername) {
                $utilisateur->setEmailCanonical($form['email']->getData());
                $utilisateur->setEnabled(true);
                $utilisateur->setAdmin();
                $utilisateur->setCGU(1);
                $utilisateur->setNationalite('fr');
                $utilisateur->setLangueParle('fr');

                //gestion du mot de passe
                $encoder = $this->container->get('security.encoder_factory')->getEncoder($utilisateur);
                $utilisateur->setPassword($encoder->encodePassword($utilisateur->getPassword(), $utilisateur->getSalt()));

                $em->persist($utilisateur);
                $em->flush();

                // Il faut l'affecter après un premier enregistrement.
                $utilisateur->setUsername($form['username']->getData());
                $utilisateur->setUsernameCanonical($form['username']->getData());
                $em->flush();

                $this->addRole($utilisateur, $form['role']->getData());

                $message = 'Utilisateur ajouté avec succès !';
                $this->container->get('session')->getFlashBag()->add('flash', $message);

                return new RedirectResponse($this->container->get('router')->generate('pat_admin_utilisateur_index'));
              }
              else {
                $message = 'Cette identifiant existe déjà';
              }
            }
            else {
              $message = 'Cette adresse mail existe déjà';
            }
          }
        }
        else {
          //s'il s'agit d'une modification de compte
          if (!preg_match($patternEmail, $form['email']->getData())) {
            $message = 'L\'adresse mail n\'est pas valide';
          }
          else {
            $isExistEmail = $em->getRepository('PatUtilisateurBundle:Utilisateur')->checkEmailCompteAction(
              $form['email']->getData(), $id_user
            );

            if (!$isExistEmail) {
              $utilisateur->setEmailCanonical($form['email']->getData());
              $em->persist($utilisateur);
              $em->flush();

              // Mise à jour des rôles.
              $role = $form['role']->getData();
              $currentUser = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneById($id_user);
              if (!in_array($role, $currentUser->getRoles())) {
                $this->removeRoles($currentUser);
                $this->addRole($currentUser, $role);
              }

              $message = 'Utilisateur modifié avec succès !';
            }
            else {
              $message = 'Cette adresse mail existe déjà';
            }
          }
        }//end if utilisateur
      }
    }

    return $this->container->get('templating')->renderResponse(
        'PatCompteBundle:AdminUtilisateur:ajouter.html.twig', array(
        'form' => $form->createView(),
        'message' => $message,
        'utilisateur' => $utilisateur,
        'id_user' => $id_user,
    ));
  }

  /**
   * Retire d'un compte les rôles existants.
   *
   * @param \Pat\UtilisateurBundle\Entity\Utilisateur $user
   * @author AG
   */
  protected function removeRoles($user)
  {
    /* @var $userManipulator \FOS\UserBundle\Util\UserManipulator */
    $userManipulator = $this->get('fos_user.util.user_manipulator');
    // Retrait des rôles existants.
    foreach ($user->getRoles() as $role) {
      $userManipulator->removeRole($user->getUserName(), $role);
    }
  }

  /**
   * Ajoute un rôle à un compte.
   *
   * @param \Pat\UtilisateurBundle\Entity\Utilisateur $user
   * @param string $role
   * @author AG
   */
  protected function addRole($user, $role)
  {
    /* @var $userManipulator \FOS\UserBundle\Util\UserManipulator */
    $userManipulator = $this->get('fos_user.util.user_manipulator');
    // Ajout du rôle sélectionné.
    $userManipulator->addRole($user->getUserName(), $role);
    // Ajout superadmin.
    if ($role == 'ROLE_ADMIN') {
      $userManipulator->promote($user->getUserName());
    }
  }

  //lister les utilisateurs
  public function indexAction($message = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $arrayRoles = $user->getRoles(); //on récupère les droits


    $utilisateur = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findBy(array("type_utilisateur" => "9"), array('id' => 'desc'));


    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminUtilisateur:liste.html.twig', array('utilisateur' => $utilisateur, 'message' => $message, 'role' => $arrayRoles[1], 'user' => $user)
    );
  }

  public function desactiverAction($id_user)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    // modification d'un utilisateur
    $utilisateur = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array("id" => $id_user, "type_utilisateur" => "9"));
    if (!$utilisateur) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $utilisateur->setEnabled('0');
    $em->persist($utilisateur);
    $em->flush();

    header("Location:".$_SERVER["HTTP_REFERER"]);
    exit;
  }

  public function activerAction($id_user)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    // modification d'un utilisateur
    $utilisateur = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array("id" => $id_user, "type_utilisateur" => "9"));
    if (!$utilisateur) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $utilisateur->setEnabled('1');
    $em->persist($utilisateur);
    $em->flush();

    header("Location:".$_SERVER["HTTP_REFERER"]);
    exit;
  }

  //modification du mot de passe
  public function passwordAction($id_user = null)
  {
    $this->locale = $this->container->get('request')->getLocale();
    $message = '';
    $formPassword = array();
    $em = $this->container->get('doctrine')->getManager();


    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }


    if (!$id_user) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }
    else {
      $utilisateur = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array("id" => $id_user));
      if (!$utilisateur) {
        throw new AccessDeniedException("Cet utilisateur n'existe pas");
      }

      $arrayRoles = $user->getRoles(); //on test les droits
      //
			//on ne peut pas modifier un mot de passe d'un utilisateur interne si on n'est pas super admin ou bien si ce n'est pas le notre
      if ($arrayRoles[1] != "ROLE_SUPER_ADMIN" and $user_context->getId() != $id_user) {
        throw new AccessDeniedException("Accès refusé");
      }
    }

    $form = $this->container->get('form.factory')->create(new AdminUtilisateurPasswordForm());
    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST') {
      $pattern = "#^[a-z0-9]+$#i";
      $formPassword = $request->request->get('password');

      if (!preg_match($pattern, $formPassword['password_new']) or strlen($formPassword['password_new']) < 6) {
        $message = "Le mot de passe n'est pas valide et doit contenir au moins 6 caractères alphanumériques";
      }
      else if ($formPassword['password_new'] != $formPassword['password_new_again']) {
        $message = "Les mots de passe ne sont pas identiques";
      }
      else {

        $encoder = $this->container->get('security.encoder_factory')->getEncoder($utilisateur);
        $utilisateur->setPassword($encoder->encodePassword($formPassword['password_new'], $utilisateur->getSalt()));

        $em->persist($utilisateur);
        $em->flush();

        $message_email = \Swift_Message::newInstance()
          ->setSubject("Votre mot de passe à été changé par un admin- ClassAppart ®")
          ->setFrom('contact@class-appart.com')
          ->setTo($utilisateur->getEmail());


//                                if($utilisateur->getMailsSecond())
//                                    $message_email->setCc(explode(',', $utilisateur->getMailsSecond()));
        if ($utilisateur->getSecondEmail()) {
          $message_email->setCc($utilisateur->getSecondEmail());
        }

        $htmlBody = $this->container->get('templating')->render('PatCompteBundle:AdminUtilisateur:email_reset_password.html.twig', array('password' => $formPassword['password_new']));

        $message_email->setBody($htmlBody, 'text/html');

        $this->container->get('mailer')->send($message_email);
//                            die;

        $message = 'Mot de passe modifié avec succès !';
        $this->container->get('session')->getFlashBag()->add(
          'notice', 'Mot de passe modifié avec succès !'
        );

        if ($utilisateur->getType() == "Propriétaire")
          return new RedirectResponse($this->container->get('router')->generate('pat_admin_proprietaire_afficher', array('id_proprio' => $utilisateur->getId())));
        elseif ($utilisateur->getType() == "Locataire")
          return new RedirectResponse($this->container->get('router')->generate('pat_admin_locataire_afficher', array('id_locataire' => $utilisateur->getId())));
      }
    }

    return $this->container->get('templating')->renderResponse(
        'PatCompteBundle:AdminUtilisateur:password.html.twig', array(
        'form' => $form->createView(),
        'message' => $message,
        'utilisateur' => $utilisateur,
    ));
  }

  /**
   * Supprime un compte.
   *
   * @param Request $request
   * @param int $userId
   * @return RedirectResponse
   * @author AG
   */
  public function supprimerAction(Request $request, $userId)
  {
    $em = $this->container->get('doctrine')->getManager();
    $user = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array(
      'id' => $userId
    ));
    $em->remove($user);
    $em->flush();

    $this->get('session')->getFlashBag()->add('notice', 'Compte supprimé.');

    return $this->redirect($this->generateUrl('pat_admin_utilisateur_index'));
  }

}
