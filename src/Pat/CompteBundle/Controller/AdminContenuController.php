<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\CompteBundle\Entity\Contenu;
use Pat\CompteBundle\Form\ContenuForm;
use Pat\UtilisateurBundle\Entity\Utilisateur;
use Pat\CompteBundle\Repository\ContenuRepository;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;

class AdminContenuController extends ContainerAware
{

  //afficher le formulaire d'ajout d'une page
  public function ajouterAction($id = null, $langue = "fr", $message = null)
  {

    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    if (isset($id)) {
      $contenu = $em->find('PatCompteBundle:Contenu', $id);

      $contenu1 = $em->find('PatCompteBundle:Contenu', $contenu->getIdParent());
      $contenu->setIdParent($contenu1);
    }
    else {
      // ajout d'un nouvel page
      $contenu = new Contenu();
      $contenu->setTri(99);
      $contenu->setAfficheMenu(0);
      $contenu->setStatut(0);
      $contenu->setOnSidebar(0);
    }

    $id_parent = "0";
    $form = $this->container->get('form.factory')->create(new ContenuForm($id_parent, $langue), $contenu);

    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST') {

      $form->bind($request);

      if ($form->isValid()) {
        $contenu->setUtilisateur($user);
        $contenu->setLangue($langue);

        if ($contenu->getIdParent())
          $contenu->setIdParent($contenu->getIdParent()->getId());
        else
          $contenu->setIdParent("0");

        //si l'url est vide, on prend le titre
        if ($contenu->getUrl() == "")
          $contenu->setUrl($em->getRepository('PatCompteBundle:Appartement')->string2url(substr($contenu->getTitre(), 0, 60)));
        else
          $contenu->setUrl($em->getRepository('PatCompteBundle:Appartement')->string2url($contenu->getUrl()));

        //on test que l'url n'existe pas déjà
        $contenu->setUrl($this->existUrl($contenu->getUrl(), $id));

        if ($contenu->getMetaTitre() == "")
          $contenu->setMetaTitre($contenu->getTitre());

        $em->persist($contenu);
        $em->flush();

        if (isset($id))
          $message = 'Page modifiée avec succès !';
        else
          $message = 'Page ajoutée avec succès !';

        $contenu1 = $em->find('PatCompteBundle:Contenu', $contenu->getIdParent());
        $contenu->setIdParent($contenu1);
        $contenu = $em->find('PatCompteBundle:Contenu', $contenu->getId());
        $form = $this->container->get('form.factory')->create(new ContenuForm($id_parent, $langue), $contenu);
      }
    }

    $contenus = $em->getRepository('PatCompteBundle:Contenu')->findOneBy(array('id' => $contenu->getId())); //on récupère la page
    $contenuMessage = "";

    if ($contenus)
      $contenuMessage = $em->getRepository('PatCompteBundle:Contenu')->findBy(array("id_parent" => $contenus->getId(), "type" => "Message"), array('tri' => 'asc')); //on récupère tous les messages liés à la page


    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminContenu:ajouter.html.twig', array(
        'form' => $form->createView(),
        'message' => $message,
        'contenus' => $contenus,
        'contenuMessage' => $contenuMessage,
        'id' => $id,
        'langue' => $langue,
    ));
  }

  //lister les appartements
  public function indexAction($message = null, $langue = "fr")
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    //si on fait un tri
    $request = $this->container->get('request');
    if ($request->getMethod() == 'POST') {
      if ($_POST["tri"] != "0") {
        $tri = $_POST["tri"];
        $id_contenu = $_POST["id_contenu"];

        $contenuUpdate = $em->getRepository('PatCompteBundle:Contenu')->find($id_contenu);
        $contenuUpdate->setTri($tri);

        $em->persist($contenuUpdate);
        $em->flush();
      }
    }

    $contenu = $em->getRepository('PatCompteBundle:Contenu')->findBy(array("type" => "Page", "id_parent" => "0", "langue" => $langue), array('tri' => 'asc'));
    $contenuMessage = $em->getRepository('PatCompteBundle:Contenu')->findBy(array("langue" => $langue), array('tri' => 'asc'));

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminContenu:liste.html.twig', array(
        'contenu' => $contenu,
        'contenuMessage' => $contenuMessage,
        'message' => $message,
        'langue' => $langue)
    );
  }

  //Afficher les pages dans le menu en haut de page
  public function menuAction($message = null, $langue = "fr")
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    //on récupère toutes les pages
    $contenu = $em->getRepository('PatCompteBundle:Contenu')->findBy(array("type" => "Page", "id_parent" => "0", "langue" => "fr"), array('tri' => 'asc'));

    //on réduit le nombre de caractères dans le titre
    for ($i = 0; $i < sizeof($contenu); $i++) {
      $contenu[$i]->setTitre(substr(stripslashes($contenu[$i]->getTitre()), 0, 38));
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminContenu:menu.html.twig', array(
        'contenu' => $contenu,
        'message' => $message,
        'langue' => $langue));
  }

  public function publierAction($id)
  {

    $em = $this->container->get('doctrine')->getManager();
    $message = "";

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $contenu = $em->getRepository('PatCompteBundle:Contenu')->findBy(array('id' => $id));

    if (!$contenu) {
      throw new NotFoundHttpException("Page non trouvée");
    }

    $contenu = $em->find('PatCompteBundle:Contenu', $id);

    if ($contenu->getStatut() == "1")
      $contenu->setStatut("0");
    else
      $contenu->setStatut("1");

    $em->persist($contenu);
    $em->flush();

    header("Location:".$_SERVER["HTTP_REFERER"]);
    exit;

    //return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminContenu:liste.html.twig', array('contenu' => $contenu, 'message'=> $message));
  }

  public function supprimerAction($id)
  {
    $em = $this->container->get('doctrine')->getManager();
    $message = "";

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $contenu = $em->getRepository('PatCompteBundle:Contenu')->findOneBy(array('id' => $id));
    if (!$contenu) {
      throw new NotFoundHttpException("Page non trouvée");
    }

    $page = $contenu->getTitre();

    //s'il y a des sous rubriques, alors on les supprime également.
    $ss_contenu = $em->getRepository('PatCompteBundle:Contenu')->findBy(array('id_parent' => $contenu->getId()));
    if ($ss_contenu) {
      foreach ($ss_contenu as $ssc)
        $em->remove($ssc);
    }

    $em->remove($contenu);
    $em->flush();

    $message = "Page '".$page."' supprimée avec succès.";
    $this->container->get('session')->getFlashBag()->add('flash', $message);

    header("Location:".$_SERVER["HTTP_REFERER"]);
    exit;
  }

  //vérifie qu'une url existe pas déjà
  public function existUrl($url, $id = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    $contenu = $em->getRepository('PatCompteBundle:Contenu')->findOthersUrl($url, $id);

    $url_modif = $url;
    $i = 1;
    while ($contenu) {
      $i++;
      $url_modif = $url.$i;
      $contenu = $em->getRepository('PatCompteBundle:Contenu')->findOthersUrl($url_modif, $id);
    }

    return $url_modif;
  }

}
