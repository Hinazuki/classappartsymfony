<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Pat\CompteBundle\Entity\Piece;
use Pat\CompteBundle\Entity\Media;
use Pat\CompteBundle\Entity\TypePiece;
use Pat\CompteBundle\Entity\Appartement;
use Pat\CompteBundle\Entity\Ville;
use Pat\CompteBundle\Entity\Tarif;
use Pat\UtilisateurBundle\Entity\Utilisateur;
use Pat\CompteBundle\Repository\AppartementRepository;
use Pat\CompteBundle\Form\AdminAppartementForm;
use Pat\CompteBundle\Form\AdminAppartementRechercheForm;
use Pat\CompteBundle\Form\AdminAppartementRechercheAvanceeForm;
use Pat\CompteBundle\Form\AppartementValidForm;
use Doctrine\ORM\EntityRepository;
use Pat\CompteBundle\Lib\RechercheProprietaire;
use Pat\CompteBundle\Lib\CalendrierAppartement;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PHPExcel;
use PHPExcel_IOFactory;

class AdminAppartementController extends ContainerAware
{

  //passer les dates au format EN
  public function toDateEn($date)
  {
    return substr($date, 6, 4)."-".substr($date, 3, 2)."-".substr($date, 0, 2);
  }

  //afficher le formulaire d'ajout d'un appart
  public function ajouterAction(Request $request, $id = null, $id_proprio = null, $message = null, $erreur = null)
  {
    $session = $request->getSession();

    $this->locale = $this->container->get('request')->getLocale();
    $validation = '';
    $em = $this->container->get('doctrine')->getManager();


    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    if (isset($id)) {
      // On stock l'id appartement à modifier dans la session afin de l'utiliser si on veut associer l'appartement à un proprietaire
      $session->set('admin_modif_appartement_id', $id);

      $appartement = $em->find('PatCompteBundle:Appartement', $id);
      $appartement->setUpdatedBy($user);
    }
    else {
      // ajout d'un nouvel appartement
      $appartement = new Appartement();
      $appartement->setStatut(0);

      $appartement->setEtatInterieur("0");
      $appartement->setCalme("0");
      $appartement->setClair("0");
      $appartement->setNeufAncien("0");
      $appartement->setStandingImmeuble("0");
      $appartement->setCmc("0");
      $appartement->setIsResa("0");
      $appartement->setIsLabeliser("0");
      $appartement->setIsReloger("0");
      $appartement->setIsSelection("0");

      $appartement->setCreatedBy($user);

      if (!is_null($id_proprio)) {
        $proprio = $em->find('PatUtilisateurBundle:Utilisateur', $id_proprio);
        $appartement->setUtilisateur($proprio);
      }
    }


    $form = $this->container->get('form.factory')->create(new AdminAppartementForm(), $appartement);

    $has_digicode = $appartement->getDigicode() == null ? false : true;
    $form['has_digicode']->setData($has_digicode);

    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST') {
      $statutActuel = $appartement->getStatut();

      $form->bind($request);

      if ($form->isValid()) {

        $newstatut = $appartement->getStatut();

        if (!$id) {
          // url temporaire le temps de générer la référence du bien
          $appartement->setUrlFr("url-temp");
          $appartement->setUrlEn("url-temp");
          $appartement->setUrlDe("url-temp");
        }

        //Téléchargement du fichier diagnostic DPE
        $appartement->setFichierDiagnostic("");
        $dir = __DIR__.'/../../../../web/images/photos/biens/';
        $ref_appart = $appartement->getReference();
        $file = $form['fichier_diagnostic']->getData();

        //on test le poids du fichier
        if (!empty($file)) {
          if (filesize($file) > 4000000) {
            $message = "Le du fichier DPE ne doit pas dépasser 4Mo";
            return $this->container->get('templating')->renderResponse(
                'PatCompteBundle:AdminAppartement:ajouter.html.twig', array(
                'form' => $form->createView(),
                'formValid' => $formValid->createView(),
                'message' => $message,
                'appartement' => $appartement,
                'validation' => '',
                'erreur' => $erreur,
                'id' => $id,
            ));
          }
          else {
            if (!file_exists($dir.$ref_appart)) {
              mkdir($dir.$ref_appart);
            }

            if (!file_exists($dir.$ref_appart."/diagnostic")) {
              mkdir($dir.$ref_appart."/diagnostic");
            }

            $extension = $file->guessExtension();

            if ($extension == "pdf" or $extension == "jpg" or $extension == "png" or $extension == "gif" or $extension == "jpeg") {
              $nom_fichier = "DPE-".$ref_appart."-".date("Y-m-d").".".$extension;
              if ($file->move($dir.$ref_appart."/diagnostic", $nom_fichier)) {
                $appartement->setFichierDiagnostic($nom_fichier);
              }
            }
            else {
              $message = "Vous pouvez télécharger des fichier PDF, JPG, GIF, PNG uniquement";
              return $this->container->get('templating')->renderResponse(
                  'PatCompteBundle:AdminAppartement:ajouter.html.twig', array(
                  'form' => $form->createView(),
                  'formValid' => $formValid->createView(),
                  'message' => $message,
                  'appartement' => $appartement,
                  'validation' => '',
                  'erreur' => $erreur,
                  'id' => $id,
              ));
            }
          }
        }

        if ($form['has_digicode']->getData() == false) {
          $appartement->setDigicode(null);
          $has_digicode = false;
        }
        else
          $has_digicode = true;


        $em->persist($appartement);
        $em->flush();

        if (isset($id)) {
          $message = 'Bien modifié avec succès !';

          // Le bien est validé on envoie un mail au proprio pour l'en avertir
          if ($statutActuel != $newstatut && $newstatut == '4') {
            $AppartProprio = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($appartement->getUtilisateur());

            if (!empty($AppartProprio)) {
              $message_email = \Swift_Message::newInstance()
                ->setSubject("Votre bien à été validé - ClassAppart ®")
                ->setFrom('contact@class-appart.com')
                ->setBcc($this->container->getParameter('mail_admin'))
                ->setTo($AppartProprio->getEmail());

//                                if($AppartProprio->getMailsSecond())
//                                    $message_email->setCc(explode(',', $AppartProprio->getMailsSecond()));
              if ($AppartProprio->getSecondEmail()) {
                $message_email->setCc($AppartProprio->getSecondEmail());
              }


              $htmlBody = $this->container->get('templating')->render('PatCompteBundle:AdminAppartement:email_validation_done.html.twig', array('utilisateur' => $AppartProprio, 'appartement' => $appartement));

              $message_email->setBody($htmlBody, 'text/html');

              $this->container->get('mailer')->send($message_email);
            }
          }
//                        die;
        }
        else {
          $appartement->setReference($appartement->getId() + 5000);

          if ($appartement->getIsMeuble() == true) {
            $meublefr = 'meuble';
            $meubleen = 'furnished';
            $meublede = 'mobliert';
          }
          else {
            $meublefr = 'non-meuble';
            $meubleen = 'unfurnished';
            $meublede = 'unmobliert';
          }

          switch ($appartement->getType()) {
            case 'APPARTEMENT':
              $typefr = 'appartement';
              $typeen = 'apartment';
              $typede = 'wohnung';
              break;
            case 'MAISON':
              $typefr = 'maison';
              $typeen = 'house';
              $typede = 'haus';
              break;
            case 'STUDIO':
              $typefr = 'studio';
              $typeen = 'studio';
              $typede = 'studio';
              break;
            case 'LOFT':
              $typefr = 'loft';
              $typeen = 'loft';
              $typede = 'loft';
              break;
            case 'GARAGE':
              $typefr = 'garage';
              $typeen = 'garage';
              $typede = 'garage';
              break;
            case 'CHAMBRE':
              $typefr = 'chambre';
              $typeen = 'room';
              $typede = 'zimmer';
              break;

            default:
              break;
          }
          $url_fr = "location-".$typefr."-".$meublefr."-".strtolower($appartement->getVille())."-".$appartement->getReference();
          $url_fr = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_fr);
          $appartement->setUrlFr($url_fr);

          $url_en = "renting-".$typeen."-".$meubleen."-".strtolower($appartement->getVille())."-".$appartement->getReference();
          $url_en = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_en);
          $appartement->setUrlEn($url_en);

          $url_de = "mieten-".$typede."-".$meublede."-".strtolower($appartement->getVille())."-".$appartement->getReference();
          $url_de = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_de);
          $appartement->setUrlDe($url_de);
          //Génération des urls
          /* $url_fr = "location-".$appartement->getType()."-meuble-".$appartement->getVille()."-".$appartement->getReference();
            $url_fr = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_fr);
            $appartement->setUrlFr($url_fr);

            $url_en = "apartment-".$appartement->getType()."-furnished-".$appartement->getVille()."-".$appartement->getReference();
            $url_en = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_en);
            $appartement->setUrlEn($url_en);

            $url_de = "wohnung-".$appartement->getType()."-mobliert-".$appartement->getVille()."-".$appartement->getReference();
            $url_de = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_de);
            $appartement->setUrlDe($url_de); */

          $em->flush();

          $message = 'Bien ajouté avec succès !';
          $this->container->get('session')->getFlashBag()->add('notice', $message);
          //return new RedirectResponse($this->container->get('router')->generate('pat_admin_tarif_ajouter', array("id_bien"=>$appartement->getId())));
          return new RedirectResponse($this->container->get('router')->generate('pat_admin_appartement_modif_proprietaire', array("id" => $appartement->getId())));
        }
      }
      else {
        //$message='Veuillez remplir tous les champs avec une étoile !';
        // On récupère toutes les erreurs pour les afficher en haut du formulaire
        foreach ($form->getChildren() as $child) {
          if ($child->hasErrors()) {
            foreach ($child->getErrors() as $error)
              $message .= $this->container->get('translator')->trans($error->getMessageTemplate())."\n";
          }
        }
      }
    }

    if ($session->get('association_appartement_proprietaire_message')) {
      $message = $session->get('association_appartement_proprietaire_message');
      $session->set('association_appartement_proprietaire_message', '');
    }



    return $this->container->get('templating')->renderResponse(
        'PatCompteBundle:AdminAppartement:ajouter.html.twig', array(
        'form' => $form->createView(),
        'message' => $message,
        'appartement' => $appartement,
        'validation' => '',
        'erreur' => $erreur,
        'id' => $id,
        'has_digicode' => $has_digicode
    ));
  }

  // Fonction qui affiche le detail d'un appartement
  // Erreur est un parametre reçu de la sélection de date du controlleur réservation.
  // erreur = 1 signifie que la date saisie doit être suppérieur à la date du jour
  // erreur = 2 signifique qu'une réservation existe déjà pour ce genre de dates.
  public function detailAction(Request $request, $id)
  {
    // Variables de traitement de l'affichage des pièces
    $i = 0;
    $j = 0;
    $message = "";
    // Fin variables

    $is_selection = 0;

    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $id));
    if (!$appartement) {
      throw new NotFoundHttpException("Cet appartement est introuvable");
    }

    $media = $em->getRepository('PatCompteBundle:Media')->findBy(array('appartement' => $appartement->getId()), array('tri' => 'asc'));
    $pieces = $em->getRepository('PatCompteBundle:Piece')->findBy(array('appartement' => $appartement->getId()));

    // Calendrier des disponibilités sur les 12 prochains mois
    $tabCal = "";
    for ($i = 0; $i <= 12; $i++) {
      $tabCal .= '<li><table border="0" cellpadding="8" cellspacing="0" align="center"><tr valign="top">';

      $date = date("d-m-Y", mktime(0, 0, 0, date("m") + $i, date("j"), date("Y"))); //on récupère le mois suivant
      $tabDate = explode("-", $date);
      $month = $tabDate[1];
      $year = $tabDate[2];

      $calendrier = new CalendrierAppartement($month, $year, $appartement->getId(), $em->getRepository('PatCompteBundle:Calendrier')->ListeResaByApp($appartement->getId()));

      $tabCal .= "<td><div class='calendarResa' style=''>".$calendrier->affichageFront("User")."</div></td>";
      $tabCal .= '</tr></table></li>';
      // Fin Calendrier
    }

    // On récupère la sélection perso des appartements
    $cookies = $request->cookies;
    if ($cookies->has('PATcookie')) {
      $recup = explode("-", $cookies->get('PATcookie'));
      $selection = $em->getRepository('PatCompteBundle:Appartement')->getManyAppartementById($recup, 5); // le second parametre est la limite
      foreach ($selection as $s):
        if ($s->getUrlFr() == $appartement->getUrlFr()) {
          $is_selection = 1;
        }
      endforeach;
    }
    else {
      $selection = null;
    }

    // On récupere les dates de recherche
    $date_debut = null;
    $date_fin = null;
    $session = $request->getSession();
    $date_debut = $session->get('date_debut');
    $date_fin = $session->get('date_fin');
    $session->set('resa_appartement', $appartement->getId()); // pour pouvoir le réutiliser si réservation

    if ($date_debut) {
      $date_debut = str_replace("/", "", $this->toDateEn($date_debut));
    }
    if ($date_fin) {
      $date_fin = str_replace("/", "", $this->toDateEn($date_fin));
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminAppartement:detail.html.twig', array('appartement' => $appartement,
        'pieces' => $pieces,
        'media' => $media,
        'selection' => $selection,
        'is_selection' => $is_selection,
        'calendrier' => $tabCal,
        'retour_page' => 'pat_front_appartement_detail',
        'message' => $message)
    );
  }

  public function modifProprietaireAction(Request $request, $id = null)
  {
    $session = $request->getSession();

    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user || $user->getTypeUtilisateur() != '9')
      throw new AccessDeniedException('This user does not have access to this section.');

    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $id));

    if (!$appartement)
      throw new NotFoundHttpException("Cet appartement est introuvable");

    $session->set('admin_modif_appartement_id', $id);

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminAppartement:modifProprietaire.html.twig', array('appartement' => $appartement));
  }

  public function selectProprietaireByAjaxAction(Request $request)
  {
    $em = $this->container->get('doctrine')->getManager();
    $nom = '';
    $prenom = '';
    $reference = '';
    $type = '';

    // On génère la liste déroulante et le bouton submit en fonction des informations trouvées
    if ($request->isXmlHttpRequest()) {
      $infos_proprio = $request->request->get('infosproprio');

      $tab_infos_proprio = explode("-", $infos_proprio);

      $nom = $tab_infos_proprio[0];
      $prenom = $tab_infos_proprio[1];
      $type = $tab_infos_proprio[2];
      $reference = $tab_infos_proprio[3];

      if ($type == 'piedaterre')
        $type = "9";
      else
        $type = "2";

      if ($nom == '' && ($reference == '' || $reference == "P"))
        $nom = "xxxx";
    }

    // On recherche le nombre de résultats afin d'afficher un message si la recherche n'a troué aucun propriétaires
    $qb = $em->createQueryBuilder()
      ->select("count(u)")
      ->from("Pat\UtilisateurBundle\Entity\Utilisateur", 'u')
      ->where("u.type_utilisateur like '%".$type."%'");

    if ($nom && $nom != '')
      $qb->andWhere("u.nom like '%".$nom."%'");

    if ($prenom && $prenom != '')
      $qb->andWhere("u.prenom like '%".$prenom."%'");

    if ($reference && $reference != '')
      $qb->andWhere("u.username like '%".$reference."%'");

    $nb_result = $qb->getQuery()->getSingleScalarResult();


    $form = $this->container->get('form.factory')->createBuilder('form')
      ->add('utilisateurs', 'entity', array(
        'class' => 'Pat\UtilisateurBundle\Entity\Utilisateur',
        'label' => "Résultats de la recherche",
        'empty_value' => "Sélectionnez le Propriétaire",
        'query_builder' => function(EntityRepository $er) use ($nom, $prenom, $reference, $type) {
          $qb = $er->createQueryBuilder('u')
            ->where("u.type_utilisateur like '%".$type."%'");

          if ($nom && $nom != '')
            $qb->andWhere("u.nom like '%".$nom."%'");

          if ($prenom && $prenom != '')
            $qb->andWhere("u.prenom like '%".$prenom."%'");

          if ($reference && $reference != '')
            $qb->andWhere("u.username like '%".$reference."%'");

          return $qb;
        },
      ))
      ->getForm();

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminAppartement:selectProprietaire.html.twig', array('formProprio' => $form->createView(), 'nb_result' => $nb_result));
  }

  public function associationAppartementProprietaireAction(Request $request)
  {
    if ($request->getMethod() == 'POST') {
      $session = $request->getSession();

      $id_proprietaire = $_POST['form']['utilisateurs'];
      $id_appartement = $session->get('admin_modif_appartement_id');

      $em = $this->container->get('doctrine')->getManager();

      $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $id_appartement));
      $utilisateur = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array('id' => $id_proprietaire));

      $appartement->setUtilisateur($utilisateur);

      $em->persist($appartement);
      $em->flush();

      $this->container->get('session')->getFlashBag()->add('notice', "Le propriétaire a correctement été associé");

      return new RedirectResponse($this->container->get('router')->generate('pat_admin_appartement_modif_proprietaire', array('id' => $id_appartement)));
    }
  }

  public function testTabVide($tab)
  {
    foreach ($tab as $t):
      if ($t != null && $t != "") {
        return false;
      }
    endforeach;
    return true;
  }

  //lister les appartements
  public function indexAction($message = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    $message = "";

    $session = $this->container->get('request')->getSession();

    $formRecherche = $this->container->get('form.factory')->create(new AdminAppartementRechercheForm());
    $formRechercheAvancee = $this->container->get('form.factory')->create(new AdminAppartementRechercheAvanceeForm());
    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST') {

      $formRecherche->bind($request);
      $formRechercheAvancee->bind($request);

      if ($formRecherche->isValid()) {
        $appartementrecherche = $request->request->get('appartementrecherche');

        $admin_search_bien = array();
        $admin_search_bien['type'] = str_replace("'", " ", $appartementrecherche["type"]);
        $admin_search_bien['nb_pieces'] = str_replace("'", " ", $appartementrecherche["nb_pieces"]);
        $admin_search_bien['ville'] = str_replace("'", " ", $appartementrecherche["ville"]);
        $admin_search_bien['rue'] = str_replace("'", " ", $appartementrecherche["rue"]);
        $admin_search_bien['reference'] = str_replace("'", " ", $appartementrecherche["reference"]);
        $admin_search_bien['statut'] = str_replace("'", " ", $appartementrecherche["statut"]);

        $session->set('admin_search_bien', $admin_search_bien);
      }
      elseif ($formRechercheAvancee->isValid()) {
        $appartementrechercheavancee = $request->request->get('appartementrechercheavancee');

        $admin_search_bien = array();
        $admin_search_bien['type'] = str_replace("'", " ", $appartementrechercheavancee["type"]);
        $admin_search_bien['nb_pieces'] = str_replace("'", " ", $appartementrechercheavancee["nb_pieces"]);
        $admin_search_bien['ville'] = str_replace("'", " ", $appartementrechercheavancee["ville"]);
        $admin_search_bien['rue'] = str_replace("'", " ", $appartementrechercheavancee["rue"]);
        $admin_search_bien['reference'] = str_replace("'", " ", $appartementrechercheavancee["reference"]);
        $admin_search_bien['statut'] = str_replace("'", " ", $appartementrechercheavancee["statut"]);

        $session->set('admin_search_bien', $admin_search_bien);
      }
      else {
        $appartement = array();
        //$message = "Veuillez entrer au moins un des critères";
      }
    }
    else {
      if ($this->container->get('request')->query->get('page')) {
        // On récupère les éléments de recherche en session
        $admin_search_bien = $session->get('admin_search_bien');
      }
      else {
        // Si page n'est pas définie, on souhaite afficher tous les biens
        $admin_search_bien = array();
        $admin_search_bien['type'] = "";
        $admin_search_bien['nb_pieces'] = "";
        $admin_search_bien['ville'] = "";
        $admin_search_bien['rue'] = "";
        $admin_search_bien['reference'] = "";
        $admin_search_bien['statut'] = "";

        $session->set('admin_search_bien', $admin_search_bien);
      }
    }




//        $media = array();
//        for($i=0; $i < sizeof($appartement);$i++){
//            $photo[$i] = $em->getRepository('PatCompteBundle:Media')->findBy(array('appartement' => $appartement[$i]->getId()), array('tri' => 'asc'));
//            if($photo[$i]){
//                    $media[$i] = $photo[$i][0];
//            }else{
//                    $media[$i] = null;
//            }
//        }

    $count = $em->getRepository('PatCompteBundle:Appartement')->searchAppartementQuery($admin_search_bien['type'], $admin_search_bien['nb_pieces'], $admin_search_bien['ville'], $admin_search_bien['rue'], $admin_search_bien['reference'], $admin_search_bien['statut'], null, null, 'count')->getSingleScalarResult();


    $query = $em->getRepository('PatCompteBundle:Appartement')->searchAppartementQuery($admin_search_bien['type'], $admin_search_bien['nb_pieces'], $admin_search_bien['ville'], $admin_search_bien['rue'], $admin_search_bien['reference'], $admin_search_bien['statut']);
    $query->setHint('knp_paginator.count', $count);

    $paginator = $this->container->get('knp_paginator');
    $pagination = $paginator->paginate(
      $query, $this->container->get('request')->query->get('page', 1), 20, array('distinct' => false)
    );

    return $this->container->get('templating')->renderResponse(
        'PatCompteBundle:AdminAppartement:liste.html.twig', array('appartement' => $pagination,
        'message' => $message,
        'formRechercheAvancee' => $formRechercheAvancee->createView(),
        'formRecherche' => $formRecherche->createView(),
        "isSearchBien" => $admin_search_bien['type']
    ));
  }

  //Rechercher un appartement
  public function rechercherAction($message = null)
  {

    $formRecherche = $this->container->get('form.factory')->create(new AdminAppartementRechercheForm());
    $request = $this->container->get('request');
    if ($request->getMethod() == 'POST') {
      $formRecherche->bind($request); //on conserve les données dans le formulaire
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminAppartement:rechercher.html.twig', array('message' => $message, 'formRecherche' => $formRecherche->createView())
    );
  }

  public function supprimerAction($id)
  {
    $em = $this->container->get('doctrine')->getManager();
    $message = "";

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());


    // modification d'un appartement existant : on recherche ses données en vérifiant que l'utilisateur à le droit me modifier l'appart
    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findBy(array('utilisateur' => $user->getId(), 'id' => $id));

    /*
      //suppression des médias
      $repositoryMedia = $em->getRepository('PatCompteBundle:Media');
      $medias = $repositoryMedia->findBy(array('appartement' => $appartement->getId()));

      for($i=0; $i<sizeof($medias); $i++) {
      @unlink(__DIR__."/../../../../web/images/photos/biens/".$medias[$i]->getFichier());
      @unlink(__DIR__."/../../../../web/images/photos/biens/small/".$medias[$i]->getFichier());
      @unlink(__DIR__."/../../../../web/images/photos/biens/tall/".$medias[$i]->getFichier());
      $em->remove($medias[$i]);
      }

      //suppression des pièces
      $repositoryPiece = $em->getRepository('PatCompteBundle:Piece');
      $pieces = $repositoryPiece->findBy(array('appartement' => $appartement->getId()));

      for($i=0;$i<sizeof($pieces);$i++)
      $em->remove($pieces[$i]);

      //suppression de l'appartement
      $em->remove($appartement);
      $em->flush(); */

    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $id));

    if (!$appartement)
      throw new NotFoundHttpException("Bien non trouvé");

    $appartement->setStatut(9);
    $em->persist($appartement);
    $em->flush();

    $this->container->get('session')->getFlashBag()->add('notice', "Le bien a été supprimé");

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_appartement_index'));

    /* header("Location:".$_SERVER["HTTP_REFERER"]);
      exit; */
    //return $this->indexAction($message);
  }

  public function validerAction($id)
  {
    $em = $this->container->get('doctrine')->getManager();
    $message = "";

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    // modification d'un appartement existant : on recherche ses données en vérifiant que l'utilisateur à le droit me modifier l'appart
    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findBy(array('utilisateur' => $user->getId(), 'id' => $id));

    if (!$appartement) {
      throw new NotFoundHttpException("Bien non trouvé");
    }



    $appartement = $em->find('PatCompteBundle:Appartement', $id);
    if ($appartement->getStatut() != "0") {
      throw new NotFoundHttpException("Cet appartement est déjà validé.");
    }


    $request = $this->container->get('request');
    $choice1 = $choice2 = "";
    if ($request->getMethod() == 'POST') {
      //on récupère les données du formulaire
      $appartementvalid = $request->request->get('appartementvalid');
      if (isset($appartementvalid["choice1"][0])) {
        $choice1 = "1";
      }
      if (isset($appartementvalid["choice2"][0])) {
        $choice2 = "1";
      }

      if ($choice1 != "") {
        $appartement->setStatut(1);
        $appartement->setCmc($choice1);
        $appartement->setIsResa($choice2);
        $em->persist($appartement);
        $em->flush();

        // Mail to class appart
        $message = \Swift_Message::newInstance()
          ->setSubject("Demande de validation d'un bien - classAppart ®")
          ->setFrom('contact@class-appart.com')
          ->setBcc($this->container->getParameter('mail_admin'))
          ->setTo($user->getEmail());

//                            if($user->getMailsSecond())
//                                $message_email->setCc(explode(',', $user->getMailsSecond()));
        if ($user->getSecondEmail()) {
          $message_email->setCc($user->getSecondEmail());
        }

        $textBody = $this->container->get('templating')->render('PatCompteBundle:AdminAppartement:email_validation.txt.twig', array('appartement' => $appartement, 'utilisateur' => $user));
        $htmlBody = $this->container->get('templating')->render('PatCompteBundle:AdminAppartement:email_validation.html.twig', array('appartement' => $appartement, 'utilisateur' => $user));

        if (!empty($htmlBody)) {
          $message->setBody($htmlBody, 'text/html')
            ->addPart($textBody, 'text/plain');
        }
        else
          $message->setBody($textBody);

        $this->container->get('mailer')->send($message);

        $message = "Votre bien REF : ".$appartement->getReference()." est désormais en cours de validation auprès de nos services.<br/>
                              Vous serez avertit par email de la validation et de la mise en ligne de votre bien.";
//			  $message = "Votre bien est désormais en cours de validation auprès de nos services.<br/>
//							Vous recevrez un mail vous informant de la mise en ligne de votre bien.";

        return $this->container->get('templating')->renderResponse(
            'PatCompteBundle:AdminAppartement:ajouter.html.twig', array(
            'validation' => $message,
        ));
      }
    }

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_appartement_editer_error', array('id' => $id, 'erreur' => "1")));
  }

  public function dupliquerAction($id)
  {
    $em = $this->container->get('doctrine')->getManager();
    $message = "";

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    // modification d'un appartement existant : on recherche ses données en vérifiant que l'utilisateur à le droit de modifier l'appart
    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $id));

    if (!$appartement)
      throw new NotFoundHttpException("Bien non trouvé");

    if ($appartement->getUtilisateur() != $user && $user->getType() != 'Administrateur')
      throw new Exception("Vous n'avez pas les droits nécessaires pour dupliquer ce bien");

    $appart = new Appartement();
    $appart = clone $appartement; //on duplique l'appartement

    $appart->setStatut(0);
    $appart->setCreatedBy($user);
    $appart->setUpdatedBy($user);

    // On réinitialise le DPE
    $appart->setStatutDiagnostic(null);
    $appart->setDateDiagnostic(null);
    $appart->setFichierDiagnostic(null);
    $appart->setConsommationLettre(null);
    $appart->setConsommationChiffre(null);
    $appart->setGazSerreLettre(null);
    $appart->setGazSerreChiffre(null);

    $em->persist($appart);

    $pieces = $em->getRepository('PatCompteBundle:Piece')->findBy(array('appartement' => $appartement->getId()));

    foreach ($pieces as $piece) {
      $i = $piece->getId();
      $pieceAppart[$i] = new Piece();
      $pieceAppart[$i] = clone $piece; //on duplique les pièces
      $pieceAppart[$i]->setAppartement($appart);
      $pieceAppart[$i]->setEquipement(null);

      // On ajoute les équipements qui ne sont pas ajoutées lors de l'appel à la méthode clone()
      $equipements = $piece->getEquipement();
      foreach ($equipements as $equipement) {
        $pieceAppart[$i]->addEquipement($equipement);
      }

      $em->persist($pieceAppart[$i]);
    }


    $tarifs = $em->getRepository('PatCompteBundle:Tarif')->findBy(array('appartement' => $appartement->getId()));

    foreach ($tarifs as $tarif) {
      $tarifAppart = new Tarif();
      $tarifAppart = clone $tarif; //on duplique les tarifs
      $tarifAppart->setAppartement($appart);
      $em->persist($tarifAppart);
    }

    $em->flush();

    // On set la référence du nouveau bien
    $appart->setReference($appart->getId() + 5000);

    // On met à jour les url avec l'id du nouvel appartement
    $urlfr = explode("-", $appartement->getUrlFr());
    $urlfr[sizeof($urlfr) - 1] = $appart->getReference();
    $urlfr = implode("-", $urlfr);
    $urlen = explode("-", $appartement->getUrlEn());
    $urlen[sizeof($urlen) - 1] = $appart->getReference();
    $urlen = implode("-", $urlen);
    $urlde = explode("-", $appartement->getUrlDe());
    $urlde[sizeof($urlde) - 1] = $appart->getReference();
    $urlde = implode("-", $urlde);

    $appart->setUrlFr($urlfr);
    $appart->setUrlEn($urlen);
    $appart->setUrlDe($urlde);

    // Copie des photos
    $photos = $em->getRepository('PatCompteBundle:Media')->findBy(array('appartement' => $appartement->getId()));
    $dir = __DIR__.'/../../../../web/images/photos/biens/';
    $ref_src = $appartement->getReference();
    $ref_dst = $appart->getReference();

    if (!file_exists($dir.$ref_dst))
      mkdir($dir.$ref_dst);

    foreach ($photos as $photo) {
      copy($dir.$ref_src."/".$photo->getFichier(), $dir.$ref_dst."/".$photo->getFichier());

      $media = new Media();
      $media = clone $photo;
      $media->setAppartement($appart);
      $em->persist($media);
    }

    $em->flush();

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_appartement_index'));
  }

  /**
   *
   * Test si le num appartement existe
   */
  public function checkNumAppartement($reference)
  {
    $em = $this->container->get('doctrine')->getManager();
    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findBy(array('reference' => $reference));

    if ($appartement) {
      return $this->checkNumAppartement(rand(1000, 9999));
    }
    else {
      return $reference;
    }
  }

  public function derniersAjoutsAction($message = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    $message = "";


    $query = $em->createQuery('SELECT a FROM Pat\CompteBundle\Entity\Appartement a ORDER BY a.created_at DESC')
      ->setMaxResults('50');

    $appartement = $query->getResult();


    return $this->container->get('templating')->renderResponse(
        'PatCompteBundle:AdminAppartement:derniersAjouts.html.twig', array('appartement' => $appartement));
  }

  // Fonction qui donne la liste des excels liés aux appartements et propose un formulaire d'ajout
  /* public function listeExcelAction(Request $request)
    {
    $em = $this->container->get('doctrine')->getManager();

    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if(!$user)
    {
    throw new AccessDeniedException('This user does not have access to this section.');
    }

    $form = $this->container->get('form.factory')->createBuilder('form')->add('excel', 'file', array('label' => "Fichier Excel : "))->getForm();

    $dir = __DIR__.'/../../../../web/excel/';
    if ($request->getMethod() == 'POST')
    {
    $form->bind($request);

    if ($form->isValid())
    {
    $file = $form['excel']->getData();

    $extension = $file->guessExtension();
    if (isset($extension) && $extension == "xls")
    {
    $file->move($dir, $file->getClientOriginalName());
    }
    else
    {
    $message = "Le type de fichier n'est pas conforme.";
    }

    }
    else
    {
    $message = "Ce fichier ne peut pas être importé.";
    }
    }


    $fichiers = array();
    $i=0;
    $MyDirectory = opendir($dir) or die('Erreur');
    while($Entry = readdir($MyDirectory)) {

    $test_nom = substr($Entry,-4,4);
    if($test_nom == ".xls")
    {
    $fichiers[$i] = $Entry;
    $i++;
    }
    }
    closedir($MyDirectory);

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminAppartement:listeExcelImport.html.twig',
    array('fichiers'=> $fichiers,'form' => $form->createView()));
    } */

  /* public function supprimeExcelAction($fichier_excel)
    {
    $em = $this->container->get('doctrine')->getManager();

    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if(!$user)
    {
    throw new AccessDeniedException('This user does not have access to this section.');
    }

    $dir = __DIR__.'/../../../../web/excel/';
    unlink($dir.$fichier_excel);

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_liste_excel'));
    } */


  // Fonction qui test si une chaine est consituée uniquement de chiffres (Fonctionne pour les nombres à virgule flottante aussi)
  public function isDigits($element)
  {
    return !preg_match("/[^0-9]/", $element);
  }

  // Fonction qui force l'usage de l'encodage UTF-8 pour la lecteur des données de l'Excel (Surtout pour les accents)
  public function decode($val)
  {
    return mb_convert_encoding($val, "UTF-8", "windows-1252");
  }

  // Fonction qui importe des Appartements contenus dans un excel
//    public function processImportExcelV1Action($fichier_excel)
//    {
//        // Pour que le script puisse utiliser l'encodage UTF-8
//        //ini_set('mbstring.internal_encoding', 'ISO-8859-1');
//
//        // ExcelReader est une librairie de lecture de fichier Excel (97-2003)
//        //require_once __DIR__.'../../Lib/ExcelReader.php';
//
//        $info = "";
//        $erreurs_par_ligne = array();
//        $erreurs_par_colonne = null;
//        $bloque_ajout = false;
//        $nb_ajouts = 0;
//
//        $em = $this->container->get('doctrine')->getManager();
//
//        $user_context = $this->container->get('security.context')->getToken()->getUser();
//	$user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
//        if(!$user)
//        {
//            throw new AccessDeniedException('This user does not have access to this section.');
//        }
//
//        $dir = __DIR__.'/../../../../web/excel/';
//
//
//        //$data = new \ExcelReader($dir.$fichier_excel,false); // le parametre false indique qu'on ne charge pas les styles de cellule, pour économiser de la ram.
//        /**  Identify the type of $inputFileName  **/
//        $inputFileType = PHPExcel_IOFactory::identify($dir.$fichier_excel);
//        /**  Create a new Reader of the type that has been identified  **/
//        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
//        /**  Advise the Reader that we only want to load cell data  **/
//        $objReader->setReadDataOnly(true);
//        /**  Load $inputFileName to a PHPExcel Object  **/
//        $objPHPExcel = $objReader->load($dir.$fichier_excel);
//
//        $worksheet = $objPHPExcel->getActiveSheet();
//        $nbLignes = $worksheet->getHighestRow();
//        $nbColonnes = $worksheet->getHighestColumn();
//
//        $data = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
//
//
//        // Pour l'instant on impose 1000 lignes max
//        if($nbLignes <= 1000)
//        {
//
//            // On parcours les lignes (Les lignes 1 et 2 servent aux intitulés excel)
//            for($i=3;$i<=$nbLignes;$i++)
//            {
//                $bloque_ajout = false;
//
//                $erreurs_par_ligne[$i] = array();
//                $erreurs_par_ligne[$i]['ligne'] = $i;
//
//                $erreurs_par_colonne = array();
//
//                $appartement = new Appartement();
//
//                //$reference = $this->checkNumAppartement(rand(1000,9999));
//                //$appartement->setReference($reference);
//
//                $proprietaire = new Utilisateur();
//
//
//                /*
//                 * Traitement de la première colonne (Reference du bien)
//                 */
//                $info = $data[$i]['A'];
//                if($this->isDigits($info) || $info == "NC") {
//
//                    // On vérifie si l'appartement existe déjà
//                    $recup_app = $em->getRepository('PatCompteBundle:Appartement')->findOneByReference($info);
//
//                    // S'il existe, on le récupère pour mettre à jour les champs
//                    if($recup_app) {
//                        $appartement = $recup_app;
//                        $em->detach($recup_app);
//                        unset($recup_app);
//
//                        $old_pieces = $em->getRepository('PatCompteBundle:Piece')->findByAppartement($appartement);
//                        foreach ($old_pieces as $old_piece)
//                            $em->remove($old_piece);
//
//                        $old_tarifs = $em->getRepository('PatCompteBundle:Tarif')->findByAppartement($appartement);
//                        foreach ($old_tarifs as $old_tarif)
//                            $em->remove($old_tarif);
//                    }
//                    else {
//                        $appartement->setReference($info);
//
//                        /*$appartement->setEtatInterieur("0");
//                        $appartement->setCalme("0");
//                        $appartement->setClair("0");
//                        $appartement->setNeufAncien("0");
//                        $appartement->setStandingImmeuble("0");	*/
//                        $appartement->setUrlFr("NC");
//                        $appartement->setUrlEn("NC");
//                        $appartement->setUrlDe("NC");
//
//                        $appartement->setStatutDiagnostic(2);
//                        $appartement->setStatut(1);
//                        $appartement->setCmc(1);
//                        $appartement->setIsSelection(0);
//                        $appartement->setIsResa(0);
//                        $appartement->setIsLabeliser(0);
//                        $appartement->setIsReloger(0);
//
//                    }
//
//                    $appartement->setUpdatedAt(date("Y-m-d H:i:s"));
//
//                }
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['A'] = "La référence n'a pas un format conforme";
//                }
//
//
//
//                /*
//                 * Traitement de la 2ème colonne (Civilite)
//                 */
//                $info = $data[$i]['B'];
//                if($info == "Mme"
//                || $info == "Mlle"
//                || $info == "M"
//                || $info == "M Mme"
//                || $info == "Ste"
//                || $info == "NC")
//                    $proprietaire->setCivilite($info);
//                else
//                    $erreurs_par_colonne['B'] = "La civilite du proprietaire n'est pas conforme";
//
//
//
//                /*
//                 * Traitement de la 3ème colonne (Nom de famille)
//                 */
//                $info = $data[$i]['C'];
//                $proprietaire->setNom($info);
//
//
//                /*
//                 * Traitement de la 4ème colonne (Adresse mail)
//                 */
//                $info = $data[$i]['D'];
//                if (filter_var($info, FILTER_VALIDATE_EMAIL) || $info == "NC") {
//                    if($info == "NC")
//                        $info = "kr@laboitededev.com";
//                    $proprietaire->setEmail($info);
//                }
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['D'] = "L'email n'est pas renseigné ou non conforme";
//                }
//
//
//
//                /*
//                 * Traitement de la 5ème colonne (Prénom)
//                 */
//                $info = $data[$i]['E'];
//                if($info == null)
//                    $proprietaire->setPrenom("NC");
//                else
//                    $proprietaire->setPrenom($info);
//
//
//
//                /*
//                 * Traitement de la 6ème colonne (Adresse Personnelle)
//                 */
//                $info = $data[$i]['F'];
//                if($info == null)
//                    $proprietaire->setAdresse("NC");
//                else
//                    $proprietaire->setAdresse($info);
//
//
//
//                /*
//                 * Traitement de la 7ème colonne (Ville Personelle)
//                 */
//                $info = $data[$i]['G'];
//                if($info == null)
//                    $proprietaire->setVille("NC");
//                else
//                    $proprietaire->setVille($info);
//
//
//
//                /*
//                 * Traitement de la 8ème colonne (Pays Personnel)
//                 */
//                $info = $data[$i]['H'];
//                if($info == null)
//                    $proprietaire->setPays("NC");
//                else
//                    $proprietaire->setPays($info);
//
//
//
//                /*
//                 * Traitement de la 9ème colonne (Nationalite)
//                 */
//                $info = $data[$i]['I'];
//                if($info == null)
//                    $proprietaire->setNationalite("NC");
//                else
//                    $proprietaire->setNationalite($info);
//
//
//
//                /*
//                 * Traitement de la 10ème colonne (Langue parlée)
//                 */
//                $info = $data[$i]['J'];
//                if($info == null)
//                    $proprietaire->setLangueParle("NC");
//                else
//                    $proprietaire->setLangueParle($info);
//
//
//
//                /*
//                 * Traitement de la 11ème colonne (Téléphone fixe)
//                 */
//                $info = $data[$i]['K'];
//                $proprietaire->setTelephone($info);
//
//
//
//                /*
//                 * Traitement de la 12ème colonne (Téléphone portable 1)
//                 */
//                $info = $data[$i]['L'];
//                $proprietaire->setMobile($info);
//
//
//
//                /*
//                 * Traitement de la 13ème colonne (Téléphone portable 2)
//                 */
//                $info = $data[$i]['M'];
//                $proprietaire->setMobile2($info);
//
//
//
//                /*
//                 * Traitement de la 14ème colonne (Nom de la Résidence)
//                 */
//                $info = $data[$i]['N'];
//                $appartement->setNomResidence($info);
//
//
//
//                /*
//                 * Traitement de la 15ème colonne (Description courte)
//                 */
//                $info = $data[$i]['O'];
//                $appartement->setDescriptionCourteFr($info);
//
//
//
//                /*
//                 * Traitement de la 16ème colonne (Description longue)
//                 */
//                $info = $data[$i]['P'];
//                $appartement->setDescriptionFr($info);
//
//
//
//                /*
//                 * Traitement de la 17ème colonne (Adresse)
//                 */
//                $info = $data[$i]['Q'];
//                if($info == null)
//                    $appartement->setAdresse("NC");
//                else
//                    $appartement->setAdresse($info);
//
//
//
//                /*
//                 * Traitement de la 18ème colonne (Ville)
//                 */
//                $info = $data[$i]['R'];
//
//                $recup_ville = $em->getRepository('PatCompteBundle:Ville')->findOneBy(array('nom' => $info));
//                if($recup_ville)
//                    $appartement->setVille($recup_ville);
//                else {
//                    $ville = new Ville();
//                    $ville->setNom($info);
//                    $em->persist($ville);
//
//                    $appartement->setVille($ville);
//                }
//
//
//
//                /*
//                 * Traitement de la 19ème colonne (Code Postal)
//                 */
//                $info = $data[$i]['S'];
//
//                if(isset($ville))
//                    $ville->setCodePostal($info);
//
//                // Si la ville existe, le code postal est déjà renseigné
//                /*if(isset($recup_ville))
//                    $recup_ville->setCodePostal($info);*/
//
//
//
//                /*
//                 * Traitement de la 20ème colonne (Pays)
//                 */
//                $info = $data[$i]['T'];
//
//                if(isset($ville))
//                    $ville->setPays($info);
//
//                // Si la ville existe, le pays est déjà renseigné
//                /*if(isset($recup_ville))
//                    $recup_ville->setPays($info);*/
//
//
//
//
//                /*
//                 * Traitement de la 21ème colonne (Type)
//                 */
//                $info = $data[$i]['U'];
//                $info = (explode(" ", $info)[0] == null ? $info : explode(" ", $info)[0]);
//
//                /*if($info == "Appartement T1"
//                || $info == "Appartement T2"
//                || $info == "Appartement T3"
//                || $info == "Appartement T4"
//                || $info == "Appartement T5"
//                || $info == "Appartement T6"
//                || $info == "Maison T1"
//                || $info == "Maison T2"
//                || $info == "Maison T3"
//                || $info == "Maison T4"
//                || $info == "Maison T5"
//                || $info == "Maison T6"
//                || $info == "Studio"
//                || $info == "Maison")*/
//                if($info == "Maison" || $info == "Appartement" || $info == "Studio")
//                {
//                    $appartement->setType($info);
//                }
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['U'] = "Type de bien : ".$info." n'est pas une entrée valide";
//                }
//
//
//
//
//                /*
//                 * Traitement de la 22ème colonne (Meublé)
//                 */
//                $info = $data[$i]['V'];
//
//                if($info == 1 || $info == 0)
//                    $appartement->setIsMeuble($info == 1 ? true : false);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['V'] = "Bien meublé : ".$info." n'est pas une entrée valide";
//                }
//
//
//
//
//                /*
//                 * Traitement de la 23ème colonne (Entrée Immeuble)
//                 */
//                $info = $data[$i]['W'];
//
//                if($info == 'clef' || $info == 'code' || $info == 'NC' || $info == null)
//                    $appartement->setEntreeImmeuble($info);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['W'] = "Entrée immeuble : ".$info." n'est pas une entrée valide";
//                }
//
//
//
//
//                /*
//                 * Traitement de la 24ème colonne (Serrure Appartement)
//                 */
//                $info = $data[$i]['X'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setSerrure($info == 0 ? false : true);
//                elseif($info == 'NC' || $info == null)
//                    $appartement->setSerrure(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['X'] = "Serrure : ".$info." n'est pas une entrée valide";
//                }
//
//
//
//
//                /*
//                 * Traitement de la 25ème colonne (Surface Carrez)
//                 */
//                $info = $data[$i]['Y'];
//
//                if(is_numeric($info) || $info == "NC" || $info == null) {
//                    if(is_numeric($info))
//                        $info = floatval($info);
//                    $appartement->setSurfaceCarrez($info);
//                }
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['Y'] = "La surface Carrez n'est pas correcte";
//                }
//
//
//
//
//                /*
//                 * Traitement de la 26ème colonne (Surface au Sol)
//                 */
//                $info = $data[$i]['Z'];
//
//                if(is_numeric($info) || $info == "NC") {
//                    if(is_numeric($info))
//                        $info = floatval($info);
//                    $appartement->setSurfaceSol($info);
//                }
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['Z'] = "La surface n'est pas correcte";
//                }
//
//
//
//
//                /*
//                 * Traitement de la 27ème colonne (Surface Terrain)
//                 */
//                $info = $data[$i]['AA'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null) {
//                    if(is_numeric($info))
//                        $info = floatval($info);
//                    $appartement->setSurfaceTerrain($info);
//                }
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AA'] = "La surface du terrain n'est pas correcte";
//                }
//
//
//
//
//                /*
//                 * Traitement de la 28ème colonne (Loyer Base)
//                 */
//                $info = $data[$i]['AB'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null) {
//                    $tarif_base = new Tarif();
//                    $tarif_base->setType('Base');
//                    $tarif_base->setAppartement($appartement);
//                    $tarif_base->setLoyer($this->isDigits($info) ? $info : 0);
//
//                    $tarif_base->setCreatedAt(new \DateTime("now"));
//                    $tarif_base->setUpdatedAt(new \DateTime("now"));
//
//                    $em->persist($tarif_base);
//                }
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AB'] = "Le loyer de base n'est pas correct";
//                }
//
//
//
//
//                /*
//                 * Traitement de la 29ème colonne (Loyer tout compris)
//                 */
//                $info = $data[$i]['AC'];
//
//                if($info == 1 || $info == 0)
//                    $tarif_base->setIsToutCompris($info == 1 ? true : false);
//                elseif($info == null || $info == "NC")
//                    $tarif_base->setIsToutCompris(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AC'] = "Le champ loyer tout compris n'est pas correct";
//                }
//
//
//
//
//                /*
//                 * Traitement des 30ème, 31ème et 32ème colonnes (Loyer Période)
//                 */
//                $loyer = $data[$i]['AD'];
//                $date_debut = $data[$i]['AE'];
//                $date_fin = $data[$i]['AF'];
//
//                if($loyer != null && $date_debut != null && $date_fin != null) {
//                    $tarif_periode = new Tarif();
//                    $tarif_periode->setType('Periode');
//                    $tarif_periode->setAppartement($appartement);
//
//                    if($this->isDigits($loyer) || $info == "NC" || $loyer == null)
//                        $tarif_periode->setLoyer($this->isDigits($loyer) ? $loyer : 0);
//                    else {
//                        $bloque_ajout = true;
//                        $erreurs_par_colonne['AD'] = "Le loyer période est incorrect";
//                    }
//
//                    // Vérifie la validité de la date
//                    if(checkdate(explode("/", $date_debut)[1], explode("/", $date_debut)[0], explode("/", $date_debut)[2]))
//                            $tarif_periode->setDateDebut($date_debut);
//                    else {
//                        $bloque_ajout = true;
//                        $erreurs_par_colonne['AE'] = "La date de début de période est incorrecte";
//                    }
//
//                    // Vérifie la validité de la date
//                    if(checkdate(explode("/", $date_fin)[1], explode("/", $date_fin)[0], explode("/", $date_fin)[2]))
//                            $tarif_periode->setDateFin($date_fin);
//                    else {
//                        $bloque_ajout = true;
//                        $erreurs_par_colonne['AF'] = "La date de fin de période est incorrecte";
//                    }
//
//                    $tarif_periode->setCreatedAt(new \DateTime("now"));
//                    $tarif_periode->setUpdatedAt(new \DateTime("now"));
//
//                    $em->persist($tarif_periode);
//                }
//
//
//
//
//                /*
//                 * Traitement des 33ème et 34ème colonnes (Loyer Durée)
//                 */
//                $loyer = $data[$i]['AG'];
//                if(preg_match("/^([0-9]{1,2}) ?- ?([0-9]{1,2})/", $data[$i]['AH'])) {
//                    preg_match("/^([0-9]{1,2}) ?- ?([0-9]{1,2})/", $data[$i]['AH'], $duree);
//                    $duree_min = $duree[1];
//                    $duree_max = $duree[2];
//                }
//
//
//                if($loyer != null && $duree_min != null && $duree_max != null) {
//                    $tarif_duree = new Tarif();
//                    $tarif_duree->setType('Duree');
//                    $tarif_duree->setAppartement($appartement);
//
//                    if($this->isDigits($loyer) || $info == "NC" || $loyer == null)
//                        $tarif_duree->setLoyer($this->isDigits($loyer) ? $loyer : 0);
//                    else {
//                        $bloque_ajout = true;
//                        $erreurs_par_colonne['AG'] = "Le loyer durée est incorrect";
//                    }
//
//                    if($this->isDigits($duree_min))
//                        $tarif_duree->setDureeMin($duree_min);
//                    else {
//                        $bloque_ajout = true;
//                        $erreurs_par_colonne['AH'] = "La durée est incorrecte";
//                    }
//
//                    if($this->isDigits($duree_max))
//                        $tarif_duree->setDureeMax($duree_max);
//                    else {
//                        $bloque_ajout = true;
//                        $erreurs_par_colonne['AH'] = "La durée est incorrecte";
//                    }
//
//                    $tarif_duree->setCreatedAt(new \DateTime("now"));
//                    $tarif_duree->setUpdatedAt(new \DateTime("now"));
//
//                    $em->persist($tarif_duree);
//                }
//
//
//
//
//                /*
//                 * Traitement de la 35ème colonne (Provisions sur charge)
//                 */
//                $info = $data[$i]['AI'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setProvisionSurCharge($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AI'] = "Le champ provisions sur charge n'est pas correct";
//                }
//
//
//
//
//                /*
//                 * Traitement de la 36ème colonne (Dépôt de garantie)
//                 */
//                $info = $data[$i]['AJ'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setDepot($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AJ'] = "Le champ dépôt de garantie est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la 37ème colonne (encaissé)
//                 */
//                $info = $data[$i]['AK'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setIsEncaisse($info == 0 ? false : true);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setIsEncaisse(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AK'] = "Le champ encaissé est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la 38ème colonne (Nombre de personnes maximales autorisées)
//                 */
//                $info = $data[$i]['AL'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setNbPersonne($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AL'] = "Le champ nombre de personne est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne AU (Duplex)
//                 */
//                $info = $data[$i]['AU'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setIsDuplex($info == 0 ? false : true);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setIsDuplex(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AU'] = "Le champ duplex est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne AX (Escalier)
//                 */
//                $info = $data[$i]['AX'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setEscalier($info == 0 ? false : true);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setEscalier(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AX'] = "Le champ escalier est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne AY (Type d'escalier)
//                 */
//                $info = $data[$i]['AY'];
//
//                if($info == "gauche" || $info == "droite")
//                    $appartement->setTypeEscalier($info);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setTypeEscalier(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AY'] = "Le champ type d'escalier est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne AZ (Etage)
//                 */
//                $info = $data[$i]['AZ'];
//
//                if($this->isDigits($info) || $info == "RDC" || $info == "NC" || $info == null)
//                    $appartement->setEtage($info);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AZ'] = "Le champ étage est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BA (Nombre d'étage)
//                 */
//                $info = $data[$i]['BA'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setNbEtage($info);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BA'] = "Le champ nombre d'étage est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BB (Ascenseur)
//                 */
//                $info = $data[$i]['BB'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setAscenseur($info == 0 ? false : true);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setAscenseur(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BB'] = "Le champ ascenseur est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BC (Type d'ascenseur)
//                 */
//                $info = $data[$i]['BC'];
//
//                if($info == "gauche" || $info == "droite")
//                    $appartement->setTypeAscenseur($info);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setTypeAscenseur(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BC'] = "Le champ type d'ascenseur est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BD (Nombre de balcons)
//                 */
//                $info = $data[$i]['BD'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setBalcon($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BD'] = "Le champ nombre de balcons est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BE (Nombre de terasses)
//                 */
//                $info = $data[$i]['BE'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setTerrasse($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BE'] = "Le champ nombre de terasses est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BF (Nombre de garages)
//                 */
//                $info = $data[$i]['BF'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setGarage($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BF'] = "Le champ nombre de garages est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BG (Nombre de parkings)
//                 */
//                $info = $data[$i]['BG'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setParking($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BG'] = "Le champ nombre de parkings est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BH (Nombre de caves)
//                 */
//                $info = $data[$i]['BH'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setCave($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BH'] = "Le champ nombre de caves est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BI (Type de stationnement)
//                 */
//                $info = $data[$i]['BI'];
//
//                if($info == "Couvert"
//                || $info == "Aérien"
//                || $info == "Sous-Sol"
//                || $info == "Garage Fermé"
//                || $info == "Ferme"
//                || $info == "NC"
//                || $info == null)
//                    $appartement->setStationnement($info);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BI'] = "Le champ type de stationement n'est pas une entrée valide";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BJ (Mode de chauffage)
//                 */
//                $info = $data[$i]['BJ'];
//
//                if($info == "Electrique"
//                || $info == "Gaz de ville"
//                || $info == "Gaz"
//                || $info == "Fuel"
//                || $info == "Climatisation réversible"
//                || $info == "Autres"
//                || $info == "NC"
//                || $info == null)
//                    $appartement->setModeChauffage($info);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BJ'] = "Le champ mode de chauffage n'est pas une entrée valide";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BK (Type de chauffage)
//                 */
//                $info = $data[$i]['BK'];
//
//                if($info == "Individuel"
//                || $info == "Collectif"
//                || $info == "NC"
//                || $info == null)
//                    $appartement->setTypeChauffage($info);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BK'] = "Le champ type de chauffage n'est pas une entrée valide";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BL (Distance Aéroport)
//                 */
//                $info = $data[$i]['BL'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setDistanceAeroport($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BL'] = "Le champ distance aéroport est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BM (Distance Autoroute)
//                 */
//                $info = $data[$i]['BM'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setDistanceAutoroute($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BM'] = "Le champ distance autoroute est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BN (Distance Gare)
//                 */
//                $info = $data[$i]['BN'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setAccesGare($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BN'] = "Le champ distance gare est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BO (Distance Commerces)
//                 */
//                $info = $data[$i]['BO'];
//
//                if(is_numeric($info) || $info == "NC" || $info == null) {
//                    if(is_numeric($info))
//                        $info = floatval ($info);
//                    $appartement->setDistanceCommerce($this->isDigits($info) ? $info : null);
//                }
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BO'] = "Le champ distance commerces est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BP (Distance Marche)
//                 */
//                $info = $data[$i]['BP'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setAccesMarche($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BP'] = "Le champ distance marche est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BQ (Distance Bus)
//                 */
//                $info = $data[$i]['BQ'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setAccesBus($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BQ'] = "Le champ distance bus est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BR (Distance Métro)
//                 */
//                $info = $data[$i]['BR'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setAccesMetro($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BR'] = "Le champ distance métro est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BS (Distance Tram)
//                 */
//                $info = $data[$i]['BS'];
//
//                if($this->isDigits($info) || $info == "NC" || $info == null)
//                    $appartement->setAccesTram($this->isDigits($info) ? $info : null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BS'] = "Le champ distance tram est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BT (Climatisation)
//                 */
//                $info = $data[$i]['BT'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setClimatisation($info == 0 ? false : true);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setClimatisation(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BT'] = "Le champ climatisation est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BU (Grenier)
//                 */
//                $info = $data[$i]['BU'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setGrenier($info == 0 ? false : true);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setGrenier(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BU'] = "Le champ grenier est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BV (Digicode)
//                 */
//                $info = $data[$i]['BV'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setDigicode($info == 0 ? false : true);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setDigicode(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BV'] = "Le champ digicode est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BW (Gardien)
//                 */
//                $info = $data[$i]['BW'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setGardien($info == 0 ? false : true);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setGardien(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BW'] = "Le champ gardien est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BX (Interphone)
//                 */
//                $info = $data[$i]['BX'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setInterphone($info == 0 ? false : true);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setInterphone(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BX'] = "Le champ interphone est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BY (Vide-ordure)
//                 */
//                $info = $data[$i]['BY'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setVideOrdure($info == 0 ? false : true);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setVideOrdure(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BY'] = "Le champ vide-ordure est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne BZ (Internet)
//                 */
//                $info = $data[$i]['BZ'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setInternet($info == 0 ? false : true);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setInternet(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['BZ'] = "Le champ internet est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne CA (Fumeur)
//                 */
//                $info = $data[$i]['CA'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setFumeur($info == 0 ? false : true);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setFumeur(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CA'] = "Le champ fumeur est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne CB (Accès Handicapé)
//                 */
//                $info = $data[$i]['CB'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setAccesHandicapes($info == 0 ? false : true);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setAccesHandicapes(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CB'] = "Le champ accès handicapé est incorrect";
//                }
//
//
//
//
//                /*
//                 * Traitement de la colonne CC (Animaux autorisés)
//                 */
//                $info = $data[$i]['CC'];
//
//                if($info == 0 || $info == 1)
//                    $appartement->setAnimal($info == 0 ? false : true);
//                elseif($info == "NC" || $info == null)
//                    $appartement->setAnimal(null);
//                else {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CC'] = "Le champ animaux autorisés est incorrect";
//                }
//
//
//
//                /*
//                 * Traitement des pièces et des équipements
//                 */
//
//                // On vérifie si l'appartement existe déjà
//                //$recup_app = $em->getRepository('PatCompteBundle:Appartement')->findOneByReference($appartement->getReference());
//
//                // S'il existe on supprime les pièces précédemments enregistrées pour ne pas les dupliquer
//                /*if($recup_app) {
//                    $old_pieces = $em->getRepository('PatCompteBundle:Piece')->findByAppartement($appartement);
//                    foreach ($old_pieces as $old_piece)
//                        $em->remove($old_piece);
//                    $em->flush();
//                    unset($old_piece);
//                }*/
//
//                $nb_pieces = $data[$i]['AM'];
//                $nb_chambres = $data[$i]['AN'];
//                $salon = $data[$i]['AO'];
//                $salle_a_manger = $data[$i]['AP'];
//                $bureau = $data[$i]['AQ'];
//                $salle_bain = $data[$i]['AR'];
//                $salle_eau = $data[$i]['AS'];
//                $mezzanine = $data[$i]['AT'];
//                $cuisine = $data[$i]['AV'];
//                $wc = $data[$i]['AW'];
//
//                $refrigerateur = $data[$i]['CD'];
//                $lave_vaisselle = $data[$i]['CE'];
//                $congelateur = $data[$i]['CF'];
//                $four = $data[$i]['CG'];
//                $micro_onde = $data[$i]['CH'];
//                $plaques_electriques = $data[$i]['CI'];
//                $plaques_induction = $data[$i]['CJ'];
//                $plaques_vitroceramiques = $data[$i]['CK'];
//                $plaques_gaz = $data[$i]['CL'];
//                $lave_linge = $data[$i]['CM'];
//                $seche_linge = $data[$i]['CN'];
//                $telephone = $data[$i]['CO'];
//                $television = $data[$i]['CP'];
//                $cheminee = $data[$i]['CQ'];
//                $climatiseur = $data[$i]['CR'];
//                $lavabo_1vasque = $data[$i]['CS'];
//                $lavabo_2vasque = $data[$i]['CT'];
//                $baignoire = $data[$i]['CU'];
//                $douche = $data[$i]['CV'];
//
//
//                if(!$this->isDigits($nb_pieces) && $nb_pieces != "NC" && $nb_pieces != null) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AM'] = "Le champ nombre de pièces est incorrect";
//                }
//                if(!$this->isDigits($nb_chambres) && $nb_chambres != "NC" && $nb_chambres != null) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AN'] = "Le champ nombre de chambres est incorrect";
//                }
//
//                $a = array(0, 1, "NC", null);
//
//                if(!in_array($salon, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AO'] = "Le champ salon est incorrect";
//                }
//                if(!in_array($salle_a_manger, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AP'] = "Le champ salle à manger est incorrect";
//                }
//                if(!in_array($bureau, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AQ'] = "Le champ bureau est incorrect";
//                }
//                if(!in_array($salle_bain, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AR'] = "Le champ salle de bain est incorrect";
//                }
//                if(!in_array($salle_eau, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AS'] = "Le champ salle d'eau est incorrect";
//                }
//                if(!in_array($mezzanine, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AT'] = "Le champ mezzanine est incorrect";
//                }
//                if(!in_array($wc, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['AW'] = "Le champ WC est incorrect";
//                }
//
//
//                if(!in_array($refrigerateur, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CD'] = "Le champ réfrigérateur est incorrect";
//                }
//                if(!in_array($lave_vaisselle, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CE'] = "Le champ lave-vaisselle est incorrect";
//                }
//                if(!in_array($congelateur, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CF'] = "Le champ congélateur est incorrect";
//                }
//                if(!in_array($four, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CG'] = "Le champ four est incorrect";
//                }
//                if(!in_array($micro_onde, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CH'] = "Le champ micro-onde est incorrect";
//                }
//                if(!in_array($plaques_electriques, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CI'] = "Le champ plaques électriques est incorrect";
//                }
//                if(!in_array($plaques_induction, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CJ'] = "Le champ plaques à induction est incorrect";
//                }
//                if(!in_array($plaques_vitroceramiques, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CK'] = "Le champ plaques vitrocéramiques est incorrect";
//                }
//                if(!in_array($plaques_gaz, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CL'] = "Le champ plauqes gaz est incorrect";
//                }
//                if(!in_array($lave_linge, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CM'] = "Le champ lave-linge est incorrect";
//                }
//                if(!in_array($seche_linge, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CN'] = "Le champ sèche-linge est incorrect";
//                }
//                if(!in_array($telephone, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CO'] = "Le champ téléphone( est incorrect";
//                }
//                if(!in_array($television, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CP'] = "Le champ télévision est incorrect";
//                }
//                if(!in_array($cheminee, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CQ'] = "Le champ cheminée est incorrect";
//                }
//                if(!in_array($climatiseur, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CR'] = "Le champ climatiseur est incorrect";
//                }
//                if(!in_array($lavabo_1vasque, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CS'] = "Le champ lavabo 1 vasque est incorrect";
//                }
//                if(!in_array($lavabo_2vasque, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CT'] = "Le champ lavabo 2 vasques est incorrect";
//                }
//                if(!in_array($baignoire, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CU'] = "Le champ baignoir est incorrect";
//                }
//                if(!in_array($douche, $a)) {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne['CV'] = "Le champ douche est incorrect";
//                }
//
//                $pieces=array();
//                $typepiece=array();
//                $k = 0;
//
//                if($this->isDigits($nb_chambres) && $nb_chambres > 0) {
//                    for($j = 0; $j < $nb_chambres; $j++) {
//                        $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Chambre"));
//
//                        $piece_chambre = new Piece();
//                        $piece_chambre->setTypepiece($typepiece[$k]);
//                        $piece_chambre->setAppartement($appartement);
//
//                        $pieces[$k] = $piece_chambre;
//                        $em->persist($pieces[$k]);
//                        $em->persist($typepiece[$k]);
//                        $k++;
//
//                        unset($piece_chambre);
//                    }
//                }
//
//                if($salon == 1) {
//                    $piece_salon = new Piece();
//                    $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Salon"));
//                    $piece_salon->setTypepiece($typepiece[$k]);
//                    $piece_salon->setAppartement($appartement);
//
//                    if($cheminee == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Cheminée"));
//                        $piece_salon->addEquipement($equipement);
//                    }
//                    if($television == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Télévision"));
//                        $piece_salon->addEquipement($equipement);
//                    }
//                    if($telephone == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Téléphone"));
//                        $piece_salon->addEquipement($equipement);
//                    }
//                    if($climatiseur == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Climatiseur"));
//                        $piece_salon->addEquipement($equipement);
//                    }
//
//                    $pieces[$k] = $piece_salon;
//                    $em->persist($pieces[$k]);
//                    $em->persist($typepiece[$k]);
//                    $k++;
//
//                    unset($piece_salon);
//                }
//
//                if($salle_a_manger == 1) {
//                    $piece_salle_a_manger = new Piece();
//                    $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Salle à manger"));
//                    $piece_salle_a_manger->setTypepiece($typepiece[$k]);
//                    $piece_salle_a_manger->setAppartement($appartement);
//
//                    $pieces[$k] = $piece_salle_a_manger;
//                    $em->persist($pieces[$k]);
//                    $em->persist($typepiece[$k]);
//                    $k++;
//                    unset($piece_salle_a_manger);
//                }
//
//                if($bureau == 1) {
//                    $piece_bureau = new Piece();
//                    $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Bureau"));
//                    $piece_bureau->setTypepiece($typepiece[$k]);
//                    $piece_bureau->setAppartement($appartement);
//
//                    $pieces[$k] = $piece_bureau;
//                    $em->persist($pieces[$k]);
//                    $em->persist($typepiece[$k]);
//                    $k++;
//                    unset($piece_bureau);
//                }
//
//                if($salle_bain == 1) {
//                    $piece_salle_bain = new Piece();
//                    $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Salle de bain"));
//                    $piece_salle_bain->setTypepiece($typepiece[$k]);
//                    $piece_salle_bain->setAppartement($appartement);
//
//                    if($lavabo_1vasque == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Lavabo 1 Vasque"));
//                        $piece_salle_bain->addEquipement($equipement);
//                    }
//                    if($lavabo_2vasque == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Lavabo 2 Vasques"));
//                        $piece_salle_bain->addEquipement($equipement);
//                    }
//                    if($baignoire == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Baignoire"));
//                        $piece_salle_bain->addEquipement($equipement);
//                    }
//                    if($douche == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Douche"));
//                        $piece_salle_bain->addEquipement($equipement);
//                    }
//
//                    $pieces[$k] = $piece_salle_bain;
//                    $em->persist($pieces[$k]);
//                    $em->persist($typepiece[$k]);
//                    $k++;
//                    unset($piece_salle_bain);
//                }
//
//                if($salle_eau == 1) {
//                    $piece_salle_eau = new Piece();
//                    $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Salle d'eau"));
//                    $piece_salle_eau->setTypepiece($typepiece[$k]);
//                    $piece_salle_eau->setAppartement($appartement);
//
//                    if($lave_linge == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Lave linge"));
//                        $piece_salle_eau->addEquipement($equipement);
//                    }
//                    if($seche_linge == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Sèche Linge"));
//                        $piece_salle_eau->addEquipement($equipement);
//                    }
//
//                    $pieces[$k] = $piece_salle_eau;
//                    $em->persist($pieces[$k]);
//                    $em->persist($typepiece[$k]);
//                    $k++;
//                    unset($piece_salle_eau);
//                }
//
//                if($mezzanine == 1) {
//                    $piece_mezzanine = new Piece();
//                    $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Mezzanine"));
//                    $piece_mezzanine->setTypepiece($typepiece[$k]);
//                    $piece_mezzanine->setAppartement($appartement);
//
//                    $pieces[$k] = $piece_mezzanine;
//                    $em->persist($pieces[$k]);
//                    $em->persist($typepiece[$k]);
//                    $k++;
//                    unset($piece_mezzanine);
//                }
//
//                if($wc == 1) {
//                    $piece_wc = new Piece();
//                    $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "WC"));
//                    $piece_wc->setTypepiece($typepiece[$k]);
//                    $piece_wc->setAppartement($appartement);
//
//                    $pieces[$k] = $piece_wc;
//                    $em->persist($pieces[$k]);
//                    $em->persist($typepiece[$k]);
//                    $k++;
//                    unset($piece_wc);
//                }
//
//                if($cuisine != null) {
//                    $piece_cuisine = new Piece();
//                    $typepiece[$k] = $em->getRepository('PatCompteBundle:TypePiece')->findOneBy(array('type' => "Cuisine"));
//                    $piece_cuisine->setTypepiece($typepiece[$k]);
//                    $piece_cuisine->setCommentaire($cuisine);
//                    $piece_cuisine->setAppartement($appartement);
//
//                    if($refrigerateur == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Réfrigerateur"));
//                        $piece_cuisine->addEquipement($equipement);
//                    }
//                    if($lave_vaisselle == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Lave Vaisselle"));
//                        $piece_cuisine->addEquipement($equipement);
//                    }
//                    if($congelateur == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Congélateur"));
//                        $piece_cuisine->addEquipement($equipement);
//                    }
//                    if($four == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Four"));
//                        $piece_cuisine->addEquipement($equipement);
//                    }
//                    if($micro_onde == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Micro-onde"));
//                        $piece_cuisine->addEquipement($equipement);
//                    }
//                    if($plaques_electriques == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Plaques Electriques"));
//                        $piece_cuisine->addEquipement($equipement);
//                    }
//                    if($plaques_induction == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Plaques à induction"));
//                        $piece_cuisine->addEquipement($equipement);
//                    }
//                    if($plaques_vitroceramiques == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Plaques Vitrocéramiques"));
//                        $piece_cuisine->addEquipement($equipement);
//                    }
//                    if($plaques_gaz == 1) {
//                        $equipement = $em->getRepository('PatCompteBundle:Equipement')->findOneBy(array('intitule' => "Plaques Gaz"));
//                        $piece_cuisine->addEquipement($equipement);
//                    }
//
//                    $pieces[$k] = $piece_cuisine;
//                    $em->persist($pieces[$k]);
//                    $em->persist($typepiece[$k]);
//                    $k++;
//                    unset($piece_cuisine);
//                }
//
//
//                /*if(count($erreurs_par_colonne) >= ($nbColonnes/3))
//                {
//                    $bloque_ajout = true;
//                    $erreurs_par_colonne[$j+1] = "Le nombre d'erreurs est trop important pour cette ligne (".$i.")";
//                    break;
//                }*/
//
//
//                if($bloque_ajout == false)
//                {
//                    // On vérifie si l'appartement existe déjà
//                    $recup_app = $em->getRepository('PatCompteBundle:Appartement')->findOneByReference($appartement->getReference());
//
//                    // S'il existe on le met à jour
//                    if($recup_app) {
//                        //$appartement->setUpdatedAt(new \DateTime("now"));
//                        $appartement->setUpdatedBy($recup_app->getUtilisateur());
//                        $em->persist($appartement);
//                        //$em->flush();
//                    }
//                    else {
//                        $isExistEmail = $em->getRepository('PatUtilisateurBundle:Utilisateur')->checkEmailCompteAction($proprietaire->getEmail());
//
//                        //$recup_prop = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array('civilite' => $proprietaire->getCivilite(),'nom' => $proprietaire->getNom(),'prenom' => $proprietaire->getPrenom()));
//                        //if($recup_prop) {
//                        if($isExistEmail) {
//                            $recup_prop = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneByEmail($proprietaire->getEmail());
//                            //echo "Le proprietaire existe deja !";
//                            $appartement->setUtilisateur($recup_prop);
//                            $appartement->setUpdatedBy($recup_prop);
//                            $em->persist($appartement);
//                            $nb_ajouts++;
//
//                            //foreach ($pieces as $piece)
//                            //   $em->persist($piece);
//
//                            //$em->flush();
//                        }
//                        // Si le propriétaire n'existe pas, on le crée
//                        else {
//                            //gestion du numéro de compte
//                            $num_compte = "P".rand(1000,9999);
//                            $num_compte = $em->getRepository('PatUtilisateurBundle:Utilisateur')->checkNumCompteAction($num_compte);
//                            $proprietaire->setNumCompte($num_compte);
//
//                            $nomtronq = substr($proprietaire->getNom(), 0, 3);
//                            $proprietaire->setUsername($num_compte.$nomtronq);
//                            $proprietaire->setUsernameCanonical($proprietaire->getUsername());
//
//                            $proprietaire->setDateNaissance(new \DateTime("now"));
//                            //$proprietaire->setNationalite("NC");
//                            //$proprietaire->setLangueParle("NC");
//
//                            //$proprietaire->setAdresse("NC");
//                            $proprietaire->setCodePostal("NC");
//                            //$proprietaire->setVille("NC");
//                            //$proprietaire->setPays("NC");
//
//                            $proprietaire->setEmailCanonical($proprietaire->getEmail());
//                            $proprietaire->setConfirmationToken(null);
//                            $proprietaire->setEnabled(true);
//                            //$proprietaire->setAlgorithm("sha512");
//                            $proprietaire->setTypeUtilisateur("2");
//                            $proprietaire->setCGU("1");
//                            $proprietaire->setCreatedAt(new \DateTime("now"));
//                            $proprietaire->setUpdatedAt(new \DateTime("now"));
//
//                            //gestion du mot de passe
//                            $password = $em->getRepository('PatUtilisateurBundle:Utilisateur')->generatePassword();
//                            $encoder = $this->container->get('security.encoder_factory')->getEncoder($proprietaire);
//                            $proprietaire->setPassword($encoder->encodePassword($password, $proprietaire->getSalt()));
//
//                            $em->persist($proprietaire);
//
//                            //envoi du mail au locataire
//                            /*$message_email = \Swift_Message::newInstance()
//                            ->setSubject("Création de votre compte proprietaire - PIED'A TERRE")
//                            ->setFrom('contact@pied-aterre.fr')
//                            //->setTo($locataire->getEmail())
//                            ->setTo("jd@interstudio.fr")
//                            ->setBody($this->container->get('templating')->render('PatCompteBundle:AdminProprietaire:email_validation.txt.twig', array('utilisateur' => $proprietaire, 'password' => $password)));
//                            $this->container->get('mailer')->send($message_email);	*/
//
//
//                            //$recup_prop = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneByNumCompte($proprietaire->getNumCompte());
//                            $recup_prop = $proprietaire;
//                            if($recup_prop)
//                            {
//
//                                $appartement->setUtilisateur($recup_prop);
//
//                                //Génération des urls
//                                $url_fr = "location-".$appartement->getType()."-meuble-".$appartement->getVille()."-".$appartement->getReference();
//                                $url_fr = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_fr);
//                                $appartement->setUrlFr($url_fr);
//
//                                $url_en = "apartment-".$appartement->getType()."-furnished-".$appartement->getVille()."-".$appartement->getReference();
//                                $url_en = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_en);
//                                $appartement->setUrlEn($url_en);
//
//                                $url_de = "wohnung-".$appartement->getType()."-mobliert-".$appartement->getVille()."-".$appartement->getReference();
//                                $url_de = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_de);
//                                $appartement->setUrlDe($url_de);
//
//                                $appartement->setUpdatedBy($recup_prop);
//
//                                $em->persist($appartement);
//                                $nb_ajouts++;
//                                //$em->flush();
//
//                                $appartement->setCreatedBy($recup_prop);
//
//                            }
//                            else
//                            {
//                                $bloque_ajout = true;
//                                $erreurs_par_colonne['DF'] = "Erreur lors de l'ajout du propriétaire.";
//                                $em->remove($appartement);
//                                //$em->flush();
//                            }
//                            $em->detach($num_compte);
//                        }
//                        $em->detach($isExistEmail);
//                    }
//
//                    $erreurs_par_ligne[$i]['message'] = "Pas d'erreurs";
//                }
//                else
//                {
//                    if(isset($recup_app))
//                        $em->detach($recup_app);
//                    $em->detach($proprietaire);
//                    $em->detach($appartement);
//                    if(isset($old_pieces)) {
//                        foreach ($old_pieces as $old_piece)
//                            $em->detach($old_piece);
//                    }
//                    foreach ($pieces as $piece)
//                        $em->detach($piece);
//                    if(isset($old_tarifs)) {
//                        foreach ($old_tarifs as $old_tarif)
//                            $em->detach($old_tarif);
//                    }
//                    if(isset($tarif_base))
//                        $em->detach($tarif_base);
//                    if(isset($tarif_duree))
//                        $em->detach($tarif_duree);
//                    if(isset($tarif_periode))
//                        $em->detach($tarif_periode);
//                    if(isset($recup_ville))
//                        $em->detach($recup_ville);
//
//                    foreach ($typepiece as $tPiece)
//                        $em->detach($tPiece);
//
//                    unset($proprietaire);
//                    unset($appartement);
//                    unset($old_pieces);
//                    unset($pieces);
//                    unset($old_tarifs);
//                    unset($tarif_base);
//                    unset($tarif_duree);
//                    unset($tarif_periode);
//
//                    $erreurs_par_ligne[$i]['message'] = $erreurs_par_colonne;
//
//                    $em->clear();
//                    continue;
//                }
//
//                $erreurs_par_ligne[$i]['message'] = $erreurs_par_colonne;
//
//                $em->flush();
//
//                if(isset($recup_app))
//                    $em->detach($recup_app);
//                $em->detach($proprietaire);
//                $em->detach($appartement);
//                if(isset($old_pieces)) {
//                    foreach ($old_pieces as $old_piece)
//                        $em->detach($old_piece);
//                }
//                foreach ($pieces as $piece)
//                    $em->detach($piece);
//                if(isset($old_tarifs)) {
//                    foreach ($old_tarifs as $old_tarif)
//                        $em->detach($old_tarif);
//                }
//                if(isset($tarif_base))
//                    $em->detach($tarif_base);
//                if(isset($tarif_duree))
//                    $em->detach($tarif_duree);
//                if(isset($tarif_periode))
//                    $em->detach($tarif_periode);
//                if(isset($recup_ville))
//                    $em->detach($recup_ville);
//
//                foreach ($typepiece as $tPiece) {
//                    $em->detach($tPiece);
//                }
//
//                unset($recup_app);
//                unset($proprietaire);
//                unset($appartement);
//                unset($old_pieces);
//                unset($pieces);
//                unset($old_tarifs);
//                unset($tarif_base);
//                unset($tarif_duree);
//                unset($tarif_periode);
//
//                $em->clear();
//
//                //var_dump(memory_get_usage());
//            }
//
//        }
//        else
//        {
//            return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminAppartement:importExcelResult.html.twig',
//            array('erreurs'=> null));
//        }
//        //print_r($erreurs_par_ligne);
//
//        return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminAppartement:importExcelResult.html.twig',
//        array('erreurs'=> $erreurs_par_ligne,'nb_ajouts' => $nb_ajouts,'nb_lignes' => $nbLignes - 1,'nom_fichier' => $fichier_excel,'debut' => 2 /*$debut*/));
//
//
//        /*
//        ### UTILS ###
//        // Afficher l'excel
//        //echo $data->dump(true,true);
//
//        // Nombre de ligne & colonnex dans l'excel
//        $nbLignes = $data->rowcount($sheet_index=0);
//        $nbColonnes = $data->colcount($sheet_index=0);
//
//        // Récupérer le type de la cellule (number|date|unknown)
//        $typeCel = $data->type($row,$col,$sheet=0);
//
//        // Accéder à la valeur d'un cellule;
//        $data->val($row,$col);
//
//        #############
//        */
//
//    }
}
