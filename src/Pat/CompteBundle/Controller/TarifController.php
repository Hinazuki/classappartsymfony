<?php

namespace Pat\CompteBundle\Controller;

use Doctrine\ORM\EntityManager;
use Pat\CompteBundle\Entity\TarifHistorique;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\CompteBundle\Entity\Appartement;
use Pat\CompteBundle\Entity\Tarif;
use Pat\CompteBundle\Form\TarifSimpleForm;
use Pat\CompteBundle\Form\TarifAdminForm;
use Pat\CompteBundle\Form\TaxesForm;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;

class TarifController extends ContainerAware
{

  public function gestionByAppAction($id_bien)
  {
    /** @var EntityManager $em */
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    if (!$user)
      throw new AccessDeniedException('This user does not have access to this section.');

    // On vérifie que l'utilisateur modifie bien le tarif de l'un de ses appartements
    if ($user->getType() != 'Administrateur') {
      $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $id_bien, 'utilisateur' => $user->getId()));

      if (!$appartement)
        throw new NotFoundHttpException("Appartement introuvable");
    }
    else
      $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $id_bien));


    $tarif = $em->getRepository('PatCompteBundle:Tarif')->findOneBy(array('appartement' => $appartement->getId()));


    if (!$tarif) {
      $tarif = new Tarif();
      $tarif->setAcompte(8); // Setting default value
      $tarif->setDepot(50); // Setting default value
      $tarif->setDepotMin(300); // Setting default value
      $tarif->setAppartement($appartement);
    }

    /** @var Tarif $oldTarif */
    $oldTarif = clone $tarif;

    if ($user->getType() == 'Administrateur') {
      $form = $this->container->get('form.factory')->create(new TarifAdminForm(), $tarif);
    }

    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST' && $user->getType() == 'Administrateur') {
      $form->bind($request);

      if ($form->isValid()) {

        $em->persist($tarif);

        $diffNuit = $oldTarif->getLoyerNuit() != $tarif->getLoyerNuit();
        $diffSemaine = $oldTarif->getLoyerSemaine1() != $tarif->getLoyerSemaine1();
        $diffMois = $oldTarif->getLoyerMois() != $tarif->getLoyerMois();
        $diffNuit2 = $oldTarif->getLoyerNuit2() != $tarif->getLoyerNuit2();
        $diffSemaine2 = $oldTarif->getLoyerSemaine12() != $tarif->getLoyerSemaine12();
        $diffMois2 = $oldTarif->getLoyerMois2() != $tarif->getLoyerMois2();

        if (
          true === $diffNuit ||
          true === $diffSemaine ||
          true === $diffMois ||
          true === $diffNuit2 ||
          true === $diffSemaine2 ||
          true === $diffMois2
        ) {
          // Creation d'une ligne d'historique de loyer
          $tarifHistorique = new TarifHistorique();
          $tarifHistorique
            ->setAppartement($tarif->getAppartement())
          ;

          if (true === $diffNuit || true === $diffSemaine || true === $diffMois) {
            $tarifHistorique
              ->setLoyerNuit($tarif->getLoyerNuit())
              ->setLoyerSemaine1($tarif->getLoyerSemaine1())
              ->setLoyerMois($tarif->getLoyerMois())
            ;
          }

          if (true === $diffNuit2 || true === $diffSemaine2 || true === $diffMois2) {
            $tarifHistorique
              ->setLoyerNuit2($tarif->getLoyerNuit2())
              ->setLoyerSemaine12($tarif->getLoyerSemaine12())
              ->setLoyerMois2($tarif->getLoyerMois2())
            ;
          }

          $appartement->addTarifHistorique($tarifHistorique);
          $em->persist($tarifHistorique);
        }

        $em->flush();

        $this->container->get('session')->getFlashBag()->add('success', 'Vos changements ont correctement étés sauvegardés.');


        if ($user->getType() == 'Administrateur')
          return new RedirectResponse($this->container->get('router')->generate('pat_admin_tarif_ajouter', array('id_bien' => $appartement->getId())));
        else {

          // On envoie un mail à l'admin pour signaler la modification
          $message_email = \Swift_Message::newInstance()
            ->setSubject("Modification d'un bien - classAppart ®")
            ->setFrom('contact@class-appart.com')
            ->setTo('info@class-appart.com');

          $textBody = $this->container->get('templating')->render('PatCompteBundle:Appartement:mail_info_admin.txt.twig', array('bien' => $appartement, 'user' => $user, 'etape' => 'les loyers'));
          $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Appartement:mail_info_admin.html.twig', array('bien' => $appartement, 'user' => $user, 'etape' => 'les loyers'));

          if (!empty($htmlBody)) {
            $message_email->setBody($htmlBody, 'text/html')
              ->addPart($textBody, 'text/plain');
          }
          else
            $message_email->setBody($textBody);

          $this->container->get('mailer')->send($message_email);

          return new RedirectResponse($this->container->get('router')->generate('pat_tarif_ajouter', array('id_bien' => $appartement->getId())));
        }
      }
    }

    $infos = array(
      'utilisateur' => $user,
      'tarif' => $tarif,
      'appartement' => $appartement,
    );

    if ($user->getType() == 'Administrateur') {
      $infos['form'] = $form->createView();
    }

    if ($user->getType() == 'Administrateur')
      return $this->container->get('templating')->renderResponse('PatCompteBundle:Tarif:gestionByAppAdmin.html.twig', $infos);
    else
      return $this->container->get('templating')->renderResponse('PatCompteBundle:Tarif:gestionByApp.html.twig', $infos);
  }

//    public function deleteAction($id_bien = null, $id_tarif = null) {
//        $em = $this->container->get('doctrine')->getManager();
//
//        //on récupère l'utilisateur courant
//        $user_context = $this->container->get('security.context')->getToken()->getUser();
//        $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
//
//        if(!$user)
//            throw new AccessDeniedException('This user does not have access to this section.');
//
//        if($user->getType() == 'Propriétaire') {
//            $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $id_bien));
//
//            if($appartement->getUtilisateur() != $user)
//                throw new \Exception('Vous ne pouvez pas supprimer ce tarif');
//        }
//
//        $tarif = $em->getRepository('PatCompteBundle:Tarif')->findOneBy(array('id' => $id_tarif, 'appartement' => $id_bien));
//
//        $em->remove($tarif);
//        $em->flush();
//
//        $this->container->get('session')->getFlashBag()->add('success', 'Le tarif a correctement été supprimé');
//
//        if($user->getType() == 'Administrateur')
//            return new RedirectResponse($this->container->get('router')->generate('pat_admin_tarif_ajouter', array('id_bien' => $id_bien)));
//        else
//            return new RedirectResponse($this->container->get('router')->generate('pat_tarif_ajouter', array('id_bien' => $appartement->getId())));
//    }


  public function taxesAction(Request $request)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    if (!$user || $user->getType() != "Administrateur") {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    $param_taxes = $em->getRepository('PatCompteBundle:Parametre')->findOneByName('taxes');
    $taxes = $param_taxes->getValue();

    $form = $this->container->get('form.factory')->create(new TaxesForm(), $taxes);

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {
        $data = $form->getData();

        $param_taxes->setValue($data);
        $em->flush();

        $this->container->get('session')->getFlashBag()->add('success', 'Vos changements ont été sauvegardés !');
      }
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Tarif:editTaxes.html.twig', array(
        'form' => $form->createView()
    ));
  }

}
