<?php

namespace Pat\CompteBundle\Controller;

use Pat\CompteBundle\Lib\CalendrierAppartement;
use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;
use Pat\CompteBundle\Form\AdminCalendrierRechercheForm;
use Pat\CompteBundle\Form\AdminCalendrierAjouterForm;
use Pat\CompteBundle\Entity\Calendrier;
use Pat\CompteBundle\Tools\ReservationTools;
use Pat\CompteBundle\Tools\CalendrierTools;

class AdminCalendrierController extends ContainerAware
{

  //calendrier
  public function indexAction($id = null)
  {
    $tabCal = $calendrier = $formView = "";
    $tableauCalendrier = array();

    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $form = $this->container->get('form.factory')->create(new AdminCalendrierRechercheForm());

    //affichage du calendrier
    $request = $this->container->get('request');

    $cal = $em->getRepository('PatCompteBundle:Calendrier')->findBy(array('appartement' => $id));

    // On récupère la date envoyée par POST
    if ($request->getMethod() == 'POST') {
      $tableauCalendrier = $request->request->get('calendrierrecherche');
      $mois = $tableauCalendrier["mois"];
      $annee = $tableauCalendrier["annee"];
    }
    // Sinon on fixe au mois actuel
    else {
      $mois = date("m");
      $annee = date("Y");
    }

    $tabCal .= '<br/><table border="0" cellpadding="10" cellspacing="0" align="center"><tr valign="top">';
    $month[0] = $mois;
    $year[0] = $annee;
    $month1[0] = $this->recupMois($month[0]);

    $calendrier = new CalendrierAppartement($month[0], $year[0], $id, $em->getRepository('PatCompteBundle:Calendrier')->ListeResaByApp($id));

    $tabCal .= "<td><div class='calendarResa' style=''>".$calendrier->affichage("Admin")."</div></td>";

    for ($i = 1; $i <= 5; $i++) {
      if ($i % 3 == 0)
        $tabCal .= "</tr><tr valign='top'>";

      if ($month[$i - 1] == "12") {
        $month[$i] = "01";
        $year[$i] = $year[$i - 1] + 1;
      }
      else {
        $month[$i] = "0".(($month[$i - 1]) * 1) + 1;
        $year[$i] = $year[$i - 1];
      }

      $calendrier->modification($month[$i], $year[$i], $id);
      $month1[$i] = $this->recupMois($month[$i]);
      $tabCal .= "<td><div class='calendarResa'>".$calendrier->affichage("Admin")."</div></td>";
    }
    $tabCal .= '</tr></table>';

    $appartement = $em->getRepository('PatCompteBundle:Appartement')->find($id);

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminCalendrier:liste.html.twig', array(
        'calendrier' => $cal,
        'tabCal' => $tabCal,
        'appartement' => $appartement,
        'tableauCalendrier' => $tableauCalendrier,
        'form' => $form->createView(),
    ));
  }

  public function recupMois($monthnb)
  {
    switch ($monthnb) {
      case 1: $month = 'Janvier';
        break;
      case 2: $month = 'F&eacute;vrier';
        break;
      case 3: $month = 'Mars';
        break;
      case 4: $month = 'Avril';
        break;
      case 5: $month = 'Mai';
        break;
      case 6: $month = 'Juin';
        break;
      case 7: $month = 'Juillet';
        break;
      case 8: $month = 'Ao&ucirc;t';
        break;
      case 9: $month = 'Septembre';
        break;
      case 10: $month = 'Octobre';
        break;
      case 11: $month = 'Novembre';
        break;
      case 12: $month = 'D&eacute;cembre';
        break;
    }

    return $month;
  }

  //affiche le calendrier
  public function calendarDispo($moisC, $anneeC, $id_appart)
  {

    $em = $this->container->get('doctrine')->getManager();

    //Creating general vars
    $year = date("Y");
    if (empty($moisC))
      $monthnb = date("n");
    else {
      $monthnb = $moisC;
      $year = $anneeC;
      if ($monthnb <= 0) {
        $monthnb = 12;
        $year = $year - 1;
      }
      elseif ($monthnb > 12) {
        $monthnb = 1;
        $year = $year + 1;
      }
    }
    $day = date("w");
    $nbdays = date("t", mktime(0, 0, 0, $monthnb, 1, $year));
    $firstday = date("w", mktime(0, 0, 0, $monthnb, 1, $year));

    //Replace the number of the day by its french name
    $daytab[1] = 'Lu';
    $daytab[2] = 'Ma';
    $daytab[3] = 'Me';
    $daytab[4] = 'Je';
    $daytab[5] = 'Ve';
    $daytab[6] = 'Sa';
    $daytab[7] = 'Di';

    //Build the calendar table
    $calendar = array();
    $z = (int) $firstday;
    if ($z == 0)
      $z = 7;
    for ($i = 1; $i <= ($nbdays / 5); $i++) {
      for ($j = 1; $j <= 7 && $j - $z + 1 + (($i * 7) - 7) <= $nbdays; $j++) {
        if ($j < $z && ($j - $z + 1 + (($i * 7) - 7)) <= 0) {
          $calendar[$i][$j] = null;
        }
        else {
          $calendar[$i][$j] = $j - $z + 1 + (($i * 7) - 7);
        }
      }
    }

    //Replace the number of the month by its french name
    switch ($monthnb) {
      case 1: $month = 'Janvier';
        break;
      case 2: $month = 'F&eacute;vrier';
        break;
      case 3: $month = 'Mars';
        break;
      case 4: $month = 'Avril';
        break;
      case 5: $month = 'Mai';
        break;
      case 6: $month = 'Juin';
        break;
      case 7: $month = 'Juillet';
        break;
      case 8: $month = 'Ao&ucirc;t';
        break;
      case 9: $month = 'Septembre';
        break;
      case 10: $month = 'Octobre';
        break;
      case 11:$month = 'Novembre';
        break;
      case 12:$month = 'D&eacute;cembre';
        break;
    }

    $calendarDispo = "";
    $calendarDispo .= '

    <div class="calendrierDispo">
        <table border="0" cellpadding="6" cellspacing="2" bgcolor="#E1E0E0">
            <tr bgcolor="#ffffff">
                <th colspan="7" align="center" class="headcal">'.($month.' '.$year).'</th>
            </tr>
            ';
    $calendarDispo .= '<tr bgcolor="#ffffff">';
    for ($i = 1; $i <= 7; $i++) {
      $calendarDispo .= '<th class="jourCal" align="center">'.$daytab[$i].'</th>';
    }
    $calendarDispo .= '</tr>';
    for ($i = 1; $i <= count($calendar); $i++) {
      $calendarDispo .= '<tr bgcolor="#ffffff">';
      for ($j = 1; $j <= 7 && $j - $z + 1 + (($i * 7) - 7) <= $nbdays; $j++) {

        /*
          if($j-$z+1+(($i*7)-7) == date("j") && $monthnb == date("n") && $year == date("Y")){
          $calendarDispo.='<th class="current"><font color="red">'.$calendar[$i][$j].'</font></th>';
          }
         */
        //on ajoute un zéro devant les chiffre de 1 à 9
        if (strlen($monthnb) == 1) {
          $monthnb = "0".$monthnb;
        }
        if (strlen($calendar[$i][$j]) == 1) {
          $calendar[$i][$j] = "0".$calendar[$i][$j];
        }

        $dateResa = $year."-".$monthnb."-".$calendar[$i][$j];

        //                    if($dateResa == "2009-01-11"){
        //						echo $dateResa."<br/>";
        //                    }
        $calendrier = $em->getRepository('PatCompteBundle:Calendrier')->findBy(array('appartement' => $id_appart, 'date' => $dateResa, 'statut' => "1"));


        if ($calendrier) { //on teste et affiche les dates de la BDD
          for ($k = 0; $k < sizeof($calendrier); $k++) {
            if ($calendrier[$k]->getReservation() != null and $calendrier[$k]->getStatut() == "1") {
              $calendarDispo .= '<th style="background-color:#D22C2C;color:#FFFFFF" bgcolor="#D22C2C">'.$calendar[$i][$j].'</th>';
            }
            else if ($calendrier[$k]->getStatut() == "1") {
              $calendarDispo .= '<th style="background-color:#F0AE0E;color:#FFFFFF" bgcolor="#F0AE0E">'.$calendar[$i][$j].'</th>';
            }
            else {
              $calendarDispo .= '<th>'.$calendar[$i][$j].'</th>';
            }
            break;
          }
        }
        else {
          $calendarDispo .= '<th>'.$calendar[$i][$j].'</th>';
        }
      }
      $calendarDispo .= '</tr>';
    }
    $calendarDispo .= '
        </table>

    </div>
            ';
    return $calendarDispo;
  }

  //bloquer débloquer des dates
  public function ajouterAction($id = null, $id_cal = null)
  {
    $message = '';
    $tableau = array();

    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $appartement = $em->getRepository('PatCompteBundle:Appartement')->find($id);
    if (!$appartement)
      throw new NotFoundHttpException("Bien non trouvé");

    if ($id_cal) {
      // Si l'on modifie un bloquage de date existant, on le récupère, crée le formulaire puis transpose les dates au format français
      $calendrier = $em->getRepository('PatCompteBundle:Calendrier')->find($id_cal);
      if (!$calendrier)
        throw new NotFoundHttpException("Dates bloquées non trouvées");
      $form = $this->container->get('form.factory')->create(new AdminCalendrierAjouterForm(), $calendrier);
    }
    else {
      $calendrier = new Calendrier();
      $form = $this->container->get('form.factory')->create(new AdminCalendrierAjouterForm(), $calendrier);
    }

    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST') {

      $form->bind($request);

      if ($form->isValid()) {
        $tableau = $request->request->get('calendrieradminajout');
        $date_debut = $tableau["date_debut"];
        $date_fin = $tableau["date_fin"];

        //passage de la date au format EN
        $date_debut_en = $this->toDateEn($date_debut);
        $date_fin_en = $this->toDateEn($date_fin);

        $test_date_debut_en = $date_debut_en;
        $test_date_fin_en = $date_fin_en;

        /* if(str_replace("-","",$test_date_debut_en) < date("Ymd"))
          $message = "La date de début doit être supérieure à la date du jour";

          else if(str_replace("-","",$test_date_fin_en) <= $test_date_debut_en)
          $message = "La date de fin doit être supérieure à la date de début";

          else { */
        //on vérifie que la période n'est pas bloquée
        $query_cal = $em->createQuery("
          SELECT c FROM PatCompteBundle:Calendrier c
          WHERE c.appartement = :id_app
          AND c.id <> :id_cal
          AND (
               (c.date_debut <= :date_debut AND c.date_fin > :date_debut)
            OR (c.date_debut >= :date_debut AND c.date_fin <= :date_fin)
            OR (c.date_debut < :date_fin AND c.date_fin >= :date_fin)
          )
          AND c.status <> :menage
        ");

        $query_cal->setParameter('id_app', $id);
        $query_cal->setParameter('date_debut', $date_debut_en);
        $query_cal->setParameter('date_fin', $date_fin_en);
        // Si on édite une plage de dates, on ne tient pas compte des anciennes dates, sinon on fixe id à 0 pour que la condition n'influe pas
        $query_cal->setParameter('id_cal', isset($id_cal) ? $id_cal : 0);
        $query_cal->setParameter('menage', CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE); // On ignore les dates bloquées pour ménage

        $result_cal = $query_cal->getResult();

        // Si c'est un blocage pour ménage, on ne bloque pas
        if ($calendrier->getStatus() == CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE)
          $result_cal = null;

        //si l'appartement est bloqué
        if (sizeof($result_cal) > 0)
          $message = "L'appartement n'est pas disponible pour ces dates";
        // Si l'appartement n'est pas bloqué, on recherche s'il est réservé
        else {
          $query_res = $em->createQuery("
            SELECT r FROM PatCompteBundle:Reservation r
            WHERE r.appartement = :id_app
            AND (
                 (r.date_debut <= :date_debut AND r.date_fin > :date_debut)
              OR (r.date_debut >= :date_debut AND r.date_fin <= :date_fin)
              OR (r.date_debut < :date_fin AND r.date_fin >= :date_fin)
            )
            AND r.status < :cancel
          ");

          $query_res->setParameter('id_app', $id);
          $query_res->setParameter('date_debut', $date_debut_en);
          $query_res->setParameter('date_fin', $date_fin_en);
          $query_res->setParameter('cancel', ReservationTools::CONSTANT_STATUS_CANCELED_BY_CA);

          $result_res = $query_res->getResult();

          // Si c'est un blocage pour ménage, on ne bloque pas
          if ($calendrier->getStatus() == CalendrierTools::CONSTANT_STATUS_BLOQUE_ADMIN_MENAGE)
            $result_res = null;

          //si l'appartement est réservé
          if (sizeof($result_res) > 0)
            $message = "L'appartement n'est pas disponible pour ces dates : bloqué par une réservation en ligne";

          else { // si la période est libre, on enregistre
            if (!isset($id_cal)) {
              $calendrier->setUtilisateur($user);
              $calendrier->setAppartement($appartement);
              $calendrier->setCreatedAt(new \DateTime("now"));
            }

            $em->persist($calendrier);

            $em->flush();

            $this->container->get('session')->getFlashBag()->add('notice', "La période a été bloquée avec succès");
            return new RedirectResponse($this->container->get('router')->generate('pat_admin_calendrier_index', array('id' => $id)));
          }
        }
        //}
      }
    }//end if $request->getMethod()

    $dates_bloquees = $em->getRepository('PatCompteBundle:Calendrier')->findBy(array('appartement' => $appartement), array('date_debut' => 'ASC'));

    if ($message != '')
      $this->container->get('session')->getFlashBag()->add('notice', $message);

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminCalendrier:ajouter.html.twig', array(
        'appartement' => $appartement,
        'tableau' => $tableau,
        'form' => $form->createView(),
        'dates_bloquees' => $dates_bloquees
    ));
  }

  public function supprimerAction($id = null, $id_cal = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $arrayRoles = $user->getRoles(); //on test les droits

    $cal = $em->getRepository('PatCompteBundle:Calendrier')->find($id_cal);
    if (!$cal)
      throw new NotFoundHttpException("Dates bloquées non trouvées");

    $em->remove($cal);
    $em->flush();

    $this->container->get('session')->getFlashBag()->add('notice', "Les dates ont correctement été débloquées");

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_calendrier_index', array('id' => $id)));
  }

  //passer les dates au format EN
  public function toDateEn($date)
  {
    return substr($date, 6, 4)."-".substr($date, 3, 2)."-".substr($date, 0, 2);
  }

  //passer les dates au format FR
  public function toDateFR($date)
  {
    return substr($date, 8, 2)."/".substr($date, 5, 2)."/".substr($date, 0, 4);
  }

  //calcul le nombre de nuits entre 2 date
  public function calculNbNuits($date_arrivee1, $date_depart1)
  {
    $date_arrivee1 = $this->toDateEn($date_arrivee1);
    $date_arrivee1 = str_replace("/", "", $date_arrivee1);

    $date_depart1 = $this->toDateEn($date_depart1);
    $date_depart1 = str_replace("/", "", $date_depart1);


    $nbjours = abs(round((strtotime($date_arrivee1) - strtotime($date_depart1)) / (60 * 60 * 24) - 1));
    $nbnuits = $nbjours - 1;

    return $nbnuits;
  }

}
