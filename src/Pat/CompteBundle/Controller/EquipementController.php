<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\CompteBundle\Entity\Equipement;
use Pat\CompteBundle\Entity\EquipementCategorie;
use Pat\CompteBundle\Form\EquipementForm;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EquipementController extends ContainerAware
{

  //afficher le formulaire d'ajout d'un équipement
  public function ajouterAction($id = null)
  {

    $this->locale = $this->container->get('request')->getLocale();
    $validation = '';
    $em = $this->container->get('doctrine')->getManager();


    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    $message = '';
    $em = $this->container->get('doctrine')->getManager();

    if (isset($id)) {
      // modification d'un équipement existant : on recherche ses données
      $equipement = $em->find('PatCompteBundle:Equipement', $id);

      if (!$equipement)
        throw new AccessDeniedException('Aucun équipement trouvé');
    }
    else {
      // ajout d'un nouvel équipement
      $equipement = new Equipement();
    }

    $form = $this->container->get('form.factory')->create(new EquipementForm(), $equipement);
    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {

        if (isset($id)) {
          $isExistEquipement = $em->getRepository('PatCompteBundle:Equipement')->checkIntituleEquipement($form['intitule']->getData(), $id);
          if (!$isExistEquipement) {

            $em->persist($equipement);
            $em->flush();

            $message = 'Equipement modifié avec succès !';
            $this->container->get('session')->getFlashBag()->add('notice', $message);
            return new RedirectResponse($this->container->get('router')->generate('pat_admin_equipement_ajouter'));
          }
          else
            $message = "Cet équipement existe déjà !";
        }
        else {
          $isExistEquipement = $em->getRepository('PatCompteBundle:Equipement')->checkIntituleEquipement($form['intitule']->getData());
          if (!$isExistEquipement) {
            $em->persist($equipement);
            $em->flush();

            $message = 'Equipement ajouté avec succès !';
            $this->container->get('session')->getFlashBag()->add('notice', $message);
            return new RedirectResponse($this->container->get('router')->generate('pat_admin_equipement_ajouter'));
          }
          else
            $message = "Cet équipement existe déjà !";
        }
      }
    }

    $equipements = $em->getRepository('PatCompteBundle:Equipement')->findAll();

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Equipement:ajouter.html.twig', array(
        'form' => $form->createView(),
        'equipements' => $equipements,
        'message' => $message,
    ));
  }

  //lister tous les équipements
  /* public function indexAction() {
    $em = $this->container->get('doctrine')->getManager();
    $equipement = $em->getRepository('PatCompteBundle:Equipement')->findAll();

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Equipement:liste.html.twig',
    array(
    'equipement' => $equipement
    ));
    } */


  // Supprimer un équipement
  public function supprimerAction($id)
  {
    $em = $this->container->get('doctrine')->getManager();
    $equipement = $em->find('PatCompteBundle:Equipement', $id);

    if (!$equipement) {
      throw new NotFoundHttpException("Equipement non trouvé");
    }

    //on récupère toutes les pièces et on vérifie qu'elles ne sont pas lié à l'équipement id
    /* $is_equipement = false;
      $piece = $em->getRepository('PatCompteBundle:Piece')->findAll();
      foreach($piece as $pieces){
      $equip = $pieces->getEquipement();
      foreach($equip as $equipements){
      //echo $id." - ".$equipements->getId()."<br/>";
      if($id == $equipements->getId()){
      $is_equipement = true;
      break;
      }
      }

      }


      if($is_equipement){
      $message = "Vous ne pouvez pas supprimer cet équipement car il est déjà lié à des biens.";
      }
      else
      { */
    $em->remove($equipement);
    $em->flush();
    $message = "Equipement supprimé avec succès !";
    //}



    $this->container->get('session')->getFlashBag()->add('flash', $message);
    return new RedirectResponse($this->container->get('router')->generate('pat_admin_equipement_ajouter'));
  }

}
