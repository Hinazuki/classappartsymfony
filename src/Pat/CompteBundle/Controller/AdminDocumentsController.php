<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Pat\CompteBundle\Form\DocumentsType;
use Pat\CompteBundle\Entity\Documents;

class AdminDocumentsController extends Controller
{

  // <summary>
  // Liste des documents
  // </summary>
  public function ListFileAction(Request $request, $user_id)
  {
    $em = $this->container->get('doctrine')->getManager();

    $proprietaire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array('id' => $user_id, 'type_utilisateur' => 2));
    if (!$proprietaire) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $documents = $em->getRepository('PatCompteBundle:Documents')->findBy(array('user' => $proprietaire));
    return $this->render('PatCompteBundle:AdminDocuments:list.html.twig', array('documents' => $documents, 'proprietaire' => $proprietaire));
  }

  // <summary>
  // Ajouter un document
  // </summary>
  public function AddFileAction(Request $request, $user_id)
  {
    $em = $this->container->get('doctrine')->getManager();

    $proprietaire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array('id' => $user_id, 'type_utilisateur' => 2));
    if (!$proprietaire) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $documents = new Documents();
    $documents->setUser($proprietaire);

    $form = $this->createForm(new DocumentsType($proprietaire->getId()), $documents, array('validation_groups' => array('edition')));

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {
        $file = $request->files->get('pat_comptebundle_documentstype');
        $file = $file['file'];
        if (!is_null($file))
          $documents->setFileSize($file->getSize());

        $em->persist($documents);
        $em->flush();

        $this->container->get('session')->getFlashBag()->add('success', 'Document ajouté avec succès');

        return $this->redirect($this->generateUrl('documentsAdmin_list', array('user_id' => $proprietaire->getId())));
      }
    }

    return $this->render('PatCompteBundle:AdminDocuments:add.html.twig', array('form' => $form->createView(), 'proprietaire' => $proprietaire));
  }

  // <summary>
  // Editer un documents
  // </summary>
  public function EditFileAction(Request $request, $fileid, $user_id)
  {
    $em = $this->container->get('doctrine')->getManager();

    $proprietaire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array('id' => $user_id, 'type_utilisateur' => 2));
    if (!$proprietaire) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $documents = $em->getRepository('PatCompteBundle:Documents')->findOneBy(array('id' => $fileid, 'user' => $proprietaire));

    if (!$documents) {
      throw $this->createNotFoundException('Unable to find Document entity.');
    }

    $form = $this->createForm(new DocumentsType($proprietaire->getId()), $documents, array('validation_groups' => array('edition')));

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {
        $file = $request->files->get('pat_comptebundle_documentstype');
        $file = $file['file'];
        if (!is_null($file))
          $documents->setFileSize($file->getSize());

        $em->persist($documents);
        $em->flush();

        $this->container->get('session')->getFlashBag()->add('success', 'Vos changements ont correctement été sauvegardés');

        return $this->redirect($this->generateUrl('documentsAdmin_list', array('user_id' => $proprietaire->getId())));
      }
    }

    return $this->render('PatCompteBundle:AdminDocuments:edit.html.twig', array('form' => $form->createView(), 'proprietaire' => $proprietaire));
  }

  // <summary>
  // Supprimer un document
  // </summary>
  public function DeleteFileAction($fileid, $user_id)
  {
    $em = $this->container->get('doctrine')->getManager();

    $proprietaire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array('id' => $user_id, 'type_utilisateur' => 2));
    if (!$proprietaire) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $document = $em->getRepository('PatCompteBundle:Documents')->findOneBy(array('id' => $fileid, 'user' => $proprietaire));

    if (!$document) {
      throw $this->createNotFoundException('Unable to find Document entity.');
    }

    $em->remove($document);
    $em->flush();

    $this->container->get('session')->getFlashBag()->add('success', 'Document supprimé avec succès');
    return $this->redirect($this->generateUrl('documentsAdmin_list', array('user_id' => $proprietaire->getId())));
  }

  // <summary>
  // Téléchargement d'un document
  // <summary>
  public function DownloadFileAction($fileid, $user_id)
  {
    $em = $this->container->get('doctrine')->getManager();

    $proprietaire = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array('id' => $user_id, 'type_utilisateur' => 2));
    if (!$proprietaire) {
      throw new AccessDeniedException("Cet utilisateur n'existe pas");
    }

    $document = $em->getRepository('PatCompteBundle:Documents')->findOneBy(array('id' => $fileid, 'user' => $proprietaire));

    if (!$document) {
      throw $this->createNotFoundException('Unable to find Document entity.');
    }

    $file = $document->getFile();
    $helper = $this->get('vich_uploader.templating.helper.uploader_helper');
    $path = $this->get('kernel')->getRootDir().'/..'.$helper->asset($document, 'file');

    // Opening file
    $content = file_get_contents($path);
    $response = new Response();
    $response->headers->set('Content-Type', $file->getMimeType());
    $response->headers->set('Content-Disposition', 'attachment;filename="'.$document->getDocumentName().'.'.$file->getExtension());

    $response->setContent($content);

    return $response;
  }

}
