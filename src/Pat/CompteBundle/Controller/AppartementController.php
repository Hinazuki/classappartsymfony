<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\CompteBundle\Entity\Appartement;
use Pat\CompteBundle\Entity\Piece;
use Pat\CompteBundle\Form\AppartementForm;
use Pat\CompteBundle\Form\AppartementValidForm;
use Pat\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;

//use Pat\FrontBundle\Form\FormulaireContactForm;
//use Pat\CompteBundle\Entity\Formulaire;

class AppartementController extends ContainerAware
{

  //afficher le formulaire d'ajout d'un appart
  public function ajouterAction($id = null, $message = null, $erreur = null)
  {

    $this->locale = $this->container->get('request')->getLocale();
    $validation = '';
    $em = $this->container->get('doctrine')->getManager();


    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    $arrayRoles = $user->getRoles(); //on test les droits
    if ($arrayRoles[0] == "ROLE_ADMIN" or $user->getTypeUtilisateur() != "2") {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    //formulaire de soumission pour validation
    $formValid = $this->container->get('form.factory')->create(new AppartementValidForm());

    if (isset($id)) {
      // modification d'un appartement existant : on recherche ses données en vérifiant que l'utilisateur à le droit me modifier l'appart
      $appartement = $em->getRepository('PatCompteBundle:Appartement')->findBy(array('utilisateur' => $user->getId(), 'id' => $id, 'statut' => array(0, 1, 2, 3, 4)));
      if (!$appartement) {
        return new RedirectResponse($this->container->get('router')->generate('pat_appartement_ajouter'));
      }
      $appartement = $em->find('PatCompteBundle:Appartement', $id);
      $appartement->setUpdatedBy($user);
    }
    else {
      // ajout d'un nouvel appartement
      $appartement = new Appartement();
      $appartement->setStatut(0);

      $appartement->setEtatInterieur("1");
      $appartement->setCalme("1");
      $appartement->setClair("1");
      $appartement->setNeufAncien("1");
      $appartement->setStandingImmeuble("1");
      $appartement->setCmc("0");
      $appartement->setIsResa("0");
      $appartement->setIsLabeliser("0");
      $appartement->setIsReloger("0");
      $appartement->setIsSelection("0");
      $appartement->setUtilisateur($user);
      $appartement->setCreatedBy($user);
      $appartement->setUpdatedBy($user);
    }



    $form = $this->container->get('form.factory')->create(new AppartementForm(), $appartement);

    $has_digicode = $appartement->getDigicode() == null ? false : true;
    $form['has_digicode']->setData($has_digicode);

    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {
        if (!$id) {
          // url temporaire le temps de générer la référence du bien
          $appartement->setUrlFr("url-temp");
          $appartement->setUrlEn("url-temp");
          $appartement->setUrlDe("url-temp");
        }

        $dir = __DIR__.'/../../../../web/';
        if (!file_exists($dir."images")) {
          mkdir($dir."images");
        }
        if (!file_exists($dir."images/photos")) {
          mkdir($dir."images/photos");
        }
        if (!file_exists($dir."images/photos/biens")) {
          mkdir($dir."images/photos/biens");
        }


        //Téléchargement du fichier diagnostic DPE
        $appartement->setFichierDiagnostic("");
        $dir = __DIR__.'/../../../../web/images/photos/biens/';
        $ref_appart = $appartement->getReference();
        $file = $form['fichier_diagnostic']->getData();

        //on test le poids du fichier
        if (!empty($file)) {
          if (filesize($file) > 4000000) {
            $message = "Le du fichier DPE ne doit pas dépasser 4Mo";
            return $this->container->get('templating')->renderResponse(
                'PatCompteBundle:Appartement:ajouter.html.twig', array(
                'form' => $form->createView(),
                'formValid' => $formValid->createView(),
                'message' => $message,
                'appartement' => $appartement,
                'validation' => '',
                'erreur' => $erreur,
                'id' => $id,
            ));
          }
          else {
            if (!file_exists($dir.$ref_appart)) {
              mkdir($dir.$ref_appart);
            }
            if (!file_exists($dir.$ref_appart."/diagnostic")) {
              mkdir($dir.$ref_appart."/diagnostic");
            }

            $extension = $file->guessExtension();
            if ($extension == "pdf" or $extension == "jpg" or $extension == "png" or $extension == "gif" or $extension == "jpeg") {
              $nom_fichier = "DPE-".$ref_appart."-".date("Y-m-d").".".$extension;
              if ($file->move($dir.$ref_appart."/diagnostic", $nom_fichier)) {
                $appartement->setFichierDiagnostic($nom_fichier);
              }
            }
            else {
              $message = "Vous pouvez télécharger des fichier PDF, JPG, GIF, PNG uniquement";
              return $this->container->get('templating')->renderResponse(
                  'PatCompteBundle:Appartement:ajouter.html.twig', array(
                  'form' => $form->createView(),
                  'formValid' => $formValid->createView(),
                  'message' => $message,
                  'appartement' => $appartement,
                  'validation' => '',
                  'erreur' => $erreur,
                  'id' => $id,
              ));
            }
          }
        }

        if ($form['has_digicode']->getData() == false) {
          $appartement->setDigicode(null);
          $has_digicode = false;
        }
        else
          $has_digicode = true;

        $em->persist($appartement);
        $em->flush();

        if (isset($id)) {
          $message = 'Bien modifié avec succès !';

          // On envoie un mail à l'admin pour signaler la modification
          $message_email = \Swift_Message::newInstance()
            ->setSubject("Modification d'un bien - classAppart ®")
            ->setFrom('contact@class-appart.com')
            ->setTo('info@class-appart.com');

          $textBody = $this->container->get('templating')->render('PatCompteBundle:Appartement:mail_info_admin.txt.twig', array('bien' => $appartement, 'user' => $user, 'etape' => 'la fiche'));
          $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Appartement:mail_info_admin.html.twig', array('bien' => $appartement, 'user' => $user, 'etape' => 'la fiche'));

          if (!empty($htmlBody)) {
            $message_email->setBody($htmlBody, 'text/html')
              ->addPart($textBody, 'text/plain');
          }
          else
            $message_email->setBody($textBody);

          $this->container->get('mailer')->send($message_email);
        }
        else {
          $appartement->setReference($appartement->getId() + 5000);

          if ($appartement->getIsMeuble() == true) {
            $meublefr = 'meuble';
            $meubleen = 'furnished';
            $meublede = 'mobliert';
          }
          else {
            $meublefr = 'non-meuble';
            $meubleen = 'unfurnished';
            $meublede = 'unmobliert';
          }

          switch ($appartement->getType()) {
            case 'APPARTEMENT':
              $typefr = 'appartement';
              $typeen = 'apartment';
              $typede = 'wohnung';
              break;
            case 'MAISON':
              $typefr = 'maison';
              $typeen = 'house';
              $typede = 'haus';
              break;
            case 'STUDIO':
              $typefr = 'studio';
              $typeen = 'studio';
              $typede = 'studio';
              break;
            case 'LOFT':
              $typefr = 'loft';
              $typeen = 'loft';
              $typede = 'loft';
              break;
            case 'GARAGE':
              $typefr = 'garage';
              $typeen = 'garage';
              $typede = 'garage';
              break;
            case 'CHAMBRE':
              $typefr = 'chambre';
              $typeen = 'room';
              $typede = 'zimmer';
              break;

            default:
              break;
          }
          $url_fr = "location-".$typefr."-".$meublefr."-".strtolower($appartement->getVille())."-".$appartement->getReference();
          $url_fr = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_fr);
          $appartement->setUrlFr($url_fr);

          $url_en = "renting-".$typeen."-".$meubleen."-".strtolower($appartement->getVille())."-".$appartement->getReference();
          $url_en = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_en);
          $appartement->setUrlEn($url_en);

          $url_de = "mieten-".$typede."-".$meublede."-".strtolower($appartement->getVille())."-".$appartement->getReference();
          $url_de = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_de);
          $appartement->setUrlDe($url_de);
          //Génération des urls
          /* $url_fr = "location-".$appartement->getType()."-meuble-".$appartement->getVille()."-".$appartement->getReference();
            $url_fr = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_fr);
            $appartement->setUrlFr($url_fr);

            $url_en = "apartment-".$appartement->getType()."-furnished-".$appartement->getVille()."-".$appartement->getReference();
            $url_en = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_en);
            $appartement->setUrlEn($url_en);

            $url_de = "wohnung-".$appartement->getType()."-mobliert-".$appartement->getVille()."-".$appartement->getReference();
            $url_de = $em->getRepository('PatCompteBundle:Appartement')->string2url($url_de);
            $appartement->setUrlDe($url_de); */

          $em->flush();

          $message = 'Bien ajouté avec succès !';
          return new RedirectResponse($this->container->get('router')->generate('pat_tarif_ajouter', array("id_bien" => $appartement->getId())));
        }
      }
      else {
        //$message='Veuillez remplir tous les champs avec une étoile !';
        // On récupère toutes les erreurs pour les afficher en haut du formulaire
        foreach ($form->getChildren() as $child) {
          if ($child->hasErrors()) {
            foreach ($child->getErrors() as $error)
              $message .= $error->getMessageTemplate()."\n";
          }
        }
      }
    }



    return $this->container->get('templating')->renderResponse(
        'PatCompteBundle:Appartement:ajouter.html.twig', array(
        'form' => $form->createView(),
        'formValid' => $formValid->createView(),
        'message' => $message,
        'appartement' => $appartement,
        'validation' => '',
        'erreur' => $erreur,
        'id' => $id,
        'has_digicode' => $has_digicode
    ));
  }

  //lister les appartements
  public function indexAction(Request $request, $message = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $arrayRoles = $user->getRoles(); //on test les droits
    if ($arrayRoles[0] == "ROLE_ADMIN" or $user->getTypeUtilisateur() != "2") {
      return new RedirectResponse($this->container->get('router')->generate('pat_admin_dashboard'));
    }

    // Gestion de la recherche
    $type = null;
    $nb_pieces = null;
    $surface_type = null;
    $surface = null;
    $loyer_type = null;
    $loyer = null;
    $ville = null;
    $id_ville = null;
    $page = 1;
    if ($request->getMethod() == 'POST') {
      $page = str_replace(' ', '', $request->request->get('page', 1));
      if ($page < 1) {
        $page = 1;
      }

      //$type = str_replace(' ','',$request->request->get('type', "type"));
      $type = $request->request->get('type', "type");
      if ($type == "" || $type == "Tous") {
        $type = null;
      }

      $nb_pieces = $request->request->get('nb_pieces', "nb_pieces");
      if ($nb_pieces == "" || $nb_pieces == "Tous") {
        $nb_pieces = null;
      }

      $surface_type = str_replace(' ', '', $request->request->get('surface_type', ""));
      if ($surface_type == "") {
        $surface_type = null;
      }

      $surface = str_replace(' ', '', $request->request->get('surface', ""));
      if ($surface == "") {
        $surface = null;
      }

      $loyer_type = str_replace(' ', '', $request->request->get('loyer_type', ""));
      if ($loyer_type == "") {
        $loyer_type = null;
      }

      $loyer = str_replace(' ', '', $request->request->get('loyer', ""));
      if ($loyer == "") {
        $loyer = null;
      }

      $ville = str_replace(' ', '', $request->request->get('ville', ""));
      if ($ville == "") {
        $ville = null;
      }
    }

    //on récupère les apparts
    $recup_nb_total_appartement = $em->getRepository('PatCompteBundle:Appartement')->rechercheBienProprietaire($user->getId(), $type, $nb_pieces, $ville, $surface, $surface_type, $loyer, $loyer_type, null, null, 'recup_nombre');
    if (isset($recup_nb_total_appartement[0]))
      $nb_total_appartement = $recup_nb_total_appartement[0]['nombre'];
    else
      $nb_total_appartement = 0;

    $appartement = $em->getRepository('PatCompteBundle:Appartement')->rechercheBienProprietaire($user->getId(), $type, $nb_pieces, $ville, $surface, $surface_type, $loyer, $loyer_type);

    $paginator = $this->container->get('knp_paginator');
    $pagination = $paginator->paginate(
      $appartement, $this->container->get('request')->query->get('page', 1), 20, array('distinct' => false)
    );

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Appartement:liste.html.twig', array(
        'appartement' => $pagination,
        'message' => $message)
    );
  }

  //public function supprimerAction(Request $request, $id)
  //{
  //    $em = $this->container->get('doctrine')->getManager();
  //    $message = "";
  //
    //    //on récupère l'utilisateur courant
  //    $user_context = $this->container->get('security.context')->getToken()->getUser();
  //    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
  //
    //    $arrayRoles = $user->getRoles(); //on test les droits
  //    if ($arrayRoles[0] == "ROLE_ADMIN" or $user->getTypeUtilisateur() != "2") {
  //        throw new AccessDeniedException('This user does not have access to this section.');
  //    }
  //
    //    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('utilisateur' => $user->getId(), 'id' => $id));
  //    if (!$appartement) {
  //        throw new NotFoundHttpException("Bien non trouvé");
  //    }
  //
    //    /*
  //        //suppression des médias
  //        $repositoryMedia = $em->getRepository('PatCompteBundle:Media');
  //        $medias = $repositoryMedia->findBy(array('appartement' => $appartement->getId()));
  //        for($i=0;$i<sizeof($medias);$i++){
  //        @unlink(__DIR__."/../../../../web/images/photos/biens/".$medias[$i]->getFichier());
  //        @unlink(__DIR__."/../../../../web/images/photos/biens/small/".$medias[$i]->getFichier());
  //        @unlink(__DIR__."/../../../../web/images/photos/biens/tall/".$medias[$i]->getFichier());
  //        $em->remove($medias[$i]);
  //        }
  //
    //        //suppression des pièces
  //        $repositoryPiece = $em->getRepository('PatCompteBundle:Piece');
  //        $pieces = $repositoryPiece->findBy(array('appartement' => $appartement->getId()));
  //        for($i=0;$i<sizeof($pieces);$i++){
  //        $em->remove($pieces[$i]);
  //        }
  //
    //        //suppression de l'appartement
  //        $em->remove($appartement);
  //        $em->flush();
  //        */
  //
    //
    //    $appartement->setStatut(9);
  //    $em->persist($appartement);
  //    $em->flush();
  //
    //    $message = "Votre bien a été supprimé.";
  //
    //    return $this->indexAction($request, $message);
  //}



  public function validerAction($id)
  {
    $em = $this->container->get('doctrine')->getManager();
    $message = "";

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $arrayRoles = $user->getRoles(); //on test les droits
    if ($arrayRoles[0] == "ROLE_ADMIN" or $user->getTypeUtilisateur() != "2") {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    // modification d'un appartement existant : on recherche ses données en vérifiant que l'utilisateur à le droit me modifier l'appart
    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findBy(array('utilisateur' => $user->getId(), 'id' => $id));

    if (!$appartement) {
      throw new NotFoundHttpException("Bien non trouvé");
    }



    $appartement = $em->find('PatCompteBundle:Appartement', $id);
    if ($appartement->getStatut() != "0") {
      throw new NotFoundHttpException("Ce bien n'a pas le statut approprié pour cette demande de validation !");
    }


    $request = $this->container->get('request');
    $choice1 = $choice2 = "";
    if ($request->getMethod() == 'POST') {
      //on récupère les données du formulaire
      $appartementvalid = $request->request->get('appartementvalid');
      if (isset($appartementvalid["choice1"][0])) {
        $choice1 = "1";
      }
      if (isset($appartementvalid["choice2"][0])) {
        $choice2 = "1";
      }

      if ($choice1 != "") {
        $appartement->setStatut(1);
        $appartement->setCmc($choice1);
        $appartement->setIsResa($choice2);
        $em->persist($appartement);
        $em->flush();

        $message_email = \Swift_Message::newInstance()
          ->setSubject("Demande de validation d'un bien - classAppart ®")
          ->setFrom('contact@class-appart.com')
          ->setTo($appartement->getUtilisateur()->getEmail());

//                            if($appartement->getUtilisateur()->getMailsSecond())
//                                $message_email->setCc(explode(',', $appartement->getUtilisateur()->getMailsSecond()));
        if ($appartement->getUtilisateur()->getSecondEmail()) {
          $message_email->setCc($appartement->getUtilisateur()->getSecondEmail());
        }

        $textBody = $this->container->get('templating')->render('PatCompteBundle:Appartement:email_validation.txt.twig', array('appartement' => $appartement, 'utilisateur' => $user));
        $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Appartement:email_validation.html.twig', array('appartement' => $appartement, 'utilisateur' => $user));

        if (!empty($htmlBody)) {
          $message_email->setBody($htmlBody, 'text/html')
            ->addPart($textBody, 'text/plain');
        }
        else
          $message_email->setBody($textBody);

        $this->container->get('mailer')->send($message_email);

        $message = "Votre bien REF : ".$appartement->getReference()." est désormais en cours de validation auprès de nos services.<br/>
                              Vous serez avertit par email de la validation et de la mise en ligne de votre bien.";
//			  $message = "Votre bien est désormais en cours de validation auprès de nos services.<br/>
//							Vous recevrez un mail vous informant de la mise en ligne de votre bien.";

        return $this->container->get('templating')->renderResponse(
            'PatCompteBundle:Appartement:ajouter.html.twig', array(
            'validation' => $message,
        ));
      }
    }

    return new RedirectResponse($this->container->get('router')->generate('pat_appartement_editer_error', array('id' => $id, 'erreur' => "1")));
  }

  //public function dupliquerAction($id)
  //{
  // $em = $this->container->get('doctrine')->getManager();
  //  $message = "";
  //
		////on récupère l'utilisateur courant
  //$user_context = $this->container->get('security.context')->getToken()->getUser();
  //$user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
  //
		//$arrayRoles = $user->getRoles(); //on test les droits
  //if($arrayRoles[0] == "ROLE_ADMIN" or $user->getTypeUtilisateur() != "2"){
  //	throw new AccessDeniedException('This user does not have access to this section.');
  //}
  //
		//// modification d'un appartement existant : on recherche ses données en vérifiant que l'utilisateur à le droit me modifier l'appart
  //$appartement = $em->getRepository('PatCompteBundle:Appartement')->findBy(array('utilisateur' => $user->getId(), 'id' => $id));
  //
		//if (!$appartement)
  //{
  //	throw new NotFoundHttpException("Bien non trouvé");
  //}
  //
		//$appartement = $em->find('PatCompteBundle:Appartement', $id);
  //
		//$appart = new Appartement();
  //$appart = clone $appartement;	//on duplique l'appartement
  //
		////on remet à jour certaine données
  //$appart->setUtilisateur($user);
  //            $appart->setCreatedBy($user);
  //            $appart->setUpdatedBy($user);
  //$appart->setStatut(0);
  //
		//$appart->setUrlFr($appart->getReference());
  //$appart->setUrlEn($appart->getReference());
  //$appart->setUrlDe($appart->getReference());
  //
		//$appart->setEtatInterieur("0");
  //$appart->setCalme("0");
  //$appart->setClair("0");
  //$appart->setNeufAncien("0");
  //$appart->setStandingImmeuble("0");
  //$appart->setCmc("0");
  //$appart->setIsResa("0");
  //$appartement->setIsLabeliser("0");
  //$appartement->setIsReloger("0");
  //$appartement->setIsSelection("0");
  //
		//$em->persist($appart);
  //
		//
		//$piece = $em->getRepository('PatCompteBundle:Piece')->findBy(array('appartement' => $appartement->getId()));
  //
		//for($i=0; $i<sizeof($piece);$i++){
  //	$piece1 = $em->find('PatCompteBundle:Piece', $piece[$i]->getId());
  //	$pieceAppart = new Piece();
  //	$pieceAppart = clone $piece1;	//on duplique les pièces
  //	$pieceAppart->setAppartement($appart);
  //	$em->persist($pieceAppart);
  //
		//
		//}
  //
		//$em->flush();
  //
    //            $appartement->setReference($appartement->getId() + 5000);
  //            $em->flush();
  //
		//return new RedirectResponse($this->container->get('router')->generate('pat_appartement_index'));
  //}

  /**
   *
   * Test si le num appartement existe
   */
  public function checkNumAppartement($reference)
  {
    $em = $this->container->get('doctrine')->getManager();
    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findBy(array('reference' => $reference));

    if ($appartement) {
      return $this->checkNumAppartement(rand(1000, 9999));
    }
    else {
      return $reference;
    }
  }

  /* public function soumettreVilleAction(Request $request)
    {

    $em = $this->container->get('doctrine')->getManager();
    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if(!$user)
    {
    throw new AccessDeniedException('This user does not have access to this section.');
    }

    $arrayRoles = $user->getRoles(); //on test les droits
    if($arrayRoles[0] == "ROLE_ADMIN" or $user->getTypeUtilisateur() != "2"){
    throw new AccessDeniedException('This user does not have access to this section.');
    }

    $action = 0; // afficher le formulaire


    $message = "";

    $test_exist = $em->getRepository('PatCompteBundle:Formulaire')->findOneBy(array("nom" => $user->getNom(),"prenom" => $user->getPrenom()));
    if($test_exist)
    {
    $action = 2;
    $message = $test_exist->getMessage();
    }
    else
    {
    if ($request->getMethod() == 'POST')
    {
    $ville = $_POST['ville'];
    if($ville && $ville != "" && strlen($ville) > 1)
    {

    $formulaire = new Formulaire();
    $formulaire->setNom($user->getNom());
    $formulaire->setPrenom($user->getPrenom());
    $formulaire->setTel($user->getTelephone());
    $formulaire->setEmail($user->getEmail());
    $formulaire->setTypeSociete("Propriétaire");
    $message = $ville;
    $formulaire->setMessage($message);
    $formulaire->setType("AccesVille");
    $formulaire->setCreatedAt(date("Y-m-d H:i:s"));

    $em->persist($formulaire);
    $em->flush();
    $action = 2; // ok

    }
    else
    {
    $action = 1; // erreur
    }
    }
    }

    return $this->container->get('templating')->renderResponse(
    'PatCompteBundle:Appartement:soumettreVille.html.twig',
    array(
    'message' => $message,
    'action' => $action,
    'message' => $message,
    ));


    } */

  /* public function listeVillesDispoAction(Request $request,$region = null,$liste_dep = null)
    {
    $em = $this->container->get('doctrine')->getManager();
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if(!$user)
    {
    throw new AccessDeniedException('This user does not have access to this section.');
    }
    $message = "";

    $arrayRoles = $user->getRoles(); //on test les droits
    if($arrayRoles[0] == "ROLE_ADMIN" or $user->getTypeUtilisateur() != "2"){
    throw new AccessDeniedException('This user does not have access to this section.');
    }

    if(!$liste_dep)
    {
    $villes = array();
    $message = "Veuillez sélectionner une région.";
    }
    else
    {
    $villes = $em->getRepository('PatCompteBundle:Ville')->getVillesByListeDep($liste_dep);
    if(count($villes) == 0)
    {
    $message = "Il n'y a pas de ville disponible dans cette région.";
    }
    }

    return $this->container->get('templating')->renderResponse(
    'PatCompteBundle:Appartement:villesDispo.html.twig',
    array(
    'region' => $region,
    'message' => $message,
    'villes' => $villes,
    ));
    } */
}
