<?php

namespace Pat\CompteBundle\Controller;

use Pat\CompteBundle\Entity\PieceEquipement;
use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\CompteBundle\Entity\Piece;
use Pat\CompteBundle\Entity\Appartement;
use Pat\CompteBundle\Form\PieceForm;
use Pat\CompteBundle\Entity\Equipement;
use Pat\CompteBundle\Form\EquipementRechercheForm;
use Pat\CompteBundle\Form\EquipementForm;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdminPieceController extends ContainerAware
{

  //afficher le formulaire d'ajout d'une pièce
  public function ajouterAction($id = null, $id_piece = null)
  {
    $message = $type_piece = '';
    $em = $this->container->get('doctrine')->getManager();
    $request = $this->container->get('request');

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user)
      throw new AccessDeniedException('This user does not have access to this section.');

    $appartement = $em->find('PatCompteBundle:Appartement', $id);
    if (!$appartement)
      throw new AccessDeniedException('Bien non trouvé.');

    if (isset($id_piece)) {
      // modification d'un équipement existant : on recherche ses données
      $piece = $em->find('PatCompteBundle:Piece', $id_piece);

      if (!$piece)
        $message = 'Aucune pièce trouvée';
    }
    else // ajout d'une nouvelle pièce
      $piece = new Piece();

    $multiEquipements = $em->getRepository('PatCompteBundle:Equipement')->findMultiEquipements();

    $form = $this->container->get('form.factory')->create(new PieceForm(), $piece, ['multiEquipements' => $multiEquipements]);

    if ($request->getMethod() == 'POST') {
      $form->bind($request);
      if ($form->isValid()) {
        $piece->setAppartement($appartement);

        // Remove old MultiEquipement
        $piece->removeAllMultiEquipements();

        // Add new MultiEquipement
        foreach ($form->get('multiEquipements')->getData() as $multiEquipement) {
          if ($multiEquipement['quantity'] > 0) {
            $pieceEquipement = new PieceEquipement();
            $pieceEquipement
              ->setPiece($piece)
              ->setEquipement($multiEquipement['Equipement'])
              ->setQuantity($multiEquipement['quantity'])
            ;

            $piece->addMultiEquipement($pieceEquipement);

            $em->persist($pieceEquipement);
          }
        }

        $em->persist($piece);
        $em->flush();

        if (isset($id_piece)) {
          $message = 'Pièce modifiée avec succès !';
          return new RedirectResponse($this->container->get('router')->generate('pat_admin_piece_index', array('id' => $id)));
        }
        else {
          $message = 'Pièce ajoutée avec succès !';
          return new RedirectResponse($this->container->get('router')->generate('pat_admin_piece_index', array('id' => $id)));
        }
      }
    }

    $equipements = $em->getRepository('PatCompteBundle:Equipement')->findAll();
    $tab_equip_cat = array();

    foreach ($equipements as $equipement)
      $tab_equip_cat[$equipement->getId()] = $equipement->getEquipementCategorie() == null ? "Autre" : $equipement->getEquipementCategorie()->getIntituleCategorie();


    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminPiece:ajouter.html.twig', array(
        'form' => $form->createView(),
        'message' => $message,
        'appartement' => $appartement,
        'tab_equip_cat' => $tab_equip_cat
    ));
  }

  //lister les Pièce
  public function indexAction($id)
  {

    $em = $this->container->get('doctrine')->getManager();
    //$piece = $em->getRepository('PatCompteBundle:Piece')->findAll();
    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());



    //on récupère les pièces par appartement
    $repository = $em->getRepository('PatCompteBundle:Piece');
    $piece = $repository->findByAppartement($id);

    $appartement = $em->find('PatCompteBundle:Appartement', $id);


    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminPiece:liste.html.twig', array('piece' => $piece, 'appartement' => $appartement)
    );
  }

  public function supprimerAction($id, $id_piece)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());


    $piece = $em->find('PatCompteBundle:Piece', $id_piece);

    if (!$piece) {
      throw new NotFoundHttpException("Piece non trouvé");
    }

    $em->remove($piece);
    $em->flush();

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_piece_index', array('id' => $id)));
  }

}
