<?php

namespace Pat\CompteBundle\Controller;

use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;
use Pat\CompteBundle\Entity\SiteTiers;
use Pat\CompteBundle\Form\AdminSiteTiersForm;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class AdminSiteTiersController extends ContainerAware
{

  public function listSiteTiersAction(Request $request)
  {
    /** @var EntityManager $em */
    $em = $this->container->get('doctrine.orm.entity_manager');
    $siteTiersRepo = $em->getRepository('PatCompteBundle:SiteTiers');

    $siteTiersQuery = $siteTiersRepo->createQueryBuilder('st')->getQuery();

    /** @var Paginator $paginator */
    $paginator = $this->container->get('knp_paginator');
    $pagination = $paginator->paginate($siteTiersQuery, $request->query->get('page', 1), 30);

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminSiteTiers:list_site_tiers.html.twig', [
        'siteTiers' => $pagination,
    ]);
  }

  public function ajouterEditerSiteTiersAction(Request $request, SiteTiers $siteTiers = null)
  {
    if (null === $siteTiers) {
      $siteTiers = new SiteTiers();
    }

    $form = $this->container->get('form.factory')->create(new AdminSiteTiersForm(), $siteTiers);

    if ($request->getMethod() === 'POST') {
      $form->handleRequest($request);

      if (true === $form->isValid()) {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $em->persist($siteTiers);
        $em->flush();

        $this->container->get('session')->getFlashBag()->add('success', 'Le site tiers a correctement été ajouté');

        return new RedirectResponse($this->container->get('router')->generate('pat_admin_site_tiers'));
      }
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminSiteTiers:ajouter_editer_site_tiers.html.twig', [
        'form' => $form->createView(),
    ]);
  }

  public function supprimerSiteTiersAction(Request $request, SiteTiers $siteTiers)
  {
    $siteName = $siteTiers->getName();
    /** @var EntityManager $em */
    $em = $this->container->get('doctrine.orm.entity_manager');

    $reservations = $this->container->get('pat.reservation_manager')->getRepository()->countReservationsWithSiteTiers($siteTiers);

    if ($reservations > 0) {
      $this->container->get('session')->getFlashBag()->add('error', 'Impossible de supprimer le site tiers '.$siteName.', il est relié à '.$reservations.' réservations');

      return new RedirectResponse($this->container->get('router')->generate('pat_admin_site_tiers'));
    }

    $em->remove($siteTiers);
    $em->flush();

    $this->container->get('session')->getFlashBag()->add('success', 'Le site tiers '.$siteName.' a bien été supprimé');

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_site_tiers'));
  }

}
