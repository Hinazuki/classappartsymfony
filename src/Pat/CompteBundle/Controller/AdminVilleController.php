<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;
use Pat\CompteBundle\Entity\Appartement;
use Pat\CompteBundle\Form\AdminReservationRechercheAppartementForm;
use Pat\CompteBundle\Entity\Ville;
use Pat\CompteBundle\Form\AdminVilleForm;

class AdminVilleController extends ContainerAware
{

  public function listeVilleAction()
  {
    $ville = '';
    $field_id = '';
    $request = $this->container->get('request');
    $formRecherche = $this->container->get('form.factory')->create(new AdminReservationRechercheAppartementForm());

    if ($request->getMethod() == 'POST') {
      $formRecherche->bind($request);
    }

    if ($request->isXmlHttpRequest()) {

      $ville = $request->request->get('ville');
      $field_id = $request->request->get('field_id');

      $em = $this->container->get('doctrine')->getManager();

      if ($ville != '') {
        $villes = $em->getRepository('PatCompteBundle:Ville')->getVillesByCaract($ville);
      }
      else {
        $villes = null;
      }
    }
    else {
      $villes = null;
    }
    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminVille:ajaxListe.html.twig', array(
        'villes' => $villes,
        'field_id' => $field_id
    ));
  }

  // demande correspond à un nom de ville demandé par un proprietaire. Il est envoyé par un admin qui aurait validé le traitement d'une demande (depuis la liste des demandes)
  public function ajouterAction($id = null, $id_demande = null, $ville_demande = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    $message = "";

    if (isset($id)) {
      $type = 'modif';
      $ville = $em->find('PatCompteBundle:Ville', $id);
    }
    else {
      $type = 'ajout';
      $ville = new Ville();
    }
    if ($id_demande && $ville_demande) {
      // On contrôle que la demande est valide
      $demande = $em->getRepository('PatCompteBundle:Formulaire')->findOneBy(array('id' => $id_demande, 'type' => "AccesVille"));
      if (!$demande) {
        throw new NotFoundHttpException("Cette demande est introuvable");
      }
      $ville->setNom($ville_demande);
      $form = $this->container->get('form.factory')->create(new AdminVilleForm(), $ville);
    }
    else {
      $form = $this->container->get('form.factory')->create(new AdminVilleForm(), $ville);
    }

    $request = $this->container->get('request');

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {
        $em->persist($ville);


        if ($id_demande && $ville_demande) {
          $em->remove($demande);
        }
        $em->flush();

        if ($id_demande && $ville_demande) {
          return new RedirectResponse($this->container->get('router')->generate('pat_admin_ville_demande_index'));
        }

        if ($type == 'modif') {
          $message = "La ville a bien été modifiée";
        }
        if ($type == 'ajout') {
          $message = "La ville a bien été ajoutée";
        }
      }
    }

    return $this->container->get('templating')->renderResponse(
        'PatCompteBundle:AdminVille:ajouter.html.twig', array(
        'ville' => $ville,
        'form' => $form->createView(),
        'type' => $type,
        'message' => $message
    ));
  }

  public function indexAction()
  {
    $em = $this->container->get('doctrine')->getManager();
    $session = $this->container->get('request')->getSession();
    $request = $this->container->get('request');

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    // Si l'on effctue une recherche, on stocke la recherche en session
    if ($request->getMethod() == 'POST') {
      $search_nom = $this->container->get('request')->request->get('search_nom');
      $search_cp = $this->container->get('request')->request->get('search_cp');
      $search_quartier = $this->container->get('request')->request->get('search_quartier');

      $session->set('ville_search_nom', $search_nom);
      $session->set('ville_search_cp', $search_cp);
      $session->set('ville_search_quartier', $search_quartier);
    }
    else {
      if ($this->container->get('request')->query->get('page')) {
        // On récupère les éléments de recherche en session
        $search_nom = $session->get('ville_search_nom');
        $search_cp = $session->get('ville_search_cp');
        $search_quartier = $session->get('ville_search_quartier');
      }
      else {
        // Si page n'est pas définie, on affiche toutes les villes
        $search_nom = null;
        $search_cp = null;
        $search_quartier = null;

        $session->set('ville_search_nom', $search_nom);
        $session->set('ville_search_cp', $search_cp);
        $session->set('ville_search_quartier', $search_quartier);
      }
    }

    $villes = $em->getRepository('PatCompteBundle:Ville')->getVillesBySearch($search_nom, $search_cp, $search_quartier);

    $paginator = $this->container->get('knp_paginator');
    $pagination = $paginator->paginate(
      $villes, $this->container->get('request')->query->get('page', 1), 30/* limit per page */
    );


    $deleteable = array();
    // On stocke pour chaque ville une valeur qui signifie si on peut la supprimer ou non. (Eq : Si elle est liée à des appartements ou non)
    foreach ($pagination as $ville) {
      $testbien = $em->getRepository('PatCompteBundle:Appartement')->findBy(array('ville' => $ville));

      $deleteable[$ville->getId()] = count($testbien) == 0 ? true : false;
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminVille:liste.html.twig', array('villes' => $pagination,
        'deleteable' => $deleteable)
    );
  }

  public function supprimerAction($id)
  {
    $em = $this->container->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    $ville = $em->find('PatCompteBundle:Ville', $id);

    if (!$ville) {
      throw new NotFoundHttpException("Ville non trouvée");
    }

    // On test que la ville n'est pas rattachée à des appartements
    $appartements = $em->getRepository('PatCompteBundle:Appartement')->findBy(array('ville' => $ville->getId()));
    if (count($appartements) == 0) {
      $em->remove($ville);
      $em->flush();
      $this->container->get('session')->getFlashBag()->add('success', 'La ville a correctement été supprimée');
    }
    else
      $this->container->get('session')->getFlashBag()->add('error', 'La ville ne peut pas être supprimée');

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_ville_index'));
  }

  public function rechercherAction()
  {
    $em = $this->container->get('doctrine')->getManager();
    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if (!$user) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminVille:menu.html.twig');
  }

  /* public function listeDemandesAction()
    {
    $em = $this->container->get('doctrine')->getManager();
    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if(!$user){
    throw new AccessDeniedException('This user does not have access to this section.');
    }

    $demandes = $em->getRepository('PatCompteBundle:Formulaire')->findBy(array('type' => "AccesVille"));

    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminVille:listeDemandes.html.twig',
    array('demandes' => $demandes)
    );
    } */

  /* public function supprimeDemandeAction($id_demande)
    {
    $em = $this->container->get('doctrine')->getManager();
    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
    if(!$user){
    throw new AccessDeniedException('This user does not have access to this section.');
    }


    $demande = $em->getRepository('PatCompteBundle:Formulaire')->findOneBy(array('id' => $id_demande, 'type' => "AccesVille"));
    if(!$demande)
    {
    throw new NotFoundHttpException("Cette demande est introuvable");
    }

    $em->remove($demande);
    $em->flush();

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_ville_demande_index'));
    } */
}
