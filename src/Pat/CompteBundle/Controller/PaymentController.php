<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Cookie;
use Pat\CompteBundle\Form\PaymentForm;
use Pat\CompteBundle\Entity\Payment;
use Pat\CompteBundle\Tools\PaymentTools;
use Pat\CompteBundle\Tools\ReservationTools;
use Pat\CompteBundle\Form\PaymentModeForm;
use Pat\FrontBundle\Lib\Etransactions;

class PaymentController extends Controller
{

  /**
   * En admin, génère un paiement pour un utilisateur, à partir de la vue du compte.
   */
  public function generatePaymentAction(Request $request, $member_id, $id_reservation = null)
  {
    $em = $this->container->get('doctrine')->getManager();
    $reservation = null;
    $member = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($member_id);

    if (!$member) {
      throw $this->createNotFoundException('Unable to find Member entity.');
    }

    $payment = new Payment();

    if (!is_null($id_reservation)) {
      $reservation = $em->getRepository('PatCompteBundle:Reservation')->find($id_reservation);
      if (!$reservation) {
        throw $this->createNotFoundException('Unable to find Reservation.');
      }
      // On associe le paiement à la réservation
      $payment->setReservation($reservation);
    }

    if (!is_null($request->request->get('payment_amount')) && !is_null($request->request->get('payment_name'))) {
      $payment->setAmount($request->request->get('payment_amount'));
      $payment->setName($request->request->get('payment_name'));
      $payment->setMsgContent("Paiement de la somme de ".$request->request->get('payment_amount')."€ pour ".$request->request->get('payment_name'));
    }

    $form = $this->container->get('form.factory')->create(new PaymentForm(), $payment);

    if ($request->getMethod() == 'POST' && is_null($request->request->get('payment_amount')) && is_null($request->request->get('payment_name'))) {
      $form->bind($request);

      if ($form->isValid()) {
        $payment->setMember($member);

        $em->persist($payment);
        $em->flush();

        $url = $request->getSchemeAndHttpHost().$this->container->get('router')->generate('pat_payment', array('token' => $payment->getToken())
        );
        $link = "<h5><a href=\"".$url."\">Cliquez ici pour procéder au paiement.</a></h5>";

        //envoi du mail au locataire
        $message_email = \Swift_Message::newInstance()
          ->setSubject($payment->getName())
          ->setFrom('contact@class-appart.com')
          ->setTo($member->getEmail())
          ->setBcc($this->container->getParameter('mail_admin'));

//                if($member->getMailsSecond())
//                    $message_email->setCc(explode(',', $member->getMailsSecond()));
        if ($member->getSecondEmail()) {
          $message_email->setCc($member->getSecondEmail());
        }

        $textBody = $this->container->get('templating')->render('PatCompteBundle:Payment:email.txt.twig', array('payment' => $payment, 'url' => $url));
        $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Payment:email.html.twig', array('payment' => $payment, 'link' => $link));

        if (!empty($htmlBody)) {
          $message_email->setBody($htmlBody, 'text/html')
            ->addPart($textBody, 'text/plain');
        }
        else
          $message_email->setBody($textBody);

        if ($this->container->get('mailer')->send($message_email))
          $this->container->get('session')->getFlashBag()->add('success', 'Le message a été envoyé !');
        else
          $this->container->get('session')->getFlashBag()->add('error', 'Echec de l\'envoi du message !');
      }
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Payment:ajouter.html.twig', array(
        'form' => $form->createView(),
        'member' => $member,
        'reservation' => $reservation
    ));
  }

  /**
   * En admin, liste les paiements pour un utilisateur.
   */
  public function listByMemberAction($member_id)
  {
    $em = $this->container->get('doctrine')->getManager();
    $member = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($member_id);

    if (!$member) {
      throw $this->createNotFoundException('Unable to find Member entity.');
    }

    $payments = $em->getRepository('PatCompteBundle:Payment')->findBy(array("member" => $member), array("createdAt" => "DESC"));

    $paginator = $this->container->get('knp_paginator');
    $pagination = $paginator->paginate(
      $payments,
      $this->getRequest()->query->get('page', 1), // page number
      20 // limit per page
    );


    if ($member->getType() == "Locataire")
      return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminLocataire:listPayments.html.twig', array(
          'locataire' => $member,
          'payments' => $pagination
      ));
    else
      return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminProprietaire:listPayments.html.twig', array(
          'proprietaire' => $member,
          'payments' => $pagination
      ));
  }

  /**
   * Page associée au lien envoyé par courriel avec generatePaymentAction.
   * Choix du mode de paiement, dont CB.
   */
  public function paymentAction(Request $request, $token = null)
  {
    $em = $this->container->get('doctrine')->getManager();
    $user_context = $this->container->get('security.context')->getToken()->getUser();

    $payment = $em->getRepository('PatCompteBundle:Payment')->findOneBy(array("token" => $token, "member" => $user_context));

    if (!$payment) {
      throw new NotFoundHttpException('Unable to find payment.');
    }

    $formPaymentMode = $this->container->get('form.factory')->create(new PaymentModeForm(), $payment);

    if ($request->getMethod() == 'POST') {
      $formPaymentMode->bind($request);

      if ($formPaymentMode->isValid()) {
        $payment->setStatus(PaymentTools::CONSTANT_STATUS_WAIT_RECEIPT);

        $em->flush();

        // Envoi du mail de confirmation
        $message_email = \Swift_Message::newInstance()
          ->setFrom('contact@class-appart.com')
          ->setTo($payment->getMember()->getEmail())
          ->setBcc($this->container->getParameter('mail_admin'));

//                if($payment->getMember()->getMailsSecond())
//                    $message_email->setCc(explode(',', $payment->getMember()->getMailsSecond()));
        if ($payment->getMember()->getSecondEmail()) {
          $message_email->setCc($payment->getMember()->getSecondEmail());
        }

        if ($payment->getModePaiement() == "virement") {
          $message_email->setSubject("Votre demande de paiement ".$payment->getId()." chez Class Appart : paiement par virement bancaire");
          $textBody = $this->container->get('templating')->render('PatCompteBundle:Payment:mail_demande_virement.txt.twig', array('payment' => $payment));
          $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Payment:mail_demande_virement.html.twig', array('payment' => $payment));
        }
        else {
          $message_email->setSubject("Votre demande de paiement ".$payment->getId()." chez Class Appart : paiement par chèque");
          $textBody = $this->container->get('templating')->render('PatCompteBundle:Payment:mail_demande_cheque.txt.twig', array('payment' => $payment));
          $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Payment:mail_demande_cheque.html.twig', array('payment' => $payment));
        }

        if (!empty($htmlBody)) {
          $message_email->setBody($htmlBody, 'text/html')
            ->addPart($textBody, 'text/plain');
        }
        else
          $message_email->setBody($textBody);

        $this->container->get('mailer')->send($message_email);

        return $this->container->get('templating')->renderResponse('PatCompteBundle:Payment:paiementConfirmation.html.twig', array(
            'status' => $payment->getModePaiement(),
        ));
      }
    }

    $viewData = array(
      'payment' => $payment,
      'formPaymentMode' => $formPaymentMode->createView(),
      'etransactions' => $this->container->getParameter('etransactions'),
      'formData' => Etransactions::prepareDataForPayment(
        $this->container->getParameter('etransactions'), $this->container->get('router'), $payment
      )
    );

    return $this->container->get('templating')->renderResponse(
        'PatCompteBundle:Payment:paiement.html.twig', $viewData
    );
  }

  /**
   * Page appelée par le site la banque en communication de serveur à serveur.
   */
  public function paymentResultAction(Request $request)
  {
    $em = $this->container->get('doctrine')->getManager();

    $error = "";
    $data = $this->container->get('request')->query->all();

    if (isset($data['reponse']) && isset($data['ref']) && isset($data['montant'])) {

    $payment = $em->getRepository('PatCompteBundle:Payment')->find($data['ref']);

      // si on ne trouve pas le paiement
    if (!$payment) {
      throw new NotFoundHttpException("Paiement non trouvée");
    }

      if ($data['reponse'] != Etransactions::SUCCESSFUL) {
        $error .= "Erreur ".$data['reponse'].": ".Etransactions::getResponseMessage($data['reponse'])." \n";
      }


      // si aucune erreur, on set le paiement à validé
      if ($error == "" && isset($data['autorisation'])) {
        $payment->setStatus(PaymentTools::CONSTANT_STATUS_PAYED);
        $payment->setModePaiement("CB");
        $payment->setPaymentResult($data);

        $em->flush();

        if ($payment->getReservation())
          $this->container->get('pat.reservation_manager')->updateStatus($payment->getReservation());

        $message_email = \Swift_Message::newInstance()
          ->setSubject("Paiement ".$payment->getId()." chez Class Appart ")
          ->setFrom('contact@class-appart.com')
          ->setTo($payment->getMember()->getEmail())
          ->setBcc($this->container->getParameter('mail_admin'));

//                if($payment->getMember()->getMailsSecond())
//                    $message_email->setCc(explode(',', $payment->getMember()->getMailsSecond()));
        if ($payment->getMember()->getSecondEmail()) {
          $message_email->setCc($payment->getMember()->getSecondEmail());
        }

        $textBody = $this->container->get('templating')->render('PatCompteBundle:Payment:mail_confirmation_CB.txt.twig', array('payment' => $payment));
        $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Payment:mail_confirmation_CB.html.twig', array('payment' => $payment));

        if (!empty($htmlBody)) {
          $message_email->setBody($htmlBody, 'text/html')
            ->addPart($textBody, 'text/plain');
        }
        else
          $message_email->setBody($textBody);

        $this->container->get('mailer')->send($message_email);
      }
      // Sinon l'on stock l'erreur rencontrée, et on envoi un mail
      else {
        $payment->setStatus(PaymentTools::CONSTANT_STATUS_CANCELED);
        $payment->setPaymentResult($data);
        $payment->setPaymentError($error);

        $em->flush();

        $message_email = \Swift_Message::newInstance()
          ->setSubject("Echec du paiement par carte bancaire sur class-appart.com")
          ->setFrom('contact@class-appart.com')
          ->setTo($payment->getMember()->getEmail())
          ->setBcc($this->container->getParameter('mail_admin'));

//                if($payment->getMember()->getMailsSecond())
//                    $message_email->setCc(explode(',', $payment->getMember()->getMailsSecond()));
        if ($payment->getMember()->getSecondEmail()) {
          $message_email->setCc($payment->getMember()->getSecondEmail());
        }

        $textBody = $this->container->get('templating')->render('PatCompteBundle:Payment:mail_refus_CB.txt.twig', array('payment' => $payment));
        $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Payment:mail_refus_CB.html.twig', array('payment' => $payment));

        if (!empty($htmlBody)) {
          $message_email->setBody($htmlBody, 'text/html')
            ->addPart($textBody, 'text/plain');
        }
        else
          $message_email->setBody($textBody);

        $this->container->get('mailer')->send($message_email);
      }
    }

    $response = new Response();
    $response->setStatusCode(200);
    return $response;
  }

  /**
   * Page de retour de paiement de la banque, côté client, pour les état succès, refusé, annulé.
   */
  public function confirmationAction(Request $request, $status = null)
  {
    $session = $request->getSession();
    $em = $this->container->get('doctrine')->getManager();

    $tab = array("effectue", "refuse", "annule");

    if (!in_array($status, $tab))
      throw new NotFoundHttpException("Statut de retour incorrect");

    if ($status == "annule") {
      $data = $this->container->get('request')->query->all();

      $error = "Erreur : La transaction a été annulée";

      $payment = $em->getRepository('PatCompteBundle:Payment')->find($data['ref']);

      // si on ne trouve pas la réservation, on envoi un mail contenant les informations de paiement
      if (!$payment) {
        throw new NotFoundHttpException("Paiement non trouvé");
      }

      $payment->setStatus(PaymentTools::CONSTANT_STATUS_CANCELED);
      $payment->setModePaiement("CB");
      $payment->setPaymentResult($data);
      $payment->setPaymentError($error);

      $em->flush();

      $message_email = \Swift_Message::newInstance()
        ->setSubject("Annulation du paiement par carte bancaire sur class-appart.com")
        ->setFrom('contact@class-appart.com')
        ->setTo($payment->getMember()->getEmail())
        ->setBcc($this->container->getParameter('mail_admin'));

//            if($payment->getMember()->getMailsSecond())
//                $message_email->setCc(explode(',', $payment->getMember()->getMailsSecond()));
      if ($payment->getMember()->getSecondEmail()) {
        $message_email->setCc($payment->getMember()->getSecondEmail());
      }

      $textBody = $this->container->get('templating')->render('PatCompteBundle:Payment:mail_annulation_CB.txt.twig', array('payment' => $payment));
      $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Payment:mail_annulation_CB.html.twig', array('payment' => $payment));

      if (!empty($htmlBody)) {
        $message_email->setBody($htmlBody, 'text/html')
          ->addPart($textBody, 'text/plain');
      }
      else
        $message_email->setBody($textBody);

      $this->container->get('mailer')->send($message_email);
    }

    $transactionResponse = Etransactions::getResponseMessage($request->query->get('reponse'));

    return $this->render(
      'PatCompteBundle:Payment:paiementConfirmation.html.twig',
      compact('status', 'transactionResponse')
    );
  }

  /**
   * En admin, permet de valider un paiement en mode chèque/virement.
   */
  public function validerAction($payment_id = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    $payment = $em->getRepository('PatCompteBundle:Payment')->find($payment_id);

    if (($payment->getModePaiement() == "cheque" || $payment->getModePaiement() == "virement") && $payment->getStatus() == PaymentTools::CONSTANT_STATUS_WAIT_RECEIPT) {
      $payment->setStatus(PaymentTools::CONSTANT_STATUS_PAYED);
      $em->flush();

      if ($payment->getReservation()) {
        $old_status = $payment->getReservation()->getStatus();
        $this->container->get('pat.reservation_manager')->updateStatus($payment->getReservation());

        // On envoie le mail uniquement si la réservation vient de passer au statut "Acompte payé"
        if ($payment->getReservation()->getStatus() == ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED && $old_status != ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED) {
          // On envoie le mail de demande de validation de réservation au propriétaire

          $message_email = \Swift_Message::newInstance()
            ->setSubject("Confirmation de réservation Class Appart")
            ->setFrom('contact@class-appart.com')
            ->setTo($payment->getReservation()->getAppartement()->getUtilisateur()->getEmail())
            ->setBcc($this->container->getParameter('mail_admin'));

//                    if($payment->getReservation()->getAppartement()->getUtilisateur()->getMailsSecond())
//                        $message_email->setCc(explode(',', $payment->getReservation()->getAppartement()->getUtilisateur()->getMailsSecond()));
          if ($payment->getReservation()->getAppartement()->getUtilisateur()->getSecondEmail()) {
            $message_email->setCc($payment->getReservation()->getAppartement()->getUtilisateur()->getSecondEmail());
          }
          $textBody = $this->container->get('templating')->render('PatCompteBundle:Reservation:email_valid_prop.txt.twig', array('reservation' => $payment->getReservation()));
          $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Reservation:email_valid_prop.html.twig', array('reservation' => $payment->getReservation()));

          if (!empty($htmlBody)) {
            $message_email->setBody($htmlBody, 'text/html')
              ->addPart($textBody, 'text/plain');
          }
          else
            $message_email->setBody($textBody);

          $this->container->get('mailer')->send($message_email);
          unset($message_email);
        }
      }

      $em->flush();

      $message_email = \Swift_Message::newInstance()
        ->setSubject("Votre paiement réf. ".$payment->getId()." chez Class Appart est arrivé")
        ->setFrom('contact@class-appart.com')
        ->setTo($payment->getMember()->getEmail())
        ->setBcc($this->container->getParameter('mail_admin'));

//            if($payment->getMember()->getMailsSecond())
//                $message_email->setCc(explode(',', $payment->getMember()->getMailsSecond()));
      if ($payment->getMember()->getSecondEmail()) {
        $message_email->setCc($payment->getMember()->getSecondEmail());
      }

      $textBody = $this->container->get('templating')->render('PatCompteBundle:Payment:mail_confirmation_cheque-virement.txt.twig', array('payment' => $payment));
      $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Payment:mail_confirmation_cheque-virement.html.twig', array('payment' => $payment));

      if (!empty($htmlBody)) {
        $message_email->setBody($htmlBody, 'text/html')
          ->addPart($textBody, 'text/plain');
      }
      else
        $message_email->setBody($textBody);

      $this->container->get('mailer')->send($message_email);

      $this->container->get('session')->getFlashBag()->add('success', 'Le paiement a correctement été validé');
    }
    else
      $this->container->get('session')->getFlashBag()->add('error', 'Le paiement ne peut pas être validé');


    return new RedirectResponse($this->container->get('router')->generate('pat_admin_list_payment_by_member', array('member_id' => $payment->getMember()->getId())));
  }

  /**
   * En admin, ajoute un paiement/remboursement, directement au statut "payé".
   */
  public function newAction(Request $request, $member_id, $id_reservation = null)
  {
    $em = $this->container->get('doctrine')->getManager();
    $reservation = null;
    $member = $em->getRepository('PatUtilisateurBundle:Utilisateur')->find($member_id);

    if (!$member) {
      throw $this->createNotFoundException('Unable to find Member entity.');
    }

    $payment = new Payment();
    $payment->setMember($member);

    if (!is_null($id_reservation)) {
      $reservation = $em->getRepository('PatCompteBundle:Reservation')->find($id_reservation);
      if (!$reservation) {
        throw $this->createNotFoundException('Unable to find Reservation.');
      }
      // On associe le paiement à la réservation
      $payment->setReservation($reservation);
    }

    $form = $this->container->get('form.factory')->create(new PaymentForm(), $payment);

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {

        $data = $form->getData();
        $payed_at = $data->getPayedAt();

        $em->persist($payment);
        $em->flush();

        $payment->setStatus(PaymentTools::CONSTANT_STATUS_PAYED);
        if (!is_null($payed_at))
          $payment->setPayedAt($payed_at);
        $em->flush();

        if ($payment->getReservation())
          $this->container->get('pat.reservation_manager')->updateStatus($payment->getReservation());

        $this->container->get('session')->getFlashBag()->add(
          'success', 'Le paiement a été créé'
        );

        if ($reservation)
          return new RedirectResponse($this->container->get('router')->generate('pat_admin_reservation_show', array('id_reservation' => $reservation->getId())));
      }
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Payment:new.html.twig', array(
        'form' => $form->createView(),
        'member' => $member,
        'reservation' => $reservation
    ));
  }

  /**
   * En admin, permet de modifier un paiement.
   */
  public function editAction(Request $request, $id_payment)
  {

    $em = $this->container->get('doctrine')->getManager();

    $payment = $em->getRepository('PatCompteBundle:Payment')->find($id_payment);

    if (!$payment) {
      throw $this->createNotFoundException('Unable to find Payment.');
    }

    $form = $this->container->get('form.factory')->create(new PaymentForm(), $payment);

    if ($request->getMethod() == 'POST') {
      $form->bind($request);

      if ($form->isValid()) {
        $em->flush();

        if ($payment->getReservation())
          $this->container->get('pat.reservation_manager')->updateStatus($payment->getReservation());

        $this->container->get('session')->getFlashBag()->add(
          'success', 'Le paiement a été modifié'
        );

        return new RedirectResponse($this->container->get('router')->generate('pat_admin_list_payment_by_member', array('member_id' => $payment->getMember()->getId())));
      }
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Payment:new.html.twig', array(
        'form' => $form->createView(),
        'member' => $payment->getMember(),
        'reservation' => $payment->getReservation(),
        'id_payment' => $id_payment
    ));
  }

  /**
   * En admin, permet de supprimer un paiement.
   */
  public function deleteAction(Request $request, $id_payment)
  {

    $em = $this->container->get('doctrine')->getManager();

    $payment = $em->getRepository('PatCompteBundle:Payment')->find($id_payment);
    if (!$payment) {
      throw $this->createNotFoundException('Unable to find Payment.');
    }

    $memberId = $payment->getMember()->getId();

    $em->remove($payment);
    $em->flush();

    $this->container->get('session')->getFlashBag()->add(
      'success', 'Le paiement a été supprimé'
    );

    return new RedirectResponse($this->container->get('router')->generate('pat_admin_list_payment_by_member', array('member_id' => $memberId)));
  }

}

