<?php

namespace Pat\CompteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
//use Pat\CompteBundle\Entity\Reservation;
use Pat\CompteBundle\Entity\Payment;
//use Pat\CompteBundle\Form\ReservationForm;
//use Pat\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
//use Doctrine\Common\Collections\ArrayCollection;
use Pat\CompteBundle\Tools\ReservationTools;
use Pat\CompteBundle\Tools\PaymentTools;
use Pat\FrontBundle\Form\PaymentModeForm;
use Pat\FrontBundle\Lib\Etransactions;

class ReservationController extends Controller
{

  //lister les réservations pour un propriétaire
  public function indexAction()
  {
    $em = $this->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $arrayRoles = $user->getRoles(); //on test les droits
    if ($arrayRoles[0] == "ROLE_ADMIN" or $user->getType() != "Propriétaire") {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    // Les reservations qui concernent le proprietaire
    $reservation = $em->getRepository('PatCompteBundle:Reservation')->getResaByProprio($user->getId());

    $paginator = $this->container->get('knp_paginator');
    $pagination = $paginator->paginate(
      $reservation, $this->container->get('request')->query->get('page', 1)/* page number */, 10/* limit per page */
    );

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Reservation:liste.html.twig', array('reservation' => $pagination));
  }

  //détail de la réservation
  public function detailAction($id_reservation)
  {
    $em = $this->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $arrayRoles = $user->getRoles(); //on test les droits
    if ($arrayRoles[0] == "ROLE_ADMIN" or $user->getTypeUtilisateur() != "2") {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    $reservation = $em->getRepository('PatCompteBundle:Reservation')->getResaByIdandProprio($id_reservation, $user->getId());

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Reservation:detail.html.twig', [
        'resa' => $reservation[0],
        'nb_nuits' => $this->container->get('pat.date_functions')->nbJours($reservation[0]->getDateDebut(), $reservation[0]->getDateFin()),
    ]);
  }

  // Fonction qui permet d'annuler une réservation (On ne la détruit pas de la base on change juste son statut et on libere les dates du calendrier)
  public function desactiverAction($id_reservation)
  {
    $em = $this->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $arrayRoles = $user->getRoles(); //on test les droits
    if ($arrayRoles[0] == "ROLE_ADMIN" or $user->getTypeUtilisateur() != "2")
      throw new AccessDeniedException('This user does not have access to this section.');

    // modification d'un appartement existant : on recherche ses données en vérifiant que l'utilisateur à le droit me modifier l'appart
    $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneBy(array('id' => $id_reservation));

    if (!$reservation)
      throw new NotFoundHttpException("Réservation non trouvée");

    if ($reservation->getIsAnnule() == '1')
      return new RedirectResponse($this->container->get('router')->generate('pat_reservation_index'));

    // On Change le statut de la réservation
    $reservation->setIsAnnule(1);
//        $em->flush();
    // envoie d'un mail avec tout le descriptif de la réservation
    $locataire = $reservation->getUtilisateur();


    $message_email = \Swift_Message::newInstance()
      ->setSubject("Annulation de votre réservation - classAppart ®")
      ->setFrom('contact@class-appart.fr')
      ->setBcc($this->container->getParameter('mail_admin'))
      ->setTo("jb@laboitededev.com");
//            ->setTo($locataire->getEmail());

    $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Reservation:email_annulation.html.twig');

    $message_email->setBody($htmlBody, 'text/html');

    $this->container->get('mailer')->send($message_email);

    return new RedirectResponse($this->container->get('router')->generate('pat_reservation_index'));
  }

  //lister les réservations pour un locataire
  public function indexLocataireAction()
  {
    $em = $this->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $arrayRoles = $user->getRoles(); //on test les droits
    // On affiche également les réservations des comptes propriétaires
    if ($user->getType() != "Locataire" && $user->getType() != "Propriétaire") {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    $reservation = $em->getRepository('PatCompteBundle:Reservation')->findBy(array("utilisateur" => $user->getId()), array("created_at" => "DESC"));

    $paginator = $this->container->get('knp_paginator');
    $pagination = $paginator->paginate(
      $reservation, $this->container->get('request')->query->get('page', 1)/* page number */, 10/* limit per page */
    );

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Reservation:liste_loc.html.twig', array('reservation' => $pagination));
  }

  //détail de la réservation
  public function detailLocataireAction($id_reservation)
  {
    $em = $this->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

    $arrayRoles = $user->getRoles(); //on test les droits
    if ($user->getType() != "Locataire" && $user->getType() != "Propriétaire") {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    //on recherche la réservation et on test si elle a bien été passée par le locataire
    $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneBy(array("id" => $id_reservation, "utilisateur" => $user));

    if (!$reservation)
      throw new NotFoundHttpException("Réservation non trouvée");

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Reservation:detail.html.twig', [
        'resa' => $reservation,
        'nb_nuits' => $this->container->get('pat.date_functions')->nbJours($reservation->getDateDebut(), $reservation->getDateFin()),
    ]);
  }

  /**
   * Termine de traiter la transaction bancaire.
   * Celle méthode est appelée directement par le serveur de la banque.
   * Communication de serveur à serveur.
   *
   * @param Request $request
   * @return Response
   * @throws NotFoundHttpException
   * @author refactoring AG
   */
  public function paymentResultAction(Request $request)
  {
    $response = new Response();
    $response->setStatusCode(200);

    $result = $request->query->all();

    if (!isset($result['reponse'])
        && !isset($result['ref'])
        && !isset($result['montant'])) {
      return $response;
    }

    $em = $this->get('doctrine')->getManager();

    $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneByReference($result['ref']);

    if (!$reservation) {
      //throw new NotFoundHttpException('Réservation non trouvée.');
      return $response;
    }

    if ($result['reponse'] != Etransactions::SUCCESSFUL) {
      $this->cancelBooking($reservation, $result);
      return $response;
    }

    $this->validBooking($reservation, $result);

    // On envoie le mail de demande de validation de réservation au propriétaire
    $this->sendValidationEmailToOwner($reservation);

    // Si la réservation est faite moins de 48h avant
    if ($reservation->getDateDebut() <= date('Y-m-d', strtotime('+2 days'))) {
      $reservation->setValidProp(ReservationTools::CONSTANT_VALID_RESA_48);
      $em->flush();

      // AG Est-ce vraiment nécessaire ?
      $this->send2ndValidationEmailToOwner($reservation);
    }

    // On envoie le mail de confirmation de paiement au locataire.
    $this->sendConfirmationEmailToTenant($reservation);

    // Dans tous les cas, on retourne 200 à la banque.
    return $response;
  }

  /**
   * Valide la réservation et le paiement.
   *
   * @param \Reservation $reservation
   * @param array $transactionResult
   * @author refactoring AG
   */
  protected function validBooking($reservation, $transactionResult)
  {
    $em = $this->get('doctrine')->getManager();

    // si aucune erreur, on set la réservation validée par la banque
    $reservation->setStatus(ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED);
    $reservation->setValidProp(null);
    $reservation->setModePaiement('CB');
    $reservation->setPaymentResult($transactionResult);
    $reservation->setPaymentError(null);

    $em->flush();
    
    // On crée un objet Payment correspondant au paiement de l'acompte
    $payment = new Payment();
    $payment->setReservation($reservation);
    $payment->setMember($reservation->getUtilisateur());
    $payment->setAmount($reservation->getArrhes());
    $payment->setPaymentResult($transactionResult);
    $payment->setName('Acompte réservation réf. '.$reservation->getReference());

    $em->persist($payment);
    $em->flush();

    // En deux fois cause n'enregistre pas convenablement au premier coup.
    $payment->setModePaiement('CB');
    $payment->setStatus(PaymentTools::CONSTANT_STATUS_PAYED);

    $em->flush();

    $this->container->get('pat.reservation_manager')->updateStatus($reservation);
    $em->flush();
  }

  /**
   * Annule la réservation et envoie un courriel au locataire.
   *
   * @param \Reservation $reservation
   * @param array $transactionResult
   * @author refactoring AG
   */
  protected function cancelBooking($reservation, $transactionResult)
  {
    $em = $this->get('doctrine')->getManager();

    $reservation->setStatus(ReservationTools::CONSTANT_STATUS_CANCELED_BY_BANK);
    $reservation->setPaymentResult($transactionResult);
    $reservation->setPaymentError(Etransactions::getResponseMessage($transactionResult['reponse']));

    $em->flush();

    $emailMessage = \Swift_Message::newInstance()
      ->setSubject('Echec du paiement par carte bancaire sur class-appart.com')
      ->setFrom('contact@class-appart.com')
      ->setBcc($this->container->getParameter('mail_admin'))
      ->setTo($reservation->getUtilisateur()->getEmail());

    $secondEmail = $reservation->getUtilisateur()->getSecondEmail();
    if ($secondEmail) {
      $emailMessage->setCc($secondEmail);
    }

    $params = array(
      'reservation' => $reservation,
      'transactionResponse' => Etransactions::getResponseMessage($transactionResult['reponse'])
    );
    $textBody = $this->renderView(
      'PatFrontBundle:Reservation:mail_refus_CB.txt.twig',
      $params
    );
    $htmlBody = $this->renderView(
      'PatFrontBundle:Reservation:mail_refus_CB.html.twig',
      $params
    );

    if (!empty($htmlBody)) {
      $emailMessage
        ->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain');
    }
    else {
      $emailMessage->setBody($textBody);
    }

    $this->get('mailer')->send($emailMessage);
  }

  /**
   * Envoie un courriel de validation au propriétaire.
   *
   * @param \Reservation $reservation
   * @author refactoring AG
   */
  protected function sendValidationEmailToOwner($reservation)
  {
    $emailMessage = \Swift_Message::newInstance()
      ->setSubject('Confirmation de réservation Class Appart')
      ->setFrom('contact@class-appart.com')
      ->setBcc($this->container->getParameter('mail_admin'))
      ->setTo($reservation->getAppartement()->getUtilisateur()->getEmail());

    $secondEmail = $reservation->getAppartement()->getUtilisateur()->getSecondEmail();
    if ($secondEmail) {
      $emailMessage->setCc($secondEmail);
    }

    $params = compact('reservation');
    $textBody = $this->renderView(
      'PatCompteBundle:Reservation:email_valid_prop.txt.twig',
      $params
    );
    $htmlBody = $this->renderView(
      'PatCompteBundle:Reservation:email_valid_prop.html.twig',
      $params
    );

    if (!empty($htmlBody)) {
      $emailMessage
        ->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain');
    }
    else {
      $emailMessage->setBody($textBody);
    }

    $this->get('mailer')->send($emailMessage);
  }

  /**
   * Envoie un courriel de validation au propriétaire.
   *
   * @param \Reservation $reservation
   * @author refactoring AG
   */
  protected function send2ndValidationEmailToOwner($reservation)
  {
    $emailMessage = \Swift_Message::newInstance()
      ->setSubject('Confirmation de réservation Class Appart')
      ->setFrom('contact@class-appart.com')
      ->setTo($reservation->getAppartement()->getUtilisateur()->getEmail())
      ->setBcc($this->container->getParameter('mail_admin'));

    $secondEmail = $reservation->getAppartement()->getUtilisateur()->getSecondEmail();
    if ($secondEmail) {
      $emailMessage->setCc($secondEmail);
    }

    $params = array('reservation' => $reservation, 'statut' => 'validé');
    $textBody = $this->renderView(
      'PatCompteBundle:Reservation:email_valid_cancel.txt.twig',
      $params
    );
    $htmlBody = $this->renderView(
      'PatCompteBundle:Reservation:email_valid_cancel.html.twig',
      $params
    );

    if (!empty($htmlBody)) {
      $emailMessage
        ->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain');
    }
    else {
      $emailMessage->setBody($textBody);
    }

    $this->get('mailer')->send($emailMessage);
  }

  /**
   * Envoie un courriel de confirmation au locataire.
   *
   * @param \Reservation $reservation
   * @author refactoring AG
   */
  protected function sendConfirmationEmailToTenant($reservation)
  {
    $emailMessage = \Swift_Message::newInstance()
      ->setSubject('Paiement de votre réservation '.$reservation->getReference().' chez Class Appart ')
      ->setFrom('contact@class-appart.com')
      ->setTo($reservation->getUtilisateur()->getEmail())
      ->setBcc($this->container->getParameter('mail_admin'));

    $secondEmail = $reservation->getUtilisateur()->getSecondEmail();
    if ($secondEmail) {
      $emailMessage->setCc($secondEmail);
    }

    $params = compact('reservation');
    $textBody = $this->renderView(
      'PatFrontBundle:Reservation:mail_confirmation_CB.txt.twig',
      $params
    );
    $htmlBody = $this->renderView(
      'PatFrontBundle:Reservation:mail_confirmation_CB.html.twig',
      $params
    );

    if (!empty($htmlBody)) {
      $emailMessage
        ->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain');
    }
    else {
      $emailMessage->setBody($textBody);
    }

    $this->get('mailer')->send($emailMessage);
  }

  public function validationAction($ref_reservation = null)
  {
    $em = $this->get('doctrine')->getManager();
    $request = $this->container->get('request');
    $session = $request->getSession();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();

    $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneBy(array('reference' => $ref_reservation, 'utilisateur' => $user_context));

    if (!$reservation)
      throw new NotFoundHttpException("Réservation non trouvée");

    if ($request->getMethod() == 'POST' && $request->request->get('Validation')) {

      if ($request->request->get('cgv') && $request->request->get('documents')) {

        return new RedirectResponse($this->container->get('router')->generate('pat_reservation_paiement', array('ref_reservation' => $reservation->getReference())));
      }
      else {
        if (!$request->request->get('cgv'))
          $this->container->get('session')->getFlashBag()->add('error', "Vous devez accepter les Conditions Générales De Vente");
        if (!$request->request->get('documents'))
          $this->container->get('session')->getFlashBag()->add('error', "Vous devez confirmer disposer des documents nécessaires");
      }
    }

    $nb_nuits = $this->container->get('pat.date_functions')->nbJours($reservation->getDateDebut(), $reservation->getDateFin());

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Reservation:validation.html.twig', array(
        'appartement' => $reservation->getAppartement(),
        'reservation' => $reservation,
        'nb_nuits' => $nb_nuits
    ));
  }

  public function paiementAction($ref_reservation = null)
  {
    $em = $this->get('doctrine')->getManager();
    $request = $this->container->get('request');
    $session = $request->getSession();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();

    $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneBy(array('reference' => $ref_reservation, 'utilisateur' => $user_context));

    if (!$reservation)
      throw new NotFoundHttpException("Réservation non trouvée");


    $session->set('resa_id', $reservation->getId());
    $session->set('resa_arrhes', $reservation->getArrhes());

    $appartement = $reservation->getAppartement();

    $formPaymentMode = $this->container->get('form.factory')->create(new PaymentModeForm(), $reservation);

    $viewData = array(
      'reservation' => $reservation,
      'appartement' => $appartement,
      'formPaymentMode' => $formPaymentMode->createView(),
      'etransactions' => $this->container->getParameter('etransactions'),
      'formData' => Etransactions::prepareDataForReservation(
        $this->container->getParameter('etransactions'), $this->container->get('router'), $reservation
      )
    );

    return $this->container->get('templating')->renderResponse(
        'PatFrontBundle:Reservation:paiement.html.twig', $viewData
    );
  }

  public function validProprioAction($ref = null, $statut = null)
  {
    $em = $this->get('doctrine')->getManager();

    //on récupère l'utilisateur courant
    $user_context = $this->container->get('security.context')->getToken()->getUser();

    $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneBy(array('reference' => $ref));

    if (!$reservation)
      throw new NotFoundHttpException("Réservation non trouvée");

    if ($reservation->getAppartement()->getUtilisateur() != $user_context)
      throw new NotFoundHttpException("Réservation non trouvée");

    // Si la valeur est null, on met à jour suivant le choix du proprio, sinon, on ne peut pas modifier le statut valid_prop, donc on ne fait rien
    if (is_null($reservation->getValidProp())) {
      if ($statut == "accept") {
        $reservation->setValidProp(ReservationTools::CONSTANT_VALID_RESA_PROP);
        $em->flush();

        /*         * ************************************************ */
        /* On envoi un mail de confirmation au propriétaire
          /************************************************** */

        $message_email = \Swift_Message::newInstance()
          ->setSubject("Confirmation de réservation Class Appart")
          ->setFrom('contact@class-appart.com')
          ->setBcc($this->container->getParameter('mail_admin'))
          ->setTo($reservation->getAppartement()->getUtilisateur()->getEmail());

//                    if($reservation->getAppartement()->getUtilisateur()->getMailsSecond())
//                        $message_email->setCc(explode(',', $reservation->getAppartement()->getUtilisateur()->getMailsSecond()));
        if ($reservation->getAppartement()->getUtilisateur()->getSecondEmail()) {
          $message_email->setCc($reservation->getAppartement()->getUtilisateur()->getSecondEmail());
        }

        $textBody = $this->container->get('templating')->render('PatCompteBundle:Reservation:email_valid_cancel.txt.twig', array('reservation' => $reservation, 'statut' => "validé"));
        $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Reservation:email_valid_cancel.html.twig', array('reservation' => $reservation, 'statut' => "validé"));

        if (!empty($htmlBody)) {
          $message_email->setBody($htmlBody, 'text/html')
            ->addPart($textBody, 'text/plain');
        }
        else
          $message_email->setBody($textBody);

        $this->container->get('mailer')->send($message_email);

        /*         * ************************************************ */
        /*         * ************************************************ */
      }
      elseif ($statut == "refuse") {
        $reservation->setValidProp(ReservationTools::CONSTANT_CANCEL_RESA_PROP);
        $reservation->setStatus(ReservationTools::CONSTANT_STATUS_CANCELED_BY_PROP);
        $em->flush();

        /*         * ************************************************ */
        // On envoi un mail d'annulation au propriétaire
        // et à l'admin
        /*         * ************************************************ */

        $message_email = \Swift_Message::newInstance()
          ->setSubject("Annulation de réservation Class Appart")
          ->setFrom('contact@class-appart.com')
          ->setBcc($this->container->getParameter('mail_admin'))
          ->setTo($reservation->getAppartement()->getUtilisateur()->getEmail());

//                    if($reservation->getAppartement()->getUtilisateur()->getMailsSecond())
//                        $message_email->setCc(explode(',', $reservation->getAppartement()->getUtilisateur()->getMailsSecond()));
        if ($reservation->getAppartement()->getUtilisateur()->getSecondEmail()) {
          $message_email->setCc($reservation->getAppartement()->getUtilisateur()->getSecondEmail());
        }

        $message_email->setBcc('info@class-appart.com');

        $textBody = $this->container->get('templating')->render('PatCompteBundle:Reservation:email_valid_cancel.txt.twig', array('reservation' => $reservation, 'statut' => "annulé"));
        $htmlBody = $this->container->get('templating')->render('PatCompteBundle:Reservation:email_valid_cancel.html.twig', array('reservation' => $reservation, 'statut' => "annulé"));

        if (!empty($htmlBody)) {
          $message_email->setBody($htmlBody, 'text/html')
            ->addPart($textBody, 'text/plain');
        }
        else
          $message_email->setBody($textBody);

        $this->container->get('mailer')->send($message_email);

        /*         * ************************************************ */
        /*         * ************************************************ */


        /*         * ************************************************ */
        /* On envoi un mail d'annulation au locataire
          /************************************************** */
        $message_email = \Swift_Message::newInstance()
          ->setSubject("Reservation  ".$reservation->getReference()." annulée chez Class Appart")
          ->setFrom('contact@class-appart.com')
          ->setBcc($this->container->getParameter('mail_admin'))
          ->setTo($reservation->getUtilisateur()->getEmail());

//                    if($reservation->getUtilisateur()->getMailsSecond())
//                        $message_email->setCc(explode(',', $reservation->getUtilisateur()->getMailsSecond()));
        if ($reservation->getUtilisateur()->getSecondEmail()) {
          $message_email->setCc($reservation->getUtilisateur()->getSecondEmail());
        }

        $textBody = $this->container->get('templating')->render('PatFrontBundle:Reservation:mail_annulation.txt.twig', array('reservation' => $reservation));
        $htmlBody = $this->container->get('templating')->render('PatFrontBundle:Reservation:mail_annulation.html.twig', array('reservation' => $reservation));

        if (!empty($htmlBody)) {
          $message_email->setBody($htmlBody, 'text/html')
            ->addPart($textBody, 'text/plain');
        }
        else
          $message_email->setBody($textBody);

        $this->container->get('mailer')->send($message_email);

        /*         * ************************************************ */
        /*         * ************************************************ */
      }
      else
        ;
    }

    return $this->container->get('templating')->renderResponse('PatCompteBundle:Reservation:valid_prop.html.twig', array(
        'reservation' => $reservation
        )
    );
  }

}
