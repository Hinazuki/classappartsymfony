<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Pat\UtilisateurBundle\Entity\Utilisateur;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Pat\CompteBundle\Repository\ReservationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Reservation
{

  /**
   * @var \Pat\CompteBundle\Entity\Appartement
   *
   * @ORM\ManyToOne(targetEntity="Appartement")
   * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
   */
  private $appartement;

  /**
   * @ORM\ManyToOne(targetEntity="Pat\UtilisateurBundle\Entity\Utilisateur", inversedBy="reservations")
   * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
   */
  private $utilisateur;

  /**
   * @ORM\OneToMany(targetEntity="Payment", mappedBy="reservation")
   */
  private $payments;

  /**
   * @ORM\OneToMany(targetEntity="Option", mappedBy="reservation", cascade={"persist", "remove"})
   */
  private $options;

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @var integer
   *
   * @ORM\Column(name="appartement_id", type="integer", nullable=true)
   */
  private $appartement_id;

  /**
   * @ORM\Column(columnDefinition="varchar(20) NULL")
   */
  private $reference;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL")
   */
  private $date_debut;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL")
   */
  private $date_fin;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL")
   */
  private $tarif;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $promotion;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL")
   */
  private $arrhes;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL")
   */
  private $depot;

  /**
   * @ORM\Column(columnDefinition="longtext NULL")
   */
  private $calcul_values;

  /**
   * @ORM\Column(columnDefinition="integer NULL")
   */
  private $nb_personne;

  /**
   * @var int
   *
   * @ORM\Column(name="nb_adulte", type="integer", nullable=true)
   */
  private $nb_adulte;

  /**
   * @var int
   *
   * @ORM\Column(name="nb_enfant", type="integer", nullable=true)
   */
  private $nb_enfant;

  /**
   * @ORM\Column(name="optionSupplementaires", type="boolean", nullable=true)
   */
  private $option_supplementaires;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $option_roses;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $option_champagne;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $option_menage_supp;

  /**
   * @ORM\Column(name="optionLitBebe", type="boolean", nullable=true)
   */
  private $option_lit_bebe;

  /**
   * @ORM\Column(name="optionTaxi", type="boolean", nullable=true)
   */
  private $option_taxi;

  /**
   * @ORM\Column(name="optionFrigo", type="boolean", nullable=true)
   */
  private $option_frigo;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $status;

  /**
   * @ORM\Column(name="valid_prop", type="integer", nullable=true)
   */
  private $valid_prop;

  /**
   * @ORM\Column(columnDefinition="varchar(8) DEFAULT 'CB'")
   * @Assert\Choice(choices = {"CB", "cheque", "virement"}, message = "Choose a valid payment mode.")
   */
  private $mode_paiement;

  /**
   * @ORM\Column(columnDefinition="longtext NULL")
   */
  private $payment_result;

  /**
   * @ORM\Column(columnDefinition="varchar(256) NULL")
   */
  private $payment_error;

  /**
   * @ORM\Column(columnDefinition="datetime NULL")
   */
  private $date_validite;

  /**
   * @ORM\Column(columnDefinition="datetime NULL")
   */
  private $created_at;

  /**
   * @ORM\Column(columnDefinition="datetime NULL")
   */
  private $updated_at;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $updated_by_admin;

  /**
   * @ORM\Column(columnDefinition="varchar(255) NULL")
   */
  private $filename_fr;

  /**
   * @ORM\Column(columnDefinition="varchar(255) NULL")
   */
  private $filename_en;

  /**
   * @var string
   *
   * @ORM\Column(columnDefinition="longtext NULL")
   */
  private $comment;

  /**
   * @var int
   *
   * @ORM\Column(type="integer", name="nb_lit_simple", nullable=true)
   */
  private $nb_lit_simple;

  /**
   * @var int
   *
   * @ORM\Column(type="integer", name="nb_lit_double", nullable=true)
   */
  private $nb_lit_double;

  /**
   * @var int
   *
   * @ORM\Column(type="integer", name="nb_canape_convertible", nullable=true)
   */
  private $nb_canape_convertible;

  /**
   * @var SiteTiers
   *
   * @ORM\ManyToOne(targetEntity="SiteTiers")
   * @ORM\JoinColumn(name="site_tiers_id", referencedColumnName="id", nullable=true)
   */
  private $site_tiers;

  /**
   * @var int
   *
   * @ORM\Column(type="integer", nullable=true)
   */
  private $notification_acompte;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL")
   */
  private $notification_payment;

  /**
   * @var Reservation
   *
   * @ORM\ManyToOne(targetEntity="Reservation", fetch="EAGER")
   * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
   */
  private $parent = null;

  /**
   * @var Facture[]
   *
   * @ORM\OneToMany(targetEntity="Facture", mappedBy="reservation")
   */
  private $factures;

  public function __toString()
  {
    return strval($this->getId());
  }

  /**
   * @ORM\PrePersist
   */
  public function setInitialValues()
  {
    // On donne une validité de 15 minutes à la réservation
    $this->date_validite = date("Y-m-d H:i:s", strtotime('+15 minutes'));

    $this->mode_paiement = 'CB';

    $this->created_at = date("Y-m-d H:i:s");
    $this->updated_at = date("Y-m-d H:i:s");
  }

  /**
   * @ORM\PreUpdate
   */
  public function onUpdate()
  {
    $this->updated_at = date("Y-m-d H:i:s");
  }

  public function generateRef()
  {
    // Generate reference with ID
    $id = str_pad(strval($this->getId()), 8, "0", STR_PAD_LEFT);
    $this->setReference($id);
  }

  public function generateReference4chiffres()
  {
    // Generate reference with ID
    $id = str_pad(strval($this->getId()), 4, "0", STR_PAD_LEFT);
    return $id;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get appartement_id
   *
   * @return integer
   */
  public function getAppartementId()
  {
    return $this->appartement_id;
  }

  /**
   * Set appartement
   *
   * @param Pat\CompteBundle\Entity\Appartement $appartement
   */
  public function setAppartement(\Pat\CompteBundle\Entity\Appartement $appartement)
  {
    $this->appartement = $appartement;
  }

  /**
   * Get appartement
   *
   * @return Pat\CompteBundle\Entity\Appartement
   */
  public function getAppartement()
  {
    return $this->appartement;
  }

  /**
   * Set utilisateur
   *
   * @param Pat\UtilisateurBundle\Entity\Utilisateur $utilisateur
   */
  public function setUtilisateur(\Pat\UtilisateurBundle\Entity\Utilisateur $utilisateur)
  {
    $this->utilisateur = $utilisateur;
  }

  /**
   * Get utilisateur
   *
   * @return Pat\UtilisateurBundle\Entity\Utilisateur
   */
  public function getUtilisateur()
  {
    return $this->utilisateur;
  }

  /**
   * Set date_debut
   *
   * @param string $dateDebut
   */
  public function setDateDebut($dateDebut)
  {
    $this->date_debut = $dateDebut;
  }

  /**
   * Get date_debut
   *
   * @return string
   */
  public function getDateDebut()
  {
    return $this->date_debut;
  }

  /**
   * Set date_fin
   *
   * @param string $dateFin
   */
  public function setDateFin($dateFin)
  {
    $this->date_fin = $dateFin;
  }

  /**
   * Get date_fin
   *
   * @return string
   */
  public function getDateFin()
  {
    return $this->date_fin;
  }

  /**
   * Set created_at
   *
   * @param string $createdAt
   */
  public function setCreatedAt($createdAt)
  {
    $this->created_at = $createdAt;
  }

  /**
   * Get created_at
   *
   * @return string
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * Set updated_at
   *
   * @param string $updatedAt
   */
  public function setUpdatedAt($updatedAt)
  {
    $this->updated_at = $updatedAt;
  }

  /**
   * Get updated_at
   *
   * @return string
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
   * Set reference
   *
   * @param string $reference
   */
  public function setReference($reference)
  {
    $this->reference = $reference;
  }

  /**
   * Get reference
   *
   * @return string
   */
  public function getReference()
  {
    return $this->reference;
  }

  /**
   * Set tarif
   *
   * @param string $tarif
   */
  public function setTarif($tarif)
  {
    $this->tarif = $tarif;
  }

  /**
   * Get tarif
   *
   * @return string
   */
  public function getTarif()
  {
    return $this->tarif;
  }

  /**
   * Set arrhes
   *
   * @param string $arrhes
   */
  public function setArrhes($arrhes)
  {
    $this->arrhes = $arrhes;
  }

  /**
   * Get arrhes
   *
   * @return string
   */
  public function getArrhes()
  {
    return $this->arrhes;
  }

  /**
   * Set calcul_values
   */
  public function setCalculValues($calcul_values)
  {
    $this->calcul_values = json_encode($calcul_values);
  }

  /**
   * Get calcul_values
   */
  public function getCalculValues()
  {
    return json_decode($this->calcul_values, true);
  }

  /**
   * Set nb_personne
   *
   * @param string $nbPersonne
   */
  public function setNbPersonne($nbPersonne)
  {
    $this->nb_personne = $nbPersonne;
  }

  /**
   * Get nb_personne
   *
   * @return string
   */
  public function getNbPersonne()
  {
    return $this->nb_personne;
  }

  /**
   * Set date_validite
   *
   * @param string $dateValidite
   */
  public function setDateValidite($dateValidite)
  {
    $this->date_validite = $dateValidite;
  }

  /**
   * Get date_validite
   *
   * @return string
   */
  public function getDateValidite()
  {
    return $this->date_validite;
  }

  /**
   * Set option_roses
   *
   * @param string $optionRoses
   * @return Reservation
   */
  public function setOptionRoses($optionRoses)
  {
    $this->option_roses = $optionRoses;

    return $this;
  }

  /**
   * Get option_roses
   *
   * @return string
   */
  public function getOptionRoses()
  {
    return $this->option_roses;
  }

  /**
   * Set option_champagne
   *
   * @param string $optionChampagne
   * @return Reservation
   */
  public function setOptionChampagne($optionChampagne)
  {
    $this->option_champagne = $optionChampagne;

    return $this;
  }

  /**
   * Get option_champagne
   *
   * @return string
   */
  public function getOptionChampagne()
  {
    return $this->option_champagne;
  }

  /**
   * Set option_lit_bebe
   *
   * @param boolean $optionLitBebe
   * @return Reservation
   */
  public function setOptionLitBebe($optionLitBebe)
  {
    $this->option_lit_bebe = $optionLitBebe;

    return $this;
  }

  /**
   * Get option_lit_bebe
   *
   * @return boolean
   */
  public function getOptionLitBebe()
  {
    return $this->option_lit_bebe;
  }

  /**
   * Set option_taxi
   *
   * @param boolean $optionTaxi
   * @return Reservation
   */
  public function setOptionTaxi($optionTaxi)
  {
    $this->option_taxi = $optionTaxi;

    return $this;
  }

  /**
   * Get option_taxi
   *
   * @return boolean
   */
  public function getOptionTaxi()
  {
    return $this->option_taxi;
  }

  /**
   * Set option_frigo
   *
   * @param boolean $optionFrigo
   * @return Reservation
   */
  public function setOptionFrigo($optionFrigo)
  {
    $this->option_frigo = $optionFrigo;

    return $this;
  }

  /**
   * Get option_frigo
   *
   * @return boolean
   */
  public function getOptionFrigo()
  {
    return $this->option_frigo;
  }

  /**
   * Set option_menage_supp
   *
   * @param string $optionMenageSupp
   * @return Reservation
   */
  public function setOptionMenageSupp($optionMenageSupp)
  {
    $this->option_menage_supp = $optionMenageSupp;

    return $this;
  }

  /**
   * Get option_menage_supp
   *
   * @return string
   */
  public function getOptionMenageSupp()
  {
    return $this->option_menage_supp;
  }

  /**
   * Set payment_result
   *
   * @param string $paymentResult
   * @return Reservation
   */
  public function setPaymentResult($paymentResult)
  {
    $this->payment_result = json_encode($paymentResult);

    return $this;
  }

  /**
   * Get payment_result
   *
   * @return string
   */
  public function getPaymentResult()
  {
    return json_decode($this->payment_result, true);
  }

  /**
   * Set payment_error
   *
   * @param string $paymentError
   * @return Reservation
   */
  public function setPaymentError($paymentError)
  {
    $this->payment_error = $paymentError;

    return $this;
  }

  /**
   * Get payment_error
   *
   * @return string
   */
  public function getPaymentError()
  {
    return $this->payment_error;
  }

  /**
   * Set mode_paiement
   *
   * @param string $modePaiement
   * @return Reservation
   */
  public function setModePaiement($modePaiement)
  {
    $this->mode_paiement = $modePaiement;

    return $this;
  }

  /**
   * Get mode_paiement
   *
   * @return string
   */
  public function getModePaiement()
  {
    return $this->mode_paiement;
  }

  /**
   * Set status
   *
   * @param integer $status
   * @return Reservation
   */
  public function setStatus($status)
  {
    $this->status = $status;

    return $this;
  }

  /**
   * Get status
   *
   * @return integer
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * Set depot
   *
   * @param string $depot
   * @return Reservation
   */
  public function setDepot($depot)
  {
    $this->depot = $depot;

    return $this;
  }

  /**
   * Get depot
   *
   * @return string
   */
  public function getDepot()
  {
    return $this->depot;
  }



  /**
   * Get Tarif Propriétaire Net
   */
  public function getTarifNet()
  {
    $values = $this->getCalculValues();
    return isset($values['tarif_net']) ? $values['tarif_net'] : 0;
  }

  /**
   * Set valid_prop
   *
   * @param boolean $validProp
   * @return Reservation
   */
  public function setValidProp($validProp)
  {
    $this->valid_prop = $validProp;

    return $this;
  }

  /**
   * Get valid_prop
   *
   * @return boolean
   */
  public function getValidProp()
  {
    return $this->valid_prop;
  }

  /**
   * Set updated_by_admin
   *
   * @param boolean $updatedByAdmin
   * @return Reservation
   */
  public function setUpdatedByAdmin($updatedByAdmin)
  {
    $this->updated_by_admin = $updatedByAdmin;

    return $this;
  }

  /**
   * Get updated_by_admin
   *
   * @return boolean
   */
  public function getUpdatedByAdmin()
  {
    return $this->updated_by_admin;
  }

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->payments = new ArrayCollection();
    $this->factures = new ArrayCollection();
  }

  /**
   * Add payments
   *
   * @param Payment $payments
   *
   * @return Reservation
   */
  public function addPayment(Payment $payments)
  {
    $this->payments[] = $payments;

    return $this;
  }

  /**
   * Remove payments
   *
   * @param Payment $payments
   */
  public function removePayment(Payment $payments)
  {
    $this->payments->removeElement($payments);
  }

  /**
   * Get payments
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getPayments()
  {
    return $this->payments;
  }

  /**
   * Add options
   *
   * @param \Pat\CompteBundle\Entity\Option $options
   * @return Reservation
   */
  public function addOption(\Pat\CompteBundle\Entity\Option $options)
  {
    $this->options[] = $options;

    return $this;
  }

  /**
   * Remove options
   *
   * @param \Pat\CompteBundle\Entity\Option $options
   */
  public function removeOption(\Pat\CompteBundle\Entity\Option $options)
  {
    $this->options->removeElement($options);
  }

  /**
   * Get options
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getOptions()
  {
    return $this->options;
  }

  /**
   * Get filename_en
   *
   * @return integer
   */
  public function getFilenameEn()
  {
    return $this->filename_en;
  }

  /**
   * Set promotion
   *
   * @param integer $filename_en
   * @return Reservation
   */
  public function setFilenameEn($filename_en)
  {
    $this->filename_en = $filename_en;

    return $this;
  }

  /**
   * Get filename_fr
   *
   * @return integer
   */
  public function getFilenameFr()
  {
    return $this->filename_fr;
  }

  /**
   * Set promotion
   *
   * @param integer $filename_fr
   * @return Reservation
   */
  public function setFilenameFr($filename_fr)
  {
    $this->filename_fr = $filename_fr;

    return $this;
  }

  /**
   * Set promotion
   *
   * @param integer $promotion
   * @return Reservation
   */
  public function setPromotion($promotion)
  {
    $this->promotion = $promotion;

    return $this;
  }

  /**
   * Get promotion
   *
   * @return integer
   */
  public function getPromotion()
  {
    return $this->promotion;
  }

  /**
   * Set option_supplementaires
   *
   * @param boolean $optionSupplementaires
   * @return Reservation
   */
  public function setOptionSupplementaires($optionSupplementaires)
  {
    $this->option_supplementaires = $optionSupplementaires;

    return $this;
  }

  /**
   * Get option_supplementaires
   *
   * @return boolean
   */
  public function getOptionSupplementaires()
  {
    return $this->option_supplementaires;
  }

  /**
   * @return string
   */
  public function getComment()
  {
    return $this->comment;
  }

  /**
   * @param string $comment
   *
   * @return Reservation
   */
  public function setComment($comment)
  {
    $this->comment = $comment;

    return $this;
  }

  /**
   * @return int
   */
  public function getNbAdulte()
  {
    return $this->nb_adulte;
  }

  /**
   * @param int $nb_adulte
   *
   * @return Reservation
   */
  public function setNbAdulte($nb_adulte)
  {
    $this->nb_adulte = $nb_adulte;

    return $this;
  }

  /**
   * @return int
   */
  public function getNbEnfant()
  {
    return $this->nb_enfant;
  }

  /**
   * @param int $nb_enfant
   *
   * @return Reservation
   */
  public function setNbEnfant($nb_enfant)
  {
    $this->nb_enfant = $nb_enfant;

    return $this;
  }

  /**
   * @return int
   */
  public function getNbLitSimple()
  {
    return $this->nb_lit_simple;
  }

  /**
   * @param int $nb_lit_simple
   *
   * @return Reservation
   */
  public function setNbLitSimple($nb_lit_simple)
  {
    $this->nb_lit_simple = $nb_lit_simple;

    return $this;
  }

  /**
   * @return int
   */
  public function getNbLitDouble()
  {
    return $this->nb_lit_double;
  }

  /**
   * @param int $nb_lit_double
   *
   * @return Reservation
   */
  public function setNbLitDouble($nb_lit_double)
  {
    $this->nb_lit_double = $nb_lit_double;

    return $this;
  }

  /**
   * @return int
   */
  public function getNbCanapeConvertible()
  {
    return $this->nb_canape_convertible;
  }

  /**
   * @param int $nb_canape_convertible
   *
   * @return Reservation
   */
  public function setNbCanapeConvertible($nb_canape_convertible)
  {
    $this->nb_canape_convertible = $nb_canape_convertible;

    return $this;
  }

  /**
   * @return SiteTiers
   */
  public function getSiteTiers()
  {
    return $this->site_tiers;
  }

  /**
   * @param SiteTiers $siteTiers
   *
   * @return Reservation
   */
  public function setSiteTiers($siteTiers)
  {
    $this->site_tiers = $siteTiers;

    return $this;
  }

  /**
   * @return int
   */
  public function isNotificationAcompte()
  {
    return $this->notification_acompte;
  }

  /**
   * @param int $notification_acompte
   *
   * @return Reservation
   */
  public function setNotificationAcompte($notification_acompte)
  {
    $this->notification_acompte = $notification_acompte;

    return $this;
  }

  /**
   * @return boolean
   */
  public function paymentIsNotified()
  {
    return $this->notification_payment;
  }

  /**
   * @param int $notification_payment
   *
   * @return Reservation
   */
  public function setNotificationPayment($notification_payment)
  {
    $this->notification_payment = $notification_payment;

    return $this;
  }

  /**
   * @return Reservation
   */
  public function getParent()
  {
    return $this->parent;
  }

  /**
   * @param Reservation $parent
   *
   * @return Reservation
   */
  public function setParent($parent)
  {
    $this->parent = $parent;

    return $this;
  }

  /**
   * @return ArrayCollection|Facture[]
   */
  public function getFactures()
  {
    return $this->factures;
  }

  /**
   * @param $factures
   *
   * @return Reservation
   */
  public function setFactures($factures)
  {
    $this->factures = $factures;


    return $this;
  }

  /**
   * @param Facture $facture
   *
   * @return Reservation
   */
  public function addFacture(Facture $facture)
  {
    if (false === $this->factures->contains($facture)) {
      $this->factures->add($facture);
    }

    return $this;
  }

  /**
   * @return Facture|null
   */
  public function getLastFacture($locale)
  {
    $criteria = new Criteria();
    $criteria
      ->andWhere(Criteria::expr()->isNull('avoirFileName'))
      ->andWhere(Criteria::expr()->eq('locale', $locale))
    ;

    $facture = $this->factures->matching($criteria)->first();

    return false === $facture ? null : $facture;
  }

  /**
   * Retourne le montant total.
   *
   * @return float
   */
  public function getTotalAmount()
  {
    $amout = $this->getTarif() + $this->getPromotion();
    foreach ($this->getOptions() as $option) {
      $amout += $option->getQuantity() * $option->getAmount();
    }
    return $amout;
  }

}
