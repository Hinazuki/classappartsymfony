<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Ville
 *
 * @ORM\Table(name="Ville")
 * @ORM\Entity(repositoryClass="Pat\CompteBundle\Repository\VilleRepository")
 */
class Ville
{

  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="nom", type="string", length=255, nullable=false)
   * @Assert\NotBlank
   */
  private $nom;

  /**
   * @var string
   *
   * @ORM\Column(name="code_postal", type="string", length=5, nullable=false)
   * @Assert\NotBlank
   */
  private $code_postal;

  /**
   * @var string
   *
   * @ORM\Column(name="pays", type="string", length=255, nullable=true)
   */
  private $pays;

  /**
   * @var float
   *
   * @ORM\Column(name="latitude", type="float", nullable=true)
   */
  private $latitude;

  /**
   * @var float
   *
   * @ORM\Column(name="longitude", type="float", nullable=true)
   */
  private $longitude;

  /**
   * @var integer
   *
   * @ORM\Column(name="nombre_habitants", type="integer", nullable=true)
   */
  private $nombreHabitants;

  /**
   * @ORM\Column(columnDefinition="varchar(1000) NULL")
   * @Assert\Length(
   *      max = "1000",
   *      maxMessage = "erreur.description_ville.maxlength"
   * )
   */
  private $description;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $quartier;

  /**
   *
   * @ORM\ManyToOne(targetEntity="Pat\CompteBundle\Entity\Departement", inversedBy="villes")
   * @ORM\JoinColumn(name="id_departement", referencedColumnName="id_departement")
   */
  private $departement;

  public function __toString()
  {
    return $this->nom." ".$this->code_postal." ".$this->pays;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set nom
   *
   * @param string $nom
   * @return Ville
   */
  public function setNom($nom)
  {
    $this->nom = $nom;

    return $this;
  }

  /**
   * Get nom
   *
   * @return string
   */
  public function getNom()
  {
    return $this->nom;
  }

  /**
   * Set codePostal
   *
   * @param string $codePostal
   * @return Ville
   */
  public function setCodePostal($codePostal)
  {
    $this->code_postal = $codePostal;

    return $this;
  }

  /**
   * Get codePostal
   *
   * @return string
   */
  public function getCodePostal()
  {
    return $this->code_postal;
  }

  /**
   * Set pays
   *
   * @param string $pays
   * @return Ville
   */
  public function setPays($pays)
  {
    $this->pays = $pays;

    return $this;
  }

  /**
   * Get pays
   *
   * @return string
   */
  public function getPays()
  {
    return $this->pays;
  }

  /**
   * Set latitude
   *
   * @param float $latitude
   * @return Ville
   */
  public function setLatitude($latitude)
  {
    $this->latitude = $latitude;

    return $this;
  }

  /**
   * Get latitude
   *
   * @return float
   */
  public function getLatitude()
  {
    return $this->latitude;
  }

  /**
   * Set longitude
   *
   * @param float $longitude
   * @return Ville
   */
  public function setLongitude($longitude)
  {
    $this->longitude = $longitude;

    return $this;
  }

  /**
   * Get longitude
   *
   * @return float
   */
  public function getLongitude()
  {
    return $this->longitude;
  }

  /**
   * Set nombreHabitants
   *
   * @param integer $nombreHabitants
   * @return Ville
   */
  public function setNombreHabitants($nombreHabitants)
  {
    $this->nombreHabitants = $nombreHabitants;

    return $this;
  }

  /**
   * Get nombreHabitants
   *
   * @return integer
   */
  public function getNombreHabitants()
  {
    return $this->nombreHabitants;
  }

  /**
   * Set departement
   *
   * @param \Pat\CompteBundle\Entity\Departement $departement
   * @return Ville
   */
  public function setDepartement(\Pat\CompteBundle\Entity\Departement $departement = null)
  {
    $this->departement = $departement;

    return $this;
  }

  /**
   * Get departement
   *
   * @return \Pat\CompteBundle\Entity\Departement
   */
  public function getDepartement()
  {
    return $this->departement;
  }

  /**
   * Set description
   *
   * @param string $description
   * @return Ville
   */
  public function setDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
   * Get description
   *
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Set quartier
   *
   * @param boolean $quartier
   * @return Ville
   */
  public function setQuartier($quartier)
  {
    $this->quartier = $quartier;

    return $this;
  }

  /**
   * Get quartier
   *
   * @return boolean
   */
  public function getQuartier()
  {
    return $this->quartier;
  }

}
