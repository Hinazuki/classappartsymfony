<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class EquipementCategorie
{

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string",length=50)
   */
  private $intitule_categorie;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set intitule_categorie
   *
   * @param string $intituleCategorie
   */
  public function setIntituleCategorie($intituleCategorie)
  {
    $this->intitule_categorie = $intituleCategorie;
  }

  /**
   * Get intitule_categorie
   *
   * @return string
   */
  public function getIntituleCategorie()
  {
    return $this->intitule_categorie;
  }

}
