<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Departement
 *
 * @ORM\Table(name="documents")
 * @ORM\Entity(repositoryClass="\Pat\CompteBundle\Repository\DocumentsRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class Documents
{

  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var datetime
   *
   * @ORM\Column(name="date_update", type="datetime", nullable=true)
   */
  private $dateUpdate;

  /**
   * @Assert\File(
   *     maxSize="5M",
   *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg", "application/pdf"},
   *     mimeTypesMessage="Le fichier joint doit être une image (png/jpg/jpeg) ou un fichier pdf.",
   *     maxSizeMessage="Le fichier joint doit faire moins de 5Mo",
   *     notFoundMessage="Le fichier n'a pas été trouvé."  ,
   *     groups={"creation", "edition"}
   * )
   * @Vich\UploadableField(mapping="document_file", fileNameProperty="filename")
   * @Assert\NotNull(groups={"creation"}, message="Veuillez sélectionner un fichier")
   * @var File $document
   */
  protected $file;

  /**
   * @var string
   *
   * @ORM\Column(name="filename", type="string", length=255, nullable=true)
   */
  protected $filename;

  /**
   * @var string
   *
   * @ORM\Column(name="document_name", type="string", length=255, nullable=false)
   * @Assert\NotBlank(groups={"creation", "edition"})
   */
  private $documentName;

  /**
   * @var string
   *
   * @ORM\Column(name="file_size", type="string", length=255, nullable=true)
   */
  private $fileSize;

  /**
   * @var boolean
   *
   * @ORM\Column(name="visible", type="boolean", nullable = true)
   */
  private $visible;

  /**
   * @var \Utilisateur
   *
   * @ORM\ManyToOne(targetEntity="Pat\UtilisateurBundle\Entity\Utilisateur")
   * @Assert\NotBlank(groups={"creation", "edition"})
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
   * })
   */
  protected $user;

  /**
   * @var \Appartement
   *
   * @ORM\ManyToOne(targetEntity="Appartement")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="appartement_id", referencedColumnName="id", nullable=true)
   * })
   */
  protected $appartement;

  public function __toString()
  {
    return $this->documentName;
  }

  /**
   * @ORM\PrePersist
   */
  public function setInitialValues()
  {
    $this->dateUpdate = new \DateTime();
  }

  /**
   * @ORM\PreUpdate
   */
  public function setUpdateValues()
  {
    $this->dateUpdate = new \DateTime();
  }

  /**
   * Get document
   *
   * @return $document
   */
  public function getFile()
  {
    return $this->file;
  }

  /**
   * Set document
   *
   * @param \Pat\CompteBundle\Entity\Upload $file
   * @return document
   */
  public function setFile($file)
  {
    $this->file = $file;

    if ($file instanceof File) {
      $this->setDateUpdate(new \DateTime());
    }
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set dateUpdate
   *
   * @param \DateTime $dateUpdate
   * @return Documents
   */
  public function setDateUpdate($dateUpdate)
  {
    $this->dateUpdate = $dateUpdate;

    return $this;
  }

  /**
   * Get dateUpdate
   *
   * @return \DateTime
   */
  public function getDateUpdate()
  {
    return $this->dateUpdate;
  }

  /**
   * Set filename
   *
   * @param string $filename
   * @return Documents
   */
  public function setFilename($filename)
  {
    $this->filename = $filename;

    return $this;
  }

  /**
   * Get filename
   *
   * @return string
   */
  public function getFilename()
  {
    return $this->filename;
  }

  /**
   * Set documentName
   *
   * @param string $documentName
   * @return Documents
   */
  public function setDocumentName($documentName)
  {
    $this->documentName = $documentName;

    return $this;
  }

  /**
   * Get documentName
   *
   * @return string
   */
  public function getDocumentName()
  {
    return $this->documentName;
  }

  /**
   * Set fileSize
   *
   * @param string $fileSize
   * @return Documents
   */
  public function setFileSize($fileSize)
  {
    $this->fileSize = $fileSize;

    return $this;
  }

  /**
   * Get fileSize
   *
   * @return string
   */
  public function getFileSize()
  {
    return $this->fileSize;
  }

  /**
   * Set visible
   *
   * @param boolean $visible
   * @return Documents
   */
  public function setVisible($visible)
  {
    $this->visible = $visible;

    return $this;
  }

  /**
   * Get visible
   *
   * @return boolean
   */
  public function getVisible()
  {
    return $this->visible;
  }

  /**
   * Set user
   *
   * @param \Pat\UtilisateurBundle\Entity\Utilisateur $user
   * @return Documents
   */
  public function setUser(\Pat\UtilisateurBundle\Entity\Utilisateur $user)
  {
    $this->user = $user;

    return $this;
  }

  /**
   * Get user
   *
   * @return \Pat\UtilisateurBundle\Entity\Utilisateur
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * Set appartement
   *
   * @param \Pat\CompteBundle\Entity\Appartement $appartement
   * @return Documents
   */
  public function setAppartement(\Pat\CompteBundle\Entity\Appartement $appartement = null)
  {
    $this->appartement = $appartement;

    return $this;
  }

  /**
   * Get appartement
   *
   * @return \Pat\CompteBundle\Entity\Appartement
   */
  public function getAppartement()
  {
    return $this->appartement;
  }

}
