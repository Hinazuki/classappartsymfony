<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints AS Assert;

/**
 * Option
 *
 * @ORM\Table(name="options")
 * @ORM\Entity(repositoryClass="Pat\CompteBundle\Repository\OptionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Option
{

  /**
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Pat\CompteBundle\Entity\Reservation", inversedBy="options")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="reservation_id", referencedColumnName="id", nullable=true)
   * })
   */
  private $reservation;

  /**
   * @var integer
   *
   * @ORM\Column(name="quantity", type="integer", nullable=false)
   * @Assert\NotBlank
   */
  private $quantity;

  /**
   * @var float
   *
   * @ORM\Column(name="amount", type="float", nullable=true)
   */
  private $amount;

  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", nullable=true)
   * @Assert\NotBlank
   */
  private $name;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="created_at", type="datetime", nullable=true)
   */
  private $createdAt;

  /**
   * @ORM\PrePersist
   */
  public function setInitialValues()
  {
    $this->createdAt = new \DateTime(date("Y-m-d H:i:s"));
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set amount
   *
   * @param float $amount
   * @return Option
   */
  public function setAmount($amount)
  {
    $this->amount = $amount;

    return $this;
  }

  /**
   * Get amount
   *
   * @return float
   */
  public function getAmount()
  {
    return $this->amount;
  }

  /**
   * Set name
   *
   * @param string $name
   * @return Option
   */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * Get name
   *
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set createdAt
   *
   * @param \DateTime $createdAt
   * @return Option
   */
  public function setCreatedAt($createdAt)
  {
    $this->createdAt = $createdAt;

    return $this;
  }

  /**
   * Get createdAt
   *
   * @return \DateTime
   */
  public function getCreatedAt()
  {
    return $this->createdAt;
  }

  /**
   * Set reservation
   *
   * @param \Pat\CompteBundle\Entity\Reservation $reservation
   * @return Option
   */
  public function setReservation(\Pat\CompteBundle\Entity\Reservation $reservation = null)
  {
    $this->reservation = $reservation;

    return $this;
  }

  /**
   * Get reservation
   *
   * @return \Pat\CompteBundle\Entity\Reservation
   */
  public function getReservation()
  {
    return $this->reservation;
  }

  /**
   * Set quantity
   *
   * @param integer $quantity
   * @return Option
   */
  public function setQuantity($quantity)
  {
    $this->quantity = $quantity;

    return $this;
  }

  /**
   * Get quantity
   *
   * @return integer
   */
  public function getQuantity()
  {
    return $this->quantity;
  }

}
