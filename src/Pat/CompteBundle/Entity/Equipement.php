<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Pat\CompteBundle\Repository\EquipementRepository")
 */
class Equipement
{

  const LIT_SIMPLE_ID = 31;
  const LIT_DOUBLE_ID = 32;
  const CANAPE_CONVERTIBLE_ID = 33;
  const LIT_SIMPLE_DOUBLE = 75;

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string",length=50)
   * @Assert\NotBlank(message = "Veuillez saisir l'intitulé de cet équipement")
   */
  private $intitule;

  /**
   * @ORM\ManyToOne(targetEntity="Pat\CompteBundle\Entity\EquipementCategorie")
   * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
   *
   * Problem with attribute onUpdate="CASCADE" which is not supported anymore on Doctrine 2.2 : \JoinColumn(name="equipement_categorie_id", referencedColumnName="id", onUpdate="CASCADE", onDelete="CASCADE")
   */
  private $equipement_categorie;

  /**
   * @var boolean
   *
   * @ORM\Column(type="boolean", options={"default"=false})
   */
  private $multi;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set intitule
   *
   * @param string $intitule
   */
  public function setIntitule($intitule)
  {
    $this->intitule = $intitule;
  }

  /**
   * Get intitule
   *
   * @return string
   */
  public function getIntitule()
  {
    return $this->intitule;
  }

  /**
   * Set equipement_categorie
   *
   * @param Pat\CompteBundle\Entity\EquipementCategorie $equipementCategorie
   */
  public function setEquipementCategorie(\Pat\CompteBundle\Entity\EquipementCategorie $equipementCategorie)
  {
    $this->equipement_categorie = $equipementCategorie;
  }

  /**
   * Get equipement_categorie
   *
   * @return Pat\CompteBundle\Entity\EquipementCategorie
   */
  public function getEquipementCategorie()
  {
    return $this->equipement_categorie;
  }

  /**
   * @return bool
   */
  public function isMulti()
  {
    return $this->multi;
  }

  /**
   * @param bool $multi
   *
   * @return Equipement
   */
  public function setMulti($multi)
  {
    $this->multi = $multi;

    return $this;
  }

}
