<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Piece
{

  /**
   * @ORM\ManyToOne(targetEntity="Appartement", inversedBy="pieces")
   *  @ORM\JoinColumn(referencedColumnName="id")
   */
  private $appartement;

  /**
   * @ORM\ManyToMany(targetEntity="Equipement", cascade={"persist"})
   * @ORM\JoinTable(name="piece_equipement",
   *      joinColumns={@ORM\JoinColumn(name="piece_id", referencedColumnName="id", onDelete="CASCADE")},
   *      inverseJoinColumns={@ORM\JoinColumn(name="equipement_id", referencedColumnName="id", onDelete="CASCADE")}
   *      )
   * @Assert\NotBlank()
   */
  private $equipement;

  /**
   * @var PieceEquipement[]
   *
   * @ORM\OneToMany(targetEntity="PieceEquipement", mappedBy="piece", orphanRemoval=true)
   */
  private $multiEquipements;

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Pat\CompteBundle\Entity\TypePiece")
   */
  private $typepiece;

  /**
   * @ORM\Column(columnDefinition="varchar(256) NULL")
   */
  private $commentaire;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set intitule
   *
   * @param string $intitule
   */
  public function setIntitule($intitule)
  {
    $this->intitule = $intitule;
  }

  /**
   * Get intitule
   *
   * @return string
   */
  public function getIntitule()
  {
    return $this->intitule;
  }

  /**
   * Set intitule
   *
   * @param string $intitule
   */
  public function setEquipement($equipement)
  {
    $this->equipement = $equipement;
  }

  /**
   * Set type
   *
   * @param string $type
   */
  public function setType($type)
  {
    $this->type = $type;
  }

  /**
   * Get type
   *
   * @return string
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Set appartement
   *
   * @param Pat\CompteBundle\Entity\Appartement $appartement
   */
  public function setAppartement(\Pat\CompteBundle\Entity\Appartement $appartement)
  {
    $this->appartement = $appartement;
  }

  /**
   * Get appartement
   *
   * @return Pat\CompteBundle\Entity\Appartement
   */
  public function getAppartement()
  {
    return $this->appartement;
  }

  /**
   * Add equipement
   *
   * @param Pat\CompteBundle\Entity\Equipement $equipement
   */
  public function addEquipement(\Pat\CompteBundle\Entity\Equipement $equipement)
  {
    $this->equipement[] = $equipement;
  }

  /**
   * Get equipement
   *
   * @return Doctrine\Common\Collections\Collection
   */
  public function getEquipement()
  {
    return $this->equipement;
  }

  /**
   * Set typepiece
   *
   * @param Pat\CompteBundle\Entity\TypePiece $typepiece
   */
  public function setTypepiece(\Pat\CompteBundle\Entity\TypePiece $typepiece)
  {
    $this->typepiece = $typepiece;
  }

  /**
   * Get typepiece
   *
   * @return Pat\CompteBundle\Entity\TypePiece
   */
  public function getTypepiece()
  {
    return $this->typepiece;
  }

  public function __construct()
  {
    $this->equipement = new ArrayCollection();
    $this->multiEquipements = new ArrayCollection();
  }

  /**
   * Set commentaire
   *
   * @param string $commentaire
   */
  public function setCommentaire($commentaire)
  {
    $this->commentaire = $commentaire;
  }

  /**
   * Get commentaire
   *
   * @return string
   */
  public function getCommentaire()
  {
    return $this->commentaire;
  }

  /**
   * Remove equipement
   *
   * @param \Pat\CompteBundle\Entity\Equipement $equipement
   */
  public function removeEquipement(\Pat\CompteBundle\Entity\Equipement $equipement)
  {
    $this->equipement->removeElement($equipement);
  }

  /**
   * @return PieceEquipement[]
   */
  public function getMultiEquipements()
  {
    return $this->multiEquipements;
  }

  public function removeAllMultiEquipements()
  {
    $multiEquipements = $this->multiEquipements;

    foreach ($multiEquipements as $equipement) {
      $this->removeMultiEquipement($equipement);
    }

    $this->multiEquipements = new ArrayCollection();
  }

  /**
   * @param $multiEquipement
   *
   * @return $this
   */
  public function addMultiEquipement($multiEquipement)
  {
    if (false === $this->multiEquipements->contains($multiEquipement)) {
      $this->multiEquipements->add($multiEquipement);
    }

    return $this;
  }

  /**
   * @param $multiEquipement
   *
   * @return $this
   */
  public function removeMultiEquipement($multiEquipement)
  {
    $this->multiEquipements->removeElement($multiEquipement);

    return $this;
  }

}
