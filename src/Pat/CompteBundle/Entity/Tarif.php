<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Pat\CompteBundle\Repository\TarifRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Tarif
{

  const LOYER_LIBRE = 1;
  const LOYER_AUTO = 2;

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\OneToOne(targetEntity="Appartement", inversedBy="tarif")
   * @ORM\JoinColumn(name="appartement", referencedColumnName="id", onDelete="CASCADE")
   */
  private $appartement;

  /**
   * @ORM\Column(columnDefinition="varchar(8) DEFAULT 'STANDARD'")
   * @Assert\Choice(choices = {"STANDARD", "PREMIUM"}, message = "Type de contrat non valide")
   */
  private $type_contrat;

  /**
   * @ORM\Column(type="integer")
   * @Assert\Type(type="integer", message="The value {{ value }} is not a valid {{ type }}.")
   * @Assert\Range(
   *      min = 0,
   *      max = 100,
   *      minMessage = "Vous devez entrer une valeur comprise entre 0 et 100",
   *      maxMessage = "Vous devez entrer une valeur comprise entre 0 et 100"
   * )
   */
  private $acompte;

  /**
   *
   * @ORM\Column(type="integer")
   * @Assert\Type(type="integer", message="The value {{ value }} is not a valid {{ type }}.")
   * @Assert\Range(
   *      min = 0,
   *      max = 100,
   *      minMessage = "Vous devez entrer une valeur comprise entre 0 et 100",
   *      maxMessage = "Vous devez entrer une valeur comprise entre 0 et 100"
   * )
   */
  private $depot; // Dépot de garantie

  /**
   * @ORM\Column(columnDefinition="decimal(10,2) NOT NULL")
   * @Assert\NotBlank()
   */
  private $depot_min;

  /**
   * @ORM\Column(columnDefinition="decimal(10,2) NOT NULL")
   * @Assert\NotBlank()
   */
  private $loyer_nuit;

  /**
   * @ORM\Column(columnDefinition="decimal(10,2) NOT NULL")
   * @Assert\NotBlank()
   */
  private $loyer_semaine1;

  /**
   * @ORM\Column(columnDefinition="decimal(10,2) NOT NULL")
   * @Assert\NotBlank()
   */
  private $loyer_mois;

  /**
   * @ORM\Column(columnDefinition="decimal(10,2) NOT NULL")
   * @Assert\NotBlank()
   */
  private $loyer_nuit_2;

  /**
   * @ORM\Column(columnDefinition="decimal(10,2) NOT NULL")
   * @Assert\NotBlank()
   */
  private $loyer_semaine1_2;

  /**
   * @ORM\Column(columnDefinition="decimal(10,2) NOT NULL")
   * @Assert\NotBlank()
   */
  private $loyer_mois_2;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $is_tout_compris;

  /**
   * @var integer
   *
   * @ORM\Column(type="integer", options={"default":1})
   */
  private $loyer_actif;

  /**
   * @ORM\Column(type="datetime")
   */
  private $created_at;

  /**
   * @ORM\Column(type="datetime")
   */
  private $updated_at;

  public function __toString()
  {
    return "".$this->loyer;
  }

  /**
   * @ORM\PrePersist
   */
  public function setInitialValues()
  {
    $this->created_at = new \DateTime("now");
    $this->updated_at = new \DateTime("now");
  }

  /**
   * @ORM\PreUpdate
   */
  public function onUpdate()
  {
    $this->updated_at = new \DateTime("now");
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set loyer_nuit
   *
   * @param string $loyerNuit
   * @return Tarif
   */
  public function setLoyerNuit($loyerNuit)
  {
    $this->loyer_nuit = $loyerNuit;

    return $this;
  }

  /**
   * Get loyer_nuit
   *
   * @return string
   */
  public function getLoyerNuit()
  {
    return $this->loyer_nuit;
  }

  /**
   * Set loyer_semaine1
   *
   * @param string $loyerSemaine1
   * @return Tarif
   */
  public function setLoyerSemaine1($loyerSemaine1)
  {
    $this->loyer_semaine1 = $loyerSemaine1;

    return $this;
  }

  /**
   * Get loyer_semaine1
   *
   * @return string
   */
  public function getLoyerSemaine1()
  {
    return $this->loyer_semaine1;
  }

  /**
   * Set loyer_mois
   *
   * @param string $loyerMois
   * @return Tarif
   */
  public function setLoyerMois($loyerMois)
  {
    $this->loyer_mois = $loyerMois;

    return $this;
  }

  /**
   * Get loyer_mois
   *
   * @return string
   */
  public function getLoyerMois()
  {
    return $this->loyer_mois;
  }

  /**
   * Set is_tout_compris
   *
   * @param boolean $isToutCompris
   * @return Tarif
   */
  public function setIsToutCompris($isToutCompris)
  {
    $this->is_tout_compris = $isToutCompris;

    return $this;
  }

  /**
   * Get is_tout_compris
   *
   * @return boolean
   */
  public function getIsToutCompris()
  {
    return $this->is_tout_compris;
  }

  /**
   * Set created_at
   *
   * @param \DateTime $createdAt
   * @return Tarif
   */
  public function setCreatedAt($createdAt)
  {
    $this->created_at = $createdAt;

    return $this;
  }

  /**
   * Get created_at
   *
   * @return \DateTime
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * Set updated_at
   *
   * @param \DateTime $updatedAt
   * @return Tarif
   */
  public function setUpdatedAt($updatedAt)
  {
    $this->updated_at = $updatedAt;

    return $this;
  }

  /**
   * Get updated_at
   *
   * @return \DateTime
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
   * Set appartement
   *
   * @param \Pat\CompteBundle\Entity\Appartement $appartement
   * @return Tarif
   */
  public function setAppartement(\Pat\CompteBundle\Entity\Appartement $appartement = null)
  {
    $this->appartement = $appartement;

    return $this;
  }

  /**
   * Get appartement
   *
   * @return \Pat\CompteBundle\Entity\Appartement
   */
  public function getAppartement()
  {
    return $this->appartement;
  }

  /**
   * Set loyer_nuit_2
   *
   * @param string $loyerNuit2
   * @return Tarif
   */
  public function setLoyerNuit2($loyerNuit2)
  {
    $this->loyer_nuit_2 = $loyerNuit2;

    return $this;
  }

  /**
   * Get loyer_nuit_2
   *
   * @return string
   */
  public function getLoyerNuit2()
  {
    return $this->loyer_nuit_2;
  }

  /**
   * Set loyer_semaine1_2
   *
   * @param string $loyerSemaine12
   * @return Tarif
   */
  public function setLoyerSemaine12($loyerSemaine12)
  {
    $this->loyer_semaine1_2 = $loyerSemaine12;

    return $this;
  }

  /**
   * Get loyer_semaine1_2
   *
   * @return string
   */
  public function getLoyerSemaine12()
  {
    return $this->loyer_semaine1_2;
  }

  /**
   * Set loyer_mois_2
   *
   * @param string $loyerMois2
   * @return Tarif
   */
  public function setLoyerMois2($loyerMois2)
  {
    $this->loyer_mois_2 = $loyerMois2;

    return $this;
  }

  /**
   * Get loyer_mois_2
   *
   * @return string
   */
  public function getLoyerMois2()
  {
    return $this->loyer_mois_2;
  }

  /**
   * Set type_contrat
   *
   * @param string $typeContrat
   * @return Tarif
   */
  public function setTypeContrat($typeContrat)
  {
    $this->type_contrat = $typeContrat;

    return $this;
  }

  /**
   * Get type_contrat
   *
   * @return string
   */
  public function getTypeContrat()
  {
    return $this->type_contrat;
  }

  /**
   * Set acompte
   *
   * @param integer $acompte
   * @return Tarif
   */
  public function setAcompte($acompte)
  {
    $this->acompte = $acompte;

    return $this;
  }

  /**
   * Get acompte
   *
   * @return integer
   */
  public function getAcompte()
  {
    return $this->acompte;
  }

  /**
   * Set depot
   *
   * @param integer $depot
   * @return Tarif
   */
  public function setDepot($depot)
  {
    $this->depot = $depot;

    return $this;
  }

  /**
   * Get depot
   *
   * @return integer
   */
  public function getDepot()
  {
    return $this->depot;
  }

  /**
   * Set depot_min
   *
   * @param string $depotMin
   * @return Tarif
   */
  public function setDepotMin($depotMin)
  {
    $this->depot_min = $depotMin;

    return $this;
  }

  /**
   * Get depot_min
   *
   * @return string
   */
  public function getDepotMin()
  {
    return $this->depot_min;
  }

  /**
   * @return int
   */
  public function getLoyerActif()
  {
    return $this->loyer_actif;
  }

  /**
   * @param int $loyer_actif
   *
   * @return Tarif
   */
  public function setLoyerActif($loyer_actif)
  {
    $this->loyer_actif = $loyer_actif;

    return $this;
  }

}
