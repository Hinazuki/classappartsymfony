<?php

namespace Pat\CompteBundle\Entity;

//use Pat\UtilisateurBundle\Entity\Utilisateur;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Pat\CompteBundle\Repository\PromotionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Promotion
{

  const TYPE_ACTIVABLE = 1;
  const TYPE_PERIODE = 2;

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @var Appartement
   *
   * @ORM\ManyToOne(targetEntity="Pat\CompteBundle\Entity\Appartement", inversedBy="promotions")
   */
  private $appartement;

  /**
   * @var int
   *
   * @ORM\Column(type="integer")
   */
  private $type;

  /**
   * @var float
   *
   * @ORM\Column(type="float")
   *
   * @Assert\LessThanOrEqual(
   *     value = 100,
   *     message="Le pourcentage doit être inférieurs à 100"
   * )
   *
   * @Assert\GreaterThanOrEqual(
   *     value="-100",
   *     message="Le pourcentage doit être supérieur à -100"
   * )
   */
  private $percentage;

  /**
   * @var int
   *
   * @ORM\Column(type="integer", nullable=true)
   */
  private $nbDaysBefore;

  /**
   * @var \DateTime
   *
   * @ORM\Column(type="datetime", nullable=true)
   */
  private $begin;

  /**
   * @var \DateTime
   *
   * @ORM\Column(type="datetime", nullable=true)
   */
  private $end;

  /**
   * @var boolean
   *
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $activated;

  /**
   * @ORM\Column(type="datetime")
   */
  private $createdAt;

  /**
   * @ORM\Column(type="datetime")
   */
  private $updatedAt;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @ORM\PrePersist
   */
  public function setInitialValues()
  {
    $this->createdAt = new \DateTime('now');
    $this->updatedAt = new \DateTime('now');
  }

  /**
   * @ORM\PreUpdate
   */
  public function onUpdate()
  {
    $this->updatedAt = new \DateTime('now');
  }

  /**
   * Set createdAt
   *
   * @param datetime $createdAt
   */
  public function setCreatedAt($createdAt)
  {
    $this->createdAt = $createdAt;
  }

  /**
   * Get created_at
   *
   * @return datetime
   */
  public function getCreatedAt()
  {
    return $this->createdAt;
  }

  /**
   * Set updatedAt
   *
   * @param datetime $updatedAt
   */
  public function setUpdatedAt($updatedAt)
  {
    $this->updatedAt = $updatedAt;
  }

  /**
   * Get updatedAt
   *
   * @return datetime
   */
  public function getUpdatedAt()
  {
    return $this->updatedAt;
  }

  /**
   * @return int
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * @param int $type
   *
   * @return Promotion
   */
  public function setType($type)
  {
    $this->type = $type;

    $this->updateWithType();

    return $this;
  }

  public function updateWithType()
  {
    if ($this::TYPE_ACTIVABLE === $this->type) {
      $this->begin = null;
      $this->end = null;
    }

    if ($this::TYPE_ACTIVABLE === $this->type && null === $this->type) {
      $this->activated = false;
    }
    elseif ($this::TYPE_PERIODE === $this->type) {
      $this->activated = null;
      $this->nbDaysBefore = null;
    }
  }

  /**
   * @return float
   */
  public function getPercentage()
  {
    return $this->percentage;
  }

  /**
   * @param float $percentage
   *
   * @return Promotion
   */
  public function setPercentage($percentage)
  {
    $this->percentage = $percentage;

    return $this;
  }

  /**
   * @return int
   */
  public function getNbDaysBefore()
  {
    return $this->nbDaysBefore;
  }

  /**
   * @param int $nbDaysBefore
   *
   * @return Promotion
   */
  public function setNbDaysBefore($nbDaysBefore)
  {
    $this->nbDaysBefore = $nbDaysBefore;

    return $this;
  }

  /**
   * @return \DateTime
   */
  public function getBegin()
  {
    return $this->begin;
  }

  /**
   * @param \DateTime $begin
   *
   * @return Promotion
   */
  public function setBegin($begin)
  {
    $this->begin = $begin;

    return $this;
  }

  /**
   * @return \DateTime
   */
  public function getEnd()
  {
    return $this->end;
  }

  /**
   * @param \DateTime $end
   *
   * @return Promotion
   */
  public function setEnd($end)
  {
    $this->end = $end;

    return $this;
  }

  /**
   * @return bool
   */
  public function isActivated()
  {
    return $this->activated;
  }

  /**
   * @param bool $activated
   *
   * @return Promotion
   */
  public function setActivated($activated)
  {
    $this->activated = $activated;

    return $this;
  }

  /**
   * @return Appartement
   */
  public function getAppartement()
  {
    return $this->appartement;
  }

  /**
   * @param Appartement $appartement
   *
   * @return Promotion
   */
  public function setAppartement($appartement)
  {
    $this->appartement = $appartement;

    return $this;
  }

  public function getActiveLimitDate()
  {
    if ($this::TYPE_ACTIVABLE !== $this->type) {
      return null;
    }

    $date = new \DateTime('now');

    $date->modify('+ '.$this->getNbDaysBefore().' days');

    return $date;
  }

}
