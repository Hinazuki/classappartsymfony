<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Pat\CompteBundle\Repository\AppartementRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Appartement
{

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Pat\UtilisateurBundle\Entity\Utilisateur")
   */
  private $utilisateur;

  //##################################################
  // DESCRIPTION DE L APPARTEMENT

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL")
   */
  private $reference;

  /**
   * @ORM\Column(columnDefinition="varchar(100) NULL")
   * @Assert\NotBlank(message = "Veuillez spécifier le type du bien")
   */
  private $type;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   * @Assert\NotBlank
   */
  private $nb_pieces;

  /**
   * @ORM\Column(columnDefinition="text NOT NULL")
   */
  private $url_fr;

  /**
   * @ORM\Column(columnDefinition="text NOT NULL")
   */
  private $url_en;

  /**
   * @ORM\Column(columnDefinition="text NOT NULL")
   */
  private $url_de;

  /**
   * @ORM\Column(columnDefinition="varchar(255) NULL")
   * @Assert\NotBlank(message = "Veuillez saisir la description courte du bien")
   */
  private $description_courte_fr;

  /**
   * @ORM\Column(columnDefinition="varchar(255) NULL")
   */
  private $description_courte_en;

  /**
   * @ORM\Column(columnDefinition="varchar(255) NULL")
   */
  private $description_courte_de;

  /**
   * @ORM\Column(columnDefinition="text NULL")
   * @Assert\NotBlank(message = "Veuillez saisir la description longue du bien")
   */
  private $description_fr;

  /**
   * @ORM\Column(columnDefinition="text NULL")
   */
  private $description_en;

  /**
   * @ORM\Column(columnDefinition="text NULL")
   */
  private $description_de;

  /**
   * @ORM\Column(columnDefinition="varchar(255) NULL")
   */
  private $nom_residence;

  /**
   * @ORM\Column(columnDefinition="varchar(255) NOT NULL")
   * @Assert\NotBlank(message = "Veuillez saisir l'adresse du bien")
   * @Assert\Length(
   *      min = "3",
   *      minMessage = "erreur.adresse_appart.minlength"
   * )
   */
  private $adresse;

  /**
   * @ORM\Column(columnDefinition="varchar(255) NULL")
   */
  private $adresse2;

  /**
   * @ORM\ManyToOne(targetEntity="Ville")
   * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
   */
  private $ville;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $is_meuble;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $is_duplex;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $distance_aeroport;  // en KM

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $distance_autoroute; // en KM

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $distance_voie_rapide; // en KM

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $distance_commerce; // en KM

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $acces_marche; // en Minutes

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $acces_gare; // en Minutes
  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $acces_bus;  // en Minutes

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $acces_metro; // en Minutes

  /**
   * @ORM\Column(columnDefinition="varchar(30) NULL")
   */
  private $arret_tram;

  /**
   * @ORM\Column(columnDefinition="varchar(30) NULL")
   */
  private $arret_tram2;

  /**
   * @ORM\Column(columnDefinition="varchar(30) NULL")
   */
  private $arret_tram3;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $acces_tram; // en Minutes

  /**
   * @ORM\Column(columnDefinition="varchar(20) NULL")
   */
  private $surface_sol;

  /**
   * @ORM\Column(columnDefinition="varchar(20) NULL")
   */
  private $surface_carrez;

  /**
   * @ORM\Column(columnDefinition="varchar(20) NULL")
   */
  private $surface_terrain;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $nb_personne;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL")
   */
  private $etage;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL")
   */
  private $nb_etage;

  /**
   * @ORM\Column(columnDefinition="varchar(20) NULL")
   */
  private $entree_immeuble; // Clef ou code

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $serrure;

  /**
   * @var int
   *
   * @ORM\Column(type="integer", name="nb_lit_simple", nullable=true)
   */
  private $nb_lit_simple;

  /**
   * @var int
   *
   * @ORM\Column(type="integer", name="nb_lit_double", nullable=true)
   */
  private $nb_lit_double;

  /**
   * @var int
   *
   * @ORM\Column(type="integer", name="nb_canape_convertible", nullable=true)
   */
  private $nb_canape_convertible;

  /**
   * @var int
   *
   * @ORM\Column(type="integer", name="nb_lit_simple_double", nullable=true)
   */
  private $nb_lit_simple_double;


  //##################################################
  // PRESTATIONS

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $escalier;

  /**
   * @ORM\Column(columnDefinition="varchar(20) NULL")
   */
  private $type_escalier; // gauche ou droite

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $ascenseur;

  /**
   * @ORM\Column(columnDefinition="varchar(20) NULL")
   */
  private $type_ascenseur; // gauche ou droite

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $cave;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $parking;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $terrasse;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $balcon;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $garage;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $grenier;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $jardin;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $vide_ordure;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $stationnement;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $interphone;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $digicode;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $acces_handicapes;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $climatisation;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $gardien;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $animal;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $fumeur;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $internet;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $emplacement_boite_lettres;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $nom_boite_lettres;


  //##################################################
  // CHAUFFAGE

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $type_chauffage;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $mode_chauffage;

  /**
   * @ORM\Column(columnDefinition="tinyint(1) NULL DEFAULT '0'")
   */
  private $statut_diagnostic;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL")
   */
  private $date_diagnostic;

  /**
   * @ORM\Column(columnDefinition="varchar(1000) NULL")
   */
  private $fichier_diagnostic;

  /**
   * @ORM\Column(columnDefinition="varchar(1) NULL")
   */
  private $consommation_lettre;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL")
   */
  private $consommation_chiffre;

  /**
   * @ORM\Column(columnDefinition="varchar(1) NULL")
   */
  private $gaz_serre_lettre;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL")
   */
  private $gaz_serre_chiffre;

  /**
   * @ORM\Column(columnDefinition="varchar(20) NULL")
   */
  private $compteur_edf;

  /**
   * @ORM\Column(columnDefinition="varchar(20) NULL")
   */
  private $compteur_gdf;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $emplacement_compteurs;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $emplacement_compteur_eau;


  //##################################################
  // NOTATION

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $etat_interieur;

  /**
   * @ORM\Column(columnDefinition="tinyint(1) NULL")
   */
  private $calme;

  /**
   * @ORM\Column(columnDefinition="tinyint(1) NULL")
   */
  private $clair;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $neuf_ancien;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $standing_immeuble;

  //##################################################
  // VISITES

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $visite_digicode;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $visite_porte;

  /**
   * @ORM\Column(columnDefinition="text NULL")
   */
  private $visite_description;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $visite_cle;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $visite_cave;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   */
  private $visite_parking;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL DEFAULT '16h00'")
   */
  private $heure_arrivee;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL DEFAULT '10h00'")
   */
  private $heure_depart;

  //##################################################
  // INFOS

  /**
   * @ORM\Column(columnDefinition="text NULL")
   */
  private $commentaire;

  /**
   * @ORM\Column(columnDefinition="text NULL")
   */
  private $confidentiel;

  /**
   * @ORM\Column(columnDefinition="text NULL")
   */
  private $coordonnees_syndic;

  //##################################################
  // INTERNE

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $is_selection;

  /**
   * @ORM\Column(columnDefinition="tinyint(2) NOT NULL DEFAULT '0'")
   */
  private $statut;

  /**
   * @ORM\Column(columnDefinition="tinyint(1) NOT NULL DEFAULT '0'")
   */
  private $cmc;

  //##################################################

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $is_resa;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $is_labeliser;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $is_reloger;

  // INFOS DATES

  /**
   * @ORM\Column(columnDefinition="date NULL")
   */
  private $publie_debut;

  /**
   * @ORM\Column(columnDefinition="date NULL")
   */
  private $publie_fin;

  /**
   * @ORM\Column(columnDefinition="datetime NULL")
   */
  private $created_at;

  /**
   * @ORM\ManyToOne(targetEntity="Pat\UtilisateurBundle\Entity\Utilisateur")
   */
  private $created_by;

  /**
   * @ORM\Column(columnDefinition="datetime NULL")
   */
  private $updated_at;

  /**
   * @ORM\ManyToOne(targetEntity="Pat\UtilisateurBundle\Entity\Utilisateur")
   */
  private $updated_by;

  /**
   * @ORM\OneToOne(targetEntity="Tarif", mappedBy="appartement")
   */
  protected $tarif;

  /**
   * @var TarifHistorique[]
   *
   * @ORM\OneToMany(targetEntity="TarifHistorique", mappedBy="appartement")
   */
  protected $tarifHistoriques;

  /**
   * @var Promotion[]
   *
   * @ORM\OneToMany(targetEntity="Pat\CompteBundle\Entity\Promotion", mappedBy="appartement")
   */
  protected $promotions;

  /**
   * @var Piece[]
   *
   * @ORM\OneToMany(targetEntity="Piece", mappedBy="appartement")
   */
  protected $pieces;

  public function __toString()
  {
    $ville = $this->getVille() == null ? "" : $this->getVille()->getNom();
    $nb_pieces = $this->getNbPieces() == null ? "" : "T".$this->getNbPieces();
    return ucfirst(strtolower($this->getType()))." ".$nb_pieces." ".$this->getSurfaceSol()." m<sup>2</sup> ".$ville." (RÉF : ".$this->getReference().")";
  }

  /**
   * @ORM\PrePersist
   */
  public function setInitialValues()
  {
    $this->created_at = date("Y-m-d H:i:s");
    $this->updated_at = date("Y-m-d H:i:s");
  }

  /**
   * @ORM\PreUpdate
   */
  public function onUpdate()
  {
    $this->updated_at = date("Y-m-d H:i:s");
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set reference
   *
   * @param string $reference
   */
  public function setReference($reference)
  {
    $this->reference = $reference;
  }

  /**
   * Get reference
   *
   * @return string
   */
  public function getReference()
  {
    return $this->reference;
  }

  /**
   * Set type
   *
   * @param string $type
   */
  public function setType($type)
  {
    $this->type = $type;
  }

  /**
   * Get type
   *
   * @return string
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Set nb_pieces
   *
   * @param integer $NbPieces
   */
  public function setNbPieces($NbPieces)
  {
    $this->nb_pieces = $NbPieces;
  }

  /**
   * Get nb_pieces
   *
   * @return integer
   */
  public function getNbPieces()
  {
    return $this->nb_pieces;
  }

  /**
   * Set url_fr
   *
   * @param string $urlFr
   */
  public function setUrlFr($urlFr)
  {
    $this->url_fr = $urlFr;
  }

  /**
   * Get url_fr
   *
   * @return string
   */
  public function getUrlFr()
  {
    return $this->url_fr;
  }

  /**
   * Set url_en
   *
   * @param string $urlEn
   */
  public function setUrlEn($urlEn)
  {
    $this->url_en = $urlEn;
  }

  /**
   * Get url_en
   *
   * @return string
   */
  public function getUrlEn()
  {
    return $this->url_en;
  }

  /**
   * Set url_de
   *
   * @param string $urlDe
   */
  public function setUrlDe($urlDe)
  {
    $this->url_de = $urlDe;
  }

  /**
   * Get url_de
   *
   * @return string
   */
  public function getUrlDe()
  {
    return $this->url_de;
  }

  /**
   * Set description_courte_fr
   *
   * @param string $descriptionCourteFr
   */
  public function setDescriptionCourteFr($descriptionCourteFr)
  {
    $this->description_courte_fr = $descriptionCourteFr;
  }

  /**
   * Get description_courte_fr
   *
   * @return string
   */
  public function getDescriptionCourteFr()
  {
    return $this->description_courte_fr;
  }

  /**
   * Set description_courte_en
   *
   * @param string $descriptionCourteEn
   */
  public function setDescriptionCourteEn($descriptionCourteEn)
  {
    $this->description_courte_en = $descriptionCourteEn;
  }

  /**
   * Get description_courte_en
   *
   * @return string
   */
  public function getDescriptionCourteEn()
  {
    return $this->description_courte_en;
  }

  /**
   * Set description_courte_de
   *
   * @param string $descriptionCourteDe
   */
  public function setDescriptionCourteDe($descriptionCourteDe)
  {
    $this->description_courte_de = $descriptionCourteDe;
  }

  /**
   * Get description_courte_de
   *
   * @return string
   */
  public function getDescriptionCourteDe()
  {
    return $this->description_courte_de;
  }

  /**
   * Set description_fr
   *
   * @param string $descriptionFr
   */
  public function setDescriptionFr($descriptionFr)
  {
    $this->description_fr = $descriptionFr;
  }

  /**
   * Get description_fr
   *
   * @return string
   */
  public function getDescriptionFr()
  {
    return $this->description_fr;
  }

  /**
   * Set description_en
   *
   * @param string $descriptionEn
   */
  public function setDescriptionEn($descriptionEn)
  {
    $this->description_en = $descriptionEn;
  }

  /**
   * Get description_en
   *
   * @return string
   */
  public function getDescriptionEn()
  {
    return $this->description_en;
  }

  /**
   * Set description_de
   *
   * @param string $descriptionDe
   */
  public function setDescriptionDe($descriptionDe)
  {
    $this->description_de = $descriptionDe;
  }

  /**
   * Get description_de
   *
   * @return string
   */
  public function getDescriptionDe()
  {
    return $this->description_de;
  }

  /**
   * Set nom_residence
   *
   * @param string $nomResidence
   */
  public function setNomResidence($nomResidence)
  {
    $this->nom_residence = $nomResidence;
  }

  /**
   * Get nom_residence
   *
   * @return string
   */
  public function getNomResidence()
  {
    return $this->nom_residence;
  }

  /**
   * Set adresse
   *
   * @param string $adresse
   */
  public function setAdresse($adresse)
  {
    $this->adresse = $adresse;
  }

  /**
   * Get adresse
   *
   * @return string
   */
  public function getAdresse()
  {
    return $this->adresse;
  }

  /**
   * Set adresse2
   *
   * @param string $adresse2
   */
  public function setAdresse2($adresse2)
  {
    $this->adresse2 = $adresse2;
  }

  /**
   * Get adresse2
   *
   * @return string
   */
  public function getAdresse2()
  {
    return $this->adresse2;
  }

  /**
   * Set is_meuble
   *
   * @param boolean $isMeuble
   */
  public function setIsMeuble($isMeuble)
  {
    $this->is_meuble = $isMeuble;
  }

  /**
   * Get is_meuble
   *
   * @return boolean
   */
  public function getIsMeuble()
  {
    return $this->is_meuble;
  }

  /**
   * Set is_duplex
   *
   * @param boolean $isDuplex
   */
  public function setIsDuplex($isDuplex)
  {
    $this->is_duplex = $isDuplex;
  }

  /**
   * Get is_duplex
   *
   * @return boolean
   */
  public function getIsDuplex()
  {
    return $this->is_duplex;
  }

  /**
   * Set distance_aeroport
   *
   * @param string $distanceAeroport
   */
  public function setDistanceAeroport($distanceAeroport)
  {
    $this->distance_aeroport = $distanceAeroport;
  }

  /**
   * Get distance_aeroport
   *
   * @return string
   */
  public function getDistanceAeroport()
  {
    return $this->distance_aeroport;
  }

  /**
   * Set distance_autoroute
   *
   * @param string $distanceAutoroute
   */
  public function setDistanceAutoroute($distanceAutoroute)
  {
    $this->distance_autoroute = $distanceAutoroute;
  }

  /**
   * Get distance_autoroute
   *
   * @return string
   */
  public function getDistanceAutoroute()
  {
    return $this->distance_autoroute;
  }

  /**
   * Set distance_voie_rapide
   *
   * @param string $distanceVoieRapide
   */
  public function setDistanceVoieRapide($distanceVoieRapide)
  {
    $this->distance_voie_rapide = $distanceVoieRapide;
  }

  /**
   * Get distance_voie_rapide
   *
   * @return string
   */
  public function getDistanceVoieRapide()
  {
    return $this->distance_voie_rapide;
  }

  /**
   * Set distance_commerce
   *
   * @param string $distanceCommerce
   */
  public function setDistanceCommerce($distanceCommerce)
  {
    $this->distance_commerce = $distanceCommerce;
  }

  /**
   * Get distance_commerce
   *
   * @return string
   */
  public function getDistanceCommerce()
  {
    return $this->distance_commerce;
  }

  /**
   * Set acces_marche
   *
   * @param string $accesMarche
   */
  public function setAccesMarche($accesMarche)
  {
    $this->acces_marche = $accesMarche;
  }

  /**
   * Get acces_marche
   *
   * @return string
   */
  public function getAccesMarche()
  {
    return $this->acces_marche;
  }

  /**
   * Set acces_gare
   *
   * @param string $accesGare
   */
  public function setAccesGare($accesGare)
  {
    $this->acces_gare = $accesGare;
  }

  /**
   * Get acces_gare
   *
   * @return string
   */
  public function getAccesGare()
  {
    return $this->acces_gare;
  }

  /**
   * Set acces_bus
   *
   * @param string $accesBus
   */
  public function setAccesBus($accesBus)
  {
    $this->acces_bus = $accesBus;
  }

  /**
   * Get acces_bus
   *
   * @return string
   */
  public function getAccesBus()
  {
    return $this->acces_bus;
  }

  /**
   * Set acces_metro
   *
   * @param string $accesMetro
   */
  public function setAccesMetro($accesMetro)
  {
    $this->acces_metro = $accesMetro;
  }

  /**
   * Get acces_metro
   *
   * @return string
   */
  public function getAccesMetro()
  {
    return $this->acces_metro;
  }

  /**
   * Set acces_tram
   *
   * @param string $accesTram
   */
  public function setAccesTram($accesTram)
  {
    $this->acces_tram = $accesTram;
  }

  /**
   * Get acces_tram
   *
   * @return string
   */
  public function getAccesTram()
  {
    return $this->acces_tram;
  }

  /**
   * Set surface_sol
   *
   * @param string $surfaceSol
   */
  public function setSurfaceSol($surfaceSol)
  {
    $this->surface_sol = $surfaceSol;
  }

  /**
   * Get surface_sol
   *
   * @return string
   */
  public function getSurfaceSol()
  {
    return $this->surface_sol;
  }

  /**
   * Set surface_carrez
   *
   * @param string $surfaceCarrez
   */
  public function setSurfaceCarrez($surfaceCarrez)
  {
    $this->surface_carrez = $surfaceCarrez;
  }

  /**
   * Get surface_carrez
   *
   * @return string
   */
  public function getSurfaceCarrez()
  {
    return $this->surface_carrez;
  }

  /**
   * Set surface_terrain
   *
   * @param string $surfaceTerrain
   */
  public function setSurfaceTerrain($surfaceTerrain)
  {
    $this->surface_terrain = $surfaceTerrain;
  }

  /**
   * Get surface_terrain
   *
   * @return string
   */
  public function getSurfaceTerrain()
  {
    return $this->surface_terrain;
  }

  /**
   * Set nb_personne
   *
   * @param integer $nbPersonne
   */
  public function setNbPersonne($nbPersonne)
  {
    $this->nb_personne = $nbPersonne;
  }

  /**
   * Get nb_personne
   *
   * @return integer
   */
  public function getNbPersonne()
  {
    return $this->nb_personne;
  }

  /**
   * Set etage
   *
   * @param string $etage
   */
  public function setEtage($etage)
  {
    $this->etage = $etage;
  }

  /**
   * Get etage
   *
   * @return string
   */
  public function getEtage()
  {
    return $this->etage;
  }

  /**
   * Set nb_etage
   *
   * @param string $nbEtage
   */
  public function setNbEtage($nbEtage)
  {
    $this->nb_etage = $nbEtage;
  }

  /**
   * Get nb_etage
   *
   * @return string
   */
  public function getNbEtage()
  {
    return $this->nb_etage;
  }

  /**
   * Set entree_immeuble
   *
   * @param string $entreeImmeuble
   */
  public function setEntreeImmeuble($entreeImmeuble)
  {
    $this->entree_immeuble = $entreeImmeuble;
  }

  /**
   * Get entree_immeuble
   *
   * @return string
   */
  public function getEntreeImmeuble()
  {
    return $this->entree_immeuble;
  }

  /**
   * Set serrure
   *
   * @param boolean $serrure
   */
  public function setSerrure($serrure)
  {
    $this->serrure = $serrure;
  }

  /**
   * Get serrure
   *
   * @return boolean
   */
  public function getSerrure()
  {
    return $this->serrure;
  }

  /**
   * Set escalier
   *
   * @param boolean $escalier
   */
  public function setEscalier($escalier)
  {
    $this->escalier = $escalier;
  }

  /**
   * Get escalier
   *
   * @return boolean
   */
  public function getEscalier()
  {
    return $this->escalier;
  }

  /**
   * Set type_escalier
   *
   * @param string $typeEscalier
   */
  public function setTypeEscalier($typeEscalier)
  {
    $this->type_escalier = $typeEscalier;
  }

  /**
   * Get type_escalier
   *
   * @return string
   */
  public function getTypeEscalier()
  {
    return $this->type_escalier;
  }

  /**
   * Set ascenseur
   *
   * @param boolean $ascenseur
   */
  public function setAscenseur($ascenseur)
  {
    $this->ascenseur = $ascenseur;
  }

  /**
   * Get ascenseur
   *
   * @return boolean
   */
  public function getAscenseur()
  {
    return $this->ascenseur;
  }

  /**
   * Set type_ascenseur
   *
   * @param string $typeAscenseur
   */
  public function setTypeAscenseur($typeAscenseur)
  {
    $this->type_ascenseur = $typeAscenseur;
  }

  /**
   * Get type_ascenseur
   *
   * @return string
   */
  public function getTypeAscenseur()
  {
    return $this->type_ascenseur;
  }

  /**
   * Set nb_cave
   *
   * @param string $nbCave
   */
  public function setNbCave($nbCave)
  {
    $this->nb_cave = $nbCave;
  }

  /**
   * Get nb_cave
   *
   * @return string
   */
  public function getNbCave()
  {
    return $this->nb_cave;
  }

  /**
   * Set nb_parking
   *
   * @param string $nbParking
   */
  public function setNbParking($nbParking)
  {
    $this->nb_parking = $nbParking;
  }

  /**
   * Get nb_parking
   *
   * @return string
   */
  public function getNbParking()
  {
    return $this->nb_parking;
  }

  /**
   * Set nb_terrasse
   *
   * @param string $nbTerrasse
   */
  public function setNbTerrasse($nbTerrasse)
  {
    $this->nb_terrasse = $nbTerrasse;
  }

  /**
   * Get nb_terrasse
   *
   * @return string
   */
  public function getNbTerrasse()
  {
    return $this->nb_terrasse;
  }

  /**
   * Set nb_balcon
   *
   * @param string $nbBalcon
   */
  public function setNbBalcon($nbBalcon)
  {
    $this->nb_balcon = $nbBalcon;
  }

  /**
   * Get nb_balcon
   *
   * @return string
   */
  public function getNbBalcon()
  {
    return $this->nb_balcon;
  }

  /**
   * Set nb_garage
   *
   * @param string $nbGarage
   */
  public function setNbGarage($nbGarage)
  {
    $this->nb_garage = $nbGarage;
  }

  /**
   * Get nb_garage
   *
   * @return string
   */
  public function getNbGarage()
  {
    return $this->nb_garage;
  }

  /**
   * Set grenier
   *
   * @param boolean $grenier
   */
  public function setGrenier($grenier)
  {
    $this->grenier = $grenier;
  }

  /**
   * Get grenier
   *
   * @return boolean
   */
  public function getGrenier()
  {
    return $this->grenier;
  }

  /**
   * Set jardin
   *
   * @param boolean $jardin
   */
  public function setJardin($jardin)
  {
    $this->jardin = $jardin;
  }

  /**
   * Get jardin
   *
   * @return boolean
   */
  public function getJardin()
  {
    return $this->jardin;
  }

  /**
   * Set vide_ordure
   *
   * @param boolean $videOrdure
   */
  public function setVideOrdure($videOrdure)
  {
    $this->vide_ordure = $videOrdure;
  }

  /**
   * Get vide_ordure
   *
   * @return boolean
   */
  public function getVideOrdure()
  {
    return $this->vide_ordure;
  }

  /**
   * Set stationnement
   *
   * @param string $stationnement
   */
  public function setStationnement($stationnement)
  {
    $this->stationnement = $stationnement;
  }

  /**
   * Get stationnement
   *
   * @return string
   */
  public function getStationnement()
  {
    return $this->stationnement;
  }

  /**
   * Set interphone
   *
   * @param boolean $interphone
   */
  public function setInterphone($interphone)
  {
    $this->interphone = $interphone;
  }

  /**
   * Get interphone
   *
   * @return boolean
   */
  public function getInterphone()
  {
    return $this->interphone;
  }

  /**
   * Set digicode
   *
   * @param string $digicode
   */
  public function setDigicode($digicode)
  {
    $this->digicode = $digicode;
  }

  /**
   * Get digicode
   *
   * @return string
   */
  public function getDigicode()
  {
    return $this->digicode;
  }

  /**
   * Set acces_handicapes
   *
   * @param boolean $accesHandicapes
   */
  public function setAccesHandicapes($accesHandicapes)
  {
    $this->acces_handicapes = $accesHandicapes;
  }

  /**
   * Get acces_handicapes
   *
   * @return boolean
   */
  public function getAccesHandicapes()
  {
    return $this->acces_handicapes;
  }

  /**
   * Set climatisation
   *
   * @param boolean $climatisation
   */
  public function setClimatisation($climatisation)
  {
    $this->climatisation = $climatisation;
  }

  /**
   * Get climatisation
   *
   * @return boolean
   */
  public function getClimatisation()
  {
    return $this->climatisation;
  }

  /**
   * Set gardien
   *
   * @param boolean $gardien
   */
  public function setGardien($gardien)
  {
    $this->gardien = $gardien;
  }

  /**
   * Get gardien
   *
   * @return boolean
   */
  public function getGardien()
  {
    return $this->gardien;
  }

  /**
   * Set animal
   *
   * @param boolean $animal
   */
  public function setAnimal($animal)
  {
    $this->animal = $animal;
  }

  /**
   * Get animal
   *
   * @return boolean
   */
  public function getAnimal()
  {
    return $this->animal;
  }

  /**
   * Set fumeur
   *
   * @param boolean $fumeur
   */
  public function setFumeur($fumeur)
  {
    $this->fumeur = $fumeur;
  }

  /**
   * Get fumeur
   *
   * @return boolean
   */
  public function getFumeur()
  {
    return $this->fumeur;
  }

  /**
   * Set internet
   *
   * @param boolean $internet
   */
  public function setInternet($internet)
  {
    $this->internet = $internet;
  }

  /**
   * Get internet
   *
   * @return boolean
   */
  public function getInternet()
  {
    return $this->internet;
  }

  /**
   * Set type_chauffage
   *
   * @param string $typeChauffage
   */
  public function setTypeChauffage($typeChauffage)
  {
    $this->type_chauffage = $typeChauffage;
  }

  /**
   * Get type_chauffage
   *
   * @return string
   */
  public function getTypeChauffage()
  {
    return $this->type_chauffage;
  }

  /**
   * Set mode_chauffage
   *
   * @param string $modeChauffage
   */
  public function setModeChauffage($modeChauffage)
  {
    $this->mode_chauffage = $modeChauffage;
  }

  /**
   * Get mode_chauffage
   *
   * @return string
   */
  public function getModeChauffage()
  {
    return $this->mode_chauffage;
  }

  /**
   * Set statut_diagnostic
   *
   * @param string $statutDiagnostic
   */
  public function setStatutDiagnostic($statutDiagnostic)
  {
    $this->statut_diagnostic = $statutDiagnostic;
  }

  /**
   * Get statut_diagnostic
   *
   * @return string
   */
  public function getStatutDiagnostic()
  {
    return $this->statut_diagnostic;
  }

  /**
   * Set date_diagnostic
   *
   * @param string $dateDiagnostic
   */
  public function setDateDiagnostic($dateDiagnostic)
  {
    $this->date_diagnostic = $dateDiagnostic;
  }

  /**
   * Get date_diagnostic
   *
   * @return string
   */
  public function getDateDiagnostic()
  {
    return $this->date_diagnostic;
  }

  /**
   * Set fichier_diagnostic
   *
   * @param string $fichierDiagnostic
   */
  public function setFichierDiagnostic($fichierDiagnostic)
  {
    $this->fichier_diagnostic = $fichierDiagnostic;
  }

  /**
   * Get fichier_diagnostic
   *
   * @return string
   */
  public function getFichierDiagnostic()
  {
    return $this->fichier_diagnostic;
  }

  /**
   * Set consommation_lettre
   *
   * @param string $consommationLettre
   */
  public function setConsommationLettre($consommationLettre)
  {
    $this->consommation_lettre = $consommationLettre;
  }

  /**
   * Get consommation_lettre
   *
   * @return string
   */
  public function getConsommationLettre()
  {
    return $this->consommation_lettre;
  }

  /**
   * Set consommation_chiffre
   *
   * @param string $consommationChiffre
   */
  public function setConsommationChiffre($consommationChiffre)
  {
    $this->consommation_chiffre = $consommationChiffre;
  }

  /**
   * Get consommation_chiffre
   *
   * @return string
   */
  public function getConsommationChiffre()
  {
    return $this->consommation_chiffre;
  }

  /**
   * Set gaz_serre_lettre
   *
   * @param string $gazSerreLettre
   */
  public function setGazSerreLettre($gazSerreLettre)
  {
    $this->gaz_serre_lettre = $gazSerreLettre;
  }

  /**
   * Get gaz_serre_lettre
   *
   * @return string
   */
  public function getGazSerreLettre()
  {
    return $this->gaz_serre_lettre;
  }

  /**
   * Set gaz_serre_chiffre
   *
   * @param string $gazSerreChiffre
   */
  public function setGazSerreChiffre($gazSerreChiffre)
  {
    $this->gaz_serre_chiffre = $gazSerreChiffre;
  }

  /**
   * Get gaz_serre_chiffre
   *
   * @return string
   */
  public function getGazSerreChiffre()
  {
    return $this->gaz_serre_chiffre;
  }

  /**
   * Set etat_interieur
   *
   * @param string $etatInterieur
   */
  public function setEtatInterieur($etatInterieur)
  {
    $this->etat_interieur = $etatInterieur;
  }

  /**
   * Get etat_interieur
   *
   * @return string
   */
  public function getEtatInterieur()
  {
    return $this->etat_interieur;
  }

  /**
   * Set calme
   *
   * @param string $calme
   */
  public function setCalme($calme)
  {
    $this->calme = $calme;
  }

  /**
   * Get calme
   *
   * @return string
   */
  public function getCalme()
  {
    return $this->calme;
  }

  /**
   * Set clair
   *
   * @param string $clair
   */
  public function setClair($clair)
  {
    $this->clair = $clair;
  }

  /**
   * Get clair
   *
   * @return string
   */
  public function getClair()
  {
    return $this->clair;
  }

  /**
   * Set neuf_ancien
   *
   * @param string $neufAncien
   */
  public function setNeufAncien($neufAncien)
  {
    $this->neuf_ancien = $neufAncien;
  }

  /**
   * Get neuf_ancien
   *
   * @return string
   */
  public function getNeufAncien()
  {
    return $this->neuf_ancien;
  }

  /**
   * Set standing_immeuble
   *
   * @param string $standingImmeuble
   */
  public function setStandingImmeuble($standingImmeuble)
  {
    $this->standing_immeuble = $standingImmeuble;
  }

  /**
   * Get standing_immeuble
   *
   * @return string
   */
  public function getStandingImmeuble()
  {
    return $this->standing_immeuble;
  }

  /**
   * Set visite_digicode
   *
   * @param string $visiteDigicode
   */
  public function setVisiteDigicode($visiteDigicode)
  {
    $this->visite_digicode = $visiteDigicode;
  }

  /**
   * Get visite_digicode
   *
   * @return string
   */
  public function getVisiteDigicode()
  {
    return $this->visite_digicode;
  }

  /**
   * Set visite_porte
   *
   * @param string $visitePorte
   */
  public function setVisitePorte($visitePorte)
  {
    $this->visite_porte = $visitePorte;
  }

  /**
   * Get visite_porte
   *
   * @return string
   */
  public function getVisitePorte()
  {
    return $this->visite_porte;
  }

  /**
   * Set visite_description
   *
   * @param string $visiteDescription
   */
  public function setVisiteDescription($visiteDescription)
  {
    $this->visite_description = $visiteDescription;
  }

  /**
   * Get visite_description
   *
   * @return string
   */
  public function getVisiteDescription()
  {
    return $this->visite_description;
  }

  /**
   * Set visite_cle
   *
   * @param string $visiteCle
   */
  public function setVisiteCle($visiteCle)
  {
    $this->visite_cle = $visiteCle;
  }

  /**
   * Get visite_cle
   *
   * @return string
   */
  public function getVisiteCle()
  {
    return $this->visite_cle;
  }

  /**
   * Set visite_cave
   *
   * @param string $visiteCave
   */
  public function setVisiteCave($visiteCave)
  {
    $this->visite_cave = $visiteCave;
  }

  /**
   * Get visite_cave
   *
   * @return string
   */
  public function getVisiteCave()
  {
    return $this->visite_cave;
  }

  /**
   * Set visite_parking
   *
   * @param string $visiteParking
   */
  public function setVisiteParking($visiteParking)
  {
    $this->visite_parking = $visiteParking;
  }

  /**
   * Get visite_parking
   *
   * @return string
   */
  public function getVisiteParking()
  {
    return $this->visite_parking;
  }

  /**
   * Set heure_arrivee
   *
   * @param string $heureArrivee
   */
  public function setHeureArrivee($heureArrivee)
  {
    $this->heure_arrivee = $heureArrivee;
  }

  /**
   * Get heure_arrivee
   *
   * @return string
   */
  public function getHeureArrivee()
  {
    return $this->heure_arrivee;
  }

  /**
   * Set heure_depart
   *
   * @param string $heureDepart
   */
  public function setHeureDepart($heureDepart)
  {
    $this->heure_depart = $heureDepart;
  }

  /**
   * Get heure_depart
   *
   * @return string
   */
  public function getHeureDepart()
  {
    return $this->heure_depart;
  }

  /**
   * Set commentaire
   *
   * @param string $commentaire
   */
  public function setCommentaire($commentaire)
  {
    $this->commentaire = $commentaire;
  }

  /**
   * Get commentaire
   *
   * @return string
   */
  public function getCommentaire()
  {
    return $this->commentaire;
  }

  /**
   * Set confidentiel
   *
   * @param string $confidentiel
   */
  public function setConfidentiel($confidentiel)
  {
    $this->confidentiel = $confidentiel;
  }

  /**
   * Get confidentiel
   *
   * @return string
   */
  public function getConfidentiel()
  {
    return $this->confidentiel;
  }

  /**
   * Set is_selection
   *
   * @param string $isSelection
   */
  public function setIsSelection($isSelection)
  {
    $this->is_selection = $isSelection;
  }

  /**
   * Get is_selection
   *
   * @return string
   */
  public function getIsSelection()
  {
    return $this->is_selection;
  }

  /**
   * Set statut
   *
   * @param string $statut
   */
  public function setStatut($statut)
  {
    $this->statut = $statut;
  }

  /**
   * Get statut
   *
   * @return string
   */
  public function getStatut()
  {
    return $this->statut;
  }

  /**
   * Set cmc
   *
   * @param string $cmc
   */
  public function setCmc($cmc)
  {
    $this->cmc = $cmc;
  }

  /**
   * Get cmc
   *
   * @return string
   */
  public function getCmc()
  {
    return $this->cmc;
  }

  /**
   * Set is_resa
   *
   * @param string $isResa
   */
  public function setIsResa($isResa)
  {
    $this->is_resa = $isResa;
  }

  /**
   * Get is_resa
   *
   * @return string
   */
  public function getIsResa()
  {
    return $this->is_resa;
  }

  /**
   * Set is_labeliser
   *
   * @param string $isLabeliser
   */
  public function setIsLabeliser($isLabeliser)
  {
    $this->is_labeliser = $isLabeliser;
  }

  /**
   * Get is_labeliser
   *
   * @return string
   */
  public function getIsLabeliser()
  {
    return $this->is_labeliser;
  }

  /**
   * Set is_reloger
   *
   * @param string $isReloger
   */
  public function setIsReloger($isReloger)
  {
    $this->is_reloger = $isReloger;
  }

  /**
   * Get is_reloger
   *
   * @return string
   */
  public function getIsReloger()
  {
    return $this->is_reloger;
  }

  /**
   * Set publie_debut
   *
   * @param string $publieDebut
   */
  public function setPublieDebut($publieDebut)
  {
    $this->publie_debut = $publieDebut;
  }

  /**
   * Get publie_debut
   *
   * @return string
   */
  public function getPublieDebut()
  {
    return $this->publie_debut;
  }

  /**
   * Set publie_fin
   *
   * @param string $publieFin
   */
  public function setPublieFin($publieFin)
  {
    $this->publie_fin = $publieFin;
  }

  /**
   * Get publie_fin
   *
   * @return string
   */
  public function getPublieFin()
  {
    return $this->publie_fin;
  }

  /**
   * Set created_at
   *
   * @param string $createdAt
   */
  public function setCreatedAt($createdAt)
  {
    $this->created_at = $createdAt;
  }

  /**
   * Get created_at
   *
   * @return string
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * Set updated_at
   *
   * @param string $updatedAt
   */
  public function setUpdatedAt($updatedAt)
  {
    $this->updated_at = $updatedAt;
  }

  /**
   * Get updated_at
   *
   * @return string
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
   * Set ville
   *
   * @param Pat\CompteBundle\Entity\Ville $ville
   */
  public function setVille(\Pat\CompteBundle\Entity\Ville $ville)
  {
    $this->ville = $ville;
  }

  /**
   * Get ville
   *
   * @return Pat\CompteBundle\Entity\Ville
   */
  public function getVille()
  {
    return $this->ville;
  }

  /**
   * Set created_by
   *
   * @param Pat\UtilisateurBundle\Entity\Utilisateur $createdBy
   */
  public function setCreatedBy(\Pat\UtilisateurBundle\Entity\Utilisateur $createdBy)
  {
    $this->created_by = $createdBy;
  }

  /**
   * Get created_by
   *
   * @return Pat\UtilisateurBundle\Entity\Utilisateur
   */
  public function getCreatedBy()
  {
    return $this->created_by;
  }

  /**
   * Set updated_by
   *
   * @param Pat\UtilisateurBundle\Entity\Utilisateur $updatedBy
   */
  public function setUpdatedBy(\Pat\UtilisateurBundle\Entity\Utilisateur $updatedBy)
  {
    $this->updated_by = $updatedBy;
  }

  /**
   * Get updated_by
   *
   * @return Pat\UtilisateurBundle\Entity\Utilisateur
   */
  public function getUpdatedBy()
  {
    return $this->updated_by;
  }

  /**
   * Set utilisateur
   *
   * @param Pat\UtilisateurBundle\Entity\Utilisateur $utilisateur
   */
  public function setUtilisateur(\Pat\UtilisateurBundle\Entity\Utilisateur $utilisateur)
  {
    $this->utilisateur = $utilisateur;
  }

  /**
   * Get utilisateur
   *
   * @return Pat\UtilisateurBundle\Entity\Utilisateur
   */
  public function getUtilisateur()
  {
    return $this->utilisateur;
  }

  /**
   * Set cave
   *
   * @param string $cave
   */
  public function setCave($cave)
  {
    $this->cave = $cave;
  }

  /**
   * Get cave
   *
   * @return string
   */
  public function getCave()
  {
    return $this->cave;
  }

  /**
   * Set parking
   *
   * @param string $parking
   */
  public function setParking($parking)
  {
    $this->parking = $parking;
  }

  /**
   * Get parking
   *
   * @return string
   */
  public function getParking()
  {
    return $this->parking;
  }

  /**
   * Set terrasse
   *
   * @param string $terrasse
   */
  public function setTerrasse($terrasse)
  {
    $this->terrasse = $terrasse;
  }

  /**
   * Get terrasse
   *
   * @return string
   */
  public function getTerrasse()
  {
    return $this->terrasse;
  }

  /**
   * Set balcon
   *
   * @param string $balcon
   */
  public function setBalcon($balcon)
  {
    $this->balcon = $balcon;
  }

  /**
   * Get balcon
   *
   * @return string
   */
  public function getBalcon()
  {
    return $this->balcon;
  }

  /**
   * Set garage
   *
   * @param string $garage
   */
  public function setGarage($garage)
  {
    $this->garage = $garage;
  }

  /**
   * Get garage
   *
   * @return string
   */
  public function getGarage()
  {
    return $this->garage;
  }

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->tarifs = new ArrayCollection();
    $this->tarifHistoriques = new ArrayCollection();
    $this->promotions = new ArrayCollection();
    $this->pieces = new ArrayCollection();
  }

  /**
   * Set compteur_edf
   *
   * @param string $compteurEdf
   * @return Appartement
   */
  public function setCompteurEdf($compteurEdf)
  {
    $this->compteur_edf = $compteurEdf;

    return $this;
  }

  /**
   * Get compteur_edf
   *
   * @return string
   */
  public function getCompteurEdf()
  {
    return $this->compteur_edf;
  }

  /**
   * Set compteur_gdf
   *
   * @param string $compteurGdf
   * @return Appartement
   */
  public function setCompteurGdf($compteurGdf)
  {
    $this->compteur_gdf = $compteurGdf;

    return $this;
  }

  /**
   * Get compteur_gdf
   *
   * @return string
   */
  public function getCompteurGdf()
  {
    return $this->compteur_gdf;
  }

  /**
   * Set emplacement_compteurs
   *
   * @param string $emplacementCompteurs
   * @return Appartement
   */
  public function setEmplacementCompteurs($emplacementCompteurs)
  {
    $this->emplacement_compteurs = $emplacementCompteurs;

    return $this;
  }

  /**
   * Get emplacement_compteurs
   *
   * @return string
   */
  public function getEmplacementCompteurs()
  {
    return $this->emplacement_compteurs;
  }

  /**
   * Set arret_tram
   *
   * @param string $arretTram
   * @return Appartement
   */
  public function setArretTram($arretTram)
  {
    $this->arret_tram = $arretTram;

    return $this;
  }

  /**
   * Get arret_tram
   *
   * @return string
   */
  public function getArretTram()
  {
    return $this->arret_tram;
  }

  /**
   * Set arret_tram2
   *
   * @param string $arretTram2
   * @return Appartement
   */
  public function setArretTram2($arretTram2)
  {
    $this->arret_tram2 = $arretTram2;

    return $this;
  }

  /**
   * Get arret_tram2
   *
   * @return string
   */
  public function getArretTram2()
  {
    return $this->arret_tram2;
  }

  /**
   * Set arret_tram3
   *
   * @param string $arretTram3
   * @return Appartement
   */
  public function setArretTram3($arretTram3)
  {
    $this->arret_tram3 = $arretTram3;

    return $this;
  }

  /**
   * Get arret_tram3
   *
   * @return string
   */
  public function getArretTram3()
  {
    return $this->arret_tram3;
  }

  /**
   * Set emplacement_boite_lettres
   *
   * @param string $emplacementBoiteLettres
   * @return Appartement
   */
  public function setEmplacementBoiteLettres($emplacementBoiteLettres)
  {
    $this->emplacement_boite_lettres = $emplacementBoiteLettres;

    return $this;
  }

  /**
   * Get emplacement_boite_lettres
   *
   * @return string
   */
  public function getEmplacementBoiteLettres()
  {
    return $this->emplacement_boite_lettres;
  }

  /**
   * Set nom_boite_lettres
   *
   * @param string $nomBoiteLettres
   * @return Appartement
   */
  public function setNomBoiteLettres($nomBoiteLettres)
  {
    $this->nom_boite_lettres = $nomBoiteLettres;

    return $this;
  }

  /**
   * Get nom_boite_lettres
   *
   * @return string
   */
  public function getNomBoiteLettres()
  {
    return $this->nom_boite_lettres;
  }

  /**
   * Set emplacement_compteur_eau
   *
   * @param string $emplacementCompteurEau
   * @return Appartement
   */
  public function setEmplacementCompteurEau($emplacementCompteurEau)
  {
    $this->emplacement_compteur_eau = $emplacementCompteurEau;

    return $this;
  }

  /**
   * Get emplacement_compteur_eau
   *
   * @return string
   */
  public function getEmplacementCompteurEau()
  {
    return $this->emplacement_compteur_eau;
  }

  /**
   * Set coordonnees_syndic
   *
   * @param string $coordonneesSyndic
   * @return Appartement
   */
  public function setCoordonneesSyndic($coordonneesSyndic)
  {
    $this->coordonnees_syndic = $coordonneesSyndic;

    return $this;
  }

  /**
   * Get coordonnees_syndic
   *
   * @return string
   */
  public function getCoordonneesSyndic()
  {
    return $this->coordonnees_syndic;
  }

  /**
   * Set tarif
   *
   * @param \Pat\CompteBundle\Entity\Tarif $tarif
   * @return Appartement
   */
  public function setTarif(\Pat\CompteBundle\Entity\Tarif $tarif = null)
  {
    $this->tarif = $tarif;

    return $this;
  }

  /**
   * Get tarif
   *
   * @return \Pat\CompteBundle\Entity\Tarif
   */
  public function getTarif()
  {
    return $this->tarif;
  }

  /**
   * @return int
   */
  public function getNbLitSimple()
  {
    return $this->nb_lit_simple;
  }

  /**
   * @param $nbLitSimple
   *
   * @return $this
   */
  public function setNbLitSimple($nbLitSimple)
  {
    $this->nb_lit_simple = $nbLitSimple;

    return $this;
  }

  /**
   * @return int
   */
  public function getNbLitDouble()
  {
    return $this->nb_lit_double;
  }

  /**
   * @param $nbLitDouble
   *
   * @return $this
   */
  public function setNbLitDouble($nbLitDouble)
  {
    $this->nb_lit_double = $nbLitDouble;

    return $this;
  }

  /**
   * @return int
   */
  public function getNbCanapeConvertible()
  {
    return $this->nb_canape_convertible;
  }

  /**
   * @param $nbCanapeConvertible
   *
   * @return $this
   */
  public function setNbCanapeConvertible($nbCanapeConvertible)
  {
    $this->nb_canape_convertible = $nbCanapeConvertible;

    return $this;
  }

  /**
   * @return int
   */
  public function getNbLitSimpleDouble()
  {
    return $this->nb_lit_simple_double;
  }

  /**
   * @param $nbLitSimpleDouble
   *
   * @return $this
   */
  public function setNbLitSimpleDouble($nbLitSimpleDouble)
  {
    $this->nb_lit_simple_double = $nbLitSimpleDouble;

    return $this;
  }

  /**
   * @return TarifHistorique[]
   */
  public function getTarifHistoriques()
  {
    $criteria = new Criteria();
    $criteria->orderBy(['created_at' => 'DESC']);

    return $this->tarifHistoriques->matching($criteria);
  }

  /**
   * @param TarifHistorique[] $tarifHistoriques
   *
   * @return Appartement
   */
  public function setTarifHistoriques($tarifHistoriques)
  {
    $this->tarifHistoriques = $tarifHistoriques;

    return $this;
  }

  /**
   * @param TarifHistorique $tarifHistorique
   */
  public function addTarifHistorique(TarifHistorique $tarifHistorique)
  {
    if (false === $this->tarifHistoriques->contains($tarifHistorique)) {
      $this->tarifHistoriques->add($tarifHistorique);
      $tarifHistorique->setAppartement($this);
    }
  }

  /**
   * @return Promotion[]
   */
  public function getPromotions()
  {
    return $this->promotions;
  }

  /**
   * @param Promotion[] $promotions
   *
   * @return Appartement
   */
  public function setPromotions($promotions)
  {
    $this->promotions = $promotions;

    return $this;
  }

  /**
   * @param Promotion $promotion
   *
   * @return Appartement
   */
  public function addPromotion(Promotion $promotion)
  {
    if (false === $this->promotions->contains($promotion)) {
      $this->promotions->add($promotion);
    }

    return $this;
  }

  /**
   * @param Promotion $promotion
   *
   * @return Appartement
   */
  public function removePromotion(Promotion $promotion)
  {
    if (true === $this->promotions->contains($promotion)) {
      $this->promotions->removeElement($promotion);
    }

    return $this;
  }

  /**
   * @return Promotion|null
   */
  public function getActivePromotion()
  {
    $criteria = new Criteria();
    $criteria
      ->andWhere(Criteria::expr()->eq('type', Promotion::TYPE_ACTIVABLE))
      ->andWhere(Criteria::expr()->eq('activated', true))
    ;

    $promotion = $this->promotions->matching($criteria)->first();

    return false === $promotion ? null : $promotion;
  }

  public function getFuturPeriodePromotion()
  {
    $criteria = new Criteria();
    $criteria
      ->andWhere(Criteria::expr()->eq('type', Promotion::TYPE_PERIODE))
      ->andWhere(Criteria::expr()->orX(
          'begin > :now'
      ))
    ;

    $promotions = $this->promotions->matching($criteria);
  }

}
