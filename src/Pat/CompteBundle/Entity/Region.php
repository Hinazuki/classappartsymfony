<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Region
 *
 * @ORM\Table(name="region")
 * @ORM\Entity
 */
class Region
{

  /**
   * @var integer
   *
   * @ORM\Column(name="id_region", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="nom_region", type="string", length=250, nullable=false)
   */
  private $nom;

  /**
   * @ORM\OneToMany(targetEntity="Departement", mappedBy="region")
   */
  protected $departements;

  public function __construct()
  {
    $this->departements = new ArrayCollection();
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set nom
   *
   * @param string $nom
   * @return Region
   */
  public function setNom($nom)
  {
    $this->nom = $nom;

    return $this;
  }

  /**
   * Get nom
   *
   * @return string
   */
  public function getNom()
  {
    return $this->nom;
  }

  /**
   * Add departements
   *
   * @param \Pat\CompteBundle\Entity\Departement $departements
   * @return Region
   */
  public function addDepartement(\Pat\CompteBundle\Entity\Departement $departements)
  {
    $this->departements[] = $departements;

    return $this;
  }

  /**
   * Remove departements
   *
   * @param \Pat\CompteBundle\Entity\Departement $departements
   */
  public function removeDepartement(\Pat\CompteBundle\Entity\Departement $departements)
  {
    $this->departements->removeElement($departements);
  }

  /**
   * Get departements
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getDepartements()
  {
    return $this->departements;
  }

}
