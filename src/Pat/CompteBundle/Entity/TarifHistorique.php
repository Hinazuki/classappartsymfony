<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class TarifHistorique
{

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Appartement", inversedBy="tarifHistoriques")
   * @ORM\JoinColumn(name="appartement", referencedColumnName="id")
   */
  private $appartement;

  /**
   * @ORM\Column(name="loyer_nuit", type="decimal", scale=2, nullable=true)
   * @Assert\NotBlank()
   */
  private $loyer_nuit;

  /**
   * @ORM\Column(name="loyer_semaine1", type="decimal", scale=2, nullable=true)
   * @Assert\NotBlank()
   */
  private $loyer_semaine1;

  /**
   * @ORM\Column(name="loyer_mois", type="decimal", scale=2, nullable=true)
   * @Assert\NotBlank()
   */
  private $loyer_mois;

  /**
   * @ORM\Column(name="loyer_nuit_2", type="decimal", scale=2, nullable=true)
   * @Assert\NotBlank()
   */
  private $loyer_nuit_2;

  /**
   * @ORM\Column(name="loyer_semaine1_2", type="decimal", scale=2, nullable=true)
   * @Assert\NotBlank()
   */
  private $loyer_semaine1_2;

  /**
   * @ORM\Column(name="loyer_mois_2", type="decimal", scale=2, nullable=true)
   * @Assert\NotBlank()
   */
  private $loyer_mois_2;

  /**
   * @ORM\Column(type="datetime")
   */
  private $created_at;

  /**
   * @ORM\Column(type="datetime")
   */
  private $updated_at;

  public function __toString()
  {
    return "".$this->loyer_semaine1;
  }

  /**
   * @ORM\PrePersist
   */
  public function setInitialValues()
  {
    $this->created_at = new \DateTime("now");
    $this->updated_at = new \DateTime("now");
  }

  /**
   * @ORM\PreUpdate
   */
  public function onUpdate()
  {
    $this->updated_at = new \DateTime("now");
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set loyer_nuit
   *
   * @param null|string $loyerNuit
   * @return Tarif
   */
  public function setLoyerNuit($loyerNuit = null)
  {
    $this->loyer_nuit = $loyerNuit;

    return $this;
  }

  /**
   * Get loyer_nuit
   *
   * @return null|string
   */
  public function getLoyerNuit()
  {
    return $this->loyer_nuit;
  }

  /**
   * Set loyer_semaine1
   *
   * @param null|string $loyerSemaine1
   * @return Tarif
   */
  public function setLoyerSemaine1($loyerSemaine1 = null)
  {
    $this->loyer_semaine1 = $loyerSemaine1;

    return $this;
  }

  /**
   * Get loyer_semaine1
   *
   * @return null|string
   */
  public function getLoyerSemaine1()
  {
    return $this->loyer_semaine1;
  }

  /**
   * Set loyer_mois
   *
   * @param null|string $loyerMois
   * @return Tarif
   */
  public function setLoyerMois($loyerMois = null)
  {
    $this->loyer_mois = $loyerMois;

    return $this;
  }

  /**
   * Get loyer_mois
   *
   * @return null|string
   */
  public function getLoyerMois()
  {
    return $this->loyer_mois;
  }

  /**
   * Set created_at
   *
   * @param \DateTime $createdAt
   * @return Tarif
   */
  public function setCreatedAt($createdAt)
  {
    $this->created_at = $createdAt;

    return $this;
  }

  /**
   * Get created_at
   *
   * @return \DateTime
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * Set updated_at
   *
   * @param \DateTime $updatedAt
   * @return Tarif
   */
  public function setUpdatedAt($updatedAt)
  {
    $this->updated_at = $updatedAt;

    return $this;
  }

  /**
   * Get updated_at
   *
   * @return \DateTime
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
   * Set appartement
   *
   * @param \Pat\CompteBundle\Entity\Appartement $appartement
   * @return Tarif
   */
  public function setAppartement(\Pat\CompteBundle\Entity\Appartement $appartement = null)
  {
    $this->appartement = $appartement;

    return $this;
  }

  /**
   * Get appartement
   *
   * @return \Pat\CompteBundle\Entity\Appartement
   */
  public function getAppartement()
  {
    return $this->appartement;
  }

  /**
   * Set loyer_nuit_2
   *
   * @param null|string $loyerNuit2
   * @return Tarif
   */
  public function setLoyerNuit2($loyerNuit2 = null)
  {
    $this->loyer_nuit_2 = $loyerNuit2;

    return $this;
  }

  /**
   * Get loyer_nuit_2
   *
   * @return null|string
   */
  public function getLoyerNuit2()
  {
    return $this->loyer_nuit_2;
  }

  /**
   * Set loyer_semaine1_2
   *
   * @param null|string $loyerSemaine12
   * @return Tarif
   */
  public function setLoyerSemaine12($loyerSemaine12 = null)
  {
    $this->loyer_semaine1_2 = $loyerSemaine12;

    return $this;
  }

  /**
   * Get loyer_semaine1_2
   *
   * @return null|string
   */
  public function getLoyerSemaine12()
  {
    return $this->loyer_semaine1_2;
  }

  /**
   * Set loyer_mois_2
   *
   * @param null|string $loyerMois2
   * @return Tarif
   */
  public function setLoyerMois2($loyerMois2 = null)
  {
    $this->loyer_mois_2 = $loyerMois2;

    return $this;
  }

  /**
   * Get loyer_mois_2
   *
   * @return null|string
   */
  public function getLoyerMois2()
  {
    return $this->loyer_mois_2;
  }

}
