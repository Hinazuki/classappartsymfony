<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class SiteTiers
{

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string")
   * @Assert\NotBlank()
   */
  private $name;

  /**
   * @var string
   *
   * @ORM\Column(name="description", type="text", nullable=true)
   */
  private $description;

  /**
   * @var string
   *
   * @ORM\Column(name="url", type="string", nullable=true)
   */
  private $url;

  /**
   * @var boolean
   *
   * @ORM\Column(name="auto_payment", type="boolean")
   */
  private $autoPayment = false;

  /**
   * @ORM\Column(type="datetime")
   */
  private $created_at;

  /**
   * @ORM\Column(type="datetime")
   */
  private $updated_at;

  public function __toString()
  {
    return $this->name;
  }

  /**
   * @ORM\PrePersist
   */
  public function setInitialValues()
  {
    $this->created_at = new \DateTime("now");
    $this->updated_at = new \DateTime("now");
  }

  /**
   * @ORM\PreUpdate
   */
  public function onUpdate()
  {
    $this->updated_at = new \DateTime("now");
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param string $name
   *
   * @return SiteTiers
   */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * @param string $description
   *
   * @return SiteTiers
   */
  public function setDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
   * @return string
   */
  public function getUrl()
  {
    return $this->url;
  }

  /**
   * @param string $url
   *
   * @return SiteTiers
   */
  public function setUrl($url)
  {
    $this->url = $url;

    return $this;
  }

  /**
   * @return bool
   */
  public function isAutoPayment()
  {
    return $this->autoPayment;
  }

  /**
   * @param bool $autoPayment
   *
   * @return SiteTiers
   */
  public function setAutoPayment($autoPayment)
  {
    $this->autoPayment = $autoPayment;

    return $this;
  }

  /**
   * Set created_at
   *
   * @param \DateTime $createdAt
   * @return Tarif
   */
  public function setCreatedAt($createdAt)
  {
    $this->created_at = $createdAt;

    return $this;
  }

  /**
   * Get created_at
   *
   * @return \DateTime
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * Set updated_at
   *
   * @param \DateTime $updatedAt
   * @return Tarif
   */
  public function setUpdatedAt($updatedAt)
  {
    $this->updated_at = $updatedAt;

    return $this;
  }

  /**
   * Get updated_at
   *
   * @return \DateTime
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

}
