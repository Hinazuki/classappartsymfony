<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Media
{

  /**
   * @ORM\ManyToOne(targetEntity="Appartement")
   *  @ORM\JoinColumn(referencedColumnName="id")
   */
  private $appartement;

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(columnDefinition="varchar(255) NULL")
   */
  private $titre;

  /**
   * @ORM\Column(type="string",length=250)
   */
  private $fichier;

  /**
   * @ORM\Column(type="integer",length=3)
   */
  private $tri;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set titre
   *
   * @param string $titre
   */
  public function setTitre($titre)
  {
    $this->titre = $titre;
  }

  /**
   * Get titre
   *
   * @return string
   */
  public function getTitre()
  {
    return $this->titre;
  }

  /**
   * Set fichier
   *
   * @param string $fichier
   */
  public function setFichier($fichier)
  {
    $this->fichier = $fichier;
  }

  /**
   * Get fichier
   *
   * @return string
   */
  public function getFichier()
  {
    return $this->fichier;
  }

  /**
   * Set tri
   *
   * @param integer $tri
   */
  public function setTri($tri)
  {
    $this->tri = $tri;
  }

  /**
   * Get tri
   *
   * @return integer
   */
  public function getTri()
  {
    return $this->tri;
  }

  /**
   * Set appartement
   *
   * @param Pat\CompteBundle\Entity\Appartement $appartement
   */
  public function setAppartement(\Pat\CompteBundle\Entity\Appartement $appartement)
  {
    $this->appartement = $appartement;
  }

  /**
   * Get appartement
   *
   * @return Pat\CompteBundle\Entity\Appartement
   */
  public function getAppartement()
  {
    return $this->appartement;
  }

  /**
   * Add appartement
   *
   * @param Pat\CompteBundle\Entity\Appartement $appartement
   */
  public function addAppartement(\Pat\CompteBundle\Entity\Appartement $appartement)
  {
    $this->appartement[] = $appartement;
  }

}
