<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints AS Assert;
use Pat\CompteBundle\Tools\PaymentTools;

/**
 * Payment
 *
 * @ORM\Table(name="payment")
 * @ORM\Entity(repositoryClass="Pat\CompteBundle\Repository\PaymentRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Payment
{

  /**
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Pat\UtilisateurBundle\Entity\Utilisateur")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="member_id", referencedColumnName="id", nullable=true)
   * })
   */
  private $member;

  /**
   * @ORM\ManyToOne(targetEntity="Pat\CompteBundle\Entity\Reservation", inversedBy="payments")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="resa_id", referencedColumnName="id", nullable=true)
   * })
   */
  private $reservation;

  /**
   * @var float
   *
   * @ORM\Column(name="amount", type="float", nullable=false)
   * @Assert\NotBlank
   */
  private $amount;

  /**
   * @ORM\Column(columnDefinition="varchar(8) DEFAULT 'CB'")
   */
  private $mode_paiement;

  /**
   * @ORM\Column(columnDefinition="longtext NULL")
   */
  private $payment_result;

  /**
   * @ORM\Column(columnDefinition="varchar(256) NULL")
   */
  private $payment_error;

  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", nullable=false)
   * @Assert\NotBlank
   */
  private $name;

  /**
   * @var string
   *
   * @ORM\Column(name="msg_content", type="text", nullable=true)
   */
  private $msgContent;

  /**
   * @var string
   *
   * @ORM\Column(name="token", type="string", nullable=true)
   */
  private $token;

  /**
   * @var integer
   *
   * @ORM\Column(name="status", type="integer",nullable= true)
   */
  private $status;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="created_at", type="datetime", nullable=true)
   */
  private $createdAt;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="payed_at", type="datetime", nullable=true)
   */
  private $payedAt;

  /**
   * @ORM\PrePersist
   */
  public function setInitialValues()
  {
    $this->token = md5('CApayment'.rand(0, 10000));
    $this->status = PaymentTools::CONSTANT_STATUS_WAIT;
//        $this->mode_paiement = "CB";

    $this->createdAt = new \DateTime(date("Y-m-d H:i:s"));
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set amount
   *
   * @param float $amount
   * @return Payment
   */
  public function setAmount($amount)
  {
    $this->amount = $amount;

    return $this;
  }

  /**
   * Get amount
   *
   * @return float
   */
  public function getAmount()
  {
    return $this->amount;
  }

  /**
   * Set name
   *
   * @param string $name
   * @return Payment
   */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * Get name
   *
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set msgContent
   *
   * @param string $msgContent
   * @return Payment
   */
  public function setMsgContent($msgContent)
  {
    $this->msgContent = $msgContent;

    return $this;
  }

  /**
   * Get msgContent
   *
   * @return string
   */
  public function getMsgContent()
  {
    return $this->msgContent;
  }

  /**
   * Set token
   *
   * @param string $token
   * @return Payment
   */
  public function setToken($token)
  {
    $this->token = $token;

    return $this;
  }

  /**
   * Get token
   *
   * @return string
   */
  public function getToken()
  {
    return $this->token;
  }

  /**
   * Set status
   *
   * @param integer $status
   * @return Payment
   */
  public function setStatus($status)
  {
    $this->status = $status;

    if ($status == PaymentTools::CONSTANT_STATUS_PAYED)
      $this->setPayedAt(new \DateTime(date("Y-m-d H:i:s")));

    return $this;
  }

  /**
   * Get status
   *
   * @return integer
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * Set createdAt
   *
   * @param \DateTime $createdAt
   * @return Payment
   */
  public function setCreatedAt($createdAt)
  {
    $this->createdAt = $createdAt;

    return $this;
  }

  /**
   * Get createdAt
   *
   * @return \DateTime
   */
  public function getCreatedAt()
  {
    return $this->createdAt;
  }

  /**
   * Set payedAt
   *
   * @param \DateTime $payedAt
   * @return Payment
   */
  public function setPayedAt($payedAt)
  {
    $this->payedAt = $payedAt;

    return $this;
  }

  /**
   * Get payedAt
   *
   * @return \DateTime
   */
  public function getPayedAt()
  {
    return $this->payedAt;
  }

  /**
   * Set member
   *
   * @param \Pat\UtilisateurBundle\Entity\Utilisateur $member
   * @return Payment
   */
  public function setMember(\Pat\UtilisateurBundle\Entity\Utilisateur $member = null)
  {
    $this->member = $member;

    return $this;
  }

  /**
   * Get member
   *
   * @return \Pat\UtilisateurBundle\Entity\Utilisateur
   */
  public function getMember()
  {
    return $this->member;
  }

  /**
   * Set mode_paiement
   *
   * @param string $modePaiement
   * @return Payment
   */
  public function setModePaiement($modePaiement)
  {
    $this->mode_paiement = $modePaiement;

    return $this;
  }

  /**
   * Get mode_paiement
   *
   * @return string
   */
  public function getModePaiement()
  {
    return $this->mode_paiement;
  }

  /**
   * Set payment_result
   *
   * @param string $paymentResult
   * @return Payment
   */
  public function setPaymentResult($paymentResult)
  {
    $this->payment_result = json_encode($paymentResult);

    return $this;
  }

  /**
   * Get payment_result
   *
   * @return string
   */
  public function getPaymentResult()
  {
    return json_decode($this->payment_result);
  }

  /**
   * Set payment_error
   *
   * @param string $paymentError
   * @return Payment
   */
  public function setPaymentError($paymentError)
  {
    $this->payment_error = $paymentError;

    return $this;
  }

  /**
   * Get payment_error
   *
   * @return string
   */
  public function getPaymentError()
  {
    return $this->payment_error;
  }

  /**
   * Set reservation
   *
   * @param \Pat\CompteBundle\Entity\Reservation $reservation
   * @return Payment
   */
  public function setReservation(\Pat\CompteBundle\Entity\Reservation $reservation = null)
  {
    $this->reservation = $reservation;

    return $this;
  }

  /**
   * Get reservation
   *
   * @return \Pat\CompteBundle\Entity\Reservation
   */
  public function getReservation()
  {
    return $this->reservation;
  }

}
