<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Departement
 *
 * @ORM\Table(name="departement")
 * @ORM\Entity
 */
class Departement
{

  /**
   * @var integer
   *
   * @ORM\Column(name="id_departement", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Pat\CompteBundle\Entity\Region", inversedBy="departements")
   * @ORM\JoinColumn(name="id_region", referencedColumnName="id_region")
   */
  private $region;

  /**
   * @var string
   *
   * @ORM\Column(name="code", type="string", length=3, nullable=false)
   */
  private $code;

  /**
   * @var string
   *
   * @ORM\Column(name="nom_departement", type="string", length=250, nullable=false)
   */
  private $nom;

  /**
   * @ORM\OneToMany(targetEntity="Ville", mappedBy="departement")
   */
  protected $villes;

  public function __construct()
  {
    $this->villes = new ArrayCollection();
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set code
   *
   * @param string $code
   * @return Departement
   */
  public function setCode($code)
  {
    $this->code = $code;

    return $this;
  }

  /**
   * Get code
   *
   * @return string
   */
  public function getCode()
  {
    return $this->code;
  }

  /**
   * Set nom
   *
   * @param string $nom
   * @return Departement
   */
  public function setNom($nom)
  {
    $this->nom = $nom;

    return $this;
  }

  /**
   * Get nom
   *
   * @return string
   */
  public function getNom()
  {
    return $this->nom;
  }

  /**
   * Set region
   *
   * @param \Pat\CompteBundle\Entity\Region $region
   * @return Departement
   */
  public function setRegion(\Pat\CompteBundle\Entity\Region $region = null)
  {
    $this->region = $region;

    return $this;
  }

  /**
   * Get region
   *
   * @return \Pat\CompteBundle\Entity\Region
   */
  public function getRegion()
  {
    return $this->region;
  }

  /**
   * Add villes
   *
   * @param \Pat\CompteBundle\Entity\Ville $villes
   * @return Departement
   */
  public function addVille(\Pat\CompteBundle\Entity\Ville $villes)
  {
    $this->villes[] = $villes;

    return $this;
  }

  /**
   * Remove villes
   *
   * @param \Pat\CompteBundle\Entity\Ville $villes
   */
  public function removeVille(\Pat\CompteBundle\Entity\Ville $villes)
  {
    $this->villes->removeElement($villes);
  }

  /**
   * Get villes
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getVilles()
  {
    return $this->villes;
  }

}
