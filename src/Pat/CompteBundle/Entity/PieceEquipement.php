<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="PieceMultiEquipement")
 */
class PieceEquipement
{

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Piece", inversedBy="multiEquipements")
   * @ORM\JoinColumn(referencedColumnName="id")
   */
  private $piece;

  /**
   * @ORM\ManyToOne(targetEntity="Equipement")
   */
  private $equipement;

  /**
   * @var int
   *
   * @ORM\Column(type="integer")
   */
  private $quantity;

  /**
   * @return mixed
   */
  public function getEquipement()
  {
    return $this->equipement;
  }

  /**
   * @param mixed $equipement
   *
   * @return PieceEquipement
   */
  public function setEquipement($equipement)
  {
    $this->equipement = $equipement;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getPiece()
  {
    return $this->piece;
  }

  /**
   * @param mixed $piece
   *
   * @return PieceEquipement
   */
  public function setPiece($piece)
  {
    $this->piece = $piece;

    return $this;
  }

  /**
   * @return int
   */
  public function getQuantity()
  {
    return $this->quantity;
  }

  /**
   * @param int $quantity
   *
   * @return PieceEquipement
   */
  public function setQuantity($quantity)
  {
    $this->quantity = $quantity;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

}
