<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Pat\CompteBundle\Repository\FactureRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Facture
{

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Reservation", inversedBy="factures")
   * @ORM\JoinColumn(name="reservation", referencedColumnName="id")
   */
  private $reservation;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private $number;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private $locale;

  /**
   * @var string
   *
   * @ORM\Column(type="text")
   */
  private $CaAddress;

  /**
   * @var string
   *
   * @ORM\Column(type="text")
   */
  private $header;

  /**
   * @var string
   *
   * @ORM\Column(type="text")
   */
  private $footer;

  /**
   * @var int
   *
   * @ORM\Column(type="integer")
   */
  private $nbAdultes;

  /**
   * @var string
   *
   * @ORM\Column(type="text")
   */
  private $calculValues;

  /**
   * @var float
   *
   * @ORM\Column(type="float", nullable=true)
   */
  private $promotion;

  /**
   * @var string
   *
   * @ORM\Column(type="text")
   */
  private $comment;

  /**
   * @var string
   *
   * @ORM\Column(type="string", nullable=true)
   */
  private $firstName;

  /**
   * @var string
   *
   * @ORM\Column(type="string", nullable=true)
   */
  private $lastName;

  /**
   * @var string
   *
   * @ORM\Column(type="string", nullable=true)
   */
  private $siret;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private $city;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private $address;

  /**
   * @var string
   *
   * @ORM\Column(type="string", nullable=true)
   */
  private $address2;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private $zipCode;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private $country;

  /**
   * @var string
   *
   * @ORM\Column(type="string", nullable=true)
   */
  private $phone;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private $email;

  /**
   * @var string
   *
   * @ORM\Column(type="text")
   */
  private $options;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private $depot;

  /**
   * @ORM\Column(type="datetime")
   */
  private $created_at;

  /**
   * @ORM\Column(type="datetime")
   */
  private $updated_at;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private $fileName;

  /**
   * @var string
   *
   * @ORM\Column(type="string", nullable=true)
   */
  private $avoirFileName;

  public function __toString()
  {
    return "".$this->id;
  }

  /**
   * @ORM\PrePersist
   */
  public function setInitialValues()
  {
    $this->created_at = new \DateTime("now");
    $this->updated_at = new \DateTime("now");
  }

  /**
   * @ORM\PreUpdate
   */
  public function onUpdate()
  {
    $this->updated_at = new \DateTime("now");
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set created_at
   *
   * @param \DateTime $createdAt
   *
   * @return Facture
   */
  public function setCreatedAt($createdAt)
  {
    $this->created_at = $createdAt;

    return $this;
  }

  /**
   * Get created_at
   *
   * @return \DateTime
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * Set updated_at
   *
   * @param \DateTime $updatedAt
   * @return Tarif
   */
  public function setUpdatedAt($updatedAt)
  {
    $this->updated_at = $updatedAt;

    return $this;
  }

  /**
   * Get updated_at
   *
   * @return \DateTime
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
   * Set reservation
   *
   * @param Reservation $reservation
   *
   * @return Facture
   */
  public function setReservation(Reservation $reservation)
  {
    $this->reservation = $reservation;

    return $this;
  }

  /**
   * Get reservation
   *
   * @return Reservation
   */
  public function getReservation()
  {
    return $this->reservation;
  }

  /**
   * @return string
   */
  public function getNumber()
  {
    return $this->number;
  }

  /**
   * @param string $number
   *
   * @return Facture
   */
  public function setNumber($number)
  {
    $this->number = $number;

    return $this;
  }

  /**
   * @return string
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * @param string $type
   *
   * @return Facture
   */
  public function setType($type)
  {
    $this->type = $type;

    return $this;
  }

  /**
   * @return string
   */
  public function getLocale()
  {
    return $this->locale;
  }

  /**
   * @param string $locale
   *
   * @return Facture
   */
  public function setLocale($locale)
  {
    $this->locale = $locale;

    return $this;
  }

  /**
   * @return string
   */
  public function getCaAddress()
  {
    return $this->CaAddress;
  }

  /**
   * @param string $CaAddress
   *
   * @return Facture
   */
  public function setCaAddress($CaAddress)
  {
    $this->CaAddress = $CaAddress;

    return $this;
  }

  /**
   * @return string
   */
  public function getHeader()
  {
    return $this->header;
  }

  /**
   * @param string $header
   *
   * @return Facture
   */
  public function setHeader($header)
  {
    $this->header = $header;

    return $this;
  }

  /**
   * @return string
   */
  public function getFooter()
  {
    return $this->footer;
  }

  /**
   * @param string $footer
   *
   * @return Facture
   */
  public function setFooter($footer)
  {
    $this->footer = $footer;

    return $this;
  }

  /**
   * @return int
   */
  public function getNbAdultes()
  {
    return $this->nbAdultes;
  }

  /**
   * @param int $nbAdultes
   *
   * @return Facture
   */
  public function setNbAdultes($nbAdultes)
  {
    $this->nbAdultes = $nbAdultes;

    return $this;
  }

  /**
   * @return string
   */
  public function getCalculValues()
  {
    return json_decode($this->calculValues, true);
  }

  /**
   * @param string $calculValues
   *
   * @return Facture
   */
  public function setCalculValues($calculValues)
  {
    $this->calculValues = json_encode($calculValues, true);

    return $this;
  }

  /**
   * @return float
   */
  public function getPromotion()
  {
    return $this->promotion;
  }

  /**
   * @param float $promotion
   *
   * @return Facture
   */
  public function setPromotion($promotion)
  {
    $this->promotion = $promotion;

    return $this;
  }

  /**
   * @return string
   */
  public function getComment()
  {
    return $this->comment;
  }

  /**
   * @param string $comment
   *
   * @return Facture
   */
  public function setComment($comment)
  {
    $this->comment = $comment;

    return $this;
  }

  /**
   * @return null|string
   */
  public function getFirstName()
  {
    return $this->firstName;
  }

  /**
   * @param null|string $firstName
   *
   * @return Facture
   */
  public function setFirstName($firstName = null)
  {
    $this->firstName = $firstName;

    return $this;
  }

  /**
   * @return null|string
   */
  public function getLastName()
  {
    return $this->lastName;
  }

  /**
   * @param null|string $lastName
   *
   * @return Facture
   */
  public function setLastName($lastName = null)
  {
    $this->lastName = $lastName;

    return $this;
  }

  /**
   * @return string
   */
  public function getSiret()
  {
    return $this->siret;
  }

  /**
   * @param string $siret
   *
   * @return Facture
   */
  public function setSiret($siret)
  {
    $this->siret = $siret;

    return $this;
  }

  /**
   * @return string
   */
  public function getCity()
  {
    return $this->city;
  }

  /**
   * @param string $city
   *
   * @return Facture
   */
  public function setCity($city)
  {
    $this->city = $city;

    return $this;
  }

  /**
   * @return string
   */
  public function getAddress()
  {
    return $this->address;
  }

  /**
   * @param string $address
   *
   * @return Facture
   */
  public function setAddress($address)
  {
    $this->address = $address;

    return $this;
  }

  /**
   * @return string
   */
  public function getAddress2()
  {
    return $this->address2;
  }

  /**
   * @param string $address2
   *
   * @return Facture
   */
  public function setAddress2($address2)
  {
    $this->address2 = $address2;

    return $this;
  }

  /**
   * @return string
   */
  public function getZipCode()
  {
    return $this->zipCode;
  }

  /**
   * @param string $zipCode
   *
   * @return Facture
   */
  public function setZipCode($zipCode)
  {
    $this->zipCode = $zipCode;

    return $this;
  }

  /**
   * @return string
   */
  public function getCountry()
  {
    return $this->country;
  }

  /**
   * @param string $country
   *
   * @return Facture
   */
  public function setCountry($country)
  {
    $this->country = $country;

    return $this;
  }

  /**
   * @return string
   */
  public function getPhone()
  {
    return $this->phone;
  }

  /**
   * @param string $phone
   *
   * @return Facture
   */
  public function setPhone($phone)
  {
    $this->phone = $phone;

    return $this;
  }

  /**
   * @return string
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * @param string $email
   *
   * @return Facture
   */
  public function setEmail($email)
  {
    $this->email = $email;

    return $this;
  }

  /**
   * @return array
   */
  public function getOptions()
  {
    return json_decode($this->options, true);
  }

  /**
   * @param array $options
   *
   * @return Facture
   */
  public function setOptions($options)
  {
    $this->options = json_encode($options, true);

    return $this;
  }

  /**
   * @return string
   */
  public function getDepot()
  {
    return $this->depot;
  }

  /**
   * @param string $depot
   *
   * @return Facture
   */
  public function setDepot($depot)
  {
    $this->depot = $depot;

    return $this;
  }

  /**
   * @return string
   */
  public function getFileName()
  {
    return $this->fileName;
  }

  /**
   * @param string $fileName
   *
   * @return Facture
   */
  public function setFileName($fileName)
  {
    $this->fileName = $fileName;

    return $this;
  }

  /**
   * @return string
   */
  public function getAvoirFileName()
  {
    return $this->avoirFileName;
  }

  /**
   * @param string $avoirFileName
   *
   * @return Facture
   */
  public function setAvoirFileName($avoirFileName)
  {
    $this->avoirFileName = $avoirFileName;

    return $this;
  }

  /**
   * Retourne le montant de la TVA.
   *
   * @param array $values
   * @return float
   */
  public function getVatAmount($values = array())
  {
    $values = empty($values) ? $this->getCalculValues() : $values;
    return ($values['tarif_net'] * $values['commission'] / 100) * $values['tva'] / 100;
  }

}
