<?php

namespace Pat\CompteBundle\Entity;

//use Pat\UtilisateurBundle\Entity\Utilisateur;
use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Pat\CompteBundle\Repository\ContenuRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Contenu
{

  /**
   * @ORM\ManyToOne(targetEntity="Pat\UtilisateurBundle\Entity\Utilisateur")
   * @ORM\JoinColumn(referencedColumnName="id")
   */
  private $utilisateur;

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $id_parent;

  /**
   * @ORM\Column(columnDefinition="varchar(20) NULL")
   */
  private $type;

  /**
   * @ORM\Column(columnDefinition="varchar(250) NULL")
   */
  private $titre;

  /**
   * @ORM\Column(columnDefinition="text NULL")
   */
  private $description;

  /**
   * @ORM\Column(columnDefinition="varchar(75) NULL")
   */
  private $url;

  /**
   * @ORM\Column(columnDefinition="varchar(100) NULL")
   */
  private $meta_titre;

  /**
   * @ORM\Column(columnDefinition="varchar(250) NULL")
   */
  private $meta_description;

  /**
   * @ORM\Column(columnDefinition="int(11) NULL")
   */
  private $tri;

  /**
   * @ORM\Column(columnDefinition="varchar(5) NOT NULL DEFAULT 'fr'")
   */
  private $langue;

  /**
   * @ORM\Column(columnDefinition="int(1) NOT NULL DEFAULT '1'")
   */
  private $affiche_menu;

  /**
   * @ORM\Column(columnDefinition="int(1) NOT NULL DEFAULT '0'")
   */
  private $statut;

  /**
   * @ORM\Column(type="datetime")
   */
  private $created_at;

  /**
   * @ORM\Column(type="datetime")
   */
  private $updated_at;

  /**
   * @ORM\Column(columnDefinition="int(1) NOT NULL DEFAULT '0'")
   */
  private $on_sidebar;

  /**
   * @ORM\PrePersist
   */
  public function setInitialValues()
  {
    $this->created_at = new \DateTime("now");
    $this->updated_at = new \DateTime("now");
  }

  /**
   * @ORM\PreUpdate
   */
  public function onUpdate()
  {
    $this->updated_at = new \DateTime("now");
  }

  public function getTitreContenu()
  {
    return $this->getTitre();
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set parent
   *
   * @param string $parent
   */
  public function setParent($parent)
  {
    $this->parent = $parent;
  }

  /**
   * Get parent
   *
   * @return string
   */
  public function getParent()
  {
    return $this->parent;
  }

  /**
   * Set type
   *
   * @param string $type
   */
  public function setType($type)
  {
    $this->type = $type;
  }

  /**
   * Get type
   *
   * @return string
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Set titre
   *
   * @param string $titre
   */
  public function setTitre($titre)
  {
    $this->titre = $titre;
  }

  /**
   * Get titre
   *
   * @return string
   */
  public function getTitre()
  {
    return $this->titre;
  }

  /**
   * Set description
   *
   * @param string $description
   */
  public function setDescription($description)
  {
    $this->description = $description;
  }

  /**
   * Get description
   *
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Set url
   *
   * @param string $url
   */
  public function setUrl($url)
  {
    $this->url = $url;
  }

  /**
   * Get url
   *
   * @return string
   */
  public function getUrl()
  {
    return $this->url;
  }

  /**
   * Set meta_titre
   *
   * @param string $metaTitre
   */
  public function setMetaTitre($metaTitre)
  {
    $this->meta_titre = $metaTitre;
  }

  /**
   * Get meta_titre
   *
   * @return string
   */
  public function getMetaTitre()
  {
    return $this->meta_titre;
  }

  /**
   * Set meta_description
   *
   * @param string $metaDescription
   */
  public function setMetaDescription($metaDescription)
  {
    $this->meta_description = $metaDescription;
  }

  /**
   * Get meta_description
   *
   * @return string
   */
  public function getMetaDescription()
  {
    return $this->meta_description;
  }

  /**
   * Set tri
   *
   * @param string $tri
   */
  public function setTri($tri)
  {
    $this->tri = $tri;
  }

  /**
   * Get tri
   *
   * @return string
   */
  public function getTri()
  {
    return $this->tri;
  }

  /**
   * Set statut
   *
   * @param string $statut
   */
  public function setStatut($statut)
  {
    $this->statut = $statut;
  }

  /**
   * Get statut
   *
   * @return string
   */
  public function getStatut()
  {
    return $this->statut;
  }

  /**
   * Set created_at
   *
   * @param datetime $createdAt
   */
  public function setCreatedAt($createdAt)
  {
    $this->created_at = $createdAt;
  }

  /**
   * Get created_at
   *
   * @return datetime
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * Set updated_at
   *
   * @param datetime $updatedAt
   */
  public function setUpdatedAt($updatedAt)
  {
    $this->updated_at = $updatedAt;
  }

  /**
   * Get updated_at
   *
   * @return datetime
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
   * Set utilisateur
   *
   * @param Pat\UtilisateurBundle\Entity\Utilisateur $utilisateur
   */
  public function setUtilisateur(\Pat\UtilisateurBundle\Entity\Utilisateur $utilisateur)
  {
    $this->utilisateur = $utilisateur;
  }

  /**
   * Get utilisateur
   *
   * @return Pat\UtilisateurBundle\Entity\Utilisateur
   */
  public function getUtilisateur()
  {
    return $this->utilisateur;
  }

  /**
   * Set id_parent
   *
   * @param string $idParent
   */
  public function setIdParent($idParent)
  {
    $this->id_parent = $idParent;
  }

  /**
   * Get id_parent
   *
   * @return string
   */
  public function getIdParent()
  {
    return $this->id_parent;
  }

  /**
   * Set affiche_menu
   *
   * @param string $afficheMenu
   */
  public function setAfficheMenu($afficheMenu)
  {
    $this->affiche_menu = $afficheMenu;
  }

  /**
   * Get affiche_menu
   *
   * @return string
   */
  public function getAfficheMenu()
  {
    return $this->affiche_menu;
  }

  /**
   * Set langue
   *
   * @param string $langue
   */
  public function setLangue($langue)
  {
    $this->langue = $langue;
  }

  /**
   * Get langue
   *
   * @return string
   */
  public function getLangue()
  {
    return $this->langue;
  }

  /**
   * Set mini_liste
   *
   * @param string $miniListe
   */
  public function setMiniListe($miniListe)
  {
    $this->mini_liste = $miniListe;
  }

  /**
   * Get mini_liste
   *
   * @return string
   */
  public function getMiniListe()
  {
    return $this->mini_liste;
  }

  /**
   * Set on_sidebar
   *
   * @param string $onSidebar
   */
  public function setOnSidebar($onSidebar)
  {
    $this->on_sidebar = $onSidebar;
  }

  /**
   * Get on_sidebar
   *
   * @return string
   */
  public function getOnSidebar()
  {
    return $this->on_sidebar;
  }

}
