<?php

namespace Pat\CompteBundle\Entity;

//use Pat\UtilisateurBundle\Entity\Utilisateur;
use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Pat\CompteBundle\Repository\CalendrierRepository")
 */
class Calendrier
{

  /**
   * @var \Pat\CompteBundle\Entity\Appartement
   *
   * @ORM\ManyToOne(targetEntity="Pat\CompteBundle\Entity\Appartement")
   * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
   */
  private $appartement;

  /**
   * @ORM\ManyToOne(targetEntity="Pat\UtilisateurBundle\Entity\Utilisateur")
   * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
   */
  private $utilisateur;

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @var integer
   *
   * @ORM\Column(name="appartement_id", type="integer", nullable=true)
   */
  private $appartement_id;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL")
   */
  private $date_debut;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="heure_debut", type="time", nullable=true)
   */
  private $heure_debut;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="heure_fin", type="time", nullable=true)
   */
  private $heure_fin;

  /**
   * @ORM\Column(columnDefinition="varchar(10) NULL")
   */
  private $date_fin;

  /**
   * @ORM\Column(columnDefinition="varchar(250) NULL")
   */
  private $intitule;

  /**
   * @ORM\Column(columnDefinition="varchar(250) NULL")
   */
  private $commentaire;

  /**
   * @ORM\Column(type="integer", nullable=true)
   */
  private $status;

  /**
   * @ORM\Column(type="datetime")
   */
  private $created_at;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get appartement_id
   *
   * @return integer
   */
  public function getAppartementId()
  {
    return $this->appartement_id;
  }

  /**
   * Set commentaire
   *
   * @param string $commentaire
   */
  public function setCommentaire($commentaire)
  {
    $this->commentaire = $commentaire;
  }

  /**
   * Get commentaire
   *
   * @return string
   */
  public function getCommentaire()
  {
    return $this->commentaire;
  }

  /**
   * Set created_at
   *
   * @param datetime $createdAt
   */
  public function setCreatedAt($createdAt)
  {
    $this->created_at = $createdAt;
  }

  /**
   * Get created_at
   *
   * @return datetime
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * Set appartement
   *
   * @param Pat\CompteBundle\Entity\Appartement $appartement
   */
  public function setAppartement(\Pat\CompteBundle\Entity\Appartement $appartement)
  {
    $this->appartement = $appartement;
  }

  /**
   * Get appartement
   *
   * @return Pat\CompteBundle\Entity\Appartement
   */
  public function getAppartement()
  {
    return $this->appartement;
  }

  /**
   * Set utilisateur
   *
   * @param Pat\UtilisateurBundle\Entity\Utilisateur $utilisateur
   */
  public function setUtilisateur(\Pat\UtilisateurBundle\Entity\Utilisateur $utilisateur)
  {
    $this->utilisateur = $utilisateur;
  }

  /**
   * Get utilisateur
   *
   * @return Pat\UtilisateurBundle\Entity\Utilisateur
   */
  public function getUtilisateur()
  {
    return $this->utilisateur;
  }

  /**
   * Set intitule
   *
   * @param string $intitule
   */
  public function setIntitule($intitule)
  {
    $this->intitule = $intitule;
  }

  /**
   * Get intitule
   *
   * @return string
   */
  public function getIntitule()
  {
    return $this->intitule;
  }

  /**
   * Set date_debut
   *
   * @param string $dateDebut
   * @return Calendrier
   */
  public function setDateDebut($dateDebut)
  {
    $this->date_debut = $dateDebut;

    return $this;
  }

  /**
   * Get date_debut
   *
   * @return string
   */
  public function getDateDebut()
  {
    return $this->date_debut;
  }

  /**
   * @return \DateTime
   */
  public function getHeureDebut()
  {
    return $this->heure_debut;
  }

  /**
   * @param \DateTime $heure_debut
   *
   * @return Calendrier
   */
  public function setHeureDebut($heure_debut)
  {
    $this->heure_debut = $heure_debut;

    return $this;
  }

  /**
   * Set date_fin
   *
   * @param string $dateFin
   * @return Calendrier
   */
  public function setDateFin($dateFin)
  {
    $this->date_fin = $dateFin;

    return $this;
  }

  /**
   * @return \DateTime
   */
  public function getHeureFin()
  {
    return $this->heure_fin;
  }

  /**
   * @param \DateTime $heure_fin
   *
   * @return Calendrier
   */
  public function setHeureFin($heure_fin)
  {
    $this->heure_fin = $heure_fin;

    return $this;
  }

  /**
   * Get date_fin
   *
   * @return string
   */
  public function getDateFin()
  {
    return $this->date_fin;
  }

  /**
   * Set status
   *
   * @param integer $status
   * @return Calendrier
   */
  public function setStatus($status)
  {
    $this->status = $status;

    return $this;
  }

  /**
   * Get status
   *
   * @return integer
   */
  public function getStatus()
  {
    return $this->status;
  }

}
