<?php

namespace Pat\CompteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Pat\CompteBundle\Repository\FormulaireRepository")
 */
class Formulaire
{

  /**
   * @ORM\GeneratedValue
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NOT NULL")
   */
  private $type;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NOT NULL")
   * @Assert\NotBlank(message = "Veuillez saisir votre nom")
   * @Assert\Length(
   *      min = "2",
   *      max = "50"
   * )
   */
  private $nom;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NOT NULL")
   * @Assert\NotBlank(message = "Veuillez saisir votre prenom")
   * @Assert\Length(
   *      min = "2",
   *      max = "50"
   * )
   */
  private $prenom;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   * @Assert\Length(
   *      max = "50"
   * )
   */
  private $societe;

  /**
   * @ORM\Column(columnDefinition="varchar(50) NULL")
   * @Assert\Length(
   *      max = "50"
   * )
   */
  private $fonction;

  private $objet;

  /**
   * @ORM\Column(columnDefinition="varchar(150) NOT NULL")
   * @Assert\NotBlank(message = "Veuillez saisir votre email")
   * @Assert\Regex("#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#")
   * @Assert\Length(
   *      max = "150"
   * )
   */
  private $email;

  /**
   * @ORM\Column(columnDefinition="varchar(20) NOT NULL")
   * @Assert\NotBlank(message = "Veuillez saisir un numéro de téléphone")
   * @Assert\Length(
   *      min = "10",
   *      max = "20"
   * )
   */
  private $tel;

  /**
   * @ORM\Column(columnDefinition="text NOT NULL")
   * @Assert\NotBlank(message = "Veuillez saisir votre message")
   * @Assert\Length(
   *      min = "1",
   *      max = "1000"
   * )
   */
  private $message;

  /**
   * @ORM\Column(columnDefinition="datetime NOT NULL")
   */
  private $created_at;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set type
   *
   * @param string $type
   */
  public function setType($type)
  {
    $this->type = $type;
  }

  /**
   * Get type
   *
   * @return string
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Set nom
   *
   * @param string $nom
   */
  public function setNom($nom)
  {
    $this->nom = $nom;
  }

  /**
   * Get nom
   *
   * @return string
   */
  public function getNom()
  {
    return $this->nom;
  }

  /**
   * Set prenom
   *
   * @param string $prenom
   */
  public function setPrenom($prenom)
  {
    $this->prenom = $prenom;
  }

  /**
   * Get prenom
   *
   * @return string
   */
  public function getPrenom()
  {
    return $this->prenom;
  }

  /**
   * Set societe
   *
   * @param string $societe
   */
  public function setSociete($societe)
  {
    $this->societe = $societe;
  }

  /**
   * Get societe
   *
   * @return string
   */
  public function getSociete()
  {
    return $this->societe;
  }

  /**
   * Set fonction
   *
   * @param string $fonction
   */
  public function setFonction($fonction)
  {
    $this->fonction = $fonction;
  }

  /**
   * Get fonction
   *
   * @return string
   */
  public function getFonction()
  {
    return $this->fonction;
  }

  /**
   * Set email
   *
   * @param string $email
   */
  public function setEmail($email)
  {
    $this->email = $email;
  }

  /**
   * Get email
   *
   * @return string
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * Set tel
   *
   * @param string $tel
   */
  public function setTel($tel)
  {
    $this->tel = $tel;
  }

  /**
   * Get tel
   *
   * @return string
   */
  public function getTel()
  {
    return $this->tel;
  }

  public function getObjet()
  {
    return $this->objet;
  }

  public function setObjet($objet)
  {
    $this->objet = $objet;
  }

  /**
   * Set type_societe
   *
   * @param string $typeSociete
   */
  public function setTypeSociete($typeSociete)
  {
    $this->type_societe = $typeSociete;
  }

  /**
   * Get type_societe
   *
   * @return string
   */
  public function getTypeSociete()
  {
    return $this->type_societe;
  }

  /**
   * Set message
   *
   * @param string $message
   */
  public function setMessage($message)
  {
    $this->message = $message;
  }

  /**
   * Get message
   *
   * @return string
   */
  public function getMessage()
  {
    return $this->message;
  }

  /**
   * Set created_at
   *
   * @param string $createdAt
   */
  public function setCreatedAt($createdAt)
  {
    $this->created_at = $createdAt;
  }

  /**
   * Get created_at
   *
   * @return string
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

}
