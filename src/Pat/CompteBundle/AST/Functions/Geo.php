<?php

namespace Pat\CompteBundle\AST\Functions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\Lexer;

// Cette classe créé une fonction spéciale pour le DQL
// A partir d'une base de donnée comportant des villes
// Elle permet de donner toutes les distances d'une ville vers toutes les autres
// On peut l'utiliser comme cela :

/*
  $qb = $this->_em->createQueryBuilder()
  ->from('Pat\CompteBundle\Entity\Ville', 'v') // On appelle la base de la ville
  ->select('v.nom') // Les données que l'on souhaite récupérer sur chaque ville
  ->where('Geo(v.latitude = :latitude, v.longitude = :longitude) < 5') // "v" correspond à la ville de la base qui est testée et ":" appelle les parametres ci dessous
  ->setParameter('latitude', $info->getLatitude()) // Latitude de la ville de départ
  ->setParameter('longitude', $info->getLongitude()); // Longitude de la ville de départ
 */
class Geo extends FunctionNode
{

  /**
   * @var \Doctrine\ORM\Query\AST\ComparisonExpression
   */
  private $latitude;

  /**
   * @var \Doctrine\ORM\Query\AST\ComparisonExpression
   */
  private $longitude;

  /**
   * Parse DQL Function
   *
   * @param Parser $parser
   */
  public function parse(Parser $parser)
  {
    $parser->match(Lexer::T_IDENTIFIER);
    $parser->match(Lexer::T_OPEN_PARENTHESIS);
    $this->latitude = $parser->ComparisonExpression();
    $parser->match(Lexer::T_COMMA);
    $this->longitude = $parser->ComparisonExpression();
    $parser->match(Lexer::T_CLOSE_PARENTHESIS);
  }

  /**
   * Get SQL
   *
   * @param SqlWalker $sqlWalker
   * @return string
   */
  public function getSql(SqlWalker $sqlWalker)
  {
    // Formule qui renvoie la distance entre deux Latitude/Longitude
    return sprintf(
      '(6366*ACOS(COS(RADIANS(%s))*COS(RADIANS(latitude))*COS(RADIANS(longitude)-RADIANS(%s))+SIN(RADIANS(%s))*SIN(RADIANS(latitude))))', $this->latitude->rightExpression->dispatch($sqlWalker), $this->longitude->rightExpression->dispatch($sqlWalker), $this->latitude->rightExpression->dispatch($sqlWalker)
    );
  }

}
