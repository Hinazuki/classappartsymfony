<?php

namespace Pat\FrontBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class DemandeDevis
{

  /**
   * @Assert\NotBlank()
   */
  private $nom;

  /**
   * @Assert\NotBlank()
   */
  private $prenom;

  /**
   * @Assert\NotBlank()
   * @Assert\Regex("#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#")
   */
  private $email;

  /**
   * @Assert\NotBlank()
   */
  private $telephone;

  /**
   * @Assert\NotBlank()
   */
  private $typeEntity;

  /**
   */
  private $typeAppartement;

  /**
   */
  private $nbPieces;

  /**
   */
  private $quartier;

  /**
   */
  private $arrivee;

  /**
   */
  private $depart;

  /**
   */
  private $nbAdultes;

  /**
   */
  private $nbPersonnes;

  /**
   */
  private $nbEnfants;

  /**
   * @var int
   */
  private $nbLitSimple;

  /**
   * @var int
   */
  private $nbLitDouble;

  /**
   * @var int
   */
  private $nbCanapeConvertible;

  /**
   */
  private $message;

  /**
   */
  private $societe;

  /**
   */
  private $fonction;

  /**
   */
  private $nbAppartements;

  public function getNom()
  {
    return $this->nom;
  }

  public function setNom($nom)
  {
    $this->nom = $nom;
  }

  public function getPrenom()
  {
    return $this->prenom;
  }

  public function setPrenom($prenom)
  {
    $this->prenom = $prenom;
  }

  public function getEmail()
  {
    return $this->email;
  }

  public function setEmail($email)
  {
    $this->email = $email;
  }

  public function getTelephone()
  {
    return $this->telephone;
  }

  public function setTelephone($telephone)
  {
    $this->telephone = $telephone;
  }

  public function getTypeEntity()
  {
    return $this->typeEntity;
  }

  public function setTypeEntity($typeEntity)
  {
    $this->typeEntity = $typeEntity;
  }

  public function getTypeAppartement()
  {
    return $this->typeAppartement;
  }

  public function setTypeAppartement($typeAppartement)
  {
    $this->typeAppartement = $typeAppartement;
  }

  public function getNbPieces()
  {
    return $this->nbPieces;
  }

  public function setNbPieces($nbPieces)
  {
    $this->nbPieces = $nbPieces;
  }

  public function getQuartier()
  {
    return $this->quartier;
  }

  public function setQuartier($quartier)
  {
    $this->quartier = $quartier;
  }

  public function getArrivee()
  {
    return $this->arrivee;
  }

  public function setArrivee($arrivee)
  {
    $this->arrivee = $arrivee;
  }

  public function getDepart()
  {
    return $this->depart;
  }

  public function setDepart($depart)
  {
    $this->depart = $depart;
  }

  public function getNbAdultes()
  {
    return $this->nbAdultes;
  }

  public function setNbAdultes($nbAdultes)
  {
    $this->nbAdultes = $nbAdultes;
  }

  public function getNbEnfants()
  {
    return $this->nbEnfants;
  }

  public function setNbEnfants($nbEnfants)
  {
    $this->nbEnfants = $nbEnfants;
  }

  /**
   * @return int
   */
  public function getNbLitSimple()
  {
    return $this->nbLitSimple;
  }

  /**
   * @param int $nbLitSimple
   *
   * @return DemandeDevis
   */
  public function setNbLitSimple($nbLitSimple)
  {
    $this->nbLitSimple = $nbLitSimple;

    return $this;
  }

  /**
   * @return int
   */
  public function getNbLitDouble()
  {
    return $this->nbLitDouble;
  }

  /**
   * @param int $nbLitDouble
   *
   * @return DemandeDevis
   */
  public function setNbLitDouble($nbLitDouble)
  {
    $this->nbLitDouble = $nbLitDouble;

    return $this;
  }

  /**
   * @return int
   */
  public function getNbCanapeConvertible()
  {
    return $this->nbCanapeConvertible;
  }

  /**
   * @param int $nbCanapeConvertible
   *
   * @return DemandeDevis
   */
  public function setNbCanapeConvertible($nbCanapeConvertible)
  {
    $this->nbCanapeConvertible = $nbCanapeConvertible;

    return $this;
  }

  public function getMessage()
  {
    return $this->message;
  }

  public function setMessage($message)
  {
    $this->message = $message;
  }

  public function getSociete()
  {
    return $this->societe;
  }

  public function setSociete($societe)
  {
    $this->societe = $societe;
  }

  public function getFonction()
  {
    return $this->fonction;
  }

  public function setFonction($fonction)
  {
    $this->fonction = $fonction;
  }

  public function getNbAppartements()
  {
    return $this->nbAppartements;
  }

  public function setNbAppartements($nbAppartements)
  {
    $this->nbAppartements = $nbAppartements;
  }

  public function getNbPersonnes()
  {
    return $this->nbPersonnes;
  }

  public function setNbPersonnes($nbPersonnes)
  {
    $this->nbPersonnes = $nbPersonnes;
  }

}
