<?php

namespace Pat\FrontBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class ProposerBien
{

  /**
   * @Assert\NotBlank()
   */
  private $nom;

  /**
   * @Assert\NotBlank()
   */
  private $prenom;

  /**
   * @Assert\NotBlank()
   * @Assert\Regex("#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#")
   */
  private $email;

  /**
   * @Assert\NotBlank()
   */
  private $telephone;

  /**
   */
  private $nbAppartements;

  /**
   */
  private $surface;

  /**
   */
  private $isMeuble;

  /**
   */
  private $nbMeuble;

  /**
   */
  private $isLoue;

  /**
   */
  private $nbLoue;

  /**
   */
  private $quartier;

  /**
   */
  private $disponibilite;

  /**
   */
  private $formule;

  /**
   */
  private $connaissance;

  public function getNom()
  {
    return $this->nom;
  }

  public function setNom($nom)
  {
    $this->nom = $nom;
  }

  public function getPrenom()
  {
    return $this->prenom;
  }

  public function setPrenom($prenom)
  {
    $this->prenom = $prenom;
  }

  public function getEmail()
  {
    return $this->email;
  }

  public function setEmail($email)
  {
    $this->email = $email;
  }

  public function getTelephone()
  {
    return $this->telephone;
  }

  public function setTelephone($telephone)
  {
    $this->telephone = $telephone;
  }

  public function getNbAppartements()
  {
    return $this->nbAppartements;
  }

  public function setNbAppartements($nbAppartements)
  {
    $this->nbAppartements = $nbAppartements;
  }

  public function getSurface()
  {
    return $this->surface;
  }

  public function setSurface($surface)
  {
    $this->surface = $surface;
  }

  public function getIsMeuble()
  {
    return $this->isMeuble;
  }

  public function setIsMeuble($isMeuble)
  {
    $this->isMeuble = $isMeuble;
  }

  public function getQuartier()
  {
    return $this->quartier;
  }

  public function setQuartier($quartier)
  {
    $this->quartier = $quartier;
  }

  public function getDisponibilite()
  {
    return $this->disponibilite;
  }

  public function setDisponibilite($disponibilite)
  {
    $this->disponibilite = $disponibilite;
  }

  public function getFormule()
  {
    return $this->formule;
  }

  public function setFormule($formule)
  {
    $this->formule = $formule;
  }

  public function getConnaissance()
  {
    return $this->connaissance;
  }

  public function setConnaissance($connaissance)
  {
    $this->connaissance = $connaissance;
  }

  public function getNbMeuble()
  {
    return $this->nbMeuble;
  }

  public function setNbMeuble($nbMeuble)
  {
    $this->nbMeuble = $nbMeuble;
  }

  public function getIsLoue()
  {
    return $this->isLoue;
  }

  public function setIsLoue($isLoue)
  {
    $this->isLoue = $isLoue;
  }

  public function getNbLoue()
  {
    return $this->nbLoue;
  }

  public function setNbLoue($nbLoue)
  {
    $this->nbLoue = $nbLoue;
  }

}
