<?php

namespace Pat\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PaymentModeForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('mode_paiement', 'choice', array(
        'label' => "Choix du mode de paiement :",
        'choices' => array(
          'CB' => 'Carte bancaire',
          /* 'cheque' => 'Chèque', */
          'virement' => 'Virement bancaire'),
        'required' => true,
        'multiple' => false,
        'expanded' => true
    ));
  }

  public function getName()
  {
    return 'frontpaymentmode';
  }

}
