<?php

namespace Pat\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class OptionsForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
//            ->add('option_roses', 'choice', array(
//                    'label' => "",
//                    "required" => false,
//                    'choices' => array('1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'9', '10'=>'10')
//                ))
//
//            ->add('option_champagne', 'choice', array(
//                    'label' => "",
//                    "required" => false,
//                    'choices' => array('1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'9', '10'=>'10')
//                ))
//
//            ->add('option_menage_supp', 'choice', array(
//                    'label' => "",
//                    "required" => false,
//                    'choices' => array('1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'9', '10'=>'10')
//                ))
//
//            ->add('option_lit_bebe', 'checkbox', array(
//                    'label' => '',
//                    'required'  => false,
//                ))
//
//            ->add('option_taxi', 'checkbox', array(
//                    'label' => '',
//                    'required'  => false,
//                ))
//
//            ->add('option_frigo', 'checkbox', array(
//                    'label' => '',
//                    'required'  => false,
//                ))
      ->add('option_supplementaires', 'checkbox', array(
        'label' => ' ',
        'required' => false,
      ))
    ;
  }

  public function getName()
  {
    return 'frontoptionsreservation';
  }

}
