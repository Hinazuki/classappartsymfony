<?php

namespace Pat\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Pat\CompteBundle\Repository\VilleRepository;
use Symfony\Component\Validator\Constraints\EqualTo;

class ProposerBienForm extends AbstractType
{

  protected $translator;
  protected $rand1;
  protected $rand2;
  protected $question;

  function __construct(Translator $t, $rand1, $rand2)
  {
    $this->translator = $t;
    $this->rand1 = $rand1;
    $this->rand2 = $rand2;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    $builder
      //LOCALISATION
      ->add('nom', 'text', array('label' => "Nom : *", "required" => true, 'max_length' => 50))
      ->add('prenom', 'text', array('label' => "Prénom : *", "required" => true, 'max_length' => 50))
      ->add('email', 'text', array('label' => "Email : *", "required" => true, 'max_length' => 50))
      ->add('telephone', 'text', array('label' => "Tel : *", "required" => true, 'max_length' => 20))
      ->add('nbAppartements', 'text', array('label' => "Nombre d'appartement(s) à louer :", 'max_length' => 2, 'required' => false))
      ->add('surface', 'text', array('label' => "Surface moyenne :", 'required' => false))
      ->add('isMeuble', 'choice', array(
        'label' => "Meublé :",
        'expanded' => true,
        'multiple' => false,
        'required' => false,
        'choices' => array(
          'Non' => 'Non',
          'Oui' => 'Oui'),
        "data" => 'Oui',
      ))
      ->add('nbMeuble', 'text', array('label' => "nombre :", 'max_length' => 2, 'required' => false))



      //TYPES DE BIENS
      ->add('isLoue', 'choice', array(
        'label' => "Déjà en location :",
        'choices' => array(
          'Non' => 'Non',
          'Oui' => 'Oui'),
        'expanded' => true,
        'required' => false,
        "data" => 'Oui',
        'multiple' => false))
      ->add('nbLoue', 'text', array('label' => "nombre :", 'max_length' => 2, 'required' => false))
      ->add('quartier', 'entity', array(
        'label' => "Quartier :",
        'required' => false,
        'empty_value' => 'Tous les quartiers',
        'expanded' => false,
        'multiple' => false,
        'class' => 'Pat\CompteBundle\Entity\Ville',
        'property' => 'nom',
        'query_builder' => function(VilleRepository $er) {
          return $er->getVillesBySearchQuery('Montpellier', '');
        }))
      ->add('disponibilite', 'choice', array(
        'label' => "Disponibilité à la location :",
        'expanded' => true,
        'multiple' => false,
        'required' => false,
        'choices' => array(
          'Toute l\'année' => 'Toute l\'année',
          'Quelques mois seulement' => 'Quelques mois seulement')
      ))
      ->add('formule', 'choice', array(
        'label' => "Formule choisie :",
        'required' => false,
        'choices' => array(
          'Standard' => 'Standard (1)',
          'Premium' => 'Premium (2)'),
        'expanded' => true,
        'multiple' => false))
      ->add('connaissance', 'choice', array(
        'label' => "Vous avez connu Class Appart par :",
        'choices' => array(
          'Bouche à oreille' => 'Bouche à oreille',
          'Agence immobilière' => 'Agence immobilière',
          'Presse' => 'Presse',
          'Internet' => 'Internet'),
        'expanded' => true,
        'required' => false,
        'multiple' => true))
    ;

    $this->question = sprintf(
      $this->translator->trans('Quelle est la somme de %s + %s ?'), $this->rand1, $this->rand2
    );
    $builder
      ->add('human', 'text', array(
        'label' => $this->translator->trans('Vous êtes humain : *'),
        'required' => true,
        'mapped' => false,
        'constraints' => array(
          new EqualTo(array(
            'value' => $this->rand1 + $this->rand2,
            'message' => $this->translator->trans('La somme ne correspond pas.'),
            ))
        )
    ));
  }

  public function getName()
  {
    return 'frontformulaireproposerbien';
  }

  public function getQuestion()
  {
    return $this->question;
  }

}
