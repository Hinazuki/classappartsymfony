<?php

namespace Pat\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\EqualTo;

class InscriptionLocataireForm extends AbstractType
{

  protected $rand1;
  protected $rand2;
  protected $question;

  public function __construct($rand1 = null, $rand2 = null)
  {
    $this->rand1 = $rand1;
    $this->rand2 = $rand2;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    // Add custom fields
    $builder
      ->add('civilite', 'choice', array(
        'label' => "Civilité :",
        'choices' => array(
          'M.' => 'M.',
          'Mlle' => 'Mlle',
          'Mme' => 'Mme',
        ),
        'multiple' => false,
      ))
      ->add('prenom', 'text', array('label' => "Prénom : *", 'required' => true))
      ->add('nom', 'text', array('label' => "Nom : *", 'required' => true))
      ->add('societe', 'text', array('label' => "Société :", 'required' => false))
      ->add('date_naissance', 'birthday', array(
        'label' => "Date de naissance :",
        'widget' => 'choice',
        'years' => range(date('Y') - 18, 1900),
        'format' => 'dd / MM / yyyy',
        'empty_value' => '',
        'required' => false))
      ->add('nationalite', 'text', array('label' => "Nationalité : *", 'required' => true))
      ->add('langue_parle', 'choice', array(
        'label' => "Langue préférée :",
        'choices' => array(
          'Français' => 'Français',
          'Anglais' => 'Anglais',
          'Allemand' => 'Allemand',
          'Espagnol' => 'Espagnol',
          'Italien' => 'Italien',
          'Russe' => 'Russe',
          'Chinois' => 'Chinois',
        ),
        'multiple' => false,
      ))
      ->add('adresse', 'text', array('label' => "Adresse : *", 'required' => true))
      ->add('adresse2', 'text', array('label' => "Adresse suite :", 'required' => false))
      ->add('code_postal', 'text', array('label' => "Code postal :", 'required' => true))
      ->add('ville', 'text', array('label' => "Ville : *", 'required' => true))
      ->add('pays', 'text', array('label' => "Pays : *", 'required' => true))
      ->add('telephone', 'text', array('label' => "Téléphone : *", 'required' => true))
      ->add('mobile', 'text', array('label' => "Mobile Mr :", 'required' => false))
      ->add('mobile2', 'text', array('label' => "Mobile Mme :", 'required' => false))
//            ->add('mails_second', 'textarea', array(
//                    'label' => "Adresses e-mail secondaires :",
//                    'required' => false
//                ))
      ->add('second_email', 'text', array(
        'label' => "Adresse e-mail secondaire :",
        'required' => false
      ))
      //->add('username', 'text', array('label' => "Identifiant", 'required' => true ))
      ->add('email', 'text', array('label' => "E-mail : *", 'required' => true))
      ->add('plainPassword', 'repeated', array(
        'label' => "Mot de Passe",
        'type' => 'password',
        'first_name' => 'Mot_de_Passe',
        'second_name' => 'Confirmation',
        'invalid_message' => 'Les champs du mot de passe doivent être identiques.',
      ))
      ->add('adresse_fact', 'text', array('label' => "Adresse : *", 'required' => true))
      ->add('adresse2_fact', 'text', array('label' => "Adresse suite :", 'required' => false))
      ->add('code_postal_fact', 'text', array('label' => "Code Postal : *", 'required' => true))
      ->add('ville_fact', 'text', array('label' => "Ville : *", 'required' => true))
      ->add('pays_fact', 'text', array('label' => "Pays : *", 'required' => true))

    ;

    // Pages publiques, hors process de résa ou admin.
    if ($this->rand1 && $this->rand2) {
      $this->question = sprintf(
        'Quelle est la somme de %s + %s ?', $this->rand1, $this->rand2
      );
      $builder
        ->add('human', 'text', array(
          'label' => 'Vous êtes humain : *',
          'required' => true,
          'mapped' => false,
          'constraints' => array(
            new EqualTo(array(
              'value' => $this->rand1 + $this->rand2,
              'message' => 'La somme ne correspond pas.',
              ))
          )
      ));
    }
  }

  public function getName()
  {
    return 'inscriptionLocataire';
  }

  public function getQuestion()
  {
    return $this->question;
  }

}
