<?php

namespace Pat\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Pat\CompteBundle\Repository\VilleRepository;
use Symfony\Component\Validator\Constraints\EqualTo;

class DemandeDevisForm extends AbstractType
{

  protected $translator;
  protected $rand1;
  protected $rand2;
  protected $question;

  function __construct(Translator $t, $rand1, $rand2)
  {
    $this->translator = $t;
    $this->rand1 = $rand1;
    $this->rand2 = $rand2;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    $builder
      //LOCALISATION
      ->add('nom', 'text', array('label' => "Nom : *", "required" => true, 'max_length' => 50))
      ->add('prenom', 'text', array('label' => "Prénom : *", "required" => true, 'max_length' => 50))
      ->add('email', 'text', array('label' => "Email : *", "required" => true, 'max_length' => 50))
      ->add('telephone', 'text', array('label' => "Tel : *", "required" => true, 'max_length' => 20))
      ->add('typeEntity', 'choice', array(
        'label' => "Je suis :",
        'expanded' => true,
        'multiple' => false,
        'choices' => array(
          'Un particulier' => 'Un particulier',
          'Une entreprise' => 'Une entreprise'),
        "data" => 'Un particulier',
        "required" => true))


      //TYPES DE BIENS
      ->add('typeAppartement', 'choice', array(
        'label' => "Type d’appartement recherché :",
        'required' => false,
        'choices' => array(
          'Appartement' => 'Appartement',
          'Studio' => 'Studio',
          'Maison' => 'Maison'),
        'expanded' => true,
        'multiple' => true))

      //NOMBRE DE PIECES
      ->add('nbPieces', 'choice', array(
        'label' => "Nombre de pièces :",
        'required' => false,
        'choices' => array("1" => '1',
          "2" => '2',
          "3" => '3',
          "4" => '4',
          "5" => '5',
          "6" => '6'),
        'expanded' => true,
        'multiple' => true))
      ->add('quartier', 'entity', array(
        'label' => "Quartier :",
        'required' => false,
        'empty_value' => 'Tous les quartiers',
        'expanded' => false,
        'multiple' => false,
        'class' => 'Pat\CompteBundle\Entity\Ville',
        'property' => 'nom',
        'query_builder' => function(VilleRepository $er) {
          return $er->getVillesBySearchQuery('Montpellier', '');
        }))


      //DATES
      ->add('arrivee', 'text', array('label' => "Arrivée :", "required" => false,))
      ->add('depart', 'text', array('label' => "Départ :", "required" => false))
      ->add('nbAdultes', 'text', array('label' => "Dont adultes :", "required" => false, 'max_length' => 2))
      ->add('nbEnfants', 'text', array('label' => "Dont enfants (-18 ans) :", "required" => false, 'max_length' => 2))
      ->add('nbLitSimple', 'text', array('label' => 'Dont lits simples : ', "required" => false, 'max_length' => 2))
      ->add('nbLitDouble', 'text', array('label' => 'Dont lits doubles : ', "required" => false, 'max_length' => 2))
      ->add('nbCanapeConvertible', 'text', array('label' => 'Dont canapés convertibles : ', "required" => false, 'max_length' => 2))
      ->add('nbPersonnes', 'text', array('label' => "Nombre de personnes", "required" => false, 'max_length' => 2))
      ->add('message', 'textarea', array('label' => "Message :", "required" => false))
      ->add('societe', 'text', array('label' => "Société :", "required" => false, 'max_length' => 50))
      ->add('fonction', 'text', array('label' => "Fonction :", "required" => false, 'max_length' => 50))
      ->add('nbAppartements', 'text', array('label' => "Nombre d'appartement(s) recherché(s) :", "required" => false, 'max_length' => 2))

    ;

    $this->question = sprintf(
      $this->translator->trans('Quelle est la somme de %s + %s ?'), $this->rand1, $this->rand2
    );
    $builder
      ->add('human', 'text', array(
        'label' => $this->translator->trans('Vous êtes humain : *'),
//            'attr' => array(
//              'placeholder' => $message
//            ),
        'required' => true,
        'mapped' => false,
        'constraints' => array(
          new EqualTo(array(
            'value' => $this->rand1 + $this->rand2,
            'message' => $this->translator->trans('La somme ne correspond pas.'),
            ))
        )
    ));
  }

  public function getName()
  {
    return 'frontformulairedemandedevis';
  }

  public function getQuestion()
  {
    return $this->question;
  }

}
