<?php

namespace Pat\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AppartementRechercheForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    $builder
      //LOCALISATION
      ->add('ville', 'text', array('label' => "Ville", "required" => false, 'max_length' => 50))
      ->add('nbPersonnes', 'choice', [
        'label' => 'Nombre de personnes',
        'choices' => [
          1 => 1,
          2 => 2,
          3 => 3,
          4 => 4,
          5 => 5,
          6 => 6,
          7 => 7,
          8 => 8,
          9 => 9,
          10 => 10,
          11 => 11,
          12 => 12,
          13 => 13,
          14 => 14,
          15 => 15,
          16 => 16,
          17 => 17,
          18 => 18,
          19 => 19,
          20 => 20,
        ],
        'required' => false,
        'empty_value' => 'Nombre de personnes',
      ])
      ->add('rayon', 'choice', array(
        'label' => "Rayon",
        'empty_value' => 'Rayon 0 km',
        'choices' => array(
          '5' => 'Rayon 5 km',
          '10' => 'Rayon 10 km',
          '15' => 'Rayon 15 km',
          '20' => 'Rayon 20 km',
          '25' => 'Rayon 25 km',
          '30' => 'Rayon 30 km'),
        "required" => false))

      //DATES
      ->add('datearrivee', 'text', array('label' => "", "required" => false,))
      ->add('datedepart', 'text', array('label' => "", "required" => false))

      //TYPES DE BIENS
      ->add('type', 'choice', array(
        'label' => "Type de bien",
        'choices' => array(
          /* 'Appartement T1'=>'T1',
            'Appartement T2'=>'T2',
            'Appartement T3'=>'T3',
            'Appartement T4'=>'T4',
            'Appartement T5'=>'T5',
            'Appartement T6'=>'T6', */
          'Appartement' => 'Appartement',
          'Studio' => 'Studio',
          'Maison' => 'Maison'),
        'expanded' => true,
        'multiple' => true,
        "required" => false))

      //NOMBRE DE PIECES
      ->add('nb_pieces', 'choice', array(
        'label' => "Nombre de pièces",
        'choices' => array(1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6'),
        'expanded' => true,
        'multiple' => true,
        "required" => false))

      //BUDGET
      ->add('budgetmini', 'number', array('label' => "Mini", "required" => false))
      ->add('budgetmaxi', 'number', array('label' => "Maxi", "required" => false))
      ->add('reference', 'text', array('label' => "Référence", "required" => false, 'max_length' => 10))

    //PLUS
    /* ->add('photo', 'checkbox', array('label'=> 'Photos', "required" => false)) */
    ;
  }

  public function getName()
  {
    return 'frontappartementrecherche';
  }

}
