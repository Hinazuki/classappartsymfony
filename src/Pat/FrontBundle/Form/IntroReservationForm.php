<?php

namespace Pat\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class IntroReservationForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    $builder
      //DATES
      ->add('datedebut', 'text', array('label' => "Début", "required" => true))
      ->add('datefin', 'text', array('label' => "Fin", "required" => true))
      ->add('ref', 'hidden', array('label' => "ref", "required" => false));
  }

  public function getName()
  {
    return 'frontintroreservation';
  }

}
