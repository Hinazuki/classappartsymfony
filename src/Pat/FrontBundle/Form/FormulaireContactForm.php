<?php

namespace Pat\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Validator\Constraints\EqualTo;

class FormulaireContactForm extends AbstractType
{

  protected $translator;
  protected $rand1;
  protected $rand2;
  protected $question;

  function __construct(Translator $t, $rand1, $rand2)
  {
    $this->translator = $t;
    $this->rand1 = $rand1;
    $this->rand2 = $rand2;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    $builder
      ->add('nom', 'text', array('label' => $this->translator->trans("Nom : *", array(), 'messages'), "required" => true, 'max_length' => 50))
      ->add('prenom', 'text', array('label' => $this->translator->trans("Prénom : *", array(), 'messages'), "required" => true, 'max_length' => 50))
      ->add('societe', 'text', array('label' => $this->translator->trans("Société :", array(), 'messages'), "required" => false, 'max_length' => 50))
      ->add('fonction', 'text', array('label' => $this->translator->trans("Fonction :", array(), 'messages'), "required" => false, 'max_length' => 50))
      ->add('email', 'text', array('label' => $this->translator->trans("Email : *", array(), 'messages'), "required" => true, 'max_length' => 150))
      ->add('objet', 'text', array('label' => $this->translator->trans("Objet : *", array(), 'messages'), "required" => true, 'max_length' => 150))
      ->add('tel', 'text', array('label' => $this->translator->trans("Tél : *", array(), 'messages'), "required" => true, 'max_length' => 20))
//            ->add('type_societe', 'choice', array(
//                'label' => "Vous êtes",
//                'choices' => array( 'particulier'=>'Particulier',
//                                    'proprietaire'=>'Propriétaire',
//                                    'resident'=>'Résident',
//                                    'presse'=>'Presse',
//                                    'professionnel'=>'Professionnel',
//                                    'agence'=>'Agence',
//                                    'autre'=>'Autre'),
//                "required" => true))
      ->add('message', 'textarea', array('label' => $this->translator->trans("Préciser votre message : *", array(), 'messages'), "required" => true));

    $this->question = sprintf(
      $this->translator->trans('Quelle est la somme de %s + %s ?'), $this->rand1, $this->rand2
    );
    $builder
      ->add('human', 'text', array(
        'label' => $this->translator->trans('Vous êtes humain : *'),
//            'attr' => array(
//              'placeholder' => $message
//            ),
        'required' => true,
        'mapped' => false,
        'constraints' => array(
          new EqualTo(array(
            'value' => $this->rand1 + $this->rand2,
            'message' => $this->translator->trans('La somme ne correspond pas.'),
            ))
        )
    ));
  }

  public function getName()
  {
    return 'frontformulairecontact';
  }

  public function getQuestion()
  {
    return $this->question;
  }

}
