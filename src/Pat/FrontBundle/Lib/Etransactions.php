<?php

namespace Pat\FrontBundle\Lib;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Préparation des données pour les paiements.
 *
 * @link https://www.ca-moncommerce.com/module-etransaction/php/
 * @author AG
 */
class Etransactions
{

  /**
   * Opération réussie.
   */
  const SUCCESSFUL = '00000';

  /**
   * Annulation volontaire par le client.
   */
  const CANCELED = '00001';

  /**
   * Retourne la structure de base.
   *
   * @param array $payboxParams
   * @return array
   */
  protected static function _getBase($payboxParams)
  {
    return array(
      'PBX_SITE' => $payboxParams['site'],
      'PBX_RANG' => $payboxParams['rank'],
      'PBX_IDENTIFIANT' => $payboxParams['id'],
      'PBX_TOTAL' => 0,
      'PBX_DEVISE' => '978',
      'PBX_CMD' => null,
      'PBX_PORTEUR' => null,
      'PBX_RETOUR' => $payboxParams['return'],
      'PBX_HASH' => $payboxParams['hash'],
      'PBX_TIME' => null,
//      'PBX_LANGUE' => 'FRA',
      'PBX_TYPEPAIEMENT' => 'CARTE',
//      'PBX_TYPECARTE' => 'CB',
      'PBX_EFFECTUE' => null,
      'PBX_REFUSE' => null,
      'PBX_ANNULE' => null,
      'PBX_REPONDRE_A' => null
    );
  }

  /**
   * Calcule le chiffrement.
   *
   * @param array $payboxParams
   * @param array $formData
   * @return string
   */
  protected static function _hash($payboxParams, $formData)
  {
    $queryData = array();
    foreach ($formData as $key => $value) {
      $queryData[] = $key.'='.$value;
    }
    $queryString = implode('&', $queryData);

    // clef ASCII => Binaire
    $binKey = pack('H*', $payboxParams[$payboxParams['mode']]['key']);
    // Hâchage.
    return strtoupper(hash_hmac('sha512', $queryString, $binKey));
  }

  /**
   * Retourne les données pour le formulaire de paiment.
   *
   * @param array $payboxParams
   * @param type $router
   * @param type $payment
   * @return array
   */
  public static function prepareDataForPayment($payboxParams, $router, $payment)
  {
    $formData = self::_getBase($payboxParams);

    $formData['PBX_EFFECTUE'] = $router->generate(
      'pat_payment_confirmation', array('status' => 'effectue'),
      UrlGeneratorInterface::ABSOLUTE_URL
    );
    $formData['PBX_REFUSE'] = $router->generate(
      'pat_payment_confirmation', array('status' => 'refuse'),
      UrlGeneratorInterface::ABSOLUTE_URL
    );
    $formData['PBX_ANNULE'] = $router->generate(
      'pat_payment_confirmation', array('status' => 'annule'),
      UrlGeneratorInterface::ABSOLUTE_URL
    );

    if ($payboxParams['host'] == 'host') {
      $formData['PBX_REPONDRE_A'] = $router->generate(
        'pat_payment_result', array(),
        UrlGeneratorInterface::ABSOLUTE_URL
      );
    }
    else {
      $formData['PBX_REPONDRE_A'] = $payboxParams['dev']['respond'];
    }

    // Voir Pat\CompteBundle\Tools\PaymentTools\MyTwigExtension
    $total = round((float) $payment->getAmount(), 2) * 100;

    $formData['PBX_TOTAL'] = $total;
    $formData['PBX_CMD'] = $payment->getId();
    $formData['PBX_PORTEUR'] = $payment->getMember()->getEmail();
    $formData['PBX_TIME'] = date('c');

    $hmac = self::_hash($payboxParams, $formData);
    $formData['PBX_HMAC'] = $hmac;

    return $formData;
  }

  /**
   * Retourne les données pour le formulaire de paiment.
   *
   * @param array $payboxParams
   * @param type $router
   * @param type $reservation
   * @return array
   */
  public static function prepareDataForReservation($payboxParams, $router, $reservation)
  {
    $formData = self::_getBase($payboxParams);

    $formData['PBX_EFFECTUE'] = $router->generate(
      'pat_front_reservation_confirmation', array('status' => 'effectue'),
      UrlGeneratorInterface::ABSOLUTE_URL
    );
    $formData['PBX_REFUSE'] = $router->generate(
      'pat_front_reservation_confirmation', array('status' => 'refuse'),
      UrlGeneratorInterface::ABSOLUTE_URL
    );
    $formData['PBX_ANNULE'] = $router->generate(
      'pat_front_reservation_confirmation', array('status' => 'annule'),
      UrlGeneratorInterface::ABSOLUTE_URL
    );

    if ($payboxParams['host'] == 'host') {
      $formData['PBX_REPONDRE_A'] = $router->generate(
        'pat_front_reservation_payment_result', array(),
        UrlGeneratorInterface::ABSOLUTE_URL
      );
    }
    // En localhost, on journalise les retours banque sur le serveur.
    // Cela évite la génération de messages d'erreur pour hôte injoignable
    // dans la backoffice bancaire de ClassAppart.
    else {
//      $formData['PBX_EFFECTUE'] = $payboxParams['dev']['successful'];
//      $formData['PBX_REFUSE'] = $payboxParams['dev']['refused'];
//      $formData['PBX_ANNULE'] = $payboxParams['dev']['canceled'];
      $formData['PBX_REPONDRE_A'] = $payboxParams['dev']['respond'];
//      $formData['PBX_ATTENTE'] = $payboxParams['dev']['waiting'];
//      $formData['PBX_IPN'] = $payboxParams['dev']['ipn'];
    }

    // Simulation d'une erreur (refus, etc).
//    if ($payboxParams['mode'] == 'dev') {
//      $formData['PBX_ERRORCODETEST'] = '00101';
//    }

    // Voir Pat\CompteBundle\Tools\PaymentTools\MyTwigExtension
    $total = round((float) $reservation->getArrhes(), 2) * 100;

    $formData['PBX_TOTAL'] = $total;
    $formData['PBX_CMD'] = $reservation->getReference();
    $formData['PBX_PORTEUR'] = $reservation->getUtilisateur()->getEmail();
    $formData['PBX_TIME'] = date('c');

    $hmac = self::_hash($payboxParams, $formData);
    $formData['PBX_HMAC'] = $hmac;

    return $formData;
  }

  /**
   *
   * Voir E-transactions_Manuel_Integration_Internet_CB5.5_FR.pdf
   * $14.1 Codes réponses du centre d'autorisation
   * Pages 45, 61
   *
   * @param string $code
   * @return string
   */
  public static function getResponseMessage($code)
  {
//    if (strpos($code, '001') === 0) {
//      return 'Paiement refusé par le centre d\'autorisation. Contactez votre établissement bancaire.';
//    }

    $messages = array(
      self::SUCCESSFUL => 'Transaction réussie.',
      self::CANCELED => 'Annulé par le client. / La connexion au centre d\'autorisation a échouée.',
      '00003' => 'Erreur de la plateforme bancaire.',
      '00004' => 'Numéro de porteur ou cryptogramme visuel invalide.',
      // Il n'y a pas de 00005
      '00006' => 'Accès refusé ou site/rang/identifiant incorrect.',
      '00008' => 'Date de fin de validité incorrecte.',
      '00009' => 'Erreur de création d\'un abonnement.',
      '00010' => 'Devise inconnue.',
      '00011' => 'Montant incorrect.',
      '00015' => 'Paiement déjà effectué.',
      '00016' => 'Abonné déjà existant.',
      '00021' => 'Carte non autorisée',
      '00029' => 'Carte non conforme.',
      '00030' => 'Temps d\'attente supérieur à 15 mn avant de procéder au paiement.',
//      '00031' => 'Réservé.',
//      '00032' => 'Réservé.',
      '00033' => 'Code pays de l\'adresse IP du navigateur de l\'acheteur non autorisé.',
      '00040' => 'Opération sans authentification 3-DSecure, bloquée par le filtre.',
      // Réseaux CB, Visa, Mastercard, American Express et Diners
      '00101' => 'Contacter l\'émetteur de carte.',
      '00102' => 'Contacter l\'émetteur de carte.',
      '00103' => 'Commerçant invalide.',
      '00104' => 'Conserver la carte.',
      '00105' => 'Ne pas honorer.',
      // Pas de 106
      '00107' => 'Conserver la carte, conditions spéciales.',
      '00108' => 'Approuver après identification du porteur.',
      // Pas de 109 à 111
      '00112' => 'Transaction invalide.',
      '00113' => 'Montant invalide.',
      '00114' => 'Numéro de porteur invalide.',
      '00115' => 'Emetteur de carte inconnu.',
      '00117' => 'Annulation client.',
      // Pas de 118
      '00119' => 'Répéter la transaction ultérieurement.',
      '00120' => 'Réponse erronée (erreur dans le domaine serveur).',
      // Pas de 121 à 123
      '00124' => 'Mise à jour de fichier non supportée.',
      '00125' => 'Impossible de localiser l\'enregistrement dans le fichier.',
      '00126' => 'Enregistrement dupliqué, ancien enregistrement remplacé.',
      '00127' => 'Erreur en édition sur champ de mise à jour fichier.',
      '00128' => 'Accès interdit au fichier.',
      '00129' => 'Mise à jour de fichier impossible.',
      '00130' => 'Erreur de format.',
      // Pas de 131 et 132
      '00133' => 'Carte expirée.',
      // Pas de 134 à 137.
      '00138' => 'Nombre d’essais code confidentiel dépassé.',
      // Pas de 139 et 140
      '00141' => 'Carte perdue.',
      // Pas de 142
      '00143' => 'Carte volée.',
      // Pas de 144 à 150
      '00151' => 'Provision insuffisante ou crédit dépassé.',
      // Pas de 152 et 153
      '00154' => 'Date de validité de la carte dépassée.',
      '00155' => 'Code confidentiel erroné.',
      '00156' => 'Carte absente du fichier.',
      '00157' => 'Transaction non permise à ce porteur.',
      '00158' => 'Transaction interdite au terminal.',
      '00159' => 'Suspicion de fraude.',
      '00160' => 'L\'accepteur de carte doit contacter l\'acquéreur.',
      '00161' => 'Dépasse la limite du montant de retrait.',
      // Pas de 162
      '00163' => 'Règles de sécurité non respectées.',
      // Pas de 164 à 167
      '00168' => 'Réponse non parvenue ou reçue trop tard.',
      // Pas de 169 à 174
      '00175' => 'Nombre d\'essais code confidentiel dépassé.',
      '00176' => 'Porteur déjà en opposition, ancien enregistrement conservé.',
      // Pas de 177 à 188
      '00189' => 'Echec de l\'authentificatio.',
      '00190' => 'Arrêt momentané du système.',
      '00191' => 'Emetteur de cartes inaccessible.',
      // Pas de 192 et 193
      '00194' => 'Demande dupliquée.',
      // Pas de 195
      '00196' => 'Mauvais fonctionnement du système.',
      '00197' => 'Echéance de la temporisation de surveillance globale.',
      '99999' => 'Opération en attente de validation par l\'émetteur du moyen de paiement.'
    );

    if (isset($messages[$code])) {
      return $messages[$code];
    }

    return 'Réponse inconnue de la banque.';
  }

}
