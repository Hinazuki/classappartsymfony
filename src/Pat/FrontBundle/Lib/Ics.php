<?php

namespace Pat\FrontBundle\Lib;

/**
 * Classe de génération de calendriers ICS.
 *
 * @link https://fr.wikipedia.org/wiki/ICalendar
 * @link https://tools.ietf.org/html/rfc2445
 * @author AG
 */
class Ics
{

  /**
   * Format des dates locales.
   */
  const D_FORMAT = 'Ymd';

  /**
   * Format des dates/heures locales.
   */
  const DT_FORMAT = 'Ymd\THis';

  /**
   * Format des dates/heures UTC.
   */
  const DT_UTC_FORMAT = 'Ymd\THis\Z';

  /**
   * Message d'indisponibilité.
   */
  const SUMMARY = 'Class-Appart (Not available)';

  /**
   * Génère un calendrier d'évènements.
   *
   * @param array $items
   * @param integer $end Timestamp
   * @return type
   */
  public static function generateCalendar($items, $end)
  {
    $ics = static::_getCalendarBegin();
    $ics .= static::_getTimeZone();

    foreach ($items as $item) {
      $ics .= static::_getEvent($item);
    }

    $ics .= static::_getCalendarEnd();

    return $ics;
  }

  /**
   * Génnère un calendrier de type Free/Busy.
   *
   * @param array $items
   * @param integer $end Timestamp
   * @param string $url
   * @return string
   * @link https://tools.ietf.org/html/rfc2445#section-4.6.4
   */
  public static function generateFreeBusyCalendar($items, $end, $url = null)
  {
    $ifb = static::_getCalendarBegin();
    $ifb .= static::_getFreeBusyBegin($end);

    foreach ($items as $item) {
      $ifb .= static::_getFreeBusyItem($item['start'], $item['end']);
    }

    $ifb .= static::_getFreeBusyEnd($url);
    $ifb .= static::_getCalendarEnd();

    return $ifb;
  }

  /**
   * Ajoute un élément de type Free/Busy.
   *
   * @param integer $start Timestamp
   * @param integer $end Timestamp
   * @return string
   * @link https://tools.ietf.org/html/rfc2445#section-4.8.2.6
   */
  protected static function _getFreeBusyItem($start, $end)
  {
    $from = static::_getDateTimeUtc($start);
    $to = static::_getDateTimeUtc($end);
    return <<<"EOT"
FREEBUSY:$from/$to

EOT;
  }

  /**
   * Retourne le début du calendrier.
   *
   * @return string
   */
  protected static function _getCalendarBegin()
  {
    return <<<'EOT'
BEGIN:VCALENDAR
PRODID:-//Class-Appart//NONSGML Class-Appart.com//EN
VERSION:2.0

EOT;
  }

  /**
   * Retourne la fin du calendrier.
   *
   * @return string
   */
  protected static function _getCalendarEnd()
  {
    return <<<'EOT'
END:VCALENDAR

EOT;
  }

  /**
   * Retourne le composant Time Zone.
   *
   * @return string
   * @link https://tools.ietf.org/html/rfc2445#section-4.6.5
   */
  protected static function _getTimeZone()
  {
    return <<<'EOT'
BEGIN:VTIMEZONE
TZID:Europe/Paris
BEGIN:STANDARD
DTSTART:19701025T030000
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10
TZNAME:CET
END:STANDARD
BEGIN:DAYLIGHT
DTSTART:19700329T020000
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3
TZNAME:CEST
END:DAYLIGHT
END:VTIMEZONE

EOT;

  }

  /**
   * Retourne un évènement.
   *
   * @param array $item
   * @return string
   * @link https://tools.ietf.org/html/rfc2445#section-4.6.1
   */
  protected static function _getEvent($item)
  {
    $uid = $item['uid'];
    $dtStamp = static::_getDateTime();
    if ($item['value'] == 'date-time') {
      $options = 'TZID=Europe/Paris';
      $dtStart = static::_getDateTime($item['start']);
      $dtEnd = static::_getDateTime($item['end']);
    }
    else {
      $options = 'VALUE=DATE';
      $dtStart = static::_getDate($item['start']);
      $dtEnd = static::_getDate($item['end']);
    }
    $summary = $item['summary'] ? $item['summary'] : static::SUMMARY;

    return <<<"EOT"
BEGIN:VEVENT
UID:$uid@class-appart.com
DTSTAMP:$dtStamp
DTSTART;$options:$dtStart
DTEND;$options:$dtEnd
SUMMARY:$summary
END:VEVENT

EOT;
  }

  /**
   * Retourne le début du composant Free/Busy.
   *
   * @param integer $end Timestamp
   * @return string
   */
  protected static function _getFreeBusyBegin($end)
  {
    $dtStamp = static::_getDateTime();
    $dtEnd = static::_getDateTime($end);

    return <<<"EOT"
BEGIN:VFREEBUSY
ORGANIZER;CN=Class Appart:mailto:contact@class-appart.com
DTSTAMP:$dtStamp
DTSTART:$dtStamp
DTEND:$dtEnd

EOT;
  }

  /**
   * Retourne la fin du composant Free/Busy.
   *
   * @param string $url
   * @return string
   */
  protected static function _getFreeBusyEnd($url = null)
  {
    $end = '';
    if ($url) {
      $end .= 'URL:.'.$url.PHP_EOL;
    }
    return $end .= 'END:VFREEBUSY'.PHP_EOL;
  }

  /**
   * Retourne une date formatée ISO-8601.
   *
   * @param integer $timestamp
   * @param boolean $formatted
   * @return string
   * @link https://tools.ietf.org/html/rfc2445#section-4.3.5
   */
  protected static function _getDate($timestamp = null, $formatted = true)
  {
    $dt = new \DateTime(null, new \DateTimeZone('Europe/Paris'));
    if ($timestamp) {
      $dt->setTimestamp($timestamp);
    }
    return $formatted ? $dt->format(static::D_FORMAT) : $dt;
  }

  /**
   * Retourne une date/heure formatée ISO-8601.
   *
   * @param integer $timestamp
   * @param boolean $formatted
   * @return string
   * @link https://tools.ietf.org/html/rfc2445#section-4.3.5
   */
  protected static function _getDateTime($timestamp = null, $formatted = true)
  {
    $dt = new \DateTime(null, new \DateTimeZone('Europe/Paris'));
    if ($timestamp) {
      $dt->setTimestamp($timestamp);
    }
    return $formatted ? $dt->format(static::DT_FORMAT) : $dt;
  }

  /**
   * Retourne une date/heure UTC formatée en ISO-8601.
   *
   * @param integer $timestamp
   * @return string
   */
  protected static function _getDateTimeUtc($timestamp = null)
  {
    $dt = static::_getDateTime($timestamp, false);
    $dt->setTimezone(new \DateTimeZone('UTC'));
    return $dt->format(static::DT_UTC_FORMAT);
  }

}
