<?php

// https://stackoverflow.com/questions/7081008/symfony2-logging-404-errors

namespace Pat\FrontBundle\Lib;

use Symfony\Component\EventDispatcher\Event;

class CustomListener
{

  public function onKernelException(Event $event)
  {
    $exception = $event->getException();

    $log = dirname(dirname(dirname(dirname(__DIR__)))).DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'custom.log';

    file_put_contents($log, print_r(array(
      'datetime' => date('Y-m-d H:i:s'),
      'message' => $exception->getMessage(),
      'request' => empty($_REQUEST) ? '' : $_REQUEST,
      'ua' => $_SERVER['HTTP_USER_AGENT'],
      'uri' => $_SERVER['REQUEST_URI']
        ), true), FILE_APPEND);
  }

}
