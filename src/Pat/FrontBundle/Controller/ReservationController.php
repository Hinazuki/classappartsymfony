<?php

namespace Pat\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Pat\CompteBundle\Lib\IntroReservation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Cookie;
use Pat\FrontBundle\Form\IntroReservationForm;
use Pat\FrontBundle\Form\OptionsForm;
use Pat\FrontBundle\Form\InscriptionLocataireForm;
use Pat\FrontBundle\Form\PaymentModeForm;
use Pat\FrontBundle\Lib\Etransactions;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
//use Doctrine\Common\Collections\ArrayCollection;
use Pat\CompteBundle\Entity\Reservation;
//use Pat\CompteBundle\Entity\Calendrier;
use Pat\CompteBundle\Entity\Option;
use Pat\CompteBundle\Entity\Payment;
use Pat\UtilisateurBundle\Entity\Utilisateur;
//use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
//use FOS\UserBundle\Model\UserInterface;
use Pat\CompteBundle\Tools\ReservationTools;
use Pat\CompteBundle\Tools\PaymentTools;


class ReservationController extends Controller
{

  public function isDispo($session)
  {
    $em = $this->container->get('doctrine')->getManager();

    return $em->getRepository('PatCompteBundle:Appartement')->isAvailableForResa($session->get('resa_appartement'), $session->get('resa_date_debut'), $session->get('resa_date_fin'));
  }

  public function montantTotal($session, $with_options = true)
  {
    $montant_total = 0;
    $em = $this->container->get('doctrine')->getManager();
    $resaDateDebut = $this->container->get('pat.date_functions')->toDateEn($session->get('resa_date_debut'));
    $resaDateFin = $this->container->get('pat.date_functions')->toDateEn($session->get('resa_date_fin'));

    $appartement = $em->getRepository('PatCompteBundle:Appartement')->find($session->get('resa_appartement'));

    $nb_nuits = $session->get('resa_nbnuits');
    $nb_adultes = (int) $session->get('resa_nbadulte');
    $opt_roses = $this->container->get('pat.tarif_manager')->getPrixRoses() * $session->get('resa_opt_roses');
    $opt_champagne = $this->container->get('pat.tarif_manager')->getPrixChampagne() * $session->get('resa_opt_champagne');
    $opt_lit_bebe = $session->get('resa_opt_lit_bebe');
    $opt_menage = $this->container->get('pat.tarif_manager')->getPrixMenage($appartement->getNbPieces()) * $session->get('resa_opt_menage');

    $param_taxes = $em->getRepository('PatCompteBundle:Parametre')->findOneByName('taxes');
    $parentReservation = $session->get('resa_parent');
    $calcul_values = $this->container->get('pat.tarif_manager')->calculLoyer($appartement->getTarif(), $param_taxes->getValue(), $appartement->getNbPieces(), $nb_nuits, $nb_adultes, $resaDateDebut, $resaDateFin, $parentReservation);
    $session->set('calcul_values', $calcul_values);
    $session->set('resa_loyer', round($calcul_values['tarif_net'], 2));

    $montant_total = $calcul_values['tarif_total'] + $opt_roses + $opt_champagne + $opt_menage;

    if ($opt_lit_bebe == true)
      $montant_total += $this->container->get('pat.tarif_manager')->getPrixLitBebe();


    $depot = $appartement->getTarif()->getDepot() * $calcul_values['tarif_total'] / 100;

    // Dépot max de 100% du loyer mensuel
    $depotmax = $this->container->get('pat.tarif_manager')->getLoyer($appartement->getTarif(), "mois") * 30; // multiplie par 30 pour avoir le prix du mois entier
    if ($depot > $depotmax || $nb_nuits > 59)
      $depot = $depotmax;

    // Dépot mini
    if ($depot < $appartement->getTarif()->getDepotMin())
      $depot = $appartement->getTarif()->getDepotMin();

    $session->set('resa_depot', round($depot));

    // Si la réservation est faite plus de 48h avant
    if ($this->container->get('pat.date_functions')->toDateEn($session->get('resa_date_debut')) > date("Y-m-d", strtotime('+2 days')))
      $session->set('resa_arrhes', round((round($montant_total, 2) * $appartement->getTarif()->getAcompte() / 100), 2));
    else
      $session->set('resa_arrhes', round($montant_total, 2) + round($depot));
    // Si l'on veut le tarif sans les options
    if ($with_options === false)
      return $calcul_values['tarif_total'];
    else
      return round($montant_total, 2);
  }

  public function selectionDateResaAction(Request $request, $message = null, $appartRef = null)
  {
    $em = $this->container->get('doctrine')->getManager();
    $session = $request->getSession();


    // On supprime les réservations non payées et dont la date de validité est passée
    $this->cleanReservations();

    $data = $request->request->get('frontintroreservation');
    if (!is_null($appartRef))
      $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array("reference" => $appartRef));
    else
      $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array("reference" => $data['ref']));
    if (!$appartement) {
      throw new NotFoundHttpException("Les informations sur l'appartement sont manquantes");
    }

    if (!is_null($session->get('resa_date_debut')) && !is_null($session->get('resa_date_debut')))
      $DateResa = new IntroReservation($session->get('resa_date_debut'), $session->get('resa_date_fin'), $appartRef);
    else
      $DateResa = new IntroReservation($session->get('date_debut'), $session->get('date_fin'), $appartRef);


    $formSelectdateReservation = $this->container->get('form.factory')->create(new IntroReservationForm(), $DateResa);

    if ($request->getMethod() == 'POST') {
      $formSelectdateReservation->bind($request);
      if ($formSelectdateReservation->isValid()) {
        $data_form = $formSelectdateReservation->getData();

        if ($appartement->getIsResa() == 1) {

          // On vérifie la validité des dates
          // Dates avec un format incorrect ou dates non valides
          if (!preg_match("#^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$#", $data_form->getDateDebut()) || !preg_match("#^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$#", $data_form->getDateFin()))
            return new RedirectResponse($this->container->get('router')->generate('pat_front_appartement_detail_probleme_dates', array("url_bien" => $appartement->getUrlFr(), "erreur" => '1')));

          $date_test1 = explode("/", $data_form->getDateDebut());
          $date_test2 = explode("/", $data_form->getDateFin());
          if (checkdate($date_test1[1], $date_test1[0], $date_test1[2]) === false || checkdate($date_test2[1], $date_test2[0], $date_test2[2]) === false)
            return new RedirectResponse($this->container->get('router')->generate('pat_front_appartement_detail_probleme_dates', array("url_bien" => $appartement->getUrlFr(), "erreur" => '1')));

          $user = $this->container->get('security.context')->getToken()->getUser();
          // Date d'arrivée inférieure à la date du jour
          if (is_object($user)) {
            if (!$user->hasRole('ROLE_ADMIN')) {
              if (mktime(null, null, null, $date_test1[1], $date_test1[0], $date_test1[2]) < time())
                return new RedirectResponse($this->container->get('router')->generate('pat_front_appartement_detail_probleme_dates', array("url_bien" => $appartement->getUrlFr(), "erreur" => '2')));
            }
          }
          else {
            if (mktime(null, null, null, $date_test1[1], $date_test1[0], $date_test1[2]) < time())
              return new RedirectResponse($this->container->get('router')->generate('pat_front_appartement_detail_probleme_dates', array("url_bien" => $appartement->getUrlFr(), "erreur" => '2')));
          }


          // Date de départ inférieure à la date d'arrivée
          if (mktime(null, null, null, $date_test2[1], $date_test2[0], $date_test2[2]) <= mktime(null, null, null, $date_test1[1], $date_test1[0], $date_test1[2]))
            return new RedirectResponse($this->container->get('router')->generate('pat_front_appartement_detail_probleme_dates', array("url_bien" => $appartement->getUrlFr(), "erreur" => '3')));


          // Réservation supérieure à 90 jours
          if ($this->container->get('pat.date_functions')->nbJours($data_form->getDateDebut(), $data_form->getDateFin()) > 90)
            return new RedirectResponse($this->container->get('router')->generate('pat_front_appartement_detail_probleme_dates', array("url_bien" => $appartement->getUrlFr(), "erreur" => '5')));

          // Réservation inférieure à 3 nuits
          if (
            $this->container->get('pat.date_functions')->nbJours($data_form->getDateDebut(), $data_form->getDateFin()) < 3 &&
            $user !== null &&
            false === $user->hasRole('ROLE_ADMIN')
          ) {
            return new RedirectResponse($this->container->get('router')->generate('pat_front_appartement_detail_probleme_dates', array("url_bien" => $appartement->getUrlFr(), "erreur" => '6')));
          }

          // Fin de test sur les dates
          // On stocke les dates en session
          $session->set('resa_date_debut', $data_form->getDateDebut());
          $session->set('resa_date_fin', $data_form->getDateFin());
          $session->set('resa_appartement', $appartement->getId());

          // On test la disponibilité de l'appartement
          if ($this->isDispo($session) == false) {
            return new RedirectResponse($this->container->get('router')->generate('pat_front_appartement_detail_probleme_dates', array("url_bien" => $appartement->getUrlFr(), "erreur" => '4')));
          }

          // Si tout est correct on continue la réservation
          return new RedirectResponse($this->container->get('router')->generate('pat_front_reservation_calcul_montant'));
        }
        else {
          return new RedirectResponse($this->container->get('router')->generate('pat_front_appartement_detail', array("url_bien" => $appartement->getUrlFr())));
        }
      }
    }

    return $this->container->get('templating')->renderResponse('PatFrontBundle:Reservation:selectionDate.html.twig', array('formSelectDateReservation' => $formSelectdateReservation->createView(), 'message' => $message)
    );
  }

  public function calculMontantAction(Request $request)
  {
    $session = $request->getSession();

    $resa_date_debut = $session->get('resa_date_debut');
    $resa_date_fin = $session->get('resa_date_fin');
    $resa_appart = $session->get('resa_appartement');
    $nb_personnes = $session->get('resa_nbpersonne') == null ? 1 : $session->get('resa_nbpersonne');
    $nb_adultes = $session->get('resa_nbadulte') == null ? 1 : $session->get('resa_nbadulte');
    $nb_enfants = $session->get('resa_nbenfant') == null ? 0 : $session->get('resa_nbenfant');
    $nb_lit_simples = $session->get('resa_nb_lit_simple') == null ? 0 : $session->get('resa_nb_lit_simple');
    $nb_lit_doubles = $session->get('resa_nb_lit_double') == null ? 0 : $session->get('resa_nb_lit_double');
    $nb_canape_convertibles = $session->get('resa_nb_canape_convertible') == null ? 0 : $session->get('resa_nb_canape_convertible');

    $tab_calcul = array();

    if ($resa_date_debut && $resa_date_fin && $resa_appart) {
      $em = $this->container->get('doctrine')->getManager();

      $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $resa_appart));
      if (!$appartement) {
        throw new NotFoundHttpException("Le bien est introuvable !");
      }

      //$loyer = $this->getLoyerFromAppByDateResa($appartement->getId(), $resa_date_debut, $resa_date_fin);
      /*
        $prix = round($this->CalculPrixNormal($loyer, $resa_date_debut, $resa_date_fin), 2);
        $tab_calcul['tarif_normal'] = $prix;

        // TODO Corriger ubg sur calcul de la promotion (au niveau de la détéction des limites)
        $tarif_sans_honoraires = $prix;

        $tarif_avec_honoraires = $tarif_sans_honoraires + round($this->calculHonorairesFixes($tarif_sans_honoraires), 2) + round($this->calculHonorairesVariables($tarif_sans_honoraires, $resa_date_debut, $resa_date_fin), 2);
        $tab_calcul['honoraires_fixes'] = round($this->calculHonorairesFixes($tarif_sans_honoraires), 2);
        $tab_calcul['honoraires_variables'] = round($this->calculHonorairesVariables($tarif_sans_honoraires, $resa_date_debut, $resa_date_fin), 2);

        $tarif_reel = $tarif_avec_honoraires + $this->calculFraisPlateForme($appartement->getId());
        $tab_calcul['frais_plateforme'] = $this->calculFraisPlateForme($appartement->getId());
        $arrhes = ($tarif_reel * 25)/100;

        $nb_nuits = round((strtotime($resa_date_fin) - strtotime($resa_date_debut))/(60*60*24));
        $media = $em->getRepository('PatCompteBundle:Media')->findOneBy(array('appartement' => $appartement->getId()), array('tri' => 'asc'));

        $tarif_reel = number_format(round($tarif_reel,2),2,","," ");
        $arrhes = number_format(round($arrhes,2),2,","," "); */
      $media = $em->getRepository('PatCompteBundle:Media')->findOneBy(array('appartement' => $appartement->getId()), array('tri' => 'asc'));

      $nb_nuits = $this->container->get('pat.date_functions')->nbJours($resa_date_debut, $resa_date_fin);

      $session->set('resa_nbnuits', $nb_nuits);
      $session->set('resa_nbpersonne', $nb_personnes);
      $session->set('resa_nbadulte', $nb_adultes);
      $session->set('resa_nbenfant', $nb_enfants);
      $session->set('resa_nb_lit_simple', $nb_lit_simples);
      $session->set('resa_nb_lit_double', $nb_lit_doubles);
      $session->set('resa_nb_canape_convertible', $nb_canape_convertibles);

      // On initialise les autres variables de session
      $session->set('resa_opt_roses', 0);
      $session->set('resa_opt_champagne', 0);
      $session->set('resa_opt_lit_bebe', false);
      $session->set('resa_opt_menage', 0);
      $session->set('resa_opt_taxi', false);
      $session->set('resa_opt_frigo', false);

      $montant_total = $this->montantTotal($session);

      return $this->container->get('templating')->renderResponse('PatFrontBundle:Reservation:calculMontant.html.twig', array('appartement' => $appartement,
          'media' => $media,
          'max_personne' => $appartement->getNbPersonne(),
          'nb_personnes' => $nb_personnes,
          'nb_adultes' => $nb_adultes,
          'nb_enfants' => $nb_enfants,
          'nb_lit_simples' => $nb_lit_simples,
          'nb_lit_doubles' => $nb_lit_doubles,
          'nb_canape_convertibles' => $nb_canape_convertibles,
          'montant_total' => $montant_total)
      );
    }
    else {
      return $this->container->get('templating')->renderResponse('PatFrontBundle:Reservation:sessionExpire.html.twig');
    }
  }

  // On récupère les dates sous la forme yyyy-mm-dd
  public function getLoyerFromAppByDateResa($id, $date_debut, $date_fin)
  {
    $em = $this->container->get('doctrine')->getManager();

    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $id));

    if (!$appartement) {
      throw new NotFoundHttpException("Cet appartement est introuvable");
    }

    $nb_jours = $this->container->get('pat.date_functions')->nbJours($date_debut, $date_fin);
    $loyer = $this->container->get('pat.tarif_manager')->getLoyerByDate($appartement->getTarif(), $nb_jours);

    return number_format($loyer, 2);
  }

  public function calculMontantAdminAction(Request $request)
  {
    $session = $request->getSession();

    $resa_date_debut = $session->get('admin_resa_date_debut');
    $resa_date_fin = $session->get('admin_resa_date_fin');
    $resa_appart = $session->get('admin_resa_appartement');
    $nb_personnes = $session->get('admin_resa_nbpersonne') == null ? 1 : $session->get('admin_resa_nbpersonne');
    $nb_adultes = $session->get('admin_resa_nbadulte') == null ? 1 : $session->get('admin_resa_nbadulte');
    $nb_enfants = $session->get('admin_resa_nbenfant') == null ? 1 : $session->get('admin_resa_nbenfant');
    $nb_lit_simples = $session->get('admin_resa_nb_lit_simple') == null ? 0 : $session->get('admin_resa_nb_lit_simple');
    $nb_lit_doubles = $session->get('admin_resa_nb_lit_double') == null ? 0 : $session->get('admin_resa_nb_lit_double');
    $nb_canape_convertibles = $session->get('admin_resa_nb_canape_convertible') == null ? 0 : $session->get('admin_resa_nb_canape_convertible');
    $tab_calcul = array();


    if ($resa_date_debut && $resa_date_fin && $resa_appart) {
      $em = $this->container->get('doctrine')->getManager();

      // on recoit la date au format d/m/y
      // on les met au format Y-m-d
      $temp_date_debut = explode("/", $resa_date_debut);
      $resa_date_debut = $temp_date_debut[2]."-".$temp_date_debut[1]."-".$temp_date_debut[0];

      $temp_date_fin = explode("/", $resa_date_fin);
      $resa_date_fin = $temp_date_fin[2]."-".$temp_date_fin[1]."-".$temp_date_fin[0];

      $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $resa_appart));

      $loyer = $this->getLoyerFromAppByDateResa($appartement->getId(), $resa_date_debut, $resa_date_fin);

      $prix = round($this->CalculPrixNormal($loyer, $resa_date_debut, $resa_date_fin), 2);
      $tab_calcul['tarif_normal'] = $prix;

      $tarif_sans_honoraires = $prix;

      $tarif_avec_honoraires = $tarif_sans_honoraires + round($this->calculHonorairesFixes($tarif_sans_honoraires), 2) + round($this->calculHonorairesVariables($tarif_sans_honoraires, $resa_date_debut, $resa_date_fin), 2);
      $tab_calcul['honoraires_fixes'] = round($this->calculHonorairesFixes($tarif_sans_honoraires), 2);
      $tab_calcul['honoraires_variables'] = round($this->calculHonorairesVariables($tarif_sans_honoraires, $resa_date_debut, $resa_date_fin), 2);

      $tarif_reel = $tarif_avec_honoraires + $this->calculFraisPlateForme($appartement->getId());
      $tab_calcul['frais_plateforme'] = $this->calculFraisPlateForme($appartement->getId());
      $arrhes = ($tarif_reel * 25) / 100;

      $tarif_reel = number_format(round($tarif_reel, 2), 2, ",", " ");
      $arrhes = number_format(round($arrhes, 2), 2, ",", " ");

      $nb_nuits = round((strtotime($resa_date_fin) - strtotime($resa_date_debut)) / (60 * 60 * 24));
      $media = $em->getRepository('PatCompteBundle:Media')->findOneBy(array('appartement' => $appartement->getId()), array('tri' => 'asc'));

      $session->set('admin_resa_prix', $tarif_reel);
      $session->set('admin_resa_arrhes', $arrhes);
      $session->set('admin_resa_nbnuits', $nb_nuits);
      $session->set('admin_resa_nbpersonne', $nb_personnes);
      $session->set('admin_resa_nbadulte', $nb_adultes);
      $session->set('admin_resa_nbenfant', $nb_enfants);
      $session->set('admin_resa_nb_lit_simple', $nb_lit_simples);
      $session->set('admin_resa_nb_lit_double', $nb_lit_doubles);
      $session->set('admin_resa_nb_canape_convertible', $nb_canape_convertibles);
      $session->set('admin_resa_tab_calcul', $tab_calcul);

      return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminReservation:calculMontant.html.twig', array('appartement' => $appartement,
          'prix' => $tarif_reel,
          'arrhes' => $arrhes,
          'nuits' => $nb_nuits,
          'media' => $media,
          'date_arrivee' => $resa_date_debut,
          'date_depart' => $resa_date_fin,
          'max_personne' => $appartement->getNbPersonne(),
          'nb_personnes' => $nb_personnes,
          'nb_adultes' => $nb_adultes,
          'nb_enfants' => $nb_enfants,
          'nb_lit_simples' => $nb_lit_simples,
          'nb_lit_doubles' => $nb_lit_doubles,
          'nb_canape_convertibles' => $nb_canape_convertibles,
          'tab_calcul' => $tab_calcul)
      );
    }
    else {
      throw new NotFoundHttpException("Les informations de réservation sont introuvables");
    }
  }

  // Fonction qui intervient quand on sélectionne la nombre de personne dans le process de réservation
  // Cette fonction recoit un appel ajax et met à jout ce nombre en session
  public function actualiseNbPersonneAction(Request $request)
  {
    $session = $request->getSession();

    if ($request->isXmlHttpRequest()) {
      $nb_personne = $request->request->get('nbpersonne');
      $nb_adulte = $request->request->get('nbadulte');
      $nb_enfant = $request->request->get('nbenfant');
      $nb_lit_simples = $request->request->get('nb_lit_simple');
      $nb_lit_doubles = $request->request->get('nb_lit_double');
      $nb_canape_convertibles = $request->request->get('nb_canape_convertible');

      if ($nb_personne != '') {
        $session->set('resa_nbpersonne', $nb_personne);
      }

      if ($nb_adulte != '') {
        $session->set('resa_nbadulte', $nb_adulte);
      }

      if ($nb_enfant != '') {
        $session->set('resa_nbenfant', $nb_enfant);
      }

      if ($nb_lit_simples != '') {
        $session->set('resa_nb_lit_simple', $nb_lit_simples);
      }

      if ($nb_lit_doubles != '') {
        $session->set('resa_nb_lit_double', $nb_lit_doubles);
      }

      if ($nb_canape_convertibles != '') {
        $session->set('resa_nb_canape_convertible', $nb_canape_convertibles);
      }
    }

    $montant_total = $this->montantTotal($session);

    $response = new Response(json_encode(array(
        'montant_total' => $this->container->get('twig.extension.mytwigext')->price_format($montant_total)." €",
        'acompte' => $this->container->get('twig.extension.mytwigext')->price_format($session->get('resa_arrhes'))." €")));

    return $response;
  }

  public function optionsAction(Request $request)
  {
    $em = $this->container->get('doctrine')->getManager();
    $session = $request->getSession();

    $resa_appart = $session->get('resa_appartement');
    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array("id" => $resa_appart));
    if (!$appartement) {
      throw new NotFoundHttpException("Les informations sur l'appartement sont manquantes");
    }

    // On test la disponibilité de l'appartement
    if ($this->isDispo($session) == false) {
      $this->container->get('session')->getFlashBag()->add('warning', "Désolé, ce bien n'est actuellement plus disponible pour ces dates.");
      return new RedirectResponse($this->container->get('router')->generate('pat_front_appartement_detail', array("url_bien" => $appartement->getUrlFr())));
    }

    // Initialisation du formulaire avec les données en session
    $resa = new Reservation();
    $resa->setOptionRoses($session->get('resa_opt_roses'));
    $resa->setOptionChampagne($session->get('resa_opt_champagne'));
    $resa->setOptionMenageSupp($session->get('resa_opt_menage'));
    $resa->setOptionLitBebe($session->get('resa_opt_lit_bebe'));
    $resa->setOptionTaxi($session->get('resa_opt_taxi'));
    $resa->setOptionFrigo($session->get('resa_opt_frigo'));
    //$resa->setOptionSupplementaires($session->get('resa_opt_supplementaires'));

    $formOptions = $this->container->get('form.factory')->create(new OptionsForm(), $resa);

    if ($request->getMethod() == 'POST') {
      $data = $request->request->all();
      $data = $data['frontoptionsreservation'];

//            On sauvegarde les options en session
      $session->set(
        'resa_opt_roses', isset($data['option_roses']) && $data['option_roses'] != '' ?
          $data['option_roses'] : 0
      );
      $session->set(
        'resa_opt_champagne', isset($data['option_champagne']) && $data['option_champagne'] != '' ?
          $data['option_champagne'] : 0
      );
      $session->set('resa_opt_lit_bebe', isset($data['option_lit_bebe']) ? true : false);
      $session->set(
        'resa_opt_menage', isset($data['option_menage_supp']) && $data['option_menage_supp'] != '' ?
          $data['option_menage_supp'] : 0
      );
      $session->set('resa_opt_taxi', isset($data['option_taxi']) ? true : false);
      $session->set('resa_opt_frigo', isset($data['option_frigo']) ? true : false);
      $session->set('resa_opt_supplementaires', isset($data['option_supplementaires']) ? true : false);
      return new RedirectResponse($this->container->get('router')->generate('pat_front_reservation_inscription'));
    }

    $montant_total = $this->montantTotal($session);
    $param_options = $em->getRepository('PatCompteBundle:Parametre')->findOneByName('text_options_reservation');

    return $this->container->get('templating')->renderResponse('PatFrontBundle:Reservation:options.html.twig', array('appartement' => $appartement,
        'form' => $formOptions->createView(),
        'montant_total' => $montant_total,
        'param_options' => $param_options)
    );
  }

  public function actualiseOptionsAction(Request $request)
  {
    $session = $request->getSession();

    if ($request->isXmlHttpRequest()) {
      $opt_roses = $request->request->get('opt_roses');
      $opt_champagne = $request->request->get('opt_champagne');
      $opt_menage = $request->request->get('opt_menage');
      $opt_lit_bebe = $request->request->get('opt_lit_bebe') == "true" ? true : false;

      $session->set('resa_opt_roses', $opt_roses);
      $session->set('resa_opt_champagne', $opt_champagne);
      $session->set('resa_opt_lit_bebe', $opt_lit_bebe);
      $session->set('resa_opt_menage', $opt_menage);
    }

    $montant_total = $this->montantTotal($session);

    $response = new Response(json_encode(array(
        'montant_total' => $this->container->get('twig.extension.mytwigext')->price_format($montant_total)." €",
        'acompte' => $this->container->get('twig.extension.mytwigext')->price_format($session->get('resa_arrhes'))." €")));

    return $response;
  }

  // fonction qui affiche le formulaire d'inscription
  // Si un utilisateur est connecté, on l'utilise.
  // Sinon on affiche un bouton pour se connecter au dessus de l'inscription
  public function inscriptionAction(Request $request)
  {
    $session = $request->getSession();
    $session->set('path_spec', 'pat_front_reservation_inscription');
    $em = $this->container->get('doctrine')->getManager();
    $user_context = $this->container->get('security.context')->getToken()->getUser();
    $message = null;

    $resa_appart = $session->get('resa_appartement');
    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array("id" => $resa_appart));
    if (!$appartement) {
      throw new NotFoundHttpException("Les informations sur l'appartement sont manquantes");
    }

    // On test la disponibilité de l'appartement
    if ($this->isDispo($session) == false) {
      $this->container->get('session')->getFlashBag()->add('warning', "Désolé, ce bien n'est actuellement plus disponible pour ces dates.");
      return new RedirectResponse($this->container->get('router')->generate('pat_front_appartement_detail', array("url_bien" => $appartement->getUrlFr())));
    }

    $locataire = new Utilisateur();
    $formInscription = $this->container->get('form.factory')->create(new InscriptionLocataireForm(), $locataire);

    if ($user_context && $user_context != 'anon.') {
      $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());

      if ($user) {
        // si un admin fait une réservation, on le redirige vers le choix de l'utilisateur
        if ($user->getType() == "Administrateur")
          return new RedirectResponse($this->container->get('router')->generate('pat_admin_reservation_recherche_locataire'));

        $session->set('resa_locataire', $user_context->getUsername());
        return new RedirectResponse($this->container->get('router')->generate('pat_front_reservation_recapitulatif'));
      }
    }

    if ($request->getMethod() == 'POST' && $request->request->get('Inscription')) {
      $formInscription->bind($request);
      if ($formInscription->isValid()) {
        //$patternUsername = "#^[a-z0-9]+$#i";
        $patternEmail = '#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';

        /* if (!preg_match($patternUsername , $formInscription['username']->getData()))
          {
          $message = "L'identifiant contient des caractères interdits";
          }
          else */if (!preg_match($patternEmail, $formInscription['email']->getData())) {
          $message = "L'adresse mail n'est pas valide";
        }
        else {
          $isExistEmail = $em->getRepository('PatUtilisateurBundle:Utilisateur')->checkEmailCompteAction($formInscription['email']->getData());
          if (!$isExistEmail) {
            $locataire->setConfirmationToken(null);
            $locataire->setEnabled(true);
            //
            // On définit que l'utilisateur est de type Locataire
            $locataire->setLocataire();
            $locataire->setCGU("1");

            //gestion du mot de passe
            $password = $formInscription['plainPassword']->getData();
            $encoder = $this->container->get('security.encoder_factory')->getEncoder($locataire);
            $locataire->setPassword($encoder->encodePassword($password, $locataire->getSalt()));

            $em->persist($locataire);
            $em->flush();

            $locataire->generateUsername();

            $em->flush();

            $user = $this->container->get('security.context')->getToken()->getUser();

            //$session->set('resa_locataire', $formInscription['username']->getData());
            $session->set('resa_locataire', $locataire->getUsername());

            //envoi du mail au locataire
            $message_email = \Swift_Message::newInstance()
              ->setSubject("Création de votre compte locataire - classAppart ®")
              ->setFrom('contact@class-appart.com')
              ->setTo($locataire->getEmail())
              ->setBcc($this->container->getParameter('mail_admin'));

//                        if($locataire->getMailsSecond())
//                            $message_email->setCc(explode(',', $locataire->getMailsSecond()));
            if ($locataire->getSecondEmail()) {
              $message_email->setCc($locataire->getSecondEmail());
            }

            $textBody = $this->container->get('templating')->render('PatCompteBundle:AdminLocataire:email_validation.txt.twig', array('utilisateur' => $locataire, 'password' => $password));
            $htmlBody = $this->container->get('templating')->render('PatCompteBundle:AdminLocataire:email_validation.html.twig', array('utilisateur' => $locataire, 'password' => $password));

            if (!empty($htmlBody)) {
              $message_email->setBody($htmlBody, 'text/html')
                ->addPart($textBody, 'text/plain');
            }
            else
              $message_email->setBody($textBody);

            $this->container->get('mailer')->send($message_email);

            return new RedirectResponse($this->container->get('router')->generate('pat_front_reservation_recapitulatif'));
          }
          else {
            $message = 'Cette adresse mail existe déjà';
          }
        }
      }
    }

    $montant_total = $this->montantTotal($session);

    return $this->container->get('templating')->renderResponse('PatFrontBundle:Reservation:inscription.html.twig', array('message' => $message,
        'formInscription' => $formInscription->createView(),
        'target_path' => 'pat_front_reservation_inscription',
        'appartement' => $appartement,
        'montant_total' => $montant_total
    ));
  }

  /**
   * Récapitulatif.
   *
   * @param Request $request
   * @return RedirectResponse
   * @throws NotFoundHttpException
   * @author small refactoring AG
   */
  public function recapitulatifAction(Request $request)
  {
    $session = $request->getSession();
    if (
      !$session->get('resa_date_debut') && !$session->get('resa_date_fin')
        && !$session->get('resa_appartement') && !$session->get('resa_arrhes')
        && !$session->get('resa_nbnuits') && !$session->get('resa_nbpersonne')
        && !$session->get('resa_locataire')
    ) {
      throw new NotFoundHttpException('Les informations de réservation sont introuvables');
    }

    $message = '';
    $user_context = $this->get('security.context')->getToken()->getUser();
    $parentReservationReference = $request->request->get('parent_reservation', null);
    $parentReservation = null;
    $session->set('resa_parent', null);

    $em = $this->get('doctrine')->getManager();

    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(
      array('id' => $session->get('resa_appartement'))
    );

    // On test la disponibilité de l'appartement
    if ($this->isDispo($session) == false) {
      $session->getFlashBag()->add(
        'warning', 'Désolé, ce bien n\'est actuellement plus disponible pour ces dates.'
      );

      return new RedirectResponse(
        $this->get('router')->generate(
          'pat_front_appartement_detail',
          array("url_bien" => $appartement->getUrlFr())
        )
      );
    }

    $media = $em->getRepository('PatCompteBundle:Media')->findOneBy(
      array('appartement' => $appartement->getId()),
      array('tri' => 'asc')
    );

    $locataire = $em->getRepository('PatUtilisateurBundle:Utilisateur')
      ->findOneBy(array('username' => $session->get('resa_locataire')));

    $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneBy(array(
      'appartement' => $appartement->getId(),
      'utilisateur' => $locataire->getId(),
      'date_debut' => $session->get('resa_date_debut'),
      'date_fin' => $session->get('resa_date_fin'),
      'tarif' => $session->get('resa_prix')
    ));

    if ($reservation) {
      // Si la réservation a déjà été validée mais pas encore payée
      if ($reservation->getStatus() == null) {
        return new RedirectResponse(
          $this->get('router')->generate('pat_front_reservation_paiement')
        );
      }
      // Si la réservation a déjà été validée et payée,$session->get('resa_locataire') on ne devrait pas se retrouver là vu que la session devrait avoir été vidée !!
      else {
        return new RedirectResponse(
          $this->get('router')->generate('fos_user_profile')
        );
      }
    }

    $nextStep = true;

    if (!empty($parentReservationReference)) {
      $parentReservation = $em->getRepository('PatCompteBundle:Reservation')
        ->findResaByReferenceAndAppartement($parentReservationReference, $appartement);

      if ($parentReservation === null) {
        $session->getFlashBag()->add(
          'error', 'Aucune réservation avec cette référence : '.$parentReservationReference
        );
        $nextStep = false;
      }
      else {
        $session->set('resa_parent', $parentReservation->getId());
      }
    }

//    if ($request->getMethod() == 'POST' && $parentReservation !== null
//        && $parentReservationReference !== $parentReservation->getReference()) {
//      $nextStep = false;
//    }
    if ($request->isMethod('POST') && $request->request->get('Details')
        && $parentReservationReference) {
      $nextStep = false;
    }

    if ($request->getMethod() == 'POST' && $request->request->get('Validation')
        && $nextStep === true) {

      if (($request->request->get('cgv') && $request->request->get('documents'))
          || (is_object($user_context) && $user_context->getType() == 'Administrateur')) {

        $siteTier = $request->request->get('site_tiers_resa');

        if (null !== $siteTier) {
          $siteTier = $em->getRepository('PatCompteBundle:SiteTiers')->find($siteTier);
        }

        $tarifManager = $this->get('pat.tarif_manager');
        $tab_calcul = array(
          'loyer' => round($session->get('resa_loyer'), 2),
          'tarif_roses' => $tarifManager->getPrixRoses(),
          'tarif_champagne' => $tarifManager->getPrixChampagne(),
          'tarif_lit_bebe' => $tarifManager->getPrixLitBebe(),
          'tarif_lit_bebe' => $tarifManager->getPrixMenage($appartement->getNbPieces())
        );
        $tab_calcul = array_merge($tab_calcul, $session->get('calcul_values'));

        // On inscrit la Réservation en Base
        $reservation = new Reservation();

        $reservation->setAppartement($appartement);
        $reservation->setStatus(ReservationTools::CONSTANT_STATUS_WAIT);
        $reservation->setUtilisateur($locataire);
        $reservation->setDateDebut($this->get('pat.date_functions')->toDateEn($session->get('resa_date_debut')));
        $reservation->setDateFin($this->get('pat.date_functions')->toDateEn($session->get('resa_date_fin')));
        $reservation->setTarif(round($this->montantTotal($session, false), 2));
        $reservation->setArrhes($session->get('resa_arrhes'));
        $reservation->setDepot(round($session->get('resa_depot')));
        $reservation->setNbPersonne($session->get('resa_nbpersonne'));
        $reservation->setNbAdulte($session->get('resa_nbadulte'));
        $reservation->setNbEnfant($session->get('resa_nbenfant'));
        $reservation->setNbLitSimple($session->get('resa_nb_lit_simple'));
        $reservation->setNbLitDouble($session->get('resa_nb_lit_double'));
        $reservation->setNbCanapeConvertible($session->get('resa_nb_canape_convertible'));
        $reservation->setParent($parentReservation);

        $isAutoPayment = false;

        
        $opt = array();
        $i = 0;
//                    $reservation->setOptionRoses($session->get('resa_opt_roses'));
        if ($session->get('resa_opt_roses') > 0) {
          $opt[$i] = new Option();
          $opt[$i]->setReservation($reservation);
          $opt[$i]->setQuantity($session->get('resa_opt_roses'));
          $opt[$i]->setName('Bouquet de 25 roses rouges');
          $opt[$i]->setAmount($tarifManager->getPrixRoses());
          $em->persist($opt[$i]);
          $i++;
        }

//                    $reservation->setOptionChampagne($session->get('resa_opt_champagne'));
        if ($session->get('resa_opt_champagne') > 0) {
          $opt[$i] = new Option();
          $opt[$i]->setReservation($reservation);
          $opt[$i]->setQuantity($session->get('resa_opt_champagne'));
          $opt[$i]->setName('Bouteille de champagne');
          $opt[$i]->setAmount($tarifManager->getPrixChampagne());
          $em->persist($opt[$i]);
          $i++;
        }

//                    $reservation->setOptionMenageSupp($session->get('resa_opt_menage'));
        if ($session->get('resa_opt_menage') > 0) {
          $opt[$i] = new Option();
          $opt[$i]->setReservation($reservation);
          $opt[$i]->setQuantity($session->get('resa_opt_menage'));
          $opt[$i]->setName('Ménage supplémentaire');
          $opt[$i]->setAmount($tarifManager->getPrixMenage($reservation->getAppartement()->getNbPieces()));
          $em->persist($opt[$i]);
          $i++;
        }

//                    $reservation->setOptionLitBebe($session->get('resa_opt_lit_bebe'));
        if ($session->get('resa_opt_lit_bebe') == true) {
          $opt[$i] = new Option();
          $opt[$i]->setReservation($reservation);
          $opt[$i]->setQuantity(1);
          $opt[$i]->setName('Location lit bébé');
          $opt[$i]->setAmount($tarifManager->getPrixLitBebe());
          $em->persist($opt[$i]);
          $i++;
        }

//                    $reservation->setOptionTaxi($session->get('resa_opt_taxi'));
        if ($session->get('resa_opt_taxi') == true) {
          $opt[$i] = new Option();
          $opt[$i]->setReservation($reservation);
          $opt[$i]->setQuantity(1);
          $opt[$i]->setName("Taxi à l'arrivée");
          $opt[$i]->setAmount(null);
          $em->persist($opt[$i]);
          $i++;
        }

//                    $reservation->setOptionFrigo($session->get('resa_opt_frigo'));
        if ($session->get('resa_opt_frigo') == true) {
          $opt[$i] = new Option();
          $opt[$i]->setReservation($reservation);
          $opt[$i]->setQuantity(1);
          $opt[$i]->setName("Remplissage du frigo");
          $opt[$i]->setAmount(null);
          $em->persist($opt[$i]);
          $i++;
        }

        if ($session->get('resa_opt_supplementaires') == true) {
          $reservation->setOptionSupplementaires(true);
        }
        else {
          $reservation->setOptionSupplementaires(false);
        }

        $reservation->setCalculValues($tab_calcul);

        if (is_object($user_context) && $user_context->getType() == 'Administrateur') {
          // Site tier
          if (null !== $siteTier) {
            $reservation->setSiteTiers($siteTier);

            if (true === $siteTier->isAutoPayment()) {
              $reservation->setStatus(ReservationTools::CONSTANT_STATUS_DEPOSIT_PAYED);
              $em->flush();
              $this->container->get('pat.reservation_manager')->updateStatus($reservation);
              $isAutoPayment = true;
            }
          }

          // Promotion
          $promo = (float) $request->request->get('promo_resa');
          $reservation->setPromotion($promo);
        }


        // on check si le réservation est autorisée (même si, normalement si la reservation n'est pas autorisée on ne peut accéder à cette page)
        if ($appartement->getIsResa() == 1) {

          $em->persist($reservation);
          $em->flush();

          $reservation->generateRef();
          $em->flush();

          // on stocke l'id de la réservation
          $session->set('resa_id', $reservation->getId());

          // Si c'est l'administrateur, on envoi un mail au locataire
          if ($user_context && $user_context != 'anon.') {
            if ($user_context->getType() == 'Administrateur') {
              if (true === $isAutoPayment) {
                $session->getFlashBag()->add(
                  'success', 'La réservation a bien été effectuée'
                );

                return new RedirectResponse(
                  $this->get('router')->generate('pat_admin_dashboard')
                );
              }
              $tarifManager = $this->get('pat.tarif_manager');
              $tab_calcul = array(
                'loyer' => round($session->get('resa_loyer'), 2),
                'tarif_roses' => $tarifManager->getPrixRoses(),
                'tarif_champagne' => $tarifManager->getPrixChampagne(),
                'tarif_lit_bebe' => $tarifManager->getPrixLitBebe(),
                'tarif_lit_bebe' => $tarifManager->getPrixMenage($appartement->getNbPieces())
              );
            
              $tab_calcul = array_merge($tab_calcul, $session->get('calcul_values'));

              
              $reservation->setCalculValues($tab_calcul);
              $em->persist($reservation);
              $em->flush();

              return new RedirectResponse(
                $this->get('router')->generate('pat_admin_reservation_send_to_locataire')
              );
            }
          }

          return new RedirectResponse(
            $this->get('router')->generate('pat_front_reservation_paiement')
          );
        }
        else {
          $message = 'Cet appartement n\'est pas disponible à la réservation';
        }
      }
      else {
        if (!$request->request->get('cgv')) {
          $session->getFlashBag()->add(
            'error', 'Vous devez accepter les Conditions Générales De Vente'
          );
        }

        if (!$request->request->get('documents')) {
          $session->getFlashBag()->add(
            'error', 'Vous devez confirmer disposer des documents nécessaires'
          );
        }
      }
    }

    $siteTiers = $bookingList = null;
    if (is_object($user_context) && $user_context->getType() == 'Administrateur') {
      $siteTiers = $em->getRepository('PatCompteBundle:SiteTiers')->findAll();
      $bookingList = $em->getRepository('PatCompteBundle:Reservation')
        ->listForApartment($appartement->getId());
    }

    $montant_total = $this->montantTotal($session);

    return $this->get('templating')->renderResponse(
      'PatFrontBundle:Reservation:recapitulatif.html.twig',
      [
        'message' => $message,
        'locataire' => $locataire,
        'appartement' => $appartement,
        'montant_total' => $montant_total,
        'media' => $media,
        'siteTiers' => $siteTiers,
        'parentReservation' => $parentReservation,
        'bookingList' => $bookingList,
      ]
    );
  }

  /**
   * Page de choix du mode de paiement.
   *
   * @param Request $request
   * @param integer $resa_id
   * @return Response
   * @throws NotFoundHttpException
   * @author refactoring AG
   */
  public function paiementAction(Request $request, $resa_id = null)
  {
    $session = $request->getSession();

    $resaId = $resa_id ? $resa_id : $session->get('resa_id');

    if (!$resaId) {
      throw new NotFoundHttpException('Les informations de réservation sont introuvables.');
    }

    // On supprime les réservation invalides.
    $this->cleanReservations();

    /* @var $repo ReservationRepository */
    $repo = $this->get('doctrine')->getRepository('PatCompteBundle:Reservation');
    /* @var $reservation Reservation */
    $reservation = $repo->findOneBy(array('id' => $resaId));

    if ($reservation) {
      $appartement = $reservation->getAppartement();

      $formPaymentMode = $this->createForm(new PaymentModeForm(), $reservation);

      $viewData = array(
        'reservation' => $reservation,
        'appartement' => $appartement,
        'formPaymentMode' => $formPaymentMode->createView(),
        'etransactions' => $this->container->getParameter('etransactions'),
        'formData' => Etransactions::prepareDataForReservation(
          $this->container->getParameter('etransactions'),
          $this->container->get('router'),
          $reservation
        )
      );

      return $this->render('PatFrontBundle:Reservation:paiement.html.twig', $viewData);
    }
    else {
      return $this->render('PatFrontBundle:Reservation:sessionExpire.html.twig');
    }
  }

  public function clearSession(Request $request)
  {
    $session = $request->getSession();

    $session->remove('resa_id');
    $session->remove('resa_parent');
    $session->remove('resa_date_debut');
    $session->remove('resa_date_fin');
    $session->remove('resa_appartement');
    $session->remove('resa_prix');
    $session->remove('resa_arrhes');
    $session->remove('resa_nbnuits');
    $session->remove('resa_nbpersonne');
    $session->remove('resa_nbadulte');
    $session->remove('resa_nbenfant');
    $session->remove('resa_nb_lit_simple');
    $session->remove('resa_nb_lit_double');
    $session->remove('resa_nb_canape_convertible');
    $session->remove('resa_locataire');
    $session->remove('path_spec');
  }

  /**
   * Page de retour de paiement de la banque, côté client, pour les état succès, refusé, annulé.
   *
   * @param Request $request
   * @param string $status
   * @return Response
   * @throws NotFoundHttpException
   * @author refactoring AG
   */
  public function confirmationAction(Request $request, $status = null)
  {
    $session = $request->getSession();

    // On arrive en POST si l'on choisit le paiement par chèque ou par virement
    if ($request->getMethod() == 'POST') {

      $resa_arrhes = $session->get('resa_arrhes');

      // on controle qu'une réservation a bien eu lieu
      if ($session->get('resa_id') && !is_null($resa_arrhes)) {
        $em = $this->get('doctrine')->getManager();
        $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneBy(array(
          'id' => $session->get('resa_id')
        ));

        if ($reservation) {
          $formPaymentMode = $this->createForm(new PaymentModeForm(), $reservation);

          $formPaymentMode->bind($request);

          if ($formPaymentMode->isValid()) {
            // On set le statut en attente
            $reservation->setStatus(ReservationTools::CONSTANT_STATUS_WAIT);

            $this->createDownpaymentForBooking($reservation);

            $status = $reservation->getModePaiement();

            $this->sendConfirmationEmailToTenant($reservation, $status);

            // On envoie le mail de demande de validation de réservation au propriétaire
            if ($status != 'virement') {
              $this->sendValidationEmailToOwner($reservation, $status);
            }

            return $this->render(
              'PatFrontBundle:Reservation:confirmation.html.twig',
              compact('status')
            );
          }
          else {
            return $this->redirect($this->generateUrl('pat_front_reservation_paiement'));
          }
        }
        else {
          return $this->render('PatFrontBundle:Reservation:sessionExpire.html.twig');
        }
      }
      else {
        throw new NotFoundHttpException('Les informations de réservation sont introuvables.');
      }
    }
    // Sinon l'on revient du paiement sécurisé par paybox
    else {
      /* AG
      $query = $request->query->all();
      $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneByReference($query['ref']);

      if (!$reservation) {
        throw new NotFoundHttpException('Réservation non trouvée');
      }
      $reservation->setModePaiement('CB');
      $reservation->setPaymentResult($query);

      return $this->render('PatFrontBundle:Reservation:confirmation.html.twig', array(
        'status' => 'effectue'
      ));
      */
      if (!in_array($status, array('effectue', 'refuse', 'annule'))) {
        throw new NotFoundHttpException('Etat de retour incorrect.');
      }

      $transactionResponse = Etransactions::getResponseMessage($request->query->get('reponse'));

      // Vraiment utile ?
      if ($status == 'annule') {
        $this->paymentCanceled($transactionResponse);
      }

      return $this->render(
        'PatFrontBundle:Reservation:confirmation.html.twig',
        compact('status', 'transactionResponse')
      );
    }
  }

  /**
   * Crée un paiement pour l'acompte.
   *
   * @param \Reservation $booking
   * @author refactoring AG
   */
  protected function createDownpaymentForBooking($booking, $status)
  {
    $em = $this->get('doctrine')->getManager();

    // On crée un objet Payment correspondant au paiement de l'acompte
    $payment = new Payment();
    $payment->setReservation($booking);
    $payment->setMember($booking->getUtilisateur());
    $payment->setAmount($booking->getArrhes());
    $payment->setName('Acompte réservation réf. '.$booking->getReference());
    $payment->setModePaiement($booking->getModePaiement());
    $payment->setStatus(PaymentTools::CONSTANT_STATUS_WAIT_RECEIPT);

    $em->persist($payment);
    $em->flush();
  }


  /**
   * Envoie un courriel de confirmation au locataire.
   *
   * @param \Reservation $reservation
   * @param string $status
   * @author refactoring AG
   */
  protected function sendConfirmationEmailToTenant($reservation, $status)
  {
    // Envoi du mail de confirmation
    $emailMessage = \Swift_Message::newInstance()
      ->setFrom('contact@class-appart.com')
      ->setTo($reservation->getUtilisateur()->getEmail())
      ->setBcc($this->container->getParameter('mail_admin'));

    $secondEmail = $reservation->getUtilisateur()->getSecondEmail();
    if ($secondEmail) {
      $emailMessage->setCc($secondEmail);
    }

    $params = compact('reservation');

    if ($status == 'virement') {
      $emailMessage->setSubject(
        'Votre demande de réservation '.$reservation->getReference()
        .' chez Class Appart : paiement par virement bancaire'
      );
      $textBody = $this->renderView(
        'PatFrontBundle:Reservation:mail_demande_virement.txt.twig',
        $params
      );
      $htmlBody = $this->renderView(
        'PatFrontBundle:Reservation:mail_demande_virement.html.twig',
        $params
      );
    }
    else {
      $emailMessage->setSubject(
        'Votre demande de réservation '.$reservation->getReference()
        .' chez Class Appart : paiement par chèque'
      );
      $textBody = $this->renderView(
        'PatFrontBundle:Reservation:mail_demande_cheque.txt.twig',
        $params
      );
      $htmlBody = $this->renderView(
        'PatFrontBundle:Reservation:mail_demande_cheque.html.twig',
        $params
      );
    }

    if (!empty($htmlBody)) {
      $emailMessage
        ->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain');
    }
    else {
      $emailMessage->setBody($textBody);
    }

    $this->get('mailer')->send($emailMessage);
  }

  /**
   * Envoie un courriel de validation au propriétaire.
   *
   * @param \Reservation $reservation
   * @author refactoring AG
   */
  protected function sendValidationEmailToOwner($reservation)
  {
    $emailMessage = \Swift_Message::newInstance()
      ->setSubject('Confirmation de réservation Class Appart')
      ->setFrom('contact@class-appart.com')
      ->setBcc($this->container->getParameter('mail_admin'))
      ->setTo($reservation->getAppartement()->getUtilisateur()->getEmail());

    $secondEmail = $reservation->getAppartement()->getUtilisateur()->getSecondEmail();
    if ($secondEmail) {
      $emailMessage->setCc($secondEmail);
    }

    $params = compact('reservation');

    $textBody = $this->renderView(
      'PatCompteBundle:Reservation:email_valid_prop.txt.twig',
      $params
    );
    $htmlBody = $this->renderView(
      'PatCompteBundle:Reservation:email_valid_prop.html.twig',
      $params
    );

    if (!empty($htmlBody)) {
      $emailMessage
        ->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain');
    }
    else {
      $emailMessage->setBody($textBody);
    }

    $this->get('mailer')->send($emailMessage);
  }

  /**
   * Envoie un message lorsqu'un client annule sa transaction bancaire.
   *
   * @param string $transactionResponse
   * @throws NotFoundHttpException
   * @author refactoring AG
   */
  protected function paymentCanceled($transactionResponse = null)
  {
    $em = $this->get('doctrine')->getManager();

    $data = $this->getRequest()->query->all();

    $error = 'Erreur : La transaction a été annulée';

    $reservation = $em->getRepository('PatCompteBundle:Reservation')->findOneByReference($data['ref']);

    // si on ne trouve pas la réservation, on envoi un mail contenant les informations de paiement
    if (!$reservation) {
      throw new NotFoundHttpException('Réservation non trouvée.');
    }

    //$reservation->setStatus(ReservationTools::CONSTANT_STATUS_CANCELED_BY_BANK);
    $reservation->setPaymentResult($data);
    $reservation->setPaymentError($error);

    $em->flush();

    $emailMessage = \Swift_Message::newInstance()
      ->setSubject('Annulation du paiement par carte bancaire sur class-appart.com')
      ->setFrom('contact@class-appart.com')
      ->setBcc($this->container->getParameter('mail_admin'))
      ->setTo($reservation->getUtilisateur()->getEmail());

    $secondEmail = $reservation->getUtilisateur()->getSecondEmail();
    if ($secondEmail) {
      $emailMessage->setCc($secondEmail);
    }

    $params = compact('reservation', 'transactionResponse');

    $textBody = $this->renderView(
      'PatFrontBundle:Reservation:mail_annulation_CB.txt.twig',
      $params
    );
    $htmlBody = $this->renderView(
      'PatFrontBundle:Reservation:mail_annulation_CB.html.twig',
      $params
    );

    if (!empty($htmlBody)) {
      $emailMessage
        ->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain');
    }
    else {
      $emailMessage->setBody($textBody);
    }

    $this->container->get('mailer')->send($emailMessage);
  }

  /**
   * Supprime les réservations périmées.
   */
  public function cleanReservations()
  {
    $em = $this->get('doctrine')->getManager();

    // On nettoie les réservations non terminées
    $resas_invalides = $em->getRepository('PatCompteBundle:Reservation')->getResaInvalide();
    if ($resas_invalides) {
      foreach ($resas_invalides as $resa) {
        $em->remove($resa);
      }
      $em->flush();
    }
  }

  /**
   * Sur annulation de l'utilisateur, on revient à la page de paiement de la résa.
   *
   * @param Request $request
   * @return RedirectResponse
   * @author AG
   */
  /*
  public function bankTransactionCanceledAction(Request $request)
  {
    $session = $request->getSession();
    if (!$session->get('resa_id')) {
      $session->set('resa_id', $request->query->get('ref'));
    }
    return $this->redirect($this->generateUrl('pat_front_reservation_paiement'));
  }
  */

  /**
   * Sur refus de la banque, on revient à la page de paiement de la résa.
   *
   * @param Request $request
   * @return RedirectResponse
   * @author AG
   */
  /*
  public function bankTransactionRefusedAction(Request $request)
  {
    $session = $request->getSession();
    if (!$session->get('resa_id')) {
      $session->set('resa_id', $request->query->get('ref'));
    }
    $session->getFlashBag()->add(
      'error',
      'Erreur de transaction'
    );
    $session->getFlashBag()->add(
      'error',
      Etransactions::getResponseMessage($request->query->get('reponse'))
    );
    return $this->redirect($this->generateUrl('pat_front_reservation_paiement'));
  }
  */

}
