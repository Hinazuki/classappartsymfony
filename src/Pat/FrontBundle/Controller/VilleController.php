<?php

namespace Pat\FrontBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;
use Pat\CompteBundle\Entity\Appartement;
use Pat\FrontBundle\Form\AppartementRechercheForm;

class VilleController extends ContainerAware
{

  // Fonction qui est appelée en Ajax pour afficher une liste de villes en fonction de caractères tapés par l'utilisateur.
  public function listeVilleAction()
  {
    $ville = '';
    $request = $this->container->get('request');
    $formRecherche = $this->container->get('form.factory')->create(new AppartementRechercheForm());

    if ($request->getMethod() == 'POST') {
      $formRecherche->bind($request);
    }

    if ($request->isXmlHttpRequest()) {

      $ville = $request->request->get('ville');

      $em = $this->container->get('doctrine')->getManager();

      if ($ville != '') {
        $villes = $em->getRepository('PatCompteBundle:Ville')->getVillesByCaract($ville);
      }
      else {
        $villes = null;
      }
    }
    else {
      $villes = null;
    }
    return $this->container->get('templating')->renderResponse('PatFrontBundle:Ville:ajaxListe.html.twig', array(
        'villes' => $villes
    ));
  }

  public function listeAllVilleAction()
  {
    $ville = '';
    $request = $this->container->get('request');

    if ($request->isXmlHttpRequest()) {

      $ville = $request->request->get('ville');

      $em = $this->container->get('doctrine')->getManager();

      if ($ville != '') {
        $villes = $em->getRepository('PatCompteBundle:Ville')->getAllVillesByCaract($ville);
      }
      else {
        $villes = null;
      }
    }
    else {
      $villes = null;
    }
    return $this->container->get('templating')->renderResponse('PatCompteBundle:AdminVille:ajaxListeAjoutAppart.html.twig', array(
        'villes' => $villes
    ));
  }

}
