<?php

namespace Pat\FrontBundle\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Pat\CompteBundle\Entity\Contenu;
use Pat\CompteBundle\Entity\Ville;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ContenuController extends ContainerAware
{

  //affichage des actus sur la page d'accueil
  public function pageMiniListeAction()
  {
    $em = $this->container->get('doctrine')->getManager();

    // On cherche la page ayant une mini liste sur l'accueil
    $page = $em->getRepository('PatCompteBundle:Contenu')->findOneBy(array("on_sidebar" => 1));
    $messages = null;

    if ($page) {
      // On affiche seulement 5 messages
      $messages = $em->getRepository('PatCompteBundle:Contenu')->findBy(array("id_parent" => $page->getId()), array("tri" => "ASC"), 3);
    }
    if ($messages) {
      // On découpe les messages pour en aficher qu'une partie
      foreach ($messages as $message):
        $message->setDescription($this->reduitChaine($message->getDescription(), 100));
      endforeach;
    }

    return $this->container->get('templating')->renderResponse('PatFrontBundle:Contenu:pageMiniListe.html.twig', array('page' => $page, 'messages' => $messages)
    );
  }

  // Affiche tous les messages d'une page
  public function pageAction($url_page = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    $page = $em->getRepository('PatCompteBundle:Contenu')->findOneBy(array("url" => $url_page, "statut" => "1"));
    if (!$page) {
      throw new NotFoundHttpException("Cette page n'est pas disponible");
    }
    if ($page->getUrl() == "accueil") {
      return new RedirectResponse($this->container->get('router')->generate('PatFrontBundle_homepage'));
    }


    $messages = $em->getRepository('PatCompteBundle:Contenu')->findBy(array("id_parent" => $page->getId(), "type" => "Message", "statut" => "1"), array("tri" => "ASC"));
    if ($messages) {
      // On découpe les messages pour en aficher qu'une partie
      foreach ($messages as $message):
        $message->setDescription($this->reduitChaine($message->getDescription(), 300));
      endforeach;
    }

    $paginator = $this->container->get('knp_paginator');
    $pagination = $paginator->paginate(
      $messages, $this->container->get('request')->query->get('page', 1), 20
    );

    return $this->container->get('templating')->renderResponse('PatFrontBundle:Contenu:page.html.twig', array('page' => $page, 'messages' => $pagination)
    );
  }

  public function messageAction($url_page = null, $url_message = null)
  {
    $em = $this->container->get('doctrine')->getManager();

    $page = $em->getRepository('PatCompteBundle:Contenu')->findOneBy(array("url" => $url_page, "statut" => "1"));
    if (!$page) {
      throw new NotFoundHttpException("Cette page n'est pas disponible");
    }

    $message = $em->getRepository('PatCompteBundle:Contenu')->findOneBy(array("url" => $url_message, "id_parent" => $page->getId(), "statut" => "1"));
    if (!$message) {
      throw new NotFoundHttpException("Cette article n'est pas disponible");
    }
//        $message->setCreatedAd($message->getCreatedAd()->format('d/m'));

    return $this->container->get('templating')->renderResponse('PatFrontBundle:Contenu:message.html.twig', array('page' => $page, 'message' => $message)
    );
  }

  // Affiche le contenu principal de la page d'accueil
  public function indexAction()
  {
    $em = $this->container->get('doctrine')->getManager();

    $accueil = $em->getRepository('PatCompteBundle:Contenu')->findById(5);
    if (!$accueil) {
      $description = "Aucune information";
    }
    else {
      $description = $accueil[0]->getDescription();
    }

    return $this->container->get('templating')->renderResponse('PatFrontBundle:Contenu:index.html.twig', array('contenu' => $description)
    );
  }

  //affiche le menu principal en haut de page
  public function menuAction()
  {
    $request = $this->container->get('request');
    $menu = array();

    /** @var EntityManager $em */
    $em = $this->container->get('doctrine')->getManager();
    $menu_titres = $em->getRepository('PatCompteBundle:Contenu')->findBy(array('type' => 'Page', 'affiche_menu' => '1', 'id_parent' => '0', 'langue' => 'fr', 'statut' => '1'), array('tri' => 'ASC'));

    $i = 0;
    $j = 0;
    foreach ($menu_titres as $titre):
      $menu[$i] = array();
      $menu[$i]['titre'] = $titre->getTitre();
      $menu[$i]['url'] = $titre->getUrl();
      $menu[$i]['sous_titre'] = array();
      $menu[$i]['id'] = $titre->getId();
      $childActive = false;

      $sous_titres = $em->getRepository('PatCompteBundle:Contenu')->findBy(array('type' => 'Page', 'affiche_menu' => '1', 'id_parent' => $titre->getId(), 'langue' => 'fr', 'statut' => '1'), array('tri' => 'ASC'));
      $j = 0;
      foreach ($sous_titres as $titre):
        if ($titre->getUrl() == $request->attributes->get('attributes')->get('url_page')) {
          $childActive = true;
        }
        $menu[$i]['sous_titre'][$j] = array();
        $menu[$i]['sous_titre'][$j]['titre'] = $titre->getTitre();
        $menu[$i]['sous_titre'][$j]['url'] = $titre->getUrl();
        $j++;
      endforeach;
      if (empty($sous_titres) && $titre->getUrl() == $request->attributes->get('attributes')->get('url_page')) {
        $childActive = true;
      }

      $menu[$i]['isVisit'] = $childActive;
      $i++;
    endforeach;

    return $this->container->get('templating')->renderResponse('PatFrontBundle:Contenu:menu.html.twig', array('menu' => $menu,
        'attributes' => $request->attributes->get('attributes'),
        'route' => $request->attributes->get('attributes')->get('_route'))
    );
  }

  //affiche le menu principal en haut de page
  public function sousMenuAction()
  {
    $request = $this->container->get('request');
    $routeParam = $request->attributes->get('attributes')->get('_route_params');
    $urlPage = !empty($routeParam['url_page']) ? $routeParam['url_page'] : '';
    $siblings = array();
    $subMenu = array();


    $em = $this->container->get('doctrine')->getManager();

    // Find id from actual page
    $idParentActualPage = $em->getRepository('PatCompteBundle:Contenu')->findOneBy(array('type' => 'Page', 'affiche_menu' => '1', 'langue' => 'fr', 'statut' => '1', 'url' => $urlPage), array('tri' => 'ASC'));


    if (!empty($idParentActualPage))
      $siblings = $em->getRepository('PatCompteBundle:Contenu')->findBy(array('type' => 'Page', 'affiche_menu' => '1', 'id_parent' => $idParentActualPage->getIdParent(), 'langue' => 'fr', 'statut' => '1'), array('tri' => 'ASC'));

    if (!empty($siblings))
      foreach ($siblings as $sibling) {
        if ($sibling->getIdParent() != 0) {
          $tempArray = array();
          $tempArray['titre'] = $sibling->getTitre();
          $tempArray['url'] = $sibling->getUrl();
          $tempArray['id'] = $sibling->getId();
          $tempArray['isVisit'] = $urlPage == $sibling->getUrl() ? true : false;

          $subMenu[] = $tempArray;
        }
      }

    return $this->container->get('templating')->renderResponse('PatFrontBundle:Contenu:sousMenu.html.twig', array('subMenu' => $subMenu,
        'attributes' => $request->attributes->get('attributes'))
    );
  }

  //découpe la chaine passée en paramètre si > $max
  public function reduitChaine($chaine, $max)
  {
    if (strlen($chaine) >= $max) {
      $chaine = substr($chaine, 0, $max);
      $espace = strrpos($chaine, " ");
      if ($espace) {
        $chaine = substr($chaine, 0, $espace);
      }
      $chaine .= '...';
    }
    return $chaine;
  }

}
