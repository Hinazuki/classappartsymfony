<?php

namespace Pat\FrontBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Pat\CompteBundle\Entity\Documents;

class DocumentsController extends Controller
{

  // <summary>
  // Téléchargement d'un document
  // <summary>
  public function DownloadFileAction($fileid)
  {

    $em = $this->container->get('doctrine')->getManager();
    $document = $em->getRepository('PatCompteBundle:Documents')->findOneBy(array('id' => $fileid, 'visible' => 1));

    if (!$document) {
      throw $this->createNotFoundException('Unable to find Document entity.');
    }

    $file = $document->getFile();
    $helper = $this->get('vich_uploader.templating.helper.uploader_helper');
    $path = $this->get('kernel')->getRootDir().'/..'.$helper->asset($document, 'document_file');

    // Opening file
    $content = file_get_contents($path);
    $response = new Response();
    $response->headers->set('Content-Type', $file->getMimeType());
    $response->headers->set('Content-Disposition', 'attachment;filename="'.$document->getDocumentName().'.'.$file->getExtension());

    $response->setContent($content);

    return $response;
  }

}
