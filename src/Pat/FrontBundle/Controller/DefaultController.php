<?php

namespace Pat\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

  public function indexAction()
  {
    return $this->render('PatFrontBundle::layout.html.twig');
  }

}
