<?php

namespace Pat\FrontBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;
use Pat\FrontBundle\Form\FormulaireContactForm;
use Pat\CompteBundle\Entity\Formulaire;
use Pat\FrontBundle\Form\ProposerBienForm;
use Pat\FrontBundle\Entity\ProposerBien;
use Pat\FrontBundle\Form\DemandeDevisForm;
use Pat\FrontBundle\Entity\DemandeDevis;
use Pat\CompteBundle\Entity\Ville;

class FormulaireController extends ContainerAware
{

  public function demandeDevisAction()
  {
    $formulaire = new DemandeDevis;

    $request = $this->container->get('request');

    $session = $request->getSession();

    if (!$session->get('rand1')) {
      $rand1 = rand(1, 9);
      $rand2 = rand(1, 9);
      $session->set('rand1', $rand1);
      $session->set('rand2', $rand2);
    }

    $form = new DemandeDevisForm(
      $this->container->get('translator'), $session->get('rand1'), $session->get('rand2')
    );
    $formDevis = $this->container->get('form.factory')->create($form, $formulaire);

    if ($request->getMethod() == 'POST') {

      $formDevis->handleRequest($request); // Pour concerver les infos dans le formulaire

      if ($formDevis->isValid()) {
        $data = $formDevis->getData();

        $message = \Swift_Message::newInstance()
          ->setSubject("Demande de devis chez Class Appart")
          ->setFrom('contact@class-appart.com')
          ->setTo($data->getEmail())
          ->setBcc(array('info@class-appart.com'));
        $htmlBody = $this->container->get('templating')->render('PatFrontBundle:Formulaire:email_demandeDevis.html.twig', array('form' => $data));

        $message->setBody($htmlBody, 'text/html');

        if ($this->container->get('mailer')->send($message)) {
          $route = $this->container->get('router')->generate('pat_front_formulaire_demande_devis_valid');
        }

        $session->remove('rand1');
        $session->remove('rand2');
        return new RedirectResponse($route);
      }
    }
    // On affiche le formulaire
    return $this->container->get('templating')->renderResponse(
        'PatFrontBundle:Formulaire:demandeDevis.html.twig', array(
        'formDemande' => $formDevis->createView(),
        'question' => $form->getQuestion()
        )
    );
  }

  public function emailDemandeDevisValidAction()
  {
    return $this->container->get('templating')->renderResponse('PatFrontBundle:Formulaire:emailDemandeDevisValid.html.twig');
  }

  public function emailProposerBienValidAction()
  {
    return $this->container->get('templating')->renderResponse('PatFrontBundle:Formulaire:emailProposerBienValid.html.twig');
  }

  public function proposerBienAction($formule = null)
  {
    $formulaire = new ProposerBien;

    if ($formule == 'standard') {
      $formulaire->setFormule('Standard');
    }
    elseif ($formule == 'premium') {
      $formulaire->setFormule('Premium');
    }

    $request = $this->container->get('request');

    $session = $request->getSession();

    if (!$session->get('rand1')) {
      $rand1 = rand(1, 9);
      $rand2 = rand(1, 9);
      $session->set('rand1', $rand1);
      $session->set('rand2', $rand2);
    }

    $form = new ProposerBienForm(
      $this->container->get('translator'), $session->get('rand1'), $session->get('rand2')
    );
    $formProposer = $this->container->get('form.factory')->create($form, $formulaire);

    if ($request->getMethod() == 'POST') {

      $formProposer->handleRequest($request);

      if ($formProposer->isValid()) {
        $data = $formProposer->getData();

        $message = \Swift_Message::newInstance()
          ->setSubject("Proposition de bien(s) chez Class Appart")
          ->setFrom('contact@class-appart.com')
          ->setTo($data->getEmail())
          ->setBcc(array('proprietaire@class-appart.com'));
        $htmlBody = $this->container->get('templating')->render(
          'PatFrontBundle:Formulaire:email_proposerBien.html.twig', array('form' => $data)
        );

        $message->setBody($htmlBody, 'text/html');

        if ($this->container->get('mailer')->send($message)) {
          $route = $this->container->get('router')->generate('pat_front_formulaire_proposer_bien_valid');
        }

        $session->remove('rand1');
        $session->remove('rand2');
        return new RedirectResponse($route);
      }
    }
    // On affiche le formulaire
    return $this->container->get('templating')->renderResponse(
        'PatFrontBundle:Formulaire:proposerBien.html.twig', array(
        'formProposer' => $formProposer->createView(),
        'question' => $form->getQuestion()
        )
    );
  }

  public function contactAction()
  {
    $em = $this->container->get('doctrine')->getManager();

    $formulaire = new Formulaire();

    $request = $this->container->get('request');

    if ($request->request->get('frontintroreservation')) {
      $formResa = $request->request->get('frontintroreservation');
      $ref = !empty($formResa['ref']) ? $formResa['ref'] : "";
      $debutResa = !empty($formResa['datedebut']) ? $formResa['datedebut'] : "";
      $arriveeResa = !empty($formResa['datefin']) ? $formResa['datefin'] : "";

      $formulaire->objet = "[$ref] Réservation du bien du $debutResa au $arriveeResa";
    }

    $session = $request->getSession();

    if (!$session->get('rand1')) {
      $rand1 = rand(1, 9);
      $rand2 = rand(1, 9);
      $session->set('rand1', $rand1);
      $session->set('rand2', $rand2);
    }

    $form = new FormulaireContactForm(
      $this->container->get('translator'), $session->get('rand1'), $session->get('rand2')
    );
    $formContact = $this->container->get('form.factory')->create($form, $formulaire);
//        $humanOptions = $formContact->get('human')->getConfig()->getOptions();

    if ($request->getMethod() == 'POST' && $request->request->get('SendContact')) {

      $formContact->handleRequest($request);

      if ($formContact->isValid()) {
        $data = $formContact->getData();

        $formulaire->setType("Contact");
        $formulaire->setCreatedAt(date("Y-m-d H:i:s"));

        $em->persist($formulaire);
        $em->flush();
        $message = \Swift_Message::newInstance()
          ->setSubject("Formulaire Contact - classAppart")
          ->setFrom('contact@class-appart.com')
//                    ->setTo('fp@laboitededev.com')
          ->setTo($data->getEmail())
          ->setBcc(array('info@class-appart.com'));
        $textBody = $this->container->get('templating')->render(
          'PatFrontBundle:Formulaire:email_contact.txt.twig', array('contact' => $data)
        );
        $htmlBody = $this->container->get('templating')->render(
          'PatFrontBundle:Formulaire:email_contact.html.twig', array('contact' => $data)
        );

        if (!empty($htmlBody)) {
          $message
            ->setBody($htmlBody, 'text/html')
            ->addPart($textBody, 'text/plain');
        }
        else {
          $message->setBody($textBody);
        }

        if ($this->container->get('mailer')->send($message)) {
          $route = $this->container->get('router')->generate('pat_front_formulaire_email_success');
        }
        else {
          $route = $this->container->get('router')->generate('pat_front_formulaire_email_failed');
        }

        $session->remove('rand1');
        $session->remove('rand2');
        return new RedirectResponse($route);
      }
    }

    // On affiche le formulaire
    return $this->container->get('templating')->renderResponse(
        'PatFrontBundle:Formulaire:formulaireContact.html.twig', array(
        'formContact' => $formContact->createView(),
        'question' => $form->getQuestion()
        )
    );
  }

  public function emailSuccessAction()
  {
    return $this->container->get('templating')->renderResponse('PatFrontBundle:Formulaire:emailSuccess.html.twig');
  }

  public function emailFailedAction()
  {
    return $this->container->get('templating')->renderResponse('PatFrontBundle:Formulaire:emailFailed.html.twig');
  }

}
