<?php

namespace Pat\FrontBundle\Controller;

use Pat\CompteBundle\Lib\CalendrierAppartement;
use Pat\CompteBundle\Lib\RechercheAppartement;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Pat\CompteBundle\Entity\Appartement;
use Pat\FrontBundle\Form\AppartementRechercheForm;
//use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
//use Doctrine\Common\Collections\ArrayCollection;
//use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Pat\FrontBundle\Lib\Ics;

class AppartementController extends Controller
{

  // Fonction qui liste les appartements en fonction du formulaire de recherche
  public function indexAction(Request $request, $message = null)
  {
    // Variables
    $liste_appart = "";
    $appartement = null;
    $appart_in_selection = null;
    $data_form = null;
    $em = $this->container->get('doctrine')->getManager();
    $session = $request->getSession();

    // On récupère du cookie les appartements mis en sélection par l'utilisateur
    $cookies = $request->cookies;
    if ($cookies->has('PATcookie')) {
      $recup = explode("-", $cookies->get('PATcookie'));
      $appart_in_selection = $em->getRepository('PatCompteBundle:Appartement')->getManyAppartementById($recup, null);
    }

    $formRecherche = $this->container->get('form.factory')->create(new AppartementRechercheForm());

    if ($request->getMethod() == 'POST') {

      $formRecherche->bind($request);

      if ($formRecherche->isValid()) {
        $data_form = $formRecherche->getData();

        // On contrôle le contenu des champs date
        $error_arr = 1;
        if (!is_null($data_form['datearrivee'])) {
          $test = explode("/", $data_form['datearrivee']);

          if (checkdate($test[1], $test[0], $test[2]))
            $error_arr = 0;
        }

        $error_dep = 1;
        if (isset($data_form['datedepart'])) {
          $test = explode("/", $data_form['datedepart']);
          if (checkdate($test[1], $test[0], $test[2]))
            $error_dep = 0;
        }

        if ($error_arr == 0 && $error_dep == 0) {
          if ($data_form) {
            // On sauvegarde la recherche dans la session
            $date_debut = $data_form['datearrivee'];
            $session->set('date_debut', $date_debut);
            $session->set('resa_date_debut', $date_debut);

            $date_fin = $data_form['datedepart'];
            $session->set('date_fin', $date_fin);
            $session->set('resa_date_fin', $date_fin);

            $session->set('budget_mini', $data_form['budgetmini']);
            $session->set('budget_maxi', $data_form['budgetmaxi']);

            // On réinitialise le type
            $session->set("Maison", null);
            $session->set("Appartement", null);
            $session->set("Studio", null);

            if ($data_form['type']) {
              foreach ($data_form['type'] as $type)
                $session->set($type, $type);
            }

            // On réinitialise le nombre de pièces
            $session->set("1pieces", null);
            $session->set("2pieces", null);
            $session->set("3pieces", null);
            $session->set("4pieces", null);
            $session->set("5pieces", null);
            $session->set("6pieces", null);

            $session->set('nbAdultes', null);

            if ($data_form['nb_pieces']) {
              foreach ($data_form['nb_pieces'] as $nb_pieces)
                $session->set($nb_pieces.'pieces', $nb_pieces);
            }

            $session->set('ville', $data_form['ville']);
            $session->set('rayon', $data_form['rayon']);
          }
        }
        else {
          $appartement = array();
          $nb_total_appartement = 0;
          $message = "Les champs Dates sont incorrectes";
        }
      }
    }
    elseif (!is_null($this->container->get('request')->query->get('page'))) {
      // Si la requête n'est pas faite en post, mais par navigation par pagination on récupère les éléments de recherche stockés en session
      $data_form['datearrivee'] = $session->get('date_debut');
      $data_form['datedepart'] = $session->get('date_fin');

      $data_form['budgetmini'] = $session->get('budget_mini');
      $data_form['budgetmaxi'] = $session->get('budget_maxi');

      if ($session->get("Maison") == true)
        $data_form['type'][] = "Maison";
      if ($session->get("Studio") == true)
        $data_form['type'][] = "Studio";
      if ($session->get("Appartement") == true)
        $data_form['type'][] = "Appartement";

      if ($session->get("1pieces") == true)
        $data_form['nb_pieces'][] = 1;
      if ($session->get("2pieces") == true)
        $data_form['nb_pieces'][] = 2;
      if ($session->get("3pieces") == true)
        $data_form['nb_pieces'][] = 3;
      if ($session->get("4pieces") == true)
        $data_form['nb_pieces'][] = 4;
      if ($session->get("5pieces") == true)
        $data_form['nb_pieces'][] = 5;
      if ($session->get("6pieces") == true)
        $data_form['nb_pieces'][] = 6;

      $data_form['nbPersonnes'] = $session->get('nbPersonnes');

      $data_form['photo'] = $session->get('photo');

      $data_form['ville'] = $session->get('ville');

      $data_form['rayon'] = $session->get('rayon');
    }
    else { // Si l'on arrive par le biais du bouton "Voir tous les biens" on réinitialise les données en session pour réellement afficher tous les biens
      $data_form['datearrivee'] = null;
      $data_form['datedepart'] = null;

      $data_form['budgetmini'] = null;
      $data_form['budgetmaxi'] = null;

      $data_form['type'] = array();
      $data_form['nb_pieces'] = array();

      $data_form['photo'] = null;

      $data_form['ville'] = null;

      $data_form['rayon'] = null;

      $data_form['nbPersonnes'] = null;

      $this->cleanSession($session);
    }

    if (isset($data_form['reference']) && !is_null($data_form['reference']))
      $appartement = $em->getRepository('PatCompteBundle:Appartement')->findBy(array('reference' => $data_form['reference']));
    else
      $appartement = $em->getRepository('PatCompteBundle:Appartement')->searchAppartementFront($data_form);

    $paginator = $this->container->get('knp_paginator');
    $pagination = $paginator->paginate(
      $appartement, $this->container->get('request')->query->get('page', 1), 20
    );

    $nb_total_appartement = $pagination->getTotalItemCount();


    // On récupère les photos respectives de chaque appartement
    $media = array();

    $app = $pagination->getItems();
    for ($i = 0; $i < sizeof($app); $i++) {
      $photo[$app[$i]->getId()] = $em->getRepository('PatCompteBundle:Media')->findBy(array('appartement' => $app[$i]->getId()), array('tri' => 'asc'));

      if ($photo[$app[$i]->getId()])
        $media[$app[$i]->getId()] = $photo[$app[$i]->getId()][0];
      else
        $media[$app[$i]->getId()] = null;

      // Liste_appart sert de sauvegarde de la recherche au cas où l'utilisateur demande d'ajouter un appart à sa selection
      // Après cet ajout (selection), liste_appart servira à rétablir la recherche dans le template liste_content
      $liste_appart = $liste_appart.$app[$i]->getId()."-";
    }

    if (!is_null($session->get('date_debut')) && !is_null($session->get('date_fin'))) {
      $nb_jours = $this->container->get('pat.date_functions')->nbJours($session->get('date_debut'), $session->get('date_fin'));
    }
    else
      $nb_jours = -1;

    return $this->container->get('templating')->renderResponse('PatFrontBundle:Appartement:liste.html.twig', array('appartement' => $pagination,
        'selection' => $appart_in_selection,
        'liste_apparts' => $liste_appart,
        'nb_resultat' => $nb_total_appartement,
        'nb_jours' => $nb_jours,
        'media' => $media,
        'message' => $message,
        'formRecherche' => $formRecherche->createView())
    );
  }

  // Fonction qui affiche le detail d'un appartement
  // Erreur est un parametre reçu de la sélection de date du controlleur réservation.
  // erreur = 1 signifie que la date saisie doit être suppérieur à la date du jour
  // erreur = 2 signifique qu'une réservation existe déjà pour ce genre de dates.
  public function detailAction(Request $request, $url_bien, $erreur = null)
  {
    // Variables de traitement de l'affichage des pièces
    $i = 0;
    $j = 0;
    $message = "";
    // Fin variables

    $is_selection = 0;

    // On vérifie que l'appartement existe bien et a le statut 4 (statut validé)
    $em = $this->container->get('doctrine')->getManager();
    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('url_fr' => $url_bien, 'statut' => "4"));
    if (!$appartement) {
      throw new NotFoundHttpException("Cet appartement est introuvable");
    }

    $media = $em->getRepository('PatCompteBundle:Media')->findBy(array('appartement' => $appartement->getId()), array('tri' => 'asc'));
    $pieces = $em->getRepository('PatCompteBundle:Piece')->findBy(array('appartement' => $appartement->getId()));
    $documents = $em->getRepository('PatCompteBundle:Documents')->findBy(array('visible' => 1, 'appartement' => $appartement->getId()));

    // Calendrier des disponibilités sur les 12 prochains mois
    $tabCal = "";
    $date = date("d-m-Y");
    $tabDate = explode("-", $date);
    $month = $tabDate[1];
    $year = $tabDate[2];

    for ($i = 0; $i <= 12; $i++) {
      $tabCal .= '<li><table border="0" cellpadding="8" cellspacing="0" align="center"><tr valign="top">';

      $calendrier = new CalendrierAppartement($month, $year, $appartement->getId(), $em->getRepository('PatCompteBundle:Calendrier')->ListeResaByApp($appartement->getId()));

      $tabCal .= "<td><div class='calendarResa' style=''>".$calendrier->affichageFront("User")."</div></td>";
      $tabCal .= '</tr></table></li>';
      // Fin Calendrier

      if ($month == '12') {
        $month = '01';
        $year++;
      }
      else
        $month++;
    }

    // On récupère la sélection perso des appartements
    $cookies = $request->cookies;
    if ($cookies->has('PATcookie')) {
      $recup = explode("-", $cookies->get('PATcookie'));
      $selection = $em->getRepository('PatCompteBundle:Appartement')->getManyAppartementById($recup, 5); // le second parametre est la limite
      foreach ($selection as $s):
        if ($s->getUrlFr() == $url_bien) {
          $is_selection = 1;
        }
      endforeach;
    }
    else {
      $selection = null;
    }

    // On récupere les dates de recherche pour les rebasculer sur le formulaire de sélection des dates de réservation.
    $date_debut = null;
    $date_fin = null;
    $session = $request->getSession();
    $date_debut = $session->get('date_debut');
    $date_fin = $session->get('date_fin');

    /* if($date_debut)
      {
      $date_debut = str_replace("/","",$this->toDateEn($date_debut));
      }
      if($date_fin)
      {
      $date_fin = str_replace("/","",$this->toDateEn($date_fin));
      } */


    // Comme le template de sélection des dates de réservation est séparé du template "detail", lors d'une saisie de dates de réservation on recharge la page
    // Si il y a une erreur avec les dates on récupère un code d'erreur avec le changement de page. On créé les messages et on les rebascule au template de selection des dates.
    if ($erreur) {
      if ($erreur == '1') {
        $message = "La date est incorrecte !";
      }
      if ($erreur == '2') {
        $message = "La date d'arrivée doit être ultérieure à la date du jour !";
      }
      if ($erreur == '3') {
        $message = "La date de départ doit être ultérieure à la date d'arrivée !";
      }
      if ($erreur == '4') {
        $message = "Une réservation existe déjà pour ces dates !";
      }
      if ($erreur == '5') {
        $message = "Pour toute réservation supérieur à 3 mois, merci d'utiliser le <a href='".$this->container->get('router')->generate('pat_front_formulaire_contact')."'>formulaire de contact</a>";
      }
      if ($erreur == '6') {
        $message = "La durée de séjour minimum est de 3 nuits.
                            Merci de modifier votre sélection pour continuer.";
      }
    }

    if (!is_null($date_debut) && !is_null($date_fin)) {
      $nb_jours = $this->container->get('pat.date_functions')->nbJours($date_debut, $date_fin);
    }
    else
      $nb_jours = -1;

    $futurePromotions = $em->getRepository('PatCompteBundle:Promotion')->findFuturPeriodePromotion($appartement->getId());

    return $this->container->get('templating')->renderResponse('PatFrontBundle:Appartement:detail.html.twig', [
        'appartement' => $appartement,
        'pieces' => $pieces,
        'media' => $media,
        'selection' => $selection,
        'is_selection' => $is_selection,
        'calendrier' => $tabCal,
        'retour_page' => 'pat_front_appartement_detail',
        'message' => $message,
        'nb_jours' => $nb_jours,
        'documents' => $documents,
        'future_promotions' => $futurePromotions,
        ]
    );
  }

  // Affiche le formulaire de recherche
  public function rechercherAction(Request $request)
  {
    $session = $request->getSession();

    // On affiche le formulaire de recherche d'appartement qu'on initialise avec la session (au cas où une recherche a déjà été faite).
    $infoForm = new RechercheAppartement(
      $session->get('ville'), $session->get('rayon'), $session->get('date_debut'), $session->get('date_fin'), [
      "0" => $session->get('Maison'),
      "1" => $session->get('Studio'),
      "2" => $session->get('Appartement'),
      //"2" => $session->get('Appartement T1'),
      //"3" => $session->get('Appartement T2'),
      //"4" => $session->get('Appartement T3'),
      //"5" => $session->get('Appartement T4'),
      //"6" => $session->get('Appartement T5'),
      //"7" => $session->get('Appartement T6')
      ], [
      "0" => $session->get('1pieces'),
      "1" => $session->get('2pieces'),
      "2" => $session->get('3pieces'),
      "3" => $session->get('4pieces'),
      "4" => $session->get('5pieces'),
      "5" => $session->get('6pieces')
      ], $session->get('budget_mini'), $session->get('budget_maxi'), null, $session->get('nbPersonnes')
    );

    $formRecherche = $this->container->get('form.factory')->create(new AppartementRechercheForm(), $infoForm);

    return $this->container->get('templating')->renderResponse('PatFrontBundle:Appartement:rechercher.html.twig', array('formRecherche' => $formRecherche->createView(),
        'attributes' => $request->attributes->get('attributes'))
    );
  }

  // Selection d'appartement définie par l'admin
  public function selectionAction($page = 1)
  {
    $n = 5;
    $em = $this->container->get('doctrine')->getManager();

    //$selections = $em->getRepository('PatCompteBundle:Appartement')->findBy(array('is_selection' => 1, "statut" => "4"),null,4,null);

    $dqlcount = "SELECT count(a) FROM Pat\CompteBundle\Entity\Appartement a
            LEFT JOIN Pat\CompteBundle\Entity\Tarif t WITH a = t.appartement
            WHERE t.appartement IS NOT NULL AND a.is_selection = 1 AND a.statut = 4";

    $dql = "SELECT a FROM Pat\CompteBundle\Entity\Appartement a
            LEFT JOIN Pat\CompteBundle\Entity\Tarif t WITH a = t.appartement
            WHERE t.appartement IS NOT NULL AND a.is_selection = 1 AND a.statut = 4
            ORDER BY a.created_at DESC";

    $query = $em->createQuery($dql)
      ->setFirstResult(($page - 1) * $n)
      ->setMaxResults($n);

    $selections = $query->getResult();
    $nb_select = $em->createQuery($dqlcount)->getResult();
    // On récupère le nombre de pages
    $page_max = ceil($nb_select[0][1] / $n);

    $media = array();
    $photo = array();
    for ($i = 0; $i < sizeof($selections); $i++) {
      $photo[$i] = $em->getRepository('PatCompteBundle:Media')->findBy(array('appartement' => $selections[$i]->getId()), array('tri' => 'asc'));
      if ($photo[$i]) {
        $media[$i] = $photo[$i][0];
      }
      else {
        $media[$i] = null;
      }
    }

    return $this->container->get('templating')->renderResponse('PatFrontBundle:Appartement:selection.html.twig', array('selections' => $selections, 'media' => $media, 'page' => $page, 'page_max' => $page_max)
    );
  }

  // Ajoute un appartement de la recherche dans un cookie (en ajax)
  public function ajoutSelectionRechercheUserAction(Request $request)
  {
    if ($request->isXmlHttpRequest()) {
      $id_appartement = $request->request->get('id_appartement');

      $new = "";
      $i = 0;
      //$request = Request::createFromGlobals(); // On récupère toutes les données stockées dans les variables globales
      $cookies = $request->cookies;
      if ($cookies->has('PATcookie')) {
        $recup = explode("-", $cookies->get('PATcookie'));
        for ($i = 0; $i < count($recup); $i++) {
          $new = $new.$recup[$i]."-";
        }
      }
      $new = $new.$id_appartement;

      $response = new Response();
      $response->headers->setCookie(new Cookie('PATcookie', $new, time() + (3600 * 48)));
      $response->send();

      return $this->container->get('templating')->renderResponse('PatFrontBundle:Appartement:selectionRetour.html.twig');
    }
    else {
      return "";
    }
  }

  // Stock l'appartement du détail dans un cookie
  public function ajoutSelectionDetailUserAction(Request $request, $id_appartement)
  {

    $em = $this->container->get('doctrine')->getManager();
    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $id_appartement));
    if (!$appartement) {
      throw new NotFoundHttpException("Cet appartement est introuvable");
    }

    $new = "";
    $i = 0;
    //$request = Request::createFromGlobals(); // On récupère toutes les données stockées dans les variables globales
    $cookies = $request->cookies;
    if ($cookies->has('PATcookie')) {
      $recup = explode("-", $cookies->get('PATcookie'));
      for ($i = 0; $i < count($recup); $i++) {
        $new = $new.$recup[$i]."-";
      }
    }
    $new = $new.$id_appartement;

    $response = new Response();
    $response->headers->setCookie(new Cookie('PATcookie', $new, time() + (3600 * 48)));
    $response->send();

    return new RedirectResponse($this->container->get('router')->generate('pat_front_appartement_detail', array("url_bien" => $appartement->getUrlFr())));
  }

  // supprime un appartement (pour la sélection utilisateur) du cookie
  public function suppressionSelectionUserAction($id_appartement, $retour_page, Request $request)
  {
    $em = $this->container->get('doctrine')->getManager();
    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findOneBy(array('id' => $id_appartement));
    if (!$appartement) {
      throw new NotFoundHttpException("Cet appartement est introuvable");
    }

    $new = "";
    $i = 0;
    //$request = Request::createFromGlobals(); // On récupère toutes les données stockées dans les variables globales
    $cookies = $request->cookies;
    if ($cookies->has('PATcookie')) {
      $recup = explode("-", $cookies->get('PATcookie'));
      for ($i = 0; $i < count($recup); $i++) {
        if ($recup[$i] != $id_appartement) { // On recopie tout sauf l'appart désigné
          $new = $new.$recup[$i]."-";
        }
      }
    }

    $response = new Response();
    $response->headers->setCookie(new Cookie('PATcookie', $new, time() + (3600 * 48)));
    $response->send();

    $message = 'Le bien a bien été supprimé de votre sélection';
    $this->container->get('session')->getFlashBag()->add('flash', $message);

    return new RedirectResponse($this->container->get('router')->generate($retour_page, array("url_bien" => $appartement->getUrlFr())));
  }

  // Correspond à la selection des appartements, par l'utilisateur lors de sa recherche, stockée dans des cookies
  public function selectionUserAction(Request $request)
  {
    $em = $this->container->get('doctrine')->getManager();
    //$request = Request::createFromGlobals(); // On récupère toutes les données stockées dans les variables globales
    $cookies = $request->cookies;
    if ($cookies->has('PATcookie')) {
      $recup = explode("-", $cookies->get('PATcookie'));
      $appartement = $em->getRepository('PatCompteBundle:Appartement')->getManyAppartementById($recup, null);
      $media = array();
      for ($i = 0; $i < sizeof($appartement); $i++) {
        $photo[$i] = $em->getRepository('PatCompteBundle:Media')->findBy(array('appartement' => $appartement[$i]->getId()), array('tri' => 'asc'));
        if ($photo[$i]) {
          $media[$i] = $photo[$i][0];
        }
        else {
          $media[$i] = null;
        }
      }

      return $this->container->get('templating')->renderResponse('PatFrontBundle:Appartement:selectionUser.html.twig', array('appartements' => $appartement, 'nb_resultat' => count($appartement), 'media' => $media, 'retour_page' => 'pat_front_appartement_selection_utilisateur')
      );
    }
    else {
      return $this->container->get('templating')->renderResponse('PatFrontBundle:Appartement:selectionUser.html.twig', array('appartements' => null)
      );
    }
  }

  // affiche les derniers appartements ajoutés
  public function dernierAjoutAction()
  {
    $em = $this->container->get('doctrine')->getManager();

    //$ajouts = $em->getRepository('PatCompteBundle:Appartement')->findBy(array("statut" => "4", 'is_selection' => 1), array("created_at" => "DESC"), 3);
    $ajouts = $em->getRepository('PatCompteBundle:Appartement')->getDernierAjouts();

    return $this->container->get('templating')->renderResponse('PatFrontBundle:Appartement:dernierAjout.html.twig', array('ajouts' => $ajouts)
    );
  }

  public function toDateEn($date)
  {
    return substr($date, 6, 4)."-".substr($date, 3, 2)."-".substr($date, 0, 2);
  }

  public function villeSearchAction(Request $request, $idVille = null)
  {

    $liste_appart = "";
    $appartement = null;
    $appart_in_selection = null;
    $em = $this->container->get('doctrine')->getManager();
    $formRecherche = $this->container->get('form.factory')->create(new AppartementRechercheForm());

    $session = $request->getSession();
    // On sauvegarde la recherche dans la session
    $session->set('date_debut', null);
    $session->set('date_fin', null);
    $session->set('resa_date_debut', null);
    $session->set('resa_date_fin', null);
    $session->set('budget_mini', null);
    $session->set('budget_maxi', null);

    // On réinitialise le type
    $session->set("Maison", null);
    $session->set("Appartement", null);
    $session->set("Studio", null);

    // On réinitialise le nombre de pièces
    $session->set("1pieces", null);
    $session->set("2pieces", null);
    $session->set("3pieces", null);
    $session->set("4pieces", null);
    $session->set("5pieces", null);
    $session->set("6pieces", null);
    $session->set('photo', false);
    $session->set('ville', null);
    $session->set('rayon', null);
    $session->set('nbPersonnes', null);

    $ville = $em->getRepository('PatCompteBundle:Ville')->find($idVille);

    $appartement = $em->getRepository('PatCompteBundle:Appartement')->findBy(array('ville' => !empty($idVille) && !empty($ville) ? $idVille : null));

    $paginator = $this->container->get('knp_paginator');
    $pagination = $paginator->paginate(
      $appartement, $this->container->get('request')->query->get('page', 1), 20
    );

    $nb_total_appartement = $pagination->getTotalItemCount();

    // On récupère les photos respectives de chaque appartement
    $media = array();

    $app = $pagination->getItems();
    for ($i = 0; $i < sizeof($app); $i++) {
      $photo[$app[$i]->getId()] = $em->getRepository('PatCompteBundle:Media')->findBy(array('appartement' => $app[$i]->getId()), array('tri' => 'asc'));

      if ($photo[$app[$i]->getId()])
        $media[$app[$i]->getId()] = $photo[$app[$i]->getId()][0];
      else
        $media[$app[$i]->getId()] = null;

      $liste_appart = $liste_appart.$app[$i]->getId()."-";
    }


    return $this->container->get('templating')->renderResponse('PatFrontBundle:Appartement:liste.html.twig', array('appartement' => $pagination,
        'ville' => $ville,
        'selection' => null,
        'liste_apparts' => null,
        'nb_resultat' => $nb_total_appartement,
        'media' => $media,
        'message' => null,
        'formRecherche' => $formRecherche->createView())
    );
  }

  /**
   * Disponibilités de l'appartement au format iCal.
   *
   * @param Request $request
   * @param string $ref
   * @return Response
   * @author AG
   */
  public function busyTimesAction(Request $request, $ref = null)
  {
    $end = strtotime('+6 months');

    $url = $this->get('router')->generate(
      'pat_front_busy', compact('ref'),
      UrlGeneratorInterface::ABSOLUTE_URL
    );

    $data = $this->container->get('doctrine')
      ->getManager()
      ->getRepository('PatCompteBundle:Appartement')
      ->findBusyTime($ref, $end);

    $ifb = Ics::generateFreeBusyCalendar($data, $end, $url);

    return new Response($ifb, 200, array(
      'content-type' => 'text/calendar;charset=utf-8'
    ));
  }

  /**
   * Calendrier de l'appartement au format iCal.
   *
   * @param Request $request
   * @param string $ref
   * @return Response
   * @author AG
   */
  public function calendarAction(Request $request, $ref = null)
  {
    $end = strtotime('+6 months');

    $data = $this->container->get('doctrine')
      ->getManager()
      ->getRepository('PatCompteBundle:Appartement')
      ->findBusyTime($ref, $end);

    $ics = Ics::generateCalendar($data, $end);

    return new Response($ics, 200, array(
      'content-type' => 'text/calendar;charset=utf-8'
    ));
  }

  // Fonction qui efface les données de recherche en session
  public function cleanSession($session)
  {
    $session->set('date_debut', null);
    $session->set('resa_date_debut', null);

    $session->set('date_fin', null);
    $session->set('resa_date_fin', null);

    $session->set('budget_mini', null);
    $session->set('budget_maxi', null);

    // On réinitialise le type
    $session->set("Maison", null);
    $session->set("Appartement", null);
    $session->set("Studio", null);

    // On réinitialise le nombre de pièces
    $session->set("1pieces", null);
    $session->set("2pieces", null);
    $session->set("3pieces", null);
    $session->set("4pieces", null);
    $session->set("5pieces", null);
    $session->set("6pieces", null);

    $session->set("nbPersonnes", null);

    $session->set('ville', null);
    $session->set('rayon', null);
  }

}
