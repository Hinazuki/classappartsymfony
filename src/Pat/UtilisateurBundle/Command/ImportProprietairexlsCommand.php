<?php

namespace Pat\UtilisateurBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityRepository;
use Pat\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PHPExcel;
use PHPExcel_IOFactory;

class ImportProprietairexlsCommand extends ContainerAwareCommand
{

  protected function configure()
  {
    $this
      ->setName('utilisateur:importProprietairexls')
      ->setDescription('Import Owners XLS file')
      ->addArgument('file', InputArgument::REQUIRED, 'Path to the xls file')
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $fichier_excel = $input->getArgument('file');

    //$erreurs_par_ligne = array();
    //$erreurs_par_colonne = null;
    $nb_ajouts = 0;

    $em = $this->getContainer()->get('doctrine')->getManager();

    /* $user_context = $this->getContainer()->get('security.context')->getToken()->getUser();
      $user = $em->find('PatUtilisateurBundle:Utilisateur', $user_context->getId());
      if(!$user)
      {
      throw new AccessDeniedException('This user does not have access to this section.');
      } */

    $dir = __DIR__.'/../../../../web/excel/';


    /**  Identify the type of $inputFileName  * */
    $inputFileType = PHPExcel_IOFactory::identify($dir.$fichier_excel);
    /**  Create a new Reader of the type that has been identified  * */
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    /**  Advise the Reader that we only want to load cell data  * */
    $objReader->setReadDataOnly(true);
    /**  Load $inputFileName to a PHPExcel Object  * */
    $objPHPExcel = $objReader->load($dir.$fichier_excel);

    $worksheet = $objPHPExcel->getActiveSheet();
    $nbLignes = $worksheet->getHighestRow();
    //$nbColonnes = $worksheet->getHighestColumn();

    $data = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);


    // On parcours les lignes (Les lignes 1 et 2 servent aux intitulés excel)
    for ($i = 3; $i <= $nbLignes; $i++) {

      $info = "";
      $bloque_ajout = false;
      $erreurs = array();

      //$erreurs_par_ligne[$i] = array();
      //$erreurs_par_ligne[$i]['ligne'] = $i;
      //$erreurs_par_colonne = array();



      $info = $data[$i]['A'];
      // On vérifie si une référence client est renseignée
      /* if(preg_match("/P[0-9]{4}/", $info)) {
        //$recup_prop = $em->getRepository('PatUtilisateurBundle:Utilisateur')->findOneBy(array('num_compte' => $info));
        $recup_prop = $em->createQueryBuilder()
        ->from('\Pat\UtilisateurBundle\Entity\Utilisateur','u')
        ->select('COUNT(u)')
        ->where("u.num_compte = :num_compte")
        ->setParameter('num_compte', $info)
        ->getQuery()
        ->getSingleScalarResult();

        // Si le propriétaire existe déjà, on ne va pas plus loin
        if($recup_prop > 0) {
        $output->writeln("Ligne ".$i." :");
        $output->writeln("Le propriétaire existe déjà");
        $output->writeln("");

        continue;
        }
        // S'il n'existe pas, on le crée
        else {
        $proprietaire = new Utilisateur();
        $proprietaire->setNumCompte($info);
        }
        }
        // Si aucune référence n'est renseignée on crée le propriétaire et on génère un numéro de compte
        else {
        $proprietaire = new Utilisateur();

        $num_compte = $em->getRepository('PatUtilisateurBundle:Utilisateur')->checkNumCompteAction("P".rand(1000,9999));
        $proprietaire->setNumCompte($num_compte);
        } */
      $proprietaire = new Utilisateur();
      $proprietaire->setUsername("temp");
      $proprietaire->setUsernameCanonical($proprietaire->getUsername());



      // Traitement de la 3ème colonne (Civilite)
      $info = trim($data[$i]['C']);
      if ($info == "Mme" || $info == "Mlle" || $info == "M" || $info == "M Mme" || $info == "Ste" || $info == "NC")
        $proprietaire->setCivilite($info);
      else
        $erreurs['C'] = "La civilité du propriétaire n'est pas conforme";



      // Traitement de la 4ème colonne (Nom de famille)
      $info = $data[$i]['D'];
      if ($info != null)
        $proprietaire->setNom($info);
      else
        $erreurs['D'] = "Le nom du propriétaire est obligatoire";



      // Traitement de la 5ème colonne (Prénom)
      $info = $data[$i]['E'];
      $proprietaire->setPrenom($info);



      // Traitement de la 6ème colonne (Adresse mail)
      $info = trim($data[$i]['F']);

      if ($info == null)
        $info = "temp@alterhome.fr";

      $recup_mail = $em->getRepository('PatUtilisateurBundle:Utilisateur')->checkEmailCompteAction($info);
      if ($recup_mail == true)
        $erreurs['F'] = "L'adresse email est déjà associée à un compte";
      else {
        if (filter_var($info, FILTER_VALIDATE_EMAIL)) {
          $proprietaire->setEmail($info);
        }
        else
          $erreurs['F'] = "L'adresse email n'est pas conforme";
      }



      // Traitement de la 7ème colonne (Adresse Personnelle)
      $info = $data[$i]['G'];
      //if($info != null)
      $proprietaire->setAdresse($info);
      //else
      //$erreurs['G'] = "L'adresse n'est pas renseignée";
      // Traitement de la 8ème colonne (Code Postal)
      $info = $data[$i]['H'];
      //if($info != null)
      $proprietaire->setCodePostal($info);
      //else
      //$erreurs['H'] = "Le code postal n'est pas renseigné";
      // Traitement de la 9ème colonne (Ville)
      $info = $data[$i]['I'];
      //if($info != null)
      $proprietaire->setVille($info);
      //else
      //$erreurs['I'] = "La ville n'est pas renseignée";
      // Traitement de la 10ème colonne (Pays)
      $info = $data[$i]['J'];
      //if($info != null)
      $proprietaire->setPays($info);
      //else
      //$erreurs['J'] = "Le pays n'est pas renseigné";
      // Traitement de la 11ème colonne (Société)
      $info = $data[$i]['K'];
      $proprietaire->setSociete($info);



      // Traitement de la 12ème colonne (Siret)
      $info = $data[$i]['L'];
      $proprietaire->setSiret($info);



      // Traitement de la 13ème colonne (Fonction)
      $info = $data[$i]['M'];
      $proprietaire->setFonction($info);



      // Traitement de la 14ème colonne (Nationalité)
      $info = $data[$i]['N'];
      $proprietaire->setNationalite($info);



      // Traitement de la 15ème colonne (Langue parlée)
      $info = $data[$i]['O'];
      if ($info == null)
        $info = "Français";
      $proprietaire->setLangueParle($info);



      // Traitement de la 16ème colonne (Téléphone fixe)
      $info = $data[$i]['P'];
      $proprietaire->setTelephone($info);



      // Traitement de la 17ème colonne (Téléphone portable 1)
      $info = $data[$i]['Q'];
      $proprietaire->setMobile($info);



      // Traitement de la 18ème colonne (Téléphone portable 2)
      $info = $data[$i]['R'];
      $proprietaire->setMobile2($info);



      // Traitement de la 19ème colonne (Fax)
      $info = $data[$i]['S'];
      $proprietaire->setFax($info);



      // Traitement de la 20ème colonne (RIB - Code établissement)
      $info = $data[$i]['T'];
      $proprietaire->setRibEtab($info);



      // Traitement de la 21ème colonne (RIB - Code guichet)
      $info = $data[$i]['U'];
      $proprietaire->setRibCode($info);



      // Traitement de la 22ème colonne (RIB - Numéro de compte)
      $info = $data[$i]['V'];
      $proprietaire->setRibCompte($info);



      // Traitement de la 23ème colonne (RIB - Code RICE)
      $info = $data[$i]['W'];
      $proprietaire->setRibRice($info);



      // Traitement de la 24ème colonne (RIB - Domiciliation)
      $info = $data[$i]['X'];
      $proprietaire->setRibDomiciliation($info);



      // Traitement de la 25ème colonne (RIB - IBAN)
      $info = $data[$i]['Y'];
      $proprietaire->setRibIban($info);



      // Traitement de la 26ème colonne (RIB - BIC)
      $info = $data[$i]['Z'];
      $proprietaire->setRibBic($info);



      // Au moins une erreur est survenue
      if ($erreurs) {
        // On affiche les erreurs sur la sortie
        $output->writeln("Ligne ".$i." :");
        foreach ($erreurs as $erreur)
          $output->writeln($erreur);
        $output->writeln("");

        //continue;
      }
      else {
        // Génération du nom d'utilisateur
        //$nomtronq = substr(str_replace(" ", "", $proprietaire->getNom()), 0, 3);
        //$proprietaire->setUsername($proprietaire->getNumCompte().$nomtronq);
        //$proprietaire->setUsernameCanonical($proprietaire->getUsername());
        $proprietaire->setNumCompte(null);

        $proprietaire->setEmailCanonical($proprietaire->getEmail());
        $proprietaire->setEnabled(true);

        // Génération du mot de passe
        $password = $em->getRepository('PatUtilisateurBundle:Utilisateur')->generatePassword();
        $encoder = $this->getContainer()->get('security.encoder_factory')->getEncoder($proprietaire);
        $proprietaire->setPassword($encoder->encodePassword($password, $proprietaire->getSalt()));

        $proprietaire->setConfirmationToken(null);

        $proprietaire->setCGU("1");
        $proprietaire->setProprietaire();


        $em->persist($proprietaire);
        $em->flush();

        $proprietaire->generateUsername();

        if ($proprietaire->getEmail() == "temp@alterhome.fr") {
          $proprietaire->setEmail('fake'.$proprietaire->getUsername().'@alterhome.fr');
          $proprietaire->setEmailCanonical($proprietaire->getEmail());
        }

        $em->flush();

        // Set num_compte to cell
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $proprietaire->getUsername());
        // Set password to cell
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $password);
        // Set email
        if ($proprietaire->getEmail() == "fake".$proprietaire->getUsername()."@alterhome.fr")
          $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, $proprietaire->getEmail());


        $nb_ajouts++;
      }

      $em->detach($proprietaire);
      unset($proprietaire);
      $em->clear();
      //$output->writeln(memory_get_usage());
    }

    $output->writeln("");
    $output->writeln(($i - 3)." lignes traitées");
    $output->writeln($nb_ajouts." propriétaires ajoutés");

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $inputFileType);
    $objWriter->save($dir."pat-proprietaire-result.xls");
  }

}
