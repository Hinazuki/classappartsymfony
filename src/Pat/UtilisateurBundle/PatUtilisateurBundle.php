<?php

namespace Pat\UtilisateurBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PatUtilisateurBundle extends Bundle
{

  public function getParent()
  {
    return 'FOSUserBundle';
  }

}
