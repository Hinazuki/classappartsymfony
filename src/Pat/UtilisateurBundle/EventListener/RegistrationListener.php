<?php

namespace Pat\UtilisateurBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use Swift_Message;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\TranslatorInterface;
use \Doctrine\ORM\EntityManager;

class RegistrationListener implements EventSubscriberInterface
{

  private $em;

  public function __construct(EntityManager $em, $mailer, $templating)
  {
    $this->em = $em;
    $this->mailer = $mailer;
    $this->templating = $templating;
  }

  public static function getSubscribedEvents()
  {
    return array(
      FOSUserEvents::REGISTRATION_COMPLETED => 'initializeValues',
    );
  }

  public function initializeValues(Event $filter)
  {
    $user = $filter->getUser();

    $user->setProprietaire();
    $user->generateUsername();

    $this->em->persist($user);
    $this->em->flush();

    $message_email = Swift_Message::newInstance()
      ->setSubject("Inscription d'un ".$user->getType()." - ClassAppart")
      ->setFrom('contact@class-appart.com')
      ->setTo('info@class-appart.com')
    ;

    $textBody = $this
      ->templating
      ->render('PatUtilisateurBundle:Registration:email_new_user_admin.txt.twig', [
      'user' => $user,
      ])
    ;
    $htmlBody = $this
      ->templating
      ->render('PatUtilisateurBundle:Registration:email_new_user_admin.html.twig', [
      'user' => $user,
      ])
    ;

    if (!empty($htmlBody)) {
      $message_email
        ->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain')
      ;
    }
    else {
      $message_email->setBody($textBody);
    }

    $this->mailer->send($message_email);
  }

}
