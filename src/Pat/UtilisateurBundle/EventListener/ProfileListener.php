<?php

namespace Pat\UtilisateurBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Event\GetResponseUserEvent as GetResponseUserEvent;
use FOS\UserBundle\Event\FormEvent as FormEvent;
use \Swift_Mailer;
use \Doctrine\ORM\EntityManager;

class ProfileListener implements EventSubscriberInterface
{

  private $em;
  private $mailer;
  private $templating;

  public function __construct(EntityManager $em, Swift_Mailer $mailer, $templating)
  {
    $this->em = $em;
    $this->mailer = $mailer;
    $this->templating = $templating;
  }

  public static function getSubscribedEvents()
  {
    return array(
      FOSUserEvents::PROFILE_EDIT_INITIALIZE => 'initialize',
      FOSUserEvents::PROFILE_EDIT_SUCCESS => 'success',
      FOSUserEvents::PROFILE_EDIT_COMPLETED => 'completed'
    );
  }

  public function initialize(GetResponseUserEvent $event)
  {
    $session = $event->getRequest()->getSession();

    // On stock le referer s'il n'est pas déjà en session
    if (is_null($session->get('previous_page'))) {
      $referer = $event->getRequest()->headers->get("referer");
      $session->set('previous_page', $referer);
    }
  }

  public function success(FormEvent $event)
  {
    $session = $event->getRequest()->getSession();

    // On récupère le referer stocké, le supprime de la session et redirige vers cette page
    $previous_page = $session->get('previous_page');
    $session->remove('previous_page');

    // On stock le referer s'il n'est pas déjà en session
    if ($previous_page) {
      $event->setResponse(new RedirectResponse($previous_page));
    }
  }

  public function completed(Event $filter)
  {
    $user = $filter->getUser();

    // Envoi d'un mail à l'admin pour signaler la modification de données
    $message_email = \Swift_Message::newInstance()
      ->setSubject("Modification de compte - classAppart ®")
      ->setFrom('contact@class-appart.com')
      ->setTo('info@class-appart.com');

    $textBody = $this->templating->render('PatUtilisateurBundle:Profile:mail_info_admin.txt.twig', array('user' => $user));
    $htmlBody = $this->templating->render('PatUtilisateurBundle:Profile:mail_info_admin.html.twig', array('user' => $user));

    if (!empty($htmlBody)) {
      $message_email->setBody($htmlBody, 'text/html')
        ->addPart($textBody, 'text/plain');
    }
    else
      $message_email->setBody($textBody);

    $this->mailer->send($message_email);
  }

}
