<?php

namespace Pat\UtilisateurBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Pat\UtilisateurBundle\Repository\UtilisateurRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Utilisateur extends BaseUser
{

  /**
   * Type des utilisateur admin.
   */
  const ADMIN = 9;

  /**
   * @ORM\OneToMany(targetEntity="Pat\CompteBundle\Entity\Reservation", mappedBy="utilisateur")
   */
  protected $reservations;

  /**
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  protected $id;

  /**
   * @ORM\Column(name="civilite", type="string", length=20)
   */
  protected $civilite;

  /**
   * @ORM\Column(name="num_compte", type="string", length=10, nullable=true)
   */
  protected $num_compte;

  /**
   * @ORM\Column(name="nom", type="string", length=255)
   * @Assert\NotBlank()
   * @Assert\NotNull()
   */
  protected $nom;

  /**
   * @ORM\Column(name="prenom", type="string", length=255, nullable = true)
   */
  protected $prenom;

  /**
   * @ORM\Column(name="societe", type="string", length=100, nullable = true)
   */
  protected $societe;

  /**
   * @ORM\Column(name="siret", type="string", length=20, nullable = true)
   */
  protected $siret;

  /**
   * @ORM\Column(name="fonction", type="string", length=100, nullable = true)
   */
  protected $fonction;

  /**
   * @ORM\Column(name="date_naissance", type="date", length=10, nullable = true)
   */
  protected $date_naissance;

  /**
   * @ORM\Column(name="nationalite", type="string", length=100, nullable = true)
   */
  protected $nationalite;

  /**
   * @ORM\Column(name="langue_parle", type="string", length=100, nullable = true)
   */
  protected $langue_parle;

  /**
   * @ORM\Column(name="adresse", type="string", length=255, nullable = true)
   */
  protected $adresse;

  /**
   * @ORM\Column(name="adresse2", type="string", length=255, nullable = true)
   */
  protected $adresse2;

  /**
   * @ORM\Column(name="code_postal", type="string", length=20, nullable = true)
   */
  protected $code_postal;

  /**
   * @ORM\Column(name="ville", type="string", length=255, nullable = true)
   */
  protected $ville;

  /**
   * @ORM\Column(name="pays", type="string", length=100, nullable = true)
   */
  protected $pays;

  /**
   * @ORM\Column(name="telephone", type="string", length=20, nullable = true)
   */
  protected $telephone;

  /**
   * @ORM\Column(name="mobile", type="string", length=20, nullable = true)
   */
  protected $mobile;

  /**
   * @ORM\Column(name="mobile2", type="string", length=20, nullable = true)
   */
  protected $mobile2;

  /**
   * @ORM\Column(name="fax", type="string", length=20, nullable = true)
   */
  protected $fax;


  /**
   * @ORM\Column(name="mails_second", type="string", length=1000, nullable = true)
   */
//	protected $mails_second;

  /**
   * @ORM\Column(name="second_email", type="string", length=255, nullable = true)
   * @Assert\Email()
   *
   * @link https://symfony.com/doc/2.3/reference/constraints/Email.html
   */
  protected $second_email;

  /**
   * @ORM\Column(name="moyen_contact", type="string", length=10, nullable = true)
   */
  protected $moyen_contact;

  /**
   * @ORM\Column(name="adresse_fact", type="string", length=255, nullable = true)
   */
  protected $adresse_fact;

  /**
   * @ORM\Column(name="adresse2_fact", type="string", length=255, nullable = true)
   */
  protected $adresse2_fact;

  /**
   * @ORM\Column(name="code_postal_fact", type="string", length=20, nullable = true)
   */
  protected $code_postal_fact;

  /**
   * @ORM\Column(name="ville_fact", type="string", length=255, nullable = true)
   */
  protected $ville_fact;

  /**
   * @ORM\Column(name="pays_fact", type="string", length=100, nullable = true)
   */
  protected $pays_fact;

  /**
   * @ORM\Column(name="info_promo", type="boolean")
   */
  protected $info_promo = false;

  /**
   * @ORM\Column(name="info_juridique", type="boolean")
   */
  protected $info_juridique = false;

  /**
   * @ORM\Column(name="info_evolution", type="boolean")
   */
  protected $info_evolution = false;

  /**
   * @ORM\Column(name="info_amelioration", type="boolean")
   */
  protected $info_amelioration = false;

  /**
   * @ORM\Column(name="info_diffusion", type="boolean")
   */
  protected $info_diffusion = false;

  /**
   * @ORM\Column(name="cgu", type="boolean")
   */
  protected $cgu = false;

  /**
   * @ORM\Column(name="rib_etab", type="string", length=10, nullable = true)
   */
  protected $rib_etab;

  /**
   * @ORM\Column(name="rib_code", type="string", length=10, nullable = true)
   */
  protected $rib_code;

  /**
   * @ORM\Column(name="rib_compte", type="string", length=20, nullable = true)
   */
  protected $rib_compte;

  /**
   * @ORM\Column(name="rib_rice", type="string", length=5, nullable = true)
   */
  protected $rib_rice;

  /**
   * @ORM\Column(name="rib_domiciliation", type="string", length=100, nullable = true)
   */
  protected $rib_domiciliation;

  /**
   * @ORM\Column(name="rib_iban", type="string", length=50, nullable = true)
   */
  protected $rib_iban;

  /**
   * @ORM\Column(name="rib_bic", type="string", length=20, nullable = true)
   */
  protected $rib_bic;

  /**
   * @ORM\Column(type="string", length=255, nullable = true)
   */
  protected $rib_agence_resp;

  /**
   * @ORM\Column(type="string", length=100, nullable = true)
   */
  protected $rib_pays;

  /**
   * @ORM\Column(type="string", length=100, nullable = true)
   */
  protected $rib_titulaire;

  /**
   * @ORM\Column(columnDefinition="tinyint(1) NULL DEFAULT '1'")
   */
  protected $type_utilisateur;

  /**
   * @ORM\Column(name="created_at", type="datetime", nullable = true)
   */
  protected $created_at;

  /**
   * @ORM\Column(name="updated_at", type="datetime", nullable = true)
   */
  protected $updated_at;

  public function __construct()
  {
    parent::__construct();
  }

  public function __toString()
  {
    return $this->nom." ".$this->prenom." (".$this->username.")";
  }

  /**
   * @ORM\PrePersist
   */
  public function setInitialValues()
  {
    $this->created_at = new \DateTime();
    $this->updated_at = new \DateTime();
    $this->username = "username".rand();
  }

  /**
   * @ORM\PreUpdate
   */
  public function onUpdate()
  {
    $this->updated_at = new \DateTime();
  }

  public function generateUsername()
  {
    $letter = "";
    if ($this->getType() == "Propriétaire")
      $letter = "P";
    if ($this->getType() == "Locataire")
      $letter = "L";

    // Generate username with ID
    $iduser = str_pad(strval($this->getId()), 7, "0", STR_PAD_LEFT);
    $nomtronq = strtoupper(substr(str_replace(" ", "", $this->getNom()), 0, 3));
    $this->setUsername($letter.$iduser.$nomtronq);
    $this->setUsernameCanonical($this->getUsername());
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set nom
   *
   * @param string $nom
   */
  public function setNom($nom)
  {
    $this->nom = $nom;
  }

  /**
   * Get nom
   *
   * @return string
   */
  public function getNom()
  {
    return $this->nom;
  }

  /**
   * Set prenom
   *
   * @param string $prenom
   */
  public function setPrenom($prenom)
  {
    $this->prenom = $prenom;
  }

  /**
   * Get prenom
   *
   * @return string
   */
  public function getPrenom()
  {
    return $this->prenom;
  }

  public function getIdentiteUtilisateur()
  {

    return $this->getNom()."  ".$this->getPrenom()." - ".$this->getNumCompte();
  }

  /**
   * Set societe
   *
   * @param string $societe
   */
  public function setSociete($societe)
  {
    $this->societe = $societe;
  }

  /**
   * Get societe
   *
   * @return string
   */
  public function getSociete()
  {
    return $this->societe;
  }

  /**
   * Set siret
   *
   * @param string $siret
   */
  public function setSiret($siret)
  {
    $this->siret = $siret;
  }

  /**
   * Get siret
   *
   * @return string
   */
  public function getSiret()
  {
    return $this->siret;
  }

  /**
   * Set fonction
   *
   * @param string $fonction
   */
  public function setFonction($fonction)
  {
    $this->fonction = $fonction;
  }

  /**
   * Get fonction
   *
   * @return string
   */
  public function getFonction()
  {
    return $this->fonction;
  }

  /**
   * Set date_naissance
   *
   * @param string $dateNaissance
   */
  public function setDateNaissance($dateNaissance)
  {
    $this->date_naissance = $dateNaissance;
  }

  /**
   * Get date_naissance
   *
   * @return string
   */
  public function getDateNaissance()
  {
    return $this->date_naissance;
  }

  /**
   * Set nationalite
   *
   * @param string $nationalite
   */
  public function setNationalite($nationalite)
  {
    $this->nationalite = $nationalite;
  }

  /**
   * Get nationalite
   *
   * @return string
   */
  public function getNationalite()
  {
    return $this->nationalite;
  }

  /**
   * Set langue_parle
   *
   * @param string $langueParle
   */
  public function setLangueParle($langueParle)
  {
    $this->langue_parle = $langueParle;
  }

  /**
   * Get langue_parle
   *
   * @return string
   */
  public function getLangueParle()
  {
    return $this->langue_parle;
  }

  /**
   * Set adresse
   *
   * @param string $adresse
   */
  public function setAdresse($adresse)
  {
    $this->adresse = $adresse;
  }

  /**
   * Get adresse
   *
   * @return string
   */
  public function getAdresse()
  {
    return $this->adresse;
  }

  /**
   * Set adresse2
   *
   * @param string $adresse2
   */
  public function setAdresse2($adresse2)
  {
    $this->adresse2 = $adresse2;
  }

  /**
   * Get adresse2
   *
   * @return string
   */
  public function getAdresse2()
  {
    return $this->adresse2;
  }

  /**
   * Set code_postal
   *
   * @param string $codePostal
   */
  public function setCodePostal($codePostal)
  {
    $this->code_postal = $codePostal;
  }

  /**
   * Get code_postal
   *
   * @return string
   */
  public function getCodePostal()
  {
    return $this->code_postal;
  }

  /**
   * Set ville
   *
   * @param string $ville
   */
  public function setVille($ville)
  {
    $this->ville = $ville;
  }

  /**
   * Get ville
   *
   * @return string
   */
  public function getVille()
  {
    return $this->ville;
  }

  /**
   * Set pays
   *
   * @param string $pays
   */
  public function setPays($pays)
  {
    $this->pays = $pays;
  }

  /**
   * Get pays
   *
   * @return string
   */
  public function getPays()
  {
    return $this->pays;
  }

  /**
   * Set telephone
   *
   * @param string $telephone
   */
  public function setTelephone($telephone)
  {
    $this->telephone = $telephone;
  }

  /**
   * Get telephone
   *
   * @return string
   */
  public function getTelephone()
  {
    return $this->telephone;
  }

  /**
   * Set mobile
   *
   * @param string $mobile
   */
  public function setMobile($mobile)
  {
    $this->mobile = $mobile;
  }

  /**
   * Get mobile
   *
   * @return string
   */
  public function getMobile()
  {
    return $this->mobile;
  }

  /**
   * Set fax
   *
   * @param string $fax
   */
  public function setFax($fax)
  {
    $this->fax = $fax;
  }

  /**
   * Get fax
   *
   * @return string
   */
  public function getFax()
  {
    return $this->fax;
  }

  /**
   * Set moyen_contact
   *
   * @param string $moyen_contact
   */
  public function setMoyenContact($moyen_contact)
  {
    $this->moyen_contact = $moyen_contact;
  }

  /**
   * Get moyen_contact
   *
   * @return string
   */
  public function getMoyenContact()
  {
    return $this->moyen_contact;
  }

  /**
   * Set adresse_fact
   *
   * @param string $adresseFact
   */
  public function setAdresseFact($adresseFact)
  {
    $this->adresse_fact = $adresseFact;
  }

  /**
   * Get adresse_fact
   *
   * @return string
   */
  public function getAdresseFact()
  {
    return $this->adresse_fact;
  }

  /**
   * Set adresse2_fact
   *
   * @param string $adresse2Fact
   */
  public function setAdresse2Fact($adresse2Fact)
  {
    $this->adresse2_fact = $adresse2Fact;
  }

  /**
   * Get adresse2_fact
   *
   * @return string
   */
  public function getAdresse2Fact()
  {
    return $this->adresse2_fact;
  }

  /**
   * Set code_postal_fact
   *
   * @param string $codePostalFact
   */
  public function setCodePostalFact($codePostalFact)
  {
    $this->code_postal_fact = $codePostalFact;
  }

  /**
   * Get code_postal_fact
   *
   * @return string
   */
  public function getCodePostalFact()
  {
    return $this->code_postal_fact;
  }

  /**
   * Set ville_fact
   *
   * @param string $villeFact
   */
  public function setVilleFact($villeFact)
  {
    $this->ville_fact = $villeFact;
  }

  /**
   * Get ville_fact
   *
   * @return string
   */
  public function getVilleFact()
  {
    return $this->ville_fact;
  }

  /**
   * Set pays_fact
   *
   * @param string $paysFact
   */
  public function setPaysFact($paysFact)
  {
    $this->pays_fact = $paysFact;
  }

  /**
   * Get pays_fact
   *
   * @return string
   */
  public function getPaysFact()
  {
    return $this->pays_fact;
  }

  /**
   * Set info_promo
   *
   * @param boolean $infoPromo
   */
  public function setInfoPromo($infoPromo)
  {
    $this->info_promo = $infoPromo;
  }

  /**
   * Get info_promo
   *
   * @return boolean
   */
  public function getInfoPromo()
  {
    return $this->info_promo;
  }

  /**
   * Set info_juridique
   *
   * @param boolean $infoJuridique
   */
  public function setInfoJuridique($infoJuridique)
  {
    $this->info_juridique = $infoJuridique;
  }

  /**
   * Get info_juridique
   *
   * @return boolean
   */
  public function getInfoJuridique()
  {
    return $this->info_juridique;
  }

  /**
   * Set info_evolution
   *
   * @param boolean $infoEvolution
   */
  public function setInfoEvolution($infoEvolution)
  {
    $this->info_evolution = $infoEvolution;
  }

  /**
   * Get info_evolution
   *
   * @return boolean
   */
  public function getInfoEvolution()
  {
    return $this->info_evolution;
  }

  /**
   * Set info_amelioration
   *
   * @param boolean $infoAmelioration
   */
  public function setInfoAmelioration($infoAmelioration)
  {
    $this->info_amelioration = $infoAmelioration;
  }

  /**
   * Get info_amelioration
   *
   * @return boolean
   */
  public function getInfoAmelioration()
  {
    return $this->info_amelioration;
  }

  /**
   * Set info_diffusion
   *
   * @param boolean $infoDiffusion
   */
  public function setInfoDiffusion($infoDiffusion)
  {
    $this->info_diffusion = $infoDiffusion;
  }

  /**
   * Get info_diffusion
   *
   * @return boolean
   */
  public function getInfoDiffusion()
  {
    return $this->info_diffusion;
  }

  /**
   * Set created_at
   *
   * @param datetime $createdAt
   */
  public function setCreatedAt($createdAt)
  {
    $this->created_at = $createdAt;
  }

  /**
   * Get created_at
   *
   * @return datetime
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * Set updated_at
   *
   * @param datetime $updatedAt
   */
  public function setUpdatedAt($updatedAt)
  {
    $this->updated_at = $updatedAt;
  }

  /**
   * Get updated_at
   *
   * @return datetime
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
   * Set statut
   *
   * @param integer $statut
   */
  public function setStatut($statut)
  {
    $this->statut = $statut;
  }

  /**
   * Get statut
   *
   * @return integer
   */
  public function getStatut()
  {
    return $this->statut;
  }

  /**
   * Set active_url
   *
   * @param string $activeUrl
   */
  public function setActiveUrl($activeUrl)
  {
    $this->active_url = $activeUrl;
  }

  /**
   * Get active_url
   *
   * @return string
   */
  public function getActiveUrl()
  {
    return $this->active_url;
  }

  /**
   * Set civilite
   *
   * @param string $civilite
   */
  public function setCivilite($civilite)
  {
    $this->civilite = $civilite;
  }

  /**
   * Get civilite
   *
   * @return string
   */
  public function getCivilite()
  {
    return $this->civilite;
  }

  /**
   * Set rib_etab
   *
   * @param string $ribEtab
   */
  public function setRibEtab($ribEtab)
  {
    $this->rib_etab = $ribEtab;
  }

  /**
   * Get rib_etab
   *
   * @return string
   */
  public function getRibEtab()
  {
    return $this->rib_etab;
  }

  /**
   * Set rib_code
   *
   * @param string $ribCode
   */
  public function setRibCode($ribCode)
  {
    $this->rib_code = $ribCode;
  }

  /**
   * Get rib_code
   *
   * @return string
   */
  public function getRibCode()
  {
    return $this->rib_code;
  }

  /**
   * Set rib_compte
   *
   * @param string $ribCompte
   */
  public function setRibCompte($ribCompte)
  {
    $this->rib_compte = $ribCompte;
  }

  /**
   * Get rib_compte
   *
   * @return string
   */
  public function getRibCompte()
  {
    return $this->rib_compte;
  }

  /**
   * Set rib_rice
   *
   * @param string $ribRice
   */
  public function setRibRice($ribRice)
  {
    $this->rib_rice = $ribRice;
  }

  /**
   * Get rib_rice
   *
   * @return string
   */
  public function getRibRice()
  {
    return $this->rib_rice;
  }

  /**
   * Set rib_domiciliation
   *
   * @param string $ribDomiciliation
   */
  public function setRibDomiciliation($ribDomiciliation)
  {
    $this->rib_domiciliation = $ribDomiciliation;
  }

  /**
   * Get rib_domiciliation
   *
   * @return string
   */
  public function getRibDomiciliation()
  {
    return $this->rib_domiciliation;
  }

  /**
   * Set rib_iban
   *
   * @param string $ribIban
   */
  public function setRibIban($ribIban)
  {
    $this->rib_iban = $ribIban;
  }

  /**
   * Get rib_iban
   *
   * @return string
   */
  public function getRibIban()
  {
    return $this->rib_iban;
  }

  /**
   * Set rib_bic
   *
   * @param string $ribBic
   */
  public function setRibBic($ribBic)
  {
    $this->rib_bic = $ribBic;
  }

  /**
   * Get rib_bic
   *
   * @return string
   */
  public function getRibBic()
  {
    return $this->rib_bic;
  }

  /**
   * Set num_compte
   *
   * @param string $numCompte
   */
  public function setNumCompte($numCompte)
  {
    $this->num_compte = $numCompte;
  }

  /**
   * Get num_compte
   *
   * @return string
   */
  public function getNumCompte()
  {
    return $this->num_compte;
  }

  /**
   * Set cgu
   *
   * @param boolean $cgu
   */
  public function setCgu($cgu)
  {
    $this->cgu = $cgu;
  }

  /**
   * Get cgu
   *
   * @return boolean
   */
  public function getCgu()
  {
    return $this->cgu;
  }

  /**
   * Set type_utilisateur
   *
   * @param string $typeUtilisateur
   */
  public function setTypeUtilisateur($typeUtilisateur)
  {
    $this->type_utilisateur = $typeUtilisateur;
  }

  /**
   * Get type_utilisateur
   *
   * @return string
   */
  public function getTypeUtilisateur()
  {
    return $this->type_utilisateur;
  }

  /**
   * Set mobile2
   *
   * @param string $mobile2
   */
  public function setMobile2($mobile2)
  {
    $this->mobile2 = $mobile2;
  }

  /**
   * Get mobile2
   *
   * @return string
   */
  public function getMobile2()
  {
    return $this->mobile2;
  }

  public function setLocataire()
  {
    $this->type_utilisateur = '1';
  }

  public function setProprietaire()
  {
    $this->type_utilisateur = '2';
  }

  public function setAdmin()
  {
    $this->type_utilisateur = self::ADMIN;
  }

  public function getType()
  {
    if ($this->type_utilisateur == '1')
      return "Locataire";
    elseif ($this->type_utilisateur == '2')
      return "Propriétaire";
    elseif ($this->type_utilisateur == self::ADMIN)
      return "Administrateur";
    else
      return null;
  }

  /**
   * Set mails_second
   *
   * @param string $mailsSecond
   * @return Utilisateur
   */
//    public function setMailsSecond($mailsSecond)
//    {
//        $this->mails_second = $mailsSecond;
//
//        return $this;
//    }

  /**
   * Set second_email
   *
   * @param string $secondEmail
   * @return Utilisateur
   */
  public function setSecondEmail($secondEmail)
  {
    $this->second_email = $secondEmail;
    return $this;
  }

  /**
   * Get mails_second
   *
   * @return string
   */
//    public function getMailsSecond()
//    {
//        return $this->mails_second;
//    }

  /**
   * Get second_email
   *
   * @return string
   */
  public function getSecondEmail()
  {
    return $this->second_email;
  }

  /**
   * Add reservations
   *
   * @param \Pat\UtilisateurBundle\Entity\Reservation $reservations
   * @return Utilisateur
   */
  public function addReservation(\Pat\CompteBundle\Entity\Reservation $reservations)
  {
    $this->reservations[] = $reservations;

    return $this;
  }

  /**
   * Remove reservations
   *
   * @param \Pat\UtilisateurBundle\Entity\Reservation $reservations
   */
  public function removeReservation(\Pat\CompteBundle\Entity\Reservation $reservations)
  {
    $this->reservations->removeElement($reservations);
  }

  /**
   * Get reservations
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getReservations()
  {
    return $this->reservations;
  }

  /**
   * Set rib_agence_resp
   *
   * @param string $ribAgenceResp
   * @return Utilisateur
   */
  public function setRibAgenceResp($ribAgenceResp)
  {
    $this->rib_agence_resp = $ribAgenceResp;

    return $this;
  }

  /**
   * Get rib_agence_resp
   *
   * @return string
   */
  public function getRibAgenceResp()
  {
    return $this->rib_agence_resp;
  }

  /**
   * Set rib_pays
   *
   * @param string $ribPays
   * @return Utilisateur
   */
  public function setRibPays($ribPays)
  {
    $this->rib_pays = $ribPays;

    return $this;
  }

  /**
   * Get rib_pays
   *
   * @return string
   */
  public function getRibPays()
  {
    return $this->rib_pays;
  }

  /**
   * Set rib_titulaire
   *
   * @param string $ribTitulaire
   * @return Utilisateur
   */
  public function setRibTitulaire($ribTitulaire)
  {
    $this->rib_titulaire = $ribTitulaire;

    return $this;
  }

  /**
   * Get rib_titulaire
   *
   * @return string
   */
  public function getRibTitulaire()
  {
    return $this->rib_titulaire;
  }

}
