<?php

namespace Pat\UtilisateurBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use FOS\UserBundle\Controller\ProfileController as BaseController;

class ProfileController extends BaseController
{

  /**
   * Show the user
   */
  public function showAction()
  {
    $current_resa = null;

    $user = $this->container->get('security.context')->getToken()->getUser();
    if (!is_object($user) || !$user instanceof UserInterface) {
      throw new AccessDeniedException('This user does not have access to this section.');
    }

    //on récupère l'utilisateur courant et on bloque cette page aux administrateurs connectés
    $em = $this->container->get('doctrine')->getManager();
    $user = $em->find('PatUtilisateurBundle:Utilisateur', $user->getId());
    $user->setAdresse(stripslashes($user->getAdresse()));
    $arrayRoles = $user->getRoles(); //on test les droits
    if ($arrayRoles[0] == "ROLE_ADMIN") {
      return new RedirectResponse($this->container->get('router')->generate('pat_admin_dashboard'));
      //throw new AccessDeniedException('This user does not have access to this section.');
    }


    //on calcul la prochaine mise à jour à faire
    if ($user->getUpdatedAt()) {
      $updatedDate = new \DateTime($user->getUpdatedAt()->format("Y-m-d H:i:s"));
      $nextUpdate = $updatedDate;
      $nextUpdate->add(new \DateInterval('P1Y'));

      $updatedDate = $updatedDate->format("d/m/Y");
      $nextUpdate = $nextUpdate->format("d/m/Y");

      $session = $this->container->get('request')->getSession();
      if ($session->get('resa_date_debut') && $session->get('resa_date_fin') && $session->get('resa_appartement') && $session->get('resa_prix') && $session->get('resa_arrhes') && $session->get('resa_nbnuits') && $session->get('resa_nbpersonne') && $session->get('resa_locataire')) {
        $current_resa = true;
      }
    }
    else {
      // s'il n'y avait pas de date de modification on en créé une
      $user->setUpdatedAt(new \DateTime("now"));
      $updatedDate = new \DateTime($user->getUpdatedAt()->format("Y-m-d H:i:s"));
      $nextUpdate = $updatedDate;
      $nextUpdate->add(new \DateInterval('P1Y'));
      $nextUpdate = $nextUpdate->format("d/m/Y");

      $em->persist($user);
      $em->flush();
    }

    return $this->container->get('templating')->renderResponse('FOSUserBundle:Profile:show.html.'.$this->container->getParameter('fos_user.template.engine'), array('user' => $user, 'nextUpdate' => $nextUpdate, 'current_resa' => $current_resa));
  }

}
