<?php

namespace Pat\UtilisateurBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Pat\UtilisateurBundle\Entity\Utilisateur;
use Pat\FrontBundle\Form\InscriptionLocataireForm;
use FOS\UserBundle\Controller\RegistrationController as BaseController;

class RegistrationController extends BaseController
{

  public function registerResidentAction(Request $request)
  {
    $session = $request->getSession();

    if (!$session->get('rand1')) {
      $rand1 = rand(1, 9);
      $rand2 = rand(1, 9);
      $session->set('rand1', $rand1);
      $session->set('rand2', $rand2);
    }

    $em = $this->container->get('doctrine')->getManager();

    $resident = new Utilisateur();
    $form = new InscriptionLocataireForm(
      $session->get('rand1'), $session->get('rand2')
    );
    $formInscription = $this->container->get('form.factory')->create($form, $resident);

    if ($request->getMethod() == 'POST') {

      $formInscription->handleRequest($request);

      if ($formInscription->isValid()) {

        $patternEmail = '#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';

        if (!preg_match($patternEmail, $formInscription['email']->getData()))
          $this->container->get('session')->getFlashBag()->add('alert', "L'adresse mail n'est pas valide");
        else {
          $isExistEmail = $em->getRepository('PatUtilisateurBundle:Utilisateur')->checkEmailCompteAction($formInscription['email']->getData());

          if (!$isExistEmail) {

            //$resident->setEmailCanonical($formInscription['email']->getData());
            $resident->setEnabled(false);

            // On définit que l'utilisateur est de type Locataire
            $resident->setLocataire();
            $resident->setCGU("1");

            //gestion du mot de passe
            $password = $formInscription['plainPassword']->getData();
            $encoder = $this->container->get('security.encoder_factory')->getEncoder($resident);
            $resident->setPassword($encoder->encodePassword($password, $resident->getSalt()));
            $resident->setConfirmationToken($this->container->get('fos_user.util.token_generator')->generateToken());
            $url = $this->container->get('router')->generate('fos_user_registration_confirm', array('token' => $resident->getConfirmationToken()), true);
            $em->persist($resident);
            $em->flush();

            $resident->generateUsername();

            $em->flush();


            //envoi du mail au locataire
            $message_email = \Swift_Message::newInstance()
              ->setSubject("Création de votre compte locataire - ClassAppart ®")
              ->setFrom('contact@class-appart.fr')
              ->setTo($resident->getEmail());

//                        if($resident->getMailsSecond())
//                            $message_email->setCc(explode(',', $resident->getMailsSecond()));
            if ($resident->getSecondEmail()) {
              $message_email->setCc($resident->getSecondEmail());
            }

            $textBody = $this->container->get('templating')->render('PatCompteBundle:AdminLocataire:email_validation.txt.twig', array('utilisateur' => $resident, 'password' => $password, 'url' => !empty($url) ? $url : ''));
            $htmlBody = $this->container->get('templating')->render('PatCompteBundle:AdminLocataire:email_validation.html.twig', array('utilisateur' => $resident, 'password' => $password, 'url' => !empty($url) ? $url : ''));

            if (!empty($htmlBody)) {
              $message_email->setBody($htmlBody, 'text/html')
                ->addPart($textBody, 'text/plain');
            }
            else
              $message_email->setBody($textBody);

            $this->container->get('mailer')->send($message_email);

            // On envoie un mail à l'admin
            $message_email = \Swift_Message::newInstance()
              ->setSubject("Inscription d'un ".$resident->getType()." - ClassAppart")
              ->setFrom('contact@class-appart.com')
              ->setTo('info@class-appart.com')
            ;

            $textBody = $this
              ->container
              ->get('templating')
              ->render('PatUtilisateurBundle:Registration:email_new_user_admin.txt.twig', [
              'user' => $resident,
              ])
            ;
            $htmlBody = $this
              ->container
              ->get('templating')
              ->render('PatUtilisateurBundle:Registration:email_new_user_admin.html.twig', [
              'user' => $resident,
              ])
            ;

            if (!empty($htmlBody)) {
              $message_email
                ->setBody($htmlBody, 'text/html')
                ->addPart($textBody, 'text/plain')
              ;
            }
            else {
              $message_email->setBody($textBody);
            }

            $this->container->get('mailer')->send($message_email);

            $session->remove('rand1');
            $session->remove('rand2');

            return $this->container->get('templating')->renderResponse('PatUtilisateurBundle:Registration:inscriptionResidentConfirmed.html.twig', array('mail' => $resident->getEmail()));
          }
          else
            $this->container->get('session')->getFlashBag()->add('alert', "Cette adresse mail existe déjà");
        }
      }
    }


    return $this->container->get('templating')->renderResponse(
        'PatUtilisateurBundle:Registration:inscriptionResident.html.twig', array(
        'formInscription' => $formInscription->createView(),
        'target_path' => 'pat_front_inscription_resident',
        'question' => $form->getQuestion()
        )
    );
  }

}
