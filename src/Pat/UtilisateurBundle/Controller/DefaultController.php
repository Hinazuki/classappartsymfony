<?php

namespace Pat\UtilisateurBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware,
    Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

  public function indexAction()
  {
    return new RedirectResponse($this->container->get('router')->generate('pat_appartement_index'));
  }

}
