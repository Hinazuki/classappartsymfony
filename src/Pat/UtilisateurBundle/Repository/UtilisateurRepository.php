<?php

namespace Pat\UtilisateurBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Entities;

class UtilisateurRepository extends EntityRepository
{

  public function searchUtilisateurQuery($nom, $prenom, $username, $type = null, $offset = null, $limit = null)
  {
    // and call the query builder on it
    $qb = $this->_em->createQueryBuilder();
    $qb->select('u')
      ->from('Pat\UtilisateurBundle\Entity\Utilisateur', 'u')
      ->where('1 = 1');

    if (!empty($nom)) {
      $qb->andWhere("u.nom like '%".$nom."%'");
    }

    if (!empty($prenom)) {
      $qb->andWhere("u.prenom like '%".$prenom."%'");
    }

    if (!empty($username)) {
      $qb->andWhere("u.username like '%".$username."%'");
    }

    if (!is_null($type))
      $qb->andWhere("u.type_utilisateur = '".$type."'");

    $qb->add('orderBy', 'u.id DESC');

    if ($offset != null && $limit != null) {
      $qb->setFirstResult($offset);
      $qb->setMaxResults($limit);
    }

    return $qb->getQuery();
  }

  //recherche un ou plusieurs utilisateur par critères
  public function searchUtilisateur($nom, $prenom, $username, $type = null, $offset = null, $limit = null)
  {
    $query = $this->searchUtilisateurQuery($nom, $prenom, $username, $type, $offset, $limit);

    return $query->getResult();
  }

  /**
   * Test si l'adresse mail existe
   */
  public function checkEmailCompteAction($email, $id = "")
  {

    //$qb = $this->_em->createQueryBuilder();
    $qb = $this->createQueryBuilder('u')
      ->select('u')
      ->where("u.email = :email");
    if (!empty($id))
      $qb->andWhere("u.id != '$id'");
    $qb->setParameter('email', $email);

    $result = $qb->getQuery()->getResult();

    if (sizeof($result) > 0)
      $result = true;
    else
      $result = false;

    $this->_em->detach($qb);
    //$this->clear();
    unset($qb);

    return $result;
  }

  /**
   * Test si l'adresse mail existe
   */
  public function checkUsernameCompteAction($username, $id = "")
  {

    $qb = $this->_em->createQueryBuilder();
    $qb->select('u')
      ->from('Pat\UtilisateurBundle\Entity\Utilisateur', 'u')
      ->where("u.username = :username");

    if (!empty($id)) {
      $qb->andWhere("u.id != '$id'");
    }

    $qb->setParameter('username', $username);

    $result = $qb->getQuery()->getResult();

    if (sizeof($result) > 0) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Génération d'un mot de passe aléatoire
   */
  public function generatePassword()
  {
    $pass = "";
    $chaine = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    //nombre de caracteres dans le mot de passe
    $nb_caract = 8;

    for ($u = 1; $u <= $nb_caract; $u++) {

      //on compte le nombre de caractÃ¨res prÃ©sents dans notre chaine
      $nb = strlen($chaine);

      // on choisie un nombre au hasard entre 0 et le nombre de caractÃ¨res de la chaine
      $nb = mt_rand(0, ($nb - 1));
      $pass .= $chaine[$nb];
    }
    return $pass;
  }

}
