<?php

namespace Pat\UtilisateurBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseType;
use Pat\UtilisateurBundle\Form\Type\UtilisateurFormType;
use Pat\UtilisateurBundle\Entity\Utilisateur;

class ProfileFormType extends BaseType
{

  public function buildUserForm(FormBuilderInterface $builder, array $options)
  {
    //parent::buildUserForm($builder, $options);
    //parent::buildForm($builder, $options);
    // Add custom fields
    $builder
      ->add('civilite', 'choice', array(
        'choices' => array(
          'M.' => 'M.',
          'Mlle' => 'Mlle',
          'Mme' => 'Mme',
        ),
        'multiple' => false,
      ))
      ->add('prenom', 'text', array('required' => false))
      ->add('nom')
      ->add('societe')
      ->add('siret')
      ->add('fonction')
      ->add('date_naissance', 'birthday', array(
        'label' => 'Date de naissance',
        'widget' => 'choice',
        'years' => range(date('Y'), 1900),
        'format' => 'dd / MM / yyyy',
        'empty_value' => '',
        'required' => false))
      ->add('nationalite')
      ->add('langue_parle', 'choice', array(
        'choices' => array(
          'Français' => 'Français',
          'Anglais' => 'Anglais',
          'Allemand' => 'Allemand',
          'Espagnol' => 'Espagnol',
          'Italien' => 'Italien',
          'Russe' => 'Russe',
          'Chinois' => 'Chinois',
        ),
        'multiple' => false,
      ))
      ->add('adresse')
      ->add('adresse2', 'text', array('required' => false))
      ->add('code_postal')
      ->add('ville')
      ->add('pays')
      ->add('telephone')
      ->add('mobile', 'text', array('label' => 'Mobile Mr :', 'required' => false))
      ->add('mobile2', 'text', array('label' => 'Mobile Mme :', 'required' => false))
//            ->add('mails_second', 'textarea', array(
//                    'label' => "Adresses e-mail secondaires :",
//                    'required' => false
//                ))
      ->add('second_email', 'text', array(
        'label' => "Adresse e-mail secondaire :",
        'required' => false
      ))
      ->add('moyen_contact', 'choice', array(
        'choices' => array(
          'TELEPHONE' => 'Téléphone',
          'MAIL' => 'Mail',
          'SMS' => 'SMS'
        ),
        'multiple' => false,
        'required' => false,
        "label" => "Moyen de contact préféré"
      ))
      //->add('username')
      //->add('email', 'email')
      ->add('adresse_fact')
      ->add('adresse2_fact')
      ->add('code_postal_fact')
      ->add('ville_fact')
      ->add('pays_fact')
      ->add('rib_etab', 'text', array("required" => false, 'max_length' => 5, 'label' => "Code Banque"))
      ->add('rib_code', 'text', array("required" => false, 'max_length' => 5, 'label' => "Code Guichet"))
      ->add('rib_compte', 'text', array("required" => false, 'max_length' => 11, 'label' => "Num compte"))
      ->add('rib_rice', 'text', array("required" => false, 'max_length' => 2, 'label' => "Clé RIB"))
      ->add('rib_domiciliation', 'text', array("required" => false, 'max_length' => 100, 'label' => "Domiciliation"))
      ->add('rib_iban', 'text', array("required" => false, 'max_length' => 27, 'label' => "Code IBAN"))
      ->add('rib_bic', 'text', array("required" => false, 'label' => "Code SWIFT (BIC)"))
      ->add('rib_agence_resp', 'text', array("required" => false, 'max_length' => 255, 'label' => "Agence responsable du compte"))
      ->add('rib_pays', 'text', array("required" => false, 'max_length' => 100, 'label' => "Pays"))
      ->add('rib_titulaire', 'text', array("required" => false, 'max_length' => 100, 'label' => "Titulaire"))
      ->add('email', 'email', array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'));
  }

  public function getName()
  {
    return 'pat_user_profile';
  }

}
