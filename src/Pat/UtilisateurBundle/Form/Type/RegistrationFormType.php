<?php

namespace Pat\UtilisateurBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Pat\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\EqualTo;

/**
 * Appels dans l'ordre :
 *  __construct
 *  setRequest
 *  buildForm
 */
class RegistrationFormType extends BaseType
{

  protected $rand1;
  protected $rand2;
  protected $question;
  private $request;

  /**
   * @param string $class The User class name
   */
  public function __construct($class)
  {
    parent::__construct($class);
  }

  /**
   * Pas le temps de surcharger tout le contrôleur de FOS.
   *
   * Appelé par l'injection de services.
   * Voir Pat\UtilisateurBundle\Resources\config\services.yml :
   *   pat_user.registration.form.type:
   *
   * @param Request $request
   * @link https://stackoverflow.com/questions/21392215/how-can-i-get-request-object-inside-a-class-in-symfony-2
   */
  public function setRequest($request)
  {
    // La méthode est appelée 2 fois.
    // Au second appel, null est passé en paramètre.
    if ($request) {
      $this->request = $request;
      $session = $this->request->getSession();

      if (!$session->get('rand1')) {
        $session->set('rand1', rand(1, 9));
        $session->set('rand2', rand(1, 9));
      }

      $this->rand1 = $session->get('rand1');
      $this->rand2 = $session->get('rand2');
    }
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    //parent::buildForm($builder, $options);
    // Add custom fields
    $builder
      ->add('civilite', 'choice', array(
        'label' => 'Civilité :',
        'choices' => array(
          'M.' => 'M.',
          'Mlle' => 'Mlle',
          'Mme' => 'Mme',
        ),
        'multiple' => false,
      ))
      ->add('prenom', 'text', array('label' => 'Prénom : *', 'required' => true))
      ->add('nom', 'text', array("label" => "Nom : *", 'required' => true))
      ->add('societe', 'text', array("label" => "Société :", 'required' => false))
      ->add('siret', 'text', array("label" => "Siret :", 'required' => false))
      ->add('fonction', 'text', array("label" => "Fonction :", 'required' => false))
      ->add('date_naissance', 'birthday', array(
        'label' => 'Date de naissance :',
        'widget' => 'choice',
        'years' => range(date('Y') - 18, 1900),
        'format' => 'dd / MM / yyyy',
        'empty_value' => '',
        'required' => false))
      ->add('nationalite', 'text', array("label" => "Nationnalité"))
      ->add('langue_parle', 'choice', array(
        'label' => 'Langue parlée :',
        'choices' => array(
          'Français' => 'Français',
          'Anglais' => 'Anglais',
          'Allemand' => 'Allemand',
          'Espagnol' => 'Espagnol',
          'Italien' => 'Italien',
          'Russe' => 'Russe',
          'Chinois' => 'Chinois',
        ),
        'multiple' => false,
      ))
      ->add('adresse', 'text', array("label" => "Adresse : *"))
      ->add('adresse2', 'text', array("label" => "Adresse suite :", 'required' => false))
      ->add('code_postal', 'text', array("label" => "Code postal : *", 'max_length' => 5))
      ->add('ville', 'text', array("label" => "Ville : *"))
      ->add('pays', 'choice', array(
        'label' => 'Pays :',
        'choices' => array(
          'France' => 'France',
        ),
        'multiple' => false,
      ))
      ->add('telephone', 'text', array("label" => "Téléphone :"))
      ->add('mobile', 'text', array("label" => "Mobile Mr :"))
      ->add('mobile2', 'text', array("label" => "Mobile Mme :", 'required' => false))
//            ->add('mails_second', 'textarea', array(
//                    'label' => "Adresses e-mail secondaires :",
//                    'required' => false
//                ))
      ->add('second_email', 'text', array(
        'label' => "Adresse e-mail secondaire :",
        'required' => false
      ))
      ->add('moyen_contact', 'choice', array(
        'choices' => array(
          'TELEPHONE' => 'Téléphone',
          'MAIL' => 'Mail',
          'SMS' => 'SMS'
        ),
        'multiple' => false,
        'required' => false,
        "label" => "Moyen de contact préféré :"
      ))

//            ->add('rib_etab', 'text', array("label" => "Code établissement", 'max_length' => 5))
//            ->add('rib_code', 'text', array("label" => "Code guichet", 'max_length' => 5))
//            ->add('rib_compte', 'text', array("label" => "Numéro de compte", 'max_length' => 11))
//            ->add('rib_rice', 'text', array("label" => "Code RICE", 'max_length' => 2))
//            ->add('rib_domiciliation', 'text', array("label" => "Domiciliation"))
//            ->add('rib_iban', 'text', array("label" => "IBAN", 'max_length' => 27))
//            ->add('rib_bic', 'text', array("label" => "BIC", 'required' => false))
      ->add('email', 'email', array('label' => 'Adresse e-mail :', 'translation_domain' => 'FOSUserBundle'))
      ->add('plainPassword', 'repeated', array(
        'type' => 'password',
        'options' => array('translation_domain' => 'FOSUserBundle'),
        'first_options' => array('label' => 'Mot de passe : *'),
        'second_options' => array('label' => 'Confirmer le mot de passe : *'),
        'invalid_message' => 'fos_user.password.mismatch',))
      ->add('cgu', 'checkbox', array('label' => " ", "required" => true))
    ;

    // Pages publiques, hors process de résa ou admin.
    if ($this->rand1 && $this->rand2) {
      $this->question = sprintf(
        'Quelle est la somme de %s + %s ?', $this->rand1, $this->rand2
      );
      $builder
        ->add('human', 'text', array(
          'label' => 'Vous êtes humain : *',
          'attr' => array(
            'placeholder' => $this->question
          ),
          'required' => true,
          'mapped' => false,
          'constraints' => array(
            new EqualTo(array(
              'value' => $this->rand1 + $this->rand2,
              'message' => 'La somme ne correspond pas.',
              ))
          )
      ));
    }
  }

  public function getName()
  {
    return 'pat_user_registration';
  }

  public function getQuestion()
  {
    return $this->question;
  }

}
