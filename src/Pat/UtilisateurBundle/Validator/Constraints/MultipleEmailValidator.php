<?php

namespace Pat\UtilisateurBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Multiple Email validator.
 *
 * @link https://symfony.com/doc/2.3/cookbook/validation/custom_constraint.html
 */
class MultipleEmailValidator extends ConstraintValidator
{

  public function validate($value, Constraint $constraint)
  {
    if (!preg_match('/^[a-zA-Z0-9]+$/', $value, $matches)) {
      $this->context->addViolation(
        $constraint->message, array('%string%' => $value)
      );
    }
  }

}
