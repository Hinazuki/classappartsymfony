<?php

namespace Pat\UtilisateurBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Multiple Email validator.
 *
 * @Annotation
 * @link https://symfony.com/doc/2.3/cookbook/validation/custom_constraint.html
 */
class MultipleEmail extends Constraint
{

  public $message = 'Some email address are invalid.';

}
