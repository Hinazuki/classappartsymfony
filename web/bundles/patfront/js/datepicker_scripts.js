var dates = $( ".cal_date_in, .cal_date_out" ).datepicker({
    changeMonth: true,
    closeText: 'Fermer',
    prevText: 'Précédent',
    nextText: 'Suivant',
    currentText: 'Aujourd\'hui',
    monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
    'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
    monthNamesShort: ['Janv.','Févr.','Mars','Avril','Mai','Juin',
    'Juil.','Août','Sept.','Oct.','Nov.','Déc.'],
    dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
    dayNamesShort: ['Dim.','Lun.','Mar.','Mer.','Jeu.','Ven.','Sam.'],
    dayNamesMin: ['D','L','M','M','J','V','S'],
    weekHeader: 'Sem.',
    dateFormat: 'dd/mm/yy',
    numberOfMonths: 1,
    showOn: "button",
    buttonImage: "/img/date_picker.png",
    buttonImageOnly: true,
    minDate: "0",
    onSelect: function( selectedDate ) {
            var c = $(this).hasClass("cal_date_in") ? ".cal_date_out" : ".cal_date_in";
            var option = $(this).hasClass("cal_date_in") ? "minDate" : "maxDate",
                    instance = $( this ).data( "datepicker" ),
                    date = $.datepicker.parseDate(
                            instance.settings.dateFormat ||
                            $.datepicker._defaults.dateFormat,
                            selectedDate, instance.settings );
            if (undefined != $(this).attr('min_days')) {
                date.setDate(date.getDate() + parseInt($(this).attr('min_days')));
            }
            $(this).parentsUntil("#contentBis").find(c).datepicker( "option", option, date );
    }
});

$('.cal_date_in, .cal_date_out').click(function() {
    $(this).datepicker().datepicker( "show" );
});

$('.cal_date_in, .cal_date_out').click(function() {
    $(this).datepicker().datepicker( "show" )
});
