$(document).ready(function() {
    $('.blocForm.active .content').hide();
    $('.blocForm.active .title').addClass("bloc-closed");
    
    $("#appartement_ville").click(function() {
        var ville = $("#appartement_ville").val();
    });


    $("#appartement_ville").keyup(function() {
        var ville = $("#appartement_ville").val();
        var DATA = 'ville=' + ville;
        var url = $("#url_ville_search").val();

        if(ville.length >= 3){
            $("#imgLoader").css("visibility", "visible");
            $.ajax({
                type: "POST",
                url: url,
                data: DATA,
                cache: false,
                success: function(data){
                    $('#resultats_recherche_villes').html(data);
                    $("#resultats_recherche_villes").show();
                    $("#imgLoader").css("visibility", "hidden");
                },
                error:function (xhr, ajaxOptions, thrownError){
                    //alert("error : "+xhr.status);
                    $("#imgLoader").css("visibility", "hidden");
                }
            });     
            return false;
        }
    });
});

$('.blocForm.active .title').click(function() {
    var bloc = $(this).parent();
    if($(this).hasClass("bloc-opened")) {
        $(this).removeClass("bloc-opened");
        $(this).addClass("bloc-closed");
        bloc.find(".content").slideUp();
    }
    else if($(this).hasClass("bloc-closed")) {
        $(this).removeClass("bloc-closed");
        $(this).addClass("bloc-opened");
        bloc.find(".content").slideDown();
    }
});