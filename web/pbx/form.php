<?php

$paybox = array(
  'site' => '1080371',
  'rank' => '02',
  //'id' => '394993981',
  'id' => '722316939',
  'return' => 'montant:M;ref:R;auto:A;trans:T;abonnement:B;paiement:P;carte:C;idtrans:S;pays:Y;erreur:E;validite:D;IP:I;BIN6:N;digest:H;sign:K',
  'hash' => 'SHA512',
  'dev' => array(
    'url' => 'https://preprod-tpeweb.e-transactions.fr/cgi/MYchoix_pagepaiement.cgi',
    'key' => '8d99b6b2485eebb14e37a1b8eb7b7d9672c2e2f4943e4e4a00c10b5907a5e7a2bb95dfa247d568baf89203b606ec845f34573b77d82c1322f66adf7f522de0a2',
    'success' => 'http://www.class-appart.com/pbx/effectue.php',
    'refused' => 'http://www.class-appart.com/pbx/refuse.php',
    'canceled' => 'http://www.class-appart.com/pbx/annule.php',
    'respond' => 'http://www.class-appart.com/pbx/repondre.php',
    'waiting' => 'http://www.class-appart.com/pbx/attente.php',
    'ipn' => 'http://www.class-appart.com/pbx/ipn.php'
  )
);

$resa = array(
  'reference' => '00001234',
  'email' => 'a.guidon+loc@ovea.com'
);

$total = round(((float) '123.45') * 100);
//$total = 12297;
$time = date('c');
//$time = '2017-09-28T16:04:44+02:00';

$formData = array(
  'PBX_SITE' => $paybox['site'],
  'PBX_RANG' => $paybox['rank'],
  'PBX_IDENTIFIANT' => $paybox['id'],
  'PBX_TOTAL' => $total,
  'PBX_DEVISE' => '978',
  'PBX_CMD' => $resa['reference'],
  'PBX_PORTEUR' => $resa['email'],
  'PBX_RETOUR' => $paybox['return'],
  'PBX_HASH' => $paybox['hash'],
  'PBX_TIME' => $time,
//  'PBX_LANGUE' => 'FRA',
  'PBX_TYPEPAIEMENT' => 'CARTE',
//  'PBX_TYPECARTE' => 'CB',
  'PBX_EFFECTUE' => $paybox['dev']['success'],
  'PBX_REFUSE' => $paybox['dev']['refused'],
  'PBX_ANNULE' => $paybox['dev']['canceled'],
  'PBX_REPONDRE_A' => $paybox['dev']['respond'],
//  'PBX_ATTENTE' => $paybox['dev']['waiting'],
//  'PBX_IPN' => $paybox['dev']['ipn']
);

$queryData = array();
foreach ($formData as $key => $value) {
  $queryData[] = $key.'='.$value;
}
$queryString = implode('&', $queryData);

// clef ASCII => Binaire
$binKey = pack('H*', $paybox['dev']['key']);
// Hâchage.
$hmac = strtoupper(hash_hmac('sha512', $queryString, $binKey));

$formData['PBX_HMAC'] = $hmac;
?>
<html>
  <head>
    <title></title>
  </head>
  <body>

<pre>
<?php
var_dump($formData);
?>
</pre>

<form method="POST" action="<?= $paybox['dev']['url'] ?>">
<!--<form method="POST" action="/pbx/ipn.php">-->
<?php foreach ($formData as $key => $value): ?>
  <input type="hidden" name="<?= $key ?>" value="<?= $value ?>">
<?php endforeach; ?>

  <input type="submit" value="Envoyer" />
</form>

  </body>
</html>
