<?php

class Dumper {

  /**
   * var_dump vers une chaîne.
   *
   * @param mixed $var
   * @return string
   */
  public static function var_dump($var) {
    ob_start();
    var_dump($var);
    return html_entity_decode(strip_tags(ob_get_clean()));
  }

  /**
   * Ajoute le résultat d'un print_r dans le journal.
   *
   * @param string $logFile
   * @param mixed $var
   */
  public static function log($logFile, $var) {
    $handle = fopen($logFile, 'ab');
    if ($handle !== false) {
      $result = fwrite($handle, print_r($var, true).PHP_EOL);
      fclose($handle);
    }
  }

  /**
   * Ajoute le résultat d'un var_dump dans le journal.
   *
   * @param string $logFile
   * @param mixed $var
   */
  public static function log_dump($logFile, $var) {
    $handle = fopen($logFile, 'ab');
    if ($handle !== false) {
      $result = fwrite($handle, self::var_dump($var).PHP_EOL);
      fclose($handle);
    }
  }

}

